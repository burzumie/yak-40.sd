/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"
#include "../../Res/sn/snd_resX.h"
#include "Sounder.h"

#ifdef _DEBUG
_CrtMemState memoryState; 
#endif

HINSTANCE	g_hInstance=NULL;
HWND		g_hWND=NULL;
bool				g_bConnectedToLogic		= false;
CSimConnect			*g_SimData				= NULL;

BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
#ifdef _DEBUG
	_CrtMemCheckpoint(&memoryState);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
#endif
	g_hInstance=(HINSTANCE)hModule;
	switch(ul_reason_for_call)	{
		case DLL_PROCESS_ATTACH: 
			DisableThreadLibraryCalls((HMODULE)hModule);
			g_hWND=GetActiveWindow();
		break;
		case DLL_THREAD_ATTACH: 
		break;
		case DLL_THREAD_DETACH:
		break;
		case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

void UpdatePanel()
{
	if(g_pSounder)
		g_pSounder->Update();

}

bool OpenPipe();
void ClosePipe();

void FSAPI fnInitPanel(void)
{
	g_bConnectedToLogic=OpenPipe();

	if(CFG_GET(CFG_SOUND_ENABLE)) {
		if(!g_pSounder)
			g_pSounder=new CSounder();
		if(g_pSounder)
			g_pSounder->Init();
	}
}

void FSAPI fnDeinitPanel(void)
{
	if(g_pSounder) {
		delete g_pSounder;
		g_pSounder=NULL;
	}

#ifdef _DEBUG
	CHECK_MEM_LEAK("SN.log");
#endif
}

#ifdef GAUGE_NAME
#undef GAUGE_NAME
#endif

#ifdef GAUGEHDR_VAR_NAME
#undef GAUGEHDR_VAR_NAME
#endif

#ifdef GAUGE_W
#undef GAUGE_W
#endif

#define GAUGE_NAME         "main"
#define GAUGEHDR_VAR_NAME  snd_hdr
#define GAUGE_W            MISC_DUMMY_SX

extern PELEMENT_HEADER snd_list;
GAUGE_CALLBACK snd_cb;
GAUGE_HEADER_FS700(GAUGE_W, GAUGE_NAME, &snd_list, 0, snd_cb, 0, 0, 0);
MAKE_STATIC(snd_bg,MISC_DUMMY,NULL,NULL,IMAGE_USE_TRANSPARENCY,0,0,0)
PELEMENT_HEADER	snd_list = &snd_bg.header;

void FSAPI snd_cb( PGAUGEHDR pgauge, SINT32 service_id, UINT32 extra_data ) 
{
	switch(service_id) {
		case PANEL_SERVICE_PRE_UPDATE:
			UpdatePanel(); 
		break;
	}
}

GAUGESIMPORT	ImportTable =						
{													
	{ 0x0000000F, (PPANELS)NULL },						
	{ 0x00000000, NULL }								
};													

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&snd_hdr),
		0
	}
};

HANDLE hFileMappingAPI=NULL;
HANDLE hFileMappingSIM=NULL;

bool OpenPipe()
{
	DWORD sizeAPI=sizeof(SDAPIEntrys);
	DWORD sizeSIM=sizeof(CSimConnect);

	hFileMappingAPI=OpenFileMappingA(FILE_MAP_READ|FILE_MAP_WRITE,FALSE,API_SHARE_NAME);
	hFileMappingSIM=OpenFileMappingA(FILE_MAP_READ|FILE_MAP_WRITE,FALSE,SIM_SHARE_NAME);

	if(hFileMappingAPI==NULL||hFileMappingSIM==NULL) {
		return false;
	}

	g_SimData=(CSimConnect *)MapViewOfFile(hFileMappingSIM,FILE_MAP_READ|FILE_MAP_WRITE,0,0,sizeSIM);
	g_pSDAPIEntrys=(SDAPIEntrys *)MapViewOfFile(hFileMappingAPI,FILE_MAP_READ|FILE_MAP_WRITE,0,0,sizeAPI);

	if(g_SimData==NULL||g_pSDAPIEntrys==NULL) {
		return false;
	}

	return true;
}

void ClosePipe()
{
	if(g_SimData) {	
		UnmapViewOfFile(g_SimData);
		g_SimData=NULL;
	}
	if(g_pSDAPIEntrys) {
		UnmapViewOfFile(g_pSDAPIEntrys);
		g_pSDAPIEntrys=NULL;
	}
	if(hFileMappingSIM) {
		CloseHandle(hFileMappingSIM);
		hFileMappingSIM=NULL;
	}
	if(hFileMappingAPI) {
		CloseHandle(hFileMappingAPI);
		hFileMappingAPI=NULL;
	}
}

