/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "Main.h"

enum PLAY_TYPE {
	PLAY_TYPE_ONCE,
	PLAY_TYPE_PLAIN,
	PLAY_TYPE_STOP,
	PLAY_TYPE_MAX
};

class CSndToken;

typedef void (*ProcessFuncDef)(CSndToken *snd);

class CSndToken 
{
private:
	std::string		m_Name;
	std::string		m_FileName;
	LONG			m_Volume;
	bool			m_Loop;
	PLAY_TYPE		m_PlayType;
	int				m_Type;
	ProcessFuncDef	ProcessFunc;

public:
	int				m_Timer;
	bool			m_TimerForced;

	sndkit::CSound *m_Sound;

	CSndToken();
	CSndToken(std::string name,std::string filename,LONG volume,bool loop,PLAY_TYPE playtype,int type,sndkit::CSound *snd);
	~CSndToken();

	void Update();
	void Play();

private:
	void SetFunc();
};

typedef CSndToken* pSndToken;
typedef std::list<pSndToken> SndTokensList;

typedef struct EXS_SOUNDS {
	std::string filename;
	std::string name;
	sndkit::CSound *m_Sound;
	LONG defvol;
	LONG oldvol;
	int playint;
}EXS_SOUNDS,*PEXS_SOUNDS,**PPEXS_SOUNDS;

class CExternalSounder
{
private:
	HRESULT hr;
	sndkit::CSoundManager *m_SndMgr;
	bool		m_Enabled;
	int			m_NumberOfSounds;
	SndTokensList lst_SndTokens;
	PEXS_SOUNDS	m_SoundsList;

	void CheckMuted();

public:
	CExternalSounder(sndkit::CSoundManager *snd);
	~CExternalSounder();

	void LoadAll();
	void Update();

};
