/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 3 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Common/CommonSys.h"
#include "../Common/Macros.h"
#include "../Common/CommonAircraft.h"
#include "../Common/CommonSimconnect.h"
#include "../Common/SimVars.h"
#include "../Api/Constants.h"
#include "../Api/APIEntry.h"
#include "../Api/ClientDef.h"
#include "../Lib/SimTools.h"
#include "../Lib/SimHack.h"
#include "../Lib/Tools.h"
#include "../Lib/sndkit.h"
#include "../lib/IniFile.h"

