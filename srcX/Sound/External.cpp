/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "External.h"

sndkit::CSound *m_SoundCurrent=NULL;

#define PROCESSSND(S) /**/ \
	if(!m_SoundCurrent)	{ \
		m_SoundCurrent=snd->m_Sound; \
	} \
	if(SND_GET(S)&&m_SoundCurrent&&!m_SoundCurrent->IsSoundPlaying()) { \
	m_SoundCurrent=snd->m_Sound; \
	if(!snd->m_TimerForced) { \
		snd->Play();	\
		snd->m_TimerForced=true; \
		snd->m_Timer=0; \
	} \
	SND_SET(S,0);	\
}

void _DUMMY					(CSndToken *snd){}

void _STAIRS_RETRACTED		(CSndToken *snd){	PROCESSSND(SND_STAIRS_RETRACTED			);}
void _SURFACES_FREE   		(CSndToken *snd){	PROCESSSND(SND_SURFACES_FREE			);}
void _TRIMMERS_NEUTRAL		(CSndToken *snd){	PROCESSSND(SND_TRIMMERS_NEUTRAL			);}
void _2077_ON         		(CSndToken *snd){	PROCESSSND(SND_2077_ON					);}
void _ELECTRICITY_CHECKED	(CSndToken *snd){	PROCESSSND(SND_ELECTRICITY_CHECKED		);}
void _ARK_ON          		(CSndToken *snd){	PROCESSSND(SND_ARK_ON					);}
void _STAB_2          		(CSndToken *snd){	PROCESSSND(SND_STAB_2					);}
void _BRAKES_CHECKED  		(CSndToken *snd){	PROCESSSND(SND_BRAKES_CHECKED			);}
void _GMK_ON         		(CSndToken *snd){	PROCESSSND(SND_GMK_ON					);}
void _FLAPS20         		(CSndToken *snd){	PROCESSSND(SND_FLAPS20					);}
void _RV3M_DH100      		(CSndToken *snd){	PROCESSSND(SND_RV3M_DH100				);}
void _ILS0_CHECKED    		(CSndToken *snd){	PROCESSSND(SND_ILS0_CHECKED				);}
void _WINDOW_CLOSED   		(CSndToken *snd){	PROCESSSND(SND_WINDOW_CLOSED			);}
void _DECISION_HEIGHT		(CSndToken *snd){	PROCESSSND(SND_DECISION_HEIGHT			);}
void _GEAR_DOWN_3_GREEN_ON	(CSndToken *snd){	PROCESSSND(SND_GEAR_DOWN_3_GREEN_ON		);}
void _LIGHTS_RETRACTED		(CSndToken *snd){	PROCESSSND(SND_LIGHTS_RETRACTED			);}
void _GEAR_RETRACTING		(CSndToken *snd){	PROCESSSND(SND_GEAR_RETRACTING			);}
void _GEAR_UP_3_RED_ON		(CSndToken *snd){	PROCESSSND(SND_GEAR_UP_3_RED_ON			);}
void _SAFE_ALTITUDE			(CSndToken *snd){	PROCESSSND(SND_SAFE_ALTITUDE			);}
void _PARKING_BRAKE			(CSndToken *snd){	PROCESSSND(SND_PARKING_BRAKE			);}
void _PDD_ON				(CSndToken *snd){	PROCESSSND(SND_PDD_ON					);}
void _LIGHTS_EXTENDED		(CSndToken *snd){	PROCESSSND(SND_LIGHTS_EXTENDED			);}
void _FUEL_PUMPS_ON			(CSndToken *snd){	PROCESSSND(SND_FUEL_PUMPS_ON			);}
void _MANOMETERS_ON			(CSndToken *snd){	PROCESSSND(SND_MANOMETERS_ON			);}
void _APU_VALVE_ON			(CSndToken *snd){	PROCESSSND(SND_APU_VALVE_ON				);}
void _APU_SPINING			(CSndToken *snd){	PROCESSSND(SND_APU_SPINING				);}
void _APU_TEMP_RAISE		(CSndToken *snd){	PROCESSSND(SND_APU_TEMP_RAISE			);}
void _APU_OIL_PRESS			(CSndToken *snd){	PROCESSSND(SND_APU_OIL_PRESS			);}
void _APU_STARTED			(CSndToken *snd){	PROCESSSND(SND_APU_STARTED				);}
void _AIRSTARTER_NORMAL		(CSndToken *snd){	PROCESSSND(SND_AIRSTARTER_NORMAL		);}
void _AI25_VALVE1			(CSndToken *snd){	PROCESSSND(SND_AI25_VALVE1				);}
void _RUD1_MG				(CSndToken *snd){	PROCESSSND(SND_RUD1_MG					);}
void _RUD2_MG				(CSndToken *snd){	PROCESSSND(SND_RUD2_MG					);}
void _RUD3_MG				(CSndToken *snd){	PROCESSSND(SND_RUD3_MG					);}
void _ENG1_SPINING			(CSndToken *snd){	PROCESSSND(SND_ENG1_SPINING				);}
void _ENG2_SPINING			(CSndToken *snd){	PROCESSSND(SND_ENG2_SPINING				);}
void _ENG3_SPINING			(CSndToken *snd){	PROCESSSND(SND_ENG3_SPINING				);}	
void _ENG1_RPM_RAISE		(CSndToken *snd){	PROCESSSND(SND_ENG1_RPM_RAISE			);}
void _ENG2_RPM_RAISE		(CSndToken *snd){	PROCESSSND(SND_ENG2_RPM_RAISE			);}
void _ENG3_RPM_RAISE		(CSndToken *snd){	PROCESSSND(SND_ENG3_RPM_RAISE			);}
void _ENG1_OILPRESS_RAISE	(CSndToken *snd){	PROCESSSND(SND_ENG1_OILPRESS_RAISE		);}
void _ENG2_OILPRESS_RAISE	(CSndToken *snd){	PROCESSSND(SND_ENG2_OILPRESS_RAISE		);}
void _ENG3_OILPRESS_RAISE	(CSndToken *snd){	PROCESSSND(SND_ENG3_OILPRESS_RAISE		);}
void _ENG1_FIREING			(CSndToken *snd){	PROCESSSND(SND_ENG1_FIREING				);}
void _ENG2_FIREING			(CSndToken *snd){	PROCESSSND(SND_ENG2_FIREING				);}
void _ENG3_FIREING			(CSndToken *snd){	PROCESSSND(SND_ENG3_FIREING				);}
void _ENG1_TEMP_RAISE		(CSndToken *snd){	PROCESSSND(SND_ENG1_TEMP_RAISE			);}
void _ENG2_TEMP_RAISE		(CSndToken *snd){	PROCESSSND(SND_ENG2_TEMP_RAISE			);}
void _ENG3_TEMP_RAISE		(CSndToken *snd){	PROCESSSND(SND_ENG3_TEMP_RAISE			);}
void _ENG1_SV_CLOSE			(CSndToken *snd){	PROCESSSND(SND_ENG1_SV_CLOSE			);}
void _ENG2_SV_CLOSE			(CSndToken *snd){	PROCESSSND(SND_ENG2_SV_CLOSE			);}
void _ENG3_SV_CLOSE			(CSndToken *snd){	PROCESSSND(SND_ENG3_SV_CLOSE			);}
void _ENG1_TEMP_OIL_NORMAL	(CSndToken *snd){	PROCESSSND(SND_ENG1_TEMP_OIL_NORMAL		);}
void _ENG2_TEMP_OIL_NORMAL	(CSndToken *snd){	PROCESSSND(SND_ENG2_TEMP_OIL_NORMAL		);}
void _ENG3_TEMP_OIL_NORMAL	(CSndToken *snd){	PROCESSSND(SND_ENG3_TEMP_OIL_NORMAL		);}
void _ENG1_STARTED 			(CSndToken *snd){	PROCESSSND(SND_ENG1_STARTED				);}
void _ENG2_STARTED 			(CSndToken *snd){	PROCESSSND(SND_ENG2_STARTED				);}
void _ENG3_STARTED 			(CSndToken *snd){	PROCESSSND(SND_ENG3_STARTED				);}
void _GEN1_NORMAL  			(CSndToken *snd){	PROCESSSND(SND_GEN1_NORMAL				);}
void _ACT_ON       			(CSndToken *snd){	PROCESSSND(SND_ACT_ON					);}
void _APU_OFF      			(CSndToken *snd){	PROCESSSND(SND_APU_OFF					);}
void _HYDRO_NORMAL    		(CSndToken *snd){	PROCESSSND(SND_HYDRO_NORMAL				);}
void _STAB_NEUTRAL	   		(CSndToken *snd){	PROCESSSND(SND_STAB_NEUTRAL				);}
void _FLAPS_EXTENDING	   	(CSndToken *snd){	PROCESSSND(SND_FLAPS_EXTENDING			);}
void _REVERS_ON		   		(CSndToken *snd){	PROCESSSND(SND_REVERS_ON				);}
void _ILS_LEFT    	   		(CSndToken *snd){	PROCESSSND(SND_ILS_LEFT					);}
void _ILS_RIGHT   	   		(CSndToken *snd){	PROCESSSND(SND_ILS_RIGHT				);}
void _ILS_DOWN    	   		(CSndToken *snd){	PROCESSSND(SND_ILS_DOWN					);}
void _ILS_UP      	   		(CSndToken *snd){	PROCESSSND(SND_ILS_UP					);}
void _AUTOPILOT_OFF	   		(CSndToken *snd){	PROCESSSND(SND_AUTOPILOT_OFF			);}
void _AUTOPILOT_ON	   		(CSndToken *snd){	PROCESSSND(SND_AUTOPILOT_ON				);}
void _SPEED_50    	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_50					);}
void _SPEED_100   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_100				);}
void _SPEED_110   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_110				);}
void _SPEED_120   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_120				);}
void _SPEED_130   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_130				);}
void _SPEED_135   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_135				);}
void _SPEED_140   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_140				);}
void _SPEED_150   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_150				);}
void _SPEED_160   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_160				);}
void _SPEED_170   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_170				);}
void _SPEED_175   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_175				);}
void _SPEED_25KN   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_25KN				);}
void _SPEED_50KN   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_50KN				);}
void _SPEED_60KN   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_60KN				);}
void _SPEED_70KN   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_70KN				);}
void _SPEED_80KN   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_80KN				);}
void _SPEED_90KN   	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_90KN				);}
void _SPEED_100KN  	   		(CSndToken *snd){	PROCESSSND(SND_SPEED_100KN				);}
void _ALT_200   	   		(CSndToken *snd){	PROCESSSND(SND_ALT_200					);}
void _ALT_180   	   		(CSndToken *snd){	PROCESSSND(SND_ALT_180					);}
void _ALT_160   	   		(CSndToken *snd){	PROCESSSND(SND_ALT_160					);}
void _ALT_140   	   		(CSndToken *snd){	PROCESSSND(SND_ALT_140					);}
void _ALT_120   	   		(CSndToken *snd){	PROCESSSND(SND_ALT_120					);}
void _ALT_100   	   		(CSndToken *snd){	PROCESSSND(SND_ALT_100					);}
void _ALT_90   	   			(CSndToken *snd){	PROCESSSND(SND_ALT_90 					);}
void _ALT_80   	   			(CSndToken *snd){	PROCESSSND(SND_ALT_80 					);}
void _ALT_70   	   			(CSndToken *snd){	PROCESSSND(SND_ALT_70 					);}
void _ALT_60   	   			(CSndToken *snd){	PROCESSSND(SND_ALT_60 					);}
void _ALT_50   	   			(CSndToken *snd){	PROCESSSND(SND_ALT_50 					);}
void _ALT_40   	   			(CSndToken *snd){	PROCESSSND(SND_ALT_40 					);}
void _ALT_30   	   			(CSndToken *snd){	PROCESSSND(SND_ALT_30 					);}
void _ALT_20   	   			(CSndToken *snd){	PROCESSSND(SND_ALT_20 					);}
void _ALT_1000FT	   		(CSndToken *snd){	PROCESSSND(SND_ALT_1000FT				);}
void _ALT_500FT 	   		(CSndToken *snd){	PROCESSSND(SND_ALT_500FT 				);}
void _ALT_400FT 	   		(CSndToken *snd){	PROCESSSND(SND_ALT_400FT 				);}
void _ALT_300FT 	   		(CSndToken *snd){	PROCESSSND(SND_ALT_300FT 				);}
void _ALT_200FT 	   		(CSndToken *snd){	PROCESSSND(SND_ALT_200FT 				);}
void _ALT_100FT 	   		(CSndToken *snd){	PROCESSSND(SND_ALT_100FT 				);}
void _ALT_50FT 	   			(CSndToken *snd){	PROCESSSND(SND_ALT_50FT  				);}
void _ALT_40FT 	   			(CSndToken *snd){	PROCESSSND(SND_ALT_40FT  				);}
void _ALT_30FT 	   			(CSndToken *snd){	PROCESSSND(SND_ALT_30FT  				);}
void _ALT_10FT 	   			(CSndToken *snd){	PROCESSSND(SND_ALT_10FT  				);}

char *SoundEvents[]={		 
	"StairsClosed",			 
	"ControlSurfacesFree",	 
	"TrimmersNeutral",		 
	"2077On",				 
	"ElectricityNormal",
	"ADFOn",
	"Htail2",
	"BrakeNormal",
	"GMKNormal",
	"Flaps20",
	"RV3MDH100",
	"ILSChecked",
	"WindowClosed",
	"DecisionHeight",
	"GearDown3GreenOn",
	"LightsRetractedAndOff",
	"GearRetracting",
	"GearUp3RedOn",
	"SafeAltitude",
	"ParkingBrake",
	"PDDOn",
	"LightsExtendedAndOn",
	"FuelPumpsOn",
	"ManometersOn",
	"APUValveOn",
	"APUSpining",
	"APUTempRaise",
	"APUOilPress",
	"APUStarted",
	"AirstarterNormal",
	"AI25Valve1",
	"RUD1MG",
	"RUD2MG",
	"RUD3MG",
	"Eng1Spining",
	"Eng2Spining",
	"Eng3Spining",
	"Eng1RPMRaise",
	"Eng2RPMRaise",
	"Eng3RPMRaise",
	"Eng1OilPressureRaise",
	"Eng2OilPressureRaise",
	"Eng3OilPressureRaise",
	"Eng1Ignition",
	"Eng2Ignition",
	"Eng3Ignition",
	"Eng1TempRaise",
	"Eng2TempRaise",
	"Eng3TempRaise",
	"Eng1SVClose",
	"Eng2SVClose",
	"Eng3SVClose",
	"Eng1Normal",
	"Eng2Normal",
	"Eng3Normal",
	"Eng1Started",
	"Eng2Started",
	"Eng3Started",
	"Gen1Normal",
	"ACTOn",
	"APUOff",
	"HydraulicNormal",
	"HtailNeutral",
	"FlapsExtending",
	"ReversOn",
	"ILSLeft",
	"ILSRight",
	"ILSDown",
	"ILSUp",
	"AutopilotOff",
	"AutopilotOn",
	"Speed50",
	"Speed100",
	"Speed110",
	"Speed120",
	"Speed130",
	"Speed135",
	"Speed140",
	"Speed150",
	"Speed160",
	"Speed170",
	"Speed175",
	"Speed25kn",
	"Speed50kn",
	"Speed60kn",
	"Speed70kn",
	"Speed80kn",
	"Speed90kn",
	"Speed100kn",
	"Alt200",
	"Alt180",
	"Alt160",
	"Alt140",
	"Alt120",
	"Alt100",
	"Alt90",
	"Alt80",
	"Alt70",
	"Alt60",
	"Alt50",
	"Alt40",
	"Alt30",
	"Alt20",
	"Alt1000ft",
	"Alt500ft",
	"Alt400ft",
	"Alt300ft",
	"Alt200ft",
	"Alt100ft",
	"Alt50ft",
	"Alt40ft",
	"Alt30ft",
	"Alt10ft",
	NULL
};

CSndToken::CSndToken()
{
	m_Name="";
	m_FileName="";
	m_Volume=-10000;
	m_Loop=false;;
	m_PlayType=PLAY_TYPE_ONCE;
	m_Type=SND_MAX;
	m_Sound=NULL;
	ProcessFunc=_DUMMY;
	m_Timer=0;
	m_TimerForced=false;
}

CSndToken::CSndToken(std::string name,std::string filename,LONG volume,bool loop,PLAY_TYPE playtype,int type,sndkit::CSound *snd)
{
	m_Name=name;
	m_FileName=filename;
	m_Volume=volume;
	m_Loop=loop;
	m_PlayType=playtype;
	m_Type=type;
	m_Sound=snd;
	ProcessFunc=_DUMMY;
	SetFunc();
	m_Timer=0;
	m_TimerForced=false;
}

CSndToken::~CSndToken()
{

}

void CSndToken::Update()
{
	if(m_TimerForced) {
		m_Timer++;
		if(m_Timer>18*2) {
			m_TimerForced=false;
		}
	}

	ProcessFunc(this);
}

void CSndToken::SetFunc()
{
	switch(m_Type) {
		case SND_STAIRS_RETRACTED		:	ProcessFunc=_STAIRS_RETRACTED		; break;
		case SND_SURFACES_FREE   		:	ProcessFunc=_SURFACES_FREE   		; break;
		case SND_TRIMMERS_NEUTRAL		:	ProcessFunc=_TRIMMERS_NEUTRAL		; break;
		case SND_2077_ON         		:	ProcessFunc=_2077_ON         		; break;
		case SND_ELECTRICITY_CHECKED	:	ProcessFunc=_ELECTRICITY_CHECKED	; break;
		case SND_ARK_ON          		:	ProcessFunc=_ARK_ON          		; break;
		case SND_STAB_2          		:	ProcessFunc=_STAB_2          		; break;
		case SND_BRAKES_CHECKED  		:	ProcessFunc=_BRAKES_CHECKED  		; break;
		case SND_GMK_ON         		:	ProcessFunc=_GMK_ON         		; break;
		case SND_FLAPS20         		:	ProcessFunc=_FLAPS20         		; break;
		case SND_RV3M_DH100      		:	ProcessFunc=_RV3M_DH100      		; break;
		case SND_ILS0_CHECKED    		:	ProcessFunc=_ILS0_CHECKED    		; break;
		case SND_WINDOW_CLOSED   		:	ProcessFunc=_WINDOW_CLOSED   		; break;
		case SND_DECISION_HEIGHT		:	ProcessFunc=_DECISION_HEIGHT		; break;
		case SND_GEAR_DOWN_3_GREEN_ON	:	ProcessFunc=_GEAR_DOWN_3_GREEN_ON	; break;
		case SND_LIGHTS_RETRACTED		:	ProcessFunc=_LIGHTS_RETRACTED		; break;
		case SND_GEAR_RETRACTING		:	ProcessFunc=_GEAR_RETRACTING		; break;
		case SND_GEAR_UP_3_RED_ON		:	ProcessFunc=_GEAR_UP_3_RED_ON		; break;
		case SND_SAFE_ALTITUDE			:	ProcessFunc=_SAFE_ALTITUDE			; break;
		case SND_PARKING_BRAKE			:	ProcessFunc=_PARKING_BRAKE			; break;
		case SND_PDD_ON					:	ProcessFunc=_PDD_ON					; break;
		case SND_LIGHTS_EXTENDED		:	ProcessFunc=_LIGHTS_EXTENDED		; break;
		case SND_FUEL_PUMPS_ON			:	ProcessFunc=_FUEL_PUMPS_ON			; break;
		case SND_MANOMETERS_ON			:	ProcessFunc=_MANOMETERS_ON			; break;
		case SND_APU_VALVE_ON			:	ProcessFunc=_APU_VALVE_ON			; break;
		case SND_APU_SPINING			:	ProcessFunc=_APU_SPINING			; break;
		case SND_APU_TEMP_RAISE			:	ProcessFunc=_APU_TEMP_RAISE			; break;
		case SND_APU_OIL_PRESS			:	ProcessFunc=_APU_OIL_PRESS			; break;
		case SND_APU_STARTED			:	ProcessFunc=_APU_STARTED			; break;
		case SND_AIRSTARTER_NORMAL		:	ProcessFunc=_AIRSTARTER_NORMAL		; break;
		case SND_AI25_VALVE1			:	ProcessFunc=_AI25_VALVE1			; break;
		case SND_RUD1_MG				:	ProcessFunc=_RUD1_MG				; break;
		case SND_RUD2_MG				:	ProcessFunc=_RUD2_MG				; break;
		case SND_RUD3_MG				:	ProcessFunc=_RUD3_MG				; break;
		case SND_ENG1_SPINING			:	ProcessFunc=_ENG1_SPINING			; break;
		case SND_ENG2_SPINING			:	ProcessFunc=_ENG2_SPINING			; break;
		case SND_ENG3_SPINING			:	ProcessFunc=_ENG3_SPINING			; break;
		case SND_ENG1_RPM_RAISE			:	ProcessFunc=_ENG1_RPM_RAISE			; break;
		case SND_ENG2_RPM_RAISE			:	ProcessFunc=_ENG2_RPM_RAISE			; break;
		case SND_ENG3_RPM_RAISE			:	ProcessFunc=_ENG3_RPM_RAISE			; break;
		case SND_ENG1_OILPRESS_RAISE	:	ProcessFunc=_ENG1_OILPRESS_RAISE	; break;
		case SND_ENG2_OILPRESS_RAISE	:	ProcessFunc=_ENG2_OILPRESS_RAISE	; break;
		case SND_ENG3_OILPRESS_RAISE	:	ProcessFunc=_ENG3_OILPRESS_RAISE	; break;
		case SND_ENG1_FIREING			:	ProcessFunc=_ENG1_FIREING			; break;
		case SND_ENG2_FIREING			:	ProcessFunc=_ENG2_FIREING			; break;
		case SND_ENG3_FIREING			:	ProcessFunc=_ENG3_FIREING			; break;
		case SND_ENG1_TEMP_RAISE		:	ProcessFunc=_ENG1_TEMP_RAISE		; break;
		case SND_ENG2_TEMP_RAISE		:	ProcessFunc=_ENG2_TEMP_RAISE		; break;
		case SND_ENG3_TEMP_RAISE		:	ProcessFunc=_ENG3_TEMP_RAISE		; break;
		case SND_ENG1_SV_CLOSE			:	ProcessFunc=_ENG1_SV_CLOSE			; break;
		case SND_ENG2_SV_CLOSE			:	ProcessFunc=_ENG2_SV_CLOSE			; break;
		case SND_ENG3_SV_CLOSE			:	ProcessFunc=_ENG3_SV_CLOSE			; break;
		case SND_ENG1_TEMP_OIL_NORMAL	:	ProcessFunc=_ENG1_TEMP_OIL_NORMAL	; break;
		case SND_ENG2_TEMP_OIL_NORMAL	:	ProcessFunc=_ENG2_TEMP_OIL_NORMAL	; break;
		case SND_ENG3_TEMP_OIL_NORMAL	:	ProcessFunc=_ENG3_TEMP_OIL_NORMAL	; break;
		case SND_ENG1_STARTED 			:	ProcessFunc=_ENG1_STARTED 			; break;
		case SND_ENG2_STARTED 			:	ProcessFunc=_ENG2_STARTED 			; break;
		case SND_ENG3_STARTED 			:	ProcessFunc=_ENG3_STARTED 			; break;
		case SND_GEN1_NORMAL  			:	ProcessFunc=_GEN1_NORMAL  			; break;
		case SND_ACT_ON       			:	ProcessFunc=_ACT_ON       			; break;
		case SND_APU_OFF      			:	ProcessFunc=_APU_OFF      			; break;
		case SND_HYDRO_NORMAL    		:	ProcessFunc=_HYDRO_NORMAL    		; break;
		case SND_STAB_NEUTRAL	   		:	ProcessFunc=_STAB_NEUTRAL	   		; break;
		case SND_FLAPS_EXTENDING	   	:	ProcessFunc=_FLAPS_EXTENDING	   	; break;
		case SND_REVERS_ON		   		:	ProcessFunc=_REVERS_ON		   		; break;
		case SND_ILS_LEFT    	   		:	ProcessFunc=_ILS_LEFT    	   		; break;
		case SND_ILS_RIGHT   	   		:	ProcessFunc=_ILS_RIGHT   	   		; break;
		case SND_ILS_DOWN    	   		:	ProcessFunc=_ILS_DOWN    	   		; break;
		case SND_ILS_UP      	   		:	ProcessFunc=_ILS_UP      	   		; break;
		case SND_AUTOPILOT_OFF	   		:	ProcessFunc=_AUTOPILOT_OFF	   		; break;
		case SND_AUTOPILOT_ON	   		:	ProcessFunc=_AUTOPILOT_ON	   		; break;
		case SND_SPEED_50    	   		:	ProcessFunc=_SPEED_50    	   		; break;
		case SND_SPEED_100   	   		:	ProcessFunc=_SPEED_100   	   		; break;
		case SND_SPEED_110   	   		:	ProcessFunc=_SPEED_110   	   		; break;
		case SND_SPEED_120   	   		:	ProcessFunc=_SPEED_120   	   		; break;
		case SND_SPEED_130   	   		:	ProcessFunc=_SPEED_130   	   		; break;
		case SND_SPEED_135   	   		:	ProcessFunc=_SPEED_135   	   		; break;
		case SND_SPEED_140   	   		:	ProcessFunc=_SPEED_140   	   		; break;
		case SND_SPEED_150   	   		:	ProcessFunc=_SPEED_150   	   		; break;
		case SND_SPEED_160   	   		:	ProcessFunc=_SPEED_160   	   		; break;
		case SND_SPEED_170   	   		:	ProcessFunc=_SPEED_170   	   		; break;
		case SND_SPEED_175   	   		:	ProcessFunc=_SPEED_175   	   		; break;
		case SND_SPEED_25KN    	   		:	ProcessFunc=_SPEED_25KN    	   		; break;
		case SND_SPEED_50KN    	   		:	ProcessFunc=_SPEED_50KN    	   		; break;
		case SND_SPEED_60KN    	   		:	ProcessFunc=_SPEED_60KN    	   		; break;
		case SND_SPEED_70KN    	   		:	ProcessFunc=_SPEED_70KN    	   		; break;
		case SND_SPEED_80KN    	   		:	ProcessFunc=_SPEED_80KN    	   		; break;
		case SND_SPEED_90KN    	   		:	ProcessFunc=_SPEED_90KN    	   		; break;
		case SND_SPEED_100KN   	   		:	ProcessFunc=_SPEED_100KN   	   		; break;
		case SND_ALT_200       	   		:	ProcessFunc=_ALT_200   		   		; break;
		case SND_ALT_180       	   		:	ProcessFunc=_ALT_180   	   			; break;
		case SND_ALT_160       	   		:	ProcessFunc=_ALT_160   	   			; break;
		case SND_ALT_140       	   		:	ProcessFunc=_ALT_140	   	   		; break;
		case SND_ALT_120       	   		:	ProcessFunc=_ALT_120   		   		; break;
		case SND_ALT_100       	   		:	ProcessFunc=_ALT_100   	   			; break;
		case SND_ALT_90       	   		:	ProcessFunc=_ALT_90	    	   		; break;
		case SND_ALT_80       	   		:	ProcessFunc=_ALT_80		   	   		; break;
		case SND_ALT_70       	   		:	ProcessFunc=_ALT_70    		   		; break;
		case SND_ALT_60       	   		:	ProcessFunc=_ALT_60    	   			; break;
		case SND_ALT_50       	   		:	ProcessFunc=_ALT_50    		  		; break;
		case SND_ALT_40       	   		:	ProcessFunc=_ALT_40    	   			; break;
		case SND_ALT_30       	   		:	ProcessFunc=_ALT_30    	   			; break;
		case SND_ALT_20       	   		:	ProcessFunc=_ALT_20    	   			; break;
		case SND_ALT_1000FT    	   		:	ProcessFunc=_ALT_1000FT	   			; break;
		case SND_ALT_500FT     	   		:	ProcessFunc=_ALT_500FT 	   	   		; break;
		case SND_ALT_400FT        	   	:	ProcessFunc=_ALT_400FT 	   	   		; break;
		case SND_ALT_300FT        	   	:	ProcessFunc=_ALT_300FT 		   		; break;
		case SND_ALT_200FT        	   	:	ProcessFunc=_ALT_200FT 	   			; break;
		case SND_ALT_100FT        	   	:	ProcessFunc=_ALT_100FT 	   	   		; break;
		case SND_ALT_50FT        	   	:	ProcessFunc=_ALT_50FT 	   	    	; break;
		case SND_ALT_40FT        	   	:	ProcessFunc=_ALT_40FT 	   		   	; break;
		case SND_ALT_30FT        	   	:	ProcessFunc=_ALT_30FT 	   	   		; break;
		case SND_ALT_10FT        	   	:	ProcessFunc=_ALT_10FT 	     	   	; break;
	}																
}																	
																	
//////////////////////////////////////////////////////////////////////////

CExternalSounder::CExternalSounder(sndkit::CSoundManager *snd)
{
	m_SndMgr=snd;
	m_Enabled=false;
	m_NumberOfSounds=0;
	m_SoundsList=NULL;
}

CExternalSounder::~CExternalSounder()
{
	if(m_SoundsList) {
		delete [] m_SoundsList;
		m_SoundsList=NULL;
	}

	for(std::list<CSndToken *>::iterator ite=lst_SndTokens.begin();ite!=lst_SndTokens.end();ite++) {
		delete (*ite);
	}

	lst_SndTokens.clear();

}

void CExternalSounder::LoadAll()
{
	char SoundPath[BUFSIZ];
	char SoundConfig[BUFSIZ];

	sprintf(SoundPath,"%s%s\\",CFG_STR_GET(CFG_STR_SOUND_PATH),CFG_STR_GET(CFG_STR_SOUND_PACK));
	sprintf(SoundConfig,"%s%s",SoundPath,MY_SOUND_CONFIG_FILE.c_str());

	TCHAR buf[128000];

	DWORD sizebuf=sizeof(buf);

	DWORD ret=GetPrivateProfileSectionNames(buf,sizebuf,SoundConfig);

	if(!ret) {
		m_Enabled=false;
		return;
	}

    m_Enabled=true;

	std::list<std::string> str;
	char tmp[256];
	int j=0;

	for(DWORD i=0;i<ret;i++) {
		if(buf[i]=='\0') {
			tmp[j]=buf[i];
			if(tmp[j]!='\0')
				tmp[j]='\0';
			j=0;
			str.push_back(tmp);
		} else {
			tmp[j++]=buf[i];
		}
	}

	m_NumberOfSounds=str.size();

	if(!m_SoundsList) {
		m_SoundsList=new EXS_SOUNDS[m_NumberOfSounds+1];
	}

	CIniFile Ini(SoundConfig);

	CFG_SET(CFG_SOUND_SPEED_IN_KNOTS,Ini.ReadBool("Common","SpeedInKnots",false));
	CFG_SET(CFG_SOUND_ALT_IN_FEET,Ini.ReadBool("Common","AltInFeet",false));

	int index=0;
	for(list<string>::iterator ite=str.begin();ite!=str.end();ite++) {
		std::string name	= (*ite);
		std::string file	= Ini.ReadString			((*ite).c_str(),"File","");
		LONG volume			= (long)Ini.ReadInt			((*ite).c_str(),"Volume",-10000);
		bool loop  			= Ini.ReadBool				((*ite).c_str(),"Loop",false);
		PLAY_TYPE playtype	= (PLAY_TYPE)Ini.ReadInt	((*ite).c_str(),"PlayType",0);
		int type;

		if(file!=""&&name!="") {

			char str[MAX_PATH];
			WCHAR wbuf[MAX_PATH];
			sprintf(str,"%s%s",SoundPath,file.c_str());
			ConvertAnsiStringToWideCch(wbuf,str,MAX_PATH);

			FILE *f=fopen(str,"r");
			if(f!=NULL) {

				int numevents=sizeof(SoundEvents)/sizeof(SoundEvents[0])-1;

				for(int i=0;i<numevents;i++) {
					if(name==SoundEvents[i]) {
						type=i+SND_STAIRS_RETRACTED;
						break;
					} else
						type=SND_MAX;
				}

				m_SoundsList[index].defvol=volume;
				m_SoundsList[index].filename=file;
				m_SoundsList[index].name=name;
				m_SoundsList[index].playint=1;
				m_SoundsList[index].oldvol=volume;


				m_SndMgr->Create(&m_SoundsList[index].m_Sound, wbuf, DSBCAPS_CTRLVOLUME, GUID_NULL, 1);
				m_SoundsList[index].m_Sound->SetVolume(volume);

				lst_SndTokens.push_back(new CSndToken(name,file,volume,loop,playtype,type,m_SoundsList[index].m_Sound));

				index++;
			}
		}
	}

	m_NumberOfSounds=index;

}

void CExternalSounder::CheckMuted()
{
	int snd_master=int(POS_GET(POS_SOUND_MASTER));
	VIEW_MODE viewmode=(VIEW_MODE)int(POS_GET(POS_ACTIVE_VIEW_MODE));

	if(snd_master) {
		if(viewmode==VIEW_MODE_COCKPIT||viewmode==VIEW_MODE_VIRTUAL_COCKPIT) {
			for(int i=0;i<m_NumberOfSounds;i++) {
				m_SoundsList[i].m_Sound->RestoreVolume();
			}
		} else {
			for(int i=0;i<m_NumberOfSounds;i++) {
				m_SoundsList[i].m_Sound->Mute();
			}
		}
	} else {
		for(int i=0;i<m_NumberOfSounds;i++) {
			m_SoundsList[i].m_Sound->Mute();
		}
	}
}

void CExternalSounder::Update()
{
	if(!m_Enabled)
		return;

	CheckMuted(); 

	for(list<CSndToken *>::const_iterator ite=lst_SndTokens.begin();ite!=lst_SndTokens.end();ite++) {
		(*ite)->Update();
	}

}

void CSndToken::Play()
{
	int snd_master=int(POS_GET(POS_SOUND_MASTER));

	if(!m_Sound||POS_GET(POS_TIME_AFTER_LOAD)<400||!snd_master)
		return;

	switch(m_PlayType) {
		case PLAY_TYPE_PLAIN:
			m_Sound->Play((DWORD)m_Loop);		
		break;
		case PLAY_TYPE_STOP:
			m_Sound->Stop();		
			m_Sound->Play((DWORD)m_Loop);		
		break;
		case PLAY_TYPE_ONCE:
			m_Sound->PlayIfNotPlay((DWORD)m_Loop);		
		break;
	}
}


