/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Hnd.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

enum HND {
	// P1
	HND_GMK							,	// "sd_6015_p1_601"

	// P4
	HND_AP_HOR						,	// "sd_6015_p4_601"
	HND_AP_HOR_CORRECTED			,	// "sd_6015_p4_602"
	HND_AP_VER						,	// "sd_6015_p4_603"

	// P5
	HND_KNBVHF2RIGHT                ,	// "sd_6015_p5_601" 
	HND_KNBVHF2LEFT                 ,	// "sd_6015_p5_602"  
	HND_KNBVHF1RIGHT                ,	// "sd_6015_p5_603" 
	HND_KNBVHF1LEFT                 ,   // "sd_6015_p5_604"  

	// P6
	HND_OBS6		                ,	// "sd_6015_p6_601"
	HND_KMP1FREQ	                ,   // "sd_6015_p6_602"
	HND_XPDR1		                ,	// "sd_6015_p6_603"
	HND_XPDR2		                ,	// "sd_6015_p6_604"
	HND_XPDR3		                ,   // "sd_6015_p6_605"
	HND_XPDR4		                ,	// "sd_6015_p6_606"
	HND_KMP1FREQ2	                ,   // "sd_6015_p6_607"

	// P7
	HND_OBS7		                ,	// "sd_6015_p6_601"
	HND_KMP2FREQ	                ,   // "sd_6015_p6_602"
	HND_PRESS_CHANGE_RATE           ,   // "sd_6015_p7_605"
	HND_PRESS_DIFF                  ,	// "sd_6015_p7_606"
	HND_PRESS_BEGIN	                ,	// "sd_6015_p7_607"
	HND_KMP2FREQ2	                ,   // "sd_6015_p6_608"

	//
	HND_MAX

};