/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Pwr.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

enum PWR {
	PWR_BUS27				,	//	"sd_6015_pa_001"
	PWR_BUS27EXT			,	//  "sd_6015_pa_002"
	PWR_BUS36				,	//  "sd_6015_pa_003"
	PWR_BUS115				,	//  "sd_6015_pa_004"
	PWR_COM1				,	//	"sd_6015_pa_005"
	PWR_COM2				,	//	"sd_6015_pa_006"
	PWR_ADF1				,	//	"sd_6015_pa_007"
	PWR_ADF2				,	//	"sd_6015_pa_008"
	PWR_KURSMP1				,	//	"sd_6015_pa_009"
	PWR_KURSMP2				,	//	"sd_6015_pa_010"
	PWR_SO72				,	//	"sd_6015_pa_011"
	PWR_AP  				,	//	"sd_6015_pa_012"
	PWR_GMK 				,	//	"sd_6015_pa_013"
	PWR_APU_READY			,	//	"sd_6015_pa_014"
	PWR_LEFT_PUMP			,	//	"sd_6015_pa_015"
	PWR_RIGHT_PUMP			,	//	"sd_6015_pa_016"
	PWR_RAP_AVAIL 			,	//	"sd_6015_pa_017"
	PWR_MAX
};

