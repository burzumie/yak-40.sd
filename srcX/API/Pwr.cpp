/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Pwr.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Pwr.h"

#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_pa_001	""
#define sd_6015_pa_002	""
#define sd_6015_pa_003	""
#define sd_6015_pa_004	""
#define sd_6015_pa_005	""
#define sd_6015_pa_006	""
#define sd_6015_pa_007	""
#define sd_6015_pa_008	""
#define sd_6015_pa_009	""
#define sd_6015_pa_010	""
#define sd_6015_pa_011	""
#define sd_6015_pa_012	""
#define sd_6015_pa_013	""
#define sd_6015_pa_014	""
#define sd_6015_pa_015	""
#define sd_6015_pa_016	""
#define sd_6015_pa_017	""

#else

#define sd_6015_pa_001	""
#define sd_6015_pa_002	""
#define sd_6015_pa_003	""
#define sd_6015_pa_004	""
#define sd_6015_pa_005	""
#define sd_6015_pa_006	""
#define sd_6015_pa_007	""
#define sd_6015_pa_008	""
#define sd_6015_pa_009	""
#define sd_6015_pa_010	""
#define sd_6015_pa_011	""
#define sd_6015_pa_012	""
#define sd_6015_pa_013	""
#define sd_6015_pa_014	""
#define sd_6015_pa_015	""
#define sd_6015_pa_016	""
#define sd_6015_pa_017	""

#endif

ApiName PwrNames[PWR_MAX]={
	{"sd_6015_pa_001",sd_6015_pa_001,"",0,0,false,false},  // PWR_BUS27
	{"sd_6015_pa_002",sd_6015_pa_002,"",0,0,false,false},  // PWR_BUS27EXT
	{"sd_6015_pa_003",sd_6015_pa_003,"",0,0,false,false},  // PWR_BUS36
	{"sd_6015_pa_004",sd_6015_pa_004,"",0,0,false,false},  // PWR_BUS115
	{"sd_6015_pa_005",sd_6015_pa_005,"",0,0,false,false},  // PWR_COM1  
	{"sd_6015_pa_006",sd_6015_pa_006,"",0,0,false,false},  // PWR_COM2  
	{"sd_6015_pa_007",sd_6015_pa_007,"",0,0,false,false},  // PWR_ADF1  
	{"sd_6015_pa_008",sd_6015_pa_008,"",0,0,false,false},  // PWR_ADF2  
	{"sd_6015_pa_009",sd_6015_pa_009,"",0,0,false,false},  // PWR_KURSMP1
	{"sd_6015_pa_010",sd_6015_pa_010,"",0,0,false,false},  // PWR_KURSMP2
	{"sd_6015_pa_011",sd_6015_pa_011,"",0,0,false,false},  // PWR_SO72
	{"sd_6015_pa_012",sd_6015_pa_012,"",0,0,false,false},  // PWR_AP
	{"sd_6015_pa_013",sd_6015_pa_013,"",0,0,false,false},  // PWR_GMK
	{"sd_6015_pa_014",sd_6015_pa_014,"",0,0,false,false},  // PWR_APU_READY
	{"sd_6015_pa_015",sd_6015_pa_015,"",0,0,false,false},  // PWR_LEFT_PUMP
	{"sd_6015_pa_016",sd_6015_pa_016,"",0,0,false,false},  // PWR_RIGHT_PUMP
	{"sd_6015_pa_017",sd_6015_pa_017,"",0,0,false,false},  // PWR_RAP_AVAIL
};

