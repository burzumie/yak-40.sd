/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Ndl.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

enum NDL {
	// �����
	NDL_MAIN_SEC                	,	// "sd_6015_pa_501"
	NDL_MAIN_MIN                	,	// "sd_6015_pa_502"	
	NDL_MAIN_HRS                	,	// "sd_6015_pa_503"	
	NDL_MINI_SEC                	,	// "sd_6015_pa_504"
	NDL_MINI_SEC_HIDE           	,	// "sd_6015_pa_505"
	NDL_MINI_MIN                	,	// "sd_6015_pa_506"	
	NDL_MINI_HRS                	,	// "sd_6015_pa_507"
	NDL_ENG1_FUEL_PSI           	,	// "sd_6015_pa_508"
	NDL_ENG1_OIL_PSI            	,	// "sd_6015_pa_509" 
	NDL_ENG1_OIL_TEMP           	,	// "sd_6015_pa_510"
	NDL_ENG2_FUEL_PSI           	,	// "sd_6015_pa_511"
	NDL_ENG2_OIL_PSI            	,	// "sd_6015_pa_512" 
	NDL_ENG2_OIL_TEMP           	,	// "sd_6015_pa_513"
	NDL_ENG3_FUEL_PSI           	,	// "sd_6015_pa_514"
	NDL_ENG3_OIL_PSI            	,	// "sd_6015_pa_515" 
	NDL_ENG3_OIL_TEMP           	,	// "sd_6015_pa_516"
	NDL_ENG1_N1                 	,	// "sd_6015_pa_517"
	NDL_ENG1_N2                 	,	// "sd_6015_pa_518"
	NDL_ENG2_N1                 	,	// "sd_6015_pa_519"
	NDL_ENG2_N2                 	,	// "sd_6015_pa_520"
	NDL_ENG3_N1                 	,	// "sd_6015_pa_521"
	NDL_ENG3_N2                 	,	// "sd_6015_pa_522"
	NDL_HTAIL_DEG               	,	// "sd_6015_pa_523"
	NDL_ENG1_TEMP               	,	// "sd_6015_pa_524"
	NDL_ENG2_TEMP               	,	// "sd_6015_pa_525"
	NDL_ENG3_TEMP               	,	// "sd_6015_pa_526"
	NDL_FLAPS                   	,	// "sd_6015_pa_527"
	NDL_KUS_TAS                 	,	// "sd_6015_pa_528"
	NDL_KUS_IAS                 	,	// "sd_6015_pa_529"
	NDL_VS                      	,	// "sd_6015_pa_530"
	NDL_GMK_PRICOURSE           	,	// "sd_6015_pa_531"
	NDL_GMK_SECCOURSE           	,	// "sd_6015_pa_532"
	NDL_GMK_SCALE               	,	// "sd_6015_pa_533"
	NDL_ENG1_THROTTLE_POS       	,	// "sd_6015_pa_534"
	NDL_ENG2_THROTTLE_POS       	,	// "sd_6015_pa_535"
	NDL_ENG3_THROTTLE_POS			,	// "sd_6015_pa_536"
	NDL_KUS_TAS_EN              	,	// "sd_6015_pa_537"
	NDL_KUS_IAS_EN              	,	// "sd_6015_pa_538"
	NDL_VS_EN                   	,	// "sd_6015_pa_539"

	// P0
	NDL_AGB0_MAIN_FLAG              ,	// "sd_6015_p0_501"
	NDL_AGB0_MAIN_PLANE             ,	// "sd_6015_p0_502"
	NDL_AGB0_MAIN_BALL              ,	// "sd_6015_p0_503"
	NDL_AGB_AUXL_FLAG               ,	// "sd_6015_p0_504"
	NDL_AGB_AUXL_PLANE              ,	// "sd_6015_p0_505"
	NDL_AGB_AUXL_BALL               ,	// "sd_6015_p0_506"
	NDL_APU_TEMP                    ,	// "sd_6015_p0_507"
	NDL_AIR_TEMP                    ,	// "sd_6015_p0_508"
	NDL_DA30_TURN                   ,	// "sd_6015_p0_509"
	NDL_DA30_BALL                   ,	// "sd_6015_p0_510"
	NDL_GFORCE_MIN                  ,	// "sd_6015_p0_511"
	NDL_GFORCE_MAX                  ,	// "sd_6015_p0_512"
	NDL_GFORCE_CUR                  ,	// "sd_6015_p0_513"
	NDL_MAIN_BRAKE_HYD_LEFT         ,	// "sd_6015_p0_514"
	NDL_MAIN_BRAKE_HYD_RIGHT        ,	// "sd_6015_p0_515"
	NDL_EMERG_BRAKE_HYD_LEFT        ,	// "sd_6015_p0_516"
	NDL_EMERG_BRAKE_HYD_RIGHT       ,	// "sd_6015_p0_517"
	NDL_MAIN_HYD                    ,	// "sd_6015_p0_518"
	NDL_EMERG_HYD                   ,	// "sd_6015_p0_519"
	NDL_IKU_YELLOW                  ,	// "sd_6015_p0_520"
	NDL_IKU_WHITE                   ,	// "sd_6015_p0_521"
	NDL_KPPMS0_SCALE                ,	// "sd_6015_p0_522"
	NDL_KPPMS0_ILS_GLIDE            ,	// "sd_6015_p0_523"
	NDL_KPPMS0_ILS_COURSE           ,	// "sd_6015_p0_524"
	NDL_PPTIZ                       ,	// "sd_6015_p0_525"
	NDL_RV3M_DH                     ,	// "sd_6015_p0_526"
	NDL_RV3M_ALT                    ,	// "sd_6015_p0_527"
	NDL_STARTER                     ,	// "sd_6015_p0_528"
	NDL_UVID0                       ,	// "sd_6015_p0_529"
	NDL_UVID0_EN                    ,	// "sd_6015_p0_530"
	NDL_RV3M_DH_EN                  ,	// "sd_6015_p0_531"
	NDL_RV3M_ALT_EN                 ,	// "sd_6015_p0_532"
	NDL_UVID0_HIDE                  ,	// "sd_6015_p0_533"
	NDL_UVID0_EN_HIDE               ,	// "sd_6015_p0_534"

	// P1
	NDL_AGB1_MAIN_FLAG              ,	// "sd_6015_p1_501"
	NDL_AGB1_MAIN_PLANE             ,	// "sd_6015_p1_502"
	NDL_AGB1_MAIN_BALL              ,	// "sd_6015_p1_503"
	NDL_KPPMS1_SCALE                ,	// "sd_6015_p1_504"
	NDL_KPPMS1_ILS_GLIDE            ,	// "sd_6015_p1_505"
	NDL_KPPMS1_ILS_COURSE           ,	// "sd_6015_p1_506"
	NDL_ARK1                        ,	// "sd_6015_p1_507"
	NDL_ARK2                        ,	// "sd_6015_p1_508"
	NDL_URVK                        ,	// "sd_6015_p1_509"
	NDL_UVPD_ALT                    ,	// "sd_6015_p1_510"
	NDL_UVPD_DIFPRESS               ,	// "sd_6015_p1_511"
	NDL_TEMP_SALON                  ,	// "sd_6015_p1_512"
	NDL_DUCT                        ,	// "sd_6015_p1_513"
	NDL_VOLTS115                    ,	// "sd_6015_p1_514"
	NDL_VOLTS36                     ,	// "sd_6015_p1_515"
	NDL_VOLTS27                     ,	// "sd_6015_p1_516"
	NDL_AMPS27                      ,	// "sd_6015_p1_517"
	NDL_AMPSGEN1                    ,	// "sd_6015_p1_518"
	NDL_AMPSGEN2                    ,	// "sd_6015_p1_519"
	NDL_AMPSGEN3                    ,	// "sd_6015_p1_520"
	NDL_ALTIM_ENG                   ,	// "sd_6015_p1_521"
	NDL_ALTIM_RUS                   ,	// "sd_6015_p1_522"
	NDL_UVID1_1000                  ,	// "sd_6015_p1_523"
	NDL_UVID1_1000_EN               ,	// "sd_6015_p1_524"
	NDL_VS_SRD                      ,	// "sd_6015_p1_525"
	NDL_UVID1_100                   ,	// "sd_6015_p1_526"
	NDL_UVID1_100_EN                ,	// "sd_6015_p1_527"
	NDL_UVID1						,	// "sd_6015_p1_528"

	// P5
	NDL_ARK1SIGNAL                  ,	// "sd_6015_p5_501"
	NDL_ARK2SIGNAL                  ,	// "sd_6015_p5_502"

	// P7
	NDL_2077DIFFPRESS               ,   // "sd_6015_p7_501"
	NDL_2077PRESSBEGIN              ,   // "sd_6015_p7_502"

	//
	NDL_MAX                          	 
};
