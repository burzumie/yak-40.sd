/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Tbl.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Tbl.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_pa_701	"ASPD LOW"                        
#define sd_6015_pa_702	"EXIT OPEN"                       
#define sd_6015_pa_703	"WARN GEAR"                       
#define sd_6015_pa_704	"BANK L HIGH"                     
#define sd_6015_pa_705	"ADI FAIL"                        
#define sd_6015_pa_706	"BANK R HIGH"                     
#define sd_6015_pa_707	"MARKER"                          
#define sd_6015_pa_708	"VOR1 TO"                    
#define sd_6015_pa_709	"VOR1 FROM"                   
#define sd_6015_pa_710	"CAB ALT HIGH"                    
#define sd_6015_pa_711	"CAB AIR PRESS HIGH"              
#define sd_6015_pa_712	"INV 36V FAIL"                    
#define sd_6015_pa_713	"INV 115V FAIL"                   
#define sd_6015_pa_714	"OIL PRESS LOW"                   
#define sd_6015_pa_715	"AIR STARTER ON"                  
#define sd_6015_pa_716	"FUEL LOW L"                      
#define sd_6015_pa_717	"FUEL LOW R"                      
#define sd_6015_pa_718	"ENG1 VIB HIGH"                   
#define sd_6015_pa_719	"ENG2 VIB HIGH"                   
#define sd_6015_pa_720	"ENG3 VIB HIGH"                   
#define sd_6015_pa_721	"STALL"                           
#define sd_6015_pa_722	"OUTER MARKER"                    
#define sd_6015_pa_723	"MIDDLE MARKER"                   
#define sd_6015_pa_724	"INNER MARKER"                    
#define sd_6015_pa_725	"STARTER"                     
#define sd_6015_pa_726	"APU OIL PRESS NORM"              
#define sd_6015_pa_727	"APU FUEL VALVE OPEN"             
#define sd_6015_pa_728	"APU ON FIRE"                     
#define sd_6015_pa_729	"APU RPM NORM"                    
#define sd_6015_pa_730	"APU RPM HIGH"                    
#define sd_6015_pa_731	"HYD PUMP ENG1 FAIL"              
#define sd_6015_pa_732	"HYD PUMP ENG2 FAIL"              
#define sd_6015_pa_733	"RAD ALTIMETER FAIL"              
#define sd_6015_pa_734	"FIRE"                            
#define sd_6015_pa_735	"CABIN CREW"                      
#define sd_6015_pa_736	"ENG OVERHEAT"                    
#define sd_6015_pa_737	"ICE ENG L"                       
#define sd_6015_pa_738	"ICE ENG R"                       
#define sd_6015_pa_739	"GEN1 FAIL"                       
#define sd_6015_pa_740	"GEN2 FAIL"                       
#define sd_6015_pa_741	"GEN3 FAIL"                       
#define sd_6015_pa_742	""                                
#define sd_6015_pa_743	""                                
#define sd_6015_pa_744	""                                
#define sd_6015_pa_745	""                                
#define sd_6015_pa_746	""                                
#define sd_6015_pa_747	""                                
#define sd_6015_pa_748	"BANK L HIGH"                     
#define sd_6015_pa_749	"BANK R HIGH"                     
#define sd_6015_pa_750	"VOR1 TO"                    
#define sd_6015_pa_751	"VOR1 FROM"                   
 
#else

#define sd_6015_pa_701	""
#define sd_6015_pa_702	""
#define sd_6015_pa_703	""
#define sd_6015_pa_704	""
#define sd_6015_pa_705	""
#define sd_6015_pa_706	""
#define sd_6015_pa_707	""
#define sd_6015_pa_708	""
#define sd_6015_pa_709	""
#define sd_6015_pa_710	""
#define sd_6015_pa_711	""
#define sd_6015_pa_712	""
#define sd_6015_pa_713	""
#define sd_6015_pa_714	""
#define sd_6015_pa_715	""
#define sd_6015_pa_716	""
#define sd_6015_pa_717	""
#define sd_6015_pa_718	""
#define sd_6015_pa_719	""
#define sd_6015_pa_720	""
#define sd_6015_pa_721	""
#define sd_6015_pa_722	""
#define sd_6015_pa_723	""
#define sd_6015_pa_724	""
#define sd_6015_pa_725	""
#define sd_6015_pa_726	""
#define sd_6015_pa_727	""
#define sd_6015_pa_728	""
#define sd_6015_pa_729	""
#define sd_6015_pa_730	""
#define sd_6015_pa_731	""
#define sd_6015_pa_732	""
#define sd_6015_pa_733	""
#define sd_6015_pa_734	""
#define sd_6015_pa_735	""
#define sd_6015_pa_736	""
#define sd_6015_pa_737	""
#define sd_6015_pa_738	""
#define sd_6015_pa_739	""
#define sd_6015_pa_740	""
#define sd_6015_pa_741	""
#define sd_6015_pa_742	""
#define sd_6015_pa_743	""
#define sd_6015_pa_744	""
#define sd_6015_pa_745	""
#define sd_6015_pa_746	""
#define sd_6015_pa_747	""
#define sd_6015_pa_748	""
#define sd_6015_pa_749	""
#define sd_6015_pa_750	""                    
#define sd_6015_pa_751	""                   

#endif
 
ApiName TblNames[TBL_MAX]={
 	// �����
 	{"sd_6015_pa_701",sd_6015_pa_701,"",0,0,false,false},  // TBL_ASPD_LOW                
 	{"sd_6015_pa_702",sd_6015_pa_702,"",0,0,false,false},  // TBL_EXIT_OPEN               
 	{"sd_6015_pa_703",sd_6015_pa_703,"",0,0,false,false},  // TBL_WARN_GEAR               
 	{"sd_6015_pa_704",sd_6015_pa_704,"",0,0,false,false},  // TBL_BANK_L_HIGH             
 	{"sd_6015_pa_705",sd_6015_pa_705,"",0,0,false,false},  // TBL_ADI_FAIL                
 	{"sd_6015_pa_706",sd_6015_pa_706,"",0,0,false,false},  // TBL_BANK_R_HIGH             
 	{"sd_6015_pa_707",sd_6015_pa_707,"",0,0,false,false},  // TBL_MARKER                  
 	{"sd_6015_pa_708",sd_6015_pa_708,"",0,0,false,false},  // TBL_VOR1_TO
 	{"sd_6015_pa_709",sd_6015_pa_709,"",0,0,false,false},  // TBL_VOR1_FROM
 	{"sd_6015_pa_710",sd_6015_pa_710,"",0,0,false,false},  // TBL_CAB_ALT_HIGH            
 	{"sd_6015_pa_711",sd_6015_pa_711,"",0,0,false,false},  // TBL_CAB_AIR_PRESS_HIGH      
 	{"sd_6015_pa_712",sd_6015_pa_712,"",0,0,false,false},  // TBL_INV_36V_FAIL            
 	{"sd_6015_pa_713",sd_6015_pa_713,"",0,0,false,false},  // TBL_INV_115V_FAIL           
 	{"sd_6015_pa_714",sd_6015_pa_714,"",0,0,false,false},  // TBL_OIL_PRESS_LOW           
 	{"sd_6015_pa_715",sd_6015_pa_715,"",0,0,false,false},  // TBL_AIR_STARTER_ON          
 	{"sd_6015_pa_716",sd_6015_pa_716,"",0,0,false,false},  // TBL_FUEL_LOW_L              
 	{"sd_6015_pa_717",sd_6015_pa_717,"",0,0,false,false},  // TBL_FUEL_LOW_R              
 	{"sd_6015_pa_718",sd_6015_pa_718,"",0,0,false,false},  // TBL_ENG1_VIB_HIGH           
	{"sd_6015_pa_719",sd_6015_pa_719,"",0,0,false,false},  // TBL_ENG2_VIB_HIGH           
	{"sd_6015_pa_720",sd_6015_pa_720,"",0,0,false,false},  // TBL_ENG3_VIB_HIGH           
	{"sd_6015_pa_721",sd_6015_pa_721,"",0,0,false,false},  // TBL_STALL                   
	{"sd_6015_pa_722",sd_6015_pa_722,"",0,0,false,false},  // TBL_OUTER_MARKER            
	{"sd_6015_pa_723",sd_6015_pa_723,"",0,0,false,false},  // TBL_MIDDLE_MARKER           
	{"sd_6015_pa_724",sd_6015_pa_724,"",0,0,false,false},  // TBL_INNER_MARKER            
	{"sd_6015_pa_725",sd_6015_pa_725,"",0,0,false,false},  // TBL_APU_STARTER             
	{"sd_6015_pa_726",sd_6015_pa_726,"",0,0,false,false},  // TBL_APU_OIL_PRESS_NORM      
	{"sd_6015_pa_727",sd_6015_pa_727,"",0,0,false,false},  // TBL_APU_FUEL_VALVE_OPEN     
	{"sd_6015_pa_728",sd_6015_pa_728,"",0,0,false,false},  // TBL_APU_ON_FIRE             
	{"sd_6015_pa_729",sd_6015_pa_729,"",0,0,false,false},  // TBL_APU_RPM_NORM            
	{"sd_6015_pa_730",sd_6015_pa_730,"",0,0,false,false},  // TBL_APU_RPM_HIGH            
	{"sd_6015_pa_731",sd_6015_pa_731,"",0,0,false,false},  // TBL_HYD_PUMP_ENG1_FAIL      
	{"sd_6015_pa_732",sd_6015_pa_732,"",0,0,false,false},  // TBL_HYD_PUMP_ENG2_FAIL      
	{"sd_6015_pa_733",sd_6015_pa_733,"",0,0,false,false},  // TBL_RAD_ALTIMETER_FAIL      
	{"sd_6015_pa_734",sd_6015_pa_734,"",0,0,false,false},  // TBL_FIRE                    
	{"sd_6015_pa_735",sd_6015_pa_735,"",0,0,false,false},  // TBL_CABIN_CREW              
	{"sd_6015_pa_736",sd_6015_pa_736,"",0,0,false,false},  // TBL_ENG_OVERHEAT            
	{"sd_6015_pa_737",sd_6015_pa_737,"",0,0,false,false},  // TBL_ICE_ENG_L               
	{"sd_6015_pa_738",sd_6015_pa_738,"",0,0,false,false},  // TBL_ICE_ENG_R               
	{"sd_6015_pa_739",sd_6015_pa_739,"",0,0,false,false},  // TBL_GEN1_FAIL               
	{"sd_6015_pa_740",sd_6015_pa_740,"",0,0,false,false},  // TBL_GEN2_FAIL               
	{"sd_6015_pa_741",sd_6015_pa_741,"",0,0,false,false},  // TBL_GEN3_FAIL               
	{"sd_6015_pa_742",sd_6015_pa_742,"",0,0,false,false},  // TBL_GMK01    
	{"sd_6015_pa_743",sd_6015_pa_743,"",0,0,false,false},  // TBL_GMK02    
	{"sd_6015_pa_744",sd_6015_pa_744,"",0,0,false,false},  // TBL_GMK03    
	{"sd_6015_pa_745",sd_6015_pa_745,"",0,0,false,false},  // TBL_GMK04    
	{"sd_6015_pa_746",sd_6015_pa_746,"",0,0,false,false},  // TBL_GMK05    
	{"sd_6015_pa_747",sd_6015_pa_747,"",0,0,false,false},  // TBL_GMK06    
	{"sd_6015_pa_748",sd_6015_pa_748,"",0,0,false,false},  // TBL_BANK3_L_HIGH             
	{"sd_6015_pa_749",sd_6015_pa_749,"",0,0,false,false},  // TBL_BANK3_R_HIGH             
	{"sd_6015_pa_750",sd_6015_pa_750,"",0,0,false,false},  // TBL_VOR2_TO
	{"sd_6015_pa_751",sd_6015_pa_751,"",0,0,false,false},  // TBL_VOR2_FROM
};														  

ApiName TblNamesE[TBL_MAX]={
	// �����
	{"sd_6015_pa_701e","","",0,0,false,false},  // TBL_ASPD_LOW                
	{"sd_6015_pa_702e","","",0,0,false,false},  // TBL_EXIT_OPEN               
	{"sd_6015_pa_703e","","",0,0,false,false},  // TBL_WARN_GEAR               
	{"sd_6015_pa_704e","","",0,0,false,false},  // TBL_BANK_L_HIGH             
	{"sd_6015_pa_705e","","",0,0,false,false},  // TBL_ADI_FAIL                
	{"sd_6015_pa_706e","","",0,0,false,false},  // TBL_BANK_R_HIGH             
	{"sd_6015_pa_707e","","",0,0,false,false},  // TBL_MARKER                  
	{"sd_6015_pa_708e","","",0,0,false,false},  // TBL_VOR1_TO
	{"sd_6015_pa_709e","","",0,0,false,false},  // TBL_VOR1_FROM
	{"sd_6015_pa_710e","","",0,0,false,false},  // TBL_CAB_ALT_HIGH            
	{"sd_6015_pa_711e","","",0,0,false,false},  // TBL_CAB_AIR_PRESS_HIGH      
	{"sd_6015_pa_712e","","",0,0,false,false},  // TBL_INV_36V_FAIL            
	{"sd_6015_pa_713e","","",0,0,false,false},  // TBL_INV_115V_FAIL           
	{"sd_6015_pa_714e","","",0,0,false,false},  // TBL_OIL_PRESS_LOW           
	{"sd_6015_pa_715e","","",0,0,false,false},  // TBL_AIR_STARTER_ON          
	{"sd_6015_pa_716e","","",0,0,false,false},  // TBL_FUEL_LOW_L              
	{"sd_6015_pa_717e","","",0,0,false,false},  // TBL_FUEL_LOW_R              
	{"sd_6015_pa_718e","","",0,0,false,false},  // TBL_ENG1_VIB_HIGH           
	{"sd_6015_pa_719e","","",0,0,false,false},  // TBL_ENG2_VIB_HIGH           
	{"sd_6015_pa_720e","","",0,0,false,false},  // TBL_ENG3_VIB_HIGH           
	{"sd_6015_pa_721e","","",0,0,false,false},  // TBL_STALL                   
	{"sd_6015_pa_722e","","",0,0,false,false},  // TBL_OUTER_MARKER            
	{"sd_6015_pa_723e","","",0,0,false,false},  // TBL_MIDDLE_MARKER           
	{"sd_6015_pa_724e","","",0,0,false,false},  // TBL_INNER_MARKER            
	{"sd_6015_pa_725e","","",0,0,false,false},  // TBL_APU_STARTER             
	{"sd_6015_pa_726e","","",0,0,false,false},  // TBL_APU_OIL_PRESS_NORM      
	{"sd_6015_pa_727e","","",0,0,false,false},  // TBL_APU_FUEL_VALVE_OPEN     
	{"sd_6015_pa_728e","","",0,0,false,false},  // TBL_APU_ON_FIRE             
	{"sd_6015_pa_729e","","",0,0,false,false},  // TBL_APU_RPM_NORM            
	{"sd_6015_pa_730e","","",0,0,false,false},  // TBL_APU_RPM_HIGH            
	{"sd_6015_pa_731e","","",0,0,false,false},  // TBL_HYD_PUMP_ENG1_FAIL      
	{"sd_6015_pa_732e","","",0,0,false,false},  // TBL_HYD_PUMP_ENG2_FAIL      
	{"sd_6015_pa_733e","","",0,0,false,false},  // TBL_RAD_ALTIMETER_FAIL      
	{"sd_6015_pa_734e","","",0,0,false,false},  // TBL_FIRE                    
	{"sd_6015_pa_735e","","",0,0,false,false},  // TBL_CABIN_CREW              
	{"sd_6015_pa_736e","","",0,0,false,false},  // TBL_ENG_OVERHEAT            
	{"sd_6015_pa_737e","","",0,0,false,false},  // TBL_ICE_ENG_L               
	{"sd_6015_pa_738e","","",0,0,false,false},  // TBL_ICE_ENG_R               
	{"sd_6015_pa_739e","","",0,0,false,false},  // TBL_GEN1_FAIL               
	{"sd_6015_pa_740e","","",0,0,false,false},  // TBL_GEN2_FAIL               
	{"sd_6015_pa_741e","","",0,0,false,false},  // TBL_GEN3_FAIL               
	{"sd_6015_pa_742e","","",0,0,false,false},  // TBL_GMK01    
	{"sd_6015_pa_743e","","",0,0,false,false},  // TBL_GMK02    
	{"sd_6015_pa_744e","","",0,0,false,false},  // TBL_GMK03    
	{"sd_6015_pa_745e","","",0,0,false,false},  // TBL_GMK04    
	{"sd_6015_pa_746e","","",0,0,false,false},  // TBL_GMK05    
	{"sd_6015_pa_747e","","",0,0,false,false},  // TBL_GMK06    
	{"sd_6015_pa_748e","","",0,0,false,false},  // TBL_BANK3_L_HIGH             
	{"sd_6015_pa_749e","","",0,0,false,false},  // TBL_BANK3_R_HIGH             
	{"sd_6015_pa_750e","","",0,0,false,false},  // TBL_VOR2_TO
	{"sd_6015_pa_751e","","",0,0,false,false},  // TBL_VOR2_FROM
};														  

