#include "../Common/CommonSys.h"
#include "../Common/Macros.h"

#ifdef _DEBUG
_CrtMemState memoryState; 
#endif

#define MY_NAME	"<Name>SD6015 YAK-40</Name>"
#define MY_PATH "<Path>SuprunovDesign\\Yak-40\\Tools\\SD6015_Yak40.dll</Path>"

#define  ENV_VAR_STRING_COUNT  (sizeof(envVarStrings)/sizeof(TCHAR*))
#define INFO_BUFFER_SIZE 32767

void printError( TCHAR* msg );

typedef struct ADDON {
	std::string Header;
	std::string Disabled;
	std::string ManualLoad;
	std::string Name;
	std::string Path;
	std::string Cmd;
	std::string NewConsole;
	std::string HeaderEnd;
}ADDON,*PADDON,**PPADDON;

typedef struct CONTAINER {
	std::string Header;
	std::string DocType;
	std::string Descr;
	std::string FileName;
	std::string Disabled;
	std::string ManualLoad;
	std::string DocEnd;
	std::vector<ADDON> Addons;
}CONTAINER,*PCONTAINER,**PPCONTAINER;

CONTAINER Container;

void GetAppDir(char *buf)
{
	TCHAR  infoBuf[INFO_BUFFER_SIZE];
	DWORD  bufCharCount = INFO_BUFFER_SIZE;
	bufCharCount=ExpandEnvironmentStrings("%APPDATA%",infoBuf,INFO_BUFFER_SIZE); 
	if(bufCharCount>INFO_BUFFER_SIZE)
		printf( "\n\t(Buffer too small to expand: \"%s\")", "%APPDATA%");
	else if(!bufCharCount)
		printError("ExpandEnvironmentStrings");

	sprintf_s(buf,MAX_PATH,"%s\\Microsoft\\FSX\\DLL.xml",infoBuf);
}

void GetAppDir2(char *buf)
{
	TCHAR  infoBuf[INFO_BUFFER_SIZE];
	DWORD  bufCharCount = INFO_BUFFER_SIZE;
	bufCharCount=ExpandEnvironmentStrings("%APPDATA%",infoBuf,INFO_BUFFER_SIZE); 
	if(bufCharCount>INFO_BUFFER_SIZE)
		printf( "\n\t(Buffer too small to expand: \"%s\")", "%APPDATA%");
	else if(!bufCharCount)
		printError("ExpandEnvironmentStrings");

	sprintf_s(buf,MAX_PATH,"%s\\Microsoft\\FSX\\SimConnect.xml",infoBuf);
}

void GetAppDir3(char *buf)
{
}

void CreateNewXML(FILE *f)
{
	fprintf(f,"<?xml version=\"1.0\" encoding=\"Windows-1252\"?>\n\n");
	fprintf(f,"<SimBase.Document Type=\"Launch\" version=\"1,0\">\n");
	fprintf(f,"  <Descr>Launch</Descr>\n");
	fprintf(f,"  <Filename>dll.xml</Filename>\n");
	fprintf(f,"  <Disabled>False</Disabled>\n");
	fprintf(f,"  <Launch.ManualLoad>False</Launch.ManualLoad>\n");
	fprintf(f,"  <Launch.Addon>\n");
	fprintf(f,"     <Disabled>True</Disabled>\n");
	fprintf(f,"     <ManualLoad>False</ManualLoad>\n");
	fprintf(f,"     <Name>Object Placement Tool</Name>\n");
	fprintf(f,"     <Path>SDK\\Mission Creation Kit\\object_placement.dll</Path>\n");
	fprintf(f,"  </Launch.Addon>\n");
	fprintf(f,"  <Launch.Addon>\n");
	fprintf(f,"     <Disabled>True</Disabled>\n");
	fprintf(f,"     <ManualLoad>False</ManualLoad>\n");
	fprintf(f,"     <Name>Traffic Toolbox</Name>\n");
	fprintf(f,"     <Path>SDK\\Environment Kit\\Traffic Toolbox SDK\\traffictoolbox.dll</Path>\n");
	fprintf(f,"  </Launch.Addon>\n");
	fprintf(f,"  <Launch.Addon>\n");
	fprintf(f,"     <Disabled>True</Disabled>\n");
	fprintf(f,"     <ManualLoad>False</ManualLoad>\n");
	fprintf(f,"     <Name>Visual Effects Tool</Name>\n");
	fprintf(f,"     <Path>SDK\\Environment Kit\\Special Effects SDK\\visualfxtool.dll</Path>\n");
	fprintf(f,"  </Launch.Addon>\n");
	fprintf(f,"  <Launch.Addon>\n");
	fprintf(f,"     <Disabled>False</Disabled>\n");
	fprintf(f,"     <ManualLoad>False</ManualLoad>\n");
	fprintf(f,"     "MY_NAME"\n");
	fprintf(f,"     "MY_PATH"\n");
	fprintf(f,"     <NewConsole>False</NewConsole>\n");
	fprintf(f,"  </Launch.Addon>\n");
	fprintf(f,"</SimBase.Document>\n");
}

const char  chWhite[]  =  " \n\t\r";

void TrimRight(std::string &rString)
{
	rString = rString.substr(0, rString.find_last_not_of(chWhite) + 1);
}

void TrimLeft(std::string &rString)
{
	int nFirst = rString.find_first_not_of(chWhite);
	if (rString.npos == nFirst) 
		return;
	rString = rString.substr(nFirst);
}

void TrimBoth(std::string &rString)
{
	TrimLeft(rString);
	TrimRight(rString);
}

void ParseXML(FILE *f)
{
	char buf[BUFSIZ];
	int numread=0;
	std::string tmp;
	bool AddonFound=false;
	int CurAddon=0;
	ADDON Addon;

	while(!feof(f)) {
		if(fgets(buf,BUFSIZ,f)==NULL) {
			return;
		}

		tmp.assign(buf);
		TrimBoth(tmp);
		strcpy_s(buf,tmp.c_str());

		if(!AddonFound) {
			if(!_strnicmp(buf,"<?xml version",13)) {
				Container.Header.assign(buf);
			} else if(!_strnicmp(buf,"<SimBase.Document Type",22)) {
				Container.DocType.assign(buf);
			} else if(!_strnicmp(buf,"<Descr>",7)) {
				Container.Descr.assign(buf);
			} else if(!_strnicmp(buf,"<Filename>",10)) {
				Container.FileName.assign(buf);
			} else if(!_strnicmp(buf,"<Disabled>",10)) {
				Container.Disabled.assign(buf);
			} else if(!_strnicmp(buf,"<Launch.ManualLoad>",19)) {
				Container.ManualLoad.assign(buf);
			} else if(!_strnicmp(buf,"</SimBase.Document>",19)) {
				Container.DocEnd.assign(buf);
			} else if(!_strnicmp(buf,"<Launch.Addon>",14)) {
				Addon.Header.assign(buf);
				AddonFound=true;
			}
		} else {
			if(!_strnicmp(buf,"<Disabled>",10)) {
				Addon.Disabled.assign(buf);
			} else if(!_strnicmp(buf,"<ManualLoad>",12)) {
				Addon.ManualLoad.assign(buf);
			} else if(!_strnicmp(buf,"<Name>",6)) {
				Addon.Name.assign(buf);
			} else if(!_strnicmp(buf,"<Path>",6)) {
				Addon.Path.assign(buf);
			} else if(!_strnicmp(buf,"<CommandLine>",13)) {
				Addon.Cmd.assign(buf);
			} else if(!_strnicmp(buf,"<NewConsole>",12)) {
				Addon.NewConsole.assign(buf);
			} else if(!_strnicmp(buf,"</Launch.Addon>",15)) {
				Addon.HeaderEnd.assign(buf);
				AddonFound=false;
				Container.Addons.push_back(Addon);
			}
		}
	}
}

void SaveXML(FILE *f)
{
	fprintf(f,"%s\n\n",Container.Header.c_str());
	fprintf(f,"%s\n",Container.DocType.c_str());
	fprintf(f,"  %s\n",Container.Descr.c_str());
	fprintf(f,"  %s\n",Container.FileName.c_str());
	fprintf(f,"  %s\n",Container.Disabled.c_str());
	fprintf(f,"  %s\n",Container.ManualLoad.c_str());

	for(size_t i=0;i<Container.Addons.size();i++) {
		fprintf(f,"  %s\n",Container.Addons[i].Header.c_str());
		if(Container.Addons[i].Disabled!="")fprintf(f,"    %s\n",Container.Addons[i].Disabled.c_str());
		if(Container.Addons[i].ManualLoad!="")fprintf(f,"    %s\n",Container.Addons[i].ManualLoad.c_str());
		if(Container.Addons[i].Name!="")fprintf(f,"    %s\n",Container.Addons[i].Name.c_str());
		if(Container.Addons[i].Path!="")fprintf(f,"    %s\n",Container.Addons[i].Path.c_str());
		if(Container.Addons[i].Cmd!="")fprintf(f,"    %s\n",Container.Addons[i].Cmd.c_str());
		if(Container.Addons[i].NewConsole!="")fprintf(f,"    %s\n",Container.Addons[i].NewConsole.c_str());
		fprintf(f,"  %s\n",Container.Addons[i].HeaderEnd.c_str());
	}

	fprintf(f,"%s\n",Container.DocEnd.c_str());
}

void CheckForSimConnectXML();
void CheckForSimConnectINI();

int main(int argc, char* argv[])
{
#ifdef _DEBUG
	_CrtMemCheckpoint(&memoryState);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
#endif
	char filename[MAX_PATH];
	FILE *f;

	GetAppDir(filename);

	errno_t err=fopen_s(&f,filename,"rt");
	if(err) {
		printf("Error open file %s\nCreating new.",filename);
		err=fopen_s(&f,filename,"wt");
		if(err) {
			printf("Error creating file %s\n",filename);
			return -1;
		}
		CreateNewXML(f);
		fclose(f);
		return 1;
	}

	ParseXML(f);

	fclose(f);

	for(size_t i=0;i<Container.Addons.size();i++) {
		if(Container.Addons[i].Name==MY_NAME)
			return 2;
	}

	ADDON add;
	add.Header="<Launch.Addon>";
	add.Disabled="<Disabled>False</Disabled>";
	add.ManualLoad="<ManualLoad>False</ManualLoad>";
	add.Name=MY_NAME;
	add.Path=MY_PATH;
	add.NewConsole="<NewConsole>False</NewConsole>";
	add.HeaderEnd="</Launch.Addon>";

	Container.Addons.push_back(add);

	err=fopen_s(&f,filename,"wt");
	if(err) {
		printf("Error creating file %s\n",filename);
		return -1;
	}

	SaveXML(f);
	fclose(f);

	CheckForSimConnectXML();
	CheckForSimConnectINI();

#ifdef _DEBUG
	CHECK_MEM_LEAK("XML.log");
#endif
	return 0;
}


void printError( TCHAR* msg )
{
	DWORD eNum;
	TCHAR sysMsg[256];
	TCHAR* p;

	eNum = GetLastError( );
	FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, eNum,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		sysMsg, 256, NULL );

	// Trim the end of the line and terminate it with a null
	p = sysMsg;
	while( ( *p > 31 ) || ( *p == 9 ) )
		++p;
	do { *p-- = 0; } while( ( p >= sysMsg ) &&
		( ( *p == '.' ) || ( *p < 33 ) ) );

	// Display the message
	printf( "\n\t%s failed with error %d (%s)", msg, eNum, sysMsg );
}

void CheckForSimConnectXML()
{
	char filename[MAX_PATH];
	FILE *f;

	GetAppDir2(filename);

	errno_t err=fopen_s(&f,filename,"rt");
	if(err) {
		printf("Error open file %s\nCreating new.",filename);
		err=fopen_s(&f,filename,"wt");
		if(err) {
			printf("Error creating file %s\n",filename);
			return;
		}

		fprintf(f,"<?xml version=\"1.0\" encoding=\"Windows-1252\"?>\n\n");

		fprintf(f,"<SimBase.Document Type=\"SimConnect\" version=\"1,0\">\n");
		fprintf(f,"  <Descr>SimConnect</Descr>\n");
		fprintf(f,"  <Filename>SimConnect.xml</Filename>\n");
		fprintf(f,"  <Disabled>False</Disabled>\n");
		fprintf(f,"  <SimConnect.Comm>\n");
		fprintf(f,"    <Disabled>False</Disabled>\n");
		fprintf(f,"    <Protocol>Auto</Protocol>\n");
		fprintf(f,"    <Scope>local</Scope>\n");
		fprintf(f,"    <Address></Address>\n");
		fprintf(f,"    <MaxClients>64</MaxClients>\n");
		fprintf(f,"    <Port></Port>\n");
		fprintf(f,"    <MaxRecvSize>4096</MaxRecvSize>\n");
		fprintf(f,"    <DisableNagle>False</DisableNagle>\n");
		fprintf(f,"  </SimConnect.Comm>\n");
		fprintf(f,"</SimBase.Document>\n");

		fclose(f);
		return;
	}
}

void CheckForSimConnectINI()
{
}
