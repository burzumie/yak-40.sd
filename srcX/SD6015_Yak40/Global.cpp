/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "Global.h"

SDGlobal *g_pGlobal=NULL;

SDGlobal::SDGlobal() 
{
	m_Container			= auto_ptr<SDLogicContainer>(new SDLogicContainer());
}

SDGlobal::~SDGlobal()
{
}

void SDGlobal::Init()
{
	SDLogicBase::InitAll();
}

void SDGlobal::Update(bool first)
{
	SDLogicBase::UpdateAll();
}

void SDGlobal::Load(const char *name)
{
	CIniFile *ini=new CIniFile(name);

	SDLogicBase::LoadAll(ini);

	delete ini;
}

void SDGlobal::Save(const char *name)
{
	CIniFile *ini=new CIniFile(name);

	SDLogicBase::SaveAll(ini);

	delete ini;
}
