#include "../Common/CommonSys.h"
#include "../Common/CommonSimconnect.h"
#include "../Common/Macros.h"
#include "../api/APIEntry.h"
#include "../api/APIName.h"
#include "Global.h"

#ifdef _DEBUG
_CrtMemState memoryState; 
#endif

CSimConnect		*g_SimData=NULL;

bool CreatePipe();
void ClosePipe();

int __stdcall DLLStart(void)
{
#ifdef _DEBUG
	_CrtMemCheckpoint(&memoryState);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(194804);
#endif

	bool AlreadyRunning;

	HANDLE hMutexOneInstance=::CreateMutex(NULL,TRUE,_T("SD6015YAK40-848E6D9F-8A60-47ee-BB06-45B9D4F778F0"));

	AlreadyRunning=(GetLastError()==ERROR_ALREADY_EXISTS);

	if(hMutexOneInstance!=NULL) {
		::ReleaseMutex(hMutexOneInstance);
	}

	if(AlreadyRunning)
		return -2;

	if(!CreatePipe()) 
		return -1;

	g_SimData->Update();

	SAFE_NEW(g_pGlobal,SDGlobal());

	return 0;
}

void __stdcall DLLStop(void)
{
	SAFE_DELETE(g_pGlobal);
	ClosePipe();
#ifdef _DEBUG
	CHECK_MEM_LEAK("SC.log");
#endif
}

HANDLE hFileMappingAPI=NULL;
HANDLE hFileMappingSIM=NULL;

#define CREATE_CONTROL_WITH_TT_D(A,B,AE)   /**/ \
	for(int i=0;i<A;i++) { \
	g_pSDAPIEntrys->AE[i].Init(); \
	g_pSDAPIEntrys->AE[i].SetLimit(B[i].min,B[i].max,B[i].loop); \
	}

#define CREATE_CONTROL_WITH_TT(A,B,AE)   /**/ \
	for(int i=0;i<A;i++) { \
	g_pSDAPIEntrys->AE[i].Init(); \
	g_pSDAPIEntrys->AE[i].SetLimit(B[i].min,B[i].max,B[i].loop); \
	}

extern ApiName AzsNames[];
extern ApiName BtnNames[];
extern ApiName GltNames[];
extern ApiName HndNames[];
extern ApiName KrmNames[];
extern ApiName LmpNames[];
extern ApiName NdlNames[];
extern ApiName PosNames[];
extern ApiName TbgNames[];
extern ApiName TblNames[];
extern ApiName SndNames[];
extern ApiName KeyNames[];

#pragma warning(disable: 4244)
bool CreatePipe()
{
	DWORD sizeAPI=sizeof(SDAPIEntrys);
	DWORD sizeSIM=sizeof(CSimConnect);

	hFileMappingAPI=CreateFileMappingA(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,sizeAPI,API_SHARE_NAME);
	hFileMappingSIM=CreateFileMappingA(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,sizeSIM,SIM_SHARE_NAME);

	if(hFileMappingAPI==NULL||hFileMappingSIM==NULL) {
		return false;
	}

	g_pSDAPIEntrys=(SDAPIEntrys *)MapViewOfFile(hFileMappingAPI,FILE_MAP_READ|FILE_MAP_WRITE,0,0,sizeAPI);
	g_SimData=(CSimConnect *)MapViewOfFile(hFileMappingSIM,FILE_MAP_READ|FILE_MAP_WRITE,0,0,sizeSIM);

	if(g_SimData==NULL||g_pSDAPIEntrys==NULL) {
		return false;
	}

#ifdef _DEBUG
	CREATE_CONTROL_WITH_TT_D(AZS_MAX,AzsNames,SDAZS)
	CREATE_CONTROL_WITH_TT_D(BTN_MAX,BtnNames,SDBTN)
	CREATE_CONTROL_WITH_TT_D(GLT_MAX,GltNames,SDGLT)
	CREATE_CONTROL_WITH_TT_D(KRM_MAX,KrmNames,SDKRM)
	CREATE_CONTROL_WITH_TT_D(HND_MAX,HndNames,SDHND)
	CREATE_CONTROL_WITH_TT_D(LMP_MAX,LmpNames,SDLMP)
	CREATE_CONTROL_WITH_TT_D(NDL_MAX,NdlNames,SDNDL)
	CREATE_CONTROL_WITH_TT_D(POS_MAX,PosNames,SDPOS)
	CREATE_CONTROL_WITH_TT_D(TBG_MAX,TbgNames,SDTBG)
	CREATE_CONTROL_WITH_TT_D(TBL_MAX,TblNames,SDTBL)
#else
	CREATE_CONTROL_WITH_TT  (AZS_MAX,AzsNames,SDAZS)
	CREATE_CONTROL_WITH_TT  (BTN_MAX,BtnNames,SDBTN)
	CREATE_CONTROL_WITH_TT  (GLT_MAX,GltNames,SDGLT)
	CREATE_CONTROL_WITH_TT  (KRM_MAX,KrmNames,SDKRM)
	CREATE_CONTROL_WITH_TT  (HND_MAX,HndNames,SDHND)
	CREATE_CONTROL_WITH_TT  (LMP_MAX,LmpNames,SDLMP)
	CREATE_CONTROL_WITH_TT  (NDL_MAX,NdlNames,SDNDL)
	CREATE_CONTROL_WITH_TT  (POS_MAX,PosNames,SDPOS)
	CREATE_CONTROL_WITH_TT  (TBG_MAX,TbgNames,SDTBG)
	CREATE_CONTROL_WITH_TT  (TBL_MAX,TblNames,SDTBL)
#endif

	g_SimData->Init();

	return true;
}

#pragma warning(default: 4244)

void ClosePipe()
{
	if(g_SimData) {	
		g_SimData->DeInit();
		UnmapViewOfFile(g_SimData);
		g_SimData=NULL;
	}
	if(g_pSDAPIEntrys) {
		UnmapViewOfFile(g_pSDAPIEntrys);
		g_pSDAPIEntrys=NULL;
	}
	if(hFileMappingSIM) {
		CloseHandle(hFileMappingSIM);
		hFileMappingSIM=NULL;
	}
	if(hFileMappingAPI) {
		CloseHandle(hFileMappingAPI);
		hFileMappingAPI=NULL;
	}
}
