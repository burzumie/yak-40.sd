#include "../Common/CommonSys.h"
#include "../Common/CommonSimconnect.h"
#include "../Common/CommonAircraft.h"
#include "../api/APIEntry.h"
#include "../api/Constants.h"
#include "../lib/IniFile.h"
#include "Global.h"

static enum EVENT_SUBSCRIBE_TO_SYSTEM_EVENT_ID {
	EVENT_SIM_1SEC						,
	EVENT_SIM_4SEC						,
	EVENT_SIM_6HZ						,
	EVENT_SIM_CRASHED					,
	EVENT_SIM_CRASHRESET				,
	EVENT_FLIGHT_LOAD					,
	EVENT_FLIGHT_SAVE					,
	EVENT_SIM_FLIGHTPLAN_ACTIVATED		,
	EVENT_SIM_FLIGHTPLAN_DEACTIVATED	,
	EVENT_SIM_FRAME						,
	EVENT_SIM_PAUSE						,
	EVENT_SIM_PAUSED					,
	EVENT_SIM_PAUSEFRAME				,
	EVENT_SIM_POSITION_CHANGED			,
	EVENT_SIM_SIM						,
	EVENT_SIM_START						,
	EVENT_SIM_STOP						,
	EVENT_SIM_SOUND						,
	EVENT_SIM_UNPAUSED					,
	EVENT_SIM_VIEW						,
	EVENT_SIM_WEATHER_MODE_CHANGED		,
	EVENT_TOGGLE_AVIONICS_MASTER		,
	EVENT_TOGGLE_ALTERNATOR1			,
	EVENT_TOGGLE_ALTERNATOR2			,
	EVENT_TOGGLE_ALTERNATOR3			,
	EVENT_XPNDR_SET         			,
	EVENT_COM_RADIO_SET        			,
	EVENT_COM2_RADIO_SET       			,
	EVENT_ADF_IDENT_DISABLE				,
	EVENT_ADF_IDENT_ENABLE 				,
	EVENT_ADF2_IDENT_DISABLE			,
	EVENT_ADF2_IDENT_ENABLE				,
	EVENT_COM1_TRANSMIT_SELECT			,
	EVENT_COM2_TRANSMIT_SELECT			,
	EVENT_COM_RECEIVE_ALL_SET			,
	EVENT_ADF_COMPLETE_SET				,
	EVENT_ADF2_COMPLETE_SET				,
	EVENT_VOR1_IDENT_DISABLE			,
	EVENT_VOR1_IDENT_ENABLE 			,
	EVENT_VOR2_IDENT_DISABLE			,
	EVENT_VOR2_IDENT_ENABLE 			,
	EVENT_VOR1_SET          			,
	EVENT_VOR2_SET          			,
	EVENT_NAV1_RADIO_SET       			,
	EVENT_NAV2_RADIO_SET       			,
	EVENT_RESET_G_FORCE_INDICATOR		,
	EVENT_ENGINE_AUTO_START				,
	EVENT_ENGINE_AUTO_SHUTDOWN			,
};

extern void CALLBACK fnSimConnectDispatchExt(SIMCONNECT_RECV *pData,DWORD cbData,void *pContext)
{
	CSimConnect *pMyHandle=(CSimConnect *)pContext;

	return pMyHandle->fnSimConnectDispatch(pData,cbData);
}

HHOOK	g_hKeyHook;
bool	g_bGndServicesVisible=false;

extern LRESULT CALLBACK fnKeyboardProc(int nCode,WPARAM wParam,LPARAM lParam)
{
	if (nCode < 0)  
		return CallNextHookEx(g_hKeyHook, nCode, wParam, lParam); 

	BYTE ls=HIBYTE(GetKeyState(VK_LSHIFT));
	BYTE rs=HIBYTE(GetKeyState(VK_RSHIFT));

	if((ls||rs)&&(wParam==48&&((lParam>>30)&1))) {
		POS_TGL(POS_GROUND_STARTUP);
		return 1;
	}

	return CallNextHookEx(g_hKeyHook, nCode, wParam, lParam); 
}

CSimConnect::CSimConnect()
{
}

CSimConnect::~CSimConnect()
{
}

void CSimConnect::Prepare()
{
	SECUREBEGIN_K
	char sGaugePath[MAX_PATH]={0};
	char sSimPath[MAX_PATH]={0};
	char temp[MAX_PATH];

	HKEY hKey;
	DWORD dwBufLen;

	RegOpenKeyExA(HKEY_LOCAL_MACHINE,FS_REG_BASE,0,REG_SZ,&hKey);
	LONG ret=RegQueryValueExA(hKey,REGISTRY_SIM_PATH.c_str(),NULL,NULL,(LPBYTE)sSimPath,&dwBufLen);
	RegCloseKey(hKey);

	GetModuleFileNameA(NULL,sGaugePath,MAX_PATH);
	ExtractPath(sGaugePath);
	if(ret!=ERROR_SUCCESS) {
		strcpy(sSimPath, sGaugePath);
		//strcat(sSimPath, MY_GAUGE_PATH_PLAIN.c_str());
	} else {
		strcat(sSimPath, "\\");
	}

	sprintf(temp,"%s%s",sSimPath,MY_CONFIG_PATH.c_str());
	strcpy(m_SimPath,sSimPath);
	strcpy(m_GaugePath,sGaugePath);
	strcpy(SDConfigBase::m_ConfigPath,temp);
	strcat(SDConfigBase::m_ConfigPath,"\\");
	sprintf(temp,"%s%s",sSimPath,MY_SOUND_PATH.c_str());
	strcpy(m_SoundPath,temp);
	strcat(m_SoundPath,"\\");
	CFG_STR_SET(CFG_STR_SOUND_PATH,m_SoundPath);
	sprintf(SDConfigBase::m_ConfigFile,"%s%s",SDConfigBase::m_ConfigPath,MY_CONFIG_FILE.c_str());
	m_Config->Load();
	CFG_SET(CFG_SOUND_ENABLE,m_Config->m_CfgSound->m_bEnable);
	CFG_STR_SET(CFG_STR_SOUND_PACK,m_Config->m_CfgSound->m_sSoundPack.c_str());
	SECUREEND_K
}

void CSimConnect::Init()
{
	if(SUCCEEDED(SimConnect_Open(&hSimConnect,"SD6015 Yak40 Module",NULL,0,0,0))) {
		PRINTINFO("Module Connected to Flight Simulator!\n","");   

		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_1SEC					,"1sec");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_4SEC					,"4sec");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_6HZ						,"6Hz");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_CRASHED					,"Crashed");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_CRASHRESET				,"CrashReset");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_FLIGHT_LOAD					,"FlightLoaded");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_FLIGHT_SAVE					,"FlightSaved");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_FLIGHTPLAN_ACTIVATED	,"FlightPlanActivated");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_FLIGHTPLAN_DEACTIVATED	,"FlightPlanDeactivated");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_FRAME					,"Frame");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_PAUSE					,"Pause");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_PAUSED					,"Paused");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_PAUSEFRAME				,"PauseFrame");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_POSITION_CHANGED		,"PositionChanged");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_SIM						,"Sim");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_START					,"SimStart");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_STOP					,"SimStop");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_SOUND					,"Sound");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_UNPAUSED				,"Unpaused");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_VIEW					,"View");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_WEATHER_MODE_CHANGED	,"WeatherModeChanged");

		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_TOGGLE_AVIONICS_MASTER, "TOGGLE_AVIONICS_MASTER");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_TOGGLE_ALTERNATOR1	, "TOGGLE_ALTERNATOR1");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_TOGGLE_ALTERNATOR2	, "TOGGLE_ALTERNATOR2");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_TOGGLE_ALTERNATOR3	, "TOGGLE_ALTERNATOR3");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_XPNDR_SET         	, "XPNDR_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_COM_RADIO_SET        	, "COM_RADIO_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_COM2_RADIO_SET       	, "COM2_RADIO_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_ADF_IDENT_DISABLE		, "RADIO_ADF_IDENT_DISABLE");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_ADF_IDENT_ENABLE 		, "RADIO_ADF_IDENT_ENABLE");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_ADF2_IDENT_DISABLE	, "RADIO_ADF2_IDENT_DISABLE");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_ADF2_IDENT_ENABLE		, "RADIO_ADF2_IDENT_ENABLE");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_COM1_TRANSMIT_SELECT	, "COM1_TRANSMIT_SELECT");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_COM2_TRANSMIT_SELECT	, "COM2_TRANSMIT_SELECT");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_COM_RECEIVE_ALL_SET	, "COM_RECEIVE_ALL_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_ADF_COMPLETE_SET		, "ADF_COMPLETE_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_ADF2_COMPLETE_SET		, "ADF2_COMPLETE_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_VOR1_IDENT_DISABLE	, "RADIO_VOR1_IDENT_DISABLE");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_VOR1_IDENT_ENABLE 	, "RADIO_VOR1_IDENT_ENABLE");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_VOR2_IDENT_DISABLE	, "RADIO_VOR2_IDENT_DISABLE");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_VOR2_IDENT_ENABLE 	, "RADIO_VOR2_IDENT_ENABLE");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_VOR1_SET 				, "VOR1_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_VOR2_SET 				, "VOR2_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_NAV1_RADIO_SET 		, "NAV1_RADIO_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_NAV2_RADIO_SET 		, "NAV2_RADIO_SET");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_RESET_G_FORCE_INDICATOR,"RESET_G_FORCE_INDICATOR");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_ENGINE_AUTO_START		, "ENGINE_AUTO_START");
		SimConnect_MapClientEventToSimEvent(hSimConnect,EVENT_ENGINE_AUTO_SHUTDOWN	, "ENGINE_AUTO_SHUTDOWN");

		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"TIME OF DAY"							, "Enum"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"AIRSPEED TRUE"							, "Knots"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"AIRSPEED INDICATED"						, "Knots"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL BATTERY VOLTAGE"				, "Volts"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL BATTERY LOAD"				, "Amperes" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL TOTAL LOAD AMPS"				, "Amperes" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL MASTER BATTERY"				, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"SIM ON GROUND"							, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"AVIONICS MASTER SWITCH"					, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL GENALT BUS AMPS:1"			, "Amperes"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL GENALT BUS AMPS:2"			, "Amperes"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL GENALT BUS AMPS:3"			, "Amperes"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL GENALT BUS VOLTAGE:1"		, "Volts"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL GENALT BUS VOLTAGE:2"		, "Volts"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ELECTRICAL GENALT BUS VOLTAGE:3"		, "Volts"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"TURB ENG CORRECTED N1:1"				, "Percent"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"TURB ENG CORRECTED N1:2"				, "Percent"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"TURB ENG CORRECTED N1:3"				, "Percent"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"TURB ENG CORRECTED N2:1"				, "Percent"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"TURB ENG CORRECTED N2:2"				, "Percent"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"TURB ENG CORRECTED N2:3"				, "Percent"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"PLANE LATITUDE"							, "Degrees"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"PLANE LONGITUDE"						, "Degrees"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"PLANE HEADING DEGREES TRUE"				, "Degrees"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"PLANE HEADING DEGREES MAGNETIC"			, "Degrees"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"MAGVAR"									, "Degrees"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"DELTA HEADING RATE"						, "Radians per second"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ADF SIGNAL:1"							, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ADF SIGNAL:2"							, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ADF RADIAL:1"							, "Degrees"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"ADF RADIAL:2"							, "Degrees"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV CDI:1"								, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV CDI:2"								, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV GSI:1"								, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV GSI:2"								, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"TRANSPONDER CODE:1"						, "BCO16"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"COM ACTIVE FREQUENCY:1"					, "Frequency BCD16"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"COM ACTIVE FREQUENCY:2"					, "Frequency BCD16"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV CODES:1"							, "Flags"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV CODES:2"							, "Flags"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV DME:1"								, "Nautical miles"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV DME:2"								, "Nautical miles"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV TOFROM:1"							, "Enum"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"NAV TOFROM:2"							, "Enum"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"TURN COORDINATOR BALL"					, "Position 128"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"VERTICAL SPEED"							, "meter per second"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"MAX G FORCE"							, "Gforce"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"MIN G FORCE"							, "Gforce"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_READ,"G FORCE"								, "Gforce"	);

		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_WRITE_ELECTRICAL_BATTERY_VOLTAGE	,"ELECTRICAL BATTERY VOLTAGE"			, "Volts"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_WRITE_ELECTRICAL_BATTERY_LOAD		,"ELECTRICAL BATTERY LOAD"				, "Amperes" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_WRITE_ELECTRICAL_TOTAL_LOAD_AMPS	,"ELECTRICAL TOTAL LOAD AMPS"			, "Amperes" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_WRITE_ELECTRICAL_MASTER_BATTERY	,"ELECTRICAL MASTER BATTERY"			, "Bool"	);

	}
}

void CSimConnect::DeInit()
{
	SimConnect_Close(hSimConnect);
}

void CSimConnect::fnSimConnectDispatch(SIMCONNECT_RECV *pData,DWORD cbData)
{
	switch(pData->dwID) {
		case SIMCONNECT_RECV_ID_EVENT_FILENAME: {
			SIMCONNECT_RECV_EVENT_FILENAME *evt = (SIMCONNECT_RECV_EVENT_FILENAME*)pData;
			switch(evt->uEventID) {
				case EVENT_FLIGHT_SAVE:
					Save(evt->szFileName);
				break;
				case EVENT_FLIGHT_LOAD:
					Load(evt->szFileName);
				break;
				case EVENT_SIM_FLIGHTPLAN_ACTIVATED:
				break;
			}
		}break;

		case SIMCONNECT_RECV_ID_EVENT: {
			SIMCONNECT_RECV_EVENT *evt = (SIMCONNECT_RECV_EVENT*)pData;

			switch(evt->uEventID) {
				case EVENT_SIM_1SEC:
					Update1Sec();
				break;
				case EVENT_SIM_4SEC:
					Update4Sec();
				break;
				case EVENT_SIM_6HZ:
					Update6Hz();
				break;
				case EVENT_SIM_CRASHED:
					m_Crashed=1;
				break;
				case EVENT_SIM_CRASHRESET:
					m_CrashReset=1;
				break;
				case EVENT_SIM_FLIGHTPLAN_DEACTIVATED:
					m_FlightPlanDeActivated=1;
				break;
				case EVENT_SIM_PAUSE:
					m_SimPaused=evt->dwData;
				break;
				case EVENT_SIM_PAUSED:
					m_SimPaused=0;
				break;
				case EVENT_SIM_UNPAUSED:
					m_SimPaused=0;
				break;
				case EVENT_SIM_POSITION_CHANGED:
					m_PositionChanged=1;
				break;
				case EVENT_SIM_SIM:
					m_SimRunning=evt->dwData;
				break;
				case EVENT_SIM_START:
					SimConnect_RequestDataOnSimObject(hSimConnect,REQUEST_READ,DEFINITION_READ,SIMCONNECT_SIMOBJECT_TYPE_USER,SIMCONNECT_PERIOD_SIM_FRAME);
					StartUp();
					m_SimRunning=1;
				break;
				case EVENT_SIM_STOP:
					ShutDown();
					m_SimRunning=0;
				break;
				case EVENT_SIM_SOUND:
					m_SoundMaster=evt->dwData;
				break;
				case EVENT_SIM_VIEW:
					m_ActiveViewMode=evt->dwData;
				break;
				case EVENT_SIM_WEATHER_MODE_CHANGED:
					m_WeatherModeChanged=1;
				break;
				case EVENT_ENGINE_AUTO_START:
					AutoStart();
				break;
				case EVENT_ENGINE_AUTO_SHUTDOWN:
					AutoShutdown();
				break;
			}
		}break;

		case SIMCONNECT_RECV_ID_EVENT_FRAME: {
			SIMCONNECT_RECV_EVENT_FRAME  *evt = (SIMCONNECT_RECV_EVENT_FRAME *)pData;

			switch(evt->uEventID) {
				case EVENT_SIM_FRAME:
					m_SimFrameRate=evt->fFrameRate;
					m_SimSpeed=evt->fSimSpeed;
				break;
				case EVENT_SIM_PAUSEFRAME:
					m_SimPauseFrameRate=evt->fFrameRate;
					m_SimPauseSpeed=evt->fSimSpeed;
				break;
			}
		}break;

		case SIMCONNECT_RECV_ID_SIMOBJECT_DATA: {
			UpdateFrame();

			SIMCONNECT_RECV_SIMOBJECT_DATA *pObjData = (SIMCONNECT_RECV_SIMOBJECT_DATA*)pData;

			switch(pObjData->dwRequestID) {
				case REQUEST_READ: {
					PSIMCONNECT_PARAMS_READ tmp=(PSIMCONNECT_PARAMS_READ)&pObjData->dwData;
					memcpy(&m_SimIn,tmp,sizeof(SIMCONNECT_PARAMS_READ));
				}break;
			}
		}break;

		case SIMCONNECT_RECV_ID_QUIT:
			m_QuitSignalCatch=true;
		break;
	}
}

void CSimConnect::StartUp()
{
	g_hKeyHook = SetWindowsHookEx( WH_KEYBOARD, fnKeyboardProc, (HINSTANCE) NULL, GetCurrentThreadId()); 
	Prepare();
	if(g_pGlobal) {
		g_pGlobal->Init();
		g_pGlobal->Update(true);
	}
	POS_SET(POS_PANEL_LANG,m_Config->m_CfgCommon->m_iLanguage);
}

void CSimConnect::ShutDown()
{
	UnhookWindowsHookEx(g_hKeyHook);
}

void CSimConnect::Update()
{
	m_Config=auto_ptr<SDConfig>(new SDConfig());
	SimConnect_CallDispatch(hSimConnect,fnSimConnectDispatchExt,this);
}

void CSimConnect::UpdateFrame()
{
	POS_SET(POS_SIM_PAUSED,m_SimPaused);
	POS_SET(POS_ACTIVE_VIEW_MODE,m_ActiveViewMode);
	POS_SET(POS_SOUND_MASTER,m_SoundMaster);
	POS_SET(POS_PANEL_STATE,PANEL_LIGHT_DAY);
	if(g_pGlobal)
		g_pGlobal->Update();
}

void CSimConnect::Update1Sec()
{
	POS_SET(POS_TIME_OF_DAY,GetParam(SIMPARAM_READ_TIME_OF_DAY)==1?TIME_OF_DAY_DAY:GetParam(SIMPARAM_READ_TIME_OF_DAY)==3?TIME_OF_DAY_NIGHT:TIME_OF_DAY_DAWN);
}

void CSimConnect::Update4Sec()
{
}

void CSimConnect::Update6Hz()
{
}

#define SAVE_SECTION(T,M,S,TYP) /**/ \
	for(i=0;i<M;i++) { \
		char buf[6]; \
		for(j=0;j<VAL_MAX;j++) { \
			sprintf(buf,T"%d%d",i,j); \
			ini->Write##TYP(SIM_FLT_MY_SECTION.c_str(),buf,g_pSDAPIEntrys->S[i].m_Val[j]); \
		} \
	}

#define LOAD_SECTION(T,M,S,TYP,TYP2) /**/ \
	for(i=0;i<M;i++) { \
		TYP2 tmp; \
		char buf[6]; \
		for(j=0;j<VAL_MAX;j++) { \
			sprintf(buf,T"%d%d",i,j); \
			tmp=ini->Read##TYP(SIM_FLT_MY_SECTION.c_str(),buf,g_pSDAPIEntrys->S[i].m_Val[j]); \
			g_pSDAPIEntrys->S[i].m_Val[j]=tmp; \
		} \
	}

void CSimConnect::Save(char *fname)
{
	CIniFile *ini=new CIniFile(fname);

	int i,j;

	SAVE_SECTION("0",AZS_MAX,SDAZS,Int);
	SAVE_SECTION("1",BTN_MAX,SDBTN,Int);
	SAVE_SECTION("2",KRM_MAX,SDKRM,Int);
	SAVE_SECTION("3",GLT_MAX,SDGLT,Int);
	SAVE_SECTION("4",POS_MAX,SDPOS,Double);
	SAVE_SECTION("5",NDL_MAX,SDNDL,Double);
	SAVE_SECTION("6",HND_MAX,SDHND,Int);
	SAVE_SECTION("7",TBL_MAX,SDTBL,Int);
	SAVE_SECTION("8",TBG_MAX,SDTBG,Int);
	SAVE_SECTION("9",LMP_MAX,SDLMP,Int);
	SAVE_SECTION("a",PWR_MAX,SDPWR,Int);
	SAVE_SECTION("b",SND_MAX,SDSND,Int);

	delete ini;

	g_pGlobal->Save(fname);
}

void CSimConnect::Load(char *fname)
{
	CIniFile *ini=new CIniFile(fname);

	int i,j;

	LOAD_SECTION("0",AZS_MAX,SDAZS,Int,int);
	LOAD_SECTION("1",BTN_MAX,SDBTN,Int,int);
	LOAD_SECTION("2",KRM_MAX,SDKRM,Int,int);
	LOAD_SECTION("3",GLT_MAX,SDGLT,Int,int);
	LOAD_SECTION("4",POS_MAX,SDPOS,Double,double);
	LOAD_SECTION("5",NDL_MAX,SDNDL,Double,double);
	LOAD_SECTION("6",HND_MAX,SDHND,Int,int);
	LOAD_SECTION("7",TBL_MAX,SDTBL,Int,int);
	LOAD_SECTION("8",TBG_MAX,SDTBG,Int,int);
	LOAD_SECTION("9",LMP_MAX,SDLMP,Int,int);
	LOAD_SECTION("a",PWR_MAX,SDPWR,Int,int);
	LOAD_SECTION("b",SND_MAX,SDSND,Int,int);

	delete ini;

	g_pGlobal->Load(fname);

}

void CSimConnect::TurnAvionicsOn() 
{
	if(!GetParam(SIMPARAM_READ_AVIONICS_MASTER_SWITCH))
		SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_TOGGLE_AVIONICS_MASTER, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::TurnAvionicsOff() 
{
	if(GetParam(SIMPARAM_READ_AVIONICS_MASTER_SWITCH))
		SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_TOGGLE_AVIONICS_MASTER, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::TurnAlternatorOn(int idx)
{
	int i=0;

	if(idx==1)
		i=SIMPARAM_READ_ELECTRICAL_GENALT_BUS_AMPS1;
	else if(idx==2)
		i=SIMPARAM_READ_ELECTRICAL_GENALT_BUS_AMPS2;
	else if(idx==3)
		i=SIMPARAM_READ_ELECTRICAL_GENALT_BUS_AMPS3;

	if(!GetParam((SIMPARAM_READ)i)) {
		ToggleAlternator(idx);
	}
}

void CSimConnect::TurnAlternatorOff(int idx)
{
	int i=0;

	if(idx==1)
		i=SIMPARAM_READ_ELECTRICAL_GENALT_BUS_AMPS1;
	else if(idx==2)
		i=SIMPARAM_READ_ELECTRICAL_GENALT_BUS_AMPS2;
	else if(idx==3)
		i=SIMPARAM_READ_ELECTRICAL_GENALT_BUS_AMPS3;

	if(GetParam((SIMPARAM_READ)i)) {
		ToggleAlternator(idx);
	}
}

void CSimConnect::ToggleAlternator(int idx)
{
	int i=0;
	
	if(idx==1)
		i=EVENT_TOGGLE_ALTERNATOR1;
	else if(idx==2)
		i=EVENT_TOGGLE_ALTERNATOR2;
	else if(idx==3)
		i=EVENT_TOGGLE_ALTERNATOR3;

	SimConnect_TransmitClientEvent(hSimConnect, 0, i, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::SetXPNDR(UINT32 val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_XPNDR_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::SetCOM1(UINT32 val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_COM_RADIO_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::SetCOM2(UINT32 val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_COM2_RADIO_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::SetADF1(UINT32 val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_ADF_COMPLETE_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::SetADF2(UINT32 val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_ADF2_COMPLETE_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::ADF1IdentEnable()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_ADF_IDENT_ENABLE, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::ADF1IdentDisable()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_ADF_IDENT_DISABLE, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::ADF2IdentEnable()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_ADF2_IDENT_ENABLE, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::ADF2IdentDisable()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_ADF2_IDENT_DISABLE, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::COM1TransmitSel()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_COM1_TRANSMIT_SELECT, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::COM2TransmitSel()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_COM2_TRANSMIT_SELECT, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::COMReceiveAllSet(int val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_COM_RECEIVE_ALL_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::VOR1IdentEnable()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_VOR1_IDENT_ENABLE, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::VOR1IdentDisable()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_VOR1_IDENT_DISABLE, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::VOR2IdentEnable()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_VOR2_IDENT_ENABLE, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::VOR2IdentDisable()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_VOR2_IDENT_DISABLE, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::SetVOR1(UINT32 val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_VOR1_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::SetVOR2(UINT32 val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_VOR2_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::SetNAV1(UINT32 val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_NAV1_RADIO_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::SetNAV2(UINT32 val)
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_NAV2_RADIO_SET, val, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::ResetGForce()
{
	SimConnect_TransmitClientEvent(hSimConnect, 0, EVENT_RESET_G_FORCE_INDICATOR, 0, SIMCONNECT_GROUP_PRIORITY_DEFAULT, SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY);
}

void CSimConnect::AutoStart()
{
	for(int i=AZS_COM1_BUS;i<AZS_EXT_PWR;i++)
		AZS_SET(i,1);

	AZS_SET(AZS_POWERSOURCE,POWER_SOURCE_BAT);
	AZS_SET(AZS_INV1_36V,1);
	AZS_SET(AZS_INV2_36V,1);
	AZS_SET(AZS_INV1_115V,1);
	AZS_SET(AZS_INV2_115V,1);
	AZS_SET(AZS_GENERATOR1,1);
	AZS_SET(AZS_GENERATOR2,1);
	AZS_SET(AZS_GENERATOR3,1);
	AZS_SET(AZS_EMERG_ENG1_CUT,0);
	AZS_SET(AZS_EMERG_ENG2_CUT,0);
	AZS_SET(AZS_EMERG_ENG3_CUT,0);
	AZS_SET(AZS_TOPL_LEV,1);
	AZS_SET(AZS_TOPL_PRAV,1);
	AZS_SET(AZS_PKAI251,3);
	AZS_SET(AZS_PKAI252,3);
	AZS_SET(AZS_PKAI253,3);
	AZS_SET(AZS_MANOM,1);
	AZS_SET(AZS_RADALT,1);
	GLT_SET(GLT_ARK1,1);
	GLT_SET(GLT_ARK2,1);
	AZS_SET(AZS_KMP1PWR,1);
	AZS_SET(AZS_KMP2PWR,1);
	AZS_SET(AZS_SO72PWR,1);
}

void CSimConnect::AutoShutdown()
{
	for(int i=AZS_COM1_BUS;i<AZS_EXT_PWR;i++)
		AZS_SET(i,0);

	AZS_SET(AZS_POWERSOURCE,POWER_SOURCE_OFF);
	AZS_SET(AZS_INV1_36V,0);
	AZS_SET(AZS_INV2_36V,0);
	AZS_SET(AZS_INV1_115V,0);
	AZS_SET(AZS_INV2_115V,0);
	AZS_SET(AZS_GENERATOR1,0);
	AZS_SET(AZS_GENERATOR2,0);
	AZS_SET(AZS_GENERATOR3,0);
	AZS_SET(AZS_EMERG_ENG1_CUT,0);
	AZS_SET(AZS_EMERG_ENG2_CUT,0);
	AZS_SET(AZS_EMERG_ENG3_CUT,0);
	AZS_SET(AZS_TOPL_LEV,0);
	AZS_SET(AZS_TOPL_PRAV,0);
	AZS_SET(AZS_PKAI251,0);
	AZS_SET(AZS_PKAI252,0);
	AZS_SET(AZS_PKAI253,0);
	AZS_SET(AZS_MANOM,0);
	AZS_SET(AZS_RADALT,0);
	GLT_SET(GLT_ARK1,0);
	GLT_SET(GLT_ARK2,0);
	AZS_SET(AZS_KMP1PWR,0);
	AZS_SET(AZS_KMP2PWR,0);
	AZS_SET(AZS_SO72PWR,0);
}
