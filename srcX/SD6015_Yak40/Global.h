/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../Lib/simtools.h"
#include "Container.h"
#include "LogicBase.h"

class SDGlobal 
{
public:
	SDGlobal();
	~SDGlobal();

	void Init();
	void Update(bool first=false);
	void Load(const char *name);
	void Save(const char *name);

private:
	auto_ptr<SDLogicContainer>	m_Container;
};

extern SDGlobal *g_pGlobal;

