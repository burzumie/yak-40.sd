/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/vs.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../LogicBase.h"

class SDVSGauge : public SDLogicBase
{
public:
	SDVSGauge(){};
	virtual ~SDVSGauge(){};

	virtual void Init(){};
	virtual void Update() {
		NDL_SET(NDL_VS,g_SimData->GetParam(SIMPARAM_READ_VERTICAL_SPEED));
		NDL_SET(NDL_VS_EN,METER_TO_FEET(NDL_GET(NDL_VS)));
	};
	virtual void Load(CIniFile *ini){};
	virtual void Save(CIniFile *ini){};

};
