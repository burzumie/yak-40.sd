/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/da30.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../LogicBase.h"

class SDDA30Gauge : public SDLogicBase
{
public:
	SDDA30Gauge(){};
	virtual ~SDDA30Gauge(){};

	virtual void Init(){};
	virtual void Update() {
		NDL_SET(NDL_DA30_BALL,g_SimData->GetParam(SIMPARAM_READ_TURN_COORDINATOR_BALL)*-1);
		if(PWR_GET(PWR_BUS36)&&AZS_GET(AZS_CAPT_VSI_BUS)) {
			NDL_SET(NDL_DA30_TURN,LimitValue<double>(rddg(g_SimData->GetParam(SIMPARAM_READ_DELTA_HEADING_RATE)),-10,10));
		} else {
			NDL_SET(NDL_DA30_TURN,0);
		}
	};
	virtual void Load(CIniFile *ini){};
	virtual void Save(CIniFile *ini){};

};
