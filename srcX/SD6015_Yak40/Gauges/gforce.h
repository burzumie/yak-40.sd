/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/gforce.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../LogicBase.h"

class SDGforceGauge : public SDLogicBase
{
private:
	bool m_SetByReset;
	double m_Min,m_Max,m_Cur;
	bool m_Powered;

	void Reset() {
		if(!m_Powered)
			return;
		m_Min=1;
		m_Max=1;
		m_Cur=1;
		m_SetByReset=true;
		g_SimData->ResetGForce();
	}

public:
	SDGforceGauge(){};
	virtual ~SDGforceGauge(){};

	virtual void Init() {
		m_Min=0;
		m_Max=0;
		m_Cur=0;
		m_SetByReset=false;
		m_Powered=false;
	};

	virtual void Update() {
		if(BTN_GET(BTN_GFORCE_RESET))
			Reset();
		if(PWR_GET(PWR_BUS27)&&PWR_GET(PWR_BUS115)&&AZS_GET(AZS_GROUND_PWR_BUS)) {
			m_Min=g_SimData->GetParam(SIMPARAM_READ_MIN_G_FORCE);
			m_Max=g_SimData->GetParam(SIMPARAM_READ_MAX_G_FORCE);
			m_Cur=g_SimData->GetParam(SIMPARAM_READ_G_FORCE);
			m_SetByReset=false;
			m_Powered=true;
		} else {
			if(!m_SetByReset)m_Cur=0;
			m_Powered=false;
		}
		NDL_SET(NDL_GFORCE_CUR,m_Cur);
		NDL_SET(NDL_GFORCE_MIN,m_Min);
		NDL_SET(NDL_GFORCE_MAX,m_Max);
	};

	virtual void Load(CIniFile *ini){};
	virtual void Save(CIniFile *ini){};

};
