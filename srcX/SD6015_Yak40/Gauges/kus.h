/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/kus.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../LogicBase.h"

class SDKUSGauge : public SDLogicBase
{
public:
	SDKUSGauge(){};
	virtual ~SDKUSGauge(){};

	virtual void Init(){};

	virtual void Update() {
		double a=g_SimData->GetParam(SIMPARAM_READ_AIRSPEED_TRUE);
		double b=g_SimData->GetParam(SIMPARAM_READ_AIRSPEED_INDICATED);
		NDL_SET(NDL_KUS_TAS,a*1.852);
		NDL_SET(NDL_KUS_IAS,b*1.852);
		NDL_SET(NDL_KUS_TAS_EN,a);
		NDL_SET(NDL_KUS_IAS_EN,b);
	};

	virtual void Load(CIniFile *ini){};
	virtual void Save(CIniFile *ini){};
};
