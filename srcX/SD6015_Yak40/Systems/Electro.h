/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/MiscSystem.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../LogicBase.h"

#define RAP_CONNECT_TIME		15*TICKS_PER_SEC

#define BUS27_NAME				"27"
#define BUS27_VOLTS_DEFAULT		27
#define BUS27_AMPS_DEFAULT		28.5

#define BUS36_NAME				"36"
#define BUS36_VOLTS_DEFAULT		36
#define BUS36_AMPS_DEFAULT		0   

#define BUS115_NAME				"115"
#define BUS115_VOLTS_DEFAULT	115
#define BUS115_AMPS_DEFAULT		0

#define BUS115_LOAD_ON_BUS27	3.0
#define BUS36_LOAD_ON_BUS27		2.0

enum POWER_SOURCE
{
	POWER_SOURCE_OFF,
	POWER_SOURCE_BAT,
	POWER_SOURCE_RAP
};

enum BATTERY_NUM {
	BATTERY_NUM_FIRST_AVAILABLE	,
	BATTERY_NUM_1				,
	BATTERY_NUM_2				,
	BATTERY_NUM_NONE			,
};

enum GALETA_27 {
	GALETA_27_GEN1				,
	GALETA_27_GEN2				,
	GALETA_27_GEN3				,
	GALETA_27_RAP				,
	GALETA_27_BATR				,
	GALETA_27_LINE				,
	GALETA_27_BATL				,
};

enum GALETA_36 {
	GALETA_36_INV2_1			,
	GALETA_36_INV2_2			,
	GALETA_36_INV2_3			,
	GALETA_36_AGB_AUXL_1		,
	GALETA_36_AGB_AUXL_2		,
	GALETA_36_AGB_AUXL_3		,
	GALETA_36_INV1_1			,
	GALETA_36_INV1_2			,
	GALETA_36_INV1_3			,
	GALETA_36_DA30_1			,
	GALETA_36_DA30_2			,
	GALETA_36_DA30_3			,
};

enum GALETA_115 {
	GALETA_115_INV2				,
	GALETA_115_INV1				,
	GALETA_115_RAP				,
};

//////////////////////////////////////////////////////////////////////////

class SDElectricalUnit
{
protected:
	double m_Volts;
	double m_Amps;

public:
	SDElectricalUnit()							: m_Volts(0),	m_Amps(0)	{};
	SDElectricalUnit(double vlt, double amp)	: m_Volts(vlt), m_Amps(amp) {};
	SDElectricalUnit(const SDElectricalUnit &elUnit) {m_Volts=elUnit.GetVolts(); m_Amps=elUnit.GetAmps();};

	inline double GetVolts()	const { return m_Volts; };
	inline double GetAmps()		const { return m_Amps;	};
};

//////////////////////////////////////////////////////////////////////////

class SDGroundUnit : public SDElectricalUnit
{
private:
	bool	m_Connector;
	bool	m_RequestConnect;
	bool	m_RequestDisconnect;
	int		m_Timer;
	bool	m_TimerReseted;
	double	m_Vltd,m_Ampd;

public:
	SDGroundUnit() {
		m_RequestConnect	= false; 
		m_RequestDisconnect = false;  
		m_Connector			= false;  
		m_TimerReseted		= false; 
	}

	void Connect(double vlt, double amp) {
		m_RequestDisconnect = false; 
		m_RequestConnect	= true; 
		m_Timer				= 0;
		m_TimerReseted		= true; 
		m_Vltd				= vlt;
		m_Ampd				= amp;
	}

	void Disconnect() {
		m_RequestConnect	= false; 
		m_RequestDisconnect = true; 
		m_Timer				= 0;
		m_TimerReseted		= true; 
	}

	inline bool IsConnected() const {return m_Connector;}

	virtual void Update() {
		m_Timer++;
		if(m_RequestConnect && !m_Connector && m_TimerReseted ) {
			if(m_Timer>RAP_CONNECT_TIME) {
				m_RequestConnect	= false;
				m_TimerReseted		= false;
				m_Connector			= true;
				m_Volts				= m_Vltd;
				m_Amps				= m_Ampd;
			}
		}

		if(m_RequestDisconnect && m_Connector && m_TimerReseted) {
			if(m_Timer>RAP_CONNECT_TIME) {
				m_RequestDisconnect = false;
				m_TimerReseted		= false;
				m_Connector			= false;
				m_Volts				= 0;
				m_Amps				= 0;
			}
		}
	}

	virtual void Load(CIniFile *ini,std::string name) {
		std::string buf;
		buf="ELGUCN"+name;
		m_Connector			= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="ELGURC"+name;
		m_RequestConnect	= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="ELGURD"+name;
		m_RequestDisconnect	= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="ELGUTM"+name;
		m_Timer				= ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		buf="ELGUTMRST"+name;
		m_TimerReseted		= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="ELGUV"+name;
		m_Vltd				= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		buf="ELGUA"+name;
		m_Ampd				= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
	};

	virtual void Save(CIniFile *ini, std::string name) {
		std::string buf;
		buf="ELGUCN"+name;
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Connector		);
		buf="ELGURC"+name;
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_RequestConnect	);
		buf="ELGURD"+name;
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_RequestDisconnect);
		buf="ELGUTM"+name;
		ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Timer			);
		buf="ELGUTMRST"+name;
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_TimerReseted	);
		buf="ELGUV"+name;
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Vltd			);
		buf="ELGUA"+name;
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Ampd			);
	};
};

//////////////////////////////////////////////////////////////////////////

class SDGroundSupply
{
private:
	SDGroundUnit m_Shrap27;
	SDGroundUnit m_Shrap115;

public:
	enum SHRAP {
		SHRAP_27V	,
		SHRAP_115V	,
		SHRAP_MAX
	};

	void RequestConnect(SHRAP shrap) {
		switch(shrap) {
case SHRAP_27V:
	m_Shrap27.Connect(BUS27_VOLTS_DEFAULT, BUS27_AMPS_DEFAULT);
	break;
case SHRAP_115V:
	m_Shrap115.Connect(BUS115_VOLTS_DEFAULT, BUS115_AMPS_DEFAULT);
	break;
		}
	}

	void RequestDisconnect(SHRAP shrap) {
		switch(shrap) {
case SHRAP_27V:
	m_Shrap27.Disconnect();
	break;
case SHRAP_115V:
	m_Shrap115.Disconnect();
	break;
		}
	}

	bool RequestStatus(SHRAP shrap)	{
		bool ret=false;
		switch(shrap) {
case SHRAP_27V:
	ret=m_Shrap27.IsConnected();
	break;
case SHRAP_115V:
	ret=m_Shrap115.IsConnected();
	break;
		}
		return ret;
	}

	double RequestVolts(SHRAP shrap) {
		double ret=0;
		switch(shrap) {
case SHRAP_27V:
	ret=AZS_GET(AZS_GROUND_PWR_BUS)?m_Shrap27.GetVolts():0;
	break;
case SHRAP_115V:
	ret=AZS_GET(AZS_GROUND_PWR_BUS)?m_Shrap115.GetVolts():0;
	break;
		}
		return ret;
	}

	double RequestAmps(SHRAP shrap) {
		double ret=0;
		switch(shrap) {
case SHRAP_27V:
	ret=AZS_GET(AZS_GROUND_PWR_BUS)?m_Shrap27.GetAmps():0;
	break;
case SHRAP_115V:
	ret=AZS_GET(AZS_GROUND_PWR_BUS)?m_Shrap115.GetAmps():0;
	break;
		}
		return ret;
	}

	virtual void Update() {
		m_Shrap27.Update();
		m_Shrap115.Update();
	}

	static SDGroundSupply *Instance() {
		SAFE_NEW(m_Self,SDGroundSupply());
		return m_Self;
	};

	static void Release() {
		SAFE_DELETE(m_Self);
	};

	virtual void Load(CIniFile *ini) {
		m_Shrap27.Load (ini,BUS27_NAME	);
		m_Shrap115.Load(ini,BUS115_NAME	);
	};

	virtual void Save(CIniFile *ini) {
		m_Shrap27.Save (ini,BUS27_NAME	);
		m_Shrap115.Save(ini,BUS115_NAME	);
	};

protected:
	static SDGroundSupply *m_Self;

	SDGroundSupply(){};

};

//////////////////////////////////////////////////////////////////////////

class SDBatteryUnit : public SDElectricalUnit
{
protected:
	bool m_Available;

public:
	SDBatteryUnit() {
		m_Volts		= g_SimData->GetParam(SIMPARAM_READ_ELECTRICAL_BATTERY_VOLTAGE);
		m_Amps		= BUS27_AMPS_DEFAULT; //g_SimData->GetParamFromSimObject(REQUEST_ELECTRICAL_BATTERY_LOAD);
		m_Available	= true;
	}

	virtual void Update() {
		m_Volts		= g_SimData->GetParam(SIMPARAM_READ_ELECTRICAL_BATTERY_VOLTAGE);
		//m_Amps		= g_SimData->GetParamFromSimObject(REQUEST_ELECTRICAL_BATTERY_LOAD);
	}

	virtual bool IsAvailable() const {return m_Available;}

	virtual double RequestVolts() const {
		return m_Volts;
	}
	virtual double RequestAmps() const {
		return m_Amps;
	}

};

//////////////////////////////////////////////////////////////////////////

class SDBatterys
{
private:
	SDBatteryUnit m_Bat1;
	SDBatteryUnit m_Bat2;

public:
	static SDBatterys *Instance() {
		SAFE_NEW(m_Self,SDBatterys());
		return m_Self;
	};

	static void Release() {
		SAFE_DELETE(m_Self);
	};

	virtual void Update() {
		m_Bat1.Update();
		m_Bat2.Update();
	};

	virtual bool IsAvailable() const {return m_Bat1.IsAvailable()||m_Bat2.IsAvailable();}

	double RequestVolts(int batnum=BATTERY_NUM_FIRST_AVAILABLE) {
		double ret=0;
		switch(batnum) {
case BATTERY_NUM_FIRST_AVAILABLE:
	if(m_Bat1.IsAvailable()) 
		ret=m_Bat1.RequestVolts();
	else if(m_Bat2.IsAvailable())
		ret=m_Bat2.RequestVolts();
	break;
case BATTERY_NUM_1:
	ret=m_Bat1.RequestVolts();
	break;
case BATTERY_NUM_2:
	ret=m_Bat2.RequestVolts();
	break;
		}
		return ret;
	};

	double RequestAmps(int batnum=BATTERY_NUM_FIRST_AVAILABLE) {
		double ret=0;
		switch(batnum) {
case BATTERY_NUM_FIRST_AVAILABLE:
	if(m_Bat1.IsAvailable()) 
		ret=m_Bat1.RequestAmps();
	else if(m_Bat2.IsAvailable())
		ret=m_Bat2.RequestAmps();
	break;
case BATTERY_NUM_1:
	ret=m_Bat1.RequestAmps();
	break;
case BATTERY_NUM_2:
	ret=m_Bat2.RequestAmps();
	break;
		}
		return ret;
	};

protected:
	static SDBatterys *m_Self;

	SDBatterys(){};
};

//////////////////////////////////////////////////////////////////////////

class SDBus27 : public SDElectricalUnit
{
private:
	bool			m_Available;
	SDGroundSupply*	m_GroundSupply;
	SDBatterys*		m_Batterys;
	double			m_TotalLoad;

public:
	SDBus27() {
		m_GroundSupply	= SDGroundSupply::Instance();
		m_Batterys		= SDBatterys::Instance();
		m_TotalLoad		= 0;
	};

	virtual ~SDBus27() {
	};

	void CalculateLoad(double load115,double load36);

	virtual void Update();

	inline bool IsAvailable()		const { return m_Available;	};

	inline double GetRapVolts27()	const { return m_GroundSupply->RequestVolts(SDGroundSupply::SHRAP_27V);		};
	inline double GetRapVolts115()	const { return m_GroundSupply->RequestVolts(SDGroundSupply::SHRAP_115V);	};

	inline double GetBatVolts27(int batnum=BATTERY_NUM_FIRST_AVAILABLE) const { 
		return m_Batterys->RequestVolts(batnum); 
	};

	inline double GetRapAmps27()	const { return m_GroundSupply->RequestAmps(SDGroundSupply::SHRAP_27V);		};
	inline double GetRapAmps115()	const { return m_GroundSupply->RequestAmps(SDGroundSupply::SHRAP_115V);		};

	inline double GetBatAmps27(int batnum=BATTERY_NUM_FIRST_AVAILABLE) const { 
		double ret=0;

		if(batnum==BATTERY_NUM_FIRST_AVAILABLE)
			ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad; 

		if(batnum==BATTERY_NUM_1) {
			if(m_Batterys->RequestVolts(BATTERY_NUM_2)<1) 
				ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad; 
			else 
				ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad/2; 
		} else if(batnum==BATTERY_NUM_2) {
			if(m_Batterys->RequestVolts(BATTERY_NUM_1)<1) 
				ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad; 
			else 
				ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad/2; 
		}

		return ret;
	};

	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

	void SetTotalLoadAmps(double val);
};

//////////////////////////////////////////////////////////////////////////

class SDBus115 : public SDElectricalUnit
{
private:
	bool			m_Available;
	SDGroundSupply*	m_GroundSupply;
	SDBus27*		m_Bus27;
	double			m_TotalLoad;

public:
	SDBus115(SDBus27 *b27) {
		m_GroundSupply	= SDGroundSupply::Instance();
		m_Available		= false;
		m_Volts			= BUS115_VOLTS_DEFAULT;
		m_Amps			= BUS115_AMPS_DEFAULT;
		m_Bus27			= b27;
		m_TotalLoad		= 0;
	};

	virtual ~SDBus115() {
	};

	virtual void Update() {

		// Input
		int p1_inv1_115v       = (int)AZS_GET(AZS_INV1_115V);
		int p1_inv2_115v       = (int)AZS_GET(AZS_INV2_115V); 
		int p8_inv1_115v_bus   = (int)AZS_GET(AZS_INV1_115V_BUS);
		int p8_inv2_115v_bus   = (int)AZS_GET(AZS_INV2_115V_BUS);

		// Output
		int pa_tbl_inv_115v_fail=0;
		int pa_lmp_inv_115v_fail=0;

		// Code
		bool bus115;
		if((((p1_inv1_115v&&p8_inv1_115v_bus)||(p1_inv2_115v&&p8_inv2_115v_bus))&&m_Bus27->IsAvailable())||m_GroundSupply->RequestStatus(SDGroundSupply::SHRAP_115V)) {
			bus115=true;
			m_Volts=BUS115_VOLTS_DEFAULT;
		} else {
			bus115=false;
			m_Volts=0;
		}

		if(((p1_inv1_115v&&p8_inv1_115v_bus)&&(p1_inv2_115v&&p8_inv2_115v_bus))&&m_Bus27->IsAvailable()) {
			m_TotalLoad=BUS115_LOAD_ON_BUS27;
			pa_tbl_inv_115v_fail = 0;
		} else if(((p1_inv1_115v&&p8_inv1_115v_bus)&&(!p1_inv2_115v||!p8_inv2_115v_bus))&&m_Bus27->IsAvailable()) {
			m_TotalLoad=BUS115_LOAD_ON_BUS27/2;
			pa_tbl_inv_115v_fail = 1;
		} else if(((!p1_inv1_115v||!p8_inv1_115v_bus)&&(p1_inv2_115v&&p8_inv2_115v_bus))&&m_Bus27->IsAvailable()) {
			m_TotalLoad=BUS115_LOAD_ON_BUS27/2;
			pa_tbl_inv_115v_fail = 1;
		} else {
			m_TotalLoad=0;
			pa_tbl_inv_115v_fail = 1;
		}

		pa_lmp_inv_115v_fail = pa_tbl_inv_115v_fail;

		if((m_GroundSupply->RequestStatus(SDGroundSupply::SHRAP_115V)&&AZS_GET(AZS_GROUND_PWR_BUS))||bus115) {
			m_Available=true;
		} else {
			m_Available=false;
		}

		TBL_SET(TBL_INV_115V_FAIL,pa_tbl_inv_115v_fail);
		LMP_SET(LMP_INV_115V_FAIL,pa_lmp_inv_115v_fail);

	};

	inline bool IsAvailable() const { return m_Available;	};

	double GetVolts115(int num) {
		// Input
		int p1_inv1_115v       = (int)AZS_GET(AZS_INV1_115V);
		int p1_inv2_115v       = (int)AZS_GET(AZS_INV2_115V); 
		int p8_inv1_115v_bus   = (int)AZS_GET(AZS_INV1_115V_BUS);
		int p8_inv2_115v_bus   = (int)AZS_GET(AZS_INV2_115V_BUS);
		int p1_powersource     = (int)AZS_GET(AZS_POWERSOURCE);
		double ret=0;

		// Code
		switch(num) {
case GALETA_115_INV1:
	if(p1_inv1_115v&&p8_inv1_115v_bus)
		ret=m_Volts;
	else
		ret=0;
	break;
case GALETA_115_INV2:
	if(p1_inv2_115v&&p8_inv2_115v_bus)
		ret=m_Volts;
	else
		ret=0;
	break;
case GALETA_115_RAP:
	if(m_GroundSupply->RequestStatus(SDGroundSupply::SHRAP_115V)) 
		ret=m_Volts;
	else 
		ret=0;
	break;
		}
		return ret;
	}

	double GetTotalLoad() const {return m_TotalLoad;};

	virtual void Load(CIniFile *ini){};
	virtual void Save(CIniFile *ini){};
};

//////////////////////////////////////////////////////////////////////////

class SDBus36 : public SDElectricalUnit
{
private:
	bool		m_Available;
	SDBus27*	m_Bus27;
	double		m_TotalLoad;

public:
	SDBus36(SDBus27 *b27) {
		m_Available	= false;
		m_Volts		= BUS36_VOLTS_DEFAULT;
		m_Amps		= BUS36_AMPS_DEFAULT;
		m_Bus27		= b27;
		m_TotalLoad	= 0;
	};

	virtual ~SDBus36() {
	};

	virtual void Update() {
		// Input
		int p1_inv1_36v        = (int)AZS_GET(AZS_INV1_36V);
		int p1_inv2_36v        = (int)AZS_GET(AZS_INV2_36V);

		// Output
		bool pa_tbl_inv_36v_fail=false;

		// Code
		bool bus36;
		if((p1_inv1_36v||p1_inv2_36v)&&m_Bus27->IsAvailable()) {
			bus36=true;
			m_Volts=BUS36_VOLTS_DEFAULT;
		} else {
			bus36=false;
			m_Volts=0;
		}

		if((p1_inv1_36v&&p1_inv2_36v)&&m_Bus27->IsAvailable()) {
			m_TotalLoad=BUS36_LOAD_ON_BUS27;
			pa_tbl_inv_36v_fail = 0;
		} else if((p1_inv1_36v&&!p1_inv2_36v)&&m_Bus27->IsAvailable()) {
			m_TotalLoad=BUS36_LOAD_ON_BUS27;
			pa_tbl_inv_36v_fail = 1;
		} else if((!p1_inv1_36v&&p1_inv2_36v)&&m_Bus27->IsAvailable()) {
			m_TotalLoad=BUS36_LOAD_ON_BUS27;
			pa_tbl_inv_36v_fail = 1;
		} else {
			m_TotalLoad=0;
			pa_tbl_inv_36v_fail = 1;
		}

		if(bus36) {
			m_Available=true;
		} else {
			m_Available=false;
		}

		TBL_SET(TBL_INV_36V_FAIL,pa_tbl_inv_36v_fail);

	};

	inline bool IsAvailable() const { return m_Available;	};

	double GetVolts36(int num) {
		// Input
		int p1_inv1_36v        = (int)AZS_GET(AZS_INV1_36V);
		int p1_inv2_36v        = (int)AZS_GET(AZS_INV2_36V);
		int p8_capt_adi        = (int)AZS_GET(AZS_CAPT_ADI_BACKUP_BUS);
		int p8_capt_vsi        = (int)AZS_GET(AZS_CAPT_VSI_BUS);

		double ret=0;

		// Code
		switch(num) {
case GALETA_36_INV2_1:
case GALETA_36_INV2_2:
case GALETA_36_INV2_3:
	if(p1_inv2_36v&&m_Available) 
		ret=m_Volts;
	else
		ret=0;
	break;
case GALETA_36_AGB_AUXL_1:
case GALETA_36_AGB_AUXL_2:
case GALETA_36_AGB_AUXL_3:
	if(p8_capt_adi&&m_Available) 
		ret=m_Volts;
	else
		ret=0;
	break;
case GALETA_36_INV1_1:
case GALETA_36_INV1_2:
case GALETA_36_INV1_3:
	if(p1_inv1_36v&&m_Available) 
		ret=m_Volts;
	else
		ret=0;
	break;
case GALETA_36_DA30_1:
case GALETA_36_DA30_2:	
case GALETA_36_DA30_3:
	if(p8_capt_vsi&&m_Available) 
		ret=m_Volts;
	else
		ret=0;
	break;
		}
		return ret;
	}

	double GetTotalLoad() const {return m_TotalLoad;};

	virtual void Load(CIniFile *ini){};
	virtual void Save(CIniFile *ini){};
};

//////////////////////////////////////////////////////////////////////////

class SDSystemElectro : public SDLogicBase
{
private:
	double GetVCVolts36(double ndl);
	double GetVCVolts115(double ndl);

protected:
	SDBus27		m_Bus27;
	SDBus36		m_Bus36;
	SDBus115	m_Bus115;

public:
	SDSystemElectro();
	virtual ~SDSystemElectro();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

	void CheckAllPower();
};
