/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Radio.h"

#define CHECK100(V,O,N) /**/ \
	V--; \
	if(V<0) \
		V+=12; \
	GLT_SET(N,V);

#define CHECK10(V,N,M) /**/ \
	GLT_SET(N,V); \
	GLT_SET(M,V); 


#define CHECK1(V,N,M) /**/ \
	GLT_SET(N,V); \
	GLT_SET(M,V); 

SDSystemRadio::SDSystemRadio()
{
}

SDSystemRadio::~SDSystemRadio()
{
}

void SDSystemRadio::Init()
{
	GLT_SET(GLT_SGU6,SPU_KB);
	GLT_SET(GLT_SGU7,SPU_KB);

	char tmp4[7];
	tmp4[1]='\0';

	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sXPDRDefault[0];
	int x1=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sXPDRDefault[1];
	int x2=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sXPDRDefault[2];
	int x3=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sXPDRDefault[3];
	int x4=atoi(tmp4);

	POS_SET(POS_XPDR1,x1);
	POS_SET(POS_XPDR2,x2);
	POS_SET(POS_XPDR3,x3);
	POS_SET(POS_XPDR4,x4);

	g_SimData->SetXPNDR(Dec2Bcd(x1*1000+x2*100+x3*10+x4));

	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF1LDefault[0];
	tmp4[1]=g_SimData->m_Config->m_CfgNav->m_sADF1LDefault[1];
	tmp4[2]='\0';
	int a1l100=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF1LDefault[2];
	tmp4[1]='\0';
	int a1l10=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF1LDefault[3];
	int a1l1=atoi(tmp4);

	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF1RDefault[0];
	tmp4[1]=g_SimData->m_Config->m_CfgNav->m_sADF1RDefault[1];
	tmp4[2]='\0';
	int a1r100=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF1RDefault[2];
	tmp4[1]='\0';
	int a1r10=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF1RDefault[3];
	int a1r1=atoi(tmp4);

	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF2LDefault[0];
	tmp4[1]=g_SimData->m_Config->m_CfgNav->m_sADF2LDefault[1];
	tmp4[2]='\0';
	int a2l100=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF2LDefault[2];
	tmp4[1]='\0';
	int a2l10=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF2LDefault[3];
	int a2l1=atoi(tmp4);

	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF2RDefault[0];
	tmp4[1]=g_SimData->m_Config->m_CfgNav->m_sADF2RDefault[1];
	tmp4[2]='\0';
	int a2r100=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF2RDefault[2];
	tmp4[1]='\0';
	int a2r10=atoi(tmp4);
	tmp4[0]=g_SimData->m_Config->m_CfgNav->m_sADF2RDefault[3];
	int a2r1=atoi(tmp4);

	CHECK100(a1l100,a1l100c,GLT_ARK1LEFT100);
	CHECK100(a1r100,a1r100c,GLT_ARK1RIGHT100);
	CHECK100(a2l100,a2l100c,GLT_ARK2LEFT100);
	CHECK100(a2r100,a2r100c,GLT_ARK2RIGHT100);

	CHECK10(a1l10,GLT_ARK1LEFT10  ,GLT_ARK1LEFTHND);
	CHECK10(a1r10,GLT_ARK1RIGHT10 ,GLT_ARK1RIGHTHND);
	CHECK10(a2l10,GLT_ARK2LEFT10  ,GLT_ARK2LEFTHND);
	CHECK10(a2r10,GLT_ARK2RIGHT10 ,GLT_ARK2RIGHTHND);

	CHECK1(a1l1,GLT_ARK1LEFT1  ,GLT_ARK1LEFT1KNB);
	CHECK1(a1r1,GLT_ARK1RIGHT1 ,GLT_ARK1RIGHT1KNB);
	CHECK1(a2l1,GLT_ARK2LEFT1  ,GLT_ARK2LEFT1KNB);
	CHECK1(a2r1,GLT_ARK2RIGHT1 ,GLT_ARK2RIGHT1KNB);

	int com1=(int)(atof(g_SimData->m_Config->m_CfgRadio->m_sCOM1Default)*1000);
	int com2=(int)(atof(g_SimData->m_Config->m_CfgRadio->m_sCOM2Default)*1000);

	g_SimData->SetCOM1(Dec2Bcd(com1));
	g_SimData->SetCOM2(Dec2Bcd(com2));

	SetCom1(com1);
	SetCom2(com2);

	m_XPNDROff=false;
	m_COM1Off=false;
	m_COM2Off=false;
}

void SDSystemRadio::Update()
{
	XPDR();
	ADF1();
	ADF2();
	COM1();
	COM2();
	SGU();

	int ark1l=(GLT_GET(GLT_ARK1LEFT100)*100+GLT_GET(GLT_ARK1LEFT10)*10+GLT_GET(GLT_ARK1LEFT1))+100;
	int ark1r=(GLT_GET(GLT_ARK1RIGHT100)*100+GLT_GET(GLT_ARK1RIGHT10)*10+GLT_GET(GLT_ARK1RIGHT1))+100;
	int ark2l=(GLT_GET(GLT_ARK2LEFT100)*100+GLT_GET(GLT_ARK2LEFT10)*10+GLT_GET(GLT_ARK2LEFT1))+100;
	int ark2r=(GLT_GET(GLT_ARK2RIGHT100)*100+GLT_GET(GLT_ARK2RIGHT10)*10+GLT_GET(GLT_ARK2RIGHT1))+100;

}

void SDSystemRadio::Load(CIniFile *ini)
{
}

void SDSystemRadio::Save(CIniFile *ini)
{
}

void SDSystemRadio::XPDR()
{
	if(PWR_GET(PWR_SO72)) {
		if(m_XPNDROff) {
			SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
			if(myf!=Bcd2Dec(m_OldXPNDR))
				m_OldXPNDR=Dec2Bcd(myf);
			g_SimData->SetXPNDR(m_OldXPNDR);
			m_XPNDROff=false;
		} else {
			SINT32 m_SimXPNDR=(SINT32)g_SimData->GetParam(SIMPARAM_READ_TRANSPONDER_CODE1);
			SINT8 xpndrsim1=((m_SimXPNDR>>12)&0x000f);
			SINT8 xpndrsim2=((m_SimXPNDR>>8) &0x000f);
			SINT8 xpndrsim3=((m_SimXPNDR>>4) &0x000f);
			SINT8 xpndrsim4=((m_SimXPNDR)    &0x000f);
			POS_SET(POS_XPDR1,xpndrsim1);
			POS_SET(POS_XPDR2,xpndrsim2);
			POS_SET(POS_XPDR3,xpndrsim3);
			POS_SET(POS_XPDR4,xpndrsim4);
		}
	} else {
		if(!m_XPNDROff) 
			m_OldXPNDR=(SINT32)g_SimData->GetParam(SIMPARAM_READ_TRANSPONDER_CODE1);
		m_XPNDROff=true;
	}
}

void SDSystemRadio::ADF1()
{
	int ark1l=int((GLT_GET(GLT_ARK1LEFT100)*100+GLT_GET(GLT_ARK1LEFT10)*10+GLT_GET(GLT_ARK1LEFT1))+100);
	int ark1r=int((GLT_GET(GLT_ARK1RIGHT100)*100+GLT_GET(GLT_ARK1RIGHT10)*10+GLT_GET(GLT_ARK1RIGHT1))+100);
	POS_SET(POS_ADF1LFREQ,ark1l);
	POS_SET(POS_ADF1RFREQ,ark1r);
	if(PWR_GET(PWR_ADF1)) {
		int ark1;
		if(!AZS_GET(AZS_ARK1LEFTRIGHT)) 
			ark1=ark1l;
		else 
			ark1=ark1r;
		g_SimData->SetADF1(Dec2Bcd(ark1*10000));
		NDL_SET(NDL_ARK1SIGNAL,min(g_SimData->GetParam(SIMPARAM_READ_ADF_SIGNAL1),200000));
	}
}

void SDSystemRadio::ADF2()
{
	int ark2l=int((GLT_GET(GLT_ARK2LEFT100)*100+GLT_GET(GLT_ARK2LEFT10)*10+GLT_GET(GLT_ARK2LEFT1))+100);
	int ark2r=int((GLT_GET(GLT_ARK2RIGHT100)*100+GLT_GET(GLT_ARK2RIGHT10)*10+GLT_GET(GLT_ARK2RIGHT1))+100);
	POS_SET(POS_ADF2LFREQ,ark2l);
	POS_SET(POS_ADF2RFREQ,ark2r);
	if(PWR_GET(PWR_ADF2)) {
		int ark2;
		if(!AZS_GET(AZS_ARK2LEFTRIGHT)) 
			ark2=ark2l;
		else 
			ark2=ark2r;
		g_SimData->SetADF2(Dec2Bcd(ark2*10000));
		NDL_SET(NDL_ARK2SIGNAL,min(g_SimData->GetParam(SIMPARAM_READ_ADF_SIGNAL2),200000));
	}
}

void SDSystemRadio::SetCom(int var,int p1,int p2,int p3)
{
	char tmp[64],tmp4[7];

	unsigned int m_SIMCOMNorm=var*10;
	double freq=(double)m_SIMCOMNorm/1000;

	sprintf(tmp,"%.3f",freq);

	if(tmp[5]=='2'||tmp[5]=='7')
		tmp[6]='5';

	tmp4[0]=tmp[0];
	tmp4[1]=tmp[1];
	tmp4[2]=tmp[2];
	tmp4[3]='\0';
	int ato=atoi(tmp4);
	ato-=118;
	POS_SET(p1,ato);

	tmp4[0]=tmp[4];
	tmp4[1]='\0';
	ato=atoi(tmp4);
	POS_SET(p2,ato);

	tmp4[0]=tmp[5];
	tmp4[1]=tmp[6];
	tmp4[2]='\0';
	ato=atoi(tmp4);
	ato/=25;
	POS_SET(p3,ato);
}

void SDSystemRadio::SetCom1(int var)
{
	SetCom(var,POS_SCLVHF1LEFT,POS_SCLVHF1MID,POS_SCLVHF1RIGHT);
}

void SDSystemRadio::SetCom2(int var)
{
	SetCom(var,POS_SCLVHF2LEFT,POS_SCLVHF2MID,POS_SCLVHF2RIGHT);
}

void SDSystemRadio::COM1()
{
	SetCom1(Bcd2Dec((UINT32)g_SimData->GetParam(SIMPARAM_READ_COM_ACTIVE_FREQUENCY1))+10000);
}

void SDSystemRadio::COM2()
{
	SetCom2(Bcd2Dec((UINT32)g_SimData->GetParam(SIMPARAM_READ_COM_ACTIVE_FREQUENCY2))+10000);
}

void SDSystemRadio::SGU()
{
	int sgul=GLT_GET(GLT_SGU6);
	int sgur=GLT_GET(GLT_SGU7);

	if((sgul==SPU_KB&&sgur==SPU_KB)||!PWR_GET(PWR_COM1)&&!PWR_GET(PWR_COM2)&&!PWR_GET(PWR_ADF1)&&!PWR_GET(PWR_ADF2)) {
		AllOff();
	} else if(sgul==SPU_COM1&&sgur==SPU_COM1) {
		AllOff();
		COM1On();
	} else if(sgul==SPU_COM2&&sgur==SPU_COM2) {
		AllOff();
		COM2On();
	} else if(sgul==SPU_ADF1&&sgur==SPU_ADF1) {
		AllOff();
		ADF1On();
	} else if(sgul==SPU_ADF2&&sgur==SPU_ADF2) {
		AllOff();
		ADF2On();
	} else if((sgul==SPU_COM1&&sgur==SPU_COM2)||(sgul==SPU_COM2&&sgur==SPU_COM1)) {
		AllOff();
		AllCOMOn();
	} else if((sgul==SPU_ADF1&&sgur==SPU_ADF2)||(sgul==SPU_ADF2&&sgur==SPU_ADF1)) {
		AllOff();
		ADF1On();
		ADF2On();
	} else if((sgul==SPU_COM1&&sgur==SPU_KB)||(sgul==SPU_KB&&sgur==SPU_COM1)) {
		AllOff();
		COM1On();
	} else if((sgul==SPU_COM2&&sgur==SPU_KB)||(sgul==SPU_KB&&sgur==SPU_COM2)) {
		AllOff();
		COM2On();
	} else if((sgul==SPU_COM1&&sgur==SPU_ADF1)||(sgul==SPU_ADF1&&sgur==SPU_COM1)) {
		AllOff();
		COM1On();
		ADF1On();
	} else if((sgul==SPU_COM2&&sgur==SPU_ADF1)||(sgul==SPU_ADF1&&sgur==SPU_COM2)) {
		AllOff();
		COM2On();
		ADF1On();
	} else if((sgul==SPU_COM1&&sgur==SPU_ADF2)||(sgul==SPU_ADF2&&sgur==SPU_COM1)) {
		AllOff();
		COM1On();
		ADF2On();
	} else if((sgul==SPU_COM2&&sgur==SPU_ADF2)||(sgul==SPU_ADF2&&sgur==SPU_COM2)) {
		AllOff();
		COM2On();
		ADF2On();
	} 
}

void SDSystemRadio::ADF1On()
{
	if(PWR_GET(PWR_ADF1))
		g_SimData->ADF1IdentEnable();
}

void SDSystemRadio::ADF2On()
{
	if(PWR_GET(PWR_ADF2))
		g_SimData->ADF2IdentEnable();
}

void SDSystemRadio::ADF1Off()
{
	g_SimData->ADF1IdentDisable();
}

void SDSystemRadio::ADF2Off()
{
	g_SimData->ADF2IdentDisable();
}

void SDSystemRadio::COM1On()
{
	//AllCOMOff();
	if(PWR_GET(PWR_COM1))
		g_SimData->COM1TransmitSel();
}

void SDSystemRadio::COM2On()
{
	//AllCOMOff();
	if(PWR_GET(PWR_COM2))
		g_SimData->COM2TransmitSel();
}

void SDSystemRadio::AllOff()
{
	AllCOMOff();
	ADF1Off();
	ADF2Off();
}

void SDSystemRadio::AllCOMOn()
{
	if(PWR_GET(PWR_COM1)&&PWR_GET(PWR_COM2)) {
		g_SimData->COMReceiveAllSet(1);
	} else {
		COM1On();
		COM2On();
	}
}

void SDSystemRadio::AllCOMOff()
{
	g_SimData->COMReceiveAllSet(0);
}

