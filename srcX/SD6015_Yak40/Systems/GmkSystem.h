/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/GmkSystem.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../LogicBase.h"

class CGyroSharedData
{
public:
	CGyroSharedData();
	void update();
	void reset();

	inline FLOAT64 currLongitude () { return m_currLongitude;           };
	inline FLOAT64 currLatitude  () { return m_currLatitude;            };
	inline FLOAT64 currElapsed   () { return 0.03; /*m_ElapsedTime.var_value.n;*/ };

	inline FLOAT64 prevLongitude () { return m_prevLongitude;   };
	inline FLOAT64 prevLatitude  () { return m_prevLatitude;    };
	inline FLOAT64 prevElapsed   () { return m_prevElapsedTime; };

	inline FLOAT64 deltaLongitude() { return m_deltaLongitude;   };
	inline FLOAT64 deltaLatitude () { return m_deltaLatitude;    };
	inline FLOAT64 deltaElapsed  () { return m_deltaElapsedTime; };

	inline FLOAT64 courseTrue    () { return g_SimData->GetParam(SIMPARAM_READ_PLANE_HEADING_DEGREES_TRUE);     };
	inline FLOAT64 courseMagnetic() { return g_SimData->GetParam(SIMPARAM_READ_PLANE_HEADING_DEGREES_MAGNETIC); };
	inline FLOAT64 magVar        () { return g_SimData->GetParam(SIMPARAM_READ_MAGVAR);         };

protected:
	FLOAT64 m_currLongitude;
	FLOAT64 m_currLatitude;

	FLOAT64 m_prevLongitude;
	FLOAT64 m_prevLatitude;
	FLOAT64 m_prevElapsedTime;
	FLOAT64 m_deltaLongitude;
	FLOAT64 m_deltaLatitude;
	FLOAT64 m_deltaElapsedTime;
};

class CGyroUnit
{
public:
	enum enMode { mdMK = 1, mdGPK = 2, mdAK = 3 };

	CGyroUnit(CGyroSharedData *pGyroSharedData);
	~CGyroUnit();

	void update(enMode mode, bool bFastCoord, int nCourseSelector, double nLatitudeSelector, bool sound=false);

	FLOAT64 course();

	double GetASA(){return m_Axis_Shift_Angle;};
	double GetOFA(){return m_Ortho_Fix_Angle;};
	double GetOSS(){return m_Own_Shift_Speed;};

	void SetASA(double val){m_Axis_Shift_Angle=val;};
	void SetOFA(double val){m_Ortho_Fix_Angle=val;};
	void SetOSS(double val){m_Own_Shift_Speed=val;};


protected:
	CGyroSharedData* m_pGyroSharedData;

	// ���� ����� ���� ��������� � �������� ���������� � ����� ���������� ������������.
	FLOAT64 m_Axis_Shift_Angle;
	// ��������������� �������� - ���� ����� ���� ��������� � ������� �������� ����������.
	FLOAT64 m_Ortho_Fix_Angle;
	// ����������� ���� ���������
	FLOAT64 m_Own_Shift_Speed;

private:
	static bool m_bSeeded;

};

class CKS8Unit
{
public:
	CKS8Unit();
	~CKS8Unit();

	void update(int powered, double modeselector, bool coordbutton, double courseselector, int hemisphereswitch, double latselector, int gyrounitswitch, double &pricoursesignal, double &seccoursesignal);

public:
	auto_ptr<CGyroSharedData> m_pGyroSharedData;
	auto_ptr<CGyroUnit>       m_pMainGyro;
	auto_ptr<CGyroUnit>       m_pAuxlGyro;

private:
};

class SDSystemGMK : public SDLogicBase
{
private:
	auto_ptr<CKS8Unit> m_KS8Unit;

public:
	SDSystemGMK();
	virtual ~SDSystemGMK();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

};
