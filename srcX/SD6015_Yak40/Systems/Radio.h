/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../LogicBase.h"
#include "../../Lib/NamedVar.h"

class SDSystemRadio : public SDLogicBase
{
private:
	enum SPU {
		SPU_COM1,
		SPU_COM2,
		SPU_KB,
		SPU_ADF1,
		SPU_ADF2
	};

	SINT32 m_OldXPNDR;
	bool m_XPNDROff;

	SINT32 m_OldCOM1;
	bool m_COM1Off;

	SINT32 m_OldCOM2;
	bool m_COM2Off;

	void ADF1();
	void ADF2();
	void XPDR();
	void COM1();
	void COM2();
	void SetCom(int var,int p1,int p2,int p3);
	void SetCom1(int var);
	void SetCom2(int var);
	void SGU();

	inline void ADF1On();
	inline void ADF2On();
	inline void ADF1Off();
	inline void ADF2Off();
	inline void COM1On();
	inline void COM2On();
	inline void COM1Off();
	inline void COM2Off();
	inline void AllOff();
	inline void AllCOMOn();
	inline void AllCOMOff();

public:
	SDSystemRadio();
	virtual ~SDSystemRadio();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};

