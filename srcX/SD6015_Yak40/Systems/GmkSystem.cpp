/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/GmkSystem.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "GmkSystem.h"

bool m_GmkPlayed=false;

CGyroSharedData::CGyroSharedData()
{
	m_prevLongitude   = m_currLongitude = g_SimData->GetParam(SIMPARAM_READ_PLANE_LONGITUDE);
	m_prevLatitude    = m_currLatitude  = g_SimData->GetParam(SIMPARAM_READ_PLANE_LATITUDE);
	m_prevElapsedTime = 0.03; //m_ElapsedTime.var_value.n;
}

void CGyroSharedData::update()
{
	m_prevLongitude   = m_currLongitude;
	m_prevLatitude    = m_currLatitude;
	m_prevElapsedTime = 0.03; //m_ElapsedTime.var_value.n;

	m_currLongitude = g_SimData->GetParam(SIMPARAM_READ_PLANE_LONGITUDE);
	m_currLatitude  = g_SimData->GetParam(SIMPARAM_READ_PLANE_LATITUDE);

	m_deltaLongitude   = m_currLongitude - m_prevLongitude;
	m_deltaLatitude    = m_currLatitude  - m_prevLatitude;
	m_deltaElapsedTime = 0.03; //m_ElapsedTime.var_value.n - m_prevElapsedTime;
}

//////////////////////////////////////////////////////////////////////////

const double nAzimutalShiftSpeed  = 15.0 / 3600; // 15��/���. - ��������� ���������
const double nFastCoordShiftSpeed =  6.0;        // 3��/�.    - �������� �������� ������������
const double nNormCoordShiftSpeed =  1.5;        // 0.5��/�.  - �������� ����������� ������������
const double nOwnShiftMaxSpeed    =  2.0 / 3600; // 2��/���   - ����������� ���� ���������

bool CGyroUnit::m_bSeeded = false;

CGyroUnit::CGyroUnit(CGyroSharedData* pGyroSharedData) : m_pGyroSharedData( pGyroSharedData ),
														 m_Ortho_Fix_Angle( 0 )
{
	if( !m_bSeeded ) { 
		srand( (unsigned)time( 0 ) ); 
		m_bSeeded = true; 
	};
	m_Axis_Shift_Angle = rand() * 360.0 / RAND_MAX;
	m_Own_Shift_Speed  = nOwnShiftMaxSpeed - (rand() * (nOwnShiftMaxSpeed * 2) / RAND_MAX);
}

CGyroUnit::~CGyroUnit(void)
{
}

void CGyroUnit::update(enMode mode, bool bFastCoord, int nCourseSelector, double nLatitudeSelector, bool sound)
{
	double CurrentCourse = course();
	double EtalonCourse  = (mode == mdMK) ? dnorm( m_pGyroSharedData->courseTrue() - m_pGyroSharedData->magVar() ) : 
		m_pGyroSharedData->courseTrue();

	// ����� �� ��� ��. ������������� � ��������� ������.
	if( mode != mdGPK ) { 
		if( abs( EtalonCourse - CurrentCourse ) <= (bFastCoord ? 0.8 : 0.1) ) {
			POS_SET(POS_GMK_READY,1);
			return;
		}
		if( turn_side( EtalonCourse, CurrentCourse ) )
			m_Axis_Shift_Angle -= m_pGyroSharedData->deltaElapsed() * (bFastCoord ? nFastCoordShiftSpeed : nNormCoordShiftSpeed);
		else 
			m_Axis_Shift_Angle += m_pGyroSharedData->deltaElapsed() * (bFastCoord ? nFastCoordShiftSpeed : nNormCoordShiftSpeed);

		return;
	}

	if(!m_GmkPlayed&&POS_GET(POS_GMK_READY)&&mode==mdGPK&&sound) {
		SND_SET(SND_GMK_ON,1);
		POS_SET(POS_GMK_READY,0);
		m_GmkPlayed=true;
	}

	//����� ���. 
	//������� �������� �����
	if( nCourseSelector ) {
		if( nCourseSelector == 1 ) m_Axis_Shift_Angle += m_pGyroSharedData->deltaElapsed() * nFastCoordShiftSpeed;
		if( nCourseSelector == 2 ) m_Axis_Shift_Angle -= m_pGyroSharedData->deltaElapsed() * nFastCoordShiftSpeed;
		return;
	}

	FLOAT64 sin_phi = sin( dgrd(m_pGyroSharedData->currLatitude()) );

	//����� ���. 
	//������� ������ ���� ����������� ��� ��������� � ������� ���������� � ��������������.
	m_Ortho_Fix_Angle += m_pGyroSharedData->deltaLongitude() * sin_phi;

	// ����������� ���� ���������
	m_Axis_Shift_Angle += m_Own_Shift_Speed * m_pGyroSharedData->deltaElapsed();
	// ������������ ���� ���������
	m_Axis_Shift_Angle += nAzimutalShiftSpeed * sin_phi * m_pGyroSharedData->deltaElapsed();

	// ������������ ���������
	m_Axis_Shift_Angle -= nAzimutalShiftSpeed * sin( dgrd(nLatitudeSelector) ) * m_pGyroSharedData->deltaElapsed();
	m_Axis_Shift_Angle = dnorm( m_Axis_Shift_Angle );  
}

FLOAT64 CGyroUnit::course()
{
	return dnorm( m_pGyroSharedData->courseTrue() - (m_Axis_Shift_Angle + m_Ortho_Fix_Angle) );
}

//////////////////////////////////////////////////////////////////////////

CKS8Unit::CKS8Unit()	:	m_pGyroSharedData (0),
							m_pMainGyro(0),
							m_pAuxlGyro(0)
{
	m_pGyroSharedData = auto_ptr<CGyroSharedData>( new CGyroSharedData						);
	m_pMainGyro       = auto_ptr<CGyroUnit		>( new CGyroUnit( m_pGyroSharedData.get() )	);
	m_pAuxlGyro       = auto_ptr<CGyroUnit		>( new CGyroUnit( m_pGyroSharedData.get() )	);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CKS8Unit::~CKS8Unit()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CKS8Unit::update(int powered, double modeselector, bool coordbutton, double courseselector, int hemisphereswitch, double latselector, int gyrounitswitch, double &pricoursesignal, double &seccoursesignal)
{
	m_pGyroSharedData->update();

	if( !powered ) { 
		m_GmkPlayed=false;
		return;
	}

	CGyroUnit::enMode PrimaryGyroMode = (CGyroUnit::enMode)(int)modeselector;
	bool    bFastCoorButton = coordbutton;
	int     nCourseSelector = (int)courseselector;
	double  nLatitudeSelector = hemisphereswitch ? -latselector : +latselector;

	CGyroUnit* priGyro;
	CGyroUnit* secGyro;

	if( gyrounitswitch ) {
		priGyro = m_pMainGyro.get();
		secGyro = m_pAuxlGyro.get();
	} else {
		priGyro = m_pAuxlGyro.get();
		secGyro = m_pMainGyro.get();
	}

	if( PrimaryGyroMode == CGyroUnit::mdGPK || PrimaryGyroMode == CGyroUnit::mdAK ) {
		priGyro->update( PrimaryGyroMode, bFastCoorButton, nCourseSelector, nLatitudeSelector, true );
		secGyro->update( CGyroUnit::mdMK, bFastCoorButton, nCourseSelector, nLatitudeSelector );
	} else {
		priGyro->update( PrimaryGyroMode,  bFastCoorButton, nCourseSelector, nLatitudeSelector, true );
		secGyro->update( CGyroUnit::mdGPK, bFastCoorButton, nCourseSelector, nLatitudeSelector );
	}

	pricoursesignal = priGyro->course();
	seccoursesignal = secGyro->course();
}

//////////////////////////////////////////////////////////////////////////

SDSystemGMK::SDSystemGMK()
{
	m_KS8Unit=auto_ptr<CKS8Unit>(new CKS8Unit());
}

SDSystemGMK::~SDSystemGMK()
{
}

void SDSystemGMK::Init()
{
}

void SDSystemGMK::Update()
{
	int m_nvKS8_Power_Source_Line		= PWR_GET(PWR_GMK);
	double m_nvKS8_Mode_Selector		= AZS_GET(AZS_GMK_MODE)+1;
	double m_nvKS8_Course_Selector		= AZS_GET(AZS_GMK_MODE)==0?0:AZS_GET(AZS_GMK_COURSE);
	int m_nvKS8_Hemisphere_Switch		= AZS_GET(AZS_GMK_HEMISPHERE);
	int m_nvKS8_GyroUnit_Switch			= AZS_GET(AZS_GMK_PRI_AUX);
	double m_nvKS8_Latitude_Selector	= NDL_GET(NDL_GMK_SCALE)<0?fabs(NDL_GET(NDL_GMK_SCALE)):90-NDL_GET(NDL_GMK_SCALE);
	bool m_nvKS8_Coord_Button			= AZS_GET(AZS_GMK_MODE)==0&&(AZS_GET(AZS_GMK_COURSE)==1||AZS_GET(AZS_GMK_COURSE)==2)?true:false;

	double m_nvKS8_Primary_Course_Signal=0;
	double m_nvKS8_Secondary_Course_Signal=0;

	m_KS8Unit->update(	m_nvKS8_Power_Source_Line,
		m_nvKS8_Mode_Selector,
		m_nvKS8_Coord_Button,
		m_nvKS8_Course_Selector,
		m_nvKS8_Hemisphere_Switch,
		m_nvKS8_Latitude_Selector,
		m_nvKS8_GyroUnit_Switch,
		m_nvKS8_Primary_Course_Signal,
		m_nvKS8_Secondary_Course_Signal
		);

	if(AZS_GET(AZS_GMK_TEST)==1&&AZS_GET(AZS_GMK_MODE)==0) {
		NDL_SET(NDL_GMK_PRICOURSE,0);
		NDL_SET(NDL_GMK_SECCOURSE,0);
		LMP_SET(LMP_GMK_ZAP,1);
		LMP_SET(LMP_GMK_OSN,1);
	} else if(AZS_GET(AZS_GMK_TEST)==2&&AZS_GET(AZS_GMK_MODE)==0) {
		NDL_SET(NDL_GMK_PRICOURSE,300);
		NDL_SET(NDL_GMK_SECCOURSE,300);
		LMP_SET(LMP_GMK_ZAP,1);
		LMP_SET(LMP_GMK_OSN,1);
	} else {
		NDL_SET(NDL_GMK_PRICOURSE,m_nvKS8_Primary_Course_Signal);
		NDL_SET(NDL_GMK_SECCOURSE,m_nvKS8_Secondary_Course_Signal);
		LMP_SET(LMP_GMK_ZAP,0);
		LMP_SET(LMP_GMK_OSN,0);
	}
}

static const string MAINASA="MainASA";
static const string MAINOFA="MainOFA";
static const string MAINOSS="MainOSS";
static const string AUXLASA="AuxlASA";
static const string AUXLOFA="AuxlOFA";
static const string AUXLOSS="AuxlOSS";

void SDSystemGMK::Load(CIniFile *ini)
{
	m_KS8Unit->m_pMainGyro->SetASA(ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),MAINASA.c_str(),rand() * 360.0 / RAND_MAX));
	m_KS8Unit->m_pAuxlGyro->SetASA(ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AUXLASA.c_str(),rand() * 360.0 / RAND_MAX));
	m_KS8Unit->m_pMainGyro->SetOFA(ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),MAINOFA.c_str(),0));
	m_KS8Unit->m_pAuxlGyro->SetOFA(ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AUXLOFA.c_str(),0));
	m_KS8Unit->m_pMainGyro->SetOSS(ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),MAINOSS.c_str(),nOwnShiftMaxSpeed - (rand() * (nOwnShiftMaxSpeed * 2) / RAND_MAX)));
	m_KS8Unit->m_pAuxlGyro->SetOSS(ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AUXLOSS.c_str(),nOwnShiftMaxSpeed - (rand() * (nOwnShiftMaxSpeed * 2) / RAND_MAX)));
}

void SDSystemGMK::Save(CIniFile *ini)
{
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),MAINASA.c_str(),m_KS8Unit->m_pMainGyro->GetASA());
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AUXLASA.c_str(),m_KS8Unit->m_pAuxlGyro->GetASA());
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),MAINOFA.c_str(),m_KS8Unit->m_pMainGyro->GetOFA());
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AUXLOFA.c_str(),m_KS8Unit->m_pAuxlGyro->GetOFA());
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),MAINOSS.c_str(),m_KS8Unit->m_pMainGyro->GetOSS());
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AUXLOSS.c_str(),m_KS8Unit->m_pAuxlGyro->GetOSS());
}

