/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	0
#define OFFY	637

//////////////////////////////////////////////////////////////////////////

static NONLINEARITY tbl_ark1signal[]={
	{{418-OFFX, 696-OFFY},     0,0},
	{{426-OFFX, 691-OFFY},  1000,0},
	{{436-OFFX, 689-OFFY},  2500,0},
	{{445-OFFX, 688-OFFY},  7500,0},
	{{455-OFFX, 689-OFFY}, 25000,0},
	{{465-OFFX, 691-OFFY}, 67500,0},
	{{472-OFFX, 696-OFFY},200000,0}
};

static MAKE_ICB(icb_ark1right1knb	,SHOW_GLT(GLT_ARK1RIGHT1KNB,10));
static MAKE_ICB(icb_ark1left1knb	,SHOW_GLT(GLT_ARK1LEFT1KNB,10));
static MAKE_ICB(icb_ark1righthnd	,SHOW_GLT(GLT_ARK1RIGHTHND,10));
static MAKE_ICB(icb_ark1lefthnd		,SHOW_GLT(GLT_ARK1LEFTHND,10));
static MAKE_ICB(icb_ark1right1		,SHOW_GLT(GLT_ARK1RIGHT1,10));
static MAKE_ICB(icb_ark1left1		,SHOW_GLT(GLT_ARK1LEFT1,10));
static MAKE_ICB(icb_ark1right10		,SHOW_GLT(GLT_ARK1RIGHT10,10));
static MAKE_ICB(icb_ark1left10		,SHOW_GLT(GLT_ARK1LEFT10,10));
static MAKE_ICB(icb_ark1right100	,SHOW_GLT(GLT_ARK1RIGHT100,12));
static MAKE_ICB(icb_ark1left100		,SHOW_GLT(GLT_ARK1LEFT100,12));
static MAKE_ICB(icb_ark1       		,SHOW_GLT(GLT_ARK1,4));
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_ndlark1signalD	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(!PWR_GET(PWR_ADF1)){return 0;}else{CHK_BRT(); return NDL_GET(NDL_ARK1SIGNAL);}}else HIDE_IMAGE(pelement);return -1;);
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_ndlark1signalM	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(!PWR_GET(PWR_ADF1)){return 0;}else{CHK_BRT(); return NDL_GET(NDL_ARK1SIGNAL);}}else HIDE_IMAGE(pelement);return -1;);
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_ndlark1signalP	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(!PWR_GET(PWR_ADF1)){return 0;}else{CHK_BRT(); return NDL_GET(NDL_ARK1SIGNAL);}}else HIDE_IMAGE(pelement);return -1;);
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_ndlark1signalR	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(!PWR_GET(PWR_ADF1)){return 0;}else{CHK_BRT(); return NDL_GET(NDL_ARK1SIGNAL);}}else HIDE_IMAGE(pelement);return -1;);
#endif
static MAKE_ICB(icb_sclark1signal	,if( PWR_GET(PWR_ADF1)){CHK_BRT(); return POS_GET(POS_PANEL_STATE);} else {return -1;});
static MAKE_ICB(icb_ark1dir     	,SHOW_AZS(AZS_ARK1DIR ,3));
static MAKE_ICB(icb_ark1cw      	,SHOW_AZS(AZS_ARK1CW  ,2));
static MAKE_ICB(icb_ark1a			,if(!PWR_GET(PWR_ADF1)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark1b			,if(!PWR_GET(PWR_ADF1)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark1c			,if(!PWR_GET(PWR_ADF1)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark1d			,if(!PWR_GET(PWR_ADF1)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark1e			,if(!PWR_GET(PWR_ADF1)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark1f			,if(!PWR_GET(PWR_ADF1)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark1g			,if(!PWR_GET(PWR_ADF1)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_ark1dir_l          ,PRS_AZS(AZS_ARK1DIR       ,1))
static MAKE_MSCB(mcb_ark1dir_r          ,PRS_AZS(AZS_ARK1DIR       ,2))
static MAKE_MSCB(mcb_ark1cw		        ,AZS_TGL(AZS_ARK1CW        ))

// ���������
static MAKE_MSCB(mcb_glt_ark1_l		    ,GLT_DEC(GLT_ARK1          ))
static MAKE_MSCB(mcb_glt_ark1_r		    ,GLT_INC(GLT_ARK1          ))
static MAKE_MSCB(mcb_glt_ark1left100_l	,GLT_DEC(GLT_ARK1LEFT100   ))
static MAKE_MSCB(mcb_glt_ark1left100_r	,GLT_INC(GLT_ARK1LEFT100   ))
static MAKE_MSCB(mcb_glt_ark1right100_l ,GLT_DEC(GLT_ARK1RIGHT100  ))
static MAKE_MSCB(mcb_glt_ark1right100_r ,GLT_INC(GLT_ARK1RIGHT100  ))
static MAKE_MSCB(mcb_glt_ark1left10_l	,GLT_DEC(GLT_ARK1LEFT10    ); GLT_DEC(GLT_ARK1LEFTHND     ))
static MAKE_MSCB(mcb_glt_ark1left10_r	,GLT_INC(GLT_ARK1LEFT10    ); GLT_INC(GLT_ARK1LEFTHND     ))
static MAKE_MSCB(mcb_glt_ark1right10_l  ,GLT_DEC(GLT_ARK1RIGHT10   ); GLT_DEC(GLT_ARK1RIGHTHND    ))
static MAKE_MSCB(mcb_glt_ark1right10_r  ,GLT_INC(GLT_ARK1RIGHT10   ); GLT_INC(GLT_ARK1RIGHTHND    ))
static MAKE_MSCB(mcb_glt_ark1left1_l	,GLT_DEC(GLT_ARK1LEFT1     ); GLT_DEC(GLT_ARK1LEFT1KNB    ))
static MAKE_MSCB(mcb_glt_ark1left1_r	,GLT_INC(GLT_ARK1LEFT1     ); GLT_INC(GLT_ARK1LEFT1KNB    ))
static MAKE_MSCB(mcb_glt_ark1right1_l   ,GLT_DEC(GLT_ARK1RIGHT1    ); GLT_DEC(GLT_ARK1RIGHT1KNB   ))
static MAKE_MSCB(mcb_glt_ark1right1_r   ,GLT_INC(GLT_ARK1RIGHT1    ); GLT_INC(GLT_ARK1RIGHT1KNB   ))

static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_ARK1DIR                 ))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_ARK1CW                  ))
static MAKE_TCB(tcb_03	,GLT_TT(GLT_ARK1                    ))
static MAKE_TCB(tcb_04	,GLT_TT(GLT_ARK1LEFT100             ))
static MAKE_TCB(tcb_05	,GLT_TT(GLT_ARK1RIGHT100            ))
static MAKE_TCB(tcb_06	,GLT_TT(GLT_ARK1LEFTHND             ))
static MAKE_TCB(tcb_07	,GLT_TT(GLT_ARK1RIGHTHND            ))
static MAKE_TCB(tcb_08	,GLT_TT(GLT_ARK1LEFT1KNB            ))
static MAKE_TCB(tcb_09	,GLT_TT(GLT_ARK1RIGHT1KNB           ))
static MAKE_TCB(tcb_10	,NDL_TT(NDL_ARK1SIGNAL				))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p5_6,HELP_NONE,0,0)

MOUSE_PBOX(0,0,P5_BACKGROUND6_D_SX,P5_BACKGROUND6_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_TTPB( "10",  393-OFFX, 669-OFFY,110, 77)    // NDL_ARK1SIGNAL				    	   
//MOUSE_TSHB( "1",  643-OFFX,  681-OFFY, 43, 33,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_ark1dir_l,mcb_ark1dir_r)
//MOUSE_TBOX( "2",  278-OFFX,  764-OFFY, 34, 46,CURSOR_HAND,MOUSE_LR,mcb_ark1cw)

// ���������
MOUSE_TSHB( "3",  511-OFFX,  709-OFFY,128, 96,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark1_l,mcb_glt_ark1_r)
MOUSE_TSHB( "4",  380-OFFX,  825-OFFY,160,148,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark1left100_l,mcb_glt_ark1left100_r)
MOUSE_TSHB( "5",  623-OFFX,  825-OFFY,160,148,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark1right100_l,mcb_glt_ark1right100_r)
MOUSE_TSHB( "6",  407-OFFX,  859-OFFY, 98, 95,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark1left10_l,mcb_glt_ark1left10_r)
MOUSE_TSHB( "7",  653-OFFX,  859-OFFY, 98, 95,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark1right10_l,mcb_glt_ark1right10_r)
MOUSE_TSHB( "8",  313-OFFX,  883-OFFY, 55, 54,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark1left1_l,mcb_glt_ark1left1_r)
MOUSE_TSHB( "9",  556-OFFX,  883-OFFY, 55, 54,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark1right1_l,mcb_glt_ark1right1_r)
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p5_6_list; 
GAUGE_HEADER_FS700_EX(P5_BACKGROUND6_D_SX, "p5_6", &p5_6_list, rect_p5_6, 0, 0, 0, 0, p5_6); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p5_swt_ark1dir			,P5_SWT_ARK1DIR_D_00		,NULL						, 643-OFFX,  681-OFFY,icb_ark1dir     		, 3 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_azs_ark1cw			,P5_AZS_ARK1_CW_D_00		,&l_p5_swt_ark1dir			, 278-OFFX,  764-OFFY,icb_ark1cw      		, 2 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1volume		,P5_COV_ARK1VOLUME_D		,&l_p5_azs_ark1cw			, 712-OFFX,  729-OFFY,icb_Ico          		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right1knb	,P5_KNB_ARK1RIGHT1_D_00		,&l_p5_cov_ark1volume		, 556-OFFX,  883-OFFY,icb_ark1right1knb		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left1knb	,P5_KNB_ARK1LEFT1_D_00		,&l_p5_glt_ark1right1knb	, 313-OFFX,  883-OFFY,icb_ark1left1knb		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1righthnd	,P5_GLT_ARK1RIGHTHANDLE_D_00,&l_p5_glt_ark1left1knb		, 632-OFFX,  844-OFFY,icb_ark1righthnd 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1lefthnd		,P5_GLT_ARK1LEFTHANDLE_D_00	,&l_p5_glt_ark1righthnd		, 379-OFFX,  844-OFFY,icb_ark1lefthnd 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1right		,P5_COV_ARK1RIGHT_D			,&l_p5_glt_ark1lefthnd		, 618-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1left		,P5_COV_ARK1LEFT_D			,&l_p5_cov_ark1right		, 374-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right1		,P5_GLT_ARK1RIGHT1_D_00		,&l_p5_cov_ark1left			, 664-OFFX,  871-OFFY,icb_ark1right1 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left1		,P5_GLT_ARK1LEFT1_D_00		,&l_p5_glt_ark1right1		, 420-OFFX,  871-OFFY,icb_ark1left1 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right10		,P5_GLT_ARK1RIGHT10_D_00	,&l_p5_glt_ark1left1		, 653-OFFX,  859-OFFY,icb_ark1right10		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left10		,P5_GLT_ARK1LEFT10_D_00		,&l_p5_glt_ark1right10		, 407-OFFX,  859-OFFY,icb_ark1left10		, 10*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1right100	,P5_GLT_ARK1RIGHT100_D_00	,&l_p5_glt_ark1left10		, 623-OFFX,  825-OFFY,icb_ark1right100		, 12*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1left100		,P5_GLT_ARK1LEFT100_D_00	,&l_p5_glt_ark1right100		, 380-OFFX,  825-OFFY,icb_ark1left100		, 12*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1			,P5_GLT_ARK1_D_00			,&l_p5_glt_ark1left100		, 511-OFFX,  709-OFFY,icb_ark1       		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_cov_ark1signal		,P5_COV_ARK1SIGNAL_D		,&l_p5_glt_ark1				, 413-OFFX,  721-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_NEEDLE2	(p5_ndl_ark1signalD		,P5_NDL_ARK1SIGNAL_D		,&l_p5_cov_ark1signal		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalD, tbl_ark1signal, 18  , p5_6)		  
MY_NEEDLE2	(p5_ndl_ark1signalM		,P5_NDL_ARK1SIGNAL_M		,&l_p5_ndl_ark1signalD		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalM, tbl_ark1signal, 18  , p5_6)		  
MY_NEEDLE2	(p5_ndl_ark1signalP		,P5_NDL_ARK1SIGNAL_P		,&l_p5_ndl_ark1signalM		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalP, tbl_ark1signal, 18  , p5_6)		  
MY_NEEDLE2	(p5_ndl_ark1signalR		,P5_NDL_ARK1SIGNAL_R		,&l_p5_ndl_ark1signalP		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalR, tbl_ark1signal, 18  , p5_6)		  
MY_ICON2	(p5_scl_ark1signal		,P5_SCL_ARK1SIGNAL_D		,&l_p5_ndl_ark1signalR		, 383-OFFX,  661-OFFY,icb_sclark1signal		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1a			,P5_TBL_ARK1A				,&l_p5_scl_ark1signal		, 274-OFFX,  732-OFFY,icb_ark1a       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1b			,P5_TBL_ARK1B				,&l_p5_tbl_ark1a			, 281-OFFX,  795-OFFY,icb_ark1b       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1c			,P5_TBL_ARK1C				,&l_p5_tbl_ark1b			, 295-OFFX,  855-OFFY,icb_ark1c       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1d			,P5_TBL_ARK1D				,&l_p5_tbl_ark1c			, 538-OFFX,  855-OFFY,icb_ark1d       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1e			,P5_TBL_ARK1E				,&l_p5_tbl_ark1d			, 532-OFFX,  834-OFFY,icb_ark1e       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_tbl_ark1f			,P5_TBL_ARK1F				,&l_p5_tbl_ark1e			, 512-OFFX,  685-OFFY,icb_ark1f       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_tbl_ark1g			,P5_TBL_ARK1G				,&l_p5_tbl_ark1f			, 705-OFFX,  721-OFFY,icb_ark1g       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_6Ico				,P5_BACKGROUND6_D			,&l_p5_tbl_ark1g			,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX	, p5_6) 
MY_STATIC2	(p5_6bg,p5_6_list		,P5_BACKGROUND6_D			,&l_p5_6Ico					,	 p5_6);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p5_swt_ark1dir			,P5_SWT_ARK1DIR_D_00		,NULL						, 643-OFFX,  681-OFFY,icb_ark1dir     		, 3 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_azs_ark1cw			,P5_AZS_ARK1_CW_D_00		,&l_p5_swt_ark1dir			, 278-OFFX,  764-OFFY,icb_ark1cw      		, 2 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1volume		,P5_COV_ARK1VOLUME_D		,&l_p5_azs_ark1cw			, 712-OFFX,  729-OFFY,icb_Ico          		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right1knb	,P5_KNB_ARK1RIGHT1_D_00		,&l_p5_cov_ark1volume		, 556-OFFX,  883-OFFY,icb_ark1right1knb		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left1knb	,P5_KNB_ARK1LEFT1_D_00		,&l_p5_glt_ark1right1knb	, 313-OFFX,  883-OFFY,icb_ark1left1knb		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1righthnd	,P5_GLT_ARK1RIGHTHANDLE_D_00,&l_p5_glt_ark1left1knb	, 632-OFFX,  844-OFFY,icb_ark1righthnd 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1lefthnd		,P5_GLT_ARK1LEFTHANDLE_D_00	,&l_p5_glt_ark1righthnd	, 379-OFFX,  844-OFFY,icb_ark1lefthnd 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1right		,P5_COV_ARK1RIGHT_D			,&l_p5_glt_ark1lefthnd		, 618-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1left		,P5_COV_ARK1LEFT_D			,&l_p5_cov_ark1right		, 374-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right1		,P5_GLT_ARK1RIGHT1_D_00		,&l_p5_cov_ark1left		, 664-OFFX,  871-OFFY,icb_ark1right1 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left1		,P5_GLT_ARK1LEFT1_D_00		,&l_p5_glt_ark1right1		, 420-OFFX,  871-OFFY,icb_ark1left1 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right10		,P5_GLT_ARK1RIGHT10_D_00	,&l_p5_glt_ark1left1		, 653-OFFX,  859-OFFY,icb_ark1right10		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left10		,P5_GLT_ARK1LEFT10_D_00		,&l_p5_glt_ark1right10		, 407-OFFX,  859-OFFY,icb_ark1left10		, 10*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1right100	,P5_GLT_ARK1RIGHT100_D_00	,&l_p5_glt_ark1left10		, 623-OFFX,  825-OFFY,icb_ark1right100		, 12*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1left100		,P5_GLT_ARK1LEFT100_D_00	,&l_p5_glt_ark1right100	, 380-OFFX,  825-OFFY,icb_ark1left100		, 12*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1			,P5_GLT_ARK1_D_00			,&l_p5_glt_ark1left100		, 511-OFFX,  709-OFFY,icb_ark1       		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_cov_ark1signal		,P5_COV_ARK1SIGNAL_D		,&l_p5_glt_ark1			, 413-OFFX,  721-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_NEEDLE2	(p5_ndl_ark1signalD		,P5_NDL_ARK1SIGNAL_D		,&l_p5_cov_ark1signal		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalD, tbl_ark1signal, 18  , p5_6)		  
MY_NEEDLE2	(p5_ndl_ark1signalP		,P5_NDL_ARK1SIGNAL_P		,&l_p5_ndl_ark1signalD		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalP, tbl_ark1signal, 18  , p5_6)		  
MY_ICON2	(p5_scl_ark1signal		,P5_SCL_ARK1SIGNAL_D		,&l_p5_ndl_ark1signalP		, 383-OFFX,  661-OFFY,icb_sclark1signal		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1a			,P5_TBL_ARK1A				,&l_p5_scl_ark1signal		, 274-OFFX,  732-OFFY,icb_ark1a       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1b			,P5_TBL_ARK1B				,&l_p5_tbl_ark1a			, 281-OFFX,  795-OFFY,icb_ark1b       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1c			,P5_TBL_ARK1C				,&l_p5_tbl_ark1b			, 295-OFFX,  855-OFFY,icb_ark1c       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1d			,P5_TBL_ARK1D				,&l_p5_tbl_ark1c			, 538-OFFX,  855-OFFY,icb_ark1d       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1e			,P5_TBL_ARK1E				,&l_p5_tbl_ark1d			, 532-OFFX,  834-OFFY,icb_ark1e       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_tbl_ark1f			,P5_TBL_ARK1F				,&l_p5_tbl_ark1e			, 512-OFFX,  685-OFFY,icb_ark1f       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_tbl_ark1g			,P5_TBL_ARK1G				,&l_p5_tbl_ark1f			, 705-OFFX,  721-OFFY,icb_ark1g       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_6Ico				,P5_BACKGROUND6_D			,&l_p5_tbl_ark1g			,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX	, p5_6) 
MY_STATIC2	(p5_6bg,p5_6_list		,P5_BACKGROUND6_D			,&l_p5_6Ico				,	 p5_6);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p5_swt_ark1dir			,P5_SWT_ARK1DIR_D_00		,NULL						, 643-OFFX,  681-OFFY,icb_ark1dir     		, 3 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_azs_ark1cw			,P5_AZS_ARK1_CW_D_00		,&l_p5_swt_ark1dir			, 278-OFFX,  764-OFFY,icb_ark1cw      		, 2 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1volume		,P5_COV_ARK1VOLUME_D		,&l_p5_azs_ark1cw			, 712-OFFX,  729-OFFY,icb_Ico          		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right1knb	,P5_KNB_ARK1RIGHT1_D_00		,&l_p5_cov_ark1volume		, 556-OFFX,  883-OFFY,icb_ark1right1knb		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left1knb	,P5_KNB_ARK1LEFT1_D_00		,&l_p5_glt_ark1right1knb	, 313-OFFX,  883-OFFY,icb_ark1left1knb		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1righthnd	,P5_GLT_ARK1RIGHTHANDLE_D_00,&l_p5_glt_ark1left1knb	, 632-OFFX,  844-OFFY,icb_ark1righthnd 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1lefthnd		,P5_GLT_ARK1LEFTHANDLE_D_00	,&l_p5_glt_ark1righthnd	, 379-OFFX,  844-OFFY,icb_ark1lefthnd 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1right		,P5_COV_ARK1RIGHT_D			,&l_p5_glt_ark1lefthnd		, 618-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1left		,P5_COV_ARK1LEFT_D			,&l_p5_cov_ark1right		, 374-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right1		,P5_GLT_ARK1RIGHT1_D_00		,&l_p5_cov_ark1left		, 664-OFFX,  871-OFFY,icb_ark1right1 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left1		,P5_GLT_ARK1LEFT1_D_00		,&l_p5_glt_ark1right1		, 420-OFFX,  871-OFFY,icb_ark1left1 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right10		,P5_GLT_ARK1RIGHT10_D_00	,&l_p5_glt_ark1left1		, 653-OFFX,  859-OFFY,icb_ark1right10		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left10		,P5_GLT_ARK1LEFT10_D_00		,&l_p5_glt_ark1right10		, 407-OFFX,  859-OFFY,icb_ark1left10		, 10*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1right100	,P5_GLT_ARK1RIGHT100_D_00	,&l_p5_glt_ark1left10		, 623-OFFX,  825-OFFY,icb_ark1right100		, 12*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1left100		,P5_GLT_ARK1LEFT100_D_00	,&l_p5_glt_ark1right100	, 380-OFFX,  825-OFFY,icb_ark1left100		, 12*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1			,P5_GLT_ARK1_D_00			,&l_p5_glt_ark1left100		, 511-OFFX,  709-OFFY,icb_ark1       		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_cov_ark1signal		,P5_COV_ARK1SIGNAL_D		,&l_p5_glt_ark1			, 413-OFFX,  721-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_NEEDLE2	(p5_ndl_ark1signalD		,P5_NDL_ARK1SIGNAL_D		,&l_p5_cov_ark1signal		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalD, tbl_ark1signal, 18  , p5_6)		  
MY_NEEDLE2	(p5_ndl_ark1signalR		,P5_NDL_ARK1SIGNAL_R		,&l_p5_ndl_ark1signalD		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalR, tbl_ark1signal, 18  , p5_6)		  
MY_ICON2	(p5_scl_ark1signal		,P5_SCL_ARK1SIGNAL_D		,&l_p5_ndl_ark1signalR		, 383-OFFX,  661-OFFY,icb_sclark1signal		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1a			,P5_TBL_ARK1A				,&l_p5_scl_ark1signal		, 274-OFFX,  732-OFFY,icb_ark1a       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1b			,P5_TBL_ARK1B				,&l_p5_tbl_ark1a			, 281-OFFX,  795-OFFY,icb_ark1b       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1c			,P5_TBL_ARK1C				,&l_p5_tbl_ark1b			, 295-OFFX,  855-OFFY,icb_ark1c       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1d			,P5_TBL_ARK1D				,&l_p5_tbl_ark1c			, 538-OFFX,  855-OFFY,icb_ark1d       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1e			,P5_TBL_ARK1E				,&l_p5_tbl_ark1d			, 532-OFFX,  834-OFFY,icb_ark1e       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_tbl_ark1f			,P5_TBL_ARK1F				,&l_p5_tbl_ark1e			, 512-OFFX,  685-OFFY,icb_ark1f       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_tbl_ark1g			,P5_TBL_ARK1G				,&l_p5_tbl_ark1f			, 705-OFFX,  721-OFFY,icb_ark1g       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_6Ico				,P5_BACKGROUND6_D			,&l_p5_tbl_ark1g			,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX	, p5_6) 
MY_STATIC2	(p5_6bg,p5_6_list		,P5_BACKGROUND6_D			,&l_p5_6Ico				,	 p5_6);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p5_swt_ark1dir			,P5_SWT_ARK1DIR_D_00		,NULL						, 643-OFFX,  681-OFFY,icb_ark1dir     		, 3 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_azs_ark1cw			,P5_AZS_ARK1_CW_D_00		,&l_p5_swt_ark1dir			, 278-OFFX,  764-OFFY,icb_ark1cw      		, 2 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1volume		,P5_COV_ARK1VOLUME_D		,&l_p5_azs_ark1cw			, 712-OFFX,  729-OFFY,icb_Ico          		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right1knb	,P5_KNB_ARK1RIGHT1_D_00		,&l_p5_cov_ark1volume		, 556-OFFX,  883-OFFY,icb_ark1right1knb		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left1knb	,P5_KNB_ARK1LEFT1_D_00		,&l_p5_glt_ark1right1knb	, 313-OFFX,  883-OFFY,icb_ark1left1knb		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1righthnd	,P5_GLT_ARK1RIGHTHANDLE_D_00,&l_p5_glt_ark1left1knb	, 632-OFFX,  844-OFFY,icb_ark1righthnd 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1lefthnd		,P5_GLT_ARK1LEFTHANDLE_D_00	,&l_p5_glt_ark1righthnd	, 379-OFFX,  844-OFFY,icb_ark1lefthnd 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1right		,P5_COV_ARK1RIGHT_D			,&l_p5_glt_ark1lefthnd		, 618-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_cov_ark1left		,P5_COV_ARK1LEFT_D			,&l_p5_cov_ark1right		, 374-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right1		,P5_GLT_ARK1RIGHT1_D_00		,&l_p5_cov_ark1left		, 664-OFFX,  871-OFFY,icb_ark1right1 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left1		,P5_GLT_ARK1LEFT1_D_00		,&l_p5_glt_ark1right1		, 420-OFFX,  871-OFFY,icb_ark1left1 		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1right10		,P5_GLT_ARK1RIGHT10_D_00	,&l_p5_glt_ark1left1		, 653-OFFX,  859-OFFY,icb_ark1right10		, 10*PANEL_LIGHT_MAX	, p5_6)	
MY_ICON2	(p5_glt_ark1left10		,P5_GLT_ARK1LEFT10_D_00		,&l_p5_glt_ark1right10		, 407-OFFX,  859-OFFY,icb_ark1left10		, 10*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1right100	,P5_GLT_ARK1RIGHT100_D_00	,&l_p5_glt_ark1left10		, 623-OFFX,  825-OFFY,icb_ark1right100		, 12*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1left100		,P5_GLT_ARK1LEFT100_D_00	,&l_p5_glt_ark1right100	, 380-OFFX,  825-OFFY,icb_ark1left100		, 12*PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_glt_ark1			,P5_GLT_ARK1_D_00			,&l_p5_glt_ark1left100		, 511-OFFX,  709-OFFY,icb_ark1       		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_cov_ark1signal		,P5_COV_ARK1SIGNAL_D		,&l_p5_glt_ark1			, 413-OFFX,  721-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_NEEDLE2	(p5_ndl_ark1signalD		,P5_NDL_ARK1SIGNAL_D		,&l_p5_cov_ark1signal		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalD, tbl_ark1signal, 18  , p5_6)		  
MY_NEEDLE2	(p5_ndl_ark1signalM		,P5_NDL_ARK1SIGNAL_M		,&l_p5_ndl_ark1signalD		, 447-OFFX,  724-OFFY, 4, 7,icb_ndlark1signalM, tbl_ark1signal, 18  , p5_6)		  
MY_ICON2	(p5_scl_ark1signal		,P5_SCL_ARK1SIGNAL_D		,&l_p5_ndl_ark1signalM		, 383-OFFX,  661-OFFY,icb_sclark1signal		, 4 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1a			,P5_TBL_ARK1A				,&l_p5_scl_ark1signal		, 274-OFFX,  732-OFFY,icb_ark1a       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1b			,P5_TBL_ARK1B				,&l_p5_tbl_ark1a			, 281-OFFX,  795-OFFY,icb_ark1b       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1c			,P5_TBL_ARK1C				,&l_p5_tbl_ark1b			, 295-OFFX,  855-OFFY,icb_ark1c       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1d			,P5_TBL_ARK1D				,&l_p5_tbl_ark1c			, 538-OFFX,  855-OFFY,icb_ark1d       		, 1 *PANEL_LIGHT_MAX	, p5_6)		
MY_ICON2	(p5_tbl_ark1e			,P5_TBL_ARK1E				,&l_p5_tbl_ark1d			, 532-OFFX,  834-OFFY,icb_ark1e       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_tbl_ark1f			,P5_TBL_ARK1F				,&l_p5_tbl_ark1e			, 512-OFFX,  685-OFFY,icb_ark1f       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_tbl_ark1g			,P5_TBL_ARK1G				,&l_p5_tbl_ark1f			, 705-OFFX,  721-OFFY,icb_ark1g       		, 1 *PANEL_LIGHT_MAX	, p5_6)			
MY_ICON2	(p5_6Ico				,P5_BACKGROUND6_D			,&l_p5_tbl_ark1g			,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX	, p5_6) 
MY_STATIC2	(p5_6bg,p5_6_list		,P5_BACKGROUND6_D			,&l_p5_6Ico				,	 p5_6);
#endif

#endif
