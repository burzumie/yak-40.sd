/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	640
#define OFFY	989

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_firebay1    	,SHOW_LMP(LMP_ENG1_BAY_ON_FIRE		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_firebay2    	,SHOW_LMP(LMP_ENG2_BAY_ON_FIRE		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_firebay3    	,SHOW_LMP(LMP_ENG3_BAY_ON_FIRE		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireextbay1   	,SHOW_LMP(LMP_EXTING1_VALVE_OPEN1	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireextbay2   	,SHOW_LMP(LMP_EXTING2_VALVE_OPEN1	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireextbay3   	,SHOW_LMP(LMP_EXTING3_VALVE_OPEN1	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireeng1    	,SHOW_LMP(LMP_ENG1_ON_FIRE			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireeng2    	,SHOW_LMP(LMP_ENG2_ON_FIRE			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireeng3    	,SHOW_LMP(LMP_ENG3_ON_FIRE			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireexteng1    	,SHOW_LMP(LMP_EXTING1_VALVE_OPEN2	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireexteng2    	,SHOW_LMP(LMP_EXTING2_VALVE_OPEN2	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireexteng3    	,SHOW_LMP(LMP_EXTING3_VALVE_OPEN2	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_crs_02			,SHOW_POSM(POS_CRS5_02,1,0))

static MAKE_TCB(tcb_01	,LMP_TT(LMP_ENG1_BAY_ON_FIRE		))
static MAKE_TCB(tcb_02	,LMP_TT(LMP_ENG2_BAY_ON_FIRE     	))
static MAKE_TCB(tcb_03	,LMP_TT(LMP_ENG3_BAY_ON_FIRE     	))
static MAKE_TCB(tcb_04	,LMP_TT(LMP_ENG1_ON_FIRE     	    ))
static MAKE_TCB(tcb_05	,LMP_TT(LMP_ENG2_ON_FIRE     	    ))
static MAKE_TCB(tcb_06	,LMP_TT(LMP_ENG3_ON_FIRE     	    ))
static MAKE_TCB(tcb_07	,LMP_TT(LMP_EXTING1_VALVE_OPEN1	    ))
static MAKE_TCB(tcb_08	,LMP_TT(LMP_EXTING2_VALVE_OPEN1	    ))
static MAKE_TCB(tcb_09	,LMP_TT(LMP_EXTING3_VALVE_OPEN1	    ))
static MAKE_TCB(tcb_10	,LMP_TT(LMP_EXTING1_VALVE_OPEN2	    ))
static MAKE_TCB(tcb_11	,LMP_TT(LMP_EXTING2_VALVE_OPEN2	    ))
static MAKE_TCB(tcb_12	,LMP_TT(LMP_EXTING3_VALVE_OPEN2	    ))
static MAKE_TCB(tcb_13	,POS_TT(POS_CRS5_02	                ))

// �������
static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p0_02			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_02,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P5);
		panel_window_open_ident(IDENT_P4);
	}
	return true;
}

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	) 
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p5_9,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P5_BACKGROUND9_D_SX,P5_BACKGROUND9_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

// �������  
MOUSE_TBOX( "13", 810-OFFX, 1120-OFFY,105,73,CURSOR_HAND,MOUSE_MLR,mcb_crs_p0_02)

MOUSE_TTPB( "1",  640-OFFX,1018-OFFY, 70,90)    // LMP_ENG1_BAY_ON_FIRE     	   
MOUSE_TTPB( "2",  709-OFFX,1018-OFFY, 70,90)    // LMP_ENG2_BAY_ON_FIRE     	   
MOUSE_TTPB( "3",  774-OFFX,1018-OFFY, 70,90)    // LMP_ENG3_BAY_ON_FIRE     	   
MOUSE_TTPB( "4",  888-OFFX,1018-OFFY, 70,90)    // LMP_ENG1_ON_FIRE     	       
MOUSE_TTPB( "5",  953-OFFX,1018-OFFY, 70,90)    // LMP_ENG2_ON_FIRE     	       
MOUSE_TTPB( "6", 1016-OFFX,1018-OFFY, 70,90)    // LMP_ENG3_ON_FIRE     	       
MOUSE_TTPB( "7",  640-OFFX,1112-OFFY, 70,90)    // LMP_EXTING1_VALVE_OPEN1	   
MOUSE_TTPB( "8",  709-OFFX,1112-OFFY, 70,90)    // LMP_EXTING2_VALVE_OPEN1	   
MOUSE_TTPB( "9",  774-OFFX,1112-OFFY, 70,90)    // LMP_EXTING3_VALVE_OPEN1	   
MOUSE_TTPB("10",  888-OFFX,1112-OFFY, 70,90)    // LMP_EXTING1_VALVE_OPEN2	   
MOUSE_TTPB("11",  953-OFFX,1112-OFFY, 70,90)    // LMP_EXTING2_VALVE_OPEN2	   
MOUSE_TTPB("12", 1016-OFFX,1112-OFFY, 70,90)    // LMP_EXTING3_VALVE_OPEN2	   
MOUSE_END       

extern PELEMENT_HEADER p5_9_list; 
GAUGE_HEADER_FS700_EX(P5_BACKGROUND9_D_SX, "p5_9", &p5_9_list, rect_p5_9, 0, 0, 0, 0, p5_9); 

MY_ICON2	(p5_crs_02			,MISC_CRS_P5_02			,NULL					, 810-OFFX, 1120-OFFY,icb_crs_02		,1					, p5_9)	
MY_ICON2	(p5_lmp_firebay1	,P5_LMP_FIREBAY1_D_00	,&l_p5_crs_02			, 640-OFFX, 1018-OFFY,icb_firebay1    	, 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_firebay2	,P5_LMP_FIREBAY2_D_00	,&l_p5_lmp_firebay1		, 709-OFFX, 1018-OFFY,icb_firebay2    	, 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_firebay3	,P5_LMP_FIREBAY3_D_00	,&l_p5_lmp_firebay2		, 774-OFFX, 1018-OFFY,icb_firebay3    	, 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_fireextbay1	,P5_LMP_FIREEXTBAY1_D_00,&l_p5_lmp_firebay3		, 640-OFFX, 1112-OFFY,icb_fireextbay1  	, 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_fireextbay2	,P5_LMP_FIREEXTBAY2_D_00,&l_p5_lmp_fireextbay1	, 709-OFFX, 1112-OFFY,icb_fireextbay2  	, 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_fireextbay3	,P5_LMP_FIREEXTBAY3_D_00,&l_p5_lmp_fireextbay2	, 774-OFFX, 1112-OFFY,icb_fireextbay3  	, 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_fireeng1	,P5_LMP_FIREENG1_D_00	,&l_p5_lmp_fireextbay3	, 888-OFFX, 1018-OFFY,icb_fireeng1    	, 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_fireeng2	,P5_LMP_FIREENG2_D_00	,&l_p5_lmp_fireeng1		, 953-OFFX, 1018-OFFY,icb_fireeng2    	, 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_fireeng3	,P5_LMP_FIREENG3_D_00	,&l_p5_lmp_fireeng2		,1016-OFFX, 1018-OFFY,icb_fireeng3    	, 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_fireexteng1	,P5_LMP_FIREEXTENG1_D_00,&l_p5_lmp_fireeng3		, 888-OFFX, 1112-OFFY,icb_fireexteng1   , 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_fireexteng2	,P5_LMP_FIREEXTENG2_D_00,&l_p5_lmp_fireexteng1	, 953-OFFX, 1112-OFFY,icb_fireexteng2   , 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_lmp_fireexteng3	,P5_LMP_FIREEXTENG3_D_00,&l_p5_lmp_fireexteng2	,1016-OFFX, 1112-OFFY,icb_fireexteng3   , 3*PANEL_LIGHT_MAX	, p5_9)
MY_ICON2	(p5_9Ico			,P5_BACKGROUND9_D		,&l_p5_lmp_fireexteng3	,		 0,			0,icb_Ico			,PANEL_LIGHT_MAX	, p5_9) 
MY_STATIC2	(p5_9bg,p5_9_list	,P5_BACKGROUND9_D		,&l_p5_9Ico				,	 p5_9);
#endif
