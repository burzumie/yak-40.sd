/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	815
#define OFFY	276

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_azsvhf2on  		,SHOW_AZS(AZS_VHF2ON,2));
static MAKE_ICB(icb_covvh2on		,if(!PWR_GET(PWR_COM2)){return -1;} else {CHK_BRT(); return POS_GET(POS_PANEL_STATE); });
static MAKE_ICB(icb_sclvhf2leftoff  ,if(!PWR_GET(PWR_COM2)){SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF2LEFT  )+POS_GET(POS_PANEL_STATE)*18;}else{HIDE_IMAGE(pelement);} return -1;);
static MAKE_ICB(icb_sclvhf2lefton   ,if(!PWR_GET(PWR_COM2)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF2LEFT  )+POS_GET(POS_PANEL_STATE)*18;} return -1;);
static MAKE_ICB(icb_sclvhf2midoff   ,if(!PWR_GET(PWR_COM2)){SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF2MID)+POS_GET(POS_PANEL_STATE)*10;}else{HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_sclvhf2midon    ,if(!PWR_GET(PWR_COM2)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF2MID)+POS_GET(POS_PANEL_STATE)*10;} return 0;);
static MAKE_ICB(icb_sclvhf2rightoff ,if(!PWR_GET(PWR_COM2)){SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF2RIGHT)+POS_GET(POS_PANEL_STATE)*4;}else{HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_sclvhf2righton  ,if(!PWR_GET(PWR_COM2)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF2RIGHT)+POS_GET(POS_PANEL_STATE)*4;} return 0;);
static MAKE_ICB(icb_knbvhf2righton  ,if(!PWR_GET(PWR_COM2)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_KNBVHF2RIGHT )+POS_GET(POS_PANEL_STATE)*20;} return 0;);
static MAKE_ICB(icb_knbvhf2lefton   ,if(!PWR_GET(PWR_COM2)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_KNBVHF2LEFT  )+POS_GET(POS_PANEL_STATE)*20;} return 0;);
static MAKE_ICB(icb_covvhf2volon    ,if(!PWR_GET(PWR_COM2)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_PANEL_STATE);});
static MAKE_ICB(icb_knbvhf2rightoff ,if(!PWR_GET(PWR_COM2)){SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_KNBVHF2RIGHT )+POS_GET(POS_PANEL_STATE)*20;}else{HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_knbvhf2leftoff  ,if(!PWR_GET(PWR_COM2)){SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_KNBVHF2LEFT  )+POS_GET(POS_PANEL_STATE)*20;}else{HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_covvhf2voloff   ,if(!PWR_GET(PWR_COM2)){SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_PANEL_STATE);}else{HIDE_IMAGE(pelement); return -1;});
static MAKE_ICB(icb_ark2leftright	,if(!PWR_GET(PWR_COM2)){SHOW_IMAGE(pelement); CHK_BRT(); return AZS_GET(AZS_ARK2LEFTRIGHT)+POS_GET(POS_PANEL_STATE)*2;} else {HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_ark2leftrighton	,if(!PWR_GET(PWR_COM2)){HIDE_IMAGE(pelement);} else {SHOW_IMAGE(pelement); CHK_BRT(); return AZS_GET(AZS_ARK2LEFTRIGHT)+POS_GET(POS_PANEL_STATE)*2;} return 0;);

//////////////////////////////////////////////////////////////////////////

// ��������
static MAKE_MSCB(mcb_azs_vhf2on		    ,AZS_TGL(AZS_VHF2ON        ))
static MAKE_MSCB(mcb_ark2leftright	    ,AZS_TGL(AZS_ARK2LEFTRIGHT ))

static BOOL FSAPI mcb_glt_vhf2left1_l	(PPIXPOINT relative_point,FLAGS32 mouse_flags)	
{
	trigger_key_event(KEY_COM2_RADIO_WHOLE_DEC,0);
	HND_DEC(HND_KNBVHF2LEFT);
	return TRUE;
}

static BOOL FSAPI mcb_glt_vhf2left1_r	(PPIXPOINT relative_point,FLAGS32 mouse_flags)	
{
	trigger_key_event(KEY_COM2_RADIO_WHOLE_INC,0);
	HND_INC(HND_KNBVHF2LEFT);
	return true;
}

static BOOL FSAPI mcb_glt_vhf2right1_l	(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	trigger_key_event(KEY_COM2_RADIO_FRACT_DEC,0);
	HND_DEC(HND_KNBVHF2RIGHT);
	return TRUE; 
}

static BOOL FSAPI mcb_glt_vhf2right1_r	(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	trigger_key_event(KEY_COM2_RADIO_FRACT_INC,0);
	HND_INC(HND_KNBVHF2RIGHT);
	return TRUE; 
}

static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_VHF2ON                  ))
static MAKE_TCB(tcb_02	,HND_TT(HND_KNBVHF1RIGHT            ))
static MAKE_TCB(tcb_03	,HND_TT(HND_KNBVHF1LEFT             ))
static MAKE_TCB(tcb_04	,AZS_TT(AZS_ARK2LEFTRIGHT           ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p5_5,HELP_NONE,0,0)

MOUSE_PBOX(0,0,P5_BACKGROUND5_D_SX,P5_BACKGROUND5_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

// �������� 
//MOUSE_TBOX(  "1",  902-OFFX,  463-OFFY, 42, 69,CURSOR_HAND,MOUSE_LR,mcb_azs_vhf2on)
MOUSE_TBOX( "4", 1397-OFFX,  500-OFFY,117, 76,CURSOR_HAND,MOUSE_LR,mcb_ark2leftright)

// ���������
MOUSE_TSHB( "2", 1235-OFFX,  455-OFFY, 80, 55,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_vhf2right1_l,mcb_glt_vhf2right1_r)
MOUSE_TSHB( "3",  955-OFFX,  455-OFFY, 80, 55,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_vhf2left1_l,mcb_glt_vhf2left1_r)

MOUSE_END       


//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p5_5_list; 
GAUGE_HEADER_FS700_EX(P5_BACKGROUND5_D_SX, "p5_5", &p5_5_list, rect_p5_5, 0, 0, 0, 0, p5_5); 

MY_ICON2	(p5_azs_ark2leftright	,P5_AZS_ARK2LEFTRIGHT_D_00	,NULL                   	,1363-OFFX,  500-OFFY,icb_ark2leftright		,2 *PANEL_LIGHT_MAX	, p5_5)	
MY_ICON2	(p5_azs_ark2leftrighton	,P5_AZS_ARK2LEFTRIGHTON_D_00,&l_p5_azs_ark2leftright	,1363-OFFX,  500-OFFY,icb_ark2leftrighton	,2 *PANEL_LIGHT_MAX	, p5_5)	
MY_ICON2	(p5_scl_vhf2righton		,P5_SCL_VHF2RIGHTON_D_00	,&l_p5_azs_ark2leftrighton	, 1165-OFFX,  397-OFFY,icb_sclvhf2righton	,4 *PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_scl_vhf2rightoff	,P5_SCL_VHF2RIGHTOFF_D_00	,&l_p5_scl_vhf2righton	, 1165-OFFX,  397-OFFY,icb_sclvhf2rightoff	,4 *PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_scl_vhf2midon		,P5_SCL_VHF2MIDON_D_00		,&l_p5_scl_vhf2rightoff	, 1139-OFFX,  397-OFFY,icb_sclvhf2midon		,10*PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_scl_vhf2midoff		,P5_SCL_VHF2MIDOFF_D_00		,&l_p5_scl_vhf2midon	, 1139-OFFX,  397-OFFY,icb_sclvhf2midoff	,10*PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_cov_vhf2volumeon	,P5_COV_VHF2VOLUMEON_D		,&l_p5_scl_vhf2midoff	,  895-OFFX,  384-OFFY,icb_covvhf2volon		,4 *PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_knb_vhf2righton		,P5_KNB_VHF2RIGHTON_D_00	,&l_p5_cov_vhf2volumeon	, 1246-OFFX,  453-OFFY,icb_knbvhf2righton	,20*PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_knb_vhf2lefton		,P5_KNB_VHF2LEFTON_D_00		,&l_p5_knb_vhf2righton	,  964-OFFX,  453-OFFY,icb_knbvhf2lefton	,20*PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_cov_vhf2volumeoff	,P5_COV_VHF2VOLUMEOFF_D		,&l_p5_knb_vhf2lefton	,  895-OFFX,  384-OFFY,icb_covvhf2voloff 	,4 *PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_knb_vhf2rightoff	,P5_KNB_VHF2RIGHTOFF_D_00	,&l_p5_cov_vhf2volumeoff, 1246-OFFX,  453-OFFY,icb_knbvhf2rightoff	,20*PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_knb_vhf2leftoff		,P5_KNB_VHF2LEFTOFF_D_00	,&l_p5_knb_vhf2rightoff	,  964-OFFX,  453-OFFY,icb_knbvhf2leftoff	,20*PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_scl_vhf2lefton		,P5_SCL_VHF2LEFTON_D_00		,&l_p5_knb_vhf2leftoff	, 1054-OFFX,  397-OFFY,icb_sclvhf2lefton	,18*PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_scl_vhf2leftoff		,P5_SCL_VHF2LEFTOFF_D_00	,&l_p5_scl_vhf2lefton	, 1054-OFFX,  397-OFFY,icb_sclvhf2leftoff	,18*PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_azs_vhf2on			,P5_AZS_VHF2ON_D_00			,&l_p5_scl_vhf2leftoff	,  902-OFFX,  463-OFFY,icb_azsvhf2on  		,4 *PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_cov_vhf2on			,P5_COV_VHF2ON_D			,&l_p5_azs_vhf2on		,  815-OFFX,  276-OFFY,icb_covvh2on			,4 *PANEL_LIGHT_MAX	, p5_5)
MY_ICON2	(p5_5Ico				,P5_BACKGROUND5_D			,&l_p5_cov_vhf2on		,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX	, p5_5) 
MY_STATIC2	(p5_5bg,p5_5_list		,P5_BACKGROUND5_D			,&l_p5_5Ico				,	 p5_5);


#endif
