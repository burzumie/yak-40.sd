/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	0
#define OFFY	0

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_llightleft		,SHOW_AZS(AZS_LLIGHTLEFT   ,3));	
static MAKE_ICB(icb_llightright		,SHOW_AZS(AZS_LLIGHTRIGHT  ,3));
static MAKE_ICB(icb_llightsmotor	,SHOW_AZS(AZS_LLIGHTSMOTOR ,2));	
static MAKE_ICB(icb_lightsnav		,SHOW_AZS(AZS_LIGHTSNAV    ,2));
static MAKE_ICB(icb_lightsbcn		,SHOW_AZS(AZS_LIGHTSBCN    ,2));

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_llightleft_u	    ,AZS_SET(AZS_LLIGHTLEFT    ,2))
static MAKE_MSCB(mcb_llightleft_c	    ,AZS_SET(AZS_LLIGHTLEFT    ,0))
static MAKE_MSCB(mcb_llightleft_d	    ,AZS_SET(AZS_LLIGHTLEFT    ,1))
static MAKE_MSCB(mcb_llightright_u	    ,AZS_SET(AZS_LLIGHTRIGHT   ,2))
static MAKE_MSCB(mcb_llightright_c	    ,AZS_SET(AZS_LLIGHTRIGHT   ,0))
static MAKE_MSCB(mcb_llightright_d	    ,AZS_SET(AZS_LLIGHTRIGHT   ,1))
static MAKE_MSCB(mcb_llightsmotor	    ,AZS_TGL(AZS_LLIGHTSMOTOR  ))
static MAKE_MSCB(mcb_lightsnav		    ,AZS_TGL(AZS_LIGHTSNAV     ))
static MAKE_MSCB(mcb_lightsbcn		    ,AZS_TGL(AZS_LIGHTSBCN     ))

static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static BOOL FSAPI mcb_lgt_close			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_close_ident(IDENT_P2);
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_LLIGHTLEFT              ))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_LLIGHTRIGHT             ))
static MAKE_TCB(tcb_03	,AZS_TT(AZS_LLIGHTSMOTOR            ))
static MAKE_TCB(tcb_04	,AZS_TT(AZS_LIGHTSNAV               ))
static MAKE_TCB(tcb_05	,AZS_TT(AZS_LIGHTSBCN               ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MOUSE_TOOLTIP_ARGS_END


MOUSE_BEGIN(rect_p5_1,HELP_NONE,0,0)
//MOUSE_PBOX(0,0,P5_BACKGROUND1_D_SX,P5_BACKGROUND1_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
#ifndef ONLY3D
MOUSE_PBOX(30,30,P5_BACKGROUND1_D_SX-30,P5_BACKGROUND1_D_SY-30,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_lgt_close)
#else
MOUSE_PBOX(30,30,P53D_BACKGROUND1_D_SX-30,P53D_BACKGROUND1_D_SY-30,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_lgt_close)
#endif
MOUSE_TSV3(  "1",  234-OFFX,  101-OFFY, 69,194,CURSOR_UPARROW,CURSOR_HAND,CURSOR_DOWNARROW,MOUSE_LR,mcb_llightleft_u,mcb_llightleft_c,mcb_llightleft_d)
MOUSE_TSV3(  "2",  303-OFFX,  101-OFFY, 69,194,CURSOR_UPARROW,CURSOR_HAND,CURSOR_DOWNARROW,MOUSE_LR,mcb_llightright_u,mcb_llightright_c,mcb_llightright_d)
MOUSE_TBOX(  "3",  372-OFFX,   68-OFFY, 97,194,CURSOR_HAND,MOUSE_LR,mcb_llightsmotor)
MOUSE_TBOX(  "4",  469-OFFX,   68-OFFY, 76,194,CURSOR_HAND,MOUSE_LR,mcb_lightsnav)
MOUSE_TBOX(  "5",  545-OFFX,   68-OFFY, 95,194,CURSOR_HAND,MOUSE_LR,mcb_lightsbcn)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p5_1_list; 
#ifndef ONLY3D
GAUGE_HEADER_FS700_EX(P5_BACKGROUND1_D_SX, "p5_1", &p5_1_list, rect_p5_1, 0, 0, 0, 0, p5_1); 
#else
GAUGE_HEADER_FS700_EX(P53D_BACKGROUND1_D_SX, "p5_1", &p5_1_list, rect_p5_1, 0, 0, 0, 0, p5_1); 
#endif

#ifndef ONLY3D

MY_ICON2	(p5_azs_llightleft	,P5_AZS_LLIGHTLEFT_D_00		,NULL					, 234-OFFX,   68-OFFY,icb_llightleft	,3*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_azs_llightright	,P5_AZS_LLIGHTRIGHT_D_00	,&l_p5_azs_llightleft	, 303-OFFX,   68-OFFY,icb_llightright	,3*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_azs_llightsmotor,P5_AZS_LLIGHTSMOTOR_D_00	,&l_p5_azs_llightright	, 372-OFFX,   68-OFFY,icb_llightsmotor	,2*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_azs_lightsnav	,P5_AZS_LIGHTSNAV_D_00		,&l_p5_azs_llightsmotor	, 469-OFFX,   68-OFFY,icb_lightsnav		,2*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_azs_lightsbcn	,P5_AZS_LIGHTSBCN_D_00		,&l_p5_azs_lightsnav	, 545-OFFX,   68-OFFY,icb_lightsbcn		,2*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_1Ico			,P5_BACKGROUND1_D			,&l_p5_azs_lightsbcn	,		 0,			0,icb_Ico			,PANEL_LIGHT_MAX	, p5_1) 
MY_STATIC2	(p5_1bg,p5_1_list	,P5_BACKGROUND1_D			,&l_p5_1Ico				, p5_1);

#else

MY_ICON2	(p5_azs_llightleft	,P53D_AZS_LLIGHTLEFT_D_00	,NULL					, 234-OFFX,   68-OFFY,icb_llightleft	,3*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_azs_llightright	,P53D_AZS_LLIGHTRIGHT_D_00	,&l_p5_azs_llightleft	, 303-OFFX,   68-OFFY,icb_llightright	,3*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_azs_llightsmotor,P53D_AZS_LLIGHTSMOTOR_D_00	,&l_p5_azs_llightright	, 372-OFFX,   68-OFFY,icb_llightsmotor	,2*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_azs_lightsnav	,P53D_AZS_LIGHTSNAV_D_00	,&l_p5_azs_llightsmotor	, 469-OFFX,   68-OFFY,icb_lightsnav		,2*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_azs_lightsbcn	,P53D_AZS_LIGHTSBCN_D_00	,&l_p5_azs_lightsnav	, 545-OFFX,   68-OFFY,icb_lightsbcn		,2*PANEL_LIGHT_MAX	, p5_1) 	
MY_ICON2	(p5_1Ico			,P53D_BACKGROUND1_D			,&l_p5_azs_lightsbcn	,		 0,			0,icb_Ico			,PANEL_LIGHT_MAX	, p5_1) 
MY_STATIC2	(p5_1bg,p5_1_list	,P53D_BACKGROUND1_D			,&l_p5_1Ico				, p5_1);

#endif
