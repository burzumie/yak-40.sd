/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"

#define COvcComScl(N,I,XI,YI,I1,XI1,YI1,I2,XI2,YI2,I3,XI3,YI3,IL,XIL,YIL,ILI1,XILI1,YILI1,ILI2,XILI2,YILI2,ILI3,XILI3,YILI3,ISA,XISA,YISA,ISB,XISB,YISB,ISC,XISC,YISC,ISAL,XISAL,YISAL,ISBL,XISBL,YISBL,ISCL,XISCL,YISCL,PWRCOM,SCLL,SCLM,SCLR,G1,G2,HNDX1,HNDY1,HNDSX1,HNDSY1,HNDX2,HNDY2,HNDSX2,HNDSY2,KE1,KE2,KE3,KE4) /**/ \
	static double FSAPI icbscl_##N(PELEMENT_ICON pelement) { \
	if(PWRCOM) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(SCLR)+POS_GET(POS_PANEL_STATE)*4; \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbsbl_##N(PELEMENT_ICON pelement) { \
	if(PWRCOM) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(SCLM)+POS_GET(POS_PANEL_STATE)*10; \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbsal_##N(PELEMENT_ICON pelement) { \
	if(PWRCOM) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(SCLL)+POS_GET(POS_PANEL_STATE)*18; \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbsc_##N(PELEMENT_ICON pelement) { \
	if(!PWRCOM) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(SCLR)+POS_GET(POS_PANEL_STATE)*4; \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbsb_##N(PELEMENT_ICON pelement) { \
	if(!PWRCOM) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(SCLM)+POS_GET(POS_PANEL_STATE)*10; \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbsa_##N(PELEMENT_ICON pelement) { \
	if(!PWRCOM) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(SCLL)+POS_GET(POS_PANEL_STATE)*18; \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icble_##N(PELEMENT_ICON pelement) { \
	if(PWRCOM&&POS_GET(POS_PANEL_LANG)) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbl_##N(PELEMENT_ICON pelement) { \
	if(PWRCOM) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!PWRCOM&&POS_GET(POS_PANEL_LANG)) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1;\
	} \
	} \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	bool FSAPI mcb1_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	HND_DEC(G1); \
	trigger_key_event(KE1,0); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	HND_INC(G1); \
	trigger_key_event(KE2,0); \
	} \
	return TRUE; \
	}; \
	bool FSAPI mcb2_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	HND_DEC(G2); \
	trigger_key_event(KE3,0); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	HND_INC(G2); \
	trigger_key_event(KE4,0); \
	} \
	return TRUE; \
	}; \
	static MAKE_TCB(N##_tcb_01,return HND_TTGET(G1);) \
	static MAKE_TCB(N##_tcb_02,return HND_TTGET(G2);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", HNDX1, HNDY1, HNDSX1, HNDSY1,CURSOR_HAND,MOUSE_LR,mcb1_##N,N) \
	MOUSE_TBOX2("2", HNDX2, HNDY2, HNDSX2, HNDSY2,CURSOR_HAND,MOUSE_LR,mcb2_##N,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoSCL,ISCL,NULL          ,XISCL,YISCL,icbscl_##N,4*PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoSBL,ISBL,&l_##N##IcoSCL,XISBL,YISBL,icbsbl_##N,10*PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoSAL,ISAL,&l_##N##IcoSBL,XISAL,YISAL,icbsal_##N,18*PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoSC ,ISC ,&l_##N##IcoSAL,XISC ,YISC ,icbsc_##N ,4*PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoSB ,ISB ,&l_##N##IcoSC ,XISB ,YISB ,icbsb_##N ,10*PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoSA ,ISA ,&l_##N##IcoSB ,XISA ,YISA ,icbsa_##N ,18*PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoLE3,ILI3,&l_##N##IcoSA ,XILI3,YILI3,icble_##N ,PANEL_LIGHT_MAX , N); \
	MY_ICON2(N##IcoLE2,ILI2,&l_##N##IcoLE3,XILI2,YILI2,icble_##N ,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoLE1,ILI1,&l_##N##IcoLE2,XILI1,YILI1,icble_##N ,PANEL_LIGHT_MAX , N); \
	MY_ICON2(N##IcoL  ,IL  ,&l_##N##IcoLE1,XIL  ,YIL  ,icbl_##N  ,PANEL_LIGHT_MAX , N); \
	MY_ICON2(N##IcoE3 ,I3  ,&l_##N##IcoL  ,XI3  ,YI3  ,icbe_##N  ,PANEL_LIGHT_MAX , N); \
	MY_ICON2(N##IcoE2 ,I2  ,&l_##N##IcoE3 ,XI2  ,YI2  ,icbe_##N  ,PANEL_LIGHT_MAX , N); \
	MY_ICON2(N##IcoE1 ,I1  ,&l_##N##IcoE2 ,XI1  ,YI1  ,icbe_##N  ,PANEL_LIGHT_MAX , N); \
	MY_ICON2(N##Ico   ,I   ,&l_##N##IcoE1 ,XI   ,YI   ,icbr_##N  ,PANEL_LIGHT_MAX , N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define COvcAdfScl(N,I,XI,YI,I1,XI1,YI1,I2,XI2,YI2,I3,XI3,YI3,IL1,XIL1,YIL1,IL2,XIL2,YIL2,IL3,XIL3,YIL3,ILI1,XILI1,YILI1,ILI2,XILI2,YILI2,ILI3,XILI3,YILI3,ILS,XILS,YILS,NDL1D,NDL1M,NDL1P,NDL1R,NDL1LD,NDL1LM,NDL1LP,NDL1LR,NDLX,NDLY,NDLCX,NDLCY,TBLN,PWRADF,NDLAPI,G,GX,GY,GXS,GYS,G2,H2,GX2,GY2,GXS2,GYS2,G3,H3,GX3,GY3,GXS3,GYS3,G22,H22,GX22,GY22,GXS22,GYS22,G32,H32,GX32,GY32,GXS32,GYS32) /**/ \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	GLT_TGL(G); \
	return TRUE; \
	} \
	bool FSAPI mcb_1##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(G2); GLT_DEC(H2); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(G2); GLT_INC(H2); \
	} \
	return TRUE; \
	} \
	bool FSAPI mcb_2##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(G3); GLT_DEC(H3); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(G3); GLT_INC(H3); \
	} \
	return TRUE; \
	} \
	bool FSAPI mcb_12##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(G22); GLT_DEC(H22); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(G22); GLT_INC(H22); \
	} \
	return TRUE; \
	} \
	bool FSAPI mcb_22##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(G32); GLT_DEC(H32); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(G32); GLT_INC(H32); \
	} \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return GLT_TTGET(G);) \
	static MAKE_TCB(N##_tcb_02,return GLT_TTGET(G2);) \
	static MAKE_TCB(N##_tcb_03,return GLT_TTGET(G3);) \
	static MAKE_TCB(N##_tcb_04,return GLT_TTGET(G22);) \
	static MAKE_TCB(N##_tcb_05,return GLT_TTGET(G32);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", GX, GY, GXS, GYS,CURSOR_HAND,MOUSE_LR,mcb_##N,N) \
	MOUSE_TBOX2("2", GX2, GY2, GXS2, GYS2,CURSOR_HAND,MOUSE_LR,mcb_1##N,N) \
	MOUSE_TBOX2("3", GX3, GY3, GXS3, GYS3,CURSOR_HAND,MOUSE_LR,mcb_2##N,N) \
	MOUSE_TBOX2("4", GX22, GY22, GXS22, GYS22,CURSOR_HAND,MOUSE_LR,mcb_12##N,N) \
	MOUSE_TBOX2("5", GX32, GY32, GXS32, GYS32,CURSOR_HAND,MOUSE_LR,mcb_22##N,N) \
	MOUSE_END \
	static double FSAPI icbs_##N(PELEMENT_ICON pelement) { \
	if(PWRADF) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icble_##N(PELEMENT_ICON pelement) { \
	if(PWRADF&&POS_GET(POS_PANEL_LANG)) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbl_##N(PELEMENT_ICON pelement) { \
	if(PWRADF&&!POS_GET(POS_PANEL_LANG)) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!PWRADF&&POS_GET(POS_PANEL_LANG)) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	double Needle##N(PELEMENT_NEEDLE pelement,int clr) { \
	if(POS_GET(POS_PANEL_STATE)==clr&&!PWRADF) { \
	SHOW_IMAGE(pelement); \
	return 0; \
	} \
	HIDE_IMAGE(pelement); \
	return 0; \
	};\
	double NeedleLit##N(PELEMENT_NEEDLE pelement,int clr) { \
	if(POS_GET(POS_PANEL_STATE)==clr&&PWRADF) {  \
	SHOW_IMAGE(pelement);\
	return NDL_GET(NDLAPI);\
	}\
	HIDE_IMAGE(pelement);\
	return 0;\
	};\
	static double FSAPI icbndld_##N(PELEMENT_NEEDLE pelement) { \
	return Needle##N(pelement,PANEL_LIGHT_DAY); \
	} \
	static double FSAPI icbndlm_##N(PELEMENT_NEEDLE pelement) { \
	return Needle##N(pelement,PANEL_LIGHT_MIX); \
	} \
	static double FSAPI icbndlp_##N(PELEMENT_NEEDLE pelement) { \
	return Needle##N(pelement,PANEL_LIGHT_PLF); \
	} \
	static double FSAPI icbndlr_##N(PELEMENT_NEEDLE pelement) { \
	return Needle##N(pelement,PANEL_LIGHT_RED); \
	} \
	static double FSAPI icbndlld_##N(PELEMENT_NEEDLE pelement) { \
	return NeedleLit##N(pelement,PANEL_LIGHT_DAY); \
	} \
	static double FSAPI icbndllm_##N(PELEMENT_NEEDLE pelement) { \
	return NeedleLit##N(pelement,PANEL_LIGHT_MIX); \
	} \
	static double FSAPI icbndllp_##N(PELEMENT_NEEDLE pelement) { \
	return NeedleLit##N(pelement,PANEL_LIGHT_PLF); \
	} \
	static double FSAPI icbndllr_##N(PELEMENT_NEEDLE pelement) { \
	return NeedleLit##N(pelement,PANEL_LIGHT_RED); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_NEEDLE2(N##NdllR ,NDL1LR,NULL          ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndllr_##N,TBLN,0,N); \
	MY_NEEDLE2(N##NdllP ,NDL1LP,&l_##N##NdllR ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndllp_##N,TBLN,0,N); \
	MY_NEEDLE2(N##NdllM ,NDL1LM,&l_##N##NdllP ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndllm_##N,TBLN,0,N); \
	MY_NEEDLE2(N##NdllD ,NDL1LD,&l_##N##NdllM ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndlld_##N,TBLN,0,N); \
	MY_NEEDLE2(N##NdlR  ,NDL1R ,&l_##N##NdllD ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndlr_##N ,TBLN,0,N); \
	MY_NEEDLE2(N##NdlP  ,NDL1P ,&l_##N##NdlR  ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndlp_##N ,TBLN,0,N); \
	MY_NEEDLE2(N##NdlM  ,NDL1M ,&l_##N##NdlP  ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndlm_##N ,TBLN,0,N); \
	MY_NEEDLE2(N##NdlD  ,NDL1D ,&l_##N##NdlM  ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndld_##N ,TBLN,0,N); \
	MY_ICON2  (N##IcoS  ,ILS   ,&l_##N##NdlD  ,XILS  ,YILS  ,icbs_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoLE3,ILI3  ,&l_##N##IcoS  ,XILI3 ,YILI3 ,icble_##N ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoLE2,ILI2  ,&l_##N##IcoLE3,XILI2 ,YILI2 ,icble_##N ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoLE1,ILI1  ,&l_##N##IcoLE2,XILI1 ,YILI1 ,icble_##N ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoE3 ,I3    ,&l_##N##IcoLE1,XI3   ,YI3   ,icbe_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoE2 ,I2    ,&l_##N##IcoE3 ,XI2   ,YI2   ,icbe_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoE1 ,I1    ,&l_##N##IcoE2 ,XI1   ,YI1   ,icbe_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoL3 ,IL3   ,&l_##N##IcoE1 ,XIL3  ,YIL3  ,icbl_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoL2 ,IL2   ,&l_##N##IcoL3 ,XIL2  ,YIL2  ,icbl_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoL1 ,IL1   ,&l_##N##IcoL2 ,XIL1  ,YIL1  ,icbl_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##Ico   ,I     ,&l_##N##IcoL1 ,XI    ,YI    ,icbr_##N  ,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list  ,I,&l_##N##Ico, N);

#define COvcAdfSclDP(N,I,XI,YI,I1,XI1,YI1,I2,XI2,YI2,I3,XI3,YI3,IL1,XIL1,YIL1,IL2,XIL2,YIL2,IL3,XIL3,YIL3,ILI1,XILI1,YILI1,ILI2,XILI2,YILI2,ILI3,XILI3,YILI3,ILS,XILS,YILS,NDL1D,NDL1M,NDL1P,NDL1R,NDL1LD,NDL1LM,NDL1LP,NDL1LR,NDLX,NDLY,NDLCX,NDLCY,TBLN,PWRADF,NDLAPI,G,GX,GY,GXS,GYS,G2,H2,GX2,GY2,GXS2,GYS2,G3,H3,GX3,GY3,GXS3,GYS3,G22,H22,GX22,GY22,GXS22,GYS22,G32,H32,GX32,GY32,GXS32,GYS32) /**/ \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	GLT_TGL(G); \
	return TRUE; \
	} \
	bool FSAPI mcb_1##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(G2); GLT_DEC(H2); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(G2); GLT_INC(H2); \
	} \
	return TRUE; \
	} \
	bool FSAPI mcb_2##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(G3); GLT_DEC(H3); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(G3); GLT_INC(H3); \
	} \
	return TRUE; \
	} \
	bool FSAPI mcb_12##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(G22); GLT_DEC(H22); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(G22); GLT_INC(H22); \
	} \
	return TRUE; \
	} \
	bool FSAPI mcb_22##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(G32); GLT_DEC(H32); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(G32); GLT_INC(H32); \
	} \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return GLT_TTGET(G);) \
	static MAKE_TCB(N##_tcb_02,return GLT_TTGET(G2);) \
	static MAKE_TCB(N##_tcb_03,return GLT_TTGET(G3);) \
	static MAKE_TCB(N##_tcb_04,return GLT_TTGET(G22);) \
	static MAKE_TCB(N##_tcb_05,return GLT_TTGET(G32);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", GX, GY, GXS, GYS,CURSOR_HAND,MOUSE_LR,mcb_##N,N) \
	MOUSE_TBOX2("2", GX2, GY2, GXS2, GYS2,CURSOR_HAND,MOUSE_LR,mcb_1##N,N) \
	MOUSE_TBOX2("3", GX3, GY3, GXS3, GYS3,CURSOR_HAND,MOUSE_LR,mcb_2##N,N) \
	MOUSE_TBOX2("4", GX22, GY22, GXS22, GYS22,CURSOR_HAND,MOUSE_LR,mcb_12##N,N) \
	MOUSE_TBOX2("5", GX32, GY32, GXS32, GYS32,CURSOR_HAND,MOUSE_LR,mcb_22##N,N) \
	MOUSE_END \
	static double FSAPI icbs_##N(PELEMENT_ICON pelement) { \
	if(PWRADF) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icble_##N(PELEMENT_ICON pelement) { \
	if(PWRADF&&POS_GET(POS_PANEL_LANG)) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbl_##N(PELEMENT_ICON pelement) { \
	if(PWRADF&&!POS_GET(POS_PANEL_LANG)) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!PWRADF&&POS_GET(POS_PANEL_LANG)) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	} \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	double Needle##N(PELEMENT_NEEDLE pelement,int clr) { \
	if(POS_GET(POS_PANEL_STATE)==clr&&!PWRADF) { \
	SHOW_IMAGE(pelement); \
	return 0; \
	} \
	HIDE_IMAGE(pelement); \
	return 0; \
	};\
	double NeedleLit##N(PELEMENT_NEEDLE pelement,int clr) { \
	if(POS_GET(POS_PANEL_STATE)==clr&&PWRADF) {  \
	SHOW_IMAGE(pelement);\
	return NDL_GET(NDLAPI);\
	}\
	HIDE_IMAGE(pelement);\
	return 0;\
	};\
	static double FSAPI icbndld_##N(PELEMENT_NEEDLE pelement) { \
	return Needle##N(pelement,PANEL_LIGHT_DAY); \
	} \
	static double FSAPI icbndlm_##N(PELEMENT_NEEDLE pelement) { \
	return Needle##N(pelement,PANEL_LIGHT_MIX); \
	} \
	static double FSAPI icbndlp_##N(PELEMENT_NEEDLE pelement) { \
	return Needle##N(pelement,PANEL_LIGHT_PLF); \
	} \
	static double FSAPI icbndlr_##N(PELEMENT_NEEDLE pelement) { \
	return Needle##N(pelement,PANEL_LIGHT_RED); \
	} \
	static double FSAPI icbndlld_##N(PELEMENT_NEEDLE pelement) { \
	return NeedleLit##N(pelement,PANEL_LIGHT_DAY); \
	} \
	static double FSAPI icbndllm_##N(PELEMENT_NEEDLE pelement) { \
	return NeedleLit##N(pelement,PANEL_LIGHT_MIX); \
	} \
	static double FSAPI icbndllp_##N(PELEMENT_NEEDLE pelement) { \
	return NeedleLit##N(pelement,PANEL_LIGHT_PLF); \
	} \
	static double FSAPI icbndllr_##N(PELEMENT_NEEDLE pelement) { \
	return NeedleLit##N(pelement,PANEL_LIGHT_RED); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_NEEDLE2(N##NdllP ,NDL1LP,NULL          ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndllp_##N,TBLN,0,N); \
	MY_NEEDLE2(N##NdllD ,NDL1LD,&l_##N##NdllP ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndlld_##N,TBLN,0,N); \
	MY_NEEDLE2(N##NdlP  ,NDL1P ,&l_##N##NdllD ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndlp_##N ,TBLN,0,N); \
	MY_NEEDLE2(N##NdlD  ,NDL1D ,&l_##N##NdlP  ,NDLX  ,NDLY  ,NDLCX ,NDLCY ,icbndld_##N ,TBLN,0,N); \
	MY_ICON2  (N##IcoS  ,ILS   ,&l_##N##NdlD  ,XILS  ,YILS  ,icbs_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoLE3,ILI3  ,&l_##N##IcoS  ,XILI3 ,YILI3 ,icble_##N ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoLE2,ILI2  ,&l_##N##IcoLE3,XILI2 ,YILI2 ,icble_##N ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoLE1,ILI1  ,&l_##N##IcoLE2,XILI1 ,YILI1 ,icble_##N ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoE3 ,I3    ,&l_##N##IcoLE1,XI3   ,YI3   ,icbe_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoE2 ,I2    ,&l_##N##IcoE3 ,XI2   ,YI2   ,icbe_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoE1 ,I1    ,&l_##N##IcoE2 ,XI1   ,YI1   ,icbe_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoL3 ,IL3   ,&l_##N##IcoE1 ,XIL3  ,YIL3  ,icbl_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoL2 ,IL2   ,&l_##N##IcoL3 ,XIL2  ,YIL2  ,icbl_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoL1 ,IL1   ,&l_##N##IcoL2 ,XIL1  ,YIL1  ,icbl_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##Ico   ,I     ,&l_##N##IcoL1 ,XI    ,YI    ,icbr_##N  ,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list  ,I,&l_##N##Ico, N);

#define COvcB(N,I,XI,YI,IE,XIE,YIE,IS1,XIS1,YIS1,IS2,XIS2,YIS2,IS3,XIS3,YIS3,G1,G2,G3,X) /**/ \
	static auto_ptr<CNamedVar> m_Xml##N; \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_INC(G1); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_DEC(G1); \
	} \
	if(!m_Xml##N.get()) \
	m_Xml##N=auto_ptr<CNamedVar>(new CNamedVar(X)); \
	m_Xml##N->set_number(GLT_GET(G1)); \
	return TRUE; \
	}; \
	static MAKE_TCB(N##_tcb_01,return GLT_TTGET(G1);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", 0, 0, I##_SX, I##_SY,CURSOR_HAND,MOUSE_LR,mcb_##N,N) \
	MOUSE_END \
	static double FSAPI icbs3_##N(PELEMENT_ICON pelement) { \
	return GLT_GET(G3)+POS_GET(POS_PANEL_STATE)*12; \
	} \
	static double FSAPI icbs2_##N(PELEMENT_ICON pelement) { \
	return GLT_GET(G2)+POS_GET(POS_PANEL_STATE)*12; \
	} \
	static double FSAPI icbs1_##N(PELEMENT_ICON pelement) { \
	return GLT_GET(G1)+POS_GET(POS_PANEL_STATE)*12; \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) { \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} else { \
	HIDE_IMAGE(pelement);\
	return -1;\
	}\
	} \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2  (N##IcoS3 ,IS3   ,NULL	      ,XIS3  ,YIS3  ,icbs3_##N ,10*PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoS2 ,IS2   ,&l_##N##IcoS3 ,XIS2  ,YIS2  ,icbs2_##N ,10*PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoS1 ,IS1   ,&l_##N##IcoS2 ,XIS1  ,YIS1  ,icbs1_##N ,12*PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##IcoE  ,IE    ,&l_##N##IcoS1 ,XIE   ,YIE   ,icbe_##N  ,PANEL_LIGHT_MAX, N); \
	MY_ICON2  (N##Ico   ,I     ,&l_##N##IcoE  ,XI    ,YI    ,icbr_##N  ,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list  ,I,&l_##N##Ico, N);

static double FSAPI icbr_Ovc01(PELEMENT_ICON pelement) 
{ 
	return POS_GET(POS_PANEL_STATE); 
}

static double FSAPI icbe_Ovc01(PELEMENT_ICON pelement) 
{ 
	if(!POS_GET(POS_PANEL_LANG)) { 
		HIDE_IMAGE(pelement); 
		return -1; 
	}
	SHOW_IMAGE(pelement); 
	return POS_GET(POS_PANEL_STATE); 
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_LLIGHTLEFT              ))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_LLIGHTRIGHT             ))
static MAKE_TCB(tcb_03	,AZS_TT(AZS_LLIGHTSMOTOR            ))
static MAKE_TCB(tcb_04	,AZS_TT(AZS_LIGHTSNAV               ))
static MAKE_TCB(tcb_05	,AZS_TT(AZS_LIGHTSBCN               ))

static MOUSE_TOOLTIP_ARGS(Ovc01_ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MOUSE_TOOLTIP_ARGS_END

static MAKE_MSCB(mcb_llightup,AZS_SET(AZS_LLIGHTLEFT    ,2))
static MAKE_MSCB(mcb_llightcn,AZS_SET(AZS_LLIGHTLEFT    ,0))
static MAKE_MSCB(mcb_llightdn,AZS_SET(AZS_LLIGHTLEFT    ,1))
static MAKE_MSCB(mcb_rlightup,AZS_SET(AZS_LLIGHTRIGHT   ,2))
static MAKE_MSCB(mcb_rlightcn,AZS_SET(AZS_LLIGHTRIGHT   ,0))
static MAKE_MSCB(mcb_rlightdn,AZS_SET(AZS_LLIGHTRIGHT   ,1))
static MAKE_MSCB(mcb_lightmtr,AZS_TGL(AZS_LLIGHTSMOTOR  ))
static MAKE_MSCB(mcb_lightano,AZS_TGL(AZS_LIGHTSNAV     ))
static MAKE_MSCB(mcb_lightbcn,AZS_TGL(AZS_LIGHTSBCN     ))

MOUSE_BEGIN(rect_Ovc01,HELP_NONE,0,0) 
MOUSE_TBOX2("1",   0,  0, 79, 90,CURSOR_HAND,MOUSE_LR,mcb_llightup,Ovc01)
MOUSE_TBOX2("1",   0, 90, 79, 65,CURSOR_HAND,MOUSE_LR,mcb_llightcn,Ovc01)
MOUSE_TBOX2("1",   0,155, 79, 40,CURSOR_HAND,MOUSE_LR,mcb_llightdn,Ovc01)
MOUSE_TBOX2("2",  80,  0, 60, 90,CURSOR_HAND,MOUSE_LR,mcb_rlightup,Ovc01)
MOUSE_TBOX2("2",  80, 90, 60, 65,CURSOR_HAND,MOUSE_LR,mcb_rlightcn,Ovc01)
MOUSE_TBOX2("2",  80,155, 60, 40,CURSOR_HAND,MOUSE_LR,mcb_rlightdn,Ovc01)
MOUSE_TBOX2("3", 140,  0, 55,195,CURSOR_HAND,MOUSE_LR,mcb_lightmtr,Ovc01)
MOUSE_TBOX2("4", 195,  0, 60,195,CURSOR_HAND,MOUSE_LR,mcb_lightano,Ovc01)
MOUSE_TBOX2("5", 255,  0, 80,195,CURSOR_HAND,MOUSE_LR,mcb_lightbcn,Ovc01)
MOUSE_END 

extern PELEMENT_HEADER Ovc01_list; 
GAUGE_HEADER_FS700_EX(VC5_OVC01_D_SX, "Ovc01", &Ovc01_list, rect_Ovc01, 0, 0, 0, 0, Ovc01); 

MY_ICON2(Ovc01IcoE4,VC5_OVC01INTL4_D,NULL,42,176,icbe_Ovc01,PANEL_LIGHT_MAX, Ovc01); 
MY_ICON2(Ovc01IcoE3,VC5_OVC01INTL3_D,&l_Ovc01IcoE4,6,89,icbe_Ovc01,PANEL_LIGHT_MAX, Ovc01); 
MY_ICON2(Ovc01IcoE2,VC5_OVC01INTL2_D,&l_Ovc01IcoE3,201,44,icbe_Ovc01,PANEL_LIGHT_MAX, Ovc01); 
MY_ICON2(Ovc01IcoE1,VC5_OVC01INTL1_D,&l_Ovc01IcoE2,34,18,icbe_Ovc01,PANEL_LIGHT_MAX, Ovc01); 
MY_ICON2(Ovc01Ico,VC5_OVC01_D,&l_Ovc01IcoE1,0,0,icbr_Ovc01,PANEL_LIGHT_MAX, Ovc01); 
MY_STATIC2(Ovc01bg,Ovc01_list,VC5_OVC01_D,&l_Ovc01Ico, Ovc01);

static double FSAPI icbr_Ovc02(PELEMENT_ICON pelement) 
{ 
	return POS_GET(POS_PANEL_STATE); 
}

static double FSAPI icbe_Ovc02(PELEMENT_ICON pelement) 
{
	if(!POS_GET(POS_PANEL_LANG)) { 
		HIDE_IMAGE(pelement); 
		return -1; 
	}
	SHOW_IMAGE(pelement); 
	return POS_GET(POS_PANEL_STATE); 
} 

static MAKE_TCB(Ovc02tcb_01	,AZS_TT(AZS_PKAI251                 ))
static MAKE_TCB(Ovc02tcb_02	,AZS_TT(AZS_PKAI252                 ))
static MAKE_TCB(Ovc02tcb_03	,AZS_TT(AZS_PKAI253                 ))

static MOUSE_TOOLTIP_ARGS(Ovc02_ttargs)
MAKE_TTA(Ovc02tcb_01	)
MAKE_TTA(Ovc02tcb_02	) 
MAKE_TTA(Ovc02tcb_03	)
MOUSE_TOOLTIP_ARGS_END

static MAKE_MSCB(mcb_pk1,if(AZS_GET(AZS_PKAI251)==2||AZS_GET(AZS_PKAI251)==3)AZS_SET(AZS_PKAI251,1);else AZS_SET(AZS_PKAI251,3);)
static MAKE_MSCB(mcb_pk2,if(AZS_GET(AZS_PKAI252)==2||AZS_GET(AZS_PKAI252)==3)AZS_SET(AZS_PKAI252,1);else AZS_SET(AZS_PKAI252,3);)
static MAKE_MSCB(mcb_pk3,if(AZS_GET(AZS_PKAI253)==2||AZS_GET(AZS_PKAI253)==3)AZS_SET(AZS_PKAI253,1);else AZS_SET(AZS_PKAI253,3);)

MOUSE_BEGIN(rect_Ovc02,HELP_NONE,0,0) 
MOUSE_TBOX2("1",  0, 0, 60,VC5_OVC02_D_SY,CURSOR_HAND,MOUSE_LR,mcb_pk1,Ovc02)
MOUSE_TBOX2("2", 60, 0, 55,VC5_OVC02_D_SY,CURSOR_HAND,MOUSE_LR,mcb_pk2,Ovc02)
MOUSE_TBOX2("3",115, 0, 53,VC5_OVC02_D_SY,CURSOR_HAND,MOUSE_LR,mcb_pk3,Ovc02)
MOUSE_END 

extern PELEMENT_HEADER Ovc02_list; 
GAUGE_HEADER_FS700_EX(VC5_OVC02_D_SX, "Ovc02", &Ovc02_list, rect_Ovc02, 0, 0, 0, 0, Ovc02);

MY_ICON2(Ovc02IcoE1,VC5_OVC02INTL_D,NULL,7,0,icbe_Ovc02,PANEL_LIGHT_MAX, Ovc02);
MY_ICON2(Ovc02Ico,VC5_OVC02_D,&l_Ovc02IcoE1,0,0,icbr_Ovc02,PANEL_LIGHT_MAX, Ovc02); 
MY_STATIC2(Ovc02bg,Ovc02_list,VC5_OVC02_D,&l_Ovc02Ico, Ovc02);

SIMPLE_GAUGE_VC	(OvcBtm		,VC5_OVCBTM_D		);
SIMPLE_GAUGE_VC	(OvcFront	,VC5_OVCFRONT_D		);
SIMPLE_GAUGE_VC	(OvcSide	,VC5_OVCSIDE_D		);
//CElem4Lang4 (Ovc01		,VC5_OVC01_D		,VC5_OVC01INTL1_D,VC5_OVC01INTL2_D,VC5_OVC01INTL3_D,VC5_OVC01INTL4_D,0,0,34,18,201,44,6,89,42,176);
//CElem4Lang1 (Ovc02		,VC5_OVC02_D		,VC5_OVC02INTL_D,0,0,7,0);
SIMPLE_GAUGE_V�_LANG2(Ovc03		,VC5_OVC03_D		,VC5_OVC03INTL1_D,VC5_OVC03INTL2_D,0,0,43,28,279,176);
SIMPLE_GAUGE_VC_LANG1(Ovc06		,VC5_OVC06_D		,VC5_OVC06INTL_D,0,0,15,58);
SIMPLE_GAUGE_VC_LANG1(Ovc07		,VC5_OVC07_D		,VC5_OVC07INTL_D,0,0,18,59);
SIMPLE_GAUGE_V�_LANG4(Ovc14		,VC5_OVC14_D		,VC5_OVC14INTL1_D,VC5_OVC14INTL2_D,VC5_OVC14INTL3_D,VC5_OVC14INTL4_D,0,0,0,11,285,71,0,136,170,136);
SIMPLE_GAUGE_V�_LANG4(Ovc15		,VC5_OVC15_D		,VC5_OVC15INTL1_D,VC5_OVC15INTL2_D,VC5_OVC15INTL3_D,VC5_OVC15INTL4_D,0,0,54,0,123,88,75,150,287,150);
SIMPLE_GAUGE_V�_LANG2(Ovc16		,VC5_OVC16_D		,VC5_OVC16INTL1_D,VC5_OVC16INTL2_D,0,0,0,5,81,112);
SIMPLE_GAUGE_VC	(Ovc17		,VC5_OVC17_D		);
SIMPLE_GAUGE_VC	(Ovc18		,VC5_OVC18_D		);
SIMPLE_GAUGE_VC	(Ovc19		,VC5_OVC19_D		);
SIMPLE_GAUGE_VC	(Ovc20		,VC5_OVC20_D		);
SIMPLE_GAUGE_VC	(Ovc10a		,VC5_OVC10A_D		);
SIMPLE_GAUGE_VC	(Ovc11a		,VC5_OVC11A_D		);
SIMPLE_GAUGE_VC	(Ovc12a		,VC5_OVC12A_D		);
SIMPLE_GAUGE_VC	(Ovc13a		,VC5_OVC13A_D		);

static NONLINEARITY tbl_adf[]={
	{{ 27,  32},     0,0},
	{{ 35,  28},  1000,0},
	{{ 43,  24},  2500,0},
	{{ 51,  23},  7500,0},
	{{ 60,  24}, 25000,0},
	{{ 68,  28}, 67500,0},
	{{ 75,  32},200000,0}
};

COvcComScl	(Ovc04,VC5_OVC04_D,0,0,VC5_OVC04INTL1_D,64,104,VC5_OVC04INTL2_D,224,175,VC5_OVC04INTL3_D,347,104,VC5_OVC04LIT_D,0,0,VC5_OVC04LITINTL1_D,64,104,VC5_OVC04LITINTL2_D,224,175,VC5_OVC04LITINTL3_D,347,104,VC5_SCL_COM1A_D_00,182,90,VC5_SCL_COM1B_D_00,244,90,VC5_SCL_COM1C_D_00,272,90,VC5_SCL_COM1ALIT_D_00,182,90,VC5_SCL_COM1BLIT_D_00,244,90,VC5_SCL_COM1CLIT_D_00,272,90,PWR_GET(PWR_COM1),POS_SCLVHF1LEFT,POS_SCLVHF1MID,POS_SCLVHF1RIGHT,HND_KNBVHF1LEFT,HND_KNBVHF1RIGHT,100,130,52,52,343,130,52,52,KEY_COM_RADIO_WHOLE_DEC,KEY_COM_RADIO_WHOLE_INC,KEY_COM_RADIO_FRACT_DEC,KEY_COM_RADIO_FRACT_INC);
COvcComScl	(Ovc05,VC5_OVC05_D,0,0,VC5_OVC05INTL1_D,81,107,VC5_OVC05INTL2_D,249,176,VC5_OVC05INTL3_D,373,107,VC5_OVC05LIT_D,0,0,VC5_OVC05LITINTL1_D,81,107,VC5_OVC05LITINTL2_D,249,176,VC5_OVC05LITINTL3_D,373,107,VC5_SCL_COM2A_D_00,207,90,VC5_SCL_COM2B_D_00,269,90,VC5_SCL_COM2C_D_00,297,90,VC5_SCL_COM2ALIT_D_00,207,90,VC5_SCL_COM2BLIT_D_00,269,90,VC5_SCL_COM2CLIT_D_00,297,90,PWR_GET(PWR_COM2),POS_SCLVHF2LEFT,POS_SCLVHF2MID,POS_SCLVHF2RIGHT,HND_KNBVHF2LEFT,HND_KNBVHF2RIGHT,127,130,52,52,369,130,52,52,KEY_COM2_RADIO_WHOLE_DEC,KEY_COM2_RADIO_WHOLE_INC,KEY_COM2_RADIO_FRACT_DEC,KEY_COM2_RADIO_FRACT_INC);


#if !defined(LITE_VERSION) 
COvcAdfScl	(Ovc08,VC5_OVC08_D,0,0,VC5_OVC08INTL1_D,19, 82,VC5_OVC08INTL2_D,220, 28,VC5_OVC08INTL3_D,230,167,VC5_OVC08LIT1_D,19,82,VC5_OVC08LIT2_D,220,28,VC5_OVC08LIT3_D,230,167,VC5_OVC08LIT1INTL1_D,19,82,VC5_OVC08LIT2INTL2_D,220,28,VC5_OVC08LIT3INTL3_D,230,167,VC5_OVC08LITSCL_D,119,21,VC5_NDL_ADF1_D,VC5_NDL_ADF1_M,VC5_NDL_ADF1_P,VC5_NDL_ADF1_R,VC5_NDL_ADF1LIT_D,VC5_NDL_ADF1LIT_M,VC5_NDL_ADF1LIT_P,VC5_NDL_ADF1LIT_R,171,77,0,5,tbl_adf,PWR_GET(PWR_ADF1),NDL_ARK1SIGNAL,GLT_ARK1,224,20,344-224,146-20,GLT_ARK1LEFT10,GLT_ARK1LEFTHND,0,200,120,60,GLT_ARK1LEFT1,GLT_MAX,0,261,120,60,GLT_ARK1RIGHT10,GLT_ARK1RIGHTHND,220,200,120,60,GLT_ARK1RIGHT1,GLT_MAX,220,261,120,60);
COvcAdfScl	(Ovc09,VC5_OVC09_D,0,0,VC5_OVC09INTL1_D,17, 82,VC5_OVC09INTL2_D,222, 27,VC5_OVC09INTL3_D,222,164,VC5_OVC09LIT1_D,17,82,VC5_OVC09LIT2_D,222,27,VC5_OVC09LIT3_D,222,164,VC5_OVC09LIT1INTL1_D,17,82,VC5_OVC09LIT2INTL2_D,222,27,VC5_OVC09LIT3INTL3_D,222,164,VC5_OVC09LITSCL_D,120,21,VC5_NDL_ADF2_D,VC5_NDL_ADF2_M,VC5_NDL_ADF2_P,VC5_NDL_ADF2_R,VC5_NDL_ADF2LIT_D,VC5_NDL_ADF2LIT_M,VC5_NDL_ADF2LIT_P,VC5_NDL_ADF2LIT_R,171,77,0,5,tbl_adf,PWR_GET(PWR_ADF2),NDL_ARK2SIGNAL,GLT_ARK2,212,17,392-221,141-17,GLT_ARK2LEFT10,GLT_ARK2LEFTHND,0,200,120,60,GLT_ARK2LEFT1,GLT_MAX,0,261,120,60,GLT_ARK2RIGHT10,GLT_ARK2RIGHTHND,220,200,120,60,GLT_ARK2RIGHT1,GLT_MAX,220,261,120,60);
#else 
COvcAdfSclDP(Ovc08,VC5_OVC08_D,0,0,VC5_OVC08INTL1_D,19, 82,VC5_OVC08INTL2_D,220, 28,VC5_OVC08INTL3_D,230,167,VC5_OVC08LIT1_D,19,82,VC5_OVC08LIT2_D,220,28,VC5_OVC08LIT3_D,230,167,VC5_OVC08LIT1INTL1_D,19,82,VC5_OVC08LIT2INTL2_D,220,28,VC5_OVC08LIT3INTL3_D,230,167,VC5_OVC08LITSCL_D,119,21,VC5_NDL_ADF1_D,VC5_NDL_ADF1_M,VC5_NDL_ADF1_P,VC5_NDL_ADF1_R,VC5_NDL_ADF1LIT_D,VC5_NDL_ADF1LIT_M,VC5_NDL_ADF1LIT_P,VC5_NDL_ADF1LIT_R,171,77,0,5,tbl_adf,PWR_GET(PWR_ADF1),NDL_ARK1SIGNAL,GLT_ARK1,224,20,344-224,146-20,GLT_ARK1LEFT10,GLT_ARK1LEFTHND,0,200,120,60,GLT_ARK1LEFT1,GLT_MAX,0,261,120,60,GLT_ARK1RIGHT10,GLT_ARK1RIGHTHND,220,200,120,60,GLT_ARK1RIGHT1,GLT_MAX,220,261,120,60);
COvcAdfSclDP(Ovc09,VC5_OVC09_D,0,0,VC5_OVC09INTL1_D,17, 82,VC5_OVC09INTL2_D,222, 27,VC5_OVC09INTL3_D,222,164,VC5_OVC09LIT1_D,17,82,VC5_OVC09LIT2_D,222,27,VC5_OVC09LIT3_D,222,164,VC5_OVC09LIT1INTL1_D,17,82,VC5_OVC09LIT2INTL2_D,222,27,VC5_OVC09LIT3INTL3_D,222,164,VC5_OVC09LITSCL_D,120,21,VC5_NDL_ADF2_D,VC5_NDL_ADF2_M,VC5_NDL_ADF2_P,VC5_NDL_ADF2_R,VC5_NDL_ADF2LIT_D,VC5_NDL_ADF2LIT_M,VC5_NDL_ADF2LIT_P,VC5_NDL_ADF2LIT_R,171,77,0,5,tbl_adf,PWR_GET(PWR_ADF2),NDL_ARK2SIGNAL,GLT_ARK2,212,17,392-221,141-17,GLT_ARK2LEFT10,GLT_ARK2LEFTHND,0,200,120,60,GLT_ARK2LEFT1,GLT_MAX,0,261,120,60,GLT_ARK2RIGHT10,GLT_ARK2RIGHTHND,220,200,120,60,GLT_ARK2RIGHT1,GLT_MAX,220,261,120,60);
#endif

COvcB		(Ovc10b,VC5_OVC10B_D,0,0,VC5_OVC10BINTL_D,0,0,VC5_SCL_ADF1SEC1_D_00,17,63,VC5_SCL_ADF1SEC2_D_00,34,63,VC5_SCL_ADF1SEC3_D_00,45,63,GLT_ARK1LEFT100,GLT_ARK1LEFT10,GLT_ARK1LEFT1,"sd_6015_p5_303");
COvcB		(Ovc11b,VC5_OVC11B_D,0,0,VC5_OVC11BINTL_D,0,0,VC5_SCL_ADF1PRI1_D_00,17,63,VC5_SCL_ADF1PRI2_D_00,34,63,VC5_SCL_ADF1PRI3_D_00,45,63,GLT_ARK1RIGHT100,GLT_ARK1RIGHT10,GLT_ARK1RIGHT1,"sd_6015_p5_305");
COvcB		(Ovc12b,VC5_OVC12B_D,0,0,VC5_OVC12BINTL_D,0,0,VC5_SCL_ADF2SEC1_D_00,17,63,VC5_SCL_ADF2SEC2_D_00,34,63,VC5_SCL_ADF2SEC3_D_00,45,63,GLT_ARK2LEFT100,GLT_ARK2LEFT10,GLT_ARK2LEFT1,"sd_6015_p5_304");
COvcB		(Ovc13b,VC5_OVC13B_D,0,0,VC5_OVC13BINTL_D,0,0,VC5_SCL_ADF2PRI1_D_00,17,63,VC5_SCL_ADF2PRI2_D_00,34,63,VC5_SCL_ADF2PRI3_D_00,45,63,GLT_ARK2RIGHT100,GLT_ARK2RIGHT10,GLT_ARK2RIGHT1,"sd_6015_p5_306");

#ifdef _DEBUG
DLLMAIND("VC5.log")
#else
DLLMAIN()
#endif

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&OvcBtm				),
		(&OvcFront				),
		(&OvcSide				),
		(&Ovc01					),
		(&Ovc02					),        
		(&Ovc03					),        
		(&Ovc06					),        
		(&Ovc07					),        
		(&Ovc14					),        
		(&Ovc15					),        
		(&Ovc16					),        
		(&Ovc17					),        
		(&Ovc18					),        
		(&Ovc19					),        
		(&Ovc20					),        
		(&Ovc10a				),        
		(&Ovc11a				),        
		(&Ovc12a				),        
		(&Ovc13a				),        
		(&Ovc04					),        
		(&Ovc05					),            
		(&Ovc08					),
		(&Ovc09					),
		(&Ovc10b				),
		(&Ovc11b				),
		(&Ovc12b				),        
		(&Ovc13b				),        
		0					
	}											
};												
							
