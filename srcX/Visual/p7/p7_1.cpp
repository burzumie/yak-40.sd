/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

#define OFFX	0
#define OFFY	0

static MAKE_ICB(icb_sgu        		,SHOW_GLT(GLT_SGU7      ,5))
static MAKE_ICB(icb_crs_p7_01		,SHOW_POSM(POS_CRS7_01,1,0))

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_glt_sgu_l      , GLT_DEC(GLT_SGU7))
static MAKE_MSCB(mcb_glt_sgu_r      , GLT_INC(GLT_SGU7))

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS7_01,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p7_01(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS7_01,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P7);
		panel_window_open_ident(IDENT_P1);
	}
	return true;
}

static MAKE_TCB(tcb_01	,POS_TT(POS_CRS7_01				))
static MAKE_TCB(tcb_02	,GLT_TT(GLT_SGU7				))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p7_1,HELP_NONE,0,0)
	MOUSE_PBOX(0,0,P7_BACKGROUND1_D_SX,P7_BACKGROUND1_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
	MOUSE_TBOX(  "1",  10-OFFX,    9-OFFY, 81, 80,CURSOR_HAND,MOUSE_MLR,mcb_crs_p7_01)
	MOUSE_TSVB(  "2", 121-OFFX,  325-OFFY, 75, 76,CURSOR_UPARROW,CURSOR_DOWNARROW,MOUSE_LR,mcb_glt_sgu_r,mcb_glt_sgu_l)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p7_1_list; 
GAUGE_HEADER_FS700_EX(P7_BACKGROUND1_D_SX, "p7_1", &p7_1_list, rect_p7_1, 0, 0, 0, 0, p7_1); 

MY_ICON2	(p7_crs_01			,MISC_CRS_P7_01			,NULL			,  10-OFFX,    9-OFFY,icb_crs_p7_01 , 1*PANEL_LIGHT_MAX,p7_1)
MY_ICON2	(p7_knb_sgu			,P7_KNB_SGURIGHT_D_00	,&l_p7_crs_01	, 121-OFFX,  325-OFFY,icb_sgu       , 5*PANEL_LIGHT_MAX,p7_1)
MY_ICON2	(p7_1Ico			,P7_BACKGROUND1_D		,&l_p7_knb_sgu	,        0,         0,icb_Ico       ,   PANEL_LIGHT_MAX,p7_1)
MY_STATIC2	(p7_1bg,p7_1_list	,P7_BACKGROUND1_D		,&l_p7_1Ico		,p7_1)

#endif