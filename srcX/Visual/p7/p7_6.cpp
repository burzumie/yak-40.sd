/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

#define OFFX	355
#define OFFY	493

MAKE_ICB(icb_kmprfreq		,SHOW_HND(HND_KMP2FREQ		,20))
MAKE_ICB(icb_kmprherz		,SHOW_BTN(BTN_KMP2ID  		,2))
static MAKE_ICB(icb_obsright		,SHOW_HND(HND_OBS7			,20))
MAKE_ICB(icb_kmp2vorl  		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP2VOR		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmp2pwrl  		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP2PWR		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst1l  		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST1		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst2l 		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST2		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst3l  		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST3		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmp2vor  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP2VOR		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmp2pwr  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP2PWR		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst1  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST1		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst2  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST2		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst3  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST3		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr1scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ1	,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr2scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ2	,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr3scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ3	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr4scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ4	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr5scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ5	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr1scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS71     	,4)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr2scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS72     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr3scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS73     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr1scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ1	,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr2scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ2	,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr3scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ3	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr4scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ4	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpr5scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ5	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr1scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS71     	,4)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr2scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS72     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr3scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS73     	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_bkgl			,if(!PWR_GET(PWR_KURSMP2)){ HIDE_IMAGE(pelement); return -1;} else {SHOW_IMAGE(pelement); return 1;}	)

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_MSCB(mcb_azs_kmp2vor		,AZS_TGL(AZS_KMP2VOR    	))
static MAKE_MSCB(mcb_azs_kmp2pwr		,AZS_TGL(AZS_KMP2PWR    	))
static MAKE_MSCB(mcb_btn_kmp2id         ,BTN_TGL(BTN_KMP2ID			))
static MAKE_MSCB(mcb_btn_kmp2test1		,PRS_BTN(BTN_KMP2TEST1  	))
static MAKE_MSCB(mcb_btn_kmp2test2		,PRS_BTN(BTN_KMP2TEST2  	))
static MAKE_MSCB(mcb_btn_kmp2test3		,PRS_BTN(BTN_KMP2TEST3  	))

BOOL FSAPI mcb_glt_kmpfreq_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV7_FREQ1);
	int f2=(int)POS_GET(POS_NAV7_FREQ2);
	int f3=(int)POS_GET(POS_NAV7_FREQ3);
	int f4=(int)POS_GET(POS_NAV7_FREQ4);
	int f5=(int)POS_GET(POS_NAV7_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(f5==5) {
			f5=0;
		} else {
			f4--;
			if(f4<0)
				f4=9;
			f5=5;
		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);

		return TRUE;
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {

		HND_DEC(HND_KMP2FREQ);

		if(f1==1&&f2==0&&f3==8) {
			f1=1;
			f2=1;
			f3=7;
		} else {
			// NAV3
			f3--;
			if(f3<0) {
				f3=9;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;

		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);

	}
	return TRUE;
}

BOOL FSAPI mcb_glt_kmpfreq_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV7_FREQ1);
	int f2=(int)POS_GET(POS_NAV7_FREQ2);
	int f3=(int)POS_GET(POS_NAV7_FREQ3);
	int f4=(int)POS_GET(POS_NAV7_FREQ4);
	int f5=(int)POS_GET(POS_NAV7_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(f5==5) {
			f4++;
			if(f4>9)
				f4=0;
			f5=0;
		} else {
			f5=5;
		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);

		return TRUE;
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {

		HND_INC(HND_KMP2FREQ);

		if(f1==1&&f2==1&&f3==7) {
			f1=1;
			f2=0;
			f3=8;
		} else {
			// NAV3
			f3++;
			if(f3>9) {
				f3=0;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);
	}
	return TRUE;
}

BOOL FSAPI mcb_glt_kmpfreqr_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV7_FREQ1);
	int f2=(int)POS_GET(POS_NAV7_FREQ2);
	int f3=(int)POS_GET(POS_NAV7_FREQ3);
	int f4=(int)POS_GET(POS_NAV7_FREQ4);
	int f5=(int)POS_GET(POS_NAV7_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE||mouse_flags&MOUSE_LEFTSINGLE) {
		if(f5==5) {
			f5=0;
		} else {
			f4--;
			if(f4<0)
				f4=9;
			f5=5;
		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);

	}
	return TRUE;
}

BOOL FSAPI mcb_glt_kmpfreqr_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV7_FREQ1);
	int f2=(int)POS_GET(POS_NAV7_FREQ2);
	int f3=(int)POS_GET(POS_NAV7_FREQ3);
	int f4=(int)POS_GET(POS_NAV7_FREQ4);
	int f5=(int)POS_GET(POS_NAV7_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE||mouse_flags&MOUSE_LEFTSINGLE) {
		if(f5==5) {
			f4++;
			if(f4>9)
				f4=0;
			f5=0;
		} else {
			f5=5;
		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);

	}
	return TRUE;
}

BOOL FSAPI mcb_glt_obs_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int o1=(int)POS_GET(POS_OBS71);
	int o2=(int)POS_GET(POS_OBS72);
	int o3=(int)POS_GET(POS_OBS73);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		for(int i=0;i<10;i++) {
			HND_DEC(HND_OBS7);

			if(!CFG_GET(CFG_OBS_ZERO_START)) {
				if(o3==1&&o2==0&&o1==0) {
					o1=3;
					o2=6;
					o3=0;
				} else {
					// OBS3
					o3--;
					if(o3<0) {
						o3=9;
						// OBS2
						o2--;
						if(o2<0) {
							o2=9;
							// OBS1
							o1--;
							if(o1<0) {
								o1=3;
							}
						}
					}
				}
			} else {
				if(o3==0&&o2==0&&o1==0) {
					o1=3;
					o2=5;
					o3=9;
				} else {
					// OBS3
					o3--;
					if(o3<0) {
						o3=9;
						// OBS2
						o2--;
						if(o2<0) {
							o2=9;
							// OBS1
							o1--;
							if(o1<0) {
								o1=3;
							}
						}
					}
				}
			}
		}
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		HND_DEC(HND_OBS7);

		if(!CFG_GET(CFG_OBS_ZERO_START)) {
			if(o3==1&&o2==0&&o1==0) {
				o1=3;
				o2=6;
				o3=0;
			} else {
				// OBS3
				o3--;
				if(o3<0) {
					o3=9;
					// OBS2
					o2--;
					if(o2<0) {
						o2=9;
						// OBS1
						o1--;
						if(o1<0) {
							o1=3;
						}
					}
				}
			}
		} else {
			if(o3==0&&o2==0&&o1==0) {
				o1=3;
				o2=5;
				o3=9;
			} else {
				// OBS3
				o3--;
				if(o3<0) {
					o3=9;
					// OBS2
					o2--;
					if(o2<0) {
						o2=9;
						// OBS1
						o1--;
						if(o1<0) {
							o1=3;
						}
					}
				}
			}
		}
	}

	POS_SET(POS_OBS71,o1);
	POS_SET(POS_OBS72,o2);
	POS_SET(POS_OBS73,o3);

	return TRUE;
}

BOOL FSAPI mcb_glt_obs_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int o1=(int)POS_GET(POS_OBS71);
	int o2=(int)POS_GET(POS_OBS72);
	int o3=(int)POS_GET(POS_OBS73);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		for(int i=0;i<10;i++) {
			HND_INC(HND_OBS7);

			if(!CFG_GET(CFG_OBS_ZERO_START)) {
				if(o3==0&&o2==6&&o1==3) {
					o1=0;
					o2=0;
					o3=1;
				} else {
					// OBS3
					o3++;
					if(o3>9) {
						o3=0;
						// OBS2
						o2++;
						if(o2>9) {
							o2=0;
							// OBS1
							o1++;
							if(o1>3) {
								o1=0;
							}
						}
					}
				}
			} else {
				if(o3==9&&o2==5&&o1==3) {
					o1=0;
					o2=0;
					o3=0;
				} else {
					// OBS3
					o3++;
					if(o3>9) {
						o3=0;
						// OBS2
						o2++;
						if(o2>9) {
							o2=0;
							// OBS1
							o1++;
							if(o1>3) {
								o1=0;
							}
						}
					}
				}
			}
		}
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		HND_INC(HND_OBS7);

		if(!CFG_GET(CFG_OBS_ZERO_START)) {
			if(o3==0&&o2==6&&o1==3) {
				o1=0;
				o2=0;
				o3=1;
			} else {
				// OBS3
				o3++;
				if(o3>9) {
					o3=0;
					// OBS2
					o2++;
					if(o2>9) {
						o2=0;
						// OBS1
						o1++;
						if(o1>3) {
							o1=0;
						}
					}
				}
			}
		} else {
			if(o3==9&&o2==5&&o1==3) {
				o1=0;
				o2=0;
				o3=0;
			} else {
				// OBS3
				o3++;
				if(o3>9) {
					o3=0;
					// OBS2
					o2++;
					if(o2>9) {
						o2=0;
						// OBS1
						o1++;
						if(o1>3) {
							o1=0;
						}
					}
				}
			}
		}
	}

	POS_SET(POS_OBS71,o1);
	POS_SET(POS_OBS72,o2);
	POS_SET(POS_OBS73,o3);

	return TRUE;
}

BOOL FSAPI mcb_obs2_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P24);
	return true;
}

BOOL FSAPI mcb_kmp2_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P24);
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_KMP2VOR				))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_KMP2PWR				))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_KMP2ID			    ))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_KMP2TEST1	        ))
static MAKE_TCB(tcb_05	,BTN_TT(BTN_KMP2TEST2	        ))
static MAKE_TCB(tcb_06	,BTN_TT(BTN_KMP2TEST3	        ))
static MAKE_TCB(tcb_07	,HND_TT(HND_OBS7				))
static MAKE_TCB(tcb_08	,HND_TT(HND_KMP2FREQ			))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	)
MAKE_TTA(tcb_08	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p7_6,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P7_BACKGROUND6_D_SX,P7_BACKGROUND6_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_PBOX( 70,20,420,160,CURSOR_HAND,MOUSE_LR,mcb_kmp2_big)
MOUSE_PBOX(530,50,370,160,CURSOR_HAND,MOUSE_LR,mcb_obs2_big)

MOUSE_TBOX(  "1", 492-OFFX,  524-OFFY, 73, 64,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp2vor)
MOUSE_TBOX(  "2", 739-OFFX,  544-OFFY, 51, 63,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp2pwr)
MOUSE_TSHB(  "8", 702-OFFX,  589-OFFY,120,100,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreqr_l,mcb_glt_kmpfreqr_r)
MOUSE_TBOX(  "3", 718-OFFX,  589-OFFY, 66,100,CURSOR_HAND,MOUSE_LR ,mcb_btn_kmp2id)
MOUSE_TBOX(  "4", 547-OFFX,  630-OFFY, 42, 44,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2test1)
MOUSE_TBOX(  "5", 612-OFFX,  636-OFFY, 42, 44,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2test2)
MOUSE_TBOX(  "6", 678-OFFX,  641-OFFY, 42, 44,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2test3)
MOUSE_TSHB(  "7",1114-OFFX,  628-OFFY, 96, 95,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_obs_l,mcb_glt_obs_r)
MOUSE_TSHB(  "8", 444-OFFX,  577-OFFY, 96, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreq_l,mcb_glt_kmpfreq_r)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p7_6_list; 
GAUGE_HEADER_FS700_EX(P7_BACKGROUND6_D_SX, "p7_6", &p7_6_list, rect_p7_6, 0, 0, 0, 0, p7_6); 

MY_ICON2		(p7_knb_kmprf			, P7_KNB_KURSMPRIGHTFREQ_D_00	,NULL          			, 444-OFFX,  577-OFFY,icb_kmprfreq  	, 20*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_knb_kmprh			, P7_BTN_KURSMPRIGHTID_D_00		,&l_p7_knb_kmprf		, 710-OFFX,  589-OFFY,icb_kmprherz  	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_knb_obsr			, P7_KNB_OBSRIGHT_D_00			,&l_p7_knb_kmprh		,1114-OFFX,  628-OFFY,icb_obsright		, 20*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_azs_kmp2vorl		, P7_AZS_KURSMP2VORLIT_D_00		,&l_p7_knb_obsr			, 492-OFFX,  524-OFFY,icb_kmp2vorl  	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_azs_kmp2vor			, P7_AZS_KURSMP2VOR_D_00		,&l_p7_azs_kmp2vorl		, 492-OFFX,  524-OFFY,icb_kmp2vor  		, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_azs_kmp2pwrlit		, P7_AZS_KURSMP2PWRLIT_D_00		,&l_p7_azs_kmp2vor		, 739-OFFX,  544-OFFY,icb_kmp2pwrl  	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_azs_kmp2pwr			, P7_AZS_KURSMP2PWR_D_00		,&l_p7_azs_kmp2pwrlit	, 739-OFFX,  544-OFFY,icb_kmp2pwr  		, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_btn_kmptst1l		, P7_BTN_KURSMP2TEST1LIT_D_00	,&l_p7_azs_kmp2pwr		, 547-OFFX,  630-OFFY,icb_kmptst1l  	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_btn_kmptst2l		, P7_BTN_KURSMP2TEST2LIT_D_00	,&l_p7_btn_kmptst1l		, 612-OFFX,  636-OFFY,icb_kmptst2l  	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_btn_kmptst3l		, P7_BTN_KURSMP2_TEST3LIT_D_00	,&l_p7_btn_kmptst2l		, 678-OFFX,  641-OFFY,icb_kmptst3l  	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_btn_kmptst1			, P7_BTN_KURSMP2TEST1_D_00		,&l_p7_btn_kmptst3l		, 547-OFFX,  630-OFFY,icb_kmptst1  		, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_btn_kmptst2			, P7_BTN_KURSMP2TEST2_D_00		,&l_p7_btn_kmptst1		, 612-OFFX,  636-OFFY,icb_kmptst2  		, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_btn_kmptst3			, P7_BTN_KURSMP2_TEST3_D_00		,&l_p7_btn_kmptst2		, 678-OFFX,  641-OFFY,icb_kmptst3  		, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr1l			, P7_SCL_KURSMPRIGHT1LIT_D_00	,&l_p7_btn_kmptst3		, 580-OFFX,  530-OFFY,icb_kmpr1scll 	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr2l			, P7_SCL_KURSMPRIGHT2LIT_D_00	,&l_p7_scl_kmpr1l		, 610-OFFX,  530-OFFY,icb_kmpr2scll 	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr3l			, P7_SCL_KURSMPRIGHT3LIT_D_00	,&l_p7_scl_kmpr2l		, 631-OFFX,  530-OFFY,icb_kmpr3scll 	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr4l			, P7_SCL_KURSMPRIGHT4LIT_D_00	,&l_p7_scl_kmpr3l		, 652-OFFX,  530-OFFY,icb_kmpr4scll 	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr5l			, P7_SCL_KURSMPRIGHT5LIT_D_00	,&l_p7_scl_kmpr4l		, 682-OFFX,  530-OFFY,icb_kmpr5scll 	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr1			, P7_SCL_KURSMPRIGHT1_D_00		,&l_p7_scl_kmpr5l		, 580-OFFX,  530-OFFY,icb_kmpr1scl  	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr2			, P7_SCL_KURSMPRIGHT2_D_00		,&l_p7_scl_kmpr1		, 610-OFFX,  530-OFFY,icb_kmpr2scl  	, 2 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr3			, P7_SCL_KURSMPRIGHT3_D_00		,&l_p7_scl_kmpr2		, 631-OFFX,  530-OFFY,icb_kmpr3scl  	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr4			, P7_SCL_KURSMPRIGHT4_D_00		,&l_p7_scl_kmpr3		, 652-OFFX,  530-OFFY,icb_kmpr4scl  	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_kmpr5			, P7_SCL_KURSMPRIGHT5_D_00		,&l_p7_scl_kmpr4		, 682-OFFX,  530-OFFY,icb_kmpr5scl  	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_obsr1l			, P7_SCL_OBSRIGHT1LIT_D_00		,&l_p7_scl_kmpr5		, 976-OFFX,  574-OFFY,icb_obsr1scll 	, 4 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_obsr2l			, P7_SCL_OBSRIGHT2LIT_D_00		,&l_p7_scl_obsr1l		,1034-OFFX,  574-OFFY,icb_obsr2scll 	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_obsr3l			, P7_SCL_OBSRIGHT3LIT_D_00		,&l_p7_scl_obsr2l		,1072-OFFX,  574-OFFY,icb_obsr3scll 	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_obsr1			, P7_SCL_OBSRIGHT1_D_00			,&l_p7_scl_obsr3l		, 976-OFFX,  574-OFFY,icb_obsr1scl  	, 4 *PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_obsr2			, P7_SCL_OBSRIGHT2_D_00			,&l_p7_scl_obsr1		,1034-OFFX,  574-OFFY,icb_obsr2scl  	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_scl_obsr3			, P7_SCL_OBSRIGHT3_D_00			,&l_p7_scl_obsr2		,1072-OFFX,  574-OFFY,icb_obsr3scl  	, 10*PANEL_LIGHT_MAX,p7_6)
MY_ICON2		(p7_6Ico				, P7_BACKGROUND6_D				,&l_p7_scl_obsr3		,        0,         0,icb_Ico			,    PANEL_LIGHT_MAX,p7_6)
MY_STATIC2		(p7_6bg,p7_6_list		, P7_BACKGROUND6_D				,&l_p7_6Ico				,p7_6)


#endif