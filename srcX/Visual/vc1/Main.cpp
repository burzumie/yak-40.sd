/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"

SIMPLE_GAUGE_VC(Frm1_1			,VC1_FRM1_1_D				);
SIMPLE_GAUGE_VC(Frm1_2			,VC1_FRM1_2_D				);
SIMPLE_GAUGE_VC(Frm1_3			,VC1_FRM1_3_D				);
SIMPLE_GAUGE_VC(Frm2_1			,VC1_FRM2_1_D				);
SIMPLE_GAUGE_VC(Frm2_2			,VC1_FRM2_2_D				);
SIMPLE_GAUGE_VC(Frm2_3			,VC1_FRM2_3_D				);
SIMPLE_GAUGE_VC(Frm2_4			,VC1_FRM2_4_D				);
SIMPLE_GAUGE_VC(Frm3_1			,VC1_FRM3_1_D				);
SIMPLE_GAUGE_VC(Frm3_2			,VC1_FRM3_2_D				);
SIMPLE_GAUGE_VC(Frm3_3			,VC1_FRM3_3_D				);
SIMPLE_GAUGE_VC(Frm3_4			,VC1_FRM3_4_D				);
SIMPLE_GAUGE_VC(Frm4_1			,VC1_FRM4_1_D				);
SIMPLE_GAUGE_VC(Frm4_2			,VC1_FRM4_2_D				);
SIMPLE_GAUGE_VC(Frm4_3			,VC1_FRM4_3_D				);
SIMPLE_GAUGE_VC(Frm4_4			,VC1_FRM4_4_D				);
SIMPLE_GAUGE_VC(Frmbtm1			,VC1_FRMBTM1_D				);
SIMPLE_GAUGE_VC(Frmbtm1_1		,VC1_FRMBTM1_1_D			);
SIMPLE_GAUGE_VC(Frmbtm1_2		,VC1_FRMBTM1_2_D			);
SIMPLE_GAUGE_VC(Frmbtm1_3		,VC1_FRMBTM1_3_D			);
SIMPLE_GAUGE_VC(Frmbtm1_4		,VC1_FRMBTM1_4_D			);
SIMPLE_GAUGE_VC(Frmbtm2			,VC1_FRMBTM2_D				);
SIMPLE_GAUGE_VC(Frmbtm2_1		,VC1_FRMBTM2_1_D			);
SIMPLE_GAUGE_VC(Frmbtm2_2		,VC1_FRMBTM2_2_D			);
SIMPLE_GAUGE_VC(Frmbtm2_3		,VC1_FRMBTM2_3_D			);
SIMPLE_GAUGE_VC(Frmbtm2_4		,VC1_FRMBTM2_4_D			);
SIMPLE_GAUGE_VC(Frmbtm2_5		,VC1_FRMBTM2_5_D			);
SIMPLE_GAUGE_VC(Frmbtm2_6		,VC1_FRMBTM2_6_D			);
SIMPLE_GAUGE_VC(Frmbtm2_7		,VC1_FRMBTM2_7_D			);
SIMPLE_GAUGE_VC(Frmbtm2_8		,VC1_FRMBTM2_8_D			);
SIMPLE_GAUGE_VC(Frmbtm3			,VC1_FRMBTM3_D				);
SIMPLE_GAUGE_VC(Frmfrt1			,VC1_FRMFRT1_D				);
SIMPLE_GAUGE_VC(Frmfrt2			,VC1_FRMFRT2_D				);
SIMPLE_GAUGE_VC(Frmfrt3			,VC1_FRMFRT3_D				);
SIMPLE_GAUGE_VC(Frmfrt4			,VC1_FRMFRT4_D				);
SIMPLE_GAUGE_VC(Frmfrt5			,VC1_FRMFRT5_D				);
SIMPLE_GAUGE_VC(Frmfrt6			,VC1_FRMFRT6_D				);
SIMPLE_GAUGE_VC(Frtdet1			,VC1_FRTDET1_D				);
SIMPLE_GAUGE_VC(Frtdet2			,VC1_FRTDET2_D				);
SIMPLE_GAUGE_VC(Frtdet3			,VC1_FRTDET3_D				);
SIMPLE_GAUGE_VC(Frtdet4			,VC1_FRTDET4_D				);
SIMPLE_GAUGE_VC(Frtdet6			,VC1_FRTDET6_D				);
SIMPLE_GAUGE_VC(LeftPultTop		,VC1_BCK_LEFTPULTTOP_D		);
SIMPLE_GAUGE_VC(LeftPultWallBtm	,VC1_BCK_LEFTPULTWALLBTM_D	);
SIMPLE_GAUGE_VC(LeftPultWallTop	,VC1_BCK_LEFTPULTWALLTOP_D	);
SIMPLE_GAUGE_VC(Mat				,VC1_MAT_D					);
SIMPLE_GAUGE_VC(Mat_Floor1		,VC1_BCK_FLOOR1_D			);
SIMPLE_GAUGE_VC(Mat_Floor2		,VC1_BCK_FLOOR2_D			);
SIMPLE_GAUGE_VC(Mat_Front		,VC1_BCK_FRONTWALL_D		);
SIMPLE_GAUGE_VC(Npl				,VC1_NPL_D					);
SIMPLE_GAUGE_VC(Npldet1			,VC1_NPLDET1_D				);
SIMPLE_GAUGE_VC(Npldet2			,VC1_NPLDET2_D				);
SIMPLE_GAUGE_VC(Npldet3			,VC1_NPLDET3_D				);
SIMPLE_GAUGE_VC(Npldet4			,VC1_NPLDET4_D				);
SIMPLE_GAUGE_VC(Npldet5			,VC1_NPLDET5_D				);
SIMPLE_GAUGE_VC(Rofa1			,VC1_ROFA1_D				);
SIMPLE_GAUGE_VC(Rofa2			,VC1_ROFA2_D				);
SIMPLE_GAUGE_VC(Rofa3			,VC1_ROFA3_D				);
SIMPLE_GAUGE_VC(Rofa4			,VC1_ROFA4_D				);
SIMPLE_GAUGE_VC(Rofa5			,VC1_ROFA5_D				);
SIMPLE_GAUGE_VC(Rofa6			,VC1_ROFA6_D				);
SIMPLE_GAUGE_VC(Rofa7			,VC1_ROFA7_D				);
SIMPLE_GAUGE_VC(Rofb1			,VC1_ROFB1_D				);
SIMPLE_GAUGE_VC(Rofb2			,VC1_ROFB2_D				);
SIMPLE_GAUGE_VC(Rofb3			,VC1_ROFB3_D				);
SIMPLE_GAUGE_VC(Rofb4			,VC1_ROFB4_D				);
SIMPLE_GAUGE_VC(Rofb5			,VC1_ROFB5_D				);
SIMPLE_GAUGE_VC(Rp_blt1			,VC1_RP_BLT1_D				);
SIMPLE_GAUGE_VC(Rp_blt2			,VC1_RP_BLT2_D				);
SIMPLE_GAUGE_VC(Rp_blt3			,VC1_RP_BLT3_D				);
SIMPLE_GAUGE_VC(Rp_blt4			,VC1_RP_BLT4_D				);
SIMPLE_GAUGE_VC(Rp_blt5			,VC1_RP_BLT5_D				);
SIMPLE_GAUGE_VC(Rp_blt6			,VC1_RP_BLT6_D				);
SIMPLE_GAUGE_VC(Rp_blt7			,VC1_RP_BLT7_D				);
SIMPLE_GAUGE_VC(Rp_blt8			,VC1_RP_BLT8_D				);
SIMPLE_GAUGE_VC(Rp_blt9			,VC1_RP_BLT9_D				);
SIMPLE_GAUGE_VC(Rp_blt10		,VC1_RP_BLT10_D				);
SIMPLE_GAUGE_VC(Rp_btm			,VC1_RP_BTM_D				);
SIMPLE_GAUGE_VC(Rp_top			,VC1_RP_TOP_D				);
SIMPLE_GAUGE_VC(Rp_pnl1			,VC1_RP_PNL1_D				);
SIMPLE_GAUGE_VC(Rp_pnl2			,VC1_RP_PNL2_D				);
SIMPLE_GAUGE_VC(Rp_pnl3			,VC1_RP_PNL3_D				);
SIMPLE_GAUGE_VC(Rp_pnl4			,VC1_RP_PNL4_D				);
SIMPLE_GAUGE_VC(Rp_pnl5			,VC1_RP_PNL5_D				);
SIMPLE_GAUGE_VC(Rp_shd1			,VC1_RP_SHD1_D				);
SIMPLE_GAUGE_VC(Rp_shd2			,VC1_RP_SHD2_D				);
SIMPLE_GAUGE_VC(Rp_shd3			,VC1_RP_SHD3_D				);
SIMPLE_GAUGE_VC(Rp_shd4			,VC1_RP_SHD4_D				);
SIMPLE_GAUGE_VC(Rp_shd5			,VC1_RP_SHD5_D				);
SIMPLE_GAUGE_VC(Rp_shd6			,VC1_RP_SHD6_D				);
SIMPLE_GAUGE_VC(Rp_shd7			,VC1_RP_SHD7_D				);
SIMPLE_GAUGE_VC_TGL_XML(Frt_hndl,"SD6015 anmfrt"			,VC1_FRT_HNDL_D,VC1_FRT_HNDL_D_SX,VC1_FRT_HNDL_D_SY,POS_TTGET(POS_OPEN_CLOSE_WINDOW));

#ifdef _DEBUG
DLLMAIND("VC1.log")
#else
DLLMAIN()
#endif

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&Frm1_1				),
		(&Frm1_2				),
		(&Frm1_3				),
		(&Frm2_1				),
		(&Frm2_2				),
		(&Frm2_3				),
		(&Frm2_4				),
		(&Frm3_1				),
		(&Frm3_2				),
		(&Frm3_3				),
		(&Frm3_4				),
		(&Frm4_1				),
		(&Frm4_2				),
		(&Frm4_3				),
		(&Frm4_4				),
		(&Frmbtm1				),
		(&Frmbtm1_1				),        
		(&Frmbtm1_2				),        
		(&Frmbtm1_3				),        
		(&Frmbtm1_4				),        
		(&Frmbtm2				),
		(&Frmbtm2_1				),        
		(&Frmbtm2_2				),        
		(&Frmbtm2_3				),        
		(&Frmbtm2_4				),        
		(&Frmbtm2_5				),        
		(&Frmbtm2_6				),        
		(&Frmbtm2_7				),        
		(&Frmbtm2_8				),        
		(&Frmbtm3				),
		(&Frmfrt1				),
		(&Frmfrt2				),
		(&Frmfrt3				),
		(&Frmfrt4				),
		(&Frmfrt5				),
		(&Frmfrt6				),
		(&Frtdet1				),
		(&Frtdet2				),
		(&Frtdet3				),
		(&Frtdet4				),
		(&Frtdet6				),
		(&Frt_hndl				),
		(&LeftPultTop			),        
		(&LeftPultWallBtm		),        
		(&LeftPultWallTop		),        
		(&Mat					),
		(&Mat_Floor1			),        
		(&Mat_Floor2			),        
		(&Mat_Front				),        
		(&Npl					),
		(&Npldet1				),
		(&Npldet2				),
		(&Npldet3				),
		(&Npldet4				),
		(&Npldet5				),
		(&Rofa1					),
		(&Rofa2					),
		(&Rofa3					),
		(&Rofa4					),
		(&Rofa5					),
		(&Rofa6					),
		(&Rofa7					),
		(&Rofb1					),
		(&Rofb2					),
		(&Rofb3					),
		(&Rofb4					),
		(&Rofb5					),
		(&Rp_blt1				),
		(&Rp_blt2				),
		(&Rp_blt3				),
		(&Rp_blt4				),
		(&Rp_blt5				),
		(&Rp_blt6				),
		(&Rp_blt7				),
		(&Rp_blt8				),
		(&Rp_blt9				),
		(&Rp_blt10				),
		(&Rp_btm				),
		(&Rp_top				),
		(&Rp_pnl1				),
		(&Rp_pnl2				),
		(&Rp_pnl3				),
		(&Rp_pnl4				),
		(&Rp_pnl5				),
		(&Rp_shd1				),
		(&Rp_shd2				),
		(&Rp_shd3				),
		(&Rp_shd4				),
		(&Rp_shd5				),
		(&Rp_shd6				),
		(&Rp_shd7				),
		0					
	}											
};												
							
