/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 3 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../CommonVisual.h"

#ifdef LITE_VERSION
#if		defined(HAVE_MIX_PANEL)
#include "../../../res/3d/vc1_res_m.h"
#elif	defined(HAVE_PLF_PANEL)
#include "../../../res/3d/vc1_res_p.h"
#elif	defined(HAVE_RED_PANEL)
#include "../../../res/3d/vc1_res_r.h"
#endif
#else
#include "../../../res/3d/vc1_resX.h"
#endif

GAUGE(Frm1_1			)
GAUGE(Frm1_2			)
GAUGE(Frm1_3			)
GAUGE(Frm2_1			)
GAUGE(Frm2_2			)
GAUGE(Frm2_3			)
GAUGE(Frm2_4			)
GAUGE(Frm3_1			)
GAUGE(Frm3_2			)
GAUGE(Frm3_3			)
GAUGE(Frm3_4			)
GAUGE(Frm4_1			)
GAUGE(Frm4_2			)
GAUGE(Frm4_3			)
GAUGE(Frm4_4			)
GAUGE(Frmbtm1			)	
GAUGE(Frmbtm1_1			)	
GAUGE(Frmbtm1_2			)	
GAUGE(Frmbtm1_3			)	
GAUGE(Frmbtm1_4			)	
GAUGE(Frmbtm2			)	
GAUGE(Frmbtm2_1			)	
GAUGE(Frmbtm2_2			)	
GAUGE(Frmbtm2_3			)	
GAUGE(Frmbtm2_4			)	
GAUGE(Frmbtm2_5			)	
GAUGE(Frmbtm2_6			)	
GAUGE(Frmbtm2_7			)	
GAUGE(Frmbtm2_8			)	
GAUGE(Frmbtm3			)	
GAUGE(Frmfrt1			)	
GAUGE(Frmfrt2			)	
GAUGE(Frmfrt3			)	
GAUGE(Frmfrt4			)	
GAUGE(Frmfrt5			)	
GAUGE(Frmfrt6			)
GAUGE(Frtdet1			)
GAUGE(Frtdet2			)
GAUGE(Frtdet3			)
GAUGE(Frtdet4			)
GAUGE(Frtdet6			)
GAUGE(LeftPultTop		)	
GAUGE(LeftPultWallBtm	)	
GAUGE(LeftPultWallTop	)	
GAUGE(Mat				)
GAUGE(Mat_Floor1		)
GAUGE(Mat_Floor2		)
GAUGE(Mat_Front			)
GAUGE(Npl				)
GAUGE(Npldet1			)
GAUGE(Npldet2			)
GAUGE(Npldet3			)
GAUGE(Npldet4			)
GAUGE(Npldet5			)
GAUGE(Rofa1				)
GAUGE(Rofa2				)
GAUGE(Rofa3				)
GAUGE(Rofa4				)
GAUGE(Rofa5				)
GAUGE(Rofa6				)
GAUGE(Rofa7				)
GAUGE(Rofb1				)
GAUGE(Rofb2				)
GAUGE(Rofb3				)
GAUGE(Rofb4				)
GAUGE(Rofb5				)
GAUGE(Rp_blt1			)
GAUGE(Rp_blt2			)
GAUGE(Rp_blt3			)
GAUGE(Rp_blt4			)
GAUGE(Rp_blt5			)
GAUGE(Rp_blt6			)
GAUGE(Rp_blt7			)
GAUGE(Rp_blt8			)
GAUGE(Rp_blt9			)
GAUGE(Rp_blt10			)
GAUGE(Rp_btm			)
GAUGE(Rp_top			)
GAUGE(Rp_pnl1			)
GAUGE(Rp_pnl2			)
GAUGE(Rp_pnl3			)
GAUGE(Rp_pnl4			)
GAUGE(Rp_pnl5			)
GAUGE(Rp_shd1			)
GAUGE(Rp_shd2			)
GAUGE(Rp_shd3			)
GAUGE(Rp_shd4			)
GAUGE(Rp_shd5			)
GAUGE(Rp_shd6			)
GAUGE(Rp_shd7			)
GAUGE(Frt_hndl			)
