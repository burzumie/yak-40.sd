/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P4.h"

#define OFFX	1123
#define OFFY	784

//////////////////////////////////////////////////////////////////////////

// �������
static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS4_01,0);
		POS_SET(POS_CRS4_02,0);
		POS_SET(POS_CRS4_03,0);
	}
	return true;
}

#ifndef ONLY3D

MOUSE_BEGIN(rect_p4_6,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P4_BACKGROUND6_D_SX,P4_BACKGROUND6_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p4_6_list; 
GAUGE_HEADER_FS700_EX(P4_BACKGROUND6_D_SX, "p4_6", &p4_6_list, rect_p4_6, 0, 0, 0, 0, p4_6); 

MY_ICON2	(p4_6Ico			,P4_BACKGROUND6_D	,NULL		,0,0,icb_Ico,PANEL_LIGHT_MAX,p4_6) 
MY_STATIC2	(p4_6bg,p4_6_list	,P4_BACKGROUND6_D	,&l_p4_6Ico	,p4_6);


#endif