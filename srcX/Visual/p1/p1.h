/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Visual/p0/P0.h $

  Last modification:
    $Date: 19.02.06 10:23 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include "../CommonVisual.h"

#ifdef LITE_VERSION
#if		defined(HAVE_MIX_PANEL)
#include "../../../res/2d/p1_res_m.h"
#elif	defined(HAVE_PLF_PANEL)
#ifdef ONLY3D
#include "../../../res/2d/p1_res_p3d.h"
#else
#include "../../../res/2d/p1_res_p.h"
#endif
#elif	defined(HAVE_RED_PANEL)
#include "../../../res/2d/p1_res_r.h"
#endif
#else
#ifdef ONLY3D
#include "../../../res/2d/p1_res3d.h"
#else
#include "../../../res/2d/p1_resX.h"
#endif
#endif

GAUGE(p1_1)
GAUGE(p1_2)
GAUGE(p1_3)
GAUGE(p1_4)
GAUGE(p1_5)
GAUGE(p1_6)
GAUGE(p1_7)

