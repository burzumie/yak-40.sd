/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P1.h"

#define OFFX	0
#define OFFY	468

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_crs_p1_03,SHOW_POSM(POS_CRS1_03,1,0))
static MAKE_ICB(icb_crs_p1_04,SHOW_POSM(POS_CRS1_04,1,0))
static MAKE_ICB(icb_tablo34,SHOW_TBL(TBL_FIRE				,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo35,SHOW_TBL(TBL_CABIN_CREW			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo36,SHOW_TBL(TBL_ENG_OVERHEAT		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo37,SHOW_TBL(TBL_AIR_STARTER_ON		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo01,double R;SHOW_TBG(TBG_WARN_GEAR		,BTN_GET(BTN_GEAR_LIGHTS_TEST),3,R); return R;)
static MAKE_ICB(icb_tablo02,double R;SHOW_TBG(TBG_GEAR1_UP		,BTN_GET(BTN_GEAR_LIGHTS_TEST),3,R); return R;)
static MAKE_ICB(icb_tablo03,double R;SHOW_TBG(TBG_GEAR2_UP		,BTN_GET(BTN_GEAR_LIGHTS_TEST),3,R); return R;)
static MAKE_ICB(icb_tablo04,double R;SHOW_TBG(TBG_GEAR3_UP		,BTN_GET(BTN_GEAR_LIGHTS_TEST),3,R); return R;)
static MAKE_ICB(icb_tablo05,double R;SHOW_TBG(TBG_WARN_FLAP		,BTN_GET(BTN_GEAR_LIGHTS_TEST),3,R); return R;)
static MAKE_ICB(icb_tablo06,double R;SHOW_TBG(TBG_GEAR1_DOWN	,BTN_GET(BTN_GEAR_LIGHTS_TEST),3,R); return R;)
static MAKE_ICB(icb_tablo07,double R;SHOW_TBG(TBG_GEAR2_DOWN	,BTN_GET(BTN_GEAR_LIGHTS_TEST),3,R); return R;)
static MAKE_ICB(icb_tablo08,double R;SHOW_TBG(TBG_GEAR3_DOWN	,BTN_GET(BTN_GEAR_LIGHTS_TEST),3,R); return R;)
static MAKE_ICB(icb_fire9  ,SHOW_BTN(BTN_DIS_FIRE_ALARM     ,2))

// ������������
static NONLINEARITY tbl_stab[]={
	{{1277-1165-OFFX,688-OFFY},  -9,0},
	{{1283-1165-OFFX,694-OFFY},  -7,0},
	{{1286-1165-OFFX,702-OFFY},  -5,0},
	{{1287-1165-OFFX,711-OFFY},  -3,0},
	{{1287-1165-OFFX,715-OFFY},  -1,0},
	{{1287-1165-OFFX,720-OFFY},   1,0},
	{{1285-1165-OFFX,724-OFFY},   3,0}
};

static MAKE_NCB(icb_stabD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_HTAIL_DEG);}else{return -3;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_stabM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_HTAIL_DEG);}else{return -3;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_stabP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_HTAIL_DEG);}else{return -3;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_stabR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_HTAIL_DEG);}else{return -3;}}else HIDE_IMAGE(pelement);return -1;)

// ��������
static NONLINEARITY tbl_flaps[]={
	{{1511-1165-OFFX,682-OFFY},   0,0},
	{{1524-1165-OFFX,701-OFFY},  20,0},
	{{1526-1165-OFFX,715-OFFY},  35,0}
};

static MAKE_NCB(icb_flapsD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_FLAPS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flapsM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_FLAPS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flapsP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_FLAPS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flapsR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_FLAPS);}else HIDE_IMAGE(pelement);return -1;)

// ���
static NONLINEARITY tbl_fuel_press_eng1[]={
	{{ 27-OFFX,1070-OFFY}, -10,0},
	{{ 30-OFFX,1067-OFFY},   0,0},
	{{ 70-OFFX,1044-OFFY},  50,0},
	{{111-OFFX,1066-OFFY}, 100,0}
};					

static NONLINEARITY tbl_oil_press_eng1[]={
	{{1212-1165-OFFX,1086-OFFY},   8,0},
	{{1224-1165-OFFX,1098-OFFY},   6,0},
	{{1229-1165-OFFX,1115-OFFY},   4,0},
	{{1225-1165-OFFX,1131-OFFY},   2,0},
	{{1213-1165-OFFX,1144-OFFY},   0,0},
	{{1209-1165-OFFX,1144-OFFY}, -10,0}
};					

static NONLINEARITY tbl_oil_temp_eng1[]={
	{{1263-1165-OFFX,1146-OFFY},-100,0},
	{{1260-1165-OFFX,1144-OFFY}, -50,0},
	{{1248-1165-OFFX,1131-OFFY},   0,0},
	{{1244-1165-OFFX,1115-OFFY},  50,0},
	{{1248-1165-OFFX,1099-OFFY}, 100,0},
	{{1261-1165-OFFX,1086-OFFY}, 150,0}
};					

static NONLINEARITY tbl_fuel_press_eng2[]={
	{{1339-1165-OFFX,1071-OFFY}, -10,0},
	{{1342-1165-OFFX,1066-OFFY},   0,0},
	{{1383-1165-OFFX,1043-OFFY},  50,0},
	{{1423-1165-OFFX,1066-OFFY}, 100,0}
};					

static NONLINEARITY tbl_oil_press_eng2[]={
	{{1358-1165-OFFX,1086-OFFY},   8,0},
	{{1370-1165-OFFX,1098-OFFY},   6,0},
	{{1374-1165-OFFX,1115-OFFY},   4,0},
	{{1370-1165-OFFX,1131-OFFY},   2,0},
	{{1358-1165-OFFX,1143-OFFY},   0,0},
	{{1355-1165-OFFX,1145-OFFY}, -10,0}
};					

static NONLINEARITY tbl_oil_temp_eng2[]={
	{{1410-1165-OFFX,1146-OFFY},-100,0},
	{{1406-1165-OFFX,1143-OFFY}, -50,0},
	{{1394-1165-OFFX,1131-OFFY},   0,0},
	{{1390-1165-OFFX,1114-OFFY},  50,0},
	{{1394-1165-OFFX,1098-OFFY}, 100,0},
	{{1406-1165-OFFX,1086-OFFY}, 150,0}
};					

static NONLINEARITY tbl_fuel_press_eng3[]={
	{{1488-1165-OFFX,1070-OFFY}, -10,0},
	{{1490-1165-OFFX,1067-OFFY},   0,0},
	{{1530-1165-OFFX,1043-OFFY},  50,0},
	{{1570-1165-OFFX,1066-OFFY}, 100,0}
};					

static NONLINEARITY tbl_oil_press_eng3[]={
	{{1505-1165-OFFX,1086-OFFY},   8,0},
	{{1517-1165-OFFX,1098-OFFY},   6,0},
	{{1522-1165-OFFX,1114-OFFY},   4,0},
	{{1518-1165-OFFX,1132-OFFY},   2,0},
	{{1506-1165-OFFX,1144-OFFY},   0,0},
	{{1502-1165-OFFX,1145-OFFY}, -10,0}
};					

static NONLINEARITY tbl_oil_temp_eng3[]={
	{{1557-1165-OFFX,1146-OFFY},-100,0},
	{{1554-1165-OFFX,1143-OFFY}, -50,0},
	{{1541-1165-OFFX,1131-OFFY},   0,0},
	{{1537-1165-OFFX,1115-OFFY},  50,0},
	{{1541-1165-OFFX,1099-OFFY}, 100,0},
	{{1553-1165-OFFX,1086-OFFY}, 150,0}
};					

static MAKE_NCB(icb_fuel_press_eng1D,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng1M,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng1P,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng1R,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng1D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng1M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng1P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng1R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng1D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng1M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng1P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng1R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng2D,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng2M,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng2P,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng2R,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng2D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng2M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng2P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng2R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng2D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng2M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng2P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng2R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng3D,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng3M,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng3P,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_fuel_press_eng3R,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng3D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng3M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng3P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_press_eng3R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng3D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng3M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng3P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_oil_temp_eng3R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)

static NONLINEARITY tbl_eng1[]={
	{{115-OFFX,906-OFFY},   0,0},
	{{115-OFFX,959-OFFY},  20,0},
	{{ 71-OFFX,985-OFFY},  40,0},
	{{ 26-OFFX,959-OFFY},  60,0},
	{{ 25-OFFX,906-OFFY},  80,0},
	{{ 56-OFFX,879-OFFY},  95,0},
	{{ 70-OFFX,878-OFFY}, 100,0}
};		  

static NONLINEARITY tbl_eng2[]={
	{{261-OFFX,906-OFFY},   0,0},
	{{261-OFFX,959-OFFY},  20,0},
	{{217-OFFX,985-OFFY},  40,0},
	{{172-OFFX,959-OFFY},  60,0},
	{{171-OFFX,906-OFFY},  80,0},
	{{202-OFFX,879-OFFY},  95,0},
	{{216-OFFX,878-OFFY}, 100,0}
};		  

static NONLINEARITY tbl_eng3[]={
	{{408-OFFX,906-OFFY},   0,0},
	{{408-OFFX,959-OFFY},  20,0},
	{{364-OFFX,985-OFFY},  40,0},
	{{319-OFFX,959-OFFY},  60,0},
	{{318-OFFX,906-OFFY},  80,0},
	{{349-OFFX,879-OFFY},  95,0},
	{{363-OFFX,878-OFFY}, 100,0}
};		  

static MAKE_NCB(icb_eng1_n1D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng1_n1M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng1_n1P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng1_n1R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng1_n2D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng1_n2M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng1_n2P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng1_n2R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng2_n1D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng2_n1M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng2_n1P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng2_n1R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng2_n2D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng2_n2M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng2_n2P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng2_n2R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng3_n1D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng3_n1M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng3_n1P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng3_n1R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng3_n2D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng3_n2M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng3_n2P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_eng3_n2R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N2			);}else HIDE_IMAGE(pelement);return -1;)

static NONLINEARITY tbl_teng1[]={
	{{ 87-OFFX,831-OFFY},   0,0},
	{{ 95-OFFX,790-OFFY}, 300,0},
	{{135-OFFX,791-OFFY}, 600,0},
	{{143-OFFX,828-OFFY}, 900,0}
};

static NONLINEARITY tbl_teng2[]={
	{{191-OFFX,831-OFFY},   0,0},
	{{199-OFFX,790-OFFY}, 300,0},
	{{240-OFFX,791-OFFY}, 600,0},
	{{247-OFFX,828-OFFY}, 900,0}
};

static NONLINEARITY tbl_teng3[]={
	{{298-OFFX,831-OFFY},   0,0},
	{{306-OFFX,790-OFFY}, 300,0},
	{{346-OFFX,791-OFFY}, 600,0},
	{{353-OFFX,828-OFFY}, 900,0}
};

static MAKE_NCB(icb_teng1D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng1M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng1P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng1R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng2D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng2M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng2P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng2R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng3D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng3M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng3P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_teng3R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_TEMP			);}else HIDE_IMAGE(pelement);return -1;)

static MAKE_ICB(icb_CovED	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);return 0;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_CovEM	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);return 0;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_CovEP	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);return 0;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_CovER	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);return 0;}else HIDE_IMAGE(pelement);return -1;)

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_fire9				,PRS_BTN(BTN_DIS_FIRE_ALARM				))
static MAKE_MSCB(mcb_check_lamps_gear	,PRS_BTN(BTN_GEAR_LIGHTS_TEST			))

// �������
static BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS1_01,0);
		POS_SET(POS_CRS1_02,0);
		POS_SET(POS_CRS1_03,0);
		POS_SET(POS_CRS1_04,0);
		POS_SET(POS_YOKE1_ICON,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p1_03(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS1_03,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P3);
		panel_window_close_ident(IDENT_P1);
		panel_window_open_ident(IDENT_P4);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p1_04(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS1_04,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P3);
		panel_window_close_ident(IDENT_P1);
		panel_window_open_ident(IDENT_P0);
	}
	return true;
}

static BOOL FSAPI mcb_navpanel_toggle(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P23);
	return TRUE;
}

static MAKE_TCB(tcb_01	,POS_TT(POS_CRS1_03						))
static MAKE_TCB(tcb_02	,POS_TT(POS_CRS1_04						))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_FIRE_LIGHTS_TEST			))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_GEAR_LIGHTS_TEST			))
static MAKE_TCB(tcb_05	,TBL_TT(TBL_FIRE						))
static MAKE_TCB(tcb_06	,TBL_TT(TBL_CABIN_CREW					))
static MAKE_TCB(tcb_07	,TBL_TT(TBL_ENG_OVERHEAT				))
static MAKE_TCB(tcb_08	,TBL_TT(TBL_AIR_STARTER_ON				))
static MAKE_TCB(tcb_09	,TBG_TT(TBG_WARN_GEAR		            ))
static MAKE_TCB(tcb_10	,TBG_TT(TBG_GEAR1_UP		            ))
static MAKE_TCB(tcb_11	,TBG_TT(TBG_GEAR2_UP		            ))
static MAKE_TCB(tcb_12	,TBG_TT(TBG_GEAR3_UP		            ))
static MAKE_TCB(tcb_13	,TBG_TT(TBG_WARN_FLAP		            ))
static MAKE_TCB(tcb_14	,TBG_TT(TBG_GEAR1_DOWN					))
static MAKE_TCB(tcb_15	,TBG_TT(TBG_GEAR2_DOWN					))
static MAKE_TCB(tcb_16	,TBG_TT(TBG_GEAR3_DOWN					))
static MAKE_TCB(tcb_17	,NDL_TT(NDL_HTAIL_DEG					))
static MAKE_TCB(tcb_18	,NDL_TT(NDL_FLAPS						))
static MAKE_TCB(tcb_19	,NDL_TT(NDL_ENG1_TEMP					))
static MAKE_TCB(tcb_20	,NDL_TT(NDL_ENG2_TEMP					))
static MAKE_TCB(tcb_21	,NDL_TT(NDL_ENG3_TEMP					))
static MAKE_TCB(tcb_22	,NDL_TT(NDL_ENG1_N1						))
static MAKE_TCB(tcb_23	,NDL_TT(NDL_ENG2_N1						))
static MAKE_TCB(tcb_24	,NDL_TT(NDL_ENG3_N1						))
static MAKE_TCB(tcb_25	,NDL_TT(NDL_ENG1_FUEL_PSI				))
static MAKE_TCB(tcb_26	,NDL_TT(NDL_ENG2_FUEL_PSI				))
static MAKE_TCB(tcb_27	,NDL_TT(NDL_ENG3_FUEL_PSI				))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MAKE_TTA(tcb_20	)
MAKE_TTA(tcb_21	)
MAKE_TTA(tcb_22	) 
MAKE_TTA(tcb_23	)
MAKE_TTA(tcb_24	)
MAKE_TTA(tcb_25	)
MAKE_TTA(tcb_26	)
MAKE_TTA(tcb_27	) 
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p1_3,HELP_NONE,0,0)

// �������� ��������
MOUSE_PBOX(0,0,P1_BACKGROUND3_SX,P1_BACKGROUND3_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_PBOX(  0, 20,45,164-20,CURSOR_HAND,MOUSE_LR,mcb_navpanel_toggle)

// �������
MOUSE_TBOX(  "1",88-OFFX, 1149-OFFY,MISC_CRS_P1_03_SX,MISC_CRS_P1_03_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p1_03)
MOUSE_TBOX(  "2",0-OFFX,  555-OFFY,MISC_CRS_P1_04_SX,MISC_CRS_P1_04_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p1_04)

// �������
MOUSE_TTPB("17", 72-OFFX, 679-OFFY,  60,  60)    // NDL_HTAIL_DEG					
MOUSE_TTPB("18",303-OFFX, 679-OFFY,  60,  60)    // NDL_FLAPS						
MOUSE_TTPB("19", 70-OFFX, 774-OFFY,  80,  80)    // NDL_ENG1_TEMP					
MOUSE_TTPB("20",177-OFFX, 774-OFFY,  80,  80)    // NDL_ENG2_TEMP					
MOUSE_TTPB("21",283-OFFX, 774-OFFY,  80,  80)    // NDL_ENG3_TEMP					
MOUSE_TTPB("22",  0-OFFX, 860-OFFY, 140, 140)    // NDL_ENG1_N1					
MOUSE_TTPB("23",146-OFFX, 860-OFFY, 140, 140)    // NDL_ENG2_N1					
MOUSE_TTPB("24",292-OFFX, 860-OFFY, 140, 140)    // NDL_ENG3_N1					
MOUSE_TTPB("25",  0-OFFX,1024-OFFY, 140, 140)    // NDL_ENG1_FUEL_PSI				
MOUSE_TTPB("26",146-OFFX,1024-OFFY, 140, 140)    // NDL_ENG2_FUEL_PSI				
MOUSE_TTPB("27",292-OFFX,1024-OFFY, 140, 140)    // NDL_ENG3_FUEL_PSI				

// ������      
MOUSE_TBOX( "3", 0-OFFX,  830-OFFY, 30, 30,CURSOR_HAND,MOUSE_DLR,mcb_fire9)
MOUSE_TBOX( "4", 202-OFFX,718-OFFY, 30, 30,CURSOR_HAND,MOUSE_DLR,mcb_check_lamps_gear)

// ����� 
MOUSE_TTPB( "5",   0-OFFX,  711-OFFY, 64,46)    // TBL_FIRE                      		
MOUSE_TTPB( "6", 362-OFFX,  715-OFFY, 65,46)    // TBL_CABIN_CREW                		
MOUSE_TTPB( "7", 362-OFFX,  764-OFFY, 65,47)    // TBL_ENG_OVERHEAT              		
MOUSE_TTPB( "8", 362-OFFX,  807-OFFY, 65,48)    // TBL_AIR_STARTER_ON            		

// ����� �����
MOUSE_TTPB(  "9", 141-OFFX, 672-OFFY, 48,36)    // TBG_WARN_GEAR		           		
MOUSE_TTPB( "10", 184-OFFX, 672-OFFY, 21,34)    // TBG_GEAR1_UP		           		
MOUSE_TTPB( "11", 205-OFFX, 664-OFFY, 24,30)    // TBG_GEAR2_UP		           		
MOUSE_TTPB( "12", 227-OFFX, 672-OFFY, 21,34)    // TBG_GEAR3_UP		           		
MOUSE_TTPB( "13", 249-OFFX, 672-OFFY, 43,34)    // TBG_WARN_FLAP		           		
MOUSE_TTPB( "14", 184-OFFX, 701-OFFY, 21,26)    // TBG_GEAR1_DOWN		           		
MOUSE_TTPB( "15", 205-OFFX, 691-OFFY, 24,37)    // TBG_GEAR2_DOWN		           		
MOUSE_TTPB( "16", 227-OFFX, 701-OFFY, 21,26)    // TBG_GEAR3_DOWN	

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p1_3_list; 
GAUGE_HEADER_FS700_EX(P1_BACKGROUND3_SX, "p1_3", &p1_3_list, rect_p1_3, 0, 0, 0, 0, p1_3); 

#define GAU_ALL4(S,E,CX,CY,NX,NY,NXC,NYC,L) /**/ \
	MY_ICON2  (cov_p1_temp_eng##E##_##S	,P1_COV_TEMPENG##E##_##S	,L,CX-OFFX,CY-OFFY,icb_CovE##S,1, p1_3) \
	MY_NEEDLE2(ndl_p1_temp_eng##E##_##S	,P1_NDL_TEMPENG##E##_##S	,&l_cov_p1_temp_eng##E##_##S,NX-OFFX,NY-OFFY,NXC,NYC,icb_teng##E##S,tbl_teng##E##,18, p1_3)

#define GAU_ALL3(S,E,CX,CY,NX1,NY1,NCX1,NCY1,L) /**/ \
	MY_ICON2  (cov_p1_rpm_eng##E##_##S	 ,P1_COV_RPMENG##E##_##S		,L,CX-OFFX,CY-OFFY,icb_CovE##S,1, p1_3)	\
	MY_NEEDLE2(ndl_p1_rpm_eng##E##_n2_##S,P1_NDL_RPMENG##E##N2_##S	,&l_cov_p1_rpm_eng##E##_##S,NX1-OFFX,NY1-OFFY,NCX1,NCY1,icb_eng##E##_n2##S,tbl_eng##E##,18, p1_3)	\
	MY_NEEDLE2(ndl_p1_rpm_eng##E##_n1_##S,P1_NDL_RPMENG##E##N1_##S	,&l_ndl_p1_rpm_eng##E##_n2_##S,NX1-OFFX,NY1-OFFY,NCX1,NCY1,icb_eng##E##_n1##S,tbl_eng##E##,18, p1_3)

#define GAU_ALL2(S,E,CX,CY,NX1,NY1,NCX1,NCY1,NX2,NY2,NCX2,NCY2,NX3,NY3,NCX3,NCY3,L)	/* */ \
	MY_ICON2  (cov_p1_emi_eng##E##_##S			,P1_COV_EMIENG##E##_##S			,L,										CX-OFFX,CY-OFFY,icb_CovE##S,1, p1_3) \
	MY_NEEDLE2(ndl_p1_emi_eng##E##_oil_temp_##S	,P1_NDL_EMIENG##E##OILTEMP_##S	,&l_cov_p1_emi_eng##E##_##S,			NX1-OFFX,NY1-OFFY,NCX1,NCY1,	icb_oil_temp_eng##E##S,		tbl_oil_temp_eng##E,18, p1_3) \
	MY_NEEDLE2(ndl_p1_emi_eng##E##_oil_press_##S,P1_NDL_EMIENG##E##OILPSI_##S	,&l_ndl_p1_emi_eng##E##_oil_temp_##S,	NX2-OFFX,NY2-OFFY,NCX2,NCY2,	icb_oil_press_eng##E##S,	tbl_oil_press_eng##E,18, p1_3) \
	MY_NEEDLE2(ndl_p1_emi_eng##E##_fuel_press_##S,P1_NDL_EMIENG##E##FUELPSI_##S	,&l_ndl_p1_emi_eng##E##_oil_press_##S,	NX3-OFFX,NY3-OFFY,NCX3,NCY3,	icb_fuel_press_eng##E##S,	tbl_fuel_press_eng##E,18, p1_3)

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
GAU_ALL2(D,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,NULL)
GAU_ALL2(D,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_D)
GAU_ALL2(D,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_D)
GAU_ALL2(M,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,&l_ndl_p1_emi_eng3_fuel_press_D)
GAU_ALL2(M,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_M)
GAU_ALL2(M,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_M)
GAU_ALL2(P,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,&l_ndl_p1_emi_eng3_fuel_press_M)
GAU_ALL2(P,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_P)
GAU_ALL2(P,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_P)
GAU_ALL2(R,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,&l_ndl_p1_emi_eng3_fuel_press_P)
GAU_ALL2(R,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_R)
GAU_ALL2(R,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_R)

GAU_ALL3(D,1, 63,928,  69,934,31,7,&l_ndl_p1_emi_eng3_fuel_press_R)
GAU_ALL3(D,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_D)
GAU_ALL3(D,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_D)
GAU_ALL3(M,1, 63,928,  69,934,31,7,&l_ndl_p1_rpm_eng3_n1_D)
GAU_ALL3(M,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_M)
GAU_ALL3(M,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_M)
GAU_ALL3(P,1, 63,928,  69,934,31,7,&l_ndl_p1_rpm_eng3_n1_M)
GAU_ALL3(P,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_P)
GAU_ALL3(P,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_P)
GAU_ALL3(R,1, 63,928,  69,934,31,7,&l_ndl_p1_rpm_eng3_n1_P)
GAU_ALL3(R,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_R)
GAU_ALL3(R,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_R)

GAU_ALL4(D,1, 97,799, 116,815,3,5,&l_ndl_p1_rpm_eng3_n1_R)
GAU_ALL4(D,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_D)
GAU_ALL4(D,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_D)
GAU_ALL4(M,1, 97,799, 116,815,3,5,&l_ndl_p1_temp_eng3_D)
GAU_ALL4(M,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_M)
GAU_ALL4(M,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_M)
GAU_ALL4(P,1, 97,799, 116,815,3,5,&l_ndl_p1_temp_eng3_M)
GAU_ALL4(P,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_P)
GAU_ALL4(P,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_P)
GAU_ALL4(R,1, 97,799, 116,815,3,5,&l_ndl_p1_temp_eng3_P)
GAU_ALL4(R,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_R)
GAU_ALL4(R,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_R)

MY_ICON2	(cov_p1_flaps       ,P1_COV_FLAPS_D         ,&l_ndl_p1_temp_eng3_R	,305-OFFX, 681-OFFY,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3)	
MY_NEEDLE2	(ndl_p1_flapsD      ,P1_NDL_FLAPS_D         ,&l_cov_p1_flaps		,327-OFFX, 711-OFFY, 4, 4,icb_flapsD	,tbl_flaps,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_flapsM      ,P1_NDL_FLAPS_M         ,&l_ndl_p1_flapsD		,327-OFFX, 711-OFFY, 4, 4,icb_flapsM	,tbl_flaps,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_flapsP      ,P1_NDL_FLAPS_P         ,&l_ndl_p1_flapsM		,327-OFFX, 711-OFFY, 4, 4,icb_flapsP	,tbl_flaps,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_flapsR      ,P1_NDL_FLAPS_R         ,&l_ndl_p1_flapsP		,327-OFFX, 711-OFFY, 4, 4,icb_flapsR	,tbl_flaps,18		, p1_3)	 
MY_ICON2	(cov_p1_stab        ,P1_COV_HTAIL_D         ,&l_ndl_p1_flapsR		, 74-OFFX, 681-OFFY,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3)	
MY_NEEDLE2	(ndl_p1_stabD       ,P1_NDL_HTAIL_D         ,&l_cov_p1_stab			, 96-OFFX, 711-OFFY, 3, 4,icb_stabD		,tbl_stab,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_stabM       ,P1_NDL_HTAIL_M         ,&l_ndl_p1_stabD		, 96-OFFX, 711-OFFY, 3, 4,icb_stabM		,tbl_stab,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_stabP       ,P1_NDL_HTAIL_P         ,&l_ndl_p1_stabM		, 96-OFFX, 711-OFFY, 3, 4,icb_stabP		,tbl_stab,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_stabR       ,P1_NDL_HTAIL_R         ,&l_ndl_p1_stabP		, 96-OFFX, 711-OFFY, 3, 4,icb_stabR		,tbl_stab,18		, p1_3)	 
MY_ICON2	(p1_gear_tablo01	,P1_TBL_GEAR1_D_00		,&l_ndl_p1_stabR		,141-OFFX, 672-OFFY,	icb_tablo01		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo02	,P1_TBL_GEAR2_D_00		,&l_p1_gear_tablo01		,184-OFFX, 672-OFFY,	icb_tablo02		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo03	,P1_TBL_GEAR3_D_00		,&l_p1_gear_tablo02		,205-OFFX, 664-OFFY,	icb_tablo03		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo04	,P1_TBL_GEAR4_D_00		,&l_p1_gear_tablo03		,227-OFFX, 672-OFFY,	icb_tablo04		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo05	,P1_TBL_GEAR5_D_00		,&l_p1_gear_tablo04		,249-OFFX, 672-OFFY,	icb_tablo05		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo06	,P1_TBL_GEAR6_D_00		,&l_p1_gear_tablo05		,184-OFFX, 701-OFFY,	icb_tablo06		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo07	,P1_TBL_GEAR7_D_00		,&l_p1_gear_tablo06		,205-OFFX, 691-OFFY,	icb_tablo07		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo08	,P1_TBL_GEAR8_D_00		,&l_p1_gear_tablo07		,227-OFFX, 701-OFFY,	icb_tablo08		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_tablo34			,P1_TBL_01_D_00			,&l_p1_gear_tablo08		,  0-OFFX, 711-OFFY,	icb_tablo34		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo35			,P1_TBL_02_D_00			,&l_p1_tablo34			,362-OFFX, 715-OFFY,	icb_tablo35		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo36			,P1_TBL_03_D_00			,&l_p1_tablo35			,362-OFFX, 764-OFFY,	icb_tablo36		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo37			,P1_TBL_04_D_00			,&l_p1_tablo36			,362-OFFX, 807-OFFY,	icb_tablo37		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(fire91				,P1_BTN_FIRE9_D_00		,&l_p1_tablo37			,  0-OFFX, 830-OFFY,	icb_fire9		,2*PANEL_LIGHT_MAX	, p1_3) 
MY_ICON2	(p1_crs_03			,MISC_CRS_P1_03			,&l_fire91				, 88-OFFX,1149-OFFY,	icb_crs_p1_03	,1					, p1_3) 
MY_ICON2	(p1_crs_04			,MISC_CRS_P1_04			,&l_p1_crs_03			,  0-OFFX, 555-OFFY,	icb_crs_p1_04	,1					, p1_3) 
MY_ICON2	(p1_3Ico			,P1_BACKGROUND3_D		,&l_p1_crs_04			,	    0,		  0,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3) 
MY_STATIC2	(p1_3bg,p1_3_list	,P1_BACKGROUND3			,&l_p1_3Ico				, p1_3);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
GAU_ALL2(D,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,NULL)
GAU_ALL2(D,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_D)
GAU_ALL2(D,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_D)
GAU_ALL2(P,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,&l_ndl_p1_emi_eng3_fuel_press_D)
GAU_ALL2(P,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_P)
GAU_ALL2(P,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_P)

GAU_ALL3(D,1, 63,928,  69,934,31,7,&l_ndl_p1_emi_eng3_fuel_press_P)
GAU_ALL3(D,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_D)
GAU_ALL3(D,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_D)
GAU_ALL3(P,1, 63,928,  69,934,31,7,&l_ndl_p1_rpm_eng3_n1_D)
GAU_ALL3(P,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_P)
GAU_ALL3(P,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_P)

GAU_ALL4(D,1, 97,799, 116,815,3,5,&l_ndl_p1_rpm_eng3_n1_P)
GAU_ALL4(D,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_D)
GAU_ALL4(D,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_D)
GAU_ALL4(P,1, 97,799, 116,815,3,5,&l_ndl_p1_temp_eng3_D)
GAU_ALL4(P,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_P)
GAU_ALL4(P,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_P)

MY_ICON2	(cov_p1_flaps       ,P1_COV_FLAPS_D         ,&l_ndl_p1_temp_eng3_P	,305-OFFX, 681-OFFY,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3)	
MY_NEEDLE2	(ndl_p1_flapsD      ,P1_NDL_FLAPS_D         ,&l_cov_p1_flaps       ,327-OFFX, 711-OFFY, 4, 4,icb_flapsD	,tbl_flaps,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_flapsP      ,P1_NDL_FLAPS_P         ,&l_ndl_p1_flapsD      ,327-OFFX, 711-OFFY, 4, 4,icb_flapsP	,tbl_flaps,18		, p1_3)	 
MY_ICON2	(cov_p1_stab        ,P1_COV_HTAIL_D         ,&l_ndl_p1_flapsP      , 74-OFFX, 681-OFFY,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3)	
MY_NEEDLE2	(ndl_p1_stabD       ,P1_NDL_HTAIL_D         ,&l_cov_p1_stab        , 96-OFFX, 711-OFFY, 3, 4,icb_stabD		,tbl_stab,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_stabP       ,P1_NDL_HTAIL_P         ,&l_ndl_p1_stabD       , 96-OFFX, 711-OFFY, 3, 4,icb_stabP		,tbl_stab,18		, p1_3)	 
MY_ICON2	(p1_gear_tablo01	,P1_TBL_GEAR1_D_00		,&l_ndl_p1_stabP       ,141-OFFX, 672-OFFY,	icb_tablo01		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo02	,P1_TBL_GEAR2_D_00		,&l_p1_gear_tablo01	,184-OFFX, 672-OFFY,	icb_tablo02		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo03	,P1_TBL_GEAR3_D_00		,&l_p1_gear_tablo02	,205-OFFX, 664-OFFY,	icb_tablo03		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo04	,P1_TBL_GEAR4_D_00		,&l_p1_gear_tablo03	,227-OFFX, 672-OFFY,	icb_tablo04		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo05	,P1_TBL_GEAR5_D_00		,&l_p1_gear_tablo04	,249-OFFX, 672-OFFY,	icb_tablo05		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo06	,P1_TBL_GEAR6_D_00		,&l_p1_gear_tablo05	,184-OFFX, 701-OFFY,	icb_tablo06		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo07	,P1_TBL_GEAR7_D_00		,&l_p1_gear_tablo06	,205-OFFX, 691-OFFY,	icb_tablo07		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo08	,P1_TBL_GEAR8_D_00		,&l_p1_gear_tablo07	,227-OFFX, 701-OFFY,	icb_tablo08		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_tablo34			,P1_TBL_01_D_00			,&l_p1_gear_tablo08	,  0-OFFX, 711-OFFY,	icb_tablo34		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo35			,P1_TBL_02_D_00			,&l_p1_tablo34		,362-OFFX, 715-OFFY,	icb_tablo35		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo36			,P1_TBL_03_D_00			,&l_p1_tablo35		,362-OFFX, 764-OFFY,	icb_tablo36		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo37			,P1_TBL_04_D_00			,&l_p1_tablo36		,362-OFFX, 807-OFFY,	icb_tablo37		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(fire91				,P1_BTN_FIRE9_D_00		,&l_p1_tablo37		,  0-OFFX, 830-OFFY,	icb_fire9		,2*PANEL_LIGHT_MAX	, p1_3) 
MY_ICON2	(p1_crs_03			,MISC_CRS_P1_03			,&l_fire91			, 88-OFFX,1149-OFFY,	icb_crs_p1_03	,1					, p1_3) 
MY_ICON2	(p1_crs_04			,MISC_CRS_P1_04			,&l_p1_crs_03		,  0-OFFX, 555-OFFY,	icb_crs_p1_04	,1					, p1_3) 
MY_ICON2	(p1_3Ico			,P1_BACKGROUND3_D		,&l_p1_crs_04		,	    0,		  0,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3) 
MY_STATIC2	(p1_3bg,p1_3_list	,P1_BACKGROUND3			,&l_p1_3Ico			, p1_3);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
GAU_ALL2(D,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,NULL)
GAU_ALL2(D,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_D)
GAU_ALL2(D,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_D)
GAU_ALL2(R,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,&l_ndl_p1_emi_eng3_fuel_press_D)
GAU_ALL2(R,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_R)
GAU_ALL2(R,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_R)

GAU_ALL3(D,1, 63,928,  69,934,31,7,&l_ndl_p1_emi_eng3_fuel_press_R)
GAU_ALL3(D,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_D)
GAU_ALL3(D,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_D)
GAU_ALL3(R,1, 63,928,  69,934,31,7,&l_ndl_p1_rpm_eng3_n1_D)
GAU_ALL3(R,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_R)
GAU_ALL3(R,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_R)

GAU_ALL4(D,1, 97,799, 116,815,3,5,&l_ndl_p1_rpm_eng3_n1_R)
GAU_ALL4(D,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_D)
GAU_ALL4(D,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_D)
GAU_ALL4(R,1, 97,799, 116,815,3,5,&l_ndl_p1_temp_eng3_D)
GAU_ALL4(R,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_R)
GAU_ALL4(R,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_R)

MY_ICON2	(cov_p1_flaps       ,P1_COV_FLAPS_D         ,&l_ndl_p1_temp_eng3_R	,305-OFFX, 681-OFFY,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3)	
MY_NEEDLE2	(ndl_p1_flapsD      ,P1_NDL_FLAPS_D         ,&l_cov_p1_flaps       ,327-OFFX, 711-OFFY, 4, 4,icb_flapsD	,tbl_flaps,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_flapsR      ,P1_NDL_FLAPS_R         ,&l_ndl_p1_flapsD      ,327-OFFX, 711-OFFY, 4, 4,icb_flapsR	,tbl_flaps,18		, p1_3)	 
MY_ICON2	(cov_p1_stab        ,P1_COV_HTAIL_D         ,&l_ndl_p1_flapsR      , 74-OFFX, 681-OFFY,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3)	
MY_NEEDLE2	(ndl_p1_stabD       ,P1_NDL_HTAIL_D         ,&l_cov_p1_stab        , 96-OFFX, 711-OFFY, 3, 4,icb_stabD		,tbl_stab,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_stabR       ,P1_NDL_HTAIL_R         ,&l_ndl_p1_stabD       , 96-OFFX, 711-OFFY, 3, 4,icb_stabR		,tbl_stab,18		, p1_3)	 
MY_ICON2	(p1_gear_tablo01	,P1_TBL_GEAR1_D_00		,&l_ndl_p1_stabR       ,141-OFFX, 672-OFFY,	icb_tablo01		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo02	,P1_TBL_GEAR2_D_00		,&l_p1_gear_tablo01	,184-OFFX, 672-OFFY,	icb_tablo02		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo03	,P1_TBL_GEAR3_D_00		,&l_p1_gear_tablo02	,205-OFFX, 664-OFFY,	icb_tablo03		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo04	,P1_TBL_GEAR4_D_00		,&l_p1_gear_tablo03	,227-OFFX, 672-OFFY,	icb_tablo04		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo05	,P1_TBL_GEAR5_D_00		,&l_p1_gear_tablo04	,249-OFFX, 672-OFFY,	icb_tablo05		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo06	,P1_TBL_GEAR6_D_00		,&l_p1_gear_tablo05	,184-OFFX, 701-OFFY,	icb_tablo06		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo07	,P1_TBL_GEAR7_D_00		,&l_p1_gear_tablo06	,205-OFFX, 691-OFFY,	icb_tablo07		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo08	,P1_TBL_GEAR8_D_00		,&l_p1_gear_tablo07	,227-OFFX, 701-OFFY,	icb_tablo08		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_tablo34			,P1_TBL_01_D_00			,&l_p1_gear_tablo08	,  0-OFFX, 711-OFFY,	icb_tablo34		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo35			,P1_TBL_02_D_00			,&l_p1_tablo34			,362-OFFX, 715-OFFY,	icb_tablo35		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo36			,P1_TBL_03_D_00			,&l_p1_tablo35			,362-OFFX, 764-OFFY,	icb_tablo36		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo37			,P1_TBL_04_D_00			,&l_p1_tablo36			,362-OFFX, 807-OFFY,	icb_tablo37		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(fire91				,P1_BTN_FIRE9_D_00		,&l_p1_tablo37			,  0-OFFX, 830-OFFY,	icb_fire9		,2*PANEL_LIGHT_MAX	, p1_3) 
MY_ICON2	(p1_crs_03			,MISC_CRS_P1_03			,&l_fire91				, 88-OFFX,1149-OFFY,	icb_crs_p1_03	,1					, p1_3) 
MY_ICON2	(p1_crs_04			,MISC_CRS_P1_04			,&l_p1_crs_03			,  0-OFFX, 555-OFFY,	icb_crs_p1_04	,1					, p1_3) 
MY_ICON2	(p1_3Ico			,P1_BACKGROUND3_D		,&l_p1_crs_04			,	    0,		  0,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3) 
MY_STATIC2	(p1_3bg,p1_3_list	,P1_BACKGROUND3			,&l_p1_3Ico			, p1_3);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
GAU_ALL2(D,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,NULL)
GAU_ALL2(D,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_D)
GAU_ALL2(D,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_D)
GAU_ALL2(M,1, 18,1067, 109,1116,3,5,  35,1116,4,5,  71,1084,4,6,&l_ndl_p1_emi_eng3_fuel_press_D)
GAU_ALL2(M,2,164,1067, 254,1116,3,5, 180,1116,3,5, 217,1084,3,6,&l_ndl_p1_emi_eng1_fuel_press_M)
GAU_ALL2(M,3,311,1067, 402,1116,3,5, 328,1116,3,5, 364,1084,3,6,&l_ndl_p1_emi_eng2_fuel_press_M)

GAU_ALL3(D,1, 63,928,  69,934,31,7,&l_ndl_p1_emi_eng3_fuel_press_M)
GAU_ALL3(D,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_D)
GAU_ALL3(D,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_D)
GAU_ALL3(M,1, 63,928,  69,934,31,7,&l_ndl_p1_rpm_eng3_n1_D)
GAU_ALL3(M,2,209,928, 216,934,31,7,&l_ndl_p1_rpm_eng1_n1_M)
GAU_ALL3(M,3,356,928, 363,934,31,7,&l_ndl_p1_rpm_eng2_n1_M)

GAU_ALL4(D,1, 97,799, 116,815,3,5,&l_ndl_p1_rpm_eng3_n1_M)
GAU_ALL4(D,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_D)
GAU_ALL4(D,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_D)
GAU_ALL4(M,1, 97,799, 116,815,3,5,&l_ndl_p1_temp_eng3_D)
GAU_ALL4(M,2,201,799, 220,815,3,5,&l_ndl_p1_temp_eng1_M)
GAU_ALL4(M,3,308,799, 327,815,2,5,&l_ndl_p1_temp_eng2_M)

MY_ICON2	(cov_p1_flaps       ,P1_COV_FLAPS_D         ,&l_ndl_p1_temp_eng3_M	,305-OFFX, 681-OFFY,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3)	
MY_NEEDLE2	(ndl_p1_flapsD      ,P1_NDL_FLAPS_D         ,&l_cov_p1_flaps       ,327-OFFX, 711-OFFY, 4, 4,icb_flapsD	,tbl_flaps,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_flapsM      ,P1_NDL_FLAPS_M         ,&l_ndl_p1_flapsD      ,327-OFFX, 711-OFFY, 4, 4,icb_flapsM	,tbl_flaps,18		, p1_3)	 
MY_ICON2	(cov_p1_stab        ,P1_COV_HTAIL_D         ,&l_ndl_p1_flapsM      , 74-OFFX, 681-OFFY,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3)	
MY_NEEDLE2	(ndl_p1_stabD       ,P1_NDL_HTAIL_D         ,&l_cov_p1_stab        , 96-OFFX, 711-OFFY, 3, 4,icb_stabD		,tbl_stab,18		, p1_3)	 
MY_NEEDLE2	(ndl_p1_stabM       ,P1_NDL_HTAIL_M         ,&l_ndl_p1_stabD       , 96-OFFX, 711-OFFY, 3, 4,icb_stabM		,tbl_stab,18		, p1_3)	 
MY_ICON2	(p1_gear_tablo01	,P1_TBL_GEAR1_D_00		,&l_ndl_p1_stabM       ,141-OFFX, 672-OFFY,	icb_tablo01		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo02	,P1_TBL_GEAR2_D_00		,&l_p1_gear_tablo01	,184-OFFX, 672-OFFY,	icb_tablo02		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo03	,P1_TBL_GEAR3_D_00		,&l_p1_gear_tablo02	,205-OFFX, 664-OFFY,	icb_tablo03		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo04	,P1_TBL_GEAR4_D_00		,&l_p1_gear_tablo03	,227-OFFX, 672-OFFY,	icb_tablo04		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo05	,P1_TBL_GEAR5_D_00		,&l_p1_gear_tablo04	,249-OFFX, 672-OFFY,	icb_tablo05		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo06	,P1_TBL_GEAR6_D_00		,&l_p1_gear_tablo05	,184-OFFX, 701-OFFY,	icb_tablo06		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo07	,P1_TBL_GEAR7_D_00		,&l_p1_gear_tablo06	,205-OFFX, 691-OFFY,	icb_tablo07		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_gear_tablo08	,P1_TBL_GEAR8_D_00		,&l_p1_gear_tablo07	,227-OFFX, 701-OFFY,	icb_tablo08		,3*PANEL_LIGHT_MAX	, p1_3)	
MY_ICON2	(p1_tablo34			,P1_TBL_01_D_00			,&l_p1_gear_tablo08	,  0-OFFX, 711-OFFY,	icb_tablo34		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo35			,P1_TBL_02_D_00			,&l_p1_tablo34			,362-OFFX, 715-OFFY,	icb_tablo35		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo36			,P1_TBL_03_D_00			,&l_p1_tablo35			,362-OFFX, 764-OFFY,	icb_tablo36		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(p1_tablo37			,P1_TBL_04_D_00			,&l_p1_tablo36			,362-OFFX, 807-OFFY,	icb_tablo37		,3*PANEL_LIGHT_MAX	, p1_3) 	
MY_ICON2	(fire91				,P1_BTN_FIRE9_D_00		,&l_p1_tablo37			,  0-OFFX, 830-OFFY,	icb_fire9		,2*PANEL_LIGHT_MAX	, p1_3) 
MY_ICON2	(p1_crs_03			,MISC_CRS_P1_03			,&l_fire91				, 88-OFFX,1149-OFFY,	icb_crs_p1_03	,1					, p1_3) 
MY_ICON2	(p1_crs_04			,MISC_CRS_P1_04			,&l_p1_crs_03			,  0-OFFX, 555-OFFY,	icb_crs_p1_04	,1					, p1_3) 
MY_ICON2	(p1_3Ico			,P1_BACKGROUND3_D		,&l_p1_crs_04			,	    0,		  0,	icb_Ico			,PANEL_LIGHT_MAX	, p1_3) 
MY_STATIC2	(p1_3bg,p1_3_list	,P1_BACKGROUND3			,&l_p1_3Ico			, p1_3);
#endif


#endif