/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P1.h"

#define OFFX	0
#define OFFY	0

static MAKE_NCB(icb_secD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_secM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_secP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_secR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_blenker			,SHOW_POS(POS_ACHS_BLENKER		,3))
static MAKE_ICB(icb_achs_right		,SHOW_BTN(BTN_CLOCK_START			 ,2))
static MAKE_ICB(icb_achs_left		,SHOW_BTN(BTN_CLOCK_HRONO			 ,2))

static MAKE_ICB(icb_lamp01,SHOW_LMP(LMP_ENG1_BAY_ON_FIRE			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp02,SHOW_LMP(LMP_ENG2_BAY_ON_FIRE			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp03,SHOW_LMP(LMP_ENG3_BAY_ON_FIRE			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp04,SHOW_LMP(LMP_ENG1_ON_FIRE				,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp05,SHOW_LMP(LMP_ENG2_ON_FIRE				,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp06,SHOW_LMP(LMP_ENG3_ON_FIRE				,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp07,SHOW_LMP(LMP_EXTING1_VALVE_OPEN1			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp08,SHOW_LMP(LMP_EXTING2_VALVE_OPEN1			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp09,SHOW_LMP(LMP_EXTING3_VALVE_OPEN1			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp10,SHOW_LMP(LMP_EXTING1_VALVE_OPEN2			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp11,SHOW_LMP(LMP_EXTING2_VALVE_OPEN2			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp12,SHOW_LMP(LMP_EXTING3_VALVE_OPEN2			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp13,SHOW_LMP(LMP_FIRE_EXTING1_RDY			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp14,SHOW_LMP(LMP_FIRE_EXTING2_RDY			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp15,SHOW_LMP(LMP_FIRE_EXTING3_RDY			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp16,SHOW_LMP(LMP_FIRE_EXTING4_RDY			,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))

static MAKE_ICB(icb_crs_02,SHOW_POSM(POS_CRS1_02,1,0))

//////////////////////////////////////////////////////////////////////////

static BOOL FSAPI mcb_clock_left(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) {
		BTN_SET(BTN_CLOCK_HRONO,0);
		POS_INC(POS_ACHS_BLENKER);
	} else {
		BTN_SET(BTN_CLOCK_HRONO,1);
	}
	return TRUE;
}
static BOOL FSAPI mcb_clock_right(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) {
		BTN_SET(BTN_CLOCK_START,0);
		POS_INC(POS_ACHS_TIMER);
	} else {
		BTN_SET(BTN_CLOCK_START,1);
	}
	return TRUE;
}

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS1_01,0);
		POS_SET(POS_CRS1_02,0);
		POS_SET(POS_CRS1_03,0);
		POS_SET(POS_CRS1_04,0);
		POS_SET(POS_YOKE1_ICON,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p1_02(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS1_02,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P3);
		panel_window_close_ident(IDENT_P1);
		panel_window_open_ident(IDENT_P5);
	}
	return true;
}

BOOL FSAPI mcb_clock_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P19);
	return true;
}

static MAKE_TCB(tcb_01	,POS_TT(POS_CRS1_02						))
static MAKE_TCB(tcb_02	,BTN_TT(BTN_CLOCK_HRONO					))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_CLOCK_START					))
static MAKE_TCB(tcb_04	,LMP_TT(LMP_ENG1_BAY_ON_FIRE			))
static MAKE_TCB(tcb_05	,LMP_TT(LMP_ENG2_BAY_ON_FIRE			))
static MAKE_TCB(tcb_06	,LMP_TT(LMP_ENG3_BAY_ON_FIRE			))
static MAKE_TCB(tcb_07	,LMP_TT(LMP_ENG1_ON_FIRE				))
static MAKE_TCB(tcb_08	,LMP_TT(LMP_ENG2_ON_FIRE				))
static MAKE_TCB(tcb_09	,LMP_TT(LMP_ENG3_ON_FIRE				))
static MAKE_TCB(tcb_10	,LMP_TT(LMP_EXTING1_VALVE_OPEN1			))
static MAKE_TCB(tcb_11	,LMP_TT(LMP_EXTING2_VALVE_OPEN1			))
static MAKE_TCB(tcb_12	,LMP_TT(LMP_EXTING3_VALVE_OPEN1			))
static MAKE_TCB(tcb_13	,LMP_TT(LMP_EXTING1_VALVE_OPEN2			))
static MAKE_TCB(tcb_14	,LMP_TT(LMP_EXTING2_VALVE_OPEN2			))
static MAKE_TCB(tcb_15	,LMP_TT(LMP_EXTING3_VALVE_OPEN2			))
static MAKE_TCB(tcb_16	,LMP_TT(LMP_FIRE_EXTING1_RDY			))
static MAKE_TCB(tcb_17	,LMP_TT(LMP_FIRE_EXTING2_RDY			))
static MAKE_TCB(tcb_18	,LMP_TT(LMP_FIRE_EXTING3_RDY			))
static MAKE_TCB(tcb_19	,LMP_TT(LMP_FIRE_EXTING4_RDY			))
static MAKE_TCB(tcb_20  ,NDL_TT(NDL_MAIN_MIN					))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MAKE_TTA(tcb_20	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p1_1,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P1_BACKGROUND1_SX,P1_BACKGROUND1_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_TBOX(  "1", 462-OFFX,   82-OFFY,MISC_CRS_P1_02_SX,MISC_CRS_P1_02_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p1_02)
MOUSE_TBOX("20",130-OFFX,  53-OFFY, 130, 130,CURSOR_HAND,MOUSE_LR,mcb_clock_big)    // NDL_MAIN_MIN					

// �������
//MOUSE_TTPB("20", 130-OFFX,  53-OFFY, 130, 130)    // NDL_MAIN_MIN					

// ������
MOUSE_TBOX( "2",  121,  142, 23, 23,CURSOR_HAND,MOUSE_DLR,mcb_clock_left)
MOUSE_TBOX( "3",  250,  143, 25, 23,CURSOR_HAND,MOUSE_DLR,mcb_clock_right)

// �����
MOUSE_TTPB(  "4",   49,   0, 50,37)    // LMP_ENG1_BAY_ON_FIRE   
MOUSE_TTPB(  "5",   94,   0, 50,37)    // LMP_ENG2_BAY_ON_FIRE   
MOUSE_TTPB(  "6",  133,   0, 50,37)    // LMP_ENG3_BAY_ON_FIRE   
MOUSE_TTPB(  "7",  203,   0, 50,37)    // LMP_ENG1_ON_FIRE       
MOUSE_TTPB(  "8",  244,   0, 50,37)    // LMP_ENG2_ON_FIRE       
MOUSE_TTPB(  "9",  281,   0, 50,37)    // LMP_ENG3_ON_FIRE       
MOUSE_TTPB( "10",   69,  37, 50,37)    // LMP_EXTING1_VALVE_OPEN1
MOUSE_TTPB( "11",  111,  37, 50,37)    // LMP_EXTING2_VALVE_OPEN1
MOUSE_TTPB( "12",  149,  37, 50,37)    // LMP_EXTING3_VALVE_OPEN1
MOUSE_TTPB( "13",  211,  37, 50,37)    // LMP_EXTING1_VALVE_OPEN2
MOUSE_TTPB( "14",  256,  37, 50,37)    // LMP_EXTING2_VALVE_OPEN2
MOUSE_TTPB( "15",  294,  37, 50,37)    // LMP_EXTING3_VALVE_OPEN2
MOUSE_TTPB( "16",  394,   0, 50,37)    // LMP_FIRE_EXTING1_RDY
MOUSE_TTPB( "17",  445,   0, 50,37)    // LMP_FIRE_EXTING2_RDY
MOUSE_TTPB( "18",  485,   0, 50,37)    // LMP_FIRE_EXTING3_RDY
MOUSE_TTPB( "19",  403,  46, 50,37)    // LMP_FIRE_EXTING4_RDY
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p1_1_list; 
GAUGE_HEADER_FS700_EX(P1_BACKGROUND1_D_SX, "p1_1", &p1_1_list, rect_p1_1, 0, 0, 0, 0, p1_1); 


#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(achs_right					,P1_BTN_ACHSRIGHT_D_00	,NULL                   		, 250-OFFX, 143-OFFY,		icb_achs_right	,2*PANEL_LIGHT_MAX, p1_1)			
MY_ICON2	(achs_left					,P1_BTN_ACHSLEFT_D_00	,&l_achs_right					, 121-OFFX, 142-OFFY,		icb_achs_left	,2*PANEL_LIGHT_MAX, p1_1)			
MY_ICON2	(cov_p1_achs_D				,P1_COV_ACHS_D			,&l_achs_left					, 191-OFFX, 93-OFFY,icb_Ico  	,PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_crs_02					,MISC_CRS_P1_02			,&l_cov_p1_achs_D				, 462-OFFX, 82-OFFY,icb_crs_02	,1						, p1_1)	
MY_NEEDLE2	(ndl_p1_achs_sec_D			,P1_NDL_ACHSSEC_D		,&l_p1_crs_02					, 198-OFFX,122-OFFY,3,5,icb_secD		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_sec_M 			,P1_NDL_ACHSSEC_M		,&l_ndl_p1_achs_sec_D			, 198-OFFX,122-OFFY,3,5,icb_secM		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_sec_P			,P1_NDL_ACHSSEC_P		,&l_ndl_p1_achs_sec_M 			, 198-OFFX,122-OFFY,3,5,icb_secP		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_sec_R			,P1_NDL_ACHSSEC_R		,&l_ndl_p1_achs_sec_P			, 198-OFFX,122-OFFY,3,5,icb_secR		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_D			,P1_NDL_ACHSMIN_D		,&l_ndl_p1_achs_sec_R			, 197-OFFX,123-OFFY,3,5,icb_minD		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_M			,P1_NDL_ACHSMIN_M		,&l_ndl_p1_achs_min_D			, 197-OFFX,123-OFFY,3,5,icb_minM		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_P			,P1_NDL_ACHSMIN_P		,&l_ndl_p1_achs_min_M			, 197-OFFX,123-OFFY,3,5,icb_minP		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_R			,P1_NDL_ACHSMIN_R		,&l_ndl_p1_achs_min_P			, 197-OFFX,123-OFFY,3,5,icb_minR		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_D		,P1_NDL_ACHSHOURS_D		,&l_ndl_p1_achs_min_R			, 198-OFFX,123-OFFY,3,6,icb_hrsD	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_M		,P1_NDL_ACHSHOURS_M		,&l_ndl_p1_achs_hours_D			, 198-OFFX,123-OFFY,3,6,icb_hrsM	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_P		,P1_NDL_ACHSHOURS_P		,&l_ndl_p1_achs_hours_M			, 198-OFFX,123-OFFY,3,6,icb_hrsP	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_R		,P1_NDL_ACHSHOURS_R		,&l_ndl_p1_achs_hours_P			, 198-OFFX,123-OFFY,3,6,icb_hrsR	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_D		,P1_NDL_ACHSMINISEC_D	,&l_ndl_p1_achs_hours_R			, 198-OFFX,149-OFFY,8,3,icb_mini_secD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_M		,P1_NDL_ACHSMINISEC_M	,&l_ndl_p1_achs_mini_sec_D		, 198-OFFX,149-OFFY,8,3,icb_mini_secM	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_P		,P1_NDL_ACHSMINISEC_P	,&l_ndl_p1_achs_mini_sec_M		, 198-OFFX,149-OFFY,8,3,icb_mini_secP	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_R		,P1_NDL_ACHSMINISEC_R	,&l_ndl_p1_achs_mini_sec_P		, 198-OFFX,149-OFFY,8,3,icb_mini_secR	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_D		,P1_NDL_ACHSMINIMIN_D	,&l_ndl_p1_achs_mini_sec_R		, 196-OFFX, 97-OFFY,1,3,icb_mini_minD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_M		,P1_NDL_ACHSMINIMIN_M	,&l_ndl_p1_achs_mini_min_D		, 196-OFFX, 97-OFFY,1,3,icb_mini_minM	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_P		,P1_NDL_ACHSMINIMIN_P	,&l_ndl_p1_achs_mini_min_M		, 196-OFFX, 97-OFFY,1,3,icb_mini_minP	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_R		,P1_NDL_ACHSMINIMIN_R	,&l_ndl_p1_achs_mini_min_P		, 196-OFFX, 97-OFFY,1,3,icb_mini_minR	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_D	,P1_NDL_ACHSMINIHOURS_D	,&l_ndl_p1_achs_mini_min_R		, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_M	,P1_NDL_ACHSMINIHOURS_M	,&l_ndl_p1_achs_mini_hours_D	, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsM	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_P	,P1_NDL_ACHSMINIHOURS_P	,&l_ndl_p1_achs_mini_hours_M	, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsP	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_R	,P1_NDL_ACHSMINIHOURS_R	,&l_ndl_p1_achs_mini_hours_P	, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsR	,0,18	, p1_1) 
MY_ICON2	(blenker_p1_achs_D			,P1_BLK_ACHSMODE_D_00	,&l_ndl_p1_achs_mini_hours_R	, 189-OFFX, 97-OFFY,icb_blenker		,3  *PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp01_D				,P1_LMP_01_D_00			,&l_blenker_p1_achs_D			,  49-OFFX,  0-OFFY,icb_lamp01		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp02_D				,P1_LMP_02_D_00			,&l_p1_lamp01_D					,  94-OFFX,  0-OFFY,icb_lamp02		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp03_D				,P1_LMP_03_D_00			,&l_p1_lamp02_D					, 133-OFFX,  0-OFFY,icb_lamp03		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp04_D				,P1_LMP_04_D_00			,&l_p1_lamp03_D					, 203-OFFX,  0-OFFY,icb_lamp04		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp05_D				,P1_LMP_05_D_00			,&l_p1_lamp04_D					, 244-OFFX,  0-OFFY,icb_lamp05		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp06_D				,P1_LMP_06_D_00			,&l_p1_lamp05_D					, 281-OFFX,  0-OFFY,icb_lamp06		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp07_D				,P1_LMP_07_D_00			,&l_p1_lamp06_D					,  69-OFFX, 37-OFFY,icb_lamp07		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp08_D				,P1_LMP_08_D_00			,&l_p1_lamp07_D					, 111-OFFX, 37-OFFY,icb_lamp08		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp09_D				,P1_LMP_09_D_00			,&l_p1_lamp08_D					, 149-OFFX, 37-OFFY,icb_lamp09		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp10_D				,P1_LMP_10_D_00			,&l_p1_lamp09_D					, 211-OFFX, 37-OFFY,icb_lamp10		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp11_D				,P1_LMP_11_D_00			,&l_p1_lamp10_D					, 256-OFFX, 37-OFFY,icb_lamp11		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp12_D				,P1_LMP_12_D_00			,&l_p1_lamp11_D					, 294-OFFX, 37-OFFY,icb_lamp12		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp13_D				,P1_LMP_13_D_00			,&l_p1_lamp12_D					, 394-OFFX,  0-OFFY,icb_lamp13		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp14_D				,P1_LMP_14_D_00			,&l_p1_lamp13_D					, 445-OFFX,  0-OFFY,icb_lamp14		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp15_D				,P1_LMP_15_D_00			,&l_p1_lamp14_D					, 485-OFFX,  0-OFFY,icb_lamp15		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp16_D				,P1_LMP_16_D_00			,&l_p1_lamp15_D					, 403-OFFX, 46-OFFY,icb_lamp16		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_1Ico					,P1_BACKGROUND1_D		,&l_p1_lamp16_D					,		 0,		  0,icb_Ico			,PANEL_LIGHT_MAX	, p1_1) 
MY_STATIC2	(p1_1bg,p1_1_list			,P1_BACKGROUND1			,&l_p1_1Ico						, p1_1);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(achs_right					,P1_BTN_ACHSRIGHT_D_00	,NULL                   	, 250-OFFX, 143-OFFY,		icb_achs_right	,2*PANEL_LIGHT_MAX, p1_1)			
MY_ICON2	(achs_left					,P1_BTN_ACHSLEFT_D_00	,&l_achs_right				, 121-OFFX, 142-OFFY,		icb_achs_left	,2*PANEL_LIGHT_MAX, p1_1)			
MY_ICON2	(cov_p1_achs_D				,P1_COV_ACHS_D			,&l_achs_left				, 191-OFFX, 93-OFFY,icb_Ico  	,PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_crs_02					,MISC_CRS_P1_02			,&l_cov_p1_achs_D			, 462-OFFX, 82-OFFY,icb_crs_02	,1						, p1_1)	
MY_NEEDLE2	(ndl_p1_achs_sec_D			,P1_NDL_ACHSSEC_D		,&l_p1_crs_02				, 198-OFFX,122-OFFY,3,5,icb_secD		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_sec_P			,P1_NDL_ACHSSEC_P		,&l_ndl_p1_achs_sec_D		, 198-OFFX,122-OFFY,3,5,icb_secP		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_D			,P1_NDL_ACHSMIN_D		,&l_ndl_p1_achs_sec_P		, 197-OFFX,123-OFFY,3,5,icb_minD		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_P			,P1_NDL_ACHSMIN_P		,&l_ndl_p1_achs_min_D		, 197-OFFX,123-OFFY,3,5,icb_minP		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_D		,P1_NDL_ACHSHOURS_D		,&l_ndl_p1_achs_min_P		, 198-OFFX,123-OFFY,3,6,icb_hrsD	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_P		,P1_NDL_ACHSHOURS_P		,&l_ndl_p1_achs_hours_D		, 198-OFFX,123-OFFY,3,6,icb_hrsP	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_D		,P1_NDL_ACHSMINISEC_D	,&l_ndl_p1_achs_hours_P		, 198-OFFX,149-OFFY,8,3,icb_mini_secD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_P		,P1_NDL_ACHSMINISEC_P	,&l_ndl_p1_achs_mini_sec_D	, 198-OFFX,149-OFFY,8,3,icb_mini_secP	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_D		,P1_NDL_ACHSMINIMIN_D	,&l_ndl_p1_achs_mini_sec_P	, 196-OFFX, 97-OFFY,1,3,icb_mini_minD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_P		,P1_NDL_ACHSMINIMIN_P	,&l_ndl_p1_achs_mini_min_D	, 196-OFFX, 97-OFFY,1,3,icb_mini_minP	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_D	,P1_NDL_ACHSMINIHOURS_D	,&l_ndl_p1_achs_mini_min_P	, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_P	,P1_NDL_ACHSMINIHOURS_P	,&l_ndl_p1_achs_mini_hours_D, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsP	,0,18	, p1_1) 
MY_ICON2	(blenker_p1_achs_D			,P1_BLK_ACHSMODE_D_00	,&l_ndl_p1_achs_mini_hours_P, 189-OFFX, 97-OFFY,icb_blenker		,3  *PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp01_D				,P1_LMP_01_D_00			,&l_blenker_p1_achs_D		,  49-OFFX,  0-OFFY,icb_lamp01		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp02_D				,P1_LMP_02_D_00			,&l_p1_lamp01_D				,  94-OFFX,  0-OFFY,icb_lamp02		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp03_D				,P1_LMP_03_D_00			,&l_p1_lamp02_D				, 133-OFFX,  0-OFFY,icb_lamp03		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp04_D				,P1_LMP_04_D_00			,&l_p1_lamp03_D				, 203-OFFX,  0-OFFY,icb_lamp04		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp05_D				,P1_LMP_05_D_00			,&l_p1_lamp04_D				, 244-OFFX,  0-OFFY,icb_lamp05		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp06_D				,P1_LMP_06_D_00			,&l_p1_lamp05_D				, 281-OFFX,  0-OFFY,icb_lamp06		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp07_D				,P1_LMP_07_D_00			,&l_p1_lamp06_D				,  69-OFFX, 37-OFFY,icb_lamp07		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp08_D				,P1_LMP_08_D_00			,&l_p1_lamp07_D				, 111-OFFX, 37-OFFY,icb_lamp08		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp09_D				,P1_LMP_09_D_00			,&l_p1_lamp08_D				, 149-OFFX, 37-OFFY,icb_lamp09		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp10_D				,P1_LMP_10_D_00			,&l_p1_lamp09_D				, 211-OFFX, 37-OFFY,icb_lamp10		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp11_D				,P1_LMP_11_D_00			,&l_p1_lamp10_D				, 256-OFFX, 37-OFFY,icb_lamp11		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp12_D				,P1_LMP_12_D_00			,&l_p1_lamp11_D				, 294-OFFX, 37-OFFY,icb_lamp12		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp13_D				,P1_LMP_13_D_00			,&l_p1_lamp12_D				, 394-OFFX,  0-OFFY,icb_lamp13		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp14_D				,P1_LMP_14_D_00			,&l_p1_lamp13_D				, 445-OFFX,  0-OFFY,icb_lamp14		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp15_D				,P1_LMP_15_D_00			,&l_p1_lamp14_D				, 485-OFFX,  0-OFFY,icb_lamp15		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp16_D				,P1_LMP_16_D_00			,&l_p1_lamp15_D				, 403-OFFX, 46-OFFY,icb_lamp16		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_1Ico					,P1_BACKGROUND1_D		,&l_p1_lamp16_D				,		 0,		  0,icb_Ico			,PANEL_LIGHT_MAX	, p1_1) 
MY_STATIC2	(p1_1bg,p1_1_list			,P1_BACKGROUND1			,&l_p1_1Ico					, p1_1);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(achs_right					,P1_BTN_ACHSRIGHT_D_00	,NULL                   	, 250-OFFX, 143-OFFY,		icb_achs_right	,2*PANEL_LIGHT_MAX, p1_1)			
MY_ICON2	(achs_left					,P1_BTN_ACHSLEFT_D_00	,&l_achs_right				, 121-OFFX, 142-OFFY,		icb_achs_left	,2*PANEL_LIGHT_MAX, p1_1)			
MY_ICON2	(cov_p1_achs_D				,P1_COV_ACHS_D			,&l_achs_left				, 191-OFFX, 93-OFFY,icb_Ico  	,PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_crs_02					,MISC_CRS_P1_02			,&l_cov_p1_achs_D			, 462-OFFX, 82-OFFY,icb_crs_02	,1						, p1_1)	
MY_NEEDLE2	(ndl_p1_achs_sec_D			,P1_NDL_ACHSSEC_D		,&l_p1_crs_02				, 198-OFFX,122-OFFY,3,5,icb_secD		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_sec_R			,P1_NDL_ACHSSEC_R		,&l_ndl_p1_achs_sec_D		, 198-OFFX,122-OFFY,3,5,icb_secR		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_D			,P1_NDL_ACHSMIN_D		,&l_ndl_p1_achs_sec_R		, 197-OFFX,123-OFFY,3,5,icb_minD		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_R			,P1_NDL_ACHSMIN_R		,&l_ndl_p1_achs_min_D		, 197-OFFX,123-OFFY,3,5,icb_minR		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_D		,P1_NDL_ACHSHOURS_D		,&l_ndl_p1_achs_min_R		, 198-OFFX,123-OFFY,3,6,icb_hrsD	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_R		,P1_NDL_ACHSHOURS_R		,&l_ndl_p1_achs_hours_D		, 198-OFFX,123-OFFY,3,6,icb_hrsR	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_D		,P1_NDL_ACHSMINISEC_D	,&l_ndl_p1_achs_hours_R		, 198-OFFX,149-OFFY,8,3,icb_mini_secD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_R		,P1_NDL_ACHSMINISEC_R	,&l_ndl_p1_achs_mini_sec_D	, 198-OFFX,149-OFFY,8,3,icb_mini_secR	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_D		,P1_NDL_ACHSMINIMIN_D	,&l_ndl_p1_achs_mini_sec_R	, 196-OFFX, 97-OFFY,1,3,icb_mini_minD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_R		,P1_NDL_ACHSMINIMIN_R	,&l_ndl_p1_achs_mini_min_D	, 196-OFFX, 97-OFFY,1,3,icb_mini_minR	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_D	,P1_NDL_ACHSMINIHOURS_D	,&l_ndl_p1_achs_mini_min_R	, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_R	,P1_NDL_ACHSMINIHOURS_R	,&l_ndl_p1_achs_mini_hours_D, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsR	,0,18	, p1_1) 
MY_ICON2	(blenker_p1_achs_D			,P1_BLK_ACHSMODE_D_00	,&l_ndl_p1_achs_mini_hours_R, 189-OFFX, 97-OFFY,icb_blenker		,3  *PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp01_D				,P1_LMP_01_D_00			,&l_blenker_p1_achs_D		,  49-OFFX,  0-OFFY,icb_lamp01		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp02_D				,P1_LMP_02_D_00			,&l_p1_lamp01_D				,  94-OFFX,  0-OFFY,icb_lamp02		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp03_D				,P1_LMP_03_D_00			,&l_p1_lamp02_D				, 133-OFFX,  0-OFFY,icb_lamp03		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp04_D				,P1_LMP_04_D_00			,&l_p1_lamp03_D				, 203-OFFX,  0-OFFY,icb_lamp04		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp05_D				,P1_LMP_05_D_00			,&l_p1_lamp04_D				, 244-OFFX,  0-OFFY,icb_lamp05		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp06_D				,P1_LMP_06_D_00			,&l_p1_lamp05_D				, 281-OFFX,  0-OFFY,icb_lamp06		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp07_D				,P1_LMP_07_D_00			,&l_p1_lamp06_D				,  69-OFFX, 37-OFFY,icb_lamp07		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp08_D				,P1_LMP_08_D_00			,&l_p1_lamp07_D				, 111-OFFX, 37-OFFY,icb_lamp08		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp09_D				,P1_LMP_09_D_00			,&l_p1_lamp08_D				, 149-OFFX, 37-OFFY,icb_lamp09		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp10_D				,P1_LMP_10_D_00			,&l_p1_lamp09_D				, 211-OFFX, 37-OFFY,icb_lamp10		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp11_D				,P1_LMP_11_D_00			,&l_p1_lamp10_D				, 256-OFFX, 37-OFFY,icb_lamp11		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp12_D				,P1_LMP_12_D_00			,&l_p1_lamp11_D				, 294-OFFX, 37-OFFY,icb_lamp12		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp13_D				,P1_LMP_13_D_00			,&l_p1_lamp12_D				, 394-OFFX,  0-OFFY,icb_lamp13		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp14_D				,P1_LMP_14_D_00			,&l_p1_lamp13_D				, 445-OFFX,  0-OFFY,icb_lamp14		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp15_D				,P1_LMP_15_D_00			,&l_p1_lamp14_D				, 485-OFFX,  0-OFFY,icb_lamp15		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp16_D				,P1_LMP_16_D_00			,&l_p1_lamp15_D				, 403-OFFX, 46-OFFY,icb_lamp16		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_1Ico					,P1_BACKGROUND1_D		,&l_p1_lamp16_D				,		 0,		  0,icb_Ico			,PANEL_LIGHT_MAX	, p1_1) 
MY_STATIC2	(p1_1bg,p1_1_list			,P1_BACKGROUND1			,&l_p1_1Ico					, p1_1);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(achs_right					,P1_BTN_ACHSRIGHT_D_00	,NULL                   	, 250-OFFX, 143-OFFY,		icb_achs_right	,2*PANEL_LIGHT_MAX, p1_1)			
MY_ICON2	(achs_left					,P1_BTN_ACHSLEFT_D_00	,&l_achs_right				, 121-OFFX, 142-OFFY,		icb_achs_left	,2*PANEL_LIGHT_MAX, p1_1)			
MY_ICON2	(cov_p1_achs_D				,P1_COV_ACHS_D			,&l_achs_left				, 191-OFFX, 93-OFFY,icb_Ico  	,PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_crs_02					,MISC_CRS_P1_02			,&l_cov_p1_achs_D			, 462-OFFX, 82-OFFY,icb_crs_02	,1						, p1_1)	
MY_NEEDLE2	(ndl_p1_achs_sec_D			,P1_NDL_ACHSSEC_D		,&l_p1_crs_02				, 198-OFFX,122-OFFY,3,5,icb_secD		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_sec_M 			,P1_NDL_ACHSSEC_M		,&l_ndl_p1_achs_sec_D		, 198-OFFX,122-OFFY,3,5,icb_secM		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_D			,P1_NDL_ACHSMIN_D		,&l_ndl_p1_achs_sec_M 		, 197-OFFX,123-OFFY,3,5,icb_minD		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_min_M			,P1_NDL_ACHSMIN_M		,&l_ndl_p1_achs_min_D		, 197-OFFX,123-OFFY,3,5,icb_minM		,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_D		,P1_NDL_ACHSHOURS_D		,&l_ndl_p1_achs_min_M		, 198-OFFX,123-OFFY,3,6,icb_hrsD	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_hours_M		,P1_NDL_ACHSHOURS_M		,&l_ndl_p1_achs_hours_D		, 198-OFFX,123-OFFY,3,6,icb_hrsM	    ,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_D		,P1_NDL_ACHSMINISEC_D	,&l_ndl_p1_achs_hours_M		, 198-OFFX,149-OFFY,8,3,icb_mini_secD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_sec_M		,P1_NDL_ACHSMINISEC_M	,&l_ndl_p1_achs_mini_sec_D	, 198-OFFX,149-OFFY,8,3,icb_mini_secM	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_D		,P1_NDL_ACHSMINIMIN_D	,&l_ndl_p1_achs_mini_sec_M	, 196-OFFX, 97-OFFY,1,3,icb_mini_minD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_min_M		,P1_NDL_ACHSMINIMIN_M	,&l_ndl_p1_achs_mini_min_D	, 196-OFFX, 97-OFFY,1,3,icb_mini_minM	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_D	,P1_NDL_ACHSMINIHOURS_D	,&l_ndl_p1_achs_mini_min_M	, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsD	,0,18	, p1_1) 
MY_NEEDLE2	(ndl_p1_achs_mini_hours_M	,P1_NDL_ACHSMINIHOURS_M	,&l_ndl_p1_achs_mini_hours_D, 197-OFFX, 97-OFFY,2,4,icb_mini_hrsM	,0,18	, p1_1) 
MY_ICON2	(blenker_p1_achs_D			,P1_BLK_ACHSMODE_D_00	,&l_ndl_p1_achs_mini_hours_M, 189-OFFX, 97-OFFY,icb_blenker		,3  *PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp01_D				,P1_LMP_01_D_00			,&l_blenker_p1_achs_D		,  49-OFFX,  0-OFFY,icb_lamp01		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp02_D				,P1_LMP_02_D_00			,&l_p1_lamp01_D				,  94-OFFX,  0-OFFY,icb_lamp02		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp03_D				,P1_LMP_03_D_00			,&l_p1_lamp02_D				, 133-OFFX,  0-OFFY,icb_lamp03		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp04_D				,P1_LMP_04_D_00			,&l_p1_lamp03_D				, 203-OFFX,  0-OFFY,icb_lamp04		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp05_D				,P1_LMP_05_D_00			,&l_p1_lamp04_D				, 244-OFFX,  0-OFFY,icb_lamp05		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp06_D				,P1_LMP_06_D_00			,&l_p1_lamp05_D				, 281-OFFX,  0-OFFY,icb_lamp06		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp07_D				,P1_LMP_07_D_00			,&l_p1_lamp06_D				,  69-OFFX, 37-OFFY,icb_lamp07		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp08_D				,P1_LMP_08_D_00			,&l_p1_lamp07_D				, 111-OFFX, 37-OFFY,icb_lamp08		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp09_D				,P1_LMP_09_D_00			,&l_p1_lamp08_D				, 149-OFFX, 37-OFFY,icb_lamp09		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp10_D				,P1_LMP_10_D_00			,&l_p1_lamp09_D				, 211-OFFX, 37-OFFY,icb_lamp10		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp11_D				,P1_LMP_11_D_00			,&l_p1_lamp10_D				, 256-OFFX, 37-OFFY,icb_lamp11		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp12_D				,P1_LMP_12_D_00			,&l_p1_lamp11_D				, 294-OFFX, 37-OFFY,icb_lamp12		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp13_D				,P1_LMP_13_D_00			,&l_p1_lamp12_D				, 394-OFFX,  0-OFFY,icb_lamp13		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp14_D				,P1_LMP_14_D_00			,&l_p1_lamp13_D				, 445-OFFX,  0-OFFY,icb_lamp14		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp15_D				,P1_LMP_15_D_00			,&l_p1_lamp14_D				, 485-OFFX,  0-OFFY,icb_lamp15		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_lamp16_D				,P1_LMP_16_D_00			,&l_p1_lamp15_D				, 403-OFFX, 46-OFFY,icb_lamp16		,3	*PANEL_LIGHT_MAX		, p1_1) 
MY_ICON2	(p1_1Ico					,P1_BACKGROUND1_D		,&l_p1_lamp16_D				,		 0,		  0,icb_Ico			,PANEL_LIGHT_MAX	, p1_1) 
MY_STATIC2	(p1_1bg,p1_1_list			,P1_BACKGROUND1			,&l_p1_1Ico					, p1_1);
#endif

#endif