/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "P6.h"

#ifdef _DEBUG
DLLMAIND("P6.log")
#else
DLLMAIN()
#endif

double FSAPI icb_Ico(PELEMENT_ICON pelement) 
{
	CHK_BRT();
	return POS_GET(POS_PANEL_STATE); 
} 

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
#ifndef ONLY3D
		(&p6_1),
		(&p6_2),
		(&p6_3),
		(&p6_4),
		(&p6_5),
		(&p6_6),
		(&p6_7),
		(&p6_8),
		(&p6_9),
#endif
		(&p6_10),
		(&p6_11),
		(&p6_12),
		0
	}
};

