/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P6.h"

#define OFFX	0
#define OFFY	0

static int curs=0;

static MAKE_ICB(icb_radalt			,SHOW_AZS(AZS_RADALT		,2));
static MAKE_ICB(icb_crs_p6_02		,CHK_BRT(); return curs-1;)

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_azs_radalt,AZS_TGL(AZS_RADALT))

static BOOL FSAPI mcb_crs_p8_01(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		curs=1;
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_toggle(IDENT_P8);
	}
	return true;
}

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS6_01,0);
		curs=0;
	}
	return true;
}

static MAKE_TCB(tcb_01,AZS_TT(AZS_RADALT))
static MAKE_TCB(tcb_02,return "Toggle left AZS panel";)

static MOUSE_TOOLTIP_ARGS(ttargs)
	MAKE_TTA(tcb_01)
	MAKE_TTA(tcb_02)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p6_1,HELP_NONE,0,0)
	MOUSE_PBOX(0,0,P6_BACKGROUND1_D_SX,P6_BACKGROUND1_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
	MOUSE_TBOX("1",136-OFFX,390-OFFY,99,124,CURSOR_HAND,MOUSE_LR,mcb_azs_radalt)
	MOUSE_TBOX("2",0,409, 54,106,CURSOR_HAND,MOUSE_MLR,mcb_crs_p8_01)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p6_1_list; 
GAUGE_HEADER_FS700_EX(P6_BACKGROUND1_D_SX, "p6_1", &p6_1_list, rect_p6_1, 0, 0, 0, 0, p6_1); 

MY_ICON2	(p6_crs_02			,MISC_CRS_P1_04			,NULL				, 0, 409,icb_crs_p6_02 ,1,p6_1)
MY_ICON2	(p6_azs_radaltim    ,P6_AZS_RADARALTIM_D_00	,&l_p6_crs_02		, 136-OFFX,  390-OFFY,icb_radalt	, 2*PANEL_LIGHT_MAX,p6_1)  
MY_ICON2	(p6_1Ico			,P6_BACKGROUND1_D		,&l_p6_azs_radaltim	,        0,         0,icb_Ico       ,   PANEL_LIGHT_MAX,p6_1)
MY_STATIC2	(p6_1bg,p6_1_list	,P6_BACKGROUND1_D		,&l_p6_1Ico	,p6_1)

#endif
