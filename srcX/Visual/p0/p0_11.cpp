/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

static double FSAPI icbndlplaner_p0_11(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_PLANE); };
static double FSAPI icbndlplanep_p0_11(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_PLANE); };
static double FSAPI icbndlplanem_p0_11(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_PLANE); };
static double FSAPI icbndlplaned_p0_11(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_PLANE); };
static double FSAPI icbndlballr_p0_11(PELEMENT_NEEDLE pelement)  { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_BALL)*0.0826+90; };
static double FSAPI icbndlballp_p0_11(PELEMENT_NEEDLE pelement)  { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_BALL)*0.0826+90; };
static double FSAPI icbndlballm_p0_11(PELEMENT_NEEDLE pelement)  { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_BALL)*0.0826+90; };
static double FSAPI icbndlballd_p0_11(PELEMENT_NEEDLE pelement)  { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_BALL)*0.0826+90; };
static double FSAPI icbndlflagr_p0_11(PELEMENT_NEEDLE pelement)  { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_FLAG); };
static double FSAPI icbndlflagp_p0_11(PELEMENT_NEEDLE pelement)  { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_FLAG); };
static double FSAPI icbndlflagm_p0_11(PELEMENT_NEEDLE pelement)  { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_FLAG); };
static double FSAPI icbndlflagd_p0_11(PELEMENT_NEEDLE pelement)  { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_FLAG); };
static double FSAPI icbmovpitchr_p0_11(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_AGB0_MAIN_PITCH)*4.1;};
static double FSAPI icbmovpitchp_p0_11(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_AGB0_MAIN_PITCH)*4.1;};
static double FSAPI icbmovpitchm_p0_11(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_AGB0_MAIN_PITCH)*4.1;};
static double FSAPI icbmovpitchd_p0_11(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_AGB0_MAIN_PITCH)*4.1;};

static double FSAPI icb_p0_11(PELEMENT_ICON pelement) 
{ 
	CHK_BRT();
	return POS_GET(POS_PANEL_STATE); 
} 

static double FSAPI icbkrmr_p0_11(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return KRM_GET(KRM_AGB_MAIN0)*18; };
static double FSAPI icbkrmp_p0_11(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return KRM_GET(KRM_AGB_MAIN0)*18; };
static double FSAPI icbkrmm_p0_11(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return KRM_GET(KRM_AGB_MAIN0)*18; };
static double FSAPI icbkrmd_p0_11(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return KRM_GET(KRM_AGB_MAIN0)*18; };

BOOL FSAPI mcb_agb_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_MSCB(mcb_agb_main_left		,KRM_DEC(KRM_AGB_MAIN0				    ))
static MAKE_MSCB(mcb_agb_main_right		,KRM_INC(KRM_AGB_MAIN0				    ))
static MAKE_MSCB(mcb_agb_main_arret		,PRS_BTN(BTN_AGB_MAIN_ARRET0			))

static MAKE_TCB(p0_11_tcb_01,return NDL_TTGET(NDL_AGB0_MAIN_PLANE);) 
static MAKE_TCB(p0_11_tcb_02,BTN_TT(BTN_AGB_MAIN_ARRET0				))
static MAKE_TCB(p0_11_tcb_03,KRM_TT(KRM_AGB_MAIN0					))

static MOUSE_TOOLTIP_ARGS(p0_11_ttargs) 
	MAKE_TTA(p0_11_tcb_01) 
	MAKE_TTA(p0_11_tcb_02) 
	MAKE_TTA(p0_11_tcb_03) 
MOUSE_TOOLTIP_ARGS_END 

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_11,HELP_NONE,0,0) 
	MOUSE_TBOX2("1", 30, 30,P17_BACKGROUND_D_SX,P17_BACKGROUND_D_SY,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_agb_big,p0_11)    
	MOUSE_TBOX2("2", 311, 45, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_agb_main_arret,p0_11)
	MOUSE_TSHB2("3", 0,300,  80, 60,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_agb_main_left,mcb_agb_main_right,p0_11) 
MOUSE_END 

extern PELEMENT_HEADER p0_11_list; 
GAUGE_HEADER_FS700_EX(P17_BACKGROUND_D_SX, "p0_11", &p0_11_list, rect_p0_11, 0, 0, 0, 0, p0_11); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(Np0_11Spot4    	,P17_COV_SPOT_D			,NULL             		, 62,352,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_ICON2	(Np0_11Spot3    	,P17_COV_SPOT_D			,&l_Np0_11Spot4    		,353,288,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_ICON2	(Np0_11Spot2    	,P17_COV_SPOT_D			,&l_Np0_11Spot3    		,353,114,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_ICON2	(Np0_11Spot1    	,P17_COV_SPOT_D			,&l_Np0_11Spot2    		, 62, 36,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_NEEDLE2	(Np0_11KrmR    		,P17_KNB04_R			,&l_Np0_11Spot1    		, 40,333, 26, 26,icbkrmr_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11KrmP    		,P17_KNB04_P			,&l_Np0_11KrmR    		, 40,333, 26, 26,icbkrmp_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11KrmM    		,P17_KNB04_M			,&l_Np0_11KrmP    		, 40,333, 26, 26,icbkrmm_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11KrmD    		,P17_KNB04_D			,&l_Np0_11KrmM    		, 40,333, 26, 26,icbkrmd_p0_11 ,0,0,p0_11); 
MY_ICON2	(Np0_11Arret    	,P17_KNB03_D			,&l_Np0_11KrmD    		,311, 45,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_NEEDLE2	(Np0_11dlPlaneR		,P17_NDL_AGB1PLANE_R	,&l_Np0_11Arret    		,201,211,115,  8,icbndlplaner_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlPlaneP		,P17_NDL_AGB1PLANE_P	,&l_Np0_11dlPlaneR		,201,211,115,  8,icbndlplanep_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlPlaneM		,P17_NDL_AGB1PLANE_M	,&l_Np0_11dlPlaneP		,201,211,115,  8,icbndlplanem_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlPlaneD		,P17_NDL_AGB1PLANE_D	,&l_Np0_11dlPlaneM		,201,211,115,  8,icbndlplaned_p0_11 ,0,0,p0_11); 
MY_ICON2	(p0_11IcoCov    	,P17_COV_AGB1_D			,&l_Np0_11dlPlaneD		,188, 94,icb_p0_11,PANEL_LIGHT_MAX, p0_11); 
MY_NEEDLE2	(Np0_11dlBallR		,P17_NDL_AGB1BALL_R		,&l_p0_11IcoCov    		,200, 94,  0, 12,icbndlballr_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlBallP		,P17_NDL_AGB1BALL_P		,&l_Np0_11dlBallR		,200, 94,  0, 12,icbndlballp_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlBallM		,P17_NDL_AGB1BALL_M		,&l_Np0_11dlBallP		,200, 94,  0, 12,icbndlballm_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlBallD		,P17_NDL_AGB1BALL_D		,&l_Np0_11dlBallM		,200, 94,  0, 12,icbndlballd_p0_11 ,0,0,p0_11); 
MY_STATIC2A	(p0_11Alpha			,P17_ALPHA_AGB1			,&l_Np0_11dlBallD		, 73, 80,p0_11); 
MY_ICON2	(p0_11IcoCovFlag	,P17_COV_AGB1FLAG_D		,&l_p0_11Alpha			, 86, 54,icb_p0_11,PANEL_LIGHT_MAX, p0_11); 
MY_NEEDLE2	(Np0_11dlFlagR		,P17_NDL_AGB1FLAG_R		,&l_p0_11IcoCovFlag		,104,133,  0, 16,icbndlflagr_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlFlagP		,P17_NDL_AGB1FLAG_P		,&l_Np0_11dlFlagR		,104,133,  0, 16,icbndlflagp_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlFlagM		,P17_NDL_AGB1FLAG_M		,&l_Np0_11dlFlagP		,104,133,  0, 16,icbndlflagm_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlFlagD		,P17_NDL_AGB1FLAG_D		,&l_Np0_11dlFlagM		,104,133,  0, 16,icbndlflagd_p0_11 ,0,0,p0_11); 
MY_MOVING2	(p0_11MovPitchR		,P17_MOV_AGB1PITCH_R	,&l_Np0_11dlFlagD		, 86, 75,NULL,0,0,icbmovpitchr_p0_11,-282,282,p0_11); 
MY_MOVING2	(p0_11MovPitchP		,P17_MOV_AGB1PITCH_P	,&l_p0_11MovPitchR		, 86, 75,NULL,0,0,icbmovpitchp_p0_11,-282,282,p0_11); 
MY_MOVING2	(p0_11MovPitchM		,P17_MOV_AGB1PITCH_M	,&l_p0_11MovPitchP		, 86, 75,NULL,0,0,icbmovpitchm_p0_11,-282,282,p0_11); 
MY_MOVING2	(p0_11MovPitchD		,P17_MOV_AGB1PITCH_D	,&l_p0_11MovPitchM		, 86, 75,NULL,0,0,icbmovpitchd_p0_11,-282,282,p0_11); 
MY_ICON2	(p0_11Ico			,P17_BACKGROUND_D		,&l_p0_11MovPitchD		,  0,  0,icb_p0_11,PANEL_LIGHT_MAX, p0_11); 
MY_STATIC2  (p0_11bg,p0_11_list ,P17_BACKGROUND_D		,&l_p0_11Ico			, p0_11);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(Np0_11Spot4    	,P17_COV_SPOT_D			,NULL             		, 62,352,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_ICON2	(Np0_11Spot3    	,P17_COV_SPOT_D			,&l_Np0_11Spot4    		,353,288,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_ICON2	(Np0_11Spot2    	,P17_COV_SPOT_D			,&l_Np0_11Spot3    		,353,114,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_ICON2	(Np0_11Spot1    	,P17_COV_SPOT_D			,&l_Np0_11Spot2    		, 62, 36,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_NEEDLE2	(Np0_11KrmP    		,P17_KNB04_P			,&l_Np0_11Spot1    		, 40,333, 26, 26,icbkrmp_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11KrmD    		,P17_KNB04_D			,&l_Np0_11KrmP    		, 40,333, 26, 26,icbkrmd_p0_11 ,0,0,p0_11); 
MY_ICON2	(Np0_11Arret    	,P17_KNB03_D			,&l_Np0_11KrmD    		,311, 45,icb_Ico,PANEL_LIGHT_MAX, p0_11); 
MY_NEEDLE2	(Np0_11dlPlaneP		,P17_NDL_AGB1PLANE_P	,&l_Np0_11Arret    		,201,211,115,  8,icbndlplanep_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlPlaneD		,P17_NDL_AGB1PLANE_D	,&l_Np0_11dlPlaneP		,201,211,115,  8,icbndlplaned_p0_11 ,0,0,p0_11); 
MY_ICON2	(p0_11IcoCov    	,P17_COV_AGB1_D			,&l_Np0_11dlPlaneD		,188, 94,icb_p0_11,PANEL_LIGHT_MAX, p0_11); 
MY_NEEDLE2	(Np0_11dlBallP		,P17_NDL_AGB1BALL_P		,&l_p0_11IcoCov    		,200, 94,  0, 12,icbndlballp_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlBallD		,P17_NDL_AGB1BALL_D		,&l_Np0_11dlBallP		,200, 94,  0, 12,icbndlballd_p0_11 ,0,0,p0_11); 
MY_STATIC2A	(p0_11Alpha			,P17_ALPHA_AGB1			,&l_Np0_11dlBallD		, 73, 80,p0_11); 
MY_ICON2	(p0_11IcoCovFlag	,P17_COV_AGB1FLAG_D		,&l_p0_11Alpha			, 86, 54,icb_p0_11,PANEL_LIGHT_MAX, p0_11); 
MY_NEEDLE2	(Np0_11dlFlagP		,P17_NDL_AGB1FLAG_P		,&l_p0_11IcoCovFlag		,104,133,  0, 16,icbndlflagp_p0_11 ,0,0,p0_11); 
MY_NEEDLE2	(Np0_11dlFlagD		,P17_NDL_AGB1FLAG_D		,&l_Np0_11dlFlagP		,104,133,  0, 16,icbndlflagd_p0_11 ,0,0,p0_11); 
MY_MOVING2	(p0_11MovPitchP		,P17_MOV_AGB1PITCH_P	,&l_Np0_11dlFlagD		, 86, 75,NULL,0,0,icbmovpitchp_p0_11,-282,282,p0_11); 
MY_MOVING2	(p0_11MovPitchD		,P17_MOV_AGB1PITCH_D	,&l_p0_11MovPitchP		, 86, 75,NULL,0,0,icbmovpitchd_p0_11,-282,282,p0_11); 
MY_ICON2	(p0_11Ico			,P17_BACKGROUND_D		,&l_p0_11MovPitchD		,  0,  0,icb_p0_11,PANEL_LIGHT_MAX, p0_11); 
MY_STATIC2  (p0_11bg,p0_11_list ,P17_BACKGROUND_D		,&l_p0_11Ico			, p0_11);
#endif


#endif