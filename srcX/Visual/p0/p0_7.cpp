/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

//////////////////////////////////////////////////////////////////////////

#ifdef HAVE_DAY_PANEL
static double FSAPI icbNdlIkuYD	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_IKU_YELLOW)-90; };
static double FSAPI icbNdlIkuWD	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_IKU_WHITE)-90; };
static double FSAPI icbNdlIkuSD	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return fabs(360-NDL_GET(NDL_GMK_PRICOURSE)); };
#endif
#ifdef HAVE_MIX_PANEL
static double FSAPI icbNdlIkuYM	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_IKU_YELLOW)-90; };
static double FSAPI icbNdlIkuWM	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_IKU_WHITE)-90; };
static double FSAPI icbNdlIkuSM	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return fabs(360-NDL_GET(NDL_GMK_PRICOURSE)); };
#endif
#ifdef HAVE_PLF_PANEL
static double FSAPI icbNdlIkuYP	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_IKU_YELLOW)-90; };
static double FSAPI icbNdlIkuWP	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_IKU_WHITE)-90; };
static double FSAPI icbNdlIkuSP	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return fabs(360-NDL_GET(NDL_GMK_PRICOURSE)); };
#endif
#ifdef HAVE_RED_PANEL
static double FSAPI icbNdlIkuYR	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_IKU_YELLOW)-90; };
static double FSAPI icbNdlIkuWR	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_IKU_WHITE)-90; };
static double FSAPI icbNdlIkuSR	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return fabs(360-NDL_GET(NDL_GMK_PRICOURSE)); };
#endif

static int tbl[]={3,0,1,2};

static double FSAPI icb_knblp0_7(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	return tbl[int(GLT_GET(GLT_IKU_LEFT))]+(POS_GET(POS_PANEL_STATE)*4);
}

static double FSAPI icb_knbrp0_7(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	return tbl[int(GLT_GET(GLT_IKU_RIGHT))]+(POS_GET(POS_PANEL_STATE)*4);
}

static double FSAPI icbL_p0_7(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	int glt=(int)GLT_GET(GLT_IKU_LEFT);
	int lng=(int)POS_GET(POS_PANEL_LANG);
	int sta=(int)POS_GET(POS_PANEL_STATE)*6;
	switch(glt) {
		case 0:
			return lng?0+sta:2+sta;
		break;
		case 1:
			return 4+sta;
		break;
		case 2:
			return 5+sta;
		break;
		case 3:
			return lng?1+sta:3+sta;
		break;
	}
	return -1;
} 

static double FSAPI icbR_p0_7(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	int glt=(int)GLT_GET(GLT_IKU_RIGHT);
	int lng=(int)POS_GET(POS_PANEL_LANG);
	int sta=(int)POS_GET(POS_PANEL_STATE)*6;
	switch(glt) {
		case 0:
			return lng?0+sta:2+sta;
		break;
		case 1:
			return 4+sta;
		break;
		case 2:
			return 5+sta;
		break;
		case 3:
			return lng?1+sta:3+sta;
		break;
	}
	return -1;
}

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_iku_l_left      	,GLT_DEC(GLT_IKU_LEFT  					))
static MAKE_MSCB(mcb_iku_l_right     	,GLT_INC(GLT_IKU_LEFT  					))
static MAKE_MSCB(mcb_iku_r_left      	,GLT_DEC(GLT_IKU_RIGHT  				))
static MAKE_MSCB(mcb_iku_r_right     	,GLT_INC(GLT_IKU_RIGHT  				))

BOOL FSAPI mcb_iku_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01,return NDL_TTGET(NDL_IKU_WHITE);) 

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_7,HELP_NONE,0,0)
	MOUSE_TBOX("1",30,30, P11_BACKGROUND_D_SX, P11_BACKGROUND_D_SY,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_iku_big)    
	MOUSE_TSHB("1", 33,242,42,42,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_iku_l_left,mcb_iku_l_right) 
	MOUSE_TSHB("1",188,242,42,42,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_iku_r_left,mcb_iku_r_right) 
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p0_7_list; 
GAUGE_HEADER_FS700_EX(P11_BACKGROUND_D_SX, "p0_7", &p0_7_list, rect_p0_7, 0, 0, 0, 0, p0_7); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_7Spot2			,P11_COV_IKUSPOT2_D			,NULL				, 197,   6, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7Spot1			,P11_COV_IKUSPOT1_D			,&l_p0_7Spot2		,  23,   6, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7knbr			,P11_KNB08_D_00				,&l_p0_7Spot1		, 188, 242, icb_knbrp0_7	, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7knbl			,P11_KNB07_D_00				,&l_p0_7knbr		,  33, 242, icb_knblp0_7	, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoSelR		,P11_SCL_IKUSELECTORR_D_ADF1,&l_p0_7knbl		, 131, 287, icbR_p0_7		, 6 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoSelL		,P11_SCL_IKUSELECTORL_D_ADF1,&l_p0_7IcoSelR		,  68, 287, icbL_p0_7		, 6 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoCov			,P11_COV_IKU_D				,&l_p0_7IcoSelL		, 118, 137, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_NEEDLE2	(p0_7NdlIkuYR		,P11_NDL_IKUYELLOW_R		,&l_p0_7IcoCov		, 132, 152, 89,  8,icbNdlIkuYR ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuYP		,P11_NDL_IKUYELLOW_P		,&l_p0_7NdlIkuYR	, 132, 152, 89,  8,icbNdlIkuYP ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuYM		,P11_NDL_IKUYELLOW_M		,&l_p0_7NdlIkuYP	, 132, 152, 89,  8,icbNdlIkuYM ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuYD		,P11_NDL_IKUYELLOW_D		,&l_p0_7NdlIkuYM	, 132, 152, 89,  8,icbNdlIkuYD ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWR		,P11_NDL_IKUWHITE_R			,&l_p0_7NdlIkuYD	, 132, 152, 89, 21,icbNdlIkuWR ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWP		,P11_NDL_IKUWHITE_P			,&l_p0_7NdlIkuWR	, 132, 152, 89, 21,icbNdlIkuWP ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWM		,P11_NDL_IKUWHITE_M			,&l_p0_7NdlIkuWP	, 132, 152, 89, 21,icbNdlIkuWM ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWD		,P11_NDL_IKUWHITE_D			,&l_p0_7NdlIkuWM	, 132, 152, 89, 21,icbNdlIkuWD ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSR		,P11_SCL_IKU_R				,&l_p0_7NdlIkuWD	, 131, 150, 95, 95,icbNdlIkuSR ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSP		,P11_SCL_IKU_P				,&l_p0_7NdlIkuSR	, 131, 150, 95, 95,icbNdlIkuSP ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSM		,P11_SCL_IKU_M				,&l_p0_7NdlIkuSP	, 131, 150, 95, 95,icbNdlIkuSM ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSD		,P11_SCL_IKU_D				,&l_p0_7NdlIkuSM	, 131, 150, 95, 95,icbNdlIkuSD ,0,2,p0_7); 
MY_ICON2	(p0_7Ico			,P11_BACKGROUND_D			,&l_p0_7NdlIkuSD	,   0,   0, icb_Ico			,PANEL_LIGHT_MAX		, p0_7) 
MY_STATIC2	(p0_7bg,p0_7_list	,P11_BACKGROUND_D			,&l_p0_7Ico			, p0_7);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p0_7Spot2			,P11_COV_IKUSPOT2_D			,NULL				, 197,   6, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7Spot1			,P11_COV_IKUSPOT1_D			,&l_p0_7Spot2			,  23,   6, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7knbr			,P11_KNB08_D_00				,&l_p0_7Spot1			, 188, 242, icb_knbrp0_7	, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7knbl			,P11_KNB07_D_00				,&l_p0_7knbr			,  33, 242, icb_knblp0_7	, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoSelR		,P11_SCL_IKUSELECTORR_D_ADF1,&l_p0_7knbl			, 131, 287, icbR_p0_7		, 6 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoSelL		,P11_SCL_IKUSELECTORL_D_ADF1,&l_p0_7IcoSelR		,  68, 287, icbL_p0_7		, 6 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoCov			,P11_COV_IKU_D				,&l_p0_7IcoSelL		, 118, 137, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_NEEDLE2	(p0_7NdlIkuYP		,P11_NDL_IKUYELLOW_P		,&l_p0_7IcoCov			, 132, 152, 89,  8,icbNdlIkuYP ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuYD		,P11_NDL_IKUYELLOW_D		,&l_p0_7NdlIkuYP		, 132, 152, 89,  8,icbNdlIkuYD ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWP		,P11_NDL_IKUWHITE_P			,&l_p0_7NdlIkuYD		, 132, 152, 89, 21,icbNdlIkuWP ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWD		,P11_NDL_IKUWHITE_D			,&l_p0_7NdlIkuWP		, 132, 152, 89, 21,icbNdlIkuWD ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSP		,P11_SCL_IKU_P				,&l_p0_7NdlIkuWD		, 131, 150, 95, 95,icbNdlIkuSP ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSD		,P11_SCL_IKU_D				,&l_p0_7NdlIkuSP		, 131, 150, 95, 95,icbNdlIkuSD ,0,2,p0_7); 
MY_ICON2	(p0_7Ico			,P11_BACKGROUND_D			,&l_p0_7NdlIkuSD		,   0,   0, icb_Ico			,PANEL_LIGHT_MAX		, p0_7) 
MY_STATIC2	(p0_7bg,p0_7_list	,P11_BACKGROUND_D			,&l_p0_7Ico			, p0_7);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_7Spot2			,P11_COV_IKUSPOT2_D			,NULL				, 197,   6, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7Spot1			,P11_COV_IKUSPOT1_D			,&l_p0_7Spot2			,  23,   6, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7knbr			,P11_KNB08_D_00				,&l_p0_7Spot1			, 188, 242, icb_knbrp0_7	, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7knbl			,P11_KNB07_D_00				,&l_p0_7knbr			,  33, 242, icb_knblp0_7	, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoSelR		,P11_SCL_IKUSELECTORR_D_ADF1,&l_p0_7knbl			, 131, 287, icbR_p0_7		, 6 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoSelL		,P11_SCL_IKUSELECTORL_D_ADF1,&l_p0_7IcoSelR		,  68, 287, icbL_p0_7		, 6 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoCov			,P11_COV_IKU_D				,&l_p0_7IcoSelL		, 118, 137, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_NEEDLE2	(p0_7NdlIkuYR		,P11_NDL_IKUYELLOW_R		,&l_p0_7IcoCov			, 132, 152, 89,  8,icbNdlIkuYR ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuYD		,P11_NDL_IKUYELLOW_D		,&l_p0_7NdlIkuYR		, 132, 152, 89,  8,icbNdlIkuYD ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWR		,P11_NDL_IKUWHITE_R			,&l_p0_7NdlIkuYD		, 132, 152, 89, 21,icbNdlIkuWR ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWD		,P11_NDL_IKUWHITE_D			,&l_p0_7NdlIkuWR		, 132, 152, 89, 21,icbNdlIkuWD ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSR		,P11_SCL_IKU_R				,&l_p0_7NdlIkuWD		, 131, 150, 95, 95,icbNdlIkuSR ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSD		,P11_SCL_IKU_D				,&l_p0_7NdlIkuSR		, 131, 150, 95, 95,icbNdlIkuSD ,0,2,p0_7); 
MY_ICON2	(p0_7Ico			,P11_BACKGROUND_D			,&l_p0_7NdlIkuSD		,   0,   0, icb_Ico			,PANEL_LIGHT_MAX		, p0_7) 
MY_STATIC2	(p0_7bg,p0_7_list	,P11_BACKGROUND_D			,&l_p0_7Ico			, p0_7);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p0_7Spot2			,P11_COV_IKUSPOT2_D			,NULL				, 197,   6, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7Spot1			,P11_COV_IKUSPOT1_D			,&l_p0_7Spot2			,  23,   6, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7knbr			,P11_KNB08_D_00				,&l_p0_7Spot1			, 188, 242, icb_knbrp0_7	, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7knbl			,P11_KNB07_D_00				,&l_p0_7knbr			,  33, 242, icb_knblp0_7	, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoSelR		,P11_SCL_IKUSELECTORR_D_ADF1,&l_p0_7knbl			, 131, 287, icbR_p0_7		, 6 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoSelL		,P11_SCL_IKUSELECTORL_D_ADF1,&l_p0_7IcoSelR		,  68, 287, icbL_p0_7		, 6 *PANEL_LIGHT_MAX	, p0_7) 
MY_ICON2	(p0_7IcoCov			,P11_COV_IKU_D				,&l_p0_7IcoSelL		, 118, 137, icb_Ico			, 4 *PANEL_LIGHT_MAX	, p0_7) 
MY_NEEDLE2	(p0_7NdlIkuYM		,P11_NDL_IKUYELLOW_M		,&l_p0_7IcoCov			, 132, 152, 89,  8,icbNdlIkuYM ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuYD		,P11_NDL_IKUYELLOW_D		,&l_p0_7NdlIkuYM		, 132, 152, 89,  8,icbNdlIkuYD ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWM		,P11_NDL_IKUWHITE_M			,&l_p0_7NdlIkuYD		, 132, 152, 89, 21,icbNdlIkuWM ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuWD		,P11_NDL_IKUWHITE_D			,&l_p0_7NdlIkuWM		, 132, 152, 89, 21,icbNdlIkuWD ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSM		,P11_SCL_IKU_M				,&l_p0_7NdlIkuWD		, 131, 150, 95, 95,icbNdlIkuSM ,0,2,p0_7); 
MY_NEEDLE2	(p0_7NdlIkuSD		,P11_SCL_IKU_D				,&l_p0_7NdlIkuSM		, 131, 150, 95, 95,icbNdlIkuSD ,0,2,p0_7); 
MY_ICON2	(p0_7Ico			,P11_BACKGROUND_D			,&l_p0_7NdlIkuSD		,   0,   0, icb_Ico			,PANEL_LIGHT_MAX		, p0_7) 
MY_STATIC2	(p0_7bg,p0_7_list	,P11_BACKGROUND_D			,&l_p0_7Ico			, p0_7);
#endif

#endif