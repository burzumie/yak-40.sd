/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

#define OFFY P16_BACKGROUND_D_SY


double ChkLangRus(PELEMENT_NEEDLE pelement, int st, int nd1) 
{ 
	CHK_BRT();
	if(POS_GET(POS_PANEL_STATE)==st) { 
		if(!POS_GET(POS_PANEL_LANG)) {
			SHOW_IMAGE(pelement); 
			return NDL_GET(nd1);
		}
	}
	HIDE_IMAGE(pelement);
	return 0;
}; 

double ChkLangEng(PELEMENT_NEEDLE pelement, int st, int nd1) 
{ 
	CHK_BRT();
	if(POS_GET(POS_PANEL_STATE)==st) { 
		if(POS_GET(POS_PANEL_LANG)) {
			SHOW_IMAGE(pelement); 
			return NDL_GET(nd1);
		}
	}
	HIDE_IMAGE(pelement);
	return 0;
}; 

//////////////////////////////////////////////////////////////////////////

#define P10_MOV_ALTIMPRESS100_M	   P10_MOV_ALTIMPRESS100_R   
#define P10_MOV_ALTIMPRESS10_M 	   P10_MOV_ALTIMPRESS10_R    
#define P10_MOV_ALTIMPRESS1_M	   P10_MOV_ALTIMPRESS1_R     

static double FSAPI icbndluvidd (PELEMENT_NEEDLE pelement)		 { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?NDL_GET(NDL_UVID0)*0.36-90:NDL_GET(NDL_UVID0_EN)*0.36-90; }; 
static double FSAPI icbndluvidm (PELEMENT_NEEDLE pelement)		 { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?NDL_GET(NDL_UVID0)*0.36-90:NDL_GET(NDL_UVID0_EN)*0.36-90; }; 
static double FSAPI icbndluvidp (PELEMENT_NEEDLE pelement)		 { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?NDL_GET(NDL_UVID0)*0.36-90:NDL_GET(NDL_UVID0_EN)*0.36-90; }; 
static double FSAPI icbndluvidr (PELEMENT_NEEDLE pelement)		 { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?NDL_GET(NDL_UVID0)*0.36-90:NDL_GET(NDL_UVID0_EN)*0.36-90; };

static double FSAPI icbmovuvid1d(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10000): POS_GET(POS_UVID0_ALT_10000_EN); };
static double FSAPI icbmovuvid2d(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_1000) : POS_GET(POS_UVID0_ALT_1000_EN);  };
static double FSAPI icbmovuvid3d(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_100)  : POS_GET(POS_UVID0_ALT_100_EN);   };
static double FSAPI icbmovuvid4d(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10)   : POS_GET(POS_UVID0_ALT_10_EN);	};

static double FSAPI icbmovuvid1m(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10000): POS_GET(POS_UVID0_ALT_10000_EN); };
static double FSAPI icbmovuvid2m(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_1000) : POS_GET(POS_UVID0_ALT_1000_EN);  };
static double FSAPI icbmovuvid3m(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_100)  : POS_GET(POS_UVID0_ALT_100_EN);   };
static double FSAPI icbmovuvid4m(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10)   : POS_GET(POS_UVID0_ALT_10_EN);	};

static double FSAPI icbmovuvid1p(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10000): POS_GET(POS_UVID0_ALT_10000_EN); };
static double FSAPI icbmovuvid2p(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_1000) : POS_GET(POS_UVID0_ALT_1000_EN);  };
static double FSAPI icbmovuvid3p(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_100)  : POS_GET(POS_UVID0_ALT_100_EN);   };
static double FSAPI icbmovuvid4p(PELEMENT_MOVING_IMAGE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10)   : POS_GET(POS_UVID0_ALT_10_EN);	};

static double FSAPI icbmovpres1d(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_100_MM); };
static double FSAPI icbmovpres2d(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_10_MM);  };
static double FSAPI icbmovpres3d(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1_MM);   };
static double FSAPI icbmovpres1m(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_100_MM); };
static double FSAPI icbmovpres2m(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_10_MM);  };
static double FSAPI icbmovpres3m(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1_MM);   };
static double FSAPI icbmovpres1p(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_100_MM); };
static double FSAPI icbmovpres2p(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_10_MM);  };
static double FSAPI icbmovpres3p(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1_MM);   };
static double FSAPI icbmovuvid1r(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10000): POS_GET(POS_UVID0_ALT_10000_EN); };
static double FSAPI icbmovuvid2r(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_1000) : POS_GET(POS_UVID0_ALT_1000_EN);  };
static double FSAPI icbmovuvid3r(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_100)  : POS_GET(POS_UVID0_ALT_100_EN);   };
static double FSAPI icbmovuvid4r(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10)   : POS_GET(POS_UVID0_ALT_10_EN);	};
static double FSAPI icbmovpres1r(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_100_MM); };
static double FSAPI icbmovpres2r(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_10_MM);  };
static double FSAPI icbmovpres3r(PELEMENT_MOVING_IMAGE pelement) { if(POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1_MM);   };

static double FSAPI icbmovpres1hgd(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1000_HG); };
static double FSAPI icbmovpres2hgd(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_100_HG);  };
static double FSAPI icbmovpres3hgd(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_10_HG);   };
static double FSAPI icbmovpres4hgd(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1_HG);   };
static double FSAPI icbmovpres1hgm(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1000_HG); };
static double FSAPI icbmovpres2hgm(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_100_HG);  };
static double FSAPI icbmovpres3hgm(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_10_HG);   };
static double FSAPI icbmovpres4hgm(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1_HG);   };
static double FSAPI icbmovpres1hgp(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1000_HG); };
static double FSAPI icbmovpres2hgp(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_100_HG);  };
static double FSAPI icbmovpres3hgp(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_10_HG);   };
static double FSAPI icbmovpres4hgp(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1_HG);   };
static double FSAPI icbmovpres1hgr(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1000_HG); };
static double FSAPI icbmovpres2hgr(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_100_HG);  };
static double FSAPI icbmovpres3hgr(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_10_HG);   };
static double FSAPI icbmovpres4hgr(PELEMENT_MOVING_IMAGE pelement) { if(!POS_GET(POS_PANEL_LANG)){HIDE_IMAGE(pelement);return 0;}if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return POS_GET(POS_UVID0_PSI_1_HG);   };

double FSAPI icbscl_p0_9(PELEMENT_ICON pelement) 
{ 
	CHK_BRT();
	if(!POS_GET(POS_PANEL_LANG)) { 
		HIDE_IMAGE(pelement); 
		return -1; 
	} 
	SHOW_IMAGE(pelement); 
	return POS_GET(POS_PANEL_STATE); 
}

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_uvid_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_MSCB(mcb_altim_press_left	,KRM_DEC(KRM_UVID0						))
static MAKE_MSCB(mcb_altim_press_right	,KRM_INC(KRM_UVID0						))

static MAKE_TCB(tcb_01,return NDL_TTGET(NDL_UVID0);) 

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_9,HELP_NONE,0,0)
	MOUSE_TBOX( "1", 30, 30,P10_BACKGROUND_D_SX,P10_BACKGROUND_D_SY,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_uvid_big)    
	MOUSE_TSHB( "1", 30, 30,200,200,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_altim_press_left,mcb_altim_press_right) 
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p0_9_list; 
GAUGE_HEADER_FS700_EX(P10_BACKGROUND_D_SX, "p0_9", &p0_9_list, rect_p0_9, 0, 0, 0, 0, p0_9); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_9Spot3     	   ,P10_COV_ASPDALTSPSPOT3_D,NULL            		, 31,287-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9Spot4     	   ,P10_COV_ASPDALTSPSPOT4_D,&l_p0_9Spot3    		,205,287-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9IcoCovUvid		,P10_COV_UVID_D			,&l_p0_9Spot4     	    ,131,426-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_NEEDLE2	(p0_9NdlUvidR		,P10_NDL_UVID_R			,&l_p0_9IcoCovUvid		,140,436-OFFY,  0,  8,icbndluvidr ,0,0,p0_9) 
MY_NEEDLE2	(p0_9NdlUvidP		,P10_NDL_UVID_P			,&l_p0_9NdlUvidR		,140,436-OFFY,  0,  8,icbndluvidp ,0,0,p0_9) 
MY_NEEDLE2	(p0_9NdlUvidM		,P10_NDL_UVID_M			,&l_p0_9NdlUvidP		,140,436-OFFY,  0,  8,icbndluvidm ,0,0,p0_9) 
MY_NEEDLE2	(p0_9NdlUvidD		,P10_NDL_UVID_D			,&l_p0_9NdlUvidM		,140,436-OFFY,  0,  8,icbndluvidd ,0,0,p0_9) 
MY_MOVING2	(p0_9MovPres3R		,P10_MOV_ALTIMPRESS1_R	,&l_p0_9NdlUvidD		,146,488-OFFY,NULL,0,0,icbmovpres3r,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2R		,P10_MOV_ALTIMPRESS10_R	,&l_p0_9MovPres3R		,120,493-OFFY,NULL,0,0,icbmovpres2r,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1R		,P10_MOV_ALTIMPRESS100_R,&l_p0_9MovPres2R		,100,493-OFFY,NULL,0,0,icbmovpres1r,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGR	,P10_MOV_ALTIMPRESS1_R	,&l_p0_9MovPres1R		,146,488-OFFY,NULL,0,0,icbmovpres4hgr,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGR	,P10_MOV_ALTIMPRESS10_R	,&l_p0_9MovPres4HGR		,129,493-OFFY,NULL,0,0,icbmovpres3hgr,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGR	,P10_MOV_ALTIMPRESS10_R	,&l_p0_9MovPres3HGR		,112,493-OFFY,NULL,0,0,icbmovpres2hgr,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGR	,P10_MOV_ALTIMPRESS100_R,&l_p0_9MovPres2HGR		, 95,493-OFFY,NULL,0,0,icbmovpres1hgr,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4R		,P10_MOV_UVID4_R		,&l_p0_9MovPres1HGR		,177,396-OFFY,NULL,0,0,icbmovuvid4r,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3R		,P10_MOV_UVID3_R		,&l_p0_9MovUvid4R		,143,402-OFFY,NULL,0,0,icbmovuvid3r,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2R		,P10_MOV_UVID2_R		,&l_p0_9MovUvid3R		,109,402-OFFY,NULL,0,0,icbmovuvid2r,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1R		,P10_MOV_UVID1_R		,&l_p0_9MovUvid2R		, 75,402-OFFY,NULL,0,0,icbmovuvid1r,0,100,p0_9) 
MY_MOVING2	(p0_9MovPres3P		,P10_MOV_ALTIMPRESS1_P	,&l_p0_9MovUvid1R		,146,488-OFFY,NULL,0,0,icbmovpres3p,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2P		,P10_MOV_ALTIMPRESS10_P	,&l_p0_9MovPres3P		,120,493-OFFY,NULL,0,0,icbmovpres2p,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1P		,P10_MOV_ALTIMPRESS100_P,&l_p0_9MovPres2P		,100,493-OFFY,NULL,0,0,icbmovpres1p,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGP	,P10_MOV_ALTIMPRESS1_P	,&l_p0_9MovPres1P		,146,488-OFFY,NULL,0,0,icbmovpres4hgp,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGP	,P10_MOV_ALTIMPRESS10_P	,&l_p0_9MovPres4HGP		,129,493-OFFY,NULL,0,0,icbmovpres3hgp,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGP	,P10_MOV_ALTIMPRESS10_P	,&l_p0_9MovPres3HGP		,112,493-OFFY,NULL,0,0,icbmovpres2hgp,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGP	,P10_MOV_ALTIMPRESS100_P,&l_p0_9MovPres2HGP		, 95,493-OFFY,NULL,0,0,icbmovpres1hgp,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4P		,P10_MOV_UVID4_P		,&l_p0_9MovPres1HGP		,177,396-OFFY,NULL,0,0,icbmovuvid4p,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3P		,P10_MOV_UVID3_P		,&l_p0_9MovUvid4P		,143,402-OFFY,NULL,0,0,icbmovuvid3p,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2P		,P10_MOV_UVID2_P		,&l_p0_9MovUvid3P		,109,402-OFFY,NULL,0,0,icbmovuvid2p,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1P		,P10_MOV_UVID1_P		,&l_p0_9MovUvid2P		, 75,402-OFFY,NULL,0,0,icbmovuvid1p,0,100,p0_9) 
MY_MOVING2	(p0_9MovPres3M		,P10_MOV_ALTIMPRESS1_M	,&l_p0_9MovUvid1P		,146,488-OFFY,NULL,0,0,icbmovpres3m,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2M		,P10_MOV_ALTIMPRESS10_M	,&l_p0_9MovPres3M		,120,493-OFFY,NULL,0,0,icbmovpres2m,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1M		,P10_MOV_ALTIMPRESS100_M,&l_p0_9MovPres2M		,100,493-OFFY,NULL,0,0,icbmovpres1m,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGM	,P10_MOV_ALTIMPRESS1_M	,&l_p0_9MovPres1M		,146,488-OFFY,NULL,0,0,icbmovpres4hgm,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGM	,P10_MOV_ALTIMPRESS10_M	,&l_p0_9MovPres4HGM		,129,493-OFFY,NULL,0,0,icbmovpres3hgm,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGM	,P10_MOV_ALTIMPRESS10_M	,&l_p0_9MovPres3HGM		,112,493-OFFY,NULL,0,0,icbmovpres2hgm,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGM	,P10_MOV_ALTIMPRESS100_M,&l_p0_9MovPres2HGM		, 95,493-OFFY,NULL,0,0,icbmovpres1hgm,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4M		,P10_MOV_UVID4_M		,&l_p0_9MovPres1HGM		,177,396-OFFY,NULL,0,0,icbmovuvid4m,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3M		,P10_MOV_UVID3_M		,&l_p0_9MovUvid4M		,143,402-OFFY,NULL,0,0,icbmovuvid3m,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2M		,P10_MOV_UVID2_M		,&l_p0_9MovUvid3M		,109,402-OFFY,NULL,0,0,icbmovuvid2m,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1M		,P10_MOV_UVID1_M		,&l_p0_9MovUvid2M		, 75,402-OFFY,NULL,0,0,icbmovuvid1m,0,100,p0_9) 
MY_MOVING2	(p0_9MovPres3D		,P10_MOV_ALTIMPRESS1_D	,&l_p0_9MovUvid1M		,146,488-OFFY,NULL,0,0,icbmovpres3d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2D		,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres3D		,120,493-OFFY,NULL,0,0,icbmovpres2d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1D		,P10_MOV_ALTIMPRESS100_D,&l_p0_9MovPres2D		,100,493-OFFY,NULL,0,0,icbmovpres1d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGD	,P10_MOV_ALTIMPRESS1_D	,&l_p0_9MovPres1D		,146,488-OFFY,NULL,0,0,icbmovpres4hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGD	,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres4HGD		,129,493-OFFY,NULL,0,0,icbmovpres3hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGD	,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres3HGD		,112,493-OFFY,NULL,0,0,icbmovpres2hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGD	,P10_MOV_ALTIMPRESS100_D,&l_p0_9MovPres2HGD		, 95,493-OFFY,NULL,0,0,icbmovpres1hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4D		,P10_MOV_UVID4_D		,&l_p0_9MovPres1HGD		,177,396-OFFY,NULL,0,0,icbmovuvid4d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3D		,P10_MOV_UVID3_D		,&l_p0_9MovUvid4D		,143,402-OFFY,NULL,0,0,icbmovuvid3d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2D		,P10_MOV_UVID2_D		,&l_p0_9MovUvid3D		,109,402-OFFY,NULL,0,0,icbmovuvid2d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1D		,P10_MOV_UVID1_D		,&l_p0_9MovUvid2D		, 75,402-OFFY,NULL,0,0,icbmovuvid1d,0,100,p0_9) 
MY_ICON2	(p0_9IcoSclUvidEng	,P10_SCL_UVIDINTL_D		,&l_p0_9MovUvid1D		, 21,313-OFFY,icbscl_p0_9,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9Ico			,P10_BACKGROUND_D		,&l_p0_9IcoSclUvidEng	,  0,  0,icb_Ico	,PANEL_LIGHT_MAX,p0_9) 
MY_STATIC2	(p0_9bg,p0_9_list	,P10_BACKGROUND_D		,&l_p0_9Ico				, p0_9);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p0_9Spot3     	   ,P10_COV_ASPDALTSPSPOT3_D,NULL            		, 31,287-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9Spot4     	   ,P10_COV_ASPDALTSPSPOT4_D,&l_p0_9Spot3    		,205,287-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9IcoCovUvid		,P10_COV_UVID_D			,&l_p0_9Spot4     	    ,131,426-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_NEEDLE2	(p0_9NdlUvidP		,P10_NDL_UVID_P			,&l_p0_9IcoCovUvid		,140,436-OFFY,  0,  8,icbndluvidp ,0,0,p0_9) 
MY_NEEDLE2	(p0_9NdlUvidD		,P10_NDL_UVID_D			,&l_p0_9NdlUvidP		,140,436-OFFY,  0,  8,icbndluvidd ,0,0,p0_9) 
MY_MOVING2	(p0_9MovPres3P		,P10_MOV_ALTIMPRESS1_P	,&l_p0_9NdlUvidD		,146,488-OFFY,NULL,0,0,icbmovpres3p,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2P		,P10_MOV_ALTIMPRESS10_P	,&l_p0_9MovPres3P		,120,493-OFFY,NULL,0,0,icbmovpres2p,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1P		,P10_MOV_ALTIMPRESS100_P,&l_p0_9MovPres2P		,100,493-OFFY,NULL,0,0,icbmovpres1p,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGP	,P10_MOV_ALTIMPRESS1_P	,&l_p0_9MovPres1P		,146,488-OFFY,NULL,0,0,icbmovpres4hgp,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGP	,P10_MOV_ALTIMPRESS10_P	,&l_p0_9MovPres4HGP		,129,493-OFFY,NULL,0,0,icbmovpres3hgp,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGP	,P10_MOV_ALTIMPRESS10_P	,&l_p0_9MovPres3HGP		,112,493-OFFY,NULL,0,0,icbmovpres2hgp,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGP	,P10_MOV_ALTIMPRESS100_P,&l_p0_9MovPres2HGP		, 95,493-OFFY,NULL,0,0,icbmovpres1hgp,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4P		,P10_MOV_UVID4_P		,&l_p0_9MovPres1HGP		,177,396-OFFY,NULL,0,0,icbmovuvid4p,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3P		,P10_MOV_UVID3_P		,&l_p0_9MovUvid4P		,143,402-OFFY,NULL,0,0,icbmovuvid3p,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2P		,P10_MOV_UVID2_P		,&l_p0_9MovUvid3P		,109,402-OFFY,NULL,0,0,icbmovuvid2p,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1P		,P10_MOV_UVID1_P		,&l_p0_9MovUvid2P		, 75,402-OFFY,NULL,0,0,icbmovuvid1p,0,100,p0_9) 
MY_MOVING2	(p0_9MovPres3D		,P10_MOV_ALTIMPRESS1_D	,&l_p0_9MovUvid1P		,146,488-OFFY,NULL,0,0,icbmovpres3d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2D		,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres3D		,120,493-OFFY,NULL,0,0,icbmovpres2d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1D		,P10_MOV_ALTIMPRESS100_D,&l_p0_9MovPres2D		,100,493-OFFY,NULL,0,0,icbmovpres1d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGD	,P10_MOV_ALTIMPRESS1_D	,&l_p0_9MovPres1D		,146,488-OFFY,NULL,0,0,icbmovpres4hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGD	,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres4HGD		,129,493-OFFY,NULL,0,0,icbmovpres3hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGD	,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres3HGD		,112,493-OFFY,NULL,0,0,icbmovpres2hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGD	,P10_MOV_ALTIMPRESS100_D,&l_p0_9MovPres2HGD		, 95,493-OFFY,NULL,0,0,icbmovpres1hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4D		,P10_MOV_UVID4_D		,&l_p0_9MovPres1HGD		,177,396-OFFY,NULL,0,0,icbmovuvid4d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3D		,P10_MOV_UVID3_D		,&l_p0_9MovUvid4D		,143,402-OFFY,NULL,0,0,icbmovuvid3d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2D		,P10_MOV_UVID2_D		,&l_p0_9MovUvid3D		,109,402-OFFY,NULL,0,0,icbmovuvid2d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1D		,P10_MOV_UVID1_D		,&l_p0_9MovUvid2D		, 75,402-OFFY,NULL,0,0,icbmovuvid1d,0,100,p0_9) 
MY_ICON2	(p0_9IcoSclUvidEng	,P10_SCL_UVIDINTL_D		,&l_p0_9MovUvid1D		, 21,313-OFFY,icbscl_p0_9,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9Ico			,P10_BACKGROUND_D		,&l_p0_9IcoSclUvidEng	,  0,  0,icb_Ico	,PANEL_LIGHT_MAX,p0_9) 
MY_STATIC2	(p0_9bg,p0_9_list	,P10_BACKGROUND_D		,&l_p0_9Ico				, p0_9);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_9Spot3     	   ,P10_COV_ASPDALTSPSPOT3_D,NULL            		, 31,287-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9Spot4     	   ,P10_COV_ASPDALTSPSPOT4_D,&l_p0_9Spot3    		,205,287-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9IcoCovUvid		,P10_COV_UVID_D			,&l_p0_9Spot4     	    ,131,426-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_NEEDLE2	(p0_9NdlUvidR		,P10_NDL_UVID_R			,&l_p0_9IcoCovUvid		,140,436-OFFY,  0,  8,icbndluvidr ,0,0,p0_9) 
MY_NEEDLE2	(p0_9NdlUvidD		,P10_NDL_UVID_D			,&l_p0_9NdlUvidM		,140,436-OFFY,  0,  8,icbndluvidd ,0,0,p0_9) 
MY_MOVING2	(p0_9MovPres3R		,P10_MOV_ALTIMPRESS1_R	,&l_p0_9NdlUvidD		,146,488-OFFY,NULL,0,0,icbmovpres3r,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2R		,P10_MOV_ALTIMPRESS10_R	,&l_p0_9MovPres3R		,120,493-OFFY,NULL,0,0,icbmovpres2r,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1R		,P10_MOV_ALTIMPRESS100_R,&l_p0_9MovPres2R		,100,493-OFFY,NULL,0,0,icbmovpres1r,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGR	,P10_MOV_ALTIMPRESS1_R	,&l_p0_9MovPres1R		,146,488-OFFY,NULL,0,0,icbmovpres4hgr,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGR	,P10_MOV_ALTIMPRESS10_R	,&l_p0_9MovPres4HGR		,129,493-OFFY,NULL,0,0,icbmovpres3hgr,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGR	,P10_MOV_ALTIMPRESS10_R	,&l_p0_9MovPres3HGR		,112,493-OFFY,NULL,0,0,icbmovpres2hgr,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGR	,P10_MOV_ALTIMPRESS100_R,&l_p0_9MovPres2HGR		, 95,493-OFFY,NULL,0,0,icbmovpres1hgr,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4R		,P10_MOV_UVID4_R		,&l_p0_9MovPres1HGR		,177,396-OFFY,NULL,0,0,icbmovuvid4r,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3R		,P10_MOV_UVID3_R		,&l_p0_9MovUvid4R		,143,402-OFFY,NULL,0,0,icbmovuvid3r,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2R		,P10_MOV_UVID2_R		,&l_p0_9MovUvid3R		,109,402-OFFY,NULL,0,0,icbmovuvid2r,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1R		,P10_MOV_UVID1_R		,&l_p0_9MovUvid2R		, 75,402-OFFY,NULL,0,0,icbmovuvid1r,0,100,p0_9) 
MY_MOVING2	(p0_9MovPres3D		,P10_MOV_ALTIMPRESS1_D	,&l_p0_9MovUvid1M		,146,488-OFFY,NULL,0,0,icbmovpres3d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2D		,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres3D		,120,493-OFFY,NULL,0,0,icbmovpres2d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1D		,P10_MOV_ALTIMPRESS100_D,&l_p0_9MovPres2D		,100,493-OFFY,NULL,0,0,icbmovpres1d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGD	,P10_MOV_ALTIMPRESS1_D	,&l_p0_9MovPres1D		,146,488-OFFY,NULL,0,0,icbmovpres4hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGD	,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres4HGD		,129,493-OFFY,NULL,0,0,icbmovpres3hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGD	,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres3HGD		,112,493-OFFY,NULL,0,0,icbmovpres2hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGD	,P10_MOV_ALTIMPRESS100_D,&l_p0_9MovPres2HGD		, 95,493-OFFY,NULL,0,0,icbmovpres1hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4D		,P10_MOV_UVID4_D		,&l_p0_9MovPres1HGD		,177,396-OFFY,NULL,0,0,icbmovuvid4d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3D		,P10_MOV_UVID3_D		,&l_p0_9MovUvid4D		,143,402-OFFY,NULL,0,0,icbmovuvid3d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2D		,P10_MOV_UVID2_D		,&l_p0_9MovUvid3D		,109,402-OFFY,NULL,0,0,icbmovuvid2d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1D		,P10_MOV_UVID1_D		,&l_p0_9MovUvid2D		, 75,402-OFFY,NULL,0,0,icbmovuvid1d,0,100,p0_9) 
MY_ICON2	(p0_9IcoSclUvidEng	,P10_SCL_UVIDINTL_D		,&l_p0_9MovUvid1D		, 21,313-OFFY,icbscl_p0_9,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9Ico			,P10_BACKGROUND_D		,&l_p0_9IcoSclUvidEng	,  0,  0,icb_Ico	,PANEL_LIGHT_MAX,p0_9) 
MY_STATIC2	(p0_9bg,p0_9_list	,P10_BACKGROUND_D		,&l_p0_9Ico				, p0_9);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p0_9Spot3     	   ,P10_COV_ASPDALTSPSPOT3_D,NULL            		, 31,287-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9Spot4     	   ,P10_COV_ASPDALTSPSPOT4_D,&l_p0_9Spot3    		,205,287-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9IcoCovUvid		,P10_COV_UVID_D			,&l_p0_9Spot4     	    ,131,426-OFFY,icb_Ico,PANEL_LIGHT_MAX,p0_9)
MY_NEEDLE2	(p0_9NdlUvidM		,P10_NDL_UVID_M			,&l_p0_9NdlUvidP		,140,436-OFFY,  0,  8,icbndluvidm ,0,0,p0_9) 
MY_NEEDLE2	(p0_9NdlUvidD		,P10_NDL_UVID_D			,&l_p0_9NdlUvidM		,140,436-OFFY,  0,  8,icbndluvidd ,0,0,p0_9) 
MY_MOVING2	(p0_9MovPres3M		,P10_MOV_ALTIMPRESS1_M	,&l_p0_9MovUvid1P		,146,488-OFFY,NULL,0,0,icbmovpres3m,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2M		,P10_MOV_ALTIMPRESS10_M	,&l_p0_9MovPres3M		,120,493-OFFY,NULL,0,0,icbmovpres2m,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1M		,P10_MOV_ALTIMPRESS100_M,&l_p0_9MovPres2M		,100,493-OFFY,NULL,0,0,icbmovpres1m,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGM	,P10_MOV_ALTIMPRESS1_M	,&l_p0_9MovPres1M		,146,488-OFFY,NULL,0,0,icbmovpres4hgm,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGM	,P10_MOV_ALTIMPRESS10_M	,&l_p0_9MovPres4HGM		,129,493-OFFY,NULL,0,0,icbmovpres3hgm,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGM	,P10_MOV_ALTIMPRESS10_M	,&l_p0_9MovPres3HGM		,112,493-OFFY,NULL,0,0,icbmovpres2hgm,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGM	,P10_MOV_ALTIMPRESS100_M,&l_p0_9MovPres2HGM		, 95,493-OFFY,NULL,0,0,icbmovpres1hgm,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4M		,P10_MOV_UVID4_M		,&l_p0_9MovPres1HGM		,177,396-OFFY,NULL,0,0,icbmovuvid4m,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3M		,P10_MOV_UVID3_M		,&l_p0_9MovUvid4M		,143,402-OFFY,NULL,0,0,icbmovuvid3m,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2M		,P10_MOV_UVID2_M		,&l_p0_9MovUvid3M		,109,402-OFFY,NULL,0,0,icbmovuvid2m,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1M		,P10_MOV_UVID1_M		,&l_p0_9MovUvid2M		, 75,402-OFFY,NULL,0,0,icbmovuvid1m,0,100,p0_9) 
MY_MOVING2	(p0_9MovPres3D		,P10_MOV_ALTIMPRESS1_D	,&l_p0_9MovUvid1M		,146,488-OFFY,NULL,0,0,icbmovpres3d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2D		,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres3D		,120,493-OFFY,NULL,0,0,icbmovpres2d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1D		,P10_MOV_ALTIMPRESS100_D,&l_p0_9MovPres2D		,100,493-OFFY,NULL,0,0,icbmovpres1d,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres4HGD	,P10_MOV_ALTIMPRESS1_D	,&l_p0_9MovPres1D		,146,488-OFFY,NULL,0,0,icbmovpres4hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres3HGD	,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres4HGD		,129,493-OFFY,NULL,0,0,icbmovpres3hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres2HGD	,P10_MOV_ALTIMPRESS10_D	,&l_p0_9MovPres3HGD		,112,493-OFFY,NULL,0,0,icbmovpres2hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovPres1HGD	,P10_MOV_ALTIMPRESS100_D,&l_p0_9MovPres2HGD		, 95,493-OFFY,NULL,0,0,icbmovpres1hgd,0,10,p0_9) 
MY_MOVING2	(p0_9MovUvid4D		,P10_MOV_UVID4_D		,&l_p0_9MovPres1HGD		,177,396-OFFY,NULL,0,0,icbmovuvid4d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid3D		,P10_MOV_UVID3_D		,&l_p0_9MovUvid4D		,143,402-OFFY,NULL,0,0,icbmovuvid3d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid2D		,P10_MOV_UVID2_D		,&l_p0_9MovUvid3D		,109,402-OFFY,NULL,0,0,icbmovuvid2d,0,100,p0_9) 
MY_MOVING2	(p0_9MovUvid1D		,P10_MOV_UVID1_D		,&l_p0_9MovUvid2D		, 75,402-OFFY,NULL,0,0,icbmovuvid1d,0,100,p0_9) 
MY_ICON2	(p0_9IcoSclUvidEng	,P10_SCL_UVIDINTL_D		,&l_p0_9MovUvid1D		, 21,313-OFFY,icbscl_p0_9,PANEL_LIGHT_MAX,p0_9)
MY_ICON2	(p0_9Ico			,P10_BACKGROUND_D		,&l_p0_9IcoSclUvidEng	,  0,  0,icb_Ico	,PANEL_LIGHT_MAX,p0_9) 
MY_STATIC2	(p0_9bg,p0_9_list	,P10_BACKGROUND_D		,&l_p0_9Ico				, p0_9);
#endif

#endif