/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

#define OFFX	488
#define OFFY	470

//////////////////////////////////////////////////////////////////////////

// ��� ��������

static MAKE_NCB(icb_flagmD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB0_MAIN_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flagmM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB0_MAIN_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flagmP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB0_MAIN_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flagmR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB0_MAIN_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planemD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB0_MAIN_PLANE);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planemM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB0_MAIN_PLANE);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planemP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB0_MAIN_PLANE);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planemR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB0_MAIN_PLANE);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballmD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballmM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballmP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballmR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB0_MAIN_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchmD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB0_MAIN_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchmM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB0_MAIN_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchmP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB0_MAIN_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchmR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB0_MAIN_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_agbmain  		,SHOW_BTN(BTN_AGB_MAIN_ARRET0	,2))
static MAKE_ICB(icb_knb_agb_main    ,SHOW_KRM(KRM_AGB_MAIN0         ,20))

// ��� ���������

static MAKE_NCB(icb_flagaD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB_AUXL_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flagaM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB_AUXL_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flagaP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB_AUXL_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flagaR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB_AUXL_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planeaD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB_AUXL_PLANE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planeaM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB_AUXL_PLANE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planeaP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB_AUXL_PLANE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planeaR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB_AUXL_PLANE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballaD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB_AUXL_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballaM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB_AUXL_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballaP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB_AUXL_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballaR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB_AUXL_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchaD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB_AUXL_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchaM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB_AUXL_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchaP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB_AUXL_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchaR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB_AUXL_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_agbauxl  		,SHOW_BTN(BTN_AGB_AUXL_ARRET	,2))
static MAKE_ICB(icb_knb_agb_auxl    ,SHOW_KRM(KRM_AGB_AUXL          ,20))

// ����������� �������
static NONLINEARITY tbl_airtemp[]={
	{{825-OFFX,1137-OFFY},-100,0},
	{{825-OFFX,1121-OFFY}, -50,0},
	{{839-OFFX,1095-OFFY},   0,0},
	{{866-OFFX,1084-OFFY},  50,0},
	{{894-OFFX,1094-OFFY}, 100,0},
	{{909-OFFX,1119-OFFY}, 150,0}
};

static MAKE_NCB(icb_airtempD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AIR_TEMP);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_airtempM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AIR_TEMP);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_airtempP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AIR_TEMP);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_airtempR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AIR_TEMP);}else HIDE_IMAGE(pelement);return -1;)

// ��-30
static NONLINEARITY tbl_da30vsi[]={
	{{982-OFFX,690-OFFY},-30,0},
	{{967-OFFX,729-OFFY},-20,0},
	{{912-OFFX,747-OFFY},-10,0},
	{{878-OFFX,728-OFFY}, -5,0},
	{{864-OFFX,691-OFFY},  0,0},
	{{877-OFFX,653-OFFY},  5,0},
	{{912-OFFX,633-OFFY}, 10,0},
	{{967-OFFX,651-OFFY}, 20,0},
	{{982-OFFX,690-OFFY}, 30,0}
};

static NONLINEARITY tbl_da30turn[]={
	{{881-OFFX,669-OFFY},-10,0},
	{{921-OFFX,659-OFFY}, 0,0},
	{{962-OFFX,668-OFFY}, 10,0}
};

static MAKE_NCB(icb_da30vsiD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30vsiM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30vsiP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30vsiR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30turnD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_DA30_TURN);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30turnM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_DA30_TURN);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30turnP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_DA30_TURN);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30turnR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_DA30_TURN);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30ballD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_DA30_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30ballM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_DA30_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30ballP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_DA30_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_da30ballR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_DA30_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)

// ��������� ����������
static NONLINEARITY tbl_gforce[]={
	{{514-OFFX,1028-OFFY},  -3,0},
	{{522-OFFX, 998-OFFY},  -2,0},
	{{550-OFFX, 984-OFFY},   0,0},
	{{578-OFFX, 997-OFFY},   1,0},
	{{586-OFFX,1028-OFFY},   2,0},
	{{569-OFFX,1054-OFFY},   3,0},
	{{538-OFFX,1057-OFFY},   4,0}
};

static MAKE_NCB(icb_gfminD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_MIN);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfminM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_MIN);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfminP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_MIN);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfminR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_MIN);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfmaxD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_MAX);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfmaxM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_MAX);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfmaxP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_MAX);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfmaxR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_MAX);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfcurD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_CUR);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfcurM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_CUR);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfcurP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_CUR);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_gfcurR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_GFORCE_CUR);}else HIDE_IMAGE(pelement);return -1;)

// ���������� ������� ��������
static NONLINEARITY tbl_hbrleft[]={
	{{770-OFFX,1089-OFFY},  150,0},
	{{768-OFFX,1114-OFFY},  100,0},
	{{748-OFFX,1134-OFFY},   50,0},
	{{723-OFFX,1130-OFFY},    0,0},
	{{719-OFFX,1134-OFFY},  -10,0}
};				   

static NONLINEARITY tbl_hbrright[]={
	{{750-OFFX,1168-OFFY}, -10,0},
	{{749-OFFX,1164-OFFY},   0,0},
	{{754-OFFX,1137-OFFY},  50,0},
	{{775-OFFX,1119-OFFY}, 100,0},
	{{799-OFFX,1117-OFFY}, 150,0}
};				   

static MAKE_NCB(icb_hbrleftD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_BRAKE_HYD_LEFT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hbrleftM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_BRAKE_HYD_LEFT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hbrleftP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_BRAKE_HYD_LEFT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hbrleftR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_BRAKE_HYD_LEFT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hbrrightD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_BRAKE_HYD_RIGHT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hbrrightM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_BRAKE_HYD_RIGHT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hbrrightP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_BRAKE_HYD_RIGHT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hbrrightR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_BRAKE_HYD_RIGHT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)

// ���������� ������� ���������
static NONLINEARITY tbl_hboleft[]={
	{{665-OFFX,1089-OFFY},  150,0},
	{{664-OFFX,1115-OFFY},  100,0},
	{{644-OFFX,1135-OFFY},   50,0},
	{{619-OFFX,1129-OFFY},    0,0},
	{{614-OFFX,1135-OFFY},  -10,0}
};				   

static NONLINEARITY tbl_hboright[]={
	{{647-OFFX,1168-OFFY}, -10,0},
	{{646-OFFX,1165-OFFY},   0,0},
	{{649-OFFX,1138-OFFY},  50,0},
	{{670-OFFX,1119-OFFY}, 100,0},
	{{694-OFFX,1118-OFFY}, 150,0}
};				   

static MAKE_NCB(icb_hboleftD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_BRAKE_HYD_LEFT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hboleftM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_BRAKE_HYD_LEFT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hboleftP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_BRAKE_HYD_LEFT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hboleftR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_BRAKE_HYD_LEFT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hborightD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_BRAKE_HYD_RIGHT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hborightM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_BRAKE_HYD_RIGHT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hborightP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_BRAKE_HYD_RIGHT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hborightR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_BRAKE_HYD_RIGHT);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)

// ���������� ��������
static NONLINEARITY tbl_hydosn[]={
	{{561-OFFX,1112-OFFY},  150,0},
	{{559-OFFX,1138-OFFY},  100,0},
	{{539-OFFX,1157-OFFY},   50,0},
	{{513-OFFX,1153-OFFY},    0,0},
	{{508-OFFX,1157-OFFY},  -10,0}
};				   

static NONLINEARITY tbl_hydavar[]={
	{{542-OFFX,1193-OFFY}, -10,0},
	{{540-OFFX,1189-OFFY},   0,0},
	{{543-OFFX,1161-OFFY},  50,0},
	{{564-OFFX,1142-OFFY}, 100,0},
	{{589-OFFX,1140-OFFY}, 150,0}
};				   

static MAKE_NCB(icb_hydmainD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_HYD);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hydmainM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_HYD);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hydmainP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_HYD);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hydmainR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_MAIN_HYD);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hydemergD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_HYD);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hydemergM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_HYD);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hydemergP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_HYD);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hydemergR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)) {SHOW_NDL(NDL_EMERG_HYD);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)

// ���
#define P0_MOV_IKUWHITE_M		  P0_MOV_IKUWHITE_R  
#define P0_MOV_IKUYELLOW_M		  P0_MOV_IKUYELLOW_R  

static MAKE_ICB(icb_knob_whi        ,SHOW_GLT(GLT_IKU_RIGHT			,4))
static MAKE_ICB(icb_knob_yel        ,SHOW_GLT(GLT_IKU_LEFT          ,4))
static MAKE_MCB(icb_mov_whiD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_GLT(GLT_IKU_RIGHT			,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_mov_whiM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_GLT(GLT_IKU_RIGHT			,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_mov_whiP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_GLT(GLT_IKU_RIGHT			,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_mov_whiR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_GLT(GLT_IKU_RIGHT			,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_mov_yelD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_GLT(GLT_IKU_LEFT			,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_mov_yelM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_GLT(GLT_IKU_LEFT			,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_mov_yelP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_GLT(GLT_IKU_LEFT			,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_mov_yelR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_GLT(GLT_IKU_LEFT			,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuyelD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_IKU_YELLOW		,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuyelM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_IKU_YELLOW		,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuyelP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_IKU_YELLOW		,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuyelR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_IKU_YELLOW		,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuwhiD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_IKU_WHITE		,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuwhiM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_IKU_WHITE		,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuwhiP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_IKU_WHITE		,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuwhiR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_IKU_WHITE		,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuscaleD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT();	return fabs(360-NDL_GET(NDL_GMK_PRICOURSE));}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuscaleM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT();	return fabs(360-NDL_GET(NDL_GMK_PRICOURSE));}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuscaleP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT();	return fabs(360-NDL_GET(NDL_GMK_PRICOURSE));}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ikuscaleR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT();	return fabs(360-NDL_GET(NDL_GMK_PRICOURSE));}else HIDE_IMAGE(pelement);return -1;)

// �����
static MAKE_ICB(icb_kppms           ,SHOW_KRM(KRM_KPPMS0			,20))
static MAKE_NCB(icb_scaleD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_SCALE		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_scaleM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_SCALE		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_scaleP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_SCALE		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_scaleR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_SCALE		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_glissD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_ILS_GLIDE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_glissM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_ILS_GLIDE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_glissP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_ILS_GLIDE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_glissR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_ILS_GLIDE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_kursD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_ILS_COURSE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_kursM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_ILS_COURSE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_kursP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_ILS_COURSE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_kursR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS0_ILS_COURSE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hdgD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS0_SCALE)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hdgM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS0_SCALE)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hdgP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS0_SCALE)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hdgR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS0_SCALE)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_blenker_kurs	,CHK_BRT(); return POS_GET(POS_KPPMS0_BLK_COURSE)?-1:0+POS_GET(POS_PANEL_STATE);)
static MAKE_ICB(icb_blenker_gliss	,CHK_BRT(); return POS_GET(POS_KPPMS0_BLK_GLIDE)?-1:0+POS_GET(POS_PANEL_STATE);)

// ���
static NONLINEARITY tbl_tas[]={
	{{570-OFFX,738-OFFY},   0,0},
	{{557-OFFX,735-OFFY}, 400,0},
	{{546-OFFX,728-OFFY}, 450,0},
	{{538-OFFX,717-OFFY}, 500,0},
	{{538-OFFX,691-OFFY}, 600,0},
	{{557-OFFX,673-OFFY}, 700,0},
	{{583-OFFX,673-OFFY}, 800,0},
	{{601-OFFX,692-OFFY}, 900,0}
};				  

static NONLINEARITY tbl_ias[]={
	{{570-OFFX,647-OFFY},   0,0},
	{{596-OFFX,653-OFFY}, 100,0},
	{{617-OFFX,670-OFFY}, 150,0},
	{{627-OFFX,695-OFFY}, 200,0},
	{{611-OFFX,745-OFFY}, 300,0},
	{{561-OFFX,761-OFFY}, 400,0},
	{{526-OFFX,742-OFFY}, 500,0},
	{{512-OFFX,705-OFFY}, 600,0},
	{{526-OFFX,669-OFFY}, 700,0}
};

static MAKE_NCB(icb_tasD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_TAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_tasM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_TAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_tasP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_TAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_tasR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_TAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_iasD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_IAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_iasM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_IAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_iasP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_IAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_iasR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_IAS);}else HIDE_IMAGE(pelement);return -1;)

// �����
static NONLINEARITY tbl_pptiz[]={
	{{1043-OFFX,890-OFFY},   0,0},
	{{1013-OFFX,847-OFFY},1000,0},
	{{1059-OFFX,803-OFFY},2000,0},
	{{1105-OFFX,847-OFFY},3000,0},
	{{1075-OFFX,890-OFFY},4000,0}
};				   

static MAKE_NCB(icb_pptizD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_PPTIZ);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_pptizM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_PPTIZ);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_pptizP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_PPTIZ);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_pptizR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_PPTIZ);}else HIDE_IMAGE(pelement);return -1;)

// ��3�
static NONLINEARITY tbl_radalt[]={
	{{1048-OFFX,637-OFFY},-10,0},
	{{1061-OFFX,633-OFFY},  0,0},
	{{1114-OFFX,662-OFFY}, 30,0},
	{{1121-OFFX,703-OFFY}, 50,0},
	{{1104-OFFX,734-OFFY},100,0},
	{{1071-OFFX,751-OFFY},200,0},
	{{1034-OFFX,745-OFFY},300,0},
	{{1009-OFFX,719-OFFY},400,0},
	{{1002-OFFX,683-OFFY},500,0},
	{{1020-OFFX,651-OFFY},600,0},
	{{1047-OFFX,630-OFFY},700,0}
};				   

static MAKE_ICB(icb_lamp			,SHOW_POS(POS_RV3M_LAMP			,2))
static MAKE_NCB(icb_vprD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_RV3M_DH			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_vprM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_RV3M_DH			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_vprP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_RV3M_DH			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_vprR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_RV3M_DH			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_radaltD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_RV3M_ALT			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_radaltM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_RV3M_ALT			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_radaltP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_RV3M_ALT			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_radaltR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_RV3M_ALT			);}else HIDE_IMAGE(pelement);return -1;)

// ����
#define P0_MOV_ALTIMPRESS100_M	   P0_MOV_ALTIMPRESS100_R   
#define P0_MOV_ALTIMPRESS10_M 	   P0_MOV_ALTIMPRESS10_R    
#define P0_MOV_ALTIMPRESS1_M	   P0_MOV_ALTIMPRESS1_R     
#define P0_MOV_ALTIM10K_M		   P0_MOV_ALTIM10K_R         
#define P0_MOV_ALTIM1K_M		   P0_MOV_ALTIM1K_R          
#define P0_MOV_ALTIM100_M		   P0_MOV_ALTIM100_R         
#define P0_MOV_ALTIM10_M		   P0_MOV_ALTIM10_R          

static NONLINEARITY tbl_alt[]={
	{{ 569-OFFX,799-OFFY},    0,0},
	{{ 599-OFFX,809-OFFY},  100,0},
	{{ 618-OFFX,834-OFFY},  200,0},
	{{ 618-OFFX,865-OFFY},  300,0},
	{{ 599-OFFX,890-OFFY},  400,0},
	{{ 570-OFFX,898-OFFY},  500,0},
	{{ 540-OFFX,890-OFFY},  600,0},
	{{ 521-OFFX,865-OFFY},  700,0},
	{{ 521-OFFX,834-OFFY},  800,0},
	{{ 540-OFFX,809-OFFY},  900,0}
};				   

static MAKE_NCB(icb_altimD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_UVID0)*0.36-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_altimM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_UVID0)*0.36-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_altimP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_UVID0)*0.36-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_altimR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_UVID0)*0.36-90;}else HIDE_IMAGE(pelement);return -1;)

static MAKE_MCB(icb_press_100D		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_100_MM	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_100M		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_100_MM	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_100P		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_100_MM	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_100R		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_100_MM	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_10D		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_10_MM	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_10M		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_10_MM	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_10P		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_10_MM	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_10R		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_10_MM	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_1D		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_1_MM		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_1M		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_1_MM		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_1P		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_1_MM		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_press_1R		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_PSI_1_MM		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_10kD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_10000	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_10kM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_10000	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_10kP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_10000	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_10kR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_10000	,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_1kD				,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_1000		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_1kM				,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_1000		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_1kP				,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_1000		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_1kR				,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_1000		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_100D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_100		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_100M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_100		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_100P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_100		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_100R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_100		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_10D				,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_10		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_10M				,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_10		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_10P				,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_10		,0);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_10R				,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_POS(POS_UVID0_ALT_10		,0);}else HIDE_IMAGE(pelement);return -1;)

static MAKE_ICB(icb_gforce   	,SHOW_BTN(BTN_GFORCE_RESET		,2))
static MAKE_ICB(icb_rv3mvpr     ,SHOW_KRM(KRM_RV3M              ,20))
static MAKE_ICB(icb_uvid        ,SHOW_KRM(KRM_UVID0             ,20))
static MAKE_ICB(icb_fuel_ind	,SHOW_AZS(AZS_FUEL_IND			,3))
static MAKE_ICB(icb_tablo01		,SHOW_TBL(TBL_ASPD_LOW			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo02		,SHOW_TBL(TBL_EXIT_OPEN			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo03		,SHOW_TBL(TBL_WARN_GEAR			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo04		,SHOW_TBL(TBL_BANK_L_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo05		,SHOW_TBL(TBL_ADI_FAIL			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo06		,SHOW_TBL(TBL_BANK_R_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo07		,SHOW_TBL(TBL_MARKER			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo08		,SHOW_TBL(TBL_VOR1_TO     		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo09		,SHOW_TBL(TBL_VOR1_FROM    		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo10		,SHOW_TBL(TBL_CAB_ALT_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo11		,SHOW_TBL(TBL_CAB_AIR_PRESS_HIGH,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo12		,SHOW_TBL(TBL_INV_36V_FAIL		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo13		,SHOW_TBL(TBL_INV_115V_FAIL		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo14		,SHOW_TBL(TBL_OIL_PRESS_LOW		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo15		,SHOW_TBL(TBL_AIR_STARTER_ON	,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo16		,SHOW_TBL(TBL_FUEL_LOW_L		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo17		,SHOW_TBL(TBL_FUEL_LOW_R		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo18		,SHOW_TBL(TBL_ENG1_VIB_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo19		,SHOW_TBL(TBL_ENG2_VIB_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo20		,SHOW_TBL(TBL_ENG3_VIB_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo21		,SHOW_TBL(TBL_STALL				,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo31		,SHOW_TBL(TBL_HYD_PUMP_ENG1_FAIL,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo32		,SHOW_TBL(TBL_HYD_PUMP_ENG2_FAIL,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo33		,SHOW_TBL(TBL_RAD_ALTIMETER_FAIL,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))

//////////////////////////////////////////////////////////////////////////

// ��������
static MAKE_MSCB(mcb_fuel_ind_left		,PRS_AZS(AZS_FUEL_IND					,1))
static MAKE_MSCB(mcb_fuel_ind_right		,PRS_AZS(AZS_FUEL_IND					,2))

// ������
static MAKE_MSCB(mcb_disalarm_hyd1		,PRS_BTN(BTN_DIS_ALARM_HYD_PUMP_1_FAIL	))
static MAKE_MSCB(mcb_disalarm_hyd2		,PRS_BTN(BTN_DIS_ALARM_HYD_PUMP_2_FAIL	))
static MAKE_MSCB(mcb_pptiz_0_test		,PRS_BTN(BTN_PPTIZ_TEST_0				))
static MAKE_MSCB(mcb_pptiz_4000_test	,PRS_BTN(BTN_PPTIZ_TEST_4000			))
static MAKE_MSCB(mcb_gforce_reset		,PRS_BTN(BTN_GFORCE_RESET				))
static MAKE_MSCB(mcb_agb_main_arret		,PRS_BTN(BTN_AGB_MAIN_ARRET0			))
static MAKE_MSCB(mcb_agb_auxl_arret		,PRS_BTN(BTN_AGB_AUXL_ARRET				))

// ���������
static MAKE_MSCB(mcb_iku_l_left      	,GLT_DEC(GLT_IKU_LEFT  					))
static MAKE_MSCB(mcb_iku_l_right     	,GLT_INC(GLT_IKU_LEFT  					))
static MAKE_MSCB(mcb_iku_r_left      	,GLT_DEC(GLT_IKU_RIGHT  				))
static MAKE_MSCB(mcb_iku_r_right     	,GLT_INC(GLT_IKU_RIGHT  				))

// ����������
static MAKE_MSCB(mcb_agb_main_left		,KRM_DEC(KRM_AGB_MAIN0				    ))
static MAKE_MSCB(mcb_agb_main_right		,KRM_INC(KRM_AGB_MAIN0				    ))
static MAKE_MSCB(mcb_agb_auxl_left		,KRM_DEC(KRM_AGB_AUXL				    ))
static MAKE_MSCB(mcb_agb_auxl_right		,KRM_INC(KRM_AGB_AUXL				    ))
static MAKE_MSCB(mcb_rv3m_left			,KRM_DEC(KRM_RV3M						))
static MAKE_MSCB(mcb_rv3m_right			,KRM_INC(KRM_RV3M						))
static MAKE_MSCB(mcb_altim_press_left	,KRM_DEC(KRM_UVID0						))
static MAKE_MSCB(mcb_altim_press_right	,KRM_INC(KRM_UVID0						))

static BOOL FSAPI mcb_kppms_kurs_left(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE)
		NDL_DEC(NDL_KPPMS0_SCALE);
	else if(mouse_flags&MOUSE_RIGHTSINGLE)
		NDL_DEC2(NDL_KPPMS0_SCALE,10);

	KRM_DEC(KRM_KPPMS0);
	return TRUE;
}

static BOOL FSAPI mcb_kppms_kurs_right(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE)
		NDL_INC(NDL_KPPMS0_SCALE);
	else if(mouse_flags&MOUSE_RIGHTSINGLE)
		NDL_INC2(NDL_KPPMS0_SCALE,10);

	KRM_INC(KRM_KPPMS0);
	return TRUE;
}

BOOL FSAPI mcb_kppms_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P12);
	return true;
}

BOOL FSAPI mcb_iku_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P11);
	return true;
}

BOOL FSAPI mcb_uvid_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P10);
	return true;
}

BOOL FSAPI mcb_kus_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P110);
	return true;
}

BOOL FSAPI mcb_tgl_lang(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_TGL(POS_PANEL_LANG);
	return true;
}

BOOL FSAPI mcb_agb_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P17);
	return true;
}

BOOL FSAPI mcb_da30_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P18);
	return true;
}

static BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS0_01,0);
		POS_SET(POS_CRS0_02,0);
		POS_SET(POS_CRS0_03,0);
		POS_SET(POS_CRS0_04,0);
		POS_SET(POS_YOKE0_ICON,0);
	}
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_FUEL_IND					))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_FUEL_IND      				))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_PPTIZ_TEST_0				))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_PPTIZ_TEST_4000				))
static MAKE_TCB(tcb_05	,BTN_TT(BTN_DIS_ALARM_HYD_PUMP_1_FAIL	))
static MAKE_TCB(tcb_06	,BTN_TT(BTN_DIS_ALARM_HYD_PUMP_2_FAIL	))
static MAKE_TCB(tcb_07	,BTN_TT(BTN_DIS_FIRE_ALARM				))
static MAKE_TCB(tcb_08	,BTN_TT(BTN_GFORCE_RESET			    ))
static MAKE_TCB(tcb_09	,BTN_TT(BTN_AGB_MAIN_ARRET0				))
static MAKE_TCB(tcb_10	,BTN_TT(BTN_AGB_AUXL_ARRET				))
static MAKE_TCB(tcb_11	,GLT_TT(GLT_IKU_LEFT					))
static MAKE_TCB(tcb_12	,GLT_TT(GLT_IKU_RIGHT					))
static MAKE_TCB(tcb_13	,KRM_TT(KRM_AGB_MAIN0					))
static MAKE_TCB(tcb_14	,KRM_TT(KRM_AGB_AUXL					))
static MAKE_TCB(tcb_15	,KRM_TT(KRM_RV3M						))
static MAKE_TCB(tcb_16	,KRM_TT(KRM_KPPMS0						))
static MAKE_TCB(tcb_17	,KRM_TT(KRM_UVID0						))
static MAKE_TCB(tcb_18	,TBL_TT(TBL_ASPD_LOW					))
static MAKE_TCB(tcb_19	,TBL_TT(TBL_EXIT_OPEN					))
static MAKE_TCB(tcb_20	,TBL_TT(TBL_WARN_GEAR					))
static MAKE_TCB(tcb_21	,TBL_TT(TBL_BANK_L_HIGH					))
static MAKE_TCB(tcb_22	,TBL_TT(TBL_ADI_FAIL					))
static MAKE_TCB(tcb_23	,TBL_TT(TBL_BANK_R_HIGH					))
static MAKE_TCB(tcb_24	,TBL_TT(TBL_MARKER						))
static MAKE_TCB(tcb_25	,TBL_TT(TBL_VOR1_TO     				))
static MAKE_TCB(tcb_26	,TBL_TT(TBL_VOR2_FROM    				))
static MAKE_TCB(tcb_27	,TBL_TT(TBL_CAB_ALT_HIGH				))
static MAKE_TCB(tcb_28	,TBL_TT(TBL_CAB_AIR_PRESS_HIGH			))
static MAKE_TCB(tcb_29	,TBL_TT(TBL_INV_36V_FAIL				))
static MAKE_TCB(tcb_30	,TBL_TT(TBL_INV_115V_FAIL				))
static MAKE_TCB(tcb_31	,TBL_TT(TBL_OIL_PRESS_LOW				))
static MAKE_TCB(tcb_32	,TBL_TT(TBL_AIR_STARTER_ON				))
static MAKE_TCB(tcb_33	,TBL_TT(TBL_FUEL_LOW_L					))
static MAKE_TCB(tcb_34	,TBL_TT(TBL_FUEL_LOW_R					))
static MAKE_TCB(tcb_35	,TBL_TT(TBL_ENG1_VIB_HIGH				))
static MAKE_TCB(tcb_36	,TBL_TT(TBL_ENG2_VIB_HIGH				))
static MAKE_TCB(tcb_37	,TBL_TT(TBL_ENG3_VIB_HIGH				))
static MAKE_TCB(tcb_38	,TBL_TT(TBL_STALL						))
static MAKE_TCB(tcb_39	,TBL_TT(TBL_HYD_PUMP_ENG1_FAIL			))
static MAKE_TCB(tcb_40	,TBL_TT(TBL_HYD_PUMP_ENG2_FAIL			))
static MAKE_TCB(tcb_41	,TBL_TT(TBL_RAD_ALTIMETER_FAIL			))
static MAKE_TCB(tcb_42	,NDL_TT(NDL_AGB0_MAIN_PLANE				))
static MAKE_TCB(tcb_43	,NDL_TT(NDL_AGB_AUXL_PLANE				))
static MAKE_TCB(tcb_44	,NDL_TT(NDL_KUS_TAS						))
static MAKE_TCB(tcb_45	,NDL_TT(NDL_UVID0						))
static MAKE_TCB(tcb_46	,NDL_TT(NDL_KPPMS0_SCALE		        ))
static MAKE_TCB(tcb_47	,NDL_TT(NDL_DA30_TURN			        ))
static MAKE_TCB(tcb_48	,NDL_TT(NDL_RV3M_ALT			        ))
static MAKE_TCB(tcb_49	,NDL_TT(NDL_IKU_WHITE			        ))
static MAKE_TCB(tcb_50	,NDL_TT(NDL_PPTIZ				        ))
static MAKE_TCB(tcb_51	,NDL_TT(NDL_GFORCE_CUR					))
static MAKE_TCB(tcb_52	,NDL_TT(NDL_MAIN_HYD			        ))
static MAKE_TCB(tcb_53	,NDL_TT(NDL_MAIN_BRAKE_HYD_LEFT			))
static MAKE_TCB(tcb_54	,NDL_TT(NDL_EMERG_BRAKE_HYD_LEFT		))
static MAKE_TCB(tcb_55	,NDL_TT(NDL_AIR_TEMP					))
static MAKE_TCB(tcb_56	,POS_TT(POS_PANEL_LANG					))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MAKE_TTA(tcb_20	)
MAKE_TTA(tcb_21	)
MAKE_TTA(tcb_22	) 
MAKE_TTA(tcb_23	)
MAKE_TTA(tcb_24	)
MAKE_TTA(tcb_25	)
MAKE_TTA(tcb_26	)
MAKE_TTA(tcb_27	) 
MAKE_TTA(tcb_28	)
MAKE_TTA(tcb_29	)
MAKE_TTA(tcb_30	)
MAKE_TTA(tcb_31	)
MAKE_TTA(tcb_32	) 
MAKE_TTA(tcb_33	)
MAKE_TTA(tcb_34	)
MAKE_TTA(tcb_35	)
MAKE_TTA(tcb_36	)
MAKE_TTA(tcb_37	) 
MAKE_TTA(tcb_38	)
MAKE_TTA(tcb_39	)
MAKE_TTA(tcb_40	)
MAKE_TTA(tcb_41	)
MAKE_TTA(tcb_42	)
MAKE_TTA(tcb_43	)
MAKE_TTA(tcb_44	)
MAKE_TTA(tcb_45	)
MAKE_TTA(tcb_46	)
MAKE_TTA(tcb_47	) 
MAKE_TTA(tcb_48	)
MAKE_TTA(tcb_49	)
MAKE_TTA(tcb_50	)
MAKE_TTA(tcb_51	)
MAKE_TTA(tcb_52	) 
MAKE_TTA(tcb_53	)
MAKE_TTA(tcb_54	)
MAKE_TTA(tcb_55	)
MAKE_TTA(tcb_56	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_4,HELP_NONE,0,0)

MOUSE_PBOX(0,0,P0_BACKGROUND4_D_SX,P0_BACKGROUND4_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_TBOX("56", 10, 70, 140, 70,CURSOR_HAND,MOUSE_LR,mcb_tgl_lang)

// �������
MOUSE_TBOX("42", 655-OFFX, 612-OFFY, 180, 180,CURSOR_HAND,MOUSE_LR,mcb_agb_big)    // NDL_AGB_MAIN_PLANE	        
MOUSE_TTPB("43", 938-OFFX, 992-OFFY, 180, 180)    // NDL_AGB_AUXL_PLANE	        
MOUSE_TBOX("44", 500-OFFX, 640-OFFY, 135, 135,CURSOR_HAND,MOUSE_LR,mcb_kus_big)    // NDL_KUS_TAS					
MOUSE_TBOX("45", 500-OFFX, 790-OFFY, 135, 135,CURSOR_HAND,MOUSE_LR,mcb_uvid_big)    // NDL_UVID						
MOUSE_TBOX("46", 679-OFFX, 837-OFFY, 140, 140,CURSOR_HAND,MOUSE_LR,mcb_kppms_big)    // NDL_KPPMS_SCALE		        
MOUSE_TBOX("47", 856-OFFX, 628-OFFY, 120, 120,CURSOR_HAND,MOUSE_LR,mcb_da30_big)    // NDL_DA30_TURN			        
MOUSE_TTPB("48", 996-OFFX, 628-OFFY, 120, 120)    // NDL_RV3M_ALT			        
MOUSE_TBOX("49", 853-OFFX, 784-OFFY, 120, 120,CURSOR_HAND,MOUSE_LR,mcb_iku_big)    // NDL_IKU_WHITE			        
MOUSE_TTPB("50", 995-OFFX, 784-OFFY, 120, 120)    // NDL_PPTIZ				        
MOUSE_TTPB("51", 500-OFFX, 976-OFFY, 100, 100)    // NDL_GFORCE_CUR		        
MOUSE_TTPB("52", 497-OFFX,1100-OFFY, 100, 100)    // NDL_MAIN_HYD			        
MOUSE_TTPB("53", 604-OFFX,1078-OFFY, 100, 100)    // NDL_MAIN_BRAKE_HYD_LEFT		
MOUSE_TTPB("54", 710-OFFX,1078-OFFY, 100, 100)    // NDL_EMERG_BRAKE_HYD_LEFT		
MOUSE_TTPB("55", 815-OFFX,1078-OFFY, 100, 100)    // NDL_AIR_TEMP					

// ��������					  
MOUSE_TBOX( "1",  975-OFFX,  925-OFFY,P0_AZS_FUELIND_D_00_SX/2			,P0_AZS_FUELIND_D_00_SY			,CURSOR_HAND,MOUSE_DLR,mcb_fuel_ind_left)
MOUSE_TBOX( "2", 1055-OFFX,  925-OFFY,P0_AZS_FUELIND_D_00_SX/2			,P0_AZS_FUELIND_D_00_SY			,CURSOR_HAND,MOUSE_DLR,mcb_fuel_ind_right)

// ������			 
MOUSE_TBOX( "3", 1023-OFFX,  906-OFFY, 20, 20,CURSOR_HAND,MOUSE_DLR,mcb_pptiz_0_test)
MOUSE_TBOX( "4", 1079-OFFX,  906-OFFY, 20, 20,CURSOR_HAND,MOUSE_DLR,mcb_pptiz_4000_test)
MOUSE_TBOX( "5",  644-OFFX, 1015-OFFY, 42, 30,CURSOR_HAND,MOUSE_DLR,mcb_disalarm_hyd1)
MOUSE_TBOX( "6",  813-OFFX, 1015-OFFY, 42, 30,CURSOR_HAND,MOUSE_DLR,mcb_disalarm_hyd2)
MOUSE_TBOX( "8",  580-OFFX, 1054-OFFY, 20, 20,CURSOR_HAND,MOUSE_DLR,mcb_gforce_reset)
MOUSE_TBOX( "9",  805-OFFX,  620-OFFY, 35, 30,CURSOR_HAND,MOUSE_DLR,mcb_agb_main_arret)
MOUSE_TBOX( "10",1080-OFFX, 1010-OFFY, 45, 30,CURSOR_HAND,MOUSE_DLR,mcb_agb_auxl_arret) 

// ���������
MOUSE_TBOX( "11",  860-OFFX,  895-OFFY, 20, 20,CURSOR_DOWNARROW,MOUSE_LR  ,mcb_iku_l_left) 
MOUSE_TBOX( "11",  880-OFFX,  895-OFFY, 20, 20,CURSOR_UPARROW  ,MOUSE_LR  ,mcb_iku_l_right) 
MOUSE_TBOX( "12",  950-OFFX,  895-OFFY, 20, 20,CURSOR_DOWNARROW,MOUSE_LR  ,mcb_iku_r_left) 
MOUSE_TBOX( "12",  970-OFFX,  895-OFFY, 20, 20,CURSOR_UPARROW  ,MOUSE_LR  ,mcb_iku_r_right) 

// ����������					 
MOUSE_TSHB( "13",  640-OFFX,  760-OFFY, 40, 30,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_agb_main_left,mcb_agb_main_right) 
MOUSE_TSHB( "14",  925-OFFX, 1135-OFFY, 40, 30,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_agb_auxl_left,mcb_agb_auxl_right) 
MOUSE_TSHB( "15",  995-OFFX,  740-OFFY, 30, 30,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_rv3m_left,mcb_rv3m_right) 
MOUSE_TSHB( "16",  790-OFFX,  955-OFFY, 30, 20,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_kppms_kurs_left,mcb_kppms_kurs_right) 
MOUSE_TSHB( "17",  550-OFFX,  920-OFFY, 40, 30,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_altim_press_left,mcb_altim_press_right) 

// �����            
MOUSE_TTPB( "18",  669-OFFX,  550-OFFY, 57,51)    // TBL_ASPD_LOW                  		
MOUSE_TTPB( "19",  713-OFFX,  504-OFFY, 57,46)    // TBL_EXIT_OPEN                 		
MOUSE_TTPB( "20",  726-OFFX,  550-OFFY, 44,51)    // TBL_WARN_GEAR                 		
MOUSE_TTPB( "21",  770-OFFX,  504-OFFY, 46,46)    // TBL_BANK_L_HIGH               		
MOUSE_TTPB( "22",  770-OFFX,  550-OFFY, 46,51)    // TBL_ADI_FAIL                  		
MOUSE_TTPB( "23",  816-OFFX,  504-OFFY, 45,46)    // TBL_BANK_R_HIGH               		
MOUSE_TTPB( "24",  816-OFFX,  550-OFFY, 45,51)    // TBL_MARKER                    		
MOUSE_TTPB( "25",  861-OFFX,  504-OFFY, 45,46)    // TBL_AP_FAIL_BANK              		
MOUSE_TTPB( "26",  861-OFFX,  550-OFFY, 45,51)    // TBL_AP_FAIL_PITCH             		
MOUSE_TTPB( "27",  906-OFFX,  504-OFFY, 46,46)    // TBL_CAB_ALT_HIGH              		
MOUSE_TTPB( "28",  906-OFFX,  550-OFFY, 46,51)    // TBL_CAB_AIR_PRESS_HIGH        		
MOUSE_TTPB( "29",  952-OFFX,  504-OFFY, 45,46)    // TBL_INV_36V_FAIL              		
MOUSE_TTPB( "30",  952-OFFX,  550-OFFY, 45,51)    // TBL_INV_115V_FAIL             		
MOUSE_TTPB( "31",  997-OFFX,  504-OFFY, 46,46)    // TBL_OIL_PRESS_LOW             		
MOUSE_TTPB( "32",  997-OFFX,  550-OFFY, 46,32)    // TBL_AIR_STARTER_ON            		
MOUSE_TTPB( "33", 1043-OFFX,  550-OFFY, 45,32)    // TBL_FUEL_LOW_L                		
MOUSE_TTPB( "34", 1088-OFFX,  550-OFFY, 46,32)    // TBL_FUEL_LOW_R                		
MOUSE_TTPB( "35",  997-OFFX,  582-OFFY, 46,41)    // TBL_ENG1_VIB_HIGH             		
MOUSE_TTPB( "36", 1043-OFFX,  582-OFFY, 45,41)    // TBL_ENG2_VIB_HIGH             		
MOUSE_TTPB( "37", 1088-OFFX,  582-OFFY, 46,41)    // TBL_ENG3_VIB_HIGH             		
MOUSE_TTPB( "38", 1043-OFFX,  470-OFFY, 49,49)    // TBL_STALL                     		
MOUSE_TTPB( "39",  696-OFFX, 1013-OFFY, 54,42)    // TBL_HYD_PUMP_ENG1_FAIL        		
MOUSE_TTPB( "40",  750-OFFX, 1013-OFFY, 55,42)    // TBL_HYD_PUMP_ENG2_FAIL        		
MOUSE_TTPB( "41",  863-OFFX, 1013-OFFY, 58,42)    // TBL_RAD_ALTIMETER_FAIL        		

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p0_4_list; 
GAUGE_HEADER_FS700_EX(P0_BACKGROUND4_D_SX, "p0_4", &p0_4_list, rect_p0_4, 0, 0, 0, 0, p0_4); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_tablo01			,P0_TBL_01_D_00				,NULL					, 669-OFFX, 550-OFFY,icb_tablo01,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo02			,P0_TBL_02_D_00				,&l_p0_tablo01			, 713-OFFX, 504-OFFY,icb_tablo02,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo03			,P0_TBL_03_D_00				,&l_p0_tablo02			, 726-OFFX, 550-OFFY,icb_tablo03,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo04			,P0_TBL_04_D_00				,&l_p0_tablo03			, 770-OFFX, 504-OFFY,icb_tablo04,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo05			,P0_TBL_05_D_00				,&l_p0_tablo04			, 770-OFFX, 550-OFFY,icb_tablo05,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo06			,P0_TBL_06_D_00				,&l_p0_tablo05			, 816-OFFX, 504-OFFY,icb_tablo06,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo07			,P0_TBL_07_D_00				,&l_p0_tablo06			, 816-OFFX, 550-OFFY,icb_tablo07,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo08			,P0_TBL_08_D_00				,&l_p0_tablo07			, 861-OFFX, 504-OFFY,icb_tablo08,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo09			,P0_TBL_09_D_00				,&l_p0_tablo08			, 861-OFFX, 550-OFFY,icb_tablo09,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo10			,P0_TBL_10_D_00				,&l_p0_tablo09			, 906-OFFX, 504-OFFY,icb_tablo10,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo11			,P0_TBL_11_D_00				,&l_p0_tablo10			, 906-OFFX, 550-OFFY,icb_tablo11,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo12			,P0_TBL_12_D_00				,&l_p0_tablo11			, 952-OFFX, 504-OFFY,icb_tablo12,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo13			,P0_TBL_13_D_00				,&l_p0_tablo12			, 952-OFFX, 550-OFFY,icb_tablo13,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo14			,P0_TBL_14_D_00				,&l_p0_tablo13			, 997-OFFX, 504-OFFY,icb_tablo14,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo15			,P0_TBL_15_D_00				,&l_p0_tablo14			, 997-OFFX, 550-OFFY,icb_tablo15,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo16			,P0_TBL_16_D_00				,&l_p0_tablo15			,1043-OFFX, 550-OFFY,icb_tablo16,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo17			,P0_TBL_17_D_00				,&l_p0_tablo16			,1088-OFFX, 550-OFFY,icb_tablo17,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo18			,P0_TBL_18_D_00				,&l_p0_tablo17			, 997-OFFX, 582-OFFY,icb_tablo18,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo19			,P0_TBL_19_D_00				,&l_p0_tablo18			,1043-OFFX, 582-OFFY,icb_tablo19,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo20			,P0_TBL_20_D_00				,&l_p0_tablo19			,1088-OFFX, 582-OFFY,icb_tablo20,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo21			,P0_TBL_21_D_00				,&l_p0_tablo20			,1043-OFFX, 470-OFFY,icb_tablo21,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo31			,P0_TBL_31_D_00				,&l_p0_tablo21			, 696-OFFX,1013-OFFY,icb_tablo31,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo32			,P0_TBL_32_D_00				,&l_p0_tablo31			, 750-OFFX,1013-OFFY,icb_tablo32,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo33			,P0_TBL_33_D_00				,&l_p0_tablo32			, 863-OFFX,1013-OFFY,icb_tablo33,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(gforce				,P0_BTN_GFORCERESET_D_00	,&l_p0_tablo33			, 580-OFFX,1054-OFFY,icb_gforce		, 2*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(rv3m_vpr			,P0_KNB_RV3MDH_D_00			,&l_gforce				, 994-OFFX, 734-OFFY,icb_rv3mvpr    ,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(altim				,P0_KNB_ALTIM_D_00			,&l_rv3m_vpr			, 549-OFFX, 912-OFFY,icb_uvid       ,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(fuel_ind			,P0_AZS_FUELIND_D_00		,&l_altim				, 980-OFFX, 930-OFFY,icb_fuel_ind	, 3*PANEL_LIGHT_MAX,p0_4)
MY_ICON2	(agb_osn_aret		,P0_BTN_AGBARRETMAIN_D_00	,&l_fuel_ind			, 800-OFFX, 621-OFFY,		icb_agbmain		,2*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(agb_osn_pitch	    ,P0_KNB_AGBMAINPITCH_D_00	,&l_agb_osn_aret		, 645-OFFX, 750-OFFY,		icb_knb_agb_main,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(cov_agb_osn_flag	,P0_COV_AGBMAINFLAG_D		,&l_agb_osn_pitch	    , 662-OFFX, 611-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_osn_flagD	,P0_NDL_AGBMAINFLAG_D		,&l_cov_agb_osn_flag	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmD		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_flagM	,P0_NDL_AGBMAINFLAG_M		,&l_ndl_agb_osn_flagD	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmM		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_flagP	,P0_NDL_AGBMAINFLAG_P		,&l_ndl_agb_osn_flagM	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmP		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_flagR	,P0_NDL_AGBMAINFLAG_R		,&l_ndl_agb_osn_flagP	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmR		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_planeD	,P0_NDL_AGBMAINPLANE_D		,&l_ndl_agb_osn_flagR	, 744-OFFX, 706-OFFY,64, 7,	icb_planemD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_planeM	,P0_NDL_AGBMAINPLANE_M		,&l_ndl_agb_osn_planeD	, 744-OFFX, 706-OFFY,64, 7,	icb_planemM		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_planeP	,P0_NDL_AGBMAINPLANE_P		,&l_ndl_agb_osn_planeM	, 744-OFFX, 706-OFFY,64, 7,	icb_planemP		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_planeR	,P0_NDL_AGBMAINPLANE_R		,&l_ndl_agb_osn_planeP	, 744-OFFX, 706-OFFY,64, 7,	icb_planemR		,0,18,p0_4)  
MY_ICON2	(cov_agb_osn_ball	,P0_COV_AGBMAINBALL_D		,&l_ndl_agb_osn_planeR	, 736-OFFX, 766-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_osn_ballD	,P0_NDL_AGBMAINBALL_D		,&l_cov_agb_osn_ball	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_ballM	,P0_NDL_AGBMAINBALL_M		,&l_ndl_agb_osn_ballD	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmM		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_ballP	,P0_NDL_AGBMAINBALL_P		,&l_ndl_agb_osn_ballM	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmP		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_ballR	,P0_NDL_AGBMAINBALL_R		,&l_ndl_agb_osn_ballP	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmR		,0,18,p0_4)  
MY_ICON2	(cov_agb_osn_rail	,P0_COV_AGBMAINRAIL_D		,&l_ndl_agb_osn_ballR	, 737-OFFX, 643-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_STAT2	(alpha_agb1			,P0_ALPHA_AGB				,&l_cov_agb_osn_rail	, 679-OFFX, 640-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_MOVING2	(mov_agb_osn_pitchD	,P0_MOV_AGBMAINPITCH_D		,&l_alpha_agb1			, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmD		,-282,282,p0_4)
MY_MOVING2	(mov_agb_osn_pitchM	,P0_MOV_AGBMAINPITCH_M		,&l_mov_agb_osn_pitchD	, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmM		,-282,282,p0_4)
MY_MOVING2	(mov_agb_osn_pitchP	,P0_MOV_AGBMAINPITCH_P		,&l_mov_agb_osn_pitchM	, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmP		,-282,282,p0_4)
MY_MOVING2	(mov_agb_osn_pitchR	,P0_MOV_AGBMAINPITCH_R		,&l_mov_agb_osn_pitchP	, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmR		,-282,282,p0_4)
MY_ICON2	(agb_res_aret		,P0_BTN_AGBARRETAUXL_D_00	,&l_mov_agb_osn_pitchR	,1085-OFFX,1006-OFFY,		icb_agbauxl		,2*PANEL_LIGHT_MAX,p0_4)
MY_ICON2	(agb_res_pitch		,P0_KNB_AGBAUXLPITCH_D_00	,&l_agb_res_aret		, 932-OFFX,1134-OFFY,		icb_knb_agb_auxl,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(cov_agb_res_flag	,P0_COV_AGBAUXLFLAG_D		,&l_agb_res_pitch		, 954-OFFX, 994-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_res_flagD	,P0_NDL_AGBAUXLFLAG_D		,&l_cov_agb_res_flag	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaD		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_flagM	,P0_NDL_AGBAUXLFLAG_M		,&l_ndl_agb_res_flagD	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaM		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_flagP	,P0_NDL_AGBAUXLFLAG_P		,&l_ndl_agb_res_flagM	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaP		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_flagR	,P0_NDL_AGBAUXLFLAG_R		,&l_ndl_agb_res_flagP	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaR		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeD	,P0_NDL_AGBAUXLPLANE_D		,&l_ndl_agb_res_flagR	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeM	,P0_NDL_AGBAUXLPLANE_M		,&l_ndl_agb_res_planeD	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaM		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeP	,P0_NDL_AGBAUXLPLANE_P		,&l_ndl_agb_res_planeM	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaP		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeR	,P0_NDL_AGBAUXLPLANE_R		,&l_ndl_agb_res_planeP	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaR		,0,18,p0_4)  
MY_ICON2	(cov_agb_res_ball	,P0_COV_AGBAUXLBALL_D		,&l_ndl_agb_res_planeR	,1019-OFFX,1147-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_res_ballD	,P0_NDL_AGBAUXLBALL_D		,&l_cov_agb_res_ball	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_ballM	,P0_NDL_AGBAUXLBALL_M		,&l_ndl_agb_res_ballD	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaM		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_ballP	,P0_NDL_AGBAUXLBALL_P		,&l_ndl_agb_res_ballM	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaP		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_ballR	,P0_NDL_AGBAUXLBALL_R		,&l_ndl_agb_res_ballP	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaR		,0,18,p0_4)  
MY_ICON2	(cov_agb_res_rail	,P0_COV_AGBAUXLRAIL_D		,&l_ndl_agb_res_ballR	,1020-OFFX,1023-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_STAT2	(alpha_agb2			,P0_ALPHA_AGB				,&l_cov_agb_res_rail	, 964-OFFX,1021-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_MOVING2	(mov_agb_res_pitchD	,P0_MOV_AGBAUXLPITCH_D		,&l_alpha_agb2			, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaD		,-282,282,p0_4)
MY_MOVING2	(mov_agb_res_pitchM	,P0_MOV_AGBAUXLPITCH_M		,&l_mov_agb_res_pitchD	, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaM		,-282,282,p0_4)
MY_MOVING2	(mov_agb_res_pitchP	,P0_MOV_AGBAUXLPITCH_P		,&l_mov_agb_res_pitchM	, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaP		,-282,282,p0_4)
MY_MOVING2	(mov_agb_res_pitchR	,P0_MOV_AGBAUXLPITCH_R		,&l_mov_agb_res_pitchP	, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaR		,-282,282,p0_4)
MY_ICON2	(cov_air_temp       ,P0_COV_TEMPAIR_D			,&l_mov_agb_res_pitchR	, 843-OFFX,1105-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)     
MY_NEEDLE2	(ndl_air_tempD		,P0_NDL_TEMPAIR_D			,&l_cov_air_temp		, 867-OFFX,1128-OFFY, 2, 4,	icb_airtempD	,tbl_airtemp,18,p0_4) 
MY_NEEDLE2	(ndl_air_tempM		,P0_NDL_TEMPAIR_M			,&l_ndl_air_tempD		, 867-OFFX,1128-OFFY, 2, 4,	icb_airtempM	,tbl_airtemp,18,p0_4) 
MY_NEEDLE2	(ndl_air_tempP		,P0_NDL_TEMPAIR_P			,&l_ndl_air_tempM		, 867-OFFX,1128-OFFY, 2, 4,	icb_airtempP	,tbl_airtemp,18,p0_4) 
MY_NEEDLE2	(ndl_air_tempR		,P0_NDL_TEMPAIR_R			,&l_ndl_air_tempP		, 867-OFFX,1128-OFFY, 2, 4,	icb_airtempR	,tbl_airtemp,18,p0_4) 
MY_ICON2	(cov_gforce		    ,P0_COV_GFORCE_D			,&l_ndl_air_tempR		, 545-OFFX,1018-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_gforceD		,P0_NDL_GFORCE_D			,&l_cov_gforce		    , 550-OFFX,1023-OFFY,21, 5,	icb_gfcurD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforceM		,P0_NDL_GFORCE_M			,&l_ndl_gforceD			, 550-OFFX,1023-OFFY,21, 5,	icb_gfcurM		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforceP		,P0_NDL_GFORCE_P			,&l_ndl_gforceM			, 550-OFFX,1023-OFFY,21, 5,	icb_gfcurP		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforceR		,P0_NDL_GFORCE_R			,&l_ndl_gforceP			, 550-OFFX,1023-OFFY,21, 5,	icb_gfcurR		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minD	,P0_NDL_GFORCEMIN_D			,&l_ndl_gforceR			, 550-OFFX,1023-OFFY,21, 5,	icb_gfminD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minM	,P0_NDL_GFORCEMIN_M			,&l_ndl_gforce_minD		, 550-OFFX,1023-OFFY,21, 5,	icb_gfminM		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minP	,P0_NDL_GFORCEMIN_P			,&l_ndl_gforce_minM		, 550-OFFX,1023-OFFY,21, 5,	icb_gfminP		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minR	,P0_NDL_GFORCEMIN_R			,&l_ndl_gforce_minP		, 550-OFFX,1023-OFFY,21, 5,	icb_gfminR		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxD	,P0_NDL_GFORCEMAX_D			,&l_ndl_gforce_minR		, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxM	,P0_NDL_GFORCEMAX_M			,&l_ndl_gforce_maxD		, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxM		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxP	,P0_NDL_GFORCEMAX_P			,&l_ndl_gforce_maxM		, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxP		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxR	,P0_NDL_GFORCEMAX_R			,&l_ndl_gforce_maxP		, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxR		,tbl_gforce,10,p0_4) 
MY_ICON2	(cov_tormoz_main	,P0_COV_BRAKEMAIN_D			,&l_ndl_gforce_maxR		, 712-OFFX,1079-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righD	,P0_NDL_BRAKEMAINRIGHT_D	,&l_cov_tormoz_main		, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightD	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righM	,P0_NDL_BRAKEMAINRIGHT_M	,&l_ndl_trm_main_righD	, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightM	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righP	,P0_NDL_BRAKEMAINRIGHT_P	,&l_ndl_trm_main_righM	, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightP	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righR	,P0_NDL_BRAKEMAINRIGHT_R	,&l_ndl_trm_main_righP	, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightR	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftD	,P0_NDL_BRAKEMAINLEFT_D		,&l_ndl_trm_main_righR	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftD	,tbl_hbrleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftM	,P0_NDL_BRAKEMAINLEFT_M		,&l_ndl_trm_main_leftD	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftM	,tbl_hbrleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftP	,P0_NDL_BRAKEMAINLEFT_P		,&l_ndl_trm_main_leftM	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftP	,tbl_hbrleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftR	,P0_NDL_BRAKEMAINLEFT_R		,&l_ndl_trm_main_leftP	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftR	,tbl_hbrleft    ,18,p0_4) 
MY_ICON2	(cov_tormoz_emer 	,P0_COV_BRAKEEMERG_D		,&l_ndl_trm_main_leftR	, 606-OFFX,1077-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)
MY_NEEDLE2	(ndl_trm_emer_righD	,P0_NDL_BRAKEEMERGRIGHT_D	,&l_cov_tormoz_emer 	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightD	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_righM	,P0_NDL_BRAKEEMERGRIGHT_M	,&l_ndl_trm_emer_righD	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightM	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_righP	,P0_NDL_BRAKEEMERGRIGHT_P	,&l_ndl_trm_emer_righM	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightP	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_righR	,P0_NDL_BRAKEEMERGRIGHT_R	,&l_ndl_trm_emer_righP	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightR	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftD	,P0_NDL_BRAKEEMERGLEFT_D	,&l_ndl_trm_emer_righR	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftD	,tbl_hboleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftM	,P0_NDL_BRAKEEMERGLEFT_M	,&l_ndl_trm_emer_leftD	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftM	,tbl_hboleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftP	,P0_NDL_BRAKEEMERGLEFT_P	,&l_ndl_trm_emer_leftM	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftP	,tbl_hboleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftR	,P0_NDL_BRAKEEMERGLEFT_R	,&l_ndl_trm_emer_leftP	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftR	,tbl_hboleft    ,18,p0_4) 
MY_ICON2	(cov_hydrosys		,P0_COV_HYDROSYS_D			,&l_ndl_trm_emer_leftR	, 501-OFFX,1101-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarD	,P0_NDL_HYDROSYSEMERG_D		,&l_cov_hydrosys		, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergD	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarM	,P0_NDL_HYDROSYSEMERG_M		,&l_ndl_hydrosys_avarD	, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergM	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarP	,P0_NDL_HYDROSYSEMERG_P		,&l_ndl_hydrosys_avarM	, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergP	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarR	,P0_NDL_HYDROSYSEMERG_R		,&l_ndl_hydrosys_avarP	, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergR	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnD	,P0_NDL_HYDROSYSMAIN_D		,&l_ndl_hydrosys_avarR	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainD	,tbl_hydosn     ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnM	,P0_NDL_HYDROSYSMAIN_M		,&l_ndl_hydrosys_osnD	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainM	,tbl_hydosn     ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnP	,P0_NDL_HYDROSYSMAIN_P		,&l_ndl_hydrosys_osnM	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainP	,tbl_hydosn     ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnR	,P0_NDL_HYDROSYSMAIN_R		,&l_ndl_hydrosys_osnP	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainR	,tbl_hydosn     ,18,p0_4) 
MY_ICON2	(iku_whi			,P0_KNB_IKUWHITE_D_00		,&l_ndl_hydrosys_osnR	, 948-OFFX, 893-OFFY,		icb_knob_whi    , 4*PANEL_LIGHT_MAX,p0_4)    
MY_ICON2	(iku_yel			,P0_KNB_IKUYELLOW_D_00		,&l_iku_whi				, 867-OFFX, 893-OFFY,		icb_knob_yel	, 4*PANEL_LIGHT_MAX,p0_4)    
MY_MOVING2	(mov_iku_whiD		,P0_MOV_IKUWHITE_D			,&l_iku_yel				, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiD	,0,3,p0_4)  
MY_MOVING2	(mov_iku_whiM		,P0_MOV_IKUWHITE_M			,&l_mov_iku_whiD		, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiM	,0,3,p0_4)  
MY_MOVING2	(mov_iku_whiP		,P0_MOV_IKUWHITE_P			,&l_mov_iku_whiM		, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiP	,0,3,p0_4)  
MY_MOVING2	(mov_iku_whiR		,P0_MOV_IKUWHITE_R			,&l_mov_iku_whiP		, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiR	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelD		,P0_MOV_IKUYELLOW_D			,&l_mov_iku_whiR		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelD	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelM		,P0_MOV_IKUYELLOW_M			,&l_mov_iku_yelD		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelM	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelP		,P0_MOV_IKUYELLOW_P			,&l_mov_iku_yelM		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelP	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelR		,P0_MOV_IKUYELLOW_R			,&l_mov_iku_yelP		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelR	,0,3,p0_4)  
MY_ICON2	(cov_iku			,P0_COV_IKU_D				,&l_mov_iku_yelR		, 913-OFFX, 839-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)		    
MY_NEEDLE2	(ndl_iku_yelD		,P0_NDL_IKUYELLOW_D			,&l_cov_iku				, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelD		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_yelM		,P0_NDL_IKUYELLOW_M			,&l_ndl_iku_yelD		, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelM		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_yelP		,P0_NDL_IKUYELLOW_P			,&l_ndl_iku_yelM		, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelP		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_yelR		,P0_NDL_IKUYELLOW_R			,&l_ndl_iku_yelP		, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelR		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiD		,P0_NDL_IKUWHITE_D			,&l_ndl_iku_yelR		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiD		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiM		,P0_NDL_IKUWHITE_M			,&l_ndl_iku_whiD		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiM		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiP		,P0_NDL_IKUWHITE_P			,&l_ndl_iku_whiM		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiP		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiR		,P0_NDL_IKUWHITE_R			,&l_ndl_iku_whiP		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiR		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_scaleD	    ,P0_NDL_IKUSCALE_D			,&l_ndl_iku_whiR		, 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleD	,0,2,p0_4)       
MY_NEEDLE2	(ndl_iku_scaleM	    ,P0_NDL_IKUSCALE_M			,&l_ndl_iku_scaleD	    , 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleM	,0,2,p0_4)       
MY_NEEDLE2	(ndl_iku_scaleP	    ,P0_NDL_IKUSCALE_P			,&l_ndl_iku_scaleM	    , 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleP	,0,2,p0_4)       
MY_NEEDLE2	(ndl_iku_scaleR	    ,P0_NDL_IKUSCALE_R			,&l_ndl_iku_scaleP	    , 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleR	,0,2,p0_4)       
MY_ICON2	(kppms_scale		,P0_KNB_KPPMSSCALE_D_00		,&l_ndl_iku_scaleR	    , 791-OFFX, 946-OFFY,		icb_kppms       ,20*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgD		,P0_NDL_KPPMSHDG_D			,&l_kppms_scale			, 750-OFFX, 904-OFFY,2,8,	icb_hdgD		,0,10,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgM		,P0_NDL_KPPMSHDG_M			,&l_ndl_kppms_hdgD		, 750-OFFX, 904-OFFY,2,8,	icb_hdgM		,0,10,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgP		,P0_NDL_KPPMSHDG_P			,&l_ndl_kppms_hdgM		, 750-OFFX, 904-OFFY,2,8,	icb_hdgP		,0,10,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgR		,P0_NDL_KPPMSHDG_R			,&l_ndl_kppms_hdgP		, 750-OFFX, 904-OFFY,2,8,	icb_hdgR		,0,10,p0_4)	
MY_STAT2	(alpha_kppms		,P0_ALPHA_KPPMS				,&l_ndl_kppms_hdgR		, 680-OFFX, 834-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_NEEDLE2	(ndl_kppms_scaleD	,P0_NDL_KPPMSSCALE_D		,&l_alpha_kppms			, 751-OFFX, 904-OFFY,69,69,	icb_scaleD		,0,18,p0_4)	
MY_NEEDLE2	(ndl_kppms_scaleM	,P0_NDL_KPPMSSCALE_M		,&l_ndl_kppms_scaleD	, 751-OFFX, 904-OFFY,69,69,	icb_scaleM		,0,18,p0_4)	
MY_NEEDLE2	(ndl_kppms_scaleP	,P0_NDL_KPPMSSCALE_P		,&l_ndl_kppms_scaleM	, 751-OFFX, 904-OFFY,69,69,	icb_scaleP		,0,18,p0_4)	
MY_NEEDLE2	(ndl_kppms_scaleR	,P0_NDL_KPPMSSCALE_R		,&l_ndl_kppms_scaleP	, 751-OFFX, 904-OFFY,69,69,	icb_scaleR		,0,18,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissD	,P0_SLD_KPPMSILSGLIDE_D		,&l_ndl_kppms_scaleR	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissD	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissM	,P0_SLD_KPPMSILSGLIDE_M		,&l_sld_kpp_ils_glissD	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissM	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissP	,P0_SLD_KPPMSILSGLIDE_P		,&l_sld_kpp_ils_glissM	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissP	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissR	,P0_SLD_KPPMSILSGLIDE_R		,&l_sld_kpp_ils_glissP	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissR	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursD	,P0_SLD_KPPMSILSCOURSE_D	,&l_sld_kpp_ils_glissR	, 748-OFFX, 858-OFFY,		icb_ils_kursD	,0.25,NULL,0,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursM	,P0_SLD_KPPMSILSCOURSE_M	,&l_sld_kpp_ils_kursD	, 748-OFFX, 858-OFFY,		icb_ils_kursM	,0.25,NULL,0,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursP	,P0_SLD_KPPMSILSCOURSE_P	,&l_sld_kpp_ils_kursM	, 748-OFFX, 858-OFFY,		icb_ils_kursP	,0.25,NULL,0,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursR	,P0_SLD_KPPMSILSCOURSE_R	,&l_sld_kpp_ils_kursP	, 748-OFFX, 858-OFFY,		icb_ils_kursR	,0.25,NULL,0,p0_4)	
MY_ICON2	(blenker_kppms_k	,P0_BLK_KPPMSCOURSE_D		,&l_sld_kpp_ils_kursR	, 728-OFFX, 884-OFFY,		icb_blenker_kurs   ,1*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(blenker_kppms_gliss,P0_BLK_KPPMSGLIDESLOPE_D	,&l_blenker_kppms_k		, 760-OFFX, 905-OFFY,		icb_blenker_gliss  ,1*PANEL_LIGHT_MAX,p0_4) 
MY_ICON2	(cov_kus			,P0_COV_KUS_D				,&l_blenker_kppms_gliss	, 562-OFFX, 697-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_kus_iasD       ,P0_NDL_KUSIAS_D			,&l_cov_kus				, 569-OFFX, 704-OFFY,41, 8,	icb_iasD		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_iasM       ,P0_NDL_KUSIAS_M			,&l_ndl_kus_iasD		, 569-OFFX, 704-OFFY,41, 8,	icb_iasM		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_iasP       ,P0_NDL_KUSIAS_P			,&l_ndl_kus_iasM		, 569-OFFX, 704-OFFY,41, 8,	icb_iasP		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_iasR       ,P0_NDL_KUSIAS_R			,&l_ndl_kus_iasP		, 569-OFFX, 704-OFFY,41, 8,	icb_iasR		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_tasD       ,P0_NDL_KUSTAS_D			,&l_ndl_kus_iasR		, 568-OFFX, 703-OFFY,10, 2,	icb_tasD		,tbl_tas,10,p0_4)    
MY_NEEDLE2	(ndl_kus_tasM       ,P0_NDL_KUSTAS_M			,&l_ndl_kus_tasD		, 568-OFFX, 703-OFFY,10, 2,	icb_tasM		,tbl_tas,10,p0_4)    
MY_NEEDLE2	(ndl_kus_tasP       ,P0_NDL_KUSTAS_P			,&l_ndl_kus_tasM		, 568-OFFX, 703-OFFY,10, 2,	icb_tasP		,tbl_tas,10,p0_4)    
MY_NEEDLE2	(ndl_kus_tasR       ,P0_NDL_KUSTAS_R			,&l_ndl_kus_tasP		, 568-OFFX, 703-OFFY,10, 2,	icb_tasR		,tbl_tas,10,p0_4)    
MY_ICON2	(cov_pptiz          ,P0_COV_PPTIZ_D				,&l_ndl_kus_tasR		,1041-OFFX, 830-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_pptizD         ,P0_NDL_PPTIZ_D				,&l_cov_pptiz			,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizD		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_pptizM         ,P0_NDL_PPTIZ_M				,&l_ndl_pptizD			,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizM		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_pptizP         ,P0_NDL_PPTIZ_P				,&l_ndl_pptizM			,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizP		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_pptizR         ,P0_NDL_PPTIZ_R				,&l_ndl_pptizP			,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizR		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_rv3m_vprD	    ,P0_NDL_RV3MDH_D			,&l_ndl_pptizR			,1062-OFFX, 695-OFFY, 4, 8,	icb_vprD		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_vprM	    ,P0_NDL_RV3MDH_M			,&l_ndl_rv3m_vprD	    ,1062-OFFX, 695-OFFY, 4, 8,	icb_vprM		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_vprP	    ,P0_NDL_RV3MDH_P			,&l_ndl_rv3m_vprM	    ,1062-OFFX, 695-OFFY, 4, 8,	icb_vprP		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_vprR	    ,P0_NDL_RV3MDH_R			,&l_ndl_rv3m_vprP	    ,1062-OFFX, 695-OFFY, 4, 8,	icb_vprR		,tbl_radalt,10,p0_4)	
MY_ICON2	(cov_rv3m		    ,P0_COV_RV3M_D				,&l_ndl_rv3m_vprR	    ,1034-OFFX, 627-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltD	,P0_NDL_RV3MRADALT_D		,&l_cov_rv3m		    ,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltD		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltM	,P0_NDL_RV3MRADALT_M		,&l_ndl_rv3m_radaltD	,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltM		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltP	,P0_NDL_RV3MRADALT_P		,&l_ndl_rv3m_radaltM	,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltP		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltR	,P0_NDL_RV3MRADALT_R		,&l_ndl_rv3m_radaltP	,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltR		,tbl_radalt,10,p0_4)	
MY_ICON2	(lamp_rv3m_vpr	    ,P0_LMP_RV3MDH_D_00			,&l_ndl_rv3m_radaltR	,1094-OFFX, 726-OFFY,		icb_lamp		,2*PANEL_LIGHT_MAX,p0_4) 
MY_ICON2	(cov_altim          ,P0_COV_ALTIM_D				,&l_lamp_rv3m_vpr	    , 563-OFFX, 847-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_altimD         ,P0_NDL_ALTIM_D				,&l_cov_altim			, 569-OFFX, 853-OFFY,3,5,	icb_altimD		,0,10,p0_4)	
MY_NEEDLE2	(ndl_altimM         ,P0_NDL_ALTIM_M				,&l_ndl_altimD			, 569-OFFX, 853-OFFY,3,5,	icb_altimM		,0,10,p0_4)	
MY_NEEDLE2	(ndl_altimP         ,P0_NDL_ALTIM_P				,&l_ndl_altimM			, 569-OFFX, 853-OFFY,3,5,	icb_altimP		,0,10,p0_4)	
MY_NEEDLE2	(ndl_altimR         ,P0_NDL_ALTIM_R				,&l_ndl_altimP			, 569-OFFX, 853-OFFY,3,5,	icb_altimR		,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100D ,P0_MOV_ALTIMPRESS100_D		,&l_ndl_altimR			, 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100D,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100M ,P0_MOV_ALTIMPRESS100_M		,&l_mov_altim_prs_100D	, 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100M,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100P ,P0_MOV_ALTIMPRESS100_P		,&l_mov_altim_prs_100M	, 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100P,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100R ,P0_MOV_ALTIMPRESS100_R		,&l_mov_altim_prs_100P	, 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100R,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10D  ,P0_MOV_ALTIMPRESS10_D		,&l_mov_altim_prs_100R	, 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10D ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10M  ,P0_MOV_ALTIMPRESS10_M		,&l_mov_altim_prs_10D	, 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10M ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10P  ,P0_MOV_ALTIMPRESS10_P		,&l_mov_altim_prs_10M	, 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10P ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10R  ,P0_MOV_ALTIMPRESS10_R		,&l_mov_altim_prs_10P	, 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10R ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1D   ,P0_MOV_ALTIMPRESS1_D	    ,&l_mov_altim_prs_10R	, 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1D	,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1M   ,P0_MOV_ALTIMPRESS1_M	    ,&l_mov_altim_prs_1D	, 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1M	,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1P   ,P0_MOV_ALTIMPRESS1_P	    ,&l_mov_altim_prs_1M	, 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1P	,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1R   ,P0_MOV_ALTIMPRESS1_R	    ,&l_mov_altim_prs_1P	, 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1R	,0,10,p0_4)	
MY_MOVING2	(mov_altim_10kD		,P0_MOV_ALTIM10K_D			,&l_mov_altim_prs_1R	, 535-OFFX, 834-OFFY,NULL,0,0,icb_10kD		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10kM		,P0_MOV_ALTIM10K_M			,&l_mov_altim_10kD		, 535-OFFX, 834-OFFY,NULL,0,0,icb_10kM		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10kP		,P0_MOV_ALTIM10K_P			,&l_mov_altim_10kM		, 535-OFFX, 834-OFFY,NULL,0,0,icb_10kP		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10kR		,P0_MOV_ALTIM10K_R			,&l_mov_altim_10kP		, 535-OFFX, 834-OFFY,NULL,0,0,icb_10kR		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kD      ,P0_MOV_ALTIM1K_D			,&l_mov_altim_10kR		, 553-OFFX, 834-OFFY,NULL,0,0,icb_1kD		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kM      ,P0_MOV_ALTIM1K_M			,&l_mov_altim_1kD		, 553-OFFX, 834-OFFY,NULL,0,0,icb_1kM		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kP      ,P0_MOV_ALTIM1K_P			,&l_mov_altim_1kM		, 553-OFFX, 834-OFFY,NULL,0,0,icb_1kP		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kR      ,P0_MOV_ALTIM1K_R			,&l_mov_altim_1kP		, 553-OFFX, 834-OFFY,NULL,0,0,icb_1kR		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100D     ,P0_MOV_ALTIM100_D			,&l_mov_altim_1kR		, 571-OFFX, 834-OFFY,NULL,0,0,icb_100D		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100M     ,P0_MOV_ALTIM100_M			,&l_mov_altim_100D		, 571-OFFX, 834-OFFY,NULL,0,0,icb_100M		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100P     ,P0_MOV_ALTIM100_P			,&l_mov_altim_100M		, 571-OFFX, 834-OFFY,NULL,0,0,icb_100P		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100R     ,P0_MOV_ALTIM100_R			,&l_mov_altim_100P		, 571-OFFX, 834-OFFY,NULL,0,0,icb_100R		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10D      ,P0_MOV_ALTIM10_D			,&l_mov_altim_100R		, 589-OFFX, 830-OFFY,NULL,0,0,icb_10D		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10M      ,P0_MOV_ALTIM10_M			,&l_mov_altim_10D		, 589-OFFX, 830-OFFY,NULL,0,0,icb_10M		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10P      ,P0_MOV_ALTIM10_P			,&l_mov_altim_10M		, 589-OFFX, 830-OFFY,NULL,0,0,icb_10P		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10R      ,P0_MOV_ALTIM10_R			,&l_mov_altim_10P		, 589-OFFX, 830-OFFY,NULL,0,0,icb_10R		,0,100,p0_4)	
MY_ICON2	(cov_da30		    ,P0_COV_DA30_D				,&l_mov_altim_10R       , 915-OFFX, 679-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiD	    ,P0_NDL_DA30VSI_D	        ,&l_cov_da30		    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiD	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiM	    ,P0_NDL_DA30VSI_M	        ,&l_ndl_da30_vsiD	    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiM	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiP	    ,P0_NDL_DA30VSI_P	        ,&l_ndl_da30_vsiM	    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiP	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiR	    ,P0_NDL_DA30VSI_R	        ,&l_ndl_da30_vsiP	    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiR	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnD	    ,P0_NDL_DA30TURN_D			,&l_ndl_da30_vsiR	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnD	,tbl_da30turn   ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnM	    ,P0_NDL_DA30TURN_M			,&l_ndl_da30_turnD	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnM	,tbl_da30turn   ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnP	    ,P0_NDL_DA30TURN_P			,&l_ndl_da30_turnM	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnP	,tbl_da30turn   ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnR	    ,P0_NDL_DA30TURN_R			,&l_ndl_da30_turnP	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnR	,tbl_da30turn   ,18,p0_4) 
MY_ICON2	(cov_da30_ball	    ,P0_COV_DA30BALL_D			,&l_ndl_da30_turnR	    , 913-OFFX, 706-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)     
MY_NEEDLE2	(ndl_da30_ballD	    ,P0_NDL_DA30BALL_D			,&l_cov_da30_ball	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballD	,0,	18,p0_4) 
MY_NEEDLE2	(ndl_da30_ballM	    ,P0_NDL_DA30BALL_M			,&l_ndl_da30_ballD	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballM	,0,	18,p0_4) 
MY_NEEDLE2	(ndl_da30_ballP	    ,P0_NDL_DA30BALL_P			,&l_ndl_da30_ballM	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballP	,0,	18,p0_4) 
MY_NEEDLE2	(ndl_da30_ballR	    ,P0_NDL_DA30BALL_R			,&l_ndl_da30_ballP	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballR	,0,	18,p0_4) 
MY_ICON2	(p0_4Ico			,P0_BACKGROUND4_D			,&l_ndl_da30_ballR	    ,		 0,		   0,		icb_Ico			,PANEL_LIGHT_MAX	, p0_4) 
MY_STATIC2	(p0_4bg,p0_4_list	,P0_BACKGROUND4				,&l_p0_4Ico				, p0_4);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p0_tablo01			,P0_TBL_01_D_00				,NULL					, 669-OFFX, 550-OFFY,icb_tablo01,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo02			,P0_TBL_02_D_00				,&l_p0_tablo01			, 713-OFFX, 504-OFFY,icb_tablo02,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo03			,P0_TBL_03_D_00				,&l_p0_tablo02			, 726-OFFX, 550-OFFY,icb_tablo03,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo04			,P0_TBL_04_D_00				,&l_p0_tablo03			, 770-OFFX, 504-OFFY,icb_tablo04,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo05			,P0_TBL_05_D_00				,&l_p0_tablo04			, 770-OFFX, 550-OFFY,icb_tablo05,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo06			,P0_TBL_06_D_00				,&l_p0_tablo05			, 816-OFFX, 504-OFFY,icb_tablo06,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo07			,P0_TBL_07_D_00				,&l_p0_tablo06			, 816-OFFX, 550-OFFY,icb_tablo07,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo08			,P0_TBL_08_D_00				,&l_p0_tablo07			, 861-OFFX, 504-OFFY,icb_tablo08,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo09			,P0_TBL_09_D_00				,&l_p0_tablo08			, 861-OFFX, 550-OFFY,icb_tablo09,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo10			,P0_TBL_10_D_00				,&l_p0_tablo09			, 906-OFFX, 504-OFFY,icb_tablo10,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo11			,P0_TBL_11_D_00				,&l_p0_tablo10			, 906-OFFX, 550-OFFY,icb_tablo11,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo12			,P0_TBL_12_D_00				,&l_p0_tablo11			, 952-OFFX, 504-OFFY,icb_tablo12,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo13			,P0_TBL_13_D_00				,&l_p0_tablo12			, 952-OFFX, 550-OFFY,icb_tablo13,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo14			,P0_TBL_14_D_00				,&l_p0_tablo13			, 997-OFFX, 504-OFFY,icb_tablo14,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo15			,P0_TBL_15_D_00				,&l_p0_tablo14			, 997-OFFX, 550-OFFY,icb_tablo15,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo16			,P0_TBL_16_D_00				,&l_p0_tablo15			,1043-OFFX, 550-OFFY,icb_tablo16,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo17			,P0_TBL_17_D_00				,&l_p0_tablo16			,1088-OFFX, 550-OFFY,icb_tablo17,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo18			,P0_TBL_18_D_00				,&l_p0_tablo17			, 997-OFFX, 582-OFFY,icb_tablo18,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo19			,P0_TBL_19_D_00				,&l_p0_tablo18			,1043-OFFX, 582-OFFY,icb_tablo19,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo20			,P0_TBL_20_D_00				,&l_p0_tablo19			,1088-OFFX, 582-OFFY,icb_tablo20,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo21			,P0_TBL_21_D_00				,&l_p0_tablo20			,1043-OFFX, 470-OFFY,icb_tablo21,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo31			,P0_TBL_31_D_00				,&l_p0_tablo21			, 696-OFFX,1013-OFFY,icb_tablo31,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo32			,P0_TBL_32_D_00				,&l_p0_tablo31			, 750-OFFX,1013-OFFY,icb_tablo32,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo33			,P0_TBL_33_D_00				,&l_p0_tablo32			, 863-OFFX,1013-OFFY,icb_tablo33,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(gforce				,P0_BTN_GFORCERESET_D_00	,&l_p0_tablo33			, 580-OFFX,1054-OFFY,icb_gforce		, 2*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(rv3m_vpr			,P0_KNB_RV3MDH_D_00			,&l_gforce				, 994-OFFX, 734-OFFY,icb_rv3mvpr    ,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(altim				,P0_KNB_ALTIM_D_00			,&l_rv3m_vpr			, 549-OFFX, 912-OFFY,icb_uvid       ,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(fuel_ind			,P0_AZS_FUELIND_D_00		,&l_altim				, 980-OFFX, 930-OFFY,icb_fuel_ind	, 3*PANEL_LIGHT_MAX,p0_4)
MY_ICON2	(agb_osn_aret		,P0_BTN_AGBARRETMAIN_D_00	,&l_fuel_ind			, 800-OFFX, 621-OFFY,		icb_agbmain		,2*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(agb_osn_pitch	    ,P0_KNB_AGBMAINPITCH_D_00	,&l_agb_osn_aret		, 645-OFFX, 750-OFFY,		icb_knb_agb_main,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(cov_agb_osn_flag	,P0_COV_AGBMAINFLAG_D		,&l_agb_osn_pitch	    , 662-OFFX, 611-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_osn_flagD	,P0_NDL_AGBMAINFLAG_D		,&l_cov_agb_osn_flag	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmD		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_flagP	,P0_NDL_AGBMAINFLAG_P		,&l_ndl_agb_osn_flagD	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmP		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_planeD	,P0_NDL_AGBMAINPLANE_D		,&l_ndl_agb_osn_flagP	, 744-OFFX, 706-OFFY,64, 7,	icb_planemD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_planeP	,P0_NDL_AGBMAINPLANE_P		,&l_ndl_agb_osn_planeD	, 744-OFFX, 706-OFFY,64, 7,	icb_planemP		,0,18,p0_4)  
MY_ICON2	(cov_agb_osn_ball	,P0_COV_AGBMAINBALL_D		,&l_ndl_agb_osn_planeP	, 736-OFFX, 766-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_osn_ballD	,P0_NDL_AGBMAINBALL_D		,&l_cov_agb_osn_ball	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_ballP	,P0_NDL_AGBMAINBALL_P		,&l_ndl_agb_osn_ballD	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmP		,0,18,p0_4)  
MY_ICON2	(cov_agb_osn_rail	,P0_COV_AGBMAINRAIL_D		,&l_ndl_agb_osn_ballP	, 737-OFFX, 643-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_STAT2	(alpha_agb1			,P0_ALPHA_AGB				,&l_cov_agb_osn_rail	, 679-OFFX, 640-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_MOVING2	(mov_agb_osn_pitchD	,P0_MOV_AGBMAINPITCH_D		,&l_alpha_agb1			, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmD		,-282,282,p0_4)
MY_MOVING2	(mov_agb_osn_pitchP	,P0_MOV_AGBMAINPITCH_P		,&l_mov_agb_osn_pitchD	, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmP		,-282,282,p0_4)
MY_ICON2	(agb_res_aret		,P0_BTN_AGBARRETAUXL_D_00	,&l_mov_agb_osn_pitchP	,1085-OFFX,1006-OFFY,		icb_agbauxl		,2*PANEL_LIGHT_MAX,p0_4)
MY_ICON2	(agb_res_pitch		,P0_KNB_AGBAUXLPITCH_D_00	,&l_agb_res_aret		, 932-OFFX,1134-OFFY,		icb_knb_agb_auxl,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(cov_agb_res_flag	,P0_COV_AGBAUXLFLAG_D		,&l_agb_res_pitch		, 954-OFFX, 994-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_res_flagD	,P0_NDL_AGBAUXLFLAG_D		,&l_cov_agb_res_flag	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaD		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_flagP	,P0_NDL_AGBAUXLFLAG_P		,&l_ndl_agb_res_flagD	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaP		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeD	,P0_NDL_AGBAUXLPLANE_D		,&l_ndl_agb_res_flagP	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeP	,P0_NDL_AGBAUXLPLANE_P		,&l_ndl_agb_res_planeD	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaP		,0,18,p0_4)  
MY_ICON2	(cov_agb_res_ball	,P0_COV_AGBAUXLBALL_D		,&l_ndl_agb_res_planeP	,1019-OFFX,1147-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_res_ballD	,P0_NDL_AGBAUXLBALL_D		,&l_cov_agb_res_ball	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_ballP	,P0_NDL_AGBAUXLBALL_P		,&l_ndl_agb_res_ballD	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaP		,0,18,p0_4)  
MY_ICON2	(cov_agb_res_rail	,P0_COV_AGBAUXLRAIL_D		,&l_ndl_agb_res_ballP	,1020-OFFX,1023-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_STAT2	(alpha_agb2			,P0_ALPHA_AGB				,&l_cov_agb_res_rail	, 964-OFFX,1021-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_MOVING2	(mov_agb_res_pitchD	,P0_MOV_AGBAUXLPITCH_D		,&l_alpha_agb2			, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaD		,-282,282,p0_4)
MY_MOVING2	(mov_agb_res_pitchP	,P0_MOV_AGBAUXLPITCH_P		,&l_mov_agb_res_pitchD	, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaP		,-282,282,p0_4)
MY_ICON2	(cov_air_temp       ,P0_COV_TEMPAIR_D			,&l_mov_agb_res_pitchP	, 843-OFFX,1105-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)     
MY_NEEDLE2	(ndl_air_tempD		,P0_NDL_TEMPAIR_D			,&l_cov_air_temp       , 867-OFFX,1128-OFFY, 2, 4,	icb_airtempD	,tbl_airtemp,18,p0_4) 
MY_NEEDLE2	(ndl_air_tempP		,P0_NDL_TEMPAIR_P			,&l_ndl_air_tempD		, 867-OFFX,1128-OFFY, 2, 4,	icb_airtempP	,tbl_airtemp,18,p0_4) 
MY_ICON2	(cov_gforce		    ,P0_COV_GFORCE_D			,&l_ndl_air_tempP		, 545-OFFX,1018-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_gforceD		,P0_NDL_GFORCE_D			,&l_cov_gforce		    , 550-OFFX,1023-OFFY,21, 5,	icb_gfcurD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforceP		,P0_NDL_GFORCE_P			,&l_ndl_gforceD		, 550-OFFX,1023-OFFY,21, 5,	icb_gfcurP		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minD	,P0_NDL_GFORCEMIN_D			,&l_ndl_gforceP		, 550-OFFX,1023-OFFY,21, 5,	icb_gfminD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minP	,P0_NDL_GFORCEMIN_P			,&l_ndl_gforce_minD	, 550-OFFX,1023-OFFY,21, 5,	icb_gfminP		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxD	,P0_NDL_GFORCEMAX_D			,&l_ndl_gforce_minP	, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxP	,P0_NDL_GFORCEMAX_P			,&l_ndl_gforce_maxD	, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxP		,tbl_gforce,10,p0_4) 
MY_ICON2	(cov_tormoz_main	,P0_COV_BRAKEMAIN_D			,&l_ndl_gforce_maxP	, 712-OFFX,1079-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righD	,P0_NDL_BRAKEMAINRIGHT_D	,&l_cov_tormoz_main	, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightD	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righP	,P0_NDL_BRAKEMAINRIGHT_P	,&l_ndl_trm_main_righD	, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightP	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftD	,P0_NDL_BRAKEMAINLEFT_D		,&l_ndl_trm_main_righP	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftD	,tbl_hbrleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftP	,P0_NDL_BRAKEMAINLEFT_P		,&l_ndl_trm_main_leftD	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftP	,tbl_hbrleft    ,18,p0_4) 
MY_ICON2	(cov_tormoz_emer 	,P0_COV_BRAKEEMERG_D		,&l_ndl_trm_main_leftP	, 606-OFFX,1077-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)
MY_NEEDLE2	(ndl_trm_emer_righD	,P0_NDL_BRAKEEMERGRIGHT_D	,&l_cov_tormoz_emer 	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightD	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_righP	,P0_NDL_BRAKEEMERGRIGHT_P	,&l_ndl_trm_emer_righD	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightP	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftD	,P0_NDL_BRAKEEMERGLEFT_D	,&l_ndl_trm_emer_righP	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftD	,tbl_hboleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftP	,P0_NDL_BRAKEEMERGLEFT_P	,&l_ndl_trm_emer_leftD	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftP	,tbl_hboleft    ,18,p0_4) 
MY_ICON2	(cov_hydrosys		,P0_COV_HYDROSYS_D			,&l_ndl_trm_emer_leftP	, 501-OFFX,1101-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarD	,P0_NDL_HYDROSYSEMERG_D		,&l_cov_hydrosys		, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergD	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarP	,P0_NDL_HYDROSYSEMERG_P		,&l_ndl_hydrosys_avarD	, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergP	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnD	,P0_NDL_HYDROSYSMAIN_D		,&l_ndl_hydrosys_avarP	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainD	,tbl_hydosn     ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnP	,P0_NDL_HYDROSYSMAIN_P		,&l_ndl_hydrosys_osnD	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainP	,tbl_hydosn     ,18,p0_4) 
MY_ICON2	(iku_whi			,P0_KNB_IKUWHITE_D_00		,&l_ndl_hydrosys_osnP	, 948-OFFX, 893-OFFY,		icb_knob_whi    , 4*PANEL_LIGHT_MAX,p0_4)    
MY_ICON2	(iku_yel			,P0_KNB_IKUYELLOW_D_00		,&l_iku_whi			, 867-OFFX, 893-OFFY,		icb_knob_yel	, 4*PANEL_LIGHT_MAX,p0_4)    
MY_MOVING2	(mov_iku_whiD		,P0_MOV_IKUWHITE_D			,&l_iku_yel			, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiD	,0,3,p0_4)  
MY_MOVING2	(mov_iku_whiP		,P0_MOV_IKUWHITE_P			,&l_mov_iku_whiD		, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiP	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelD		,P0_MOV_IKUYELLOW_D			,&l_mov_iku_whiP		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelD	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelP		,P0_MOV_IKUYELLOW_P			,&l_mov_iku_yelD		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelP	,0,3,p0_4)  
MY_ICON2	(cov_iku			,P0_COV_IKU_D				,&l_mov_iku_yelP		, 913-OFFX, 839-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)		    
MY_NEEDLE2	(ndl_iku_yelD		,P0_NDL_IKUYELLOW_D			,&l_cov_iku			, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelD		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_yelP		,P0_NDL_IKUYELLOW_P			,&l_ndl_iku_yelD		, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelP		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiD		,P0_NDL_IKUWHITE_D			,&l_ndl_iku_yelP		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiD		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiP		,P0_NDL_IKUWHITE_P			,&l_ndl_iku_whiD		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiP		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_scaleD	    ,P0_NDL_IKUSCALE_D			,&l_ndl_iku_whiP		, 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleD	,0,2,p0_4)       
MY_NEEDLE2	(ndl_iku_scaleP	    ,P0_NDL_IKUSCALE_P			,&l_ndl_iku_scaleD	    , 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleP	,0,2,p0_4)       
MY_ICON2	(kppms_scale		,P0_KNB_KPPMSSCALE_D_00		,&l_ndl_iku_scaleP	    , 791-OFFX, 946-OFFY,		icb_kppms       ,20*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgD		,P0_NDL_KPPMSHDG_D			,&l_kppms_scale		, 750-OFFX, 904-OFFY,2,8,	icb_hdgD		,0,10,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgP		,P0_NDL_KPPMSHDG_P			,&l_ndl_kppms_hdgD		, 750-OFFX, 904-OFFY,2,8,	icb_hdgP		,0,10,p0_4)	
MY_STAT2	(alpha_kppms		,P0_ALPHA_KPPMS				,&l_ndl_kppms_hdgP		, 680-OFFX, 834-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_NEEDLE2	(ndl_kppms_scaleD	,P0_NDL_KPPMSSCALE_D		,&l_alpha_kppms		, 751-OFFX, 904-OFFY,69,69,	icb_scaleD		,0,18,p0_4)	
MY_NEEDLE2	(ndl_kppms_scaleP	,P0_NDL_KPPMSSCALE_P		,&l_ndl_kppms_scaleD	, 751-OFFX, 904-OFFY,69,69,	icb_scaleP		,0,18,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissD	,P0_SLD_KPPMSILSGLIDE_D		,&l_ndl_kppms_scaleP	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissD	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissP	,P0_SLD_KPPMSILSGLIDE_P		,&l_sld_kpp_ils_glissD	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissP	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursD	,P0_SLD_KPPMSILSCOURSE_D	,&l_sld_kpp_ils_glissP	, 748-OFFX, 858-OFFY,		icb_ils_kursD	,0.25,NULL,0,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursP	,P0_SLD_KPPMSILSCOURSE_P	,&l_sld_kpp_ils_kursD	, 748-OFFX, 858-OFFY,		icb_ils_kursP	,0.25,NULL,0,p0_4)	
MY_ICON2	(blenker_kppms_k	,P0_BLK_KPPMSCOURSE_D		,&l_sld_kpp_ils_kursP	, 728-OFFX, 884-OFFY,		icb_blenker_kurs   ,1*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(blenker_kppms_gliss,P0_BLK_KPPMSGLIDESLOPE_D	,&l_blenker_kppms_k	, 760-OFFX, 905-OFFY,		icb_blenker_gliss  ,1*PANEL_LIGHT_MAX,p0_4) 
MY_ICON2	(cov_kus			,P0_COV_KUS_D				,&l_blenker_kppms_gliss, 562-OFFX, 697-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_kus_iasD       ,P0_NDL_KUSIAS_D			,&l_cov_kus			, 569-OFFX, 704-OFFY,41, 8,	icb_iasD		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_iasP       ,P0_NDL_KUSIAS_P			,&l_ndl_kus_iasD       , 569-OFFX, 704-OFFY,41, 8,	icb_iasP		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_tasD       ,P0_NDL_KUSTAS_D			,&l_ndl_kus_iasP       , 568-OFFX, 703-OFFY,10, 2,	icb_tasD		,tbl_tas,10,p0_4)    
MY_NEEDLE2	(ndl_kus_tasP       ,P0_NDL_KUSTAS_P			,&l_ndl_kus_tasD       , 568-OFFX, 703-OFFY,10, 2,	icb_tasP		,tbl_tas,10,p0_4)    
MY_ICON2	(cov_pptiz          ,P0_COV_PPTIZ_D				,&l_ndl_kus_tasP       ,1041-OFFX, 830-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_pptizD         ,P0_NDL_PPTIZ_D				,&l_cov_pptiz          ,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizD		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_pptizP         ,P0_NDL_PPTIZ_P				,&l_ndl_pptizD         ,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizP		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_rv3m_vprD	    ,P0_NDL_RV3MDH_D			,&l_ndl_pptizP         ,1062-OFFX, 695-OFFY, 4, 8,	icb_vprD		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_vprP	    ,P0_NDL_RV3MDH_P			,&l_ndl_rv3m_vprD	    ,1062-OFFX, 695-OFFY, 4, 8,	icb_vprP		,tbl_radalt,10,p0_4)	
MY_ICON2	(cov_rv3m		    ,P0_COV_RV3M_D				,&l_ndl_rv3m_vprP	    ,1034-OFFX, 627-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltD	,P0_NDL_RV3MRADALT_D		,&l_cov_rv3m		    ,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltD		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltP	,P0_NDL_RV3MRADALT_P		,&l_ndl_rv3m_radaltD	,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltP		,tbl_radalt,10,p0_4)	
MY_ICON2	(lamp_rv3m_vpr	    ,P0_LMP_RV3MDH_D_00			,&l_ndl_rv3m_radaltP	,1094-OFFX, 726-OFFY,		icb_lamp		,2*PANEL_LIGHT_MAX,p0_4) 
MY_ICON2	(cov_altim          ,P0_COV_ALTIM_D				,&l_lamp_rv3m_vpr	    , 563-OFFX, 847-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_altimD         ,P0_NDL_ALTIM_D				,&l_cov_altim          , 569-OFFX, 853-OFFY,3,5,	icb_altimD		,0,10,p0_4)	
MY_NEEDLE2	(ndl_altimP         ,P0_NDL_ALTIM_P				,&l_ndl_altimD         , 569-OFFX, 853-OFFY,3,5,	icb_altimP		,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100D ,P0_MOV_ALTIMPRESS100_D		,&l_ndl_altimP         , 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100D,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100P ,P0_MOV_ALTIMPRESS100_P		,&l_mov_altim_prs_100D , 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100P,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10D  ,P0_MOV_ALTIMPRESS10_D		,&l_mov_altim_prs_100P , 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10D ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10P  ,P0_MOV_ALTIMPRESS10_P		,&l_mov_altim_prs_10D  , 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10P ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1D   ,P0_MOV_ALTIMPRESS1_D	    ,&l_mov_altim_prs_10P  , 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1D	,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1P   ,P0_MOV_ALTIMPRESS1_P	    ,&l_mov_altim_prs_1D   , 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1P	,0,10,p0_4)	
MY_MOVING2	(mov_altim_10kD		,P0_MOV_ALTIM10K_D			,&l_mov_altim_prs_1P   , 535-OFFX, 834-OFFY,NULL,0,0,icb_10kD		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10kP		,P0_MOV_ALTIM10K_P			,&l_mov_altim_10kD		, 535-OFFX, 834-OFFY,NULL,0,0,icb_10kP		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kD      ,P0_MOV_ALTIM1K_D		    ,&l_mov_altim_10kP		, 553-OFFX, 834-OFFY,NULL,0,0,icb_1kD		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kP      ,P0_MOV_ALTIM1K_P		    ,&l_mov_altim_1kD      , 553-OFFX, 834-OFFY,NULL,0,0,icb_1kP		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100D     ,P0_MOV_ALTIM100_D			,&l_mov_altim_1kP      , 571-OFFX, 834-OFFY,NULL,0,0,icb_100D		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100P     ,P0_MOV_ALTIM100_P			,&l_mov_altim_100D     , 571-OFFX, 834-OFFY,NULL,0,0,icb_100P		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10D      ,P0_MOV_ALTIM10_D		    ,&l_mov_altim_100P     , 589-OFFX, 830-OFFY,NULL,0,0,icb_10D		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10P      ,P0_MOV_ALTIM10_P		    ,&l_mov_altim_10D      , 589-OFFX, 830-OFFY,NULL,0,0,icb_10P		,0,100,p0_4)	
MY_ICON2	(cov_da30		    ,P0_COV_DA30_D				,&l_mov_altim_10P      , 915-OFFX, 679-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiD	    ,P0_NDL_DA30VSI_D	        ,&l_cov_da30		    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiD	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiP	    ,P0_NDL_DA30VSI_P	        ,&l_ndl_da30_vsiD	    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiP	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnD	    ,P0_NDL_DA30TURN_D			,&l_ndl_da30_vsiP	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnD	,tbl_da30turn   ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnP	    ,P0_NDL_DA30TURN_P			,&l_ndl_da30_turnD	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnP	,tbl_da30turn   ,18,p0_4) 
MY_ICON2	(cov_da30_ball	    ,P0_COV_DA30BALL_D			,&l_ndl_da30_turnP	    , 913-OFFX, 706-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)     
MY_NEEDLE2	(ndl_da30_ballD	    ,P0_NDL_DA30BALL_D			,&l_cov_da30_ball	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballD	,0,	18,p0_4) 
MY_NEEDLE2	(ndl_da30_ballP	    ,P0_NDL_DA30BALL_P			,&l_ndl_da30_ballD	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballP	,0,	18,p0_4) 
MY_ICON2	(p0_4Ico			,P0_BACKGROUND4_D			,&l_ndl_da30_ballP	    ,		 0,		   0,		icb_Ico			,PANEL_LIGHT_MAX	, p0_4) 
MY_STATIC2	(p0_4bg,p0_4_list	,P0_BACKGROUND4				,&l_p0_4Ico			, p0_4);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_tablo01			,P0_TBL_01_D_00				,NULL					, 669-OFFX, 550-OFFY,icb_tablo01,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo02			,P0_TBL_02_D_00				,&l_p0_tablo01			, 713-OFFX, 504-OFFY,icb_tablo02,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo03			,P0_TBL_03_D_00				,&l_p0_tablo02			, 726-OFFX, 550-OFFY,icb_tablo03,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo04			,P0_TBL_04_D_00				,&l_p0_tablo03			, 770-OFFX, 504-OFFY,icb_tablo04,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo05			,P0_TBL_05_D_00				,&l_p0_tablo04			, 770-OFFX, 550-OFFY,icb_tablo05,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo06			,P0_TBL_06_D_00				,&l_p0_tablo05			, 816-OFFX, 504-OFFY,icb_tablo06,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo07			,P0_TBL_07_D_00				,&l_p0_tablo06			, 816-OFFX, 550-OFFY,icb_tablo07,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo08			,P0_TBL_08_D_00				,&l_p0_tablo07			, 861-OFFX, 504-OFFY,icb_tablo08,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo09			,P0_TBL_09_D_00				,&l_p0_tablo08			, 861-OFFX, 550-OFFY,icb_tablo09,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo10			,P0_TBL_10_D_00				,&l_p0_tablo09			, 906-OFFX, 504-OFFY,icb_tablo10,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo11			,P0_TBL_11_D_00				,&l_p0_tablo10			, 906-OFFX, 550-OFFY,icb_tablo11,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo12			,P0_TBL_12_D_00				,&l_p0_tablo11			, 952-OFFX, 504-OFFY,icb_tablo12,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo13			,P0_TBL_13_D_00				,&l_p0_tablo12			, 952-OFFX, 550-OFFY,icb_tablo13,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo14			,P0_TBL_14_D_00				,&l_p0_tablo13			, 997-OFFX, 504-OFFY,icb_tablo14,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo15			,P0_TBL_15_D_00				,&l_p0_tablo14			, 997-OFFX, 550-OFFY,icb_tablo15,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo16			,P0_TBL_16_D_00				,&l_p0_tablo15			,1043-OFFX, 550-OFFY,icb_tablo16,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo17			,P0_TBL_17_D_00				,&l_p0_tablo16			,1088-OFFX, 550-OFFY,icb_tablo17,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo18			,P0_TBL_18_D_00				,&l_p0_tablo17			, 997-OFFX, 582-OFFY,icb_tablo18,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo19			,P0_TBL_19_D_00				,&l_p0_tablo18			,1043-OFFX, 582-OFFY,icb_tablo19,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo20			,P0_TBL_20_D_00				,&l_p0_tablo19			,1088-OFFX, 582-OFFY,icb_tablo20,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo21			,P0_TBL_21_D_00				,&l_p0_tablo20			,1043-OFFX, 470-OFFY,icb_tablo21,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo31			,P0_TBL_31_D_00				,&l_p0_tablo21			, 696-OFFX,1013-OFFY,icb_tablo31,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo32			,P0_TBL_32_D_00				,&l_p0_tablo31			, 750-OFFX,1013-OFFY,icb_tablo32,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo33			,P0_TBL_33_D_00				,&l_p0_tablo32			, 863-OFFX,1013-OFFY,icb_tablo33,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(gforce				,P0_BTN_GFORCERESET_D_00	,&l_p0_tablo33			, 580-OFFX,1054-OFFY,icb_gforce		, 2*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(rv3m_vpr			,P0_KNB_RV3MDH_D_00			,&l_gforce				, 994-OFFX, 734-OFFY,icb_rv3mvpr    ,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(altim				,P0_KNB_ALTIM_D_00			,&l_rv3m_vpr			, 549-OFFX, 912-OFFY,icb_uvid       ,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(fuel_ind			,P0_AZS_FUELIND_D_00		,&l_altim				, 980-OFFX, 930-OFFY,icb_fuel_ind	, 3*PANEL_LIGHT_MAX,p0_4)
MY_ICON2	(agb_osn_aret		,P0_BTN_AGBARRETMAIN_D_00	,&l_fuel_ind			, 800-OFFX, 621-OFFY,		icb_agbmain		,2*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(agb_osn_pitch	    ,P0_KNB_AGBMAINPITCH_D_00	,&l_agb_osn_aret		, 645-OFFX, 750-OFFY,		icb_knb_agb_main,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(cov_agb_osn_flag	,P0_COV_AGBMAINFLAG_D		,&l_agb_osn_pitch	    , 662-OFFX, 611-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_osn_flagD	,P0_NDL_AGBMAINFLAG_D		,&l_cov_agb_osn_flag	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmD		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_flagR	,P0_NDL_AGBMAINFLAG_R		,&l_ndl_agb_osn_flagD	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmR		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_planeD	,P0_NDL_AGBMAINPLANE_D		,&l_ndl_agb_osn_flagR	, 744-OFFX, 706-OFFY,64, 7,	icb_planemD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_planeR	,P0_NDL_AGBMAINPLANE_R		,&l_ndl_agb_osn_planeD	, 744-OFFX, 706-OFFY,64, 7,	icb_planemR		,0,18,p0_4)  
MY_ICON2	(cov_agb_osn_ball	,P0_COV_AGBMAINBALL_D		,&l_ndl_agb_osn_planeR	, 736-OFFX, 766-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_osn_ballD	,P0_NDL_AGBMAINBALL_D		,&l_cov_agb_osn_ball	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_ballR	,P0_NDL_AGBMAINBALL_R		,&l_ndl_agb_osn_ballD	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmR		,0,18,p0_4)  
MY_ICON2	(cov_agb_osn_rail	,P0_COV_AGBMAINRAIL_D		,&l_ndl_agb_osn_ballR	, 737-OFFX, 643-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_STAT2	(alpha_agb1			,P0_ALPHA_AGB				,&l_cov_agb_osn_rail	, 679-OFFX, 640-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_MOVING2	(mov_agb_osn_pitchD	,P0_MOV_AGBMAINPITCH_D		,&l_alpha_agb1			, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmD		,-282,282,p0_4)
MY_MOVING2	(mov_agb_osn_pitchR	,P0_MOV_AGBMAINPITCH_R		,&l_mov_agb_osn_pitchD	, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmR		,-282,282,p0_4)
MY_ICON2	(agb_res_aret		,P0_BTN_AGBARRETAUXL_D_00	,&l_mov_agb_osn_pitchR	,1085-OFFX,1006-OFFY,		icb_agbauxl		,2*PANEL_LIGHT_MAX,p0_4)
MY_ICON2	(agb_res_pitch		,P0_KNB_AGBAUXLPITCH_D_00	,&l_agb_res_aret		, 932-OFFX,1134-OFFY,		icb_knb_agb_auxl,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(cov_agb_res_flag	,P0_COV_AGBAUXLFLAG_D		,&l_agb_res_pitch		, 954-OFFX, 994-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_res_flagD	,P0_NDL_AGBAUXLFLAG_D		,&l_cov_agb_res_flag	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaD		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_flagR	,P0_NDL_AGBAUXLFLAG_R		,&l_ndl_agb_res_flagD	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaR		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeD	,P0_NDL_AGBAUXLPLANE_D		,&l_ndl_agb_res_flagR	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeR	,P0_NDL_AGBAUXLPLANE_R		,&l_ndl_agb_res_planeD	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaR		,0,18,p0_4)  
MY_ICON2	(cov_agb_res_ball	,P0_COV_AGBAUXLBALL_D		,&l_ndl_agb_res_planeR	,1019-OFFX,1147-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_res_ballD	,P0_NDL_AGBAUXLBALL_D		,&l_cov_agb_res_ball	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_ballR	,P0_NDL_AGBAUXLBALL_R		,&l_ndl_agb_res_ballD	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaR		,0,18,p0_4)  
MY_ICON2	(cov_agb_res_rail	,P0_COV_AGBAUXLRAIL_D		,&l_ndl_agb_res_ballR	,1020-OFFX,1023-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_STAT2	(alpha_agb2			,P0_ALPHA_AGB				,&l_cov_agb_res_rail	, 964-OFFX,1021-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_MOVING2	(mov_agb_res_pitchD	,P0_MOV_AGBAUXLPITCH_D		,&l_alpha_agb2			, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaD		,-282,282,p0_4)
MY_MOVING2	(mov_agb_res_pitchR	,P0_MOV_AGBAUXLPITCH_R		,&l_mov_agb_res_pitchD	, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaR		,-282,282,p0_4)
MY_ICON2	(cov_air_temp       ,P0_COV_TEMPAIR_D			,&l_mov_agb_res_pitchR	, 843-OFFX,1105-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)     
MY_NEEDLE2	(ndl_air_tempD		,P0_NDL_TEMPAIR_D			,&l_cov_air_temp       , 867-OFFX,1128-OFFY, 2, 4,	icb_airtempD	,tbl_airtemp,18,p0_4) 
MY_NEEDLE2	(ndl_air_tempR		,P0_NDL_TEMPAIR_R			,&l_ndl_air_tempD		, 867-OFFX,1128-OFFY, 2, 4,	icb_airtempR	,tbl_airtemp,18,p0_4) 
MY_ICON2	(cov_gforce		    ,P0_COV_GFORCE_D			,&l_ndl_air_tempR		, 545-OFFX,1018-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_gforceD		,P0_NDL_GFORCE_D			,&l_cov_gforce		    , 550-OFFX,1023-OFFY,21, 5,	icb_gfcurD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforceR		,P0_NDL_GFORCE_R			,&l_ndl_gforceD		, 550-OFFX,1023-OFFY,21, 5,	icb_gfcurR		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minD	,P0_NDL_GFORCEMIN_D			,&l_ndl_gforceR		, 550-OFFX,1023-OFFY,21, 5,	icb_gfminD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minR	,P0_NDL_GFORCEMIN_R			,&l_ndl_gforce_minD	, 550-OFFX,1023-OFFY,21, 5,	icb_gfminR		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxD	,P0_NDL_GFORCEMAX_D			,&l_ndl_gforce_minR	, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxR	,P0_NDL_GFORCEMAX_R			,&l_ndl_gforce_maxD	, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxR		,tbl_gforce,10,p0_4) 
MY_ICON2	(cov_tormoz_main	,P0_COV_BRAKEMAIN_D			,&l_ndl_gforce_maxR	, 712-OFFX,1079-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righD	,P0_NDL_BRAKEMAINRIGHT_D	,&l_cov_tormoz_main	, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightD	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righR	,P0_NDL_BRAKEMAINRIGHT_R	,&l_ndl_trm_main_righD	, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightR	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftD	,P0_NDL_BRAKEMAINLEFT_D		,&l_ndl_trm_main_righR	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftD	,tbl_hbrleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftR	,P0_NDL_BRAKEMAINLEFT_R		,&l_ndl_trm_main_leftD	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftR	,tbl_hbrleft    ,18,p0_4) 
MY_ICON2	(cov_tormoz_emer 	,P0_COV_BRAKEEMERG_D		,&l_ndl_trm_main_leftR	, 606-OFFX,1077-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)
MY_NEEDLE2	(ndl_trm_emer_righD	,P0_NDL_BRAKEEMERGRIGHT_D	,&l_cov_tormoz_emer 	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightD	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_righR	,P0_NDL_BRAKEEMERGRIGHT_R	,&l_ndl_trm_emer_righD	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightR	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftD	,P0_NDL_BRAKEEMERGLEFT_D	,&l_ndl_trm_emer_righR	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftD	,tbl_hboleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftR	,P0_NDL_BRAKEEMERGLEFT_R	,&l_ndl_trm_emer_leftD	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftR	,tbl_hboleft    ,18,p0_4) 
MY_ICON2	(cov_hydrosys		,P0_COV_HYDROSYS_D			,&l_ndl_trm_emer_leftR	, 501-OFFX,1101-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarD	,P0_NDL_HYDROSYSEMERG_D		,&l_cov_hydrosys		, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergD	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarR	,P0_NDL_HYDROSYSEMERG_R		,&l_ndl_hydrosys_avarD	, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergR	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnD	,P0_NDL_HYDROSYSMAIN_D		,&l_ndl_hydrosys_avarR	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainD	,tbl_hydosn     ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnR	,P0_NDL_HYDROSYSMAIN_R		,&l_ndl_hydrosys_osnD	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainR	,tbl_hydosn     ,18,p0_4) 
MY_ICON2	(iku_whi			,P0_KNB_IKUWHITE_D_00		,&l_ndl_hydrosys_osnR	, 948-OFFX, 893-OFFY,		icb_knob_whi    , 4*PANEL_LIGHT_MAX,p0_4)    
MY_ICON2	(iku_yel			,P0_KNB_IKUYELLOW_D_00		,&l_iku_whi			, 867-OFFX, 893-OFFY,		icb_knob_yel	, 4*PANEL_LIGHT_MAX,p0_4)    
MY_MOVING2	(mov_iku_whiD		,P0_MOV_IKUWHITE_D			,&l_iku_yel			, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiD	,0,3,p0_4)  
MY_MOVING2	(mov_iku_whiR		,P0_MOV_IKUWHITE_R			,&l_mov_iku_whiD		, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiR	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelD		,P0_MOV_IKUYELLOW_D			,&l_mov_iku_whiR		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelD	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelR		,P0_MOV_IKUYELLOW_R			,&l_mov_iku_yelD		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelR	,0,3,p0_4)  
MY_ICON2	(cov_iku			,P0_COV_IKU_D				,&l_mov_iku_yelR		, 913-OFFX, 839-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)		    
MY_NEEDLE2	(ndl_iku_yelD		,P0_NDL_IKUYELLOW_D			,&l_cov_iku			, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelD		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_yelR		,P0_NDL_IKUYELLOW_R			,&l_ndl_iku_yelD		, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelR		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiD		,P0_NDL_IKUWHITE_D			,&l_ndl_iku_yelR		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiD		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiR		,P0_NDL_IKUWHITE_R			,&l_ndl_iku_whiD		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiR		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_scaleD	    ,P0_NDL_IKUSCALE_D			,&l_ndl_iku_whiR		, 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleD	,0,2,p0_4)       
MY_NEEDLE2	(ndl_iku_scaleR	    ,P0_NDL_IKUSCALE_R			,&l_ndl_iku_scaleD	    , 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleR	,0,2,p0_4)       
MY_ICON2	(kppms_scale		,P0_KNB_KPPMSSCALE_D_00		,&l_ndl_iku_scaleR	    , 791-OFFX, 946-OFFY,		icb_kppms       ,20*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgD		,P0_NDL_KPPMSHDG_D			,&l_kppms_scale		, 750-OFFX, 904-OFFY,2,8,	icb_hdgD		,0,10,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgR		,P0_NDL_KPPMSHDG_R			,&l_ndl_kppms_hdgD		, 750-OFFX, 904-OFFY,2,8,	icb_hdgR		,0,10,p0_4)	
MY_STAT2	(alpha_kppms		,P0_ALPHA_KPPMS				,&l_ndl_kppms_hdgR		, 680-OFFX, 834-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_NEEDLE2	(ndl_kppms_scaleD	,P0_NDL_KPPMSSCALE_D		,&l_alpha_kppms		, 751-OFFX, 904-OFFY,69,69,	icb_scaleD		,0,18,p0_4)	
MY_NEEDLE2	(ndl_kppms_scaleR	,P0_NDL_KPPMSSCALE_R		,&l_ndl_kppms_scaleD	, 751-OFFX, 904-OFFY,69,69,	icb_scaleR		,0,18,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissD	,P0_SLD_KPPMSILSGLIDE_D		,&l_ndl_kppms_scaleR	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissD	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissR	,P0_SLD_KPPMSILSGLIDE_R		,&l_sld_kpp_ils_glissD	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissR	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursD	,P0_SLD_KPPMSILSCOURSE_D	,&l_sld_kpp_ils_glissR	, 748-OFFX, 858-OFFY,		icb_ils_kursD	,0.25,NULL,0,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursR	,P0_SLD_KPPMSILSCOURSE_R	,&l_sld_kpp_ils_kursD	, 748-OFFX, 858-OFFY,		icb_ils_kursR	,0.25,NULL,0,p0_4)	
MY_ICON2	(blenker_kppms_k	,P0_BLK_KPPMSCOURSE_D		,&l_sld_kpp_ils_kursR	, 728-OFFX, 884-OFFY,		icb_blenker_kurs   ,1*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(blenker_kppms_gliss,P0_BLK_KPPMSGLIDESLOPE_D	,&l_blenker_kppms_k	, 760-OFFX, 905-OFFY,		icb_blenker_gliss  ,1*PANEL_LIGHT_MAX,p0_4) 
MY_ICON2	(cov_kus			,P0_COV_KUS_D				,&l_blenker_kppms_gliss, 562-OFFX, 697-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_kus_iasD       ,P0_NDL_KUSIAS_D			,&l_cov_kus			, 569-OFFX, 704-OFFY,41, 8,	icb_iasD		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_iasR       ,P0_NDL_KUSIAS_R			,&l_ndl_kus_iasD       , 569-OFFX, 704-OFFY,41, 8,	icb_iasR		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_tasD       ,P0_NDL_KUSTAS_D			,&l_ndl_kus_iasR       , 568-OFFX, 703-OFFY,10, 2,	icb_tasD		,tbl_tas,10,p0_4)    
MY_NEEDLE2	(ndl_kus_tasR       ,P0_NDL_KUSTAS_R			,&l_ndl_kus_tasD       , 568-OFFX, 703-OFFY,10, 2,	icb_tasR		,tbl_tas,10,p0_4)    
MY_ICON2	(cov_pptiz          ,P0_COV_PPTIZ_D				,&l_ndl_kus_tasR       ,1041-OFFX, 830-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_pptizD         ,P0_NDL_PPTIZ_D				,&l_cov_pptiz          ,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizD		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_pptizR         ,P0_NDL_PPTIZ_R				,&l_ndl_pptizD         ,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizR		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_rv3m_vprD	    ,P0_NDL_RV3MDH_D			,&l_ndl_pptizR         ,1062-OFFX, 695-OFFY, 4, 8,	icb_vprD		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_vprR	    ,P0_NDL_RV3MDH_R			,&l_ndl_rv3m_vprD	    ,1062-OFFX, 695-OFFY, 4, 8,	icb_vprR		,tbl_radalt,10,p0_4)	
MY_ICON2	(cov_rv3m		    ,P0_COV_RV3M_D				,&l_ndl_rv3m_vprR	    ,1034-OFFX, 627-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltD	,P0_NDL_RV3MRADALT_D		,&l_cov_rv3m		    ,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltD		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltR	,P0_NDL_RV3MRADALT_R		,&l_ndl_rv3m_radaltD	,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltR		,tbl_radalt,10,p0_4)	
MY_ICON2	(lamp_rv3m_vpr	    ,P0_LMP_RV3MDH_D_00			,&l_ndl_rv3m_radaltR	,1094-OFFX, 726-OFFY,		icb_lamp		,2*PANEL_LIGHT_MAX,p0_4) 
MY_ICON2	(cov_altim          ,P0_COV_ALTIM_D				,&l_lamp_rv3m_vpr	    , 563-OFFX, 847-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_altimD         ,P0_NDL_ALTIM_D				,&l_cov_altim          , 569-OFFX, 853-OFFY,3,5,	icb_altimD		,0,10,p0_4)	
MY_NEEDLE2	(ndl_altimR         ,P0_NDL_ALTIM_R				,&l_ndl_altimD         , 569-OFFX, 853-OFFY,3,5,	icb_altimR		,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100D ,P0_MOV_ALTIMPRESS100_D		,&l_ndl_altimR         , 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100D,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100R ,P0_MOV_ALTIMPRESS100_R		,&l_mov_altim_prs_100D , 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100R,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10D  ,P0_MOV_ALTIMPRESS10_D		,&l_mov_altim_prs_100R , 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10D ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10R  ,P0_MOV_ALTIMPRESS10_R		,&l_mov_altim_prs_10D  , 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10R ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1D   ,P0_MOV_ALTIMPRESS1_D	    ,&l_mov_altim_prs_10R  , 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1D	,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1R   ,P0_MOV_ALTIMPRESS1_R	    ,&l_mov_altim_prs_1D   , 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1R	,0,10,p0_4)	
MY_MOVING2	(mov_altim_10kD		,P0_MOV_ALTIM10K_D			,&l_mov_altim_prs_1R   , 535-OFFX, 834-OFFY,NULL,0,0,icb_10kD		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10kR		,P0_MOV_ALTIM10K_R			,&l_mov_altim_10kD		, 535-OFFX, 834-OFFY,NULL,0,0,icb_10kR		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kD      ,P0_MOV_ALTIM1K_D		    ,&l_mov_altim_10kR		, 553-OFFX, 834-OFFY,NULL,0,0,icb_1kD		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kR      ,P0_MOV_ALTIM1K_R		    ,&l_mov_altim_1kD      , 553-OFFX, 834-OFFY,NULL,0,0,icb_1kR		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100D     ,P0_MOV_ALTIM100_D			,&l_mov_altim_1kR      , 571-OFFX, 834-OFFY,NULL,0,0,icb_100D		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100R     ,P0_MOV_ALTIM100_R			,&l_mov_altim_100D     , 571-OFFX, 834-OFFY,NULL,0,0,icb_100R		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10D      ,P0_MOV_ALTIM10_D		    ,&l_mov_altim_100R     , 589-OFFX, 830-OFFY,NULL,0,0,icb_10D		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10R      ,P0_MOV_ALTIM10_R		    ,&l_mov_altim_10D      , 589-OFFX, 830-OFFY,NULL,0,0,icb_10R		,0,100,p0_4)	
MY_ICON2	(cov_da30		    ,P0_COV_DA30_D				,&l_mov_altim_10R      , 915-OFFX, 679-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiD	    ,P0_NDL_DA30VSI_D	        ,&l_cov_da30		    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiD	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiR	    ,P0_NDL_DA30VSI_R	        ,&l_ndl_da30_vsiD	    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiR	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnD	    ,P0_NDL_DA30TURN_D			,&l_ndl_da30_vsiR	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnD	,tbl_da30turn   ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnR	    ,P0_NDL_DA30TURN_R			,&l_ndl_da30_turnD	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnR	,tbl_da30turn   ,18,p0_4) 
MY_ICON2	(cov_da30_ball	    ,P0_COV_DA30BALL_D			,&l_ndl_da30_turnR	    , 913-OFFX, 706-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)     
MY_NEEDLE2	(ndl_da30_ballD	    ,P0_NDL_DA30BALL_D			,&l_cov_da30_ball	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballD	,0,	18,p0_4) 
MY_NEEDLE2	(ndl_da30_ballR	    ,P0_NDL_DA30BALL_R			,&l_ndl_da30_ballD	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballR	,0,	18,p0_4) 
MY_ICON2	(p0_4Ico			,P0_BACKGROUND4_D			,&l_ndl_da30_ballR	    ,		 0,		   0,		icb_Ico			,PANEL_LIGHT_MAX	, p0_4) 
MY_STATIC2	(p0_4bg,p0_4_list	,P0_BACKGROUND4				,&l_p0_4Ico			, p0_4);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p0_tablo01			,P0_TBL_01_D_00				,NULL					, 669-OFFX, 550-OFFY,icb_tablo01,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo02			,P0_TBL_02_D_00				,&l_p0_tablo01			, 713-OFFX, 504-OFFY,icb_tablo02,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo03			,P0_TBL_03_D_00				,&l_p0_tablo02			, 726-OFFX, 550-OFFY,icb_tablo03,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo04			,P0_TBL_04_D_00				,&l_p0_tablo03			, 770-OFFX, 504-OFFY,icb_tablo04,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo05			,P0_TBL_05_D_00				,&l_p0_tablo04			, 770-OFFX, 550-OFFY,icb_tablo05,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo06			,P0_TBL_06_D_00				,&l_p0_tablo05			, 816-OFFX, 504-OFFY,icb_tablo06,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo07			,P0_TBL_07_D_00				,&l_p0_tablo06			, 816-OFFX, 550-OFFY,icb_tablo07,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo08			,P0_TBL_08_D_00				,&l_p0_tablo07			, 861-OFFX, 504-OFFY,icb_tablo08,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo09			,P0_TBL_09_D_00				,&l_p0_tablo08			, 861-OFFX, 550-OFFY,icb_tablo09,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo10			,P0_TBL_10_D_00				,&l_p0_tablo09			, 906-OFFX, 504-OFFY,icb_tablo10,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo11			,P0_TBL_11_D_00				,&l_p0_tablo10			, 906-OFFX, 550-OFFY,icb_tablo11,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo12			,P0_TBL_12_D_00				,&l_p0_tablo11			, 952-OFFX, 504-OFFY,icb_tablo12,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo13			,P0_TBL_13_D_00				,&l_p0_tablo12			, 952-OFFX, 550-OFFY,icb_tablo13,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo14			,P0_TBL_14_D_00				,&l_p0_tablo13			, 997-OFFX, 504-OFFY,icb_tablo14,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo15			,P0_TBL_15_D_00				,&l_p0_tablo14			, 997-OFFX, 550-OFFY,icb_tablo15,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo16			,P0_TBL_16_D_00				,&l_p0_tablo15			,1043-OFFX, 550-OFFY,icb_tablo16,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo17			,P0_TBL_17_D_00				,&l_p0_tablo16			,1088-OFFX, 550-OFFY,icb_tablo17,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo18			,P0_TBL_18_D_00				,&l_p0_tablo17			, 997-OFFX, 582-OFFY,icb_tablo18,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo19			,P0_TBL_19_D_00				,&l_p0_tablo18			,1043-OFFX, 582-OFFY,icb_tablo19,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo20			,P0_TBL_20_D_00				,&l_p0_tablo19			,1088-OFFX, 582-OFFY,icb_tablo20,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo21			,P0_TBL_21_D_00				,&l_p0_tablo20			,1043-OFFX, 470-OFFY,icb_tablo21,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo31			,P0_TBL_31_D_00				,&l_p0_tablo21			, 696-OFFX,1013-OFFY,icb_tablo31,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo32			,P0_TBL_32_D_00				,&l_p0_tablo31			, 750-OFFX,1013-OFFY,icb_tablo32,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(p0_tablo33			,P0_TBL_33_D_00				,&l_p0_tablo32			, 863-OFFX,1013-OFFY,icb_tablo33,3*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(gforce				,P0_BTN_GFORCERESET_D_00	,&l_p0_tablo33			, 580-OFFX,1054-OFFY,icb_gforce		, 2*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(rv3m_vpr			,P0_KNB_RV3MDH_D_00			,&l_gforce				, 994-OFFX, 734-OFFY,icb_rv3mvpr    ,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(altim				,P0_KNB_ALTIM_D_00			,&l_rv3m_vpr			, 549-OFFX, 912-OFFY,icb_uvid       ,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(fuel_ind			,P0_AZS_FUELIND_D_00		,&l_altim				, 980-OFFX, 930-OFFY,icb_fuel_ind	, 3*PANEL_LIGHT_MAX,p0_4)
MY_ICON2	(agb_osn_aret		,P0_BTN_AGBARRETMAIN_D_00	,&l_fuel_ind			, 800-OFFX, 621-OFFY,		icb_agbmain		,2*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(agb_osn_pitch	    ,P0_KNB_AGBMAINPITCH_D_00	,&l_agb_osn_aret		, 645-OFFX, 750-OFFY,		icb_knb_agb_main,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(cov_agb_osn_flag	,P0_COV_AGBMAINFLAG_D		,&l_agb_osn_pitch	    , 662-OFFX, 611-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_osn_flagD	,P0_NDL_AGBMAINFLAG_D		,&l_cov_agb_osn_flag	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmD		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_flagM	,P0_NDL_AGBMAINFLAG_M		,&l_ndl_agb_osn_flagD	, 691-OFFX, 668-OFFY, 2, 9,	icb_flagmM		,0,6,p0_4)   
MY_NEEDLE2	(ndl_agb_osn_planeD	,P0_NDL_AGBMAINPLANE_D		,&l_ndl_agb_osn_flagM	, 744-OFFX, 706-OFFY,64, 7,	icb_planemD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_planeM	,P0_NDL_AGBMAINPLANE_M		,&l_ndl_agb_osn_planeD	, 744-OFFX, 706-OFFY,64, 7,	icb_planemM		,0,18,p0_4)  
MY_ICON2	(cov_agb_osn_ball	,P0_COV_AGBMAINBALL_D		,&l_ndl_agb_osn_planeM	, 736-OFFX, 766-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_osn_ballD	,P0_NDL_AGBMAINBALL_D		,&l_cov_agb_osn_ball	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_osn_ballM	,P0_NDL_AGBMAINBALL_M		,&l_ndl_agb_osn_ballD	, 743-OFFX, 637-OFFY, 7,10,	icb_ballmM		,0,18,p0_4)  
MY_ICON2	(cov_agb_osn_rail	,P0_COV_AGBMAINRAIL_D		,&l_ndl_agb_osn_ballM	, 737-OFFX, 643-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_STAT2	(alpha_agb1			,P0_ALPHA_AGB				,&l_cov_agb_osn_rail	, 679-OFFX, 640-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_MOVING2	(mov_agb_osn_pitchD	,P0_MOV_AGBMAINPITCH_D		,&l_alpha_agb1			, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmD		,-282,282,p0_4)
MY_MOVING2	(mov_agb_osn_pitchM	,P0_MOV_AGBMAINPITCH_M		,&l_mov_agb_osn_pitchD	, 673-OFFX, 634-OFFY,0,0,0,	icb_pitchmM		,-282,282,p0_4)
MY_ICON2	(agb_res_aret		,P0_BTN_AGBARRETAUXL_D_00	,&l_mov_agb_osn_pitchM	,1085-OFFX,1006-OFFY,		icb_agbauxl		,2*PANEL_LIGHT_MAX,p0_4)
MY_ICON2	(agb_res_pitch		,P0_KNB_AGBAUXLPITCH_D_00	,&l_agb_res_aret		, 932-OFFX,1134-OFFY,		icb_knb_agb_auxl,20*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(cov_agb_res_flag	,P0_COV_AGBAUXLFLAG_D		,&l_agb_res_pitch		, 954-OFFX, 994-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_res_flagD	,P0_NDL_AGBAUXLFLAG_D		,&l_cov_agb_res_flag	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaD		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_flagM	,P0_NDL_AGBAUXLFLAG_M		,&l_ndl_agb_res_flagD	, 974-OFFX,1049-OFFY, 2, 8,	icb_flagaM		,0,6,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeD	,P0_NDL_AGBAUXLPLANE_D		,&l_ndl_agb_res_flagM	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_planeM	,P0_NDL_AGBAUXLPLANE_M		,&l_ndl_agb_res_planeD	,1027-OFFX,1087-OFFY,63, 7,	icb_planeaM		,0,18,p0_4)  
MY_ICON2	(cov_agb_res_ball	,P0_COV_AGBAUXLBALL_D		,&l_ndl_agb_res_planeM	,1019-OFFX,1147-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_NEEDLE2	(ndl_agb_res_ballD	,P0_NDL_AGBAUXLBALL_D		,&l_cov_agb_res_ball	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaD		,0,18,p0_4)  
MY_NEEDLE2	(ndl_agb_res_ballM	,P0_NDL_AGBAUXLBALL_M		,&l_ndl_agb_res_ballD	,1025-OFFX,1017-OFFY, 5,10,	icb_ballaM		,0,18,p0_4)  
MY_ICON2	(cov_agb_res_rail	,P0_COV_AGBAUXLRAIL_D		,&l_ndl_agb_res_ballM	,1020-OFFX,1023-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	    
MY_STAT2	(alpha_agb2			,P0_ALPHA_AGB				,&l_cov_agb_res_rail	, 964-OFFX,1021-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_MOVING2	(mov_agb_res_pitchD	,P0_MOV_AGBAUXLPITCH_D		,&l_alpha_agb2			, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaD		,-282,282,p0_4)
MY_MOVING2	(mov_agb_res_pitchM	,P0_MOV_AGBAUXLPITCH_M		,&l_mov_agb_res_pitchD	, 957-OFFX,1014-OFFY,0,0,0,	icb_pitchaM		,-282,282,p0_4)
MY_ICON2	(cov_air_temp       ,P0_COV_TEMPAIR_D			,&l_mov_agb_res_pitchM	, 843-OFFX,1105-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)     
MY_NEEDLE2	(ndl_air_tempD		,P0_NDL_TEMPAIR_D			,&l_cov_air_temp       , 867-OFFX,1128-OFFY, 2, 4,	icb_airtempD	,tbl_airtemp,18,p0_4) 
MY_NEEDLE2	(ndl_air_tempM		,P0_NDL_TEMPAIR_M			,&l_ndl_air_tempD		, 867-OFFX,1128-OFFY, 2, 4,	icb_airtempM	,tbl_airtemp,18,p0_4) 
MY_ICON2	(cov_gforce		    ,P0_COV_GFORCE_D			,&l_ndl_air_tempM		, 545-OFFX,1018-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_gforceD		,P0_NDL_GFORCE_D			,&l_cov_gforce		    , 550-OFFX,1023-OFFY,21, 5,	icb_gfcurD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforceM		,P0_NDL_GFORCE_M			,&l_ndl_gforceD		, 550-OFFX,1023-OFFY,21, 5,	icb_gfcurM		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minD	,P0_NDL_GFORCEMIN_D			,&l_ndl_gforceM		, 550-OFFX,1023-OFFY,21, 5,	icb_gfminD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_minM	,P0_NDL_GFORCEMIN_M			,&l_ndl_gforce_minD	, 550-OFFX,1023-OFFY,21, 5,	icb_gfminM		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxD	,P0_NDL_GFORCEMAX_D			,&l_ndl_gforce_minM	, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxD		,tbl_gforce,10,p0_4) 
MY_NEEDLE2	(ndl_gforce_maxM	,P0_NDL_GFORCEMAX_M			,&l_ndl_gforce_maxD	, 550-OFFX,1023-OFFY,21, 5,	icb_gfmaxM		,tbl_gforce,10,p0_4) 
MY_ICON2	(cov_tormoz_main	,P0_COV_BRAKEMAIN_D			,&l_ndl_gforce_maxM	, 712-OFFX,1079-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righD	,P0_NDL_BRAKEMAINRIGHT_D	,&l_cov_tormoz_main	, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightD	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_righM	,P0_NDL_BRAKEMAINRIGHT_M	,&l_ndl_trm_main_righD	, 788-OFFX,1153-OFFY, 2, 4,	icb_hbrrightM	,tbl_hbrright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftD	,P0_NDL_BRAKEMAINLEFT_D		,&l_ndl_trm_main_righM	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftD	,tbl_hbrleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_main_leftM	,P0_NDL_BRAKEMAINLEFT_M		,&l_ndl_trm_main_leftD	, 735-OFFX,1102-OFFY, 2, 4,	icb_hbrleftM	,tbl_hbrleft    ,18,p0_4) 
MY_ICON2	(cov_tormoz_emer 	,P0_COV_BRAKEEMERG_D		,&l_ndl_trm_main_leftM	, 606-OFFX,1077-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4)
MY_NEEDLE2	(ndl_trm_emer_righD	,P0_NDL_BRAKEEMERGRIGHT_D	,&l_cov_tormoz_emer 	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightD	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_righM	,P0_NDL_BRAKEEMERGRIGHT_M	,&l_ndl_trm_emer_righD	, 683-OFFX,1153-OFFY, 2, 4,	icb_hborightM	,tbl_hboright   ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftD	,P0_NDL_BRAKEEMERGLEFT_D	,&l_ndl_trm_emer_righM	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftD	,tbl_hboleft    ,18,p0_4) 
MY_NEEDLE2	(ndl_trm_emer_leftM	,P0_NDL_BRAKEEMERGLEFT_M	,&l_ndl_trm_emer_leftD	, 630-OFFX,1102-OFFY, 2, 4,	icb_hboleftM	,tbl_hboleft    ,18,p0_4) 
MY_ICON2	(cov_hydrosys		,P0_COV_HYDROSYS_D			,&l_ndl_trm_emer_leftM	, 501-OFFX,1101-OFFY,		icb_Ico			,PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarD	,P0_NDL_HYDROSYSEMERG_D		,&l_cov_hydrosys		, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergD	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_avarM	,P0_NDL_HYDROSYSEMERG_M		,&l_ndl_hydrosys_avarD	, 578-OFFX,1176-OFFY, 2, 4,	icb_hydemergM	,tbl_hydavar    ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnD	,P0_NDL_HYDROSYSMAIN_D		,&l_ndl_hydrosys_avarM	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainD	,tbl_hydosn     ,18,p0_4) 
MY_NEEDLE2	(ndl_hydrosys_osnM	,P0_NDL_HYDROSYSMAIN_M		,&l_ndl_hydrosys_osnD	, 524-OFFX,1125-OFFY, 2, 4,	icb_hydmainM	,tbl_hydosn     ,18,p0_4) 
MY_ICON2	(iku_whi			,P0_KNB_IKUWHITE_D_00		,&l_ndl_hydrosys_osnM	, 948-OFFX, 893-OFFY,		icb_knob_whi    , 4*PANEL_LIGHT_MAX,p0_4)    
MY_ICON2	(iku_yel			,P0_KNB_IKUYELLOW_D_00		,&l_iku_whi			, 867-OFFX, 893-OFFY,		icb_knob_yel	, 4*PANEL_LIGHT_MAX,p0_4)    
MY_MOVING2	(mov_iku_whiD		,P0_MOV_IKUWHITE_D			,&l_iku_yel			, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiD	,0,3,p0_4)  
MY_MOVING2	(mov_iku_whiM		,P0_MOV_IKUWHITE_M			,&l_mov_iku_whiD		, 926-OFFX, 922-OFFY,NULL,0,0,icb_mov_whiM	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelD		,P0_MOV_IKUYELLOW_D			,&l_mov_iku_whiM		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelD	,0,3,p0_4)  
MY_MOVING2	(mov_iku_yelM		,P0_MOV_IKUYELLOW_M			,&l_mov_iku_yelD		, 891-OFFX, 922-OFFY,NULL,0,0,icb_mov_yelM	,0,3,p0_4)  
MY_ICON2	(cov_iku			,P0_COV_IKU_D				,&l_mov_iku_yelM		, 913-OFFX, 839-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)		    
MY_NEEDLE2	(ndl_iku_yelD		,P0_NDL_IKUYELLOW_D			,&l_cov_iku			, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelD		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_yelM		,P0_NDL_IKUYELLOW_M			,&l_ndl_iku_yelD		, 922-OFFX, 848-OFFY,48, 8,	icb_ikuyelM		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiD		,P0_NDL_IKUWHITE_D			,&l_ndl_iku_yelM		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiD		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_whiM		,P0_NDL_IKUWHITE_M			,&l_ndl_iku_whiD		, 922-OFFX, 848-OFFY,48,14,	icb_ikuwhiM		,0,2,p0_4)	    
MY_NEEDLE2	(ndl_iku_scaleD	    ,P0_NDL_IKUSCALE_D			,&l_ndl_iku_whiM		, 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleD	,0,2,p0_4)       
MY_NEEDLE2	(ndl_iku_scaleM	    ,P0_NDL_IKUSCALE_M			,&l_ndl_iku_scaleD	    , 921-OFFX, 846-OFFY,52,52,	icb_ikuscaleM	,0,2,p0_4)       
MY_ICON2	(kppms_scale		,P0_KNB_KPPMSSCALE_D_00		,&l_ndl_iku_scaleM	    , 791-OFFX, 946-OFFY,		icb_kppms       ,20*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgD		,P0_NDL_KPPMSHDG_D			,&l_kppms_scale		, 750-OFFX, 904-OFFY,2,8,	icb_hdgD		,0,10,p0_4)	
MY_NEEDLE2	(ndl_kppms_hdgM		,P0_NDL_KPPMSHDG_M			,&l_ndl_kppms_hdgD		, 750-OFFX, 904-OFFY,2,8,	icb_hdgM		,0,10,p0_4)	
MY_STAT2	(alpha_kppms		,P0_ALPHA_KPPMS				,&l_ndl_kppms_hdgM		, 680-OFFX, 834-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p0_4) 
MY_NEEDLE2	(ndl_kppms_scaleD	,P0_NDL_KPPMSSCALE_D		,&l_alpha_kppms		, 751-OFFX, 904-OFFY,69,69,	icb_scaleD		,0,18,p0_4)	
MY_NEEDLE2	(ndl_kppms_scaleM	,P0_NDL_KPPMSSCALE_M		,&l_ndl_kppms_scaleD	, 751-OFFX, 904-OFFY,69,69,	icb_scaleM		,0,18,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissD	,P0_SLD_KPPMSILSGLIDE_D		,&l_ndl_kppms_scaleM	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissD	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_glissM	,P0_SLD_KPPMSILSGLIDE_M		,&l_sld_kpp_ils_glissD	, 702-OFFX, 898-OFFY,NULL,0,icb_ils_glissM	,0.25,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursD	,P0_SLD_KPPMSILSCOURSE_D	,&l_sld_kpp_ils_glissM	, 748-OFFX, 858-OFFY,		icb_ils_kursD	,0.25,NULL,0,p0_4)	
MY_SLIDER2	(sld_kpp_ils_kursM	,P0_SLD_KPPMSILSCOURSE_M	,&l_sld_kpp_ils_kursD	, 748-OFFX, 858-OFFY,		icb_ils_kursM	,0.25,NULL,0,p0_4)	
MY_ICON2	(blenker_kppms_k	,P0_BLK_KPPMSCOURSE_D		,&l_sld_kpp_ils_kursM	, 728-OFFX, 884-OFFY,		icb_blenker_kurs   ,1*PANEL_LIGHT_MAX,p0_4)	
MY_ICON2	(blenker_kppms_gliss,P0_BLK_KPPMSGLIDESLOPE_D	,&l_blenker_kppms_k	, 760-OFFX, 905-OFFY,		icb_blenker_gliss  ,1*PANEL_LIGHT_MAX,p0_4) 
MY_ICON2	(cov_kus			,P0_COV_KUS_D				,&l_blenker_kppms_gliss, 562-OFFX, 697-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_kus_iasD       ,P0_NDL_KUSIAS_D			,&l_cov_kus			, 569-OFFX, 704-OFFY,41, 8,	icb_iasD		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_iasM       ,P0_NDL_KUSIAS_M			,&l_ndl_kus_iasD       , 569-OFFX, 704-OFFY,41, 8,	icb_iasM		,tbl_ias,10,p0_4)	
MY_NEEDLE2	(ndl_kus_tasD       ,P0_NDL_KUSTAS_D			,&l_ndl_kus_iasM       , 568-OFFX, 703-OFFY,10, 2,	icb_tasD		,tbl_tas,10,p0_4)    
MY_NEEDLE2	(ndl_kus_tasM       ,P0_NDL_KUSTAS_M			,&l_ndl_kus_tasD       , 568-OFFX, 703-OFFY,10, 2,	icb_tasM		,tbl_tas,10,p0_4)    
MY_ICON2	(cov_pptiz          ,P0_COV_PPTIZ_D				,&l_ndl_kus_tasM       ,1041-OFFX, 830-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_pptizD         ,P0_NDL_PPTIZ_D				,&l_cov_pptiz          ,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizD		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_pptizM         ,P0_NDL_PPTIZ_M				,&l_ndl_pptizD         ,1059-OFFX, 849-OFFY, 3, 5,	icb_pptizM		,tbl_pptiz,9,p0_4) 
MY_NEEDLE2	(ndl_rv3m_vprD	    ,P0_NDL_RV3MDH_D			,&l_ndl_pptizM         ,1062-OFFX, 695-OFFY, 4, 8,	icb_vprD		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_vprM	    ,P0_NDL_RV3MDH_M			,&l_ndl_rv3m_vprD	    ,1062-OFFX, 695-OFFY, 4, 8,	icb_vprM		,tbl_radalt,10,p0_4)	
MY_ICON2	(cov_rv3m		    ,P0_COV_RV3M_D				,&l_ndl_rv3m_vprM	    ,1034-OFFX, 627-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltD	,P0_NDL_RV3MRADALT_D		,&l_cov_rv3m		    ,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltD		,tbl_radalt,10,p0_4)	
MY_NEEDLE2	(ndl_rv3m_radaltM	,P0_NDL_RV3MRADALT_M		,&l_ndl_rv3m_radaltD	,1062-OFFX, 695-OFFY, 2, 6,	icb_radaltM		,tbl_radalt,10,p0_4)	
MY_ICON2	(lamp_rv3m_vpr	    ,P0_LMP_RV3MDH_D_00			,&l_ndl_rv3m_radaltM	,1094-OFFX, 726-OFFY,		icb_lamp		,2*PANEL_LIGHT_MAX,p0_4) 
MY_ICON2	(cov_altim          ,P0_COV_ALTIM_D				,&l_lamp_rv3m_vpr	    , 563-OFFX, 847-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)	
MY_NEEDLE2	(ndl_altimD         ,P0_NDL_ALTIM_D				,&l_cov_altim          , 569-OFFX, 853-OFFY,3,5,	icb_altimD		,0,10,p0_4)	
MY_NEEDLE2	(ndl_altimM         ,P0_NDL_ALTIM_M				,&l_ndl_altimD         , 569-OFFX, 853-OFFY,3,5,	icb_altimM		,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100D ,P0_MOV_ALTIMPRESS100_D		,&l_ndl_altimM         , 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100D,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_100M ,P0_MOV_ALTIMPRESS100_M		,&l_mov_altim_prs_100D , 548-OFFX, 880-OFFY,NULL,0,0,icb_press_100M,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10D  ,P0_MOV_ALTIMPRESS10_D		,&l_mov_altim_prs_100M , 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10D ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_10M  ,P0_MOV_ALTIMPRESS10_M		,&l_mov_altim_prs_10D  , 560-OFFX, 880-OFFY,NULL,0,0,icb_press_10M ,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1D   ,P0_MOV_ALTIMPRESS1_D	    ,&l_mov_altim_prs_10M  , 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1D	,0,10,p0_4)	
MY_MOVING2	(mov_altim_prs_1M   ,P0_MOV_ALTIMPRESS1_M	    ,&l_mov_altim_prs_1D   , 573-OFFX, 877-OFFY,NULL,0,0,icb_press_1M	,0,10,p0_4)	
MY_MOVING2	(mov_altim_10kD		,P0_MOV_ALTIM10K_D			,&l_mov_altim_prs_1M   , 535-OFFX, 834-OFFY,NULL,0,0,icb_10kD		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10kM		,P0_MOV_ALTIM10K_M			,&l_mov_altim_10kD		, 535-OFFX, 834-OFFY,NULL,0,0,icb_10kM		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kD      ,P0_MOV_ALTIM1K_D		    ,&l_mov_altim_10kM		, 553-OFFX, 834-OFFY,NULL,0,0,icb_1kD		,0,100,p0_4)	
MY_MOVING2	(mov_altim_1kM      ,P0_MOV_ALTIM1K_M		    ,&l_mov_altim_1kD      , 553-OFFX, 834-OFFY,NULL,0,0,icb_1kM		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100D     ,P0_MOV_ALTIM100_D			,&l_mov_altim_1kM      , 571-OFFX, 834-OFFY,NULL,0,0,icb_100D		,0,100,p0_4)	
MY_MOVING2	(mov_altim_100M     ,P0_MOV_ALTIM100_M			,&l_mov_altim_100D     , 571-OFFX, 834-OFFY,NULL,0,0,icb_100M		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10D      ,P0_MOV_ALTIM10_D		    ,&l_mov_altim_100M     , 589-OFFX, 830-OFFY,NULL,0,0,icb_10D		,0,100,p0_4)	
MY_MOVING2	(mov_altim_10M      ,P0_MOV_ALTIM10_M		    ,&l_mov_altim_10D      , 589-OFFX, 830-OFFY,NULL,0,0,icb_10M		,0,100,p0_4)	
MY_ICON2	(cov_da30		    ,P0_COV_DA30_D				,&l_mov_altim_10M      , 915-OFFX, 679-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiD	    ,P0_NDL_DA30VSI_D	        ,&l_cov_da30		    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiD	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_vsiM	    ,P0_NDL_DA30VSI_M	        ,&l_ndl_da30_vsiD	    , 922-OFFX, 688-OFFY,20, 6,	icb_da30vsiM	,tbl_da30vsi    ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnD	    ,P0_NDL_DA30TURN_D			,&l_ndl_da30_vsiM	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnD	,tbl_da30turn   ,18,p0_4) 
MY_NEEDLE2	(ndl_da30_turnM	    ,P0_NDL_DA30TURN_M			,&l_ndl_da30_turnD	    , 923-OFFX, 733-OFFY, 8, 7,	icb_da30turnM	,tbl_da30turn   ,18,p0_4) 
MY_ICON2	(cov_da30_ball	    ,P0_COV_DA30BALL_D			,&l_ndl_da30_turnM	    , 913-OFFX, 706-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX,p0_4)     
MY_NEEDLE2	(ndl_da30_ballD	    ,P0_NDL_DA30BALL_D			,&l_cov_da30_ball	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballD	,0,	18,p0_4) 
MY_NEEDLE2	(ndl_da30_ballM	    ,P0_NDL_DA30BALL_M			,&l_ndl_da30_ballD	    , 921-OFFX, 565-OFFY, 8,11,	icb_da30ballM	,0,	18,p0_4) 
MY_ICON2	(p0_4Ico			,P0_BACKGROUND4_D			,&l_ndl_da30_ballM	    ,		 0,		   0,		icb_Ico			,PANEL_LIGHT_MAX	, p0_4) 
MY_STATIC2	(p0_4bg,p0_4_list	,P0_BACKGROUND4				,&l_p0_4Ico			, p0_4);
#endif

#endif