/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

#define OFFX	488
#define OFFY	0

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_fire1      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY1,2))
static MAKE_ICB(icb_fire2      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY2,2))
static MAKE_ICB(icb_fire4      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG1    ,2))
static MAKE_ICB(icb_fire5      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG2    ,2))

static MAKE_ICB(icb_crs_p0_02,SHOW_POSM(POS_CRS0_02,1,0))

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_fire1				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY1	))
static MAKE_MSCB(mcb_fire2				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY2	))
static MAKE_MSCB(mcb_fire4				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG1		))
static MAKE_MSCB(mcb_fire5				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG2		))

static BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS0_01,0);
		POS_SET(POS_CRS0_02,0);
		POS_SET(POS_CRS0_03,0);
		POS_SET(POS_CRS0_04,0);
		POS_SET(POS_YOKE0_ICON,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p0_02(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS0_02,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P2);
		panel_window_close_ident(IDENT_P0);
		panel_window_open_ident(IDENT_P5);
	}
	return true;
}

static MAKE_TCB(tcb_01	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG_BAY1		))
static MAKE_TCB(tcb_02	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG_BAY2		))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG1			))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG2			))
static MAKE_TCB(tcb_05	,POS_TT(POS_CRS0_02						))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_3,HELP_NONE,0,0)

MOUSE_PBOX(0,0,P0_BACKGROUND3_D_SX,P0_BACKGROUND3_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_TBOX( "5", 1015-OFFX,   68-OFFY,MISC_CRS_P0_02_SX,MISC_CRS_P0_02_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p0_02)

MOUSE_TBOX( "1", 1092-OFFX,    0-OFFY, 38, 30,CURSOR_HAND,MOUSE_DLR,mcb_fire1)
MOUSE_TBOX( "2", 1130-OFFX,    0-OFFY, 38, 30,CURSOR_HAND,MOUSE_DLR,mcb_fire2)
MOUSE_TBOX( "3", 1082-OFFX,   30-OFFY, 34, 29,CURSOR_HAND,MOUSE_DLR,mcb_fire4)
MOUSE_TBOX( "4", 1125-OFFX,   30-OFFY, 34, 29,CURSOR_HAND,MOUSE_DLR,mcb_fire5)
MOUSE_END

extern PELEMENT_HEADER p0_3_list; 
GAUGE_HEADER_FS700_EX(P0_BACKGROUND3_D_SX, "p0_3", &p0_3_list, rect_p0_3, 0, 0, 0, 0, p0_3); 

MY_ICON2	(p0_crs_02			,MISC_CRS_P0_02		,NULL		, 1015-OFFX,   68-OFFY,icb_crs_p0_02,1, p0_3)	
MY_ICON2	(fire1				,P0_BTN_FIRE1_D_00	,&l_p0_crs_02,1092-OFFX,    0-OFFY,icb_fire1	,2*PANEL_LIGHT_MAX	, p0_3) 	
MY_ICON2	(fire2				,P0_BTN_FIRE2_D_00	,&l_fire1	, 1130-OFFX,	0-OFFY,icb_fire2	,2*PANEL_LIGHT_MAX	, p0_3) 	
MY_ICON2	(fire4				,P0_BTN_FIRE4_D_00	,&l_fire2	, 1082-OFFX,   30-OFFY,icb_fire4	,2*PANEL_LIGHT_MAX	, p0_3) 	
MY_ICON2	(fire5				,P0_BTN_FIRE5_D_00	,&l_fire4	, 1125-OFFX,   30-OFFY,icb_fire5	,2*PANEL_LIGHT_MAX	, p0_3) 	
MY_ICON2	(p0_3Ico			,P0_BACKGROUND3_D	,&l_fire5	,		  0,		 0,icb_Ico		,PANEL_LIGHT_MAX	, p0_3) 
MY_STATIC2  (p0_3bg,p0_3_list	,P0_BACKGROUND3		,&l_p0_3Ico	, p0_3);


#endif