/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

static NONLINEARITY tbl_VS[]={
	{{250,158},-30,0},
	{{220,232},-20,0},
	{{114,269},-10,0},
	{{ 47,230}, -5,0},
	{{ 19,158},  0,0},
	{{ 48, 83},  5,0},
	{{113, 41}, 10,0},
	{{222, 81}, 20,0},
	{{250,158}, 30,0}
};

static NONLINEARITY tbl_Turn[]={
	{{ 56,115},-1,0},
	{{135, 96}, 0,0},
	{{211,115}, 1,0}
};

static double FSAPI icbndlvsr_p0_12		(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_VS); };
static double FSAPI icbndlvsp_p0_12		(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_VS); };
static double FSAPI icbndlvsm_p0_12		(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_VS); };
static double FSAPI icbndlvsd_p0_12		(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_VS); };
static double FSAPI icbndlturnr_p0_12	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_DA30_TURN); };
static double FSAPI icbndlturnp_p0_12	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_DA30_TURN); };
static double FSAPI icbndlturnm_p0_12	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_DA30_TURN); };
static double FSAPI icbndlturnd_p0_12	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_DA30_TURN); };
static double FSAPI icbndlballr_p0_12	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_DA30_BALL)*0.0826+90; };
static double FSAPI icbndlballp_p0_12	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_DA30_BALL)*0.0826+90; };
static double FSAPI icbndlballm_p0_12	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_DA30_BALL)*0.0826+90; };
static double FSAPI icbndlballd_p0_12	(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_DA30_BALL)*0.0826+90; };

static double FSAPI icb_p0_12(PELEMENT_ICON pelement) 
{ 
	CHK_BRT();
	return POS_GET(POS_PANEL_STATE); 
} 

BOOL FSAPI mcb_da30_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(p0_12_tcb_01,return NDL_TTGET(NDL_DA30_TURN);) 

static MOUSE_TOOLTIP_ARGS(p0_12_ttargs) 
	MAKE_TTA(p0_12_tcb_01) 
MOUSE_TOOLTIP_ARGS_END 

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_12,HELP_NONE,0,0) 
	MOUSE_TBOX2("1", 30, 30,P18_BACKGROUND_D_SX,P18_BACKGROUND_D_SY,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_da30_big,p0_12)    
MOUSE_END 

extern PELEMENT_HEADER p0_12_list; 
GAUGE_HEADER_FS700_EX(P18_BACKGROUND_D_SX, "p0_12", &p0_12_list, rect_p0_12, 0, 0, 0, 0, p0_12); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_12Spot1         ,P16_COV_ASPDALTSPSPOT1_D,NULL             		, 32,  8,icb_Ico,PANEL_LIGHT_MAX,p0_12)
MY_ICON2	(p0_12Spot2         ,P16_COV_ASPDALTSPSPOT2_D,&l_p0_12Spot1    		,200,  8,icb_Ico,PANEL_LIGHT_MAX,p0_12)
MY_ICON2	(p0_12IcoCov    	,P18_COV_DA30A_D		,&l_p0_12Spot2     		,121,143,icb_p0_12,PANEL_LIGHT_MAX, p0_12); 
MY_NEEDLE2	(Np0_12dlVSR		,P18_NDL_DA30VS_R		,&l_p0_12IcoCov    		,135,158, 37,  9,icbndlvsr_p0_12 ,tbl_VS,0,p0_12); 
MY_NEEDLE2	(Np0_12dlVSP		,P18_NDL_DA30VS_P		,&l_Np0_12dlVSR			,135,158, 37,  9,icbndlvsp_p0_12 ,tbl_VS,0,p0_12); 
MY_NEEDLE2	(Np0_12dlVSM		,P18_NDL_DA30VS_M		,&l_Np0_12dlVSP			,135,158, 37,  9,icbndlvsm_p0_12 ,tbl_VS,0,p0_12); 
MY_NEEDLE2	(Np0_12dlVSD		,P18_NDL_DA30VS_D		,&l_Np0_12dlVSM			,135,158, 37,  9,icbndlvsd_p0_12 ,tbl_VS,0,p0_12); 
MY_NEEDLE2	(Np0_12dlTurnR		,P18_NDL_DA30TURN_R		,&l_Np0_12dlVSD			,135,224, 30, 13,icbndlturnr_p0_12 ,tbl_Turn,0,p0_12); 
MY_NEEDLE2	(Np0_12dlTurnP		,P18_NDL_DA30TURN_P		,&l_Np0_12dlTurnR		,135,224, 30, 13,icbndlturnp_p0_12 ,tbl_Turn,0,p0_12); 
MY_NEEDLE2	(Np0_12dlTurnM		,P18_NDL_DA30TURN_M		,&l_Np0_12dlTurnP		,135,224, 30, 13,icbndlturnm_p0_12 ,tbl_Turn,0,p0_12); 
MY_NEEDLE2	(Np0_12dlTurnD		,P18_NDL_DA30TURN_D		,&l_Np0_12dlTurnM		,135,224, 30, 13,icbndlturnd_p0_12 ,tbl_Turn,0,p0_12); 
MY_ICON2	(p0_12IcoCovBall	,P18_COV_DA30B_D		,&l_Np0_12dlTurnD		,121,192,icb_p0_12,PANEL_LIGHT_MAX, p0_12); 
MY_NEEDLE2	(Np0_12dlBallR		,P18_NDL_DA30BALL_R		,&l_p0_12IcoCovBall		,135,  9,  0, 13,icbndlballr_p0_12 ,0,0,p0_12); 
MY_NEEDLE2	(Np0_12dlBallP		,P18_NDL_DA30BALL_P		,&l_Np0_12dlBallR		,135,  9,  0, 13,icbndlballp_p0_12 ,0,0,p0_12); 
MY_NEEDLE2	(Np0_12dlBallM		,P18_NDL_DA30BALL_M		,&l_Np0_12dlBallP		,135,  9,  0, 13,icbndlballm_p0_12 ,0,0,p0_12); 
MY_NEEDLE2	(Np0_12dlBallD		,P18_NDL_DA30BALL_D		,&l_Np0_12dlBallM		,135,  9,  0, 13,icbndlballd_p0_12 ,0,0,p0_12); 
MY_ICON2	(p0_12Ico			,P18_BACKGROUND_D		,&l_Np0_12dlBallD		,  0,  0,icb_p0_12,PANEL_LIGHT_MAX, p0_12); 
MY_STATIC2(p0_12bg,p0_12_list	,P18_BACKGROUND_D		,&l_p0_12Ico			, p0_12);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p0_12Spot1         ,P16_COV_ASPDALTSPSPOT1_D,NULL             		, 32,  8,icb_Ico,PANEL_LIGHT_MAX,p0_12)
MY_ICON2	(p0_12Spot2         ,P16_COV_ASPDALTSPSPOT2_D,&l_p0_12Spot1    		,200,  8,icb_Ico,PANEL_LIGHT_MAX,p0_12)
MY_ICON2	(p0_12IcoCov    	,P18_COV_DA30A_D		,&l_p0_12Spot2     		,121,143,icb_p0_12,PANEL_LIGHT_MAX, p0_12); 
MY_NEEDLE2	(Np0_12dlVSP		,P18_NDL_DA30VS_P		,&l_p0_12IcoCov    		,135,158, 37,  9,icbndlvsp_p0_12 ,tbl_VS,0,p0_12); 
MY_NEEDLE2	(Np0_12dlVSD		,P18_NDL_DA30VS_D		,&l_Np0_12dlVSP			,135,158, 37,  9,icbndlvsd_p0_12 ,tbl_VS,0,p0_12); 
MY_NEEDLE2	(Np0_12dlTurnP		,P18_NDL_DA30TURN_P		,&l_Np0_12dlVSD			,135,224, 30, 13,icbndlturnp_p0_12 ,tbl_Turn,0,p0_12); 
MY_NEEDLE2	(Np0_12dlTurnD		,P18_NDL_DA30TURN_D		,&l_Np0_12dlTurnP		,135,224, 30, 13,icbndlturnd_p0_12 ,tbl_Turn,0,p0_12); 
MY_ICON2	(p0_12IcoCovBall	,P18_COV_DA30B_D		,&l_Np0_12dlTurnD		,121,192,icb_p0_12,PANEL_LIGHT_MAX, p0_12); 
MY_NEEDLE2	(Np0_12dlBallP		,P18_NDL_DA30BALL_P		,&l_p0_12IcoCovBall		,135,  9,  0, 13,icbndlballp_p0_12 ,0,0,p0_12); 
MY_NEEDLE2	(Np0_12dlBallD		,P18_NDL_DA30BALL_D		,&l_Np0_12dlBallP		,135,  9,  0, 13,icbndlballd_p0_12 ,0,0,p0_12); 
MY_ICON2	(p0_12Ico			,P18_BACKGROUND_D		,&l_Np0_12dlBallD		,  0,  0,icb_p0_12,PANEL_LIGHT_MAX, p0_12); 
MY_STATIC2(p0_12bg,p0_12_list	,P18_BACKGROUND_D		,&l_p0_12Ico			, p0_12);
#endif

#endif
