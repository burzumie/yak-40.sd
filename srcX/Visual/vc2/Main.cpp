/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"

SIMPLE_GAUGE_VC				(Clmn1				,VC2_CLMN1_D				);
SIMPLE_GAUGE_VC				(Clmn2				,VC2_CLMN2_D				);
SIMPLE_GAUGE_VC				(Clmn3				,VC2_CLMN3_D				);
SIMPLE_GAUGE_VC				(Clmn4				,VC2_CLMN4_D				);
SIMPLE_GAUGE_VC				(Clmn_nbld1_front	,VC2_CLMN_NBLD1_FRONT_D		);
SIMPLE_GAUGE_VC				(Clmn_nbld1_side	,VC2_CLMN_NBLD1_SIDE_D		);
SIMPLE_GAUGE_VC				(Clmn_nbld1_top		,VC2_CLMN_NBLD1_TOP_D		);
SIMPLE_GAUGE_VC				(Clmn_rubber		,VC2_CLMN_RUBBER_D			);
SIMPLE_GAUGE_VC				(Clmn_shaft			,VC2_CLMN_SHAFT_D			);
SIMPLE_GAUGE_VC				(Clmn_shaft_base	,VC2_CLMN_SHAFT_BASE_D		);
SIMPLE_GAUGE_VC				(Clmna_hndl1a		,VC2_CLMNA_HNDL1A_D			);
SIMPLE_GAUGE_VC				(Clmna_hndl1b		,VC2_CLMNA_HNDL1B_D			);
SIMPLE_GAUGE_VC				(Clmna_hndl2a		,VC2_CLMNA_HNDL2A_D			);
SIMPLE_GAUGE_VC				(Clmna_hndl2b		,VC2_CLMNA_HNDL2B_D			);
SIMPLE_GAUGE_VC				(Clmna_logo			,VC2_CLMNA_LOGO_D			);
SIMPLE_GAUGE_VC				(Clmna_nbld2_front	,VC2_CLMNA_NBLD2_FRONT_D	);
SIMPLE_GAUGE_VC				(Clmna_nbld2_side	,VC2_CLMNA_NBLD2_SIDE_D		);
SIMPLE_GAUGE_VC				(Clmna_nbld2_top	,VC2_CLMNA_NBLD2_TOP_D		);
SIMPLE_GAUGE_VC_PRESS_BTN	(Clmna_btn1_front	,BTN_APOFF2		,VC2_CLMNA_BTN1_FRONT_D	);
SIMPLE_GAUGE_VC_PRESS_BTN	(Clmna_btn1_side	,BTN_APOFF3		,VC2_CLMNA_BTN1_SIDE_D	);
SIMPLE_GAUGE_VC_PRESS_BTN	(Clmna_btn2_front	,BTN_COM2		,VC2_CLMNA_BTN2_FRONT_D	);
SIMPLE_GAUGE_VC_PRESS_BTN	(Clmna_btn2_side	,BTN_COM3		,VC2_CLMNA_BTN2_SIDE_D	);
SIMPLE_GAUGE_VC_PRESS_BTN_2_POS(Clmn_trim1		,BTN_TRIMEMERG2	,VC2_CLMN_TRIM1_D		);
SIMPLE_GAUGE_VC_PRESS_BTN_2_POS(Clmna_trim2		,BTN_TRIMMAIN2	,VC2_CLMNA_TRIM2_D		);
SIMPLE_GAUGE_VC(Sts01		,VC2_STS01_D		);
SIMPLE_GAUGE_VC(Sts02		,VC2_STS02_D		);
SIMPLE_GAUGE_VC(Sts03		,VC2_STS03_D		);
SIMPLE_GAUGE_VC(Sts04		,VC2_STS04_D		);
SIMPLE_GAUGE_VC(Sts05		,VC2_STS05_D		);
SIMPLE_GAUGE_VC(Sts06		,VC2_STS06_D		);
SIMPLE_GAUGE_VC(Sts07		,VC2_STS07_D		);
SIMPLE_GAUGE_VC(Sts08		,VC2_STS08_D		);
SIMPLE_GAUGE_VC(Sts09		,VC2_STS09_D		);
SIMPLE_GAUGE_VC(Sts10		,VC2_STS10_D		);
SIMPLE_GAUGE_VC(Sts11		,VC2_STS11_D		);
SIMPLE_GAUGE_VC(Sts12		,VC2_STS12_D		);
SIMPLE_GAUGE_VC(Sts13		,VC2_STS13_D		);
SIMPLE_GAUGE_VC(Sts14		,VC2_STS14_D		);
SIMPLE_GAUGE_VC(Sts15		,VC2_STS15_D		);
SIMPLE_GAUGE_VC(Sts16		,VC2_STS16_D		);
SIMPLE_GAUGE_VC(StsHandleSide,VC2_STSHANDLESIDE_D);
SIMPLE_GAUGE_VC(StsHandleTop	,VC2_STSHANDLETOP_D	);
SIMPLE_GAUGE_VC(StsRailSide	,VC2_STSRAILSIDE_D	);
SIMPLE_GAUGE_VC(StsRailTop	,VC2_STSRAILTOP_D	);
SIMPLE_GAUGE_VC(StsSupport	,VC2_STSSUPPORT_D	);
SIMPLE_GAUGE_VC(Rad_btn_side	,VC2_RAD_BTN_SIDE_D			);
SIMPLE_GAUGE_VC(Rad_btn1		,VC2_RAD_BTN1_D				);
SIMPLE_GAUGE_VC(Rad_btn2		,VC2_RAD_BTN2_D				);
SIMPLE_GAUGE_VC(Rad_btn3		,VC2_RAD_BTN3_D				);
SIMPLE_GAUGE_VC(Rad_btn4		,VC2_RAD_BTN4_D				);
SIMPLE_GAUGE_VC(Rad_btnr_side,VC2_RAD_BTNR_SIDE_D		);
SIMPLE_GAUGE_VC(Rad_btnr1	,VC2_RAD_BTNR1_D			);
SIMPLE_GAUGE_VC(Rad_btnr2	,VC2_RAD_BTNR2_D			);
SIMPLE_GAUGE_VC(Rad_btnr3	,VC2_RAD_BTNR3_D			);
SIMPLE_GAUGE_VC(Rad_btnr4	,VC2_RAD_BTNR4_D			);
SIMPLE_GAUGE_VC(Rad_det1		,VC2_RAD_DET1_D				);
SIMPLE_GAUGE_VC(Rad_det2		,VC2_RAD_DET2_D				);
SIMPLE_GAUGE_VC(Rad_frn		,VC2_RAD_FRN_D				);
SIMPLE_GAUGE_VC(Rad_knb1		,VC2_RAD_KNB1_D				);
SIMPLE_GAUGE_VC(Rad_knb2		,VC2_RAD_KNB2_D				);
SIMPLE_GAUGE_VC(Rad_knb3		,VC2_RAD_KNB3_D				);
SIMPLE_GAUGE_VC(Rad_knob_side,VC2_RAD_KNOB_SIDE_D		);
SIMPLE_GAUGE_VC(Rad_sde		,VC2_RAD_SDE_D				);
SIMPLE_GAUGE_VC(Rad_shd1		,VC2_RAD_SHD1_D				);
SIMPLE_GAUGE_VC(Rad_shd2		,VC2_RAD_SHD2_D				);
SIMPLE_GAUGE_VC(Rad_shd3		,VC2_RAD_SHD3_D				);
SIMPLE_GAUGE_VC(Rad_shd4		,VC2_RAD_SHD4_D				);
SIMPLE_GAUGE_VC(Rad_shd5		,VC2_RAD_SHD5_D				);
SIMPLE_GAUGE_VC(Rad_shd6		,VC2_RAD_SHD6_D				);
SIMPLE_GAUGE_VC(Rad_shd7		,VC2_RAD_SHD7_D				);
SIMPLE_GAUGE_VC(Rad_shd8		,VC2_RAD_SHD8_D				);
SIMPLE_GAUGE_VC(Rad_shd9		,VC2_RAD_SHD9_D				);
SIMPLE_GAUGE_VC(Rad_shd10	,VC2_RAD_SHD10_D			);
SIMPLE_GAUGE_VC(Rad_shd11	,VC2_RAD_SHD11_D			);
SIMPLE_GAUGE_VC(Rad_shd12	,VC2_RAD_SHD12_D			);
SIMPLE_GAUGE_VC(Rad_shd13	,VC2_RAD_SHD13_D			);
SIMPLE_GAUGE_VC(Rad_top		,VC2_RAD_TOP_D				);
SIMPLE_GAUGE_VC(Spg_sde		,VC2_SPG_SDE_D				);
SIMPLE_GAUGE_VC(Spg_top		,VC2_SPG_TOP_D				);
SIMPLE_GAUGE_VC(PTSSpot01	,VC2_PTSSPOT01_D	);
SIMPLE_GAUGE_VC(PTSSpot02	,VC2_PTSSPOT02_D	);
SIMPLE_GAUGE_VC(PTSSpot03	,VC2_PTSSPOT03_D	);
SIMPLE_GAUGE_VC(PTSSpot04	,VC2_PTSSPOT04_D	);
SIMPLE_GAUGE_VC(PTSSpot05	,VC2_PTSSPOT05_D	);
SIMPLE_GAUGE_VC(SpotLightCap	,VC2_SPOTLIGHTCAP_D	);
SIMPLE_GAUGE_VC(SpotLightTop	,VC2_SPOTLIGHTTOP_D	);

#ifdef _DEBUG
DLLMAIND("VC2.log")
#else
DLLMAIN()
#endif

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&Rad_btn_side			),
		(&Rad_btn1				),
		(&Rad_btn2				),
		(&Rad_btn3				),
		(&Rad_btn4				),
		(&Rad_btnr_side			),
		(&Rad_btnr1				),
		(&Rad_btnr2				),
		(&Rad_btnr3				),
		(&Rad_btnr4				),
		(&Rad_det1				),
		(&Rad_det2				),
		(&Rad_frn				),
		(&Rad_knb1				),
		(&Rad_knb2				),
		(&Rad_knb3				),
		(&Rad_knob_side			),
		(&Rad_sde				),
		(&Rad_shd1				),
		(&Rad_shd2				),
		(&Rad_shd3				),
		(&Rad_shd4				),
		(&Rad_shd5				),
		(&Rad_shd6				),
		(&Rad_shd7				),
		(&Rad_shd8				),
		(&Rad_shd9				),
		(&Rad_shd10				),
		(&Rad_shd11				),
		(&Rad_shd12				),
		(&Rad_shd13				),
		(&Rad_top				),
		(&Spg_sde				),
		(&Spg_top				),
		(&Clmn1					),
		(&Clmn2					),
		(&Clmn3					),
		(&Clmn4					),
		(&Clmn_nbld1_front		),        
		(&Clmn_nbld1_side		),        
		(&Clmn_nbld1_top		),        
		(&Clmn_rubber			),        
		(&Clmn_shaft			),        
		(&Clmn_shaft_base		),        
		(&Clmn_trim1			),        
		(&Clmna_btn1_front		),        
		(&Clmna_btn1_side		),        
		(&Clmna_btn2_front		),        
		(&Clmna_btn2_side		),        
		(&Clmna_hndl1a			),        
		(&Clmna_hndl1b			),        
		(&Clmna_hndl2a			),        
		(&Clmna_hndl2b			),        
		(&Clmna_logo			),        
		(&Clmna_nbld2_front		),            
		(&Clmna_nbld2_side		),        
		(&Clmna_nbld2_top		),        
		(&Clmna_trim2			),        
		(&Sts01					),
		(&Sts02					),
		(&Sts03					),
		(&Sts04					),
		(&Sts05					),        
		(&Sts06					),        
		(&Sts07					),        
		(&Sts08					),        
		(&Sts09					),        
		(&Sts10					),        
		(&Sts11					),        
		(&Sts12					),        
		(&Sts13					),        
		(&Sts14					),        
		(&Sts15					),        
		(&Sts16					),        
		(&StsHandleSide			),        
		(&StsHandleTop			),        
		(&StsRailSide			),        
		(&StsRailTop			),        
		(&StsSupport			),            
		(&PTSSpot01				),        
		(&PTSSpot02				),        
		(&PTSSpot03				),        
		(&PTSSpot04				),        
		(&PTSSpot05				),        
		(&SpotLightCap			),        
		(&SpotLightTop			),            
		0					
	}											
};												
							
