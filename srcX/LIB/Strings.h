/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Lib/Strings.h $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include "../Common/CommonSys.h"

HRESULT __cdecl ConvertAnsiStringToWideCch(WCHAR *wstrDestination, const char *strSource, int cchDestChar);
HRESULT __cdecl ConvertWideStringToAnsiCch(char *strDestination, const WCHAR *wstrSource, int cchDestChar);

/* ���������� ��� ������ rStr1 � rStr2, ���������� true, ���� ��� �����
!������ eng! */
bool CompareNoCase(const std::string &rStr1, const std::string &rStr2);
bool CompareNoCase(const std::wstring &rStr1, const std::wstring &rStr2);

/* ������� ���������� ������� (chWhite) � ����� ������, ������, ����� */
void TrimBoth(std::string &rString);
void TrimRight(std::string &rString);
void TrimLeft(std::string &rString);
void TrimBoth(std::wstring &rString);
void TrimRight(std::wstring &rString);
void TrimLeft(std::wstring &rString);

void __cdecl ExtractPath(char *in);

class CTokenizerW
{
public:
	CTokenizerW(const std::wstring& cs, const std::wstring& csDelim);
	void SetDelimiters(const std::wstring& csDelim);

	bool Next(std::wstring& cs);
	std::wstring	Tail() const;

private:
	std::wstring m_cs;
	std::bitset<256> m_delim;
	size_t m_nCurPos;
};

class CTokenizerA
{
public:
	CTokenizerA(const std::string& cs, const std::string& csDelim);
	void SetDelimiters(const std::string& csDelim);

	bool Next(std::string& cs);
	std::string	Tail() const;

private:
	std::string m_cs;
	std::bitset<256> m_delim;
	size_t m_nCurPos;
};

//////////////////////////////////////////////////////////////////////////
// StrMap
//

class CStrMap{
public:
	CStrMap( int extrabytes=sizeof(double));
	void CreateFromChain( int extrabytes, char *strchain, void *data );
	~CStrMap();
	void AddString(const char *str, void *data);
	void ShrinkMem();
	void Clear();
	void SetCapacity(int NewCapacity );
	int IndexOf(char *str, void **data );
	int Replace( char *str,void *data );
	char* GetString(int index, int *len, void **data );
	void FillFromChain(char *strchain, void *data );
	int IndexOf(char *str, int len, void **data );
	void AddStr(const char *str, int len, void *data );
	int Replace( char *str, int len, void *data );
private:
	void Trim(int NewCount );
	void TrimClear(int NewCount );
	int   FCount, FCapacity;
	int   FExtraLen, FRecordLen;
	char  FList[16383];
};

//////////////////////////////////////////////////////////////////////////
// Symtable
//

class CSymTable{
public:
	typedef struct {
		char Sym[4];
		char Len;
		char Index;
		char More;
	} SymbolRec;
	virtual ~CSymTable();

	void PrepareSymbols( char *symbols );
	int FindSymbol( char *str, int *nchars );
protected:
	CSymTable(){};
private:
	SymbolRec* table[256];
};

class CMathSymTable: public CSymTable{
public:
	CMathSymTable();
	virtual ~CMathSymTable(){};
};

//////////////////////////////////////////////////////////////////////////
// Lexer
//

typedef unsigned char uchar;

typedef enum {
	CH_LETTER = 0x01, CH_DIGIT = 0x02, CH_SEPARAT = 0x04,
	CH_SYMBOL = 0x08, CH_QUOTE = 0x10,
	CH_UNKNOWN= 0x7E, CH_FINAL = 0x7F
} hqCharType;

typedef enum {
	TOK_ERROR, TOK_NONE, TOK_FINAL, TOK_INT, TOK_FLOAT, TOK_SYMBOL,
	TOK_NAME, TOK_STRING
} hqTokenType;

class CLexer{
public:
	// input params
	int		cssn;	// Comment Start Symbol Number. -1 if none
	char    *ComEnd;	// End of comment
	CSymTable *SymTable;
	hqCharType *CharTypeTable;

	// output params
	char       *Name;
	int		NameLen;
	double	ExtValue;
	int		IntValue;
	hqTokenType PrevTokenType;
	hqCharType	CharType;
	int		NoIntegers;
	int SetParseString(const char *str );
	hqTokenType GetNextToken();
	~CLexer(){
		if(str_)
			free(str_);
	};
	char* GetCurrentPos();
	CLexer():str_(NULL){
	};
private:
	char *SS, *str_;
};

/* Misc */

void InitCharTypeTable( hqCharType *CharTypeTable, int CharTypes );

extern char const Win1251UpcaseTbl[];

inline void GetStringFromResource(std::string file,UINT id,std::string &ret,std::string def)
{
	HANDLE hExe;
	CHAR sz[160];

	hExe = LoadLibraryA(file.c_str()); 

	if(hExe == NULL) { 
		ret=def;
		return;
	} 
	if(!LoadStringA((HINSTANCE)hExe,id,sz,sizeof(sz))) {
		ret=def;
		return;
	}

	if(!FreeLibrary((HINSTANCE)hExe)) { 
		ret=def;
		return;
	} 

	ret.assign(sz);
}

inline int CountOfDelimitedWord(char *str,char c)
{
	int t=0;
	for(int i=0;i<(int)strlen(str);i++) {
		char b=str[i];
		if(b==c)t++;
	}
	return t+1;
}

inline char **DelimitText(char *str,int *numw)
{
	int numwords=CountOfDelimitedWord(str,',');
	char **buf=new char*[numwords];
	for(int i=0;i<numwords;i++)buf[i]=new char[256];
	int t=0,k=0;
	for(int j=0;j<(int)strlen(str);j++) {
		char c=str[j];
		if(c==','||c=='\0') {
			buf[k][t]='\0';
			t=0;
			k++;
		} else buf[k][t++]=c;
	}
	buf[k][t]='\0';
	*numw=numwords;
	return buf;
}
