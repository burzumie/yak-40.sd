/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Lib/IniFile.cpp $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "IniFile.h"

CIniFile::CIniFile(const char * fname)
{
	strcpy(filename,fname);
	ConvertAnsiStringToWideCch(filenamew,filename,MAX_PATH);
}

CIniFile::~CIniFile()
{
}

//////////////////////////////////////////////////////////////////////////

char * CIniFile::ReadString(const char * Section,const char * Key,const char * Default)
{
	GetPrivateProfileString(Section,Key,Default,_buf,2048,filename);
	return _buf;
}

LPWSTR CIniFile::ReadStringW(LPCWSTR Section,LPCWSTR Key,LPCWSTR Default)
{
	GetPrivateProfileStringW(Section,Key,Default,_ret,2048,filenamew);
	return _ret;
}

//////////////////////////////////////////////////////////////////////////

void CIniFile::WriteString(const char * Section,const char * Key,const char * Value)
{
	SECUREBEGIN_E
	WritePrivateProfileString(Section,Key,Value,filename);
	SECUREEND_E
}

void CIniFile::WriteStringW(LPCWSTR Section,LPCWSTR Key,LPCWSTR Value)
{
	SECUREBEGIN_B
	WritePrivateProfileStringW(Section,Key,Value,filenamew);
	SECUREEND_B
}

//////////////////////////////////////////////////////////////////////////

int CIniFile::ReadInt(const char * Section,const char * Key,int Default)
{
	return GetPrivateProfileInt(Section,Key,Default,filename);
}

int CIniFile::ReadIntW(LPCWSTR Section,LPCWSTR Key,int Default)
{
	return GetPrivateProfileIntW(Section,Key,Default,filenamew);
}

//////////////////////////////////////////////////////////////////////////

void CIniFile::WriteInt(const char * Section,const char * Key,int Value)
{
	SECUREBEGIN_D
	sprintf(_buf,"%d",Value);
	WriteString(Section,Key,_buf);
	SECUREEND_D
}

void CIniFile::WriteIntW(LPCWSTR Section,LPCWSTR Key,int Value)
{
	SECUREBEGIN_J
	sprintf(_buf,"%d",Value);
	ConvertAnsiStringToWideCch(_ret,_buf,4096);
	WriteStringW(Section,Key,_ret);
	SECUREEND_J
}

//////////////////////////////////////////////////////////////////////////

double CIniFile::ReadDouble(const char * Section,const char * Key,double Default)
{
    if(!_stricoll(ReadString((const char *)Section,Key,"Not found"),"Not found"))
		return Default;
    else 
		return atof(ReadString((const char *)Section,Key,NULL));
}

double CIniFile::ReadDoubleW(LPCWSTR Section,LPCWSTR Key,double Default)
{
	if(!_wcsicoll(ReadStringW(Section,Key,L"Not found"),L"Not found"))
		return Default;
	else 
		return wcstod(ReadStringW(Section,Key,NULL),NULL);
		//return wtof(ReadStringW(Section,Key,NULL));
}

//////////////////////////////////////////////////////////////////////////

void CIniFile::WriteDouble(const char * Section,const char * Key,double Value)
{
	sprintf(_buf,"%f",Value);
	WriteString(Section,Key,_buf);
}

void CIniFile::WriteDoubleW(LPCWSTR Section,LPCWSTR Key,double Value)
{
	sprintf(_buf,"%f",Value);
	ConvertAnsiStringToWideCch(_ret,_buf,4096);
	WriteStringW(Section,Key,_ret);
}

//////////////////////////////////////////////////////////////////////////

bool CIniFile::ReadBool(const char * Section,const char * Key,bool Default)
{
	if(!_stricoll(ReadString(Section,Key,NULL),"true"))return true;
	else if(!_stricoll(ReadString(Section,Key,NULL),"false"))return false;
	else return Default;
}

bool CIniFile::ReadBoolW(LPCWSTR Section,LPCWSTR Key,bool Default)
{
	if(!_wcsicoll(ReadStringW(Section,Key,NULL),L"true"))return true;
	else if(!_wcsicoll(ReadStringW(Section,Key,NULL),L"false"))return false;
	else return Default;
}

//////////////////////////////////////////////////////////////////////////

void CIniFile::WriteBool(const char * Section,const char * Key,bool Value)
{
	if(Value==true)WriteString(Section,Key,"True");
	else WriteString(Section,Key,"False");
}

void CIniFile::WriteBoolW(LPCWSTR Section,LPCWSTR Key,bool Value)
{
	if(Value==true)WriteStringW(Section,Key,L"True");
	else WriteStringW(Section,Key,L"False");
}
