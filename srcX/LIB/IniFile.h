/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Lib/IniFile.h $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Common/CommonSys.h"
#include "Strings.h"

class CIniFile
{
private:
	char filename[MAX_PATH];
	WCHAR filenamew[MAX_PATH];
	TCHAR _buf[BUFSIZ];
	WCHAR _ret[BUFSIZ];

public:
	CIniFile(const char *fname);
	~CIniFile();

	char *	ReadString(const char *Section,const char *Key,const char *Default);
	void	WriteString(const char *Section,const char *Key,const char *Value);
	int		ReadInt(const char *Section,const char *Key,int Default);
	void	WriteInt(const char *Section,const char *Key,int Value);
	double	ReadDouble(const char *Section,const char *Key,double Default);
	void	WriteDouble(const char *Section,const char *Key,double Value);
	bool	ReadBool(const char *Section,const char *Key,bool Default);
	void	WriteBool(const char *Section,const char *Key,bool Value);
	
	LPWSTR	ReadStringW(LPCWSTR Section,LPCWSTR Key,LPCWSTR Default);
	void	WriteStringW(LPCWSTR Section,LPCWSTR Key,LPCWSTR Value);
	int		ReadIntW(LPCWSTR Section,LPCWSTR Key,int Default);
	void	WriteIntW(LPCWSTR Section,LPCWSTR Key,int Value);
	double	ReadDoubleW(LPCWSTR Section,LPCWSTR Key,double Default);
	void	WriteDoubleW(LPCWSTR Section,LPCWSTR Key,double Value);
	bool	ReadBoolW(LPCWSTR Section,LPCWSTR Key,bool Default);
	void	WriteBoolW(LPCWSTR Section,LPCWSTR Key,bool Value);
};

