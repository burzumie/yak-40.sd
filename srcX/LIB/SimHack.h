#pragma once

#include "../Common/CommonSys.h"

enum VIEW_MODE {
	VIEW_MODE_INVALID = 0x0,
	VIEW_MODE_COCKPIT = 0x1,
	VIEW_MODE_VIRTUAL_COCKPIT = 0x2,
	VIEW_MODE_TOWER = 0x3,
	VIEW_MODE_SPOT = 0x4,
	VIEW_MODE_MAP = 0x5,
	VIEW_MODE_TRACK = 0x6,
	VIEW_MODE_MAX = 0x7,
	VIEW_MODE_FIRST = 0x1,
	VIEW_MODE_LAST = 0x4,
};

enum GEAR_MOVEMENT_TYPE {
	MOVEMENT_NONE = 0x0,
	MOVEMENT_EXTENDING = 0x1,
	MOVEMENT_RETRACTING = 0x2
};

enum MSG_TYPE_ENUM {
	MSG_NONE = 0x0,
	MSG_COMM_SEND = 0x1,
	MSG_COMM_RECEIVE = 0x2,
	MSG_ATIS = 0x3,
	MSG_INSTRUCTOR = 0x4,
	MSG_GENERIC_PRINT = 0x5,
	MSG_GENERIC_SCROLL = 0x6,
	MSG_MENU_TITLE = 0x7,
	MSG_MENU_OPTION = 0x8,
	MSG_DAMAGE_PLAYER = 0x9,
	MSG_DAMAGE_VICTIM = 0xa,
	MSG_CFS_CDS = 0xb,
	MSG_WAVECAPTION = 0xc,
	MSG_DEBUG = 0xd,
	MSG_LESSON_GOAL = 0xe,
	MSG_MAX = 0xf,
};

#pragma pack(1)
typedef struct _fsx_engine_params_t
{
	BYTE  some_data[0xC];
	double engine_leverer;
	BYTE  some_data1[0xD8];
}fsx_engine_params_t;

typedef struct _fsx_turbine_data_t 
{
	double N1;
	double N2;
	double corrected_N1;
	double corrected_N2;
	double corrected_FF;
	BYTE some_data[0x58];
}fsx_turbine_data_t;

typedef struct AIRPLANE_GEOMETRY
{
	BYTE data1[0xCC];
	double htail_incidence;
	BYTE data2[0x184-0xCC-0x8];
}AIRPLANE_GEOMETRY,*PAIRPLANE_GEOMETRY,**PPAIRPLANE_GEOMETRY;

enum CONTACT_POINT_SOUNDS {	
	CONTACT_SOUND_CENTER			= 0x0,
	CONTACT_SOUND_AUX				= 0x1,
	CONTACT_SOUND_LEFT				= 0x2,
	CONTACT_SOUND_RIGHT				= 0x3,
	CONTACT_SOUND_FUSELAGE_SCRAPE	= 0x4,
	CONTACT_SOUND_LEFT_WING_SCRAPE	= 0x5,
	CONTACT_SOUND_RIGHT_WING_SCRAPE	= 0x6,
	CONTACT_SOUND_AUX1_SCRAPE		= 0x7,
	CONTACT_SOUND_AUX2_SCRAPE		= 0x8,
	CONTACT_SOUND_XTAIL_SCRAPE		= 0x9,
	MAX_CONTACT_SOUND				= 0xA
};

typedef struct _contact_point_fixed_data
{
	XYZF64 Pos;
	int Flags;
	double SpringConstant;
	double DampingConstant;
	double DampingRatio;
	double CrashVelocity;
	enum CONTACT_POINT_SOUNDS eSound;
	double MaxToStaticCompressionRatio;
	double max_compression;
	double wheel_radius;
	double ExtensionRate;
	double RetractionRate;
	double MaxSteerAngle;
	int bFullCastoring;
	double damaging_dynamic_pressure;
	double max_airspeed_retraction;
}CONTACT_POINT_FIXED_DATA,*PCONTACT_POINT_FIXED_DATA,**PPCONTACT_POINT_FIXED_DATA;

typedef struct _fsx_breaks_t
{
	float toe_brake_scale;
	BYTE   data1[0x14-0x04];
}fsx_breaks_t;

typedef struct _aero_tuning_constants {
	double ZeroAlphaLiftScalar;
	double StallAlphaClean;
	double ParasiticDragScalar;
	double InducedDragScalar;
	double ElevatorEffScalar;
	double AileronEffScalar;
	double RudderEffScalar;
	double ElevatorTrimEffScalar;
	double AileronTrimEffScalar;
	double RudderTrimEffScalar;
	double PitchStabilityScalar;
	double RollStabilityScalar;
	double YawStabilityScalar;
	double HiAlphaOnRoll;
	double HiAlphaOnYaw;
	float PfactorOnYawScalar;
	float TorqueOnRollScalar;
	float GyroPrecessOnYawScalar;
	float GyroPrecessOnPitchScalar;
}AERO_TUNING_CONSTANTS,*PAERO_TUNING_CONSTANTS,**PPAERO_TUNING_CONSTANTS;

typedef struct _sim_fixed_data
{
	BYTE data1[0xF4];
	fsx_breaks_t *breaks;
	PAIRPLANE_GEOMETRY airplane_geometry;
	BYTE data2[0x2F8];
	double static_thrust;
	BYTE data3[0x2F30-0x3FC];
	PCONTACT_POINT_FIXED_DATA ContactFixedData;
}SIM_FIXED_DATA,*PSIM_FIXED_DATA,**PPSIM_FIXED_DATA;

typedef struct _sim_data
{
	PSIM_FIXED_DATA pStaticData;
	BYTE some_data[0x438];
	fsx_turbine_data_t *turbines;
	BYTE some_data1[0x4];
	fsx_engine_params_t *engines;
}SIM_DATA,*PSIM_DATA,**PPSIM_DATA;

class IAircraftSim
{
	//4 bytes of virtual methods table
	virtual unsigned int __stdcall Init() = 0;
	virtual unsigned int __stdcall Load(void *IAircraftContainer, int) = 0;
	virtual void __stdcall Simulate(double) = 0;
	virtual unsigned int __stdcall VarGet(enum SIMVAR_ID, void*, int) = 0;
	virtual unsigned int __stdcall VarGet(enum SIMVAR_ID, char*, unsigned int, int) = 0;
	virtual unsigned int __stdcall VarGet(enum SIMVAR_ID, unsigned int, double*, int) = 0;
	virtual unsigned int __stdcall VarSet(enum SIMVAR_ID, void*, int) = 0;
	virtual unsigned int __stdcall VarSet(enum SIMVAR_ID, unsigned int, double, int) = 0;
};
class Sim1_main_class : public IAircraftSim
{
	virtual unsigned int __stdcall Init(){return(0);}
	virtual unsigned int __stdcall Load(void *IAircraftContainer, int){return(0);}
	virtual void __stdcall Simulate(double){};
	virtual unsigned int __stdcall VarGet(enum SIMVAR_ID, void*, int){return(0);}
	virtual unsigned int __stdcall VarGet(enum SIMVAR_ID, char*, unsigned int, int){return(0);};
	virtual unsigned int __stdcall VarGet(enum SIMVAR_ID, unsigned int, double*, int){return(0);};
	virtual unsigned int __stdcall VarSet(enum SIMVAR_ID, void*, int){return(0);};
	virtual unsigned int __stdcall VarSet(enum SIMVAR_ID, unsigned int, double, int){return(0);}
public:
	BYTE some_data[0x274-0x4];
	PSIM_DATA sim1_data;
};

typedef struct _fsx_sim1_exports
{
	class IAircraftSim*  (__stdcall *UserAircraftGet)(void);
}fsx_sim1_exports;

#pragma pack()

class CSimBaseModule
{
protected:
	HMODULE m_hModule;
	std::string m_ModuleName; 

public:
	CSimBaseModule(std::string modulename) {
		m_ModuleName=modulename;
		m_hModule=::LoadLibrary(m_ModuleName.c_str());
	}
	virtual ~CSimBaseModule() {
		if(m_hModule)
			::FreeLibrary(m_hModule);
	}
};

class CSimModuleWindow : protected CSimBaseModule
{
private:
	typedef unsigned int (FSAPI *windowShowMessageFn)(char*, enum MSG_TYPE_ENUM, unsigned int, int);
	windowShowMessageFn simShowMessageFn;

	static const int ShowWindowOrdinal=24;

public:
	CSimModuleWindow() : CSimBaseModule("window.dll") {
		simShowMessageFn=NULL;
		if(m_hModule) {
			simShowMessageFn=(windowShowMessageFn)::GetProcAddress(m_hModule,MAKEINTRESOURCE(ShowWindowOrdinal));
		}
	}
	virtual ~CSimModuleWindow(){};

	UINT ShowMessage(char *str, enum MSG_TYPE_ENUM type, unsigned int sec, int col) {
		if(simShowMessageFn)
			return simShowMessageFn(str,type,sec,col);
		return -1;
	}
};

typedef struct _CABLE_INFO {
	int cCable;
	LATLONALT allaCable[8];
}CABLE_INFO,*PCABLE_INFO,**PPCABLE_INFO;

typedef struct _SURFACE_INFO {
	double dElevation;
	SURFACE_TYPE eSurfaceType;
	SURFACE_CONDITION eSurfaceCondition;
	_FLOAT64_VECTOR3 vecNormal;
	XYZF64 vecVelocity;
	XYZF64 rotVelocity;
	int bOnPlatform;
	_CABLE_INFO cables;
}SURFACE_INFO,*PSURFACE_INFO,**PPSURFACE_INFO;

class CSimModuleG3D : protected CSimBaseModule
{
private:
	typedef DWORD (FSAPI *GetFurfaceFn)(LATLONALT *,SURFACE_INFO *,DWORD Unknown);
	GetFurfaceFn simGetFurfaceFn;

	static const int GetSurfaceOrdinal=10;

public:
	CSimModuleG3D() : CSimBaseModule("g3d.dll") {
		simGetFurfaceFn=NULL;
		if(m_hModule) {
			simGetFurfaceFn=(GetFurfaceFn)::GetProcAddress(m_hModule,MAKEINTRESOURCE(GetSurfaceOrdinal));
		}
	}
	virtual ~CSimModuleG3D(){};

	DWORD GetSurface(LATLONALT *pos,SURFACE_INFO *surface,DWORD Unknown) {
		if(simGetFurfaceFn)
			return simGetFurfaceFn(pos,surface,Unknown);
		return -1;
	}
};

class CSimModuleSim1 : protected CSimBaseModule
{
private:
	typedef class IAircraftSim* (FSAPI *UserAircraftGetFn)();
	UserAircraftGetFn simUserAircraftGetFn;

	static const int UserAircraftGetOrdinal=9;

public:
	CSimModuleSim1() : CSimBaseModule("sim1.dll") {
		simUserAircraftGetFn=NULL;
		if(m_hModule) {
			simUserAircraftGetFn=(UserAircraftGetFn)::GetProcAddress(m_hModule,MAKEINTRESOURCE(UserAircraftGetOrdinal));
		}
	}
	virtual ~CSimModuleSim1(){};

	class IAircraftSim* UserAircraftGet() {
		if(simUserAircraftGetFn)
			return simUserAircraftGetFn();
		return NULL;
	}
};

class CSimModulesContainer
{
public:
	std::auto_ptr<CSimModuleWindow> m_Window;
	std::auto_ptr<CSimModuleG3D>	m_G3D;
	std::auto_ptr<CSimModuleSim1>	m_Sim1;

	CSimModulesContainer() {
		m_Window=std::auto_ptr<CSimModuleWindow>(new CSimModuleWindow()	);
		m_G3D	=std::auto_ptr<CSimModuleG3D>	(new CSimModuleG3D()	);
		m_Sim1	=std::auto_ptr<CSimModuleSim1>	(new CSimModuleSim1()	);
	}

	~CSimModulesContainer() {};
};

extern CSimModulesContainer *g_SimModules;
