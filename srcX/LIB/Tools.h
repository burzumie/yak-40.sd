/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Lib/Tools.h $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#pragma warning(disable: 4996)

#include "../Common/CommonSys.h"

inline double dgrd(double angle){return angle*PI/180.0;}
inline double rddg(double angle){return angle*180.0/PI;}
inline double dnorm(double angle){if(angle>=0&&angle<360)return angle;if(angle<0)angle+=360;return angle-360*(int)(angle/360);}

#define	Bcd2Dec(BcdNum) HornerScheme(BcdNum,0x10,10)
#define	Dec2Bcd(DecNum) HornerScheme(DecNum,10,0x10)

UINT32 __cdecl HornerScheme(UINT32 Num,UINT32 Divider,UINT32 Factor);

inline int turn_side(double selected_angle, double current_angle)
{
	if(selected_angle>180) {
		if(current_angle<=selected_angle&&current_angle>(selected_angle-180))return 1;
		else return 0;
	} else {
		if(current_angle>=selected_angle&&current_angle<(selected_angle+180))return 0;
		else return 1;
	}
}

template <class X>
void SwapValue(X &p1,X &p2)
{
	X t=p1;
	p1=p2;
	p2=t;
}

template <class X>
X LimitValue(X v,X min,X max)
{
	X ret=v;
	if(v<min)ret=min;
	if(v>max)ret=max;
	return ret;
}

template <class X>
X LimitValueMin(X v,X min)
{
	if(v<min)v=min;
	return v;
}

template <class X>
X LimitValueMax(X v,X max)
{
	if(v>max)v=max;
	return v;
}

template <class X>
X NormaliseDegree180(X var)
{
	if(var>180)var-=360;
	if(var<-180)var+=360;
	return var;
}

template <class X>
X NormaliseDegree360(X var)
{
	while(var>360)var-=360;
	while(var<0)var+=360;
	return var;
}

template <class X>
X NormaliseDegree360180(X var)
{
	if(var>360)var-=360;
	if(var<0)var+=360;
	if(var>180)var-=360;
	if(var<-180)var+=360;
	return var;
}

template <class X>
X NormaliseDegreeW(X var)
{
	if(var>360) {
		while(var>360)var-=360.0F;
	}
	if(var<0) {
		while(var<0)var+=360.0F;
	}
	return var;
}

inline bool PathFileExists(char *name)
{
	FILE *f=fopen(name,"r");
	if(!f)
		return false;

	fclose(f);
	return true;
}

inline double GetPress1(double val)
{
	return int(val)%10;
}

inline double GetPress10(double val)
{
	double a1=int(val)%10;
	double a10=int(val)%100;
	return a1>90?((a10-a1)+(a1-90)*10)/1:(a10-a1)/1;
}

inline double GetPress100(double val)
{
	double a10=int(val)%100;
	double a100=int(val)%1000;
	return a10>90?((a100-a10)+(a10-90)*10)/10:(a100-a10)/10;
}

inline double GetAlt10(double val)
{
	return int(val)%100;
}

inline double GetAlt100(double val)
{
	double a10=int(val)%100;
	double a100=int(val)%1000;
	return a10>90?((a100-a10)+(a10-90)*10)/10:(a100-a10)/10;
}

inline double GetAlt1000(double val)
{
	if(val<990)
		return 0;
	double a10=int(val)%100;
	double a100=int(val)%1000;
	double a1000=int(val)%10000;
	return (a10>90&&a100>900)?((a1000-a100)+(a10-90)*100)/100:(a1000-a100)/100;
}

inline double GetAlt10000(double val)
{
	if(val<9990)
		return 0;
	double a10=int(val)%100;
	double a100=int(val)%1000;
	double a1000=int(val)%10000;
	double a10000=int(val)%100000;
	return (a10>90&&a100>900&&a1000>9000)?((a10000-a1000)+(a10-90)*1000)/1000:(a10000-a1000)/1000;
}
