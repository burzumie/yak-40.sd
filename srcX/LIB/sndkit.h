/**********************************************************
	(c) Igor Borozdin 2006

	Project beginned 2006-08-2

	$Author: Bor $ 
	$Date: 2006/08/13 14:50:27 $ 
	$Revision: 1.3 $

	$RCSfile: sndkit.h,v $ 

	Desc: Sound engine for MSFS
***********************************************************/

#ifndef __SNDKIT_H__
#define __SNDKIT_H__

#include <dsound.h>
#include <dxerr.h>
#include <MMSystem.h>


#if defined(DEBUG) || defined(_DEBUG)
#ifndef V
#define V(x)           { hr = x; if( FAILED(hr) ) { DXUTTrace( __FILE__, (DWORD)__LINE__, hr, L#x, true ); } }
#endif
#ifndef V_RETURN
#define V_RETURN(x)    { hr = x; if( FAILED(hr) ) { return DXUTTrace( __FILE__, (DWORD)__LINE__, hr, L#x, true ); } }
#endif
#else
#ifndef V
#define V(x)           { hr = x; }
#endif
#ifndef V_RETURN
#define V_RETURN(x)    { hr = x; if( FAILED(hr) ) { return hr; } }
#endif
#endif

#define DXUT_StopSound(s)         { if(s) s->Stop(); }
#define DXUT_PlaySound(s)         { if(s) s->Play( 0, 0 ); }
#define DXUT_PlaySoundLooping(s)  { if(s) s->Play( 0, DSBPLAY_LOOPING ); }


void DXUTOutputDebugStringW( LPCWSTR strMsg, ... );
void DXUTOutputDebugStringA( LPCSTR strMsg, ... );
HRESULT WINAPI DXUTTrace( const CHAR* strFile, DWORD dwLine, HRESULT hr, const WCHAR* strMsg, bool bPopMsgBox );

#ifdef UNICODE
#define DXUTOutputDebugString DXUTOutputDebugStringW
#else
#define DXUTOutputDebugString DXUTOutputDebugStringA
#endif


// These macros are very similar to dxerr's but it special cases the HRESULT defined
// by DXUT to pop better message boxes. 
#if defined(DEBUG) || defined(_DEBUG)
#define DXUT_ERR(str,hr)           DXTraceW( __FILE__, (DWORD)__LINE__, hr, str, false )
#define DXUT_ERR_MSGBOX(str,hr)    DXTraceW( __FILE__, (DWORD)__LINE__, hr, str, true )
#define DXUTTRACE                  DXUTOutputDebugString
#else
#define DXUT_ERR(str,hr)           (hr)
#define DXUT_ERR_MSGBOX(str,hr)    (hr)
#define DXUTTRACE                  (__noop)
#endif




namespace sndkit
{

	typedef struct _SAMPLELOOP
	{
		DWORD dwCuePointID;
		DWORD dwType; 
		DWORD dwStart;
		DWORD dwEnd;
		DWORD dwFraction;
		DWORD dwPlayCount;
	} SAMPLELOOP, *PSAMPLELOOP, **PPSAMPLELOOP;


	typedef struct _SMPL 
	{ 
		DWORD dwManufacturer; 
		DWORD dwProduct; 
		DWORD dwSamplePeriod; 
		DWORD dwMIDIUnityNote; 
		DWORD dwMIDIPitchFraction; 
		DWORD dwSMPTEFormat; 
		DWORD dwSNPTEOffset; 
		DWORD dwNumSampleLoops; 
		DWORD dwSamplerData; 
	} SMPL; 

	class CWaveFile
	{

	public:
		WAVEFORMATEX* m_pwfx;        // Pointer to WAVEFORMATEX structure
		HMMIO         m_hmmio;       // MM I/O handle for the WAVE
		MMCKINFO      m_ck;          // Multimedia RIFF chunk
		MMCKINFO      m_ckRiff;      // Use in opening a WAVE file
		DWORD         m_dwSize;      // The size of the wave file
		MMIOINFO      m_mmioinfoOut;	
		DWORD         m_dwFlags;
		BOOL          m_bIsReadingFromMemory;
		BYTE*         m_pbData;
		BYTE*         m_pbDataCur;
		ULONG         m_ulDataSize;
		CHAR*         m_pResourceBuffer;
		
		SMPL		  m_Sample;
		PSAMPLELOOP	  m_pSampleLoops;


	protected:
		HRESULT ReadMMIO();

	public:
		CWaveFile();
		~CWaveFile();

		HRESULT Open( LPWSTR strFileName, WAVEFORMATEX* pwfx,HMODULE hModule);
	    HRESULT OpenFromMemory( BYTE* pbData, ULONG ulDataSize, WAVEFORMATEX* pwfx );
		HRESULT Close();

		HRESULT Read( BYTE* pBuffer, DWORD dwSizeToRead, DWORD* pdwSizeRead );
   
		DWORD   GetSize();
		HRESULT ResetFile();
		WAVEFORMATEX* GetFormat() { return m_pwfx; };

	};


	class CSound
	{
		friend class CSoundManager;
	protected:
		DWORD		m_dwDSBufferSize;
		CWaveFile*	m_pWaveFile;
		DWORD		m_dwNumBuffers;
		DWORD		m_dwCreationFlags;
		LONG		m_lVolume;

		LPDIRECTSOUNDBUFFER* m_apDSBuffer;
		HANDLE				m_hLoopEvent;
		DWORD				m_dwStartPosition;
		DWORD				m_dwStopPosition;

		DSBPOSITIONNOTIFY	m_PosNotify;
		LPDIRECTSOUNDNOTIFY	m_pDSNotify;

		BOOL	m_bMute;


    	HRESULT RestoreBuffer( LPDIRECTSOUNDBUFFER pDSB, BOOL* pbWasRestored );

	public:
		CSound( LPDIRECTSOUNDBUFFER* apDSBuffer, DWORD dwDSBufferSize, DWORD dwNumBuffers, CWaveFile* pWaveFile, DWORD dwCreationFlags );
		virtual ~CSound();

		HRESULT FillBufferWithSound( LPDIRECTSOUNDBUFFER pDSB, BOOL bRepeatWavIfBufferLarger );
		LPDIRECTSOUNDBUFFER GetFreeBuffer();
		LPDIRECTSOUNDBUFFER GetBuffer( DWORD dwIndex );

		HRESULT Play( DWORD dwFlags = 0, DWORD dwPriority = 0, LONG lVolume = 0, LONG lFrequency = -1, LONG lPan = 0 );
		HRESULT PlayIfNotPlay(DWORD dwFlags = 0, DWORD dwPriority = 0, LONG lVolume = 0, LONG lFrequency = -1, LONG lPan = 0) {
			if(!IsSoundPlaying())
				return Play(dwFlags,dwPriority,lVolume,lFrequency,lPan);
			return S_OK;
		}
		HRESULT Stop();
		HRESULT StopIfPlay() {
			if(IsSoundPlaying())
				return Stop();
			return S_OK;
		}
		HRESULT Reset();
		BOOL    IsSoundPlaying();
		HRESULT SetVolume( LONG lVolume );
		HRESULT IncVolume( LONG lDec ) { return SetVolume( m_lVolume + lDec ); };
		HRESULT DecVolume( LONG lDec ) { return SetVolume( m_lVolume - lDec ); };
		HRESULT Mute( LONG lVolume = DSBVOLUME_MIN );
		HRESULT RestoreVolume( void );
		HRESULT	Pause(void);
		HRESULT Replay(void);
	};

	// 128 ������
	#define MAX_SND_SOCKETS	1024

	//-----------------------------------------------------------------------------
	// Name: class CSoundManager
	// Desc: 
	//-----------------------------------------------------------------------------
	class CSoundManager
	{
	protected:
		IDirectSound8* m_pDS;

		HANDLE	m_hThread;
		DWORD	m_dwThreadID;
		DWORD	m_nMaxSndCount;

		DWORD	s_nEventsCount;
		HANDLE	s_hEvents[MAX_SND_SOCKETS];
		CSound	*s_LoopSounds[MAX_SND_SOCKETS];
		BOOL	s_bIsTerminating;

		DWORD	m_nSoundsCount;
		CSound	*m_Sounds[MAX_SND_SOCKETS];

		HMODULE					m_HDLL;
		HINSTANCE				m_HWND;
		HWND					m_WND;

	public:
		CSoundManager();
		~CSoundManager();

		HRESULT Initialize( HINSTANCE hdll, DWORD dwCoopLevel, HWND wnd );
		inline  LPDIRECTSOUND8 GetDirectSound() { return m_pDS; }
		HRESULT SetPrimaryBufferFormat( DWORD dwPrimaryChannels, DWORD dwPrimaryFreq, DWORD dwPrimaryBitRate );

		BOOL	AddAndNotify(CSound* pSound);
		HRESULT Create( CSound** ppSound, LPWSTR strWaveFileName, DWORD dwCreationFlags = 0, GUID guid3DAlgorithm = GUID_NULL, DWORD dwNumBuffers = 1 );
		HRESULT CreateFromMemory( CSound** ppSound, BYTE* pbData, ULONG ulDataSize, LPWAVEFORMATEX pwfx, DWORD dwCreationFlags = 0, GUID guid3DAlgorithm = GUID_NULL, DWORD dwNumBuffers = 1 );

		DWORD SoundEventHandler(void);
		static	DWORD WINAPI SoundEventHandler(LPVOID param)
		{
			return ( (CSoundManager* ) param)->SoundEventHandler();
		};
	};
};

#endif //__SNDKIT_H__