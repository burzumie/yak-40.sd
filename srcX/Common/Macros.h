/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Common/Macros.h $

  Last modification:
    $Date: 19.02.06 5:54 $
    $Revision: 7 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include "../api/panelstools.h"

/************************************************************************/
/* DLLMAIN DEBUG                                                        */
/************************************************************************/
#define DLLMAIND(FML)	double FSAPI icb_dummy(PELEMENT_ICON pelement) \
{ \
	CHK_BRT(); \
	return 0; \
} \
_CrtMemState memoryState; \
HINSTANCE g_hInstance=NULL; \
HANDLE hFileMappingAPI=NULL;\
\
bool OpenPipe()\
{\
	DWORD sizeAPI=sizeof(SDAPIEntrys);\
\
	hFileMappingAPI=OpenFileMappingA(FILE_MAP_READ|FILE_MAP_WRITE,FALSE,API_SHARE_NAME);\
\
	if(hFileMappingAPI==NULL) {\
		return false;\
	}\
\
	g_pSDAPIEntrys=(SDAPIEntrys *)MapViewOfFile(hFileMappingAPI,FILE_MAP_READ|FILE_MAP_WRITE,0,0,sizeAPI);\
\
	if(g_pSDAPIEntrys==NULL) {\
		return false;\
	}\
\
	return true;\
}\
\
void ClosePipe()\
{\
	if(g_pSDAPIEntrys) {\
		UnmapViewOfFile(g_pSDAPIEntrys);\
		g_pSDAPIEntrys=NULL;\
	}\
	if(hFileMappingAPI) {\
		CloseHandle(hFileMappingAPI);\
		hFileMappingAPI=NULL;\
	}\
}\
BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) \
{ \
	_CrtMemCheckpoint(&memoryState); \
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF); \
	g_hInstance=(HINSTANCE)hModule; \
	switch(ul_reason_for_call)	{ \
		case DLL_PROCESS_ATTACH: \
		DisableThreadLibraryCalls((HMODULE)hModule); \
		break; \
		case DLL_THREAD_ATTACH: \
		break; \
		case DLL_THREAD_DETACH: \
		break; \
		case DLL_PROCESS_DETACH: \
		break; \
	} \
	return TRUE; \
} \
static void UpdatePanel() \
{ \
} \
static void FSAPI fnInitPanel(void) \
{ \
	OpenPipe();	\
} \
static void FSAPI fnDeinitPanel(void) \
{ \
	ClosePipe(); \
	CHECK_MEM_LEAK(FML); \
}\
GAUGESIMPORT	ImportTable =						\
{													\
	{ 0x0000000F, (PPANELS)NULL },					\
	{ 0x00000000, NULL }							\
};													

/************************************************************************/
/* DLLMAIN                                                              */
/************************************************************************/

#define DLLMAIN()	double FSAPI icb_dummy(PELEMENT_ICON pelement) \
{ \
	CHK_BRT(); \
	return 0; \
} \
HINSTANCE g_hInstance=NULL; \
	HANDLE hFileMappingAPI=NULL;\
	\
	bool OpenPipe()\
{\
	DWORD sizeAPI=sizeof(SDAPIEntrys);\
	\
	hFileMappingAPI=OpenFileMappingA(FILE_MAP_READ|FILE_MAP_WRITE,FALSE,API_SHARE_NAME);\
	\
	if(hFileMappingAPI==NULL) {\
	return false;\
	}\
	\
	g_pSDAPIEntrys=(SDAPIEntrys *)MapViewOfFile(hFileMappingAPI,FILE_MAP_READ|FILE_MAP_WRITE,0,0,sizeAPI);\
	\
	if(g_pSDAPIEntrys==NULL) {\
	return false;\
	}\
	\
	return true;\
}\
	\
	void ClosePipe()\
{\
	if(g_pSDAPIEntrys) {\
	UnmapViewOfFile(g_pSDAPIEntrys);\
	g_pSDAPIEntrys=NULL;\
	}\
	if(hFileMappingAPI) {\
	CloseHandle(hFileMappingAPI);\
	hFileMappingAPI=NULL;\
	}\
}\
BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) \
{ \
	g_hInstance=(HINSTANCE)hModule; \
	switch(ul_reason_for_call)	{ \
		case DLL_PROCESS_ATTACH: \
		DisableThreadLibraryCalls((HMODULE)hModule); \
		break; \
		case DLL_THREAD_ATTACH: \
		break; \
		case DLL_THREAD_DETACH: \
		break; \
		case DLL_PROCESS_DETACH: \
		break; \
	} \
	return TRUE; \
} \
static void UpdatePanel() \
{ \
} \
static void FSAPI fnInitPanel(void) \
{ \
	OpenPipe();\
} \
static void FSAPI fnDeinitPanel(void) \
{ \
	ClosePipe();\
}\
	GAUGESIMPORT	ImportTable =						\
{													\
	{ 0x0000000F, (PPANELS)NULL },					\
	{ 0x00000000, NULL }							\
};													

/************************************************************************/
/* �������� ������ ������ � ������ ������ � ���� � ����� ����� �        */
/************************************************************************/
#define CHECK_MEM_LEAK(F) /**/ \
	HANDLE hfile = CreateFileA ("F:\\MemLeaks\\Yak40\\"##F, \
	GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, \
	FILE_ATTRIBUTE_NORMAL, NULL); \
	_CrtSetReportMode (_CRT_WARN, _CRTDBG_MODE_FILE); \
	_CrtSetReportFile (_CRT_WARN, hfile); \
	_CrtSetReportMode (_CRT_ERROR, _CRTDBG_MODE_FILE); \
	_CrtSetReportFile (_CRT_ERROR, hfile); \
	_CrtMemDumpAllObjectsSince(&memoryState); \
	CloseHandle (hfile);
