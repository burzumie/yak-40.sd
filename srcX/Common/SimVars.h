/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/SimVars.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "CommonSYS.h"

class SDSimVars
{
private:
	bool		m_StdtokensInited;
	MODULE_VAR	m_StdTokens[C_GAUGE_TOKEN];

public:
	SDSimVars() {
		m_StdtokensInited=false;
	}

	void InitVars() {
		SECUREBEGIN_C

		if(!m_StdtokensInited) {
			for(int i=1;i<C_GAUGE_TOKEN;i++) {
				m_StdTokens[i].id=(GAUGE_TOKEN)i;
				initialize_var(&m_StdTokens[i]);
			}
			m_StdtokensInited=true;
		}

		SECUREEND_C
	}

	void UpdateVars() {
		if(m_StdtokensInited) {
			for(int i=1;i<C_GAUGE_TOKEN;i++) {
				lookup_var(&m_StdTokens[i]);
			}
		}
	}

	void UpdateVars(GAUGE_TOKEN token) {
		if(m_StdtokensInited) {
			lookup_var(&m_StdTokens[token]);
		}
	}

	inline double GetN(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.n;	}
	inline ENUM   GetE(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.e;	}
	inline FLAGS  GetF(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.f;	}
	inline VAR32  GetD(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.d;	}
	inline BOOL   GetB(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.b;	}
	inline VAR32  GetO(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.o;	}
	inline PVOID  GetP(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.p;	}

};
