/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: fs_sdk.h,v $

  Last modification:
    Date:      $Date: 2005/08/22 17:31:58 $
    Version:   $Revision: 1.3 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK__
#define _FS_SDK__
 
#pragma pack(4)

template<DWORD T>struct ModuleIdentifier
{
	DWORD ID;
	inline ModuleIdentifier():ID(T){};
};

struct sImportTab
{
#ifdef _FS_SDK_FS6
	ModuleIdentifier<MID_FS6ID> mFSID;
	FS6* pFS6;
#endif
#ifdef _FS_SDK_ABLSCPT
	ModuleIdentifier<MID_ABLSCPT> m0;
	ABLSCPT* pABLSCPT;
#endif
#ifdef _FS_SDK_ACONTAIN
	ModuleIdentifier<MID_ACONTAIN> m1;
	ACONTAIN* pACONTAIN;
#endif
#ifdef _FS_SDK_AI_PLAYR
	ModuleIdentifier<MID_AI_PLAYR> m2;
	AI_PLAYR* pAIPlayer;
#endif
#ifdef _FS_SDK_ATC
	ModuleIdentifier<MID_ATC> m3;
	ATC* pABLSCPT;
#endif
#ifdef _FS_SDK_CONTROLS
	ModuleIdentifier<MID_CONTROLS> m4;
	CONTROLS* pControls;
#endif
#ifdef _FS_SDK_DEMO
	ModuleIdentifier<MID_DEMO> m5;
	DEMO* pDemo;
#endif
#ifdef 	_FS_SDK_DYNAMIC
	ModuleIdentifier<MID_DYNAMIC> m6;
	DYNAMIC* pDynamic;
#endif
#ifdef _FS_SDK_FACILITIES
	ModuleIdentifier<MID_FACILITIES> m7;
	FACILITIES* pFACILITIES;
#endif
#ifdef _FS_SDK_FE
	ModuleIdentifier<MID_FE> m8;
	FE* pFE;
#endif
#ifdef _FS_SDK_FLIGHT
	ModuleIdentifier<MID_FLIGHT> m9;
	FLIGHT* pFlight;
#endif
#ifdef _FS_SDK_FSUI
	ModuleIdentifier<MID_FSUI> m10;
	FSUI* pFSUI;
#endif
#ifdef _FS_SDK_G2D
	ModuleIdentifier<MID_G2D> m11;
	G2D* pG2D;
#endif
#ifdef _FS_SDK_G3D
	ModuleIdentifier<MID_G3D> m12;
	G3D* pG3D;
#endif
#ifdef _FS_SDK_GLOBAL
	ModuleIdentifier<MID_GLOBAL> m13;
	GLOBAL* pGlobal;
#endif
#ifdef _FS_SDK_INSTRUCTOR
	ModuleIdentifier<MID_INSTRUCTOR> m14;
	INSTRUCTOR* pInstructor;
#endif
#ifdef _FS_SDK_MAIN
	ModuleIdentifier<MID_MAIN> m15;
	MAIN* pMain;
#endif
#ifdef _FS_SDK_MPCHAT
	ModuleIdentifier<MID_MPCHAT> m16;
	MPCHAT* pMPChat;
#endif
#ifdef _FS_SDK_MULTIPLAYER
	ModuleIdentifier<MID_MULTIPLAYER> m17;
	MULTIPLAYER* pMULTIPLAYER;
#endif
#ifdef 	_FS_SDK_PANELS
	ModuleIdentifier<MID_PANELS> m18;
	PANELS* pPanels;
#endif
#ifdef 	_FS_SDK_REALITY
	ModuleIdentifier<MID_REALITY> m19;
	REALITY* pReality;
#endif
#ifdef 	_FS_SDK_SIM1
	ModuleIdentifier<MID_SIM1> m20;
	SIM1* pSIM1;
#endif
#ifdef 	_FS_SDK_SIMSCHED
	ModuleIdentifier<MID_SIMSCHED> m21;
	SIMSCHED* pSIMSCHED;
#endif
#ifdef 	_FS_SDK_SOUND
	ModuleIdentifier<MID_SOUND> m22;
	SOUND* pSOUND;
#endif
#ifdef 	_FS_SDK_SYMMAP
	ModuleIdentifier<MID_SYMMAP> m23;
	SYMMAP* pSYMMAP;
#endif
#ifdef 	_FS_SDK_TERRAIN
	ModuleIdentifier<MID_TERRAIN> m24;
	TERRAIN* pTERRAIN;
#endif
#ifdef 	_FS_SDK_TRAFFIC
	ModuleIdentifier<MID_TRAFFIC> m25;
	TRAFFIC* pTRAFFIC;
#endif
#ifdef 	_FS_SDK_UTIL
	ModuleIdentifier<MID_UTIL> m26;
	UTIL* pUtil;
#endif
#ifdef 	_FS_SDK_VISUALFX
	ModuleIdentifier<MID_VISUALFX> m28;
	VISUALFX* pVISUALFX;
#endif
#ifdef 	_FS_SDK_WEATHER
	ModuleIdentifier<MID_WEATHER> m29;
	WEATHER* pWEATHER;
#endif
#ifdef 	_FS_SDK_WINDOW
	ModuleIdentifier<MID_WINDOW> m30;
	WINDOW* pWindow;
#endif
	ModuleIdentifier<MID_TERMINATOR> dummy0;
	ModuleIdentifier<MID_TERMINATOR> dummy1;
};

extern "C" __declspec(dllexport)sImportTab ImportTable;

#pragma pack()

#endif