/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: panels.h,v $

  Last modification:
    Date:      $Date: 2005/09/18 08:04:12 $
    Version:   $Revision: 1.3 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_PANELS
#define _FS_SDK_PANELS

#pragma pack(4)

typedef enum    VAR_TYPE {
    VAR_TYPE_NONE,

    TYPE_BOOL8,
    TYPE_UINT8,
    TYPE_SINT8,
    TYPE_FLAGS8,
    TYPE_ENUM8,

    TYPE_BOOL16,
    TYPE_ANGL16,
    TYPE_UINT16,
    TYPE_SINT16,
    TYPE_UIF16,
    TYPE_SIF16,
    TYPE_FLAGS16,
    TYPE_ENUM16,
    TYPE_BCD16,
    TYPE_BCO16,
    TYPE_VAR16,

    TYPE_BOOL32,
    TYPE_ANGL32,
    TYPE_UINT32,
    TYPE_SINT32,
    TYPE_UIF32,
    TYPE_SIF32,
    TYPE_FLAGS32,
    TYPE_ENUM32,
    TYPE_VAR32,

    TYPE_ANGL48,
    TYPE_SINT48,
    TYPE_UIF48,
    TYPE_SIF48,

    TYPE_UINT64,
    TYPE_SINT64,
    TYPE_SIF64,
    TYPE_FLOAT64,

    TYPE_BOOL,
    TYPE_FLAGS,
    TYPE_ENUM,

    TYPE_VOID,
    TYPE_PVOID,

    TYPE_PUINT32,
    TYPE_PSINT32,
    TYPE_PFLOAT64,

    VAR_TYPE_MAX
} VAR_TYPE;
	
typedef union   UNIVERSAL_VAR {
    FLOAT64 n;              // any number
    BOOL    b;              // any boolean
    ENUM    e;              // any enumerated value
    FLAGS   f;              // any flags field
    PVOID   p;              // any pointer
    VAR32   d;              // any binary coded decimal
    VAR32   o;              // any binary coded octal
} UNIVERSAL_VAR,*PUNIVERSAL_VAR,**PPUNIVERSAL_VAR;

typedef struct  MODULE_VAR {
    GAUGE_TOKEN             id;
    PVOID                   var_ptr;
    VAR_TYPE                var_type;
    UNIVERSAL_VAR           var_value;
    UNIVERSAL_VAR           var_old;                // value of global var last iteration
} MODULE_VAR,*PMODULE_VAR,**PPMODULE_VAR;
	
	
	
// Panel identifiers
#define IDENT_MAIN_PANEL						0
#define IDENT_THROTTLE_PANEL                    10
#define IDENT_RADIO_STACK_PANEL                 50
#define IDENT_COMPASS_PANEL                     75
#define IDENT_MINI_CONTROLS_PANEL               100
#define IDENT_ANNUNCIATOR_PANEL                 125
#define IDENT_ANNUNCIATOR2_PANEL                150
#define IDENT_IFR_MAIN_PANEL                    175
#define IDENT_COLLECTIVE_PANEL                  200
#define IDENT_GPS_PANEL                         225
#define IDENT_OVERHEAD_PANEL                    250
#define IDENT_USER								10000
#define IDENT_USER_MAX							19999
#define IDENT_AIRCRAFT_HUD_3D_PANEL				20000
#define IDENT_HELICOPTER_HUD_3D_PANEL			20001
#define IDENT_CONTROLS_HUD_3D_PANEL             20002
#define IDENT_TEXT_HUD_3D_PANEL                 20003
#define IDENT_DAMAGE_HUD_3D_PANEL               20004
#define IDENT_SCORE_HUD_3D_PANEL                20005
#define IDENT_LSO_WINDOW						22000
#define IDENT_INVALID							4294967295
	
// HUD settings
#define HUD_ON                          0x1
#define HUD_METRIC                      0x2
	
	
// PANEL STATES
//
#define PS_MAIN_PANEL_VISIBLE           BIT1
#define PS_MAIN_PANEL_COMPOSED          BIT2
#define PS_PADLOCK_SERVER				BIT3            // not used yet
#define PS_PADLOCK_CLIENT				BIT4            // not used yet
#define PS_HELICOPTER_HUD_VISIBLE       BIT5
#define PS_AIRCRAFT_HUD_VISIBLE         BIT6
#define PS_AIRCRAFT_HUD_ON				BIT7
#define PS_RADIO_STACK_VISIBLE          BIT8
#define PS_HUD_METRIC_OUTPUT            BIT9
#define PS_MAIN_PANEL_ON				BIT10
#define PS_CONTROLS_HUD_VISIBLE         BIT11
#define PS_RADIO_STACK_AUTOPOPED        BIT12
#define PS_MAIN_PANEL_UNDOCKED          BIT13
#define PS_AIRCRAFT_HUD_EXIST           BIT14
#define PS_HELICOPTER_HUD_EXIST         BIT15
#define PS_HUD_EXIST					(PS_AIRCRAFT_HUD_EXIST|PS_HELICOPTER_HUD_EXIST)
#define PS_PANELS_LOAD_VISIBLE			BIT16
#define PS_HUD_LOAD_VISIBLE				BIT17
#define PS_PANEL_VISIBLE				BIT18
#define PS_HUD_PANEL_VISIBLE			BIT19
#define PS_READONLY						 (~(PS_PADLOCK_SERVER|PS_PADLOCK_CLIENT|PS_HUD_METRIC_OUTPUT))

#define PS_SET                                          0xFFFFFFFF
#define PS_CLEAR                                        0x00000000
	
#define GAUGE_FLAG_NORMAL				0
#define	GAUGE_FLAG_HIDDEN				0x1
#define GAUGE_FLAG_BLINKING				0x2
#define GAUGE_FLAG_GRAYED				0x4
#define GAUGE_FLAG_HILIGHTED			0x8

#define UNITS_UNKNOWN					(-1)
#define UNITS_STRING					(-2)
	
#ifdef __cplusplus

class IGaugeCDrawableCreateParameters
{
public:
	enum PARAMETER_TYPE {
		PARAMETER_TYPE_NONE = 0,
		PARAMETER_TYPE_BOOL = 1,		
		PARAMETER_TYPE_FLOAT = 2,
		PARAMETER_TYPE_INT = 3,
		PARAMETER_TYPE_STRING = 4,
	};

	virtual bool GetParameter (const char* szParName,const char** pszValue) const = 0;
	virtual bool GetParameter (const char* szParName,FLOAT64* pdValue) const = 0;
	virtual bool GetParameter (const char* szParName,SINT32* piValue) const = 0;
	virtual bool GetParameter (const char* szParName,bool* pbValue) const = 0;
	virtual bool SetParameterId (const char* szParName,SINT32 id,PARAMETER_TYPE type) const = 0;
};

class IGaugeCDrawableDrawParameters 
{
public:
	virtual bool GetParameter (SINT32 id,const char** pszValue) const = 0;
	virtual bool GetParameter (SINT32 id,FLOAT64* pdValue) const = 0;
	virtual bool GetParameter (SINT32 id,SINT32* piValue) const = 0;
	virtual bool GetParameter (SINT32 id,bool* pbValue) const = 0;
	virtual FLOAT64 GetScaleX () const = 0;
	virtual FLOAT64 GetScaleY () const = 0;
	virtual const PIXRECT* GetScreenRectangle () const = 0;			// return pointer to a rect relative to the flight sim window left top corner or NULL if undocked or virtual cockpit
};

class IGaugeCDrawable
{
public:
	virtual ULONG AddRef () = 0;
	virtual ULONG Release () = 0;
	virtual FLAGS32 GetFlags () = 0;
	virtual void Update () = 0;
	virtual void Show (bool on) = 0;
	virtual bool Draw (IGaugeCDrawableDrawParameters* pParameters,PIXPOINT size,HDC hdc,PIMAGE pImage) = 0;
	virtual bool SetupDraw (PIXPOINT size,HDC hdc,PIMAGE pImage) = 0;
	virtual bool GetDraw (IGaugeCDrawableDrawParameters* pParameters)  = 0;

	// flags returened by GetFlags
	enum {
		TAKES_DC = 0x1,				// Draw() method should be called with a valid hdc
		TAKES_PIMAGE = 0x2,			// Draw() method should be called with a pImage
		NOT_RESIZABLE =	0x4,		// can not accept size different than in XML file
		DRAWS_ALPHA = 0x8,			// wants to use alpha channel
		NO_TRANSPARENCY = 0x10,		// image is solid rectangle
		MASK_TRANSPARENCY = 0x20,	// use alpha channel to mask image
		DOUBLE_BUFFER = 0x40		// will draw in background,use double buffering
	};
};

class IGaugeCCallback {
public:
	virtual ULONG AddRef () = 0;
	virtual ULONG Release () = 0;
	virtual IGaugeCCallback* QueryInterface (PCSTRINGZ pszInterface) = 0;
	virtual void Update () = 0;
	virtual bool GetPropertyValue (SINT32 id,FLOAT64* pValue) = 0;
	virtual bool GetPropertyValue (SINT32 id,PCSTRINGZ* pszValue) = 0;
	virtual bool SetPropertyValue (SINT32 id,FLOAT64 value) = 0;
	virtual bool SetPropertyValue (SINT32 id,PCSTRINGZ szValue) = 0;
	virtual IGaugeCDrawable* CreateGaugeCDrawable (SINT32 id,const IGaugeCDrawableCreateParameters* pParameters) = 0;
};

class IAircraftCCallback {
public:
	virtual ULONG AddRef () = 0;
	virtual ULONG Release () = 0;
	virtual IAircraftCCallback* QueryInterface (PCSTRINGZ pszInterface) = 0;
	virtual IGaugeCCallback* CreateGaugeCCallback () = 0;
	virtual void Update () = 0;
};

class IPanelCCallback {
public:
	virtual ULONG AddRef () = 0;
	virtual ULONG Release () = 0;
	virtual IPanelCCallback* QueryInterface (PCSTRINGZ pszInterface) = 0;
	virtual UINT32 GetVersion () = 0;
	virtual IAircraftCCallback*  CreateAircraftCCallback (UINT32 ContainerID) = 0;
	virtual bool  ConvertStringToProperty (PCSTRINGZ keyword,SINT32* pID) = 0;
	virtual bool  ConvertPropertyToString (SINT32 id,PPCSTRINGZ pKeyword) = 0;
	virtual bool  GetPropertyUnits (SINT32 id,ENUM* pEnum) = 0;			// UNITS_UNKNOWN if no units,UNITS_STRING if string
};

#define DECLARE_PANEL_CALLBACK_REFCOUNT(CLASSNAME)	\
	private:										\
		ULONG	m_RefCount;							\
	public:											\
		ULONG AddRef ();							\
		ULONG Release ();							\
		
#define DEFINE_PANEL_CALLBACK_REFCOUNT(CLASSNAME)	\
	ULONG CLASSNAME::AddRef ()						\
	{												\
		return ++m_RefCount;						\
	}												\
	ULONG CLASSNAME::Release ()						\
	{												\
		ULONG result = --m_RefCount;				\
		if (result < 1)								\
			delete this;							\
		return result;								\
	}

#define INIT_PANEL_CALLBACK_REFCOUNT(CLASSNAME)		\
	m_RefCount = 1;

#else
typedef struct {void* pVtbl;} IGaugeCCallback;
typedef struct {void* pVtbl;} IAircraftCCallback;
typedef struct {void* pVtbl;} IPanelCCallback;
#endif


typedef int (*GAUGE_KEY_EVENT_HANDLER) (ID32 event,UINT32 evdata,PVOID userdata);

//
// gauge interface routines used by the panels system
//
//
// FS6.1,not used by FS7.0 gauges
typedef UINT32 GENERATE_PHASE;

typedef void FSAPI QUERY_ROUTINE( void );
typedef			   QUERY_ROUTINE           *PQUERY_ROUTINE;
	
typedef void FSAPI INSTALL_ROUTINE( PVOID resource_file_handle );
typedef			   INSTALL_ROUTINE         *PINSTALL_ROUTINE;

typedef void FSAPI INITIALIZE_ROUTINE( void );
typedef			   INITIALIZE_ROUTINE		*PINITIALIZE_ROUTINE;

typedef void FSAPI UPDATE_ROUTINE( void );
typedef			   UPDATE_ROUTINE          *PUPDATE_ROUTINE;
	
typedef void FSAPI GENERATE_ROUTINE( UINT32 phase );
typedef			   GENERATE_ROUTINE        *PGENERATE_ROUTINE;

typedef void FSAPI DRAW_ROUTINE( void );
typedef			   DRAW_ROUTINE            *PDRAW_ROUTINE;

typedef void FSAPI KILL_ROUTINE( void );
typedef			   KILL_ROUTINE            *PKILL_ROUTINE;
	
	
typedef enum {
        PANEL_TYPE_PLAIN,              //  Radio Stack
        PANEL_TYPE_HUD,                //  Aircraft HUD
        PANEL_TYPE_SPECIAL,            //      LSO
        PANEL_TYPE_TEXTURE,            //  Virtual Cockpit Texture
} PANEL_TYPE;
	
	
typedef FLOAT64 FSAPI FN_FLOAT64      ( FLOAT64       val );
typedef               FN_FLOAT64      *PFN_FLOAT64;
	
typedef FLOAT64 FSAPI MODULE_VAR_CB   ( PMODULE_VAR   val );
typedef				  MODULE_VAR_CB   *PMODULE_VAR_CB;
	
	
// Seqsel Selection stuff
#define SELECT_NONE								0
#define SELECT_1								1
#define SELECT_ZOOM                             2
#define SELECT_MAGNETO                          3
#define SELECT_COM_WHOLE                        4
#define SELECT_COM_FRACTION                     5
#define SELECT_NAV1_WHOLE                       6
#define SELECT_NAV1_FRACTION					7
#define SELECT_NAV2_WHOLE                       8
#define SELECT_NAV2_FRACTION					9
#define SELECT_XPNDR_1000                       10
#define SELECT_XPNDR_0100                       11
#define SELECT_XPNDR_0010                       12
#define SELECT_XPNDR_0001                       13
#define SELECT_VOR1                             14
#define SELECT_VOR2                             15
#define SELECT_ENGINE                           16
#define SELECT_DME1                             17
#define SELECT_DME2                             18
#define SELECT_ADF_100                          19
#define SELECT_ADF_010                          20
#define SELECT_ADF_001                          21
#define SELECT_EGT_BUG                          22
#define SELECT_SIM_RATE                         23
#define SELECT_CLOCK_HOURS                      24
#define SELECT_CLOCK_MINUTES					25
#define SELECT_CLOCK_SECONDS					26
#define SELECT_COM2_WHOLE						27
#define SELECT_COM2_FRACTION					28
#define SELECT_ADF_TENTHS						29
	
// used for the image_flags field
#define         IMAGE_USE_TRANSPARENCY          BIT0    // these don't change dynamically and are set
#define         IMAGE_USE_ERASE                         BIT1
#define         IMAGE_USE_BRIGHT                        BIT2
#define         IMAGE_ERASE_ON_FAILURE          BIT3    // not used now
#define         IMAGE_NO_STATIC_BLENDING        BIT4    // not used now
#define         IMAGE_CREATE_DIBSECTION         BIT5
#define         IMAGE_BILINEAR_GRAY                     BIT6
#define         IMAGE_BILINEAR_COLOR            BIT7
#define         IMAGE_PRESERVE_COLOR_IN_HUD     BIT8
#define         IMAGE_CONTAINS_NO_MASK          BIT9

#define         IMAGE_SPRITE_FORCE_TRANS        BIT10   // not used now
#define         IMAGE_BLT_MASK_ONLY                     BIT11

#define         IMAGE_CONTAINS_MASK                     BIT12   // Image contains mask bits
#define         IMAGE_USE_ALPHA                         BIT13   // Image contains alpha channel
#define         IMAGE_USE_LUMINOUS                      BIT14   // Image is bright when the interior light is on
#define         IMAGE_USE_LUMINOUS_PARTIAL  BIT15       // Parts of image are lit by interior light (alpha channel)

#define         IMAGE_HIDDEN_TREE                       BIT25
#define         IMAGE_NO_STRETCH                        BIT27   // these change dynamically
#define         IMAGE_HUD_COLOR_MAP                     BIT28
#define         IMAGE_NO_TRANSLATION            BIT29
#define         IMAGE_HIDDEN                            BIT30
#define         IMAGE_ON_SCREEN                         BIT31


#define         GET_IMAGE_HIDDEN(element)                       (element->image_flags & IMAGE_HIDDEN)
#define         SHOW_IMAGE(element)                                     (element->image_flags &= ~IMAGE_HIDDEN)
#define         HIDE_IMAGE(element)                                     (element->image_flags |= IMAGE_HIDDEN)

#define         GET_IMAGE_HIDDEN_TREE(element)          (element->image_flags & IMAGE_HIDDEN_TREE)
#define         SHOW_IMAGE_TREE(element)                        (element->image_flags &= ~IMAGE_HIDDEN_TREE)
#define         HIDE_IMAGE_TREE(element)                        (element->image_flags |= IMAGE_HIDDEN_TREE)

#define         GET_USE_TRANSPARENCY( element )         (element->image_flags & IMAGE_USE_TRANSPARENCY)
#define         GET_USE_ERASE( element )                        (element->image_flags & IMAGE_USE_ERASE)
#define         GET_USE_BRIGHT( element )                       (element->image_flags & IMAGE_USE_BRIGHT)
#define         GET_ERASE_ON_FAILURE( element )         (element->image_flags & IMAGE_ERASE_ON_FAILURE)

#define         GET_ON_SCREEN( element )                        (element->image_flags & IMAGE_ON_SCREEN)
#define         SET_ON_SCREEN( element )                        element->image_flags |= IMAGE_ON_SCREEN
#define         SET_OFF_SCREEN( element )                       element->image_flags &= ~IMAGE_ON_SCREEN


#define         GET_FAILED( element )                           (element->image_flags & GAUGE_FAILURE)
#define         SET_GAUGE_FAILED( element)                      element->image_flags |= GAUGE_FAILURE
#define         SET_GAUGE_NOT_FAILED( element)          element->image_flags &= ~GAUGE_FAILURE
	
// added for backward compability:
#define			IMAGE_ANTI_ALIAS	IMAGE_BILINEAR_COLOR
/////////////////////////////////////////////////////////////////////////////////////////////////////
//macros from fs2kgauges.h
/////////////////////////////////////////////////////////////////////////////////////////////////////
#define		LIGHT_IMAGE( element )						((element)->image_flags |= IMAGE_USE_BRIGHT)
#define		DARKEN_IMAGE( element )						((element)->image_flags &= ~IMAGE_USE_BRIGHT)

#define		FORCE_LIGHT(element)						SET_OFF_SCREEN(element);\
														LIGHT_IMAGE(element)

#define		FORCE_DARKEN(element)						SET_OFF_SCREEN(element);\
														DARKEN_IMAGE(element)

#define		GET_USE_LUMINOUS( element )				((element)->image_flags & IMAGE_USE_LUMINOUS)
#define		LUMINOUS_IMAGE( element )				((element)->image_flags |= IMAGE_USE_LUMINOUS)
#define		DELUMINOUS_IMAGE( element )				((element)->image_flags &= ~IMAGE_USE_LUMINOUS)

#define		TOGGLE_IMAGE_DATA( element ,IMAGE_FLAGS )		((element)->image_flags ^=IMAGE_FLAGS)
#define		ADD_IMAGE_DATA( element ,IMAGE_FLAGS )			((element)->image_flags |=IMAGE_FLAGS)
#define		REMOVE_IMAGE_DATA( element ,IMAGE_FLAGS )		((element)->image_flags &=~IMAGE_FLAGS)

#define		REDRAW_IMAGE( element )						SET_OFF_SCREEN( element )

#define		GET_BIT( value ,filtermask )			 (  (value) & (filtermask) )
#define		CHECK_BIT( value ,filtermask )			 ( ((value) & (filtermask) )!=FLAGS0 )
#define		CHECK_BIT_STRICT( value ,filtermask )	 ( ((value) & (filtermask) )==(filtermask) )
#define		ADD_BIT( value ,bit )					 (  (value) |=(bit) )
#define		REMOVE_BIT( value ,bit)				 (  (value) &=~(bit) )

//conversion factors and useful constants
#define		PI									3.1415926535897932384626433832795		//circle number

#define		TICKS_PER_SEC						18

#define		FS_LAT_FACTOR						111130.555557
#define		FS_LON_FACTOR						781874935307.40
#define		LAT_DIVIDER 						40007000.0
#define		LON_DIVIDER 						281474976710656.0
#define		FUEL_LEVEL_PCT						83886.08
#define		RADIANS_TO_DEGREE_FACTOR			(180.0/PI)
#define		METER_FEET_FACTOR					3.28084
#define		KILOMETER_NM_MILE_FACTOR			0.54
#define		POUND_KILOGRAM_FACTOR				0.453592
#define		METER_PER_SECOND_KNOT_FACTOR		1.944
#define		GALLON_LITRE_FACTOR					3.785
#define		INCH_HG_PSI_FACTOR	(.4912)
#define		LBS_KILOGRAMM_FACTOR				2.2046

//useful conversion macros
#define		FEET_TO_METER( val )				( ( val )/METER_FEET_FACTOR )
#define		METER_TO_FEET( val )				( ( val )*METER_FEET_FACTOR )

#define		NAUTIC_MILE_TO_METER( val )			( ( val )*(1000.0/KILOMETER_NM_MILE_FACTOR) )
#define		METER_TO_NAUTIC_MILE( val )			( ( val )*(KILOMETER_NM_MILE_FACTOR/1000.0) )

#define		POUND_TO_KILOGRAM( pound )			( ( pound ) *  POUND_KILOGRAM_FACTOR )
#define		KILOGRAM_TO_POUND( kilogram )		( ( kilogram ) /  POUND_KILOGRAM_FACTOR )

#define		POUND_TO_METRIC_TON( pound )		( POUND_KILOGRAM_FACTOR*(pound)/1000.0 )
#define		METRIC_TON_TO_POUND( ton )			( (( ton )* 1000.0)/POUND_KILOGRAM_FACTOR )

#define		METER_PER_SECOND_TO_KNOT( val )		( ( val )*METER_PER_SECOND_KNOT_FACTOR )
#define		KNOT_TO_METER_PER_SECOND( val )		( ( val )/METER_PER_SECOND_KNOT_FACTOR )

#define		LITRE_TO_GALLON( litre )			( (litre)/GALLON_LITRE_FACTOR )
#define		GALLON_TO_LITRE( gallon )			( (gallon)*GALLON_LITRE_FACTOR )

#define		PSI_TO_PSF(press)	(144.0*(press))
#define		PSF_TO_PSI(press)	((press)/144.0)

#define		INCH_HG_TO_PSI(press)	((press)*INCH_HG_PSI_FACTOR)
#define		PSI_TO_INCH_HG(press)	((press)/INCH_HG_PSI_FACTOR)

#define		INCH_HG_TO_PSF(press)	((press)*INCH_HG_PSI_FACTOR*144.0)
#define		PSF_TO_INCH_HG(press)	((press)/INCH_HG_PSI_FACTOR/144.0)
//temperature conversions
#define		FAHRENHEIT_TO_CELSIUS( fahrenheit )	( 5.0/9.0*((fahrenheit)-32.0) )
#define		CELSIUS_TO_FAHRENHEIT( celsius )	( 32.0+9.0/5.0*(celsius) )

#define		CELSIUS_TO_KELVIN( celsius )		( (celsius)+273.15 )
#define		KELVIN_TO_CELSIUS( kelvin )			( (kelvin)-273.15 )

#define		RANKINE_TO_KELVIN( rankine )		( 5.0/9.0*(rankine) )
#define		KELVIN_TO_RANKINE( kelvin )			( 9.0/5.0*(kelvin) )

#define		RANKINE_TO_FAHRENHEIT( rankine )	( (rankine)-459.67 )
#define		FAHRENHEIT_TO_RANKINE( fahrenheit )	( (fahrenheit)+459.67 )

#define		CELSIUS_TO_RANKINE( celsius )		( FAHRENHEIT_TO_RANKINE(CELSIUS_TO_FAHRENHEIT(celsius)) )
#define		RANKINE_TO_CELSIUS( rankine )		( FAHRENHEIT_TO_CELSIUS(RANKINE_TO_FAHRENHEIT(rankine)) )

// flightsimulator related conversion macros
#define		FS_LATITUDE_DEG( val )	 (( val )/FS_LAT_FACTOR)		//FS latitude conversion to degrees latitude (north positive,south negative)
#define		FS_LONGITUDE_DEG( val )	 ( ( val ) > 140737488355332) ? ( val )/FS_LON_FACTOR-360.0 : ( val )/FS_LON_FACTOR	//FS longitude conversion to degrees longitude (east positive,west negative)

#define		FS_VSPD_FT( val)		(METER_TO_FEET((val)/256.0)*60.0)//FS vertical speed in feet/min from 1/256 m/sec

#define		FS_GROUND_ALT_FT(val)	(METER_TO_FEET((val)/256.0))	//ground altittude in feet
#define		FS_PLANE_ALT_FT(val)	(METER_TO_FEET(val))			//plane altitude in feet
#define		FS_MACH(val)			(val*3.2/65536.0)
//trigonometric functions in degrees instead of radians
#define		DEG_SIN( val )	sin( ( val )/RADIANS_TO_DEGREE_FACTOR)					//sinus in degrees
#define		DEG_COS( val )	cos( ( val )/RADIANS_TO_DEGREE_FACTOR)					//cosine in degrees
#define		DEG_TAN( val )	tan( ( val )/RADIANS_TO_DEGREE_FACTOR)					//tangens in degrees
#define		DEG_ASIN( val )	RADIANS_TO_DEGREE_FACTOR * asin(( val ))				//arcsinus in degrees
#define		DEG_ACOS( val )	RADIANS_TO_DEGREE_FACTOR * acos(( val ))				//arccosine in degrees
#define		DEG_ATAN( val )	RADIANS_TO_DEGREE_FACTOR * atan(( val ))				//arctangens in degrees
#define		DEG_ATAN2( val1 ,val2 )	RADIANS_TO_DEGREE_FACTOR * atan2( val1,val2 )	//atan2 in degrees

#define		LBS_TO_KG(val)	POUND_TO_KILOGRAM((val))
#define		KG_TO_LBS(val)	KILOGRAM_TO_POUND((val))

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// macros to add/remove_imagedata_to_listelement
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define	HIDE_LISTELEMENT( pelement ,pos_element )			add_imagedata_to_listelement( pelement ,pos_element ,IMAGE_HIDDEN )
#define	SHOW_LISTELEMENT( pelement ,pos_element )			remove_imagedata_from_listelement( pelement ,pos_element ,IMAGE_HIDDEN )
#define	REDRAW_LISTELEMENT( pelement ,pos_element )		remove_imagedata_from_listelement( pelement ,pos_element ,IMAGE_ON_SCREEN )

#define	LIGHT_LISTELELMENT(pelement,pos_element)		add_imagedata_to_listelement( pelement ,pos_element ,IMAGE_USE_BRIGHT )
#define	DARKEN_LISTELEMENT(pelement,pos_element)		remove_imagedata_from_listelement( pelement ,pos_element ,IMAGE_USE_BRIGHT )
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//see also definiton of add_imagedata_to_listelement(...); remove_imagedata_to_listelement(...); lines 3054-3082
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
typedef enum ELEMENT_TYPE_ENUM {
        ELEMENT_TYPE_STATIC_IMAGE,
        ELEMENT_TYPE_NEEDLE,
        ELEMENT_TYPE_STRING,
        ELEMENT_TYPE_SLIDER,
        ELEMENT_TYPE_ICON,
        ELEMENT_TYPE_MOVING_IMAGE,
        ELEMENT_TYPE_SPRITE,
}
ELEMENT_TYPE_ENUM;
	
	
#define ASI_ALT_MANUALLY_TUNABLE                BIT0
#define ASI_HEADING_MANUALLY_TUNABLE            BIT1
	
	
typedef struct NONLINEARITY
{
        PIXPOINT        pt;
        FLOAT64         value;
        FLOAT64         degrees;
}
NONLINEARITY,*PNONLINEARITY,**PPNONLINEARITY;

typedef enum    FAILURE_KEY
{
        FAIL_NONE = 0,

        OLD_FAIL_SYSTEM_ELECTRICAL,			// obsolete: dont use it in new gauges; use FAIL_SYSTEM_ELECTRICAL_PANELS
        FAIL_SYSTEM_ENGINE,
        FAIL_SYSTEM_PITOT_STATIC,
        FAIL_SYSTEM_VACUUM,

        FAIL_GAUGE_ADF,
        FAIL_GAUGE_AIRSPEED,
        FAIL_GAUGE_ALTIMETER,
        FAIL_GAUGE_ATTITUDE,
        FAIL_GAUGE_COMMUNICATIONS,
        FAIL_GAUGE_FUEL_INDICATORS,
        FAIL_GAUGE_GYRO_HEADING,
        FAIL_GAUGE_MAGNETIC_COMPASS,
        FAIL_GAUGE_NAVIGATION,                 // this assumes no vors
        FAIL_GAUGE_NAVIGATION_VOR1,            // only vor1
        FAIL_GAUGE_NAVIGATION_VOR2,            // only vor2
        FAIL_GAUGE_NAVIGATION_BOTH,            // both vors
        FAIL_GAUGE_TRANSPONDER,
        FAIL_GAUGE_TURN_COORDINATOR,
        FAIL_GAUGE_VERTICAL_SPEED,

        FAIL_SYSTEM_ELECTRICAL_PANELS,
        FAIL_SYSTEM_ELECTRICAL_AVIONICS,

        FAIL_KEY_MAX
}
FAILURE_KEY,*PFAILURE_KEY,**PPFAILURE_KEY;
	
typedef enum    FAILURE_ACTION
{
        FAIL_ACTION_NONE = 0,
        FAIL_ACTION_FREEZE,
        FAIL_ACTION_ZERO,
        FAIL_ACTION_NO_DRAW,
        FAIL_ACTION_COVER,

        FAIL_ACTION_MAX
}
FAILURE_ACTION,*PFAILURE_ACTION,**PPFAILURE_ACTION;
	
typedef struct  FAILURE_RECORD
{
        FAILURE_KEY             key;
        FAILURE_ACTION          action;
        BOOL                    bInit;
}
FAILURE_RECORD,*PFAILURE_RECORD,**PPFAILURE_RECORD;
	
typedef struct  IMAGE_SET
{
        PIMAGE  final;
        PIMAGE  source;
        PIMAGE  reserved1;
        PIMAGE  reserved2;
} IMAGE_SET,*PIMAGE_SET,**PPIMAGE_SET;
	
	
#define PANEL_SERVICE_PRE_QUERY							0
#define PANEL_SERVICE_POST_QUERY						1
#define PANEL_SERVICE_PRE_INSTALL						2       // extra_data = resource_handle
#define PANEL_SERVICE_POST_INSTALL						3       // extra_data = resource_handle
#define PANEL_SERVICE_PRE_INITIALIZE					4
#define PANEL_SERVICE_POST_INITIALIZE					5
#define PANEL_SERVICE_PRE_UPDATE						6
#define PANEL_SERVICE_POST_UPDATE						7
#define PANEL_SERVICE_PRE_GENERATE						8       // extra_data = phase
#define PANEL_SERVICE_POST_GENERATE						9       // extra_data = phase
#define PANEL_SERVICE_PRE_DRAW							10
#define PANEL_SERVICE_POST_DRAW							11
#define PANEL_SERVICE_PRE_KILL							12
#define PANEL_SERVICE_POST_KILL							13
#define PANEL_SERVICE_CONNECT_TO_WINDOW					14      // extra_data = PANEL_WND
#define PANEL_SERVICE_DISCONNECT						15      // extra_data = PANEL_WND
#define PANEL_SERVICE_PANEL_OPEN						16
#define PANEL_SERVICE_PANEL_CLOSE						17
	
	
#define GAUGE_HEADER_VERSION_FS610						610    // FS 6.10 gauges
#define GAUGE_HEADER_VERSION_FSNEW						625    // OLD style gauges compiled in the FS70 TREE
#define GAUGE_HEADER_VERSION_CFS1						650    // CFS1 Gauges
#define GAUGE_HEADER_VERSION_FS700						700    // new FS7 gauges built after CFS1 is finalized
#define GAUGE_HEADER_VERSION_CFS2						750    // gauges built after FS2000 shipped and before CFS2 shipped
#define GAUGE_HEADER_VERSION_FS800						800    // FS2002 gauges	
#define GAUGE_HEADER_VERSION_FS900						900	   // FS2004 gauges


typedef struct GAUGEHDR *PGAUGEHDR,**PPGAUGEHDR;
typedef struct ELEMENT_HEADER *PELEMENT_HEADER,**PPELEMENT_HEADER;
typedef struct MOUSERECT *PMOUSERECT,**PPMOUSERECT;
	
typedef void FSAPI GAUGE_CALLBACK( PGAUGEHDR pgauge,int service_id,UINT32 extra_data );
typedef            GAUGE_CALLBACK  *PGAUGE_CALLBACK;
	
typedef struct  GAUGEHDR
{
	UINT32					gauge_header_version;
	char					*gauge_name;         
	PPELEMENT_HEADER		elements_list;               
	PQUERY_ROUTINE			query_routine;               
	PINSTALL_ROUTINE		install_routine;             
	PINITIALIZE_ROUTINE		initialize_routine;          
	PUPDATE_ROUTINE			update_routine;              
	PGENERATE_ROUTINE		generate_routine;            
	PDRAW_ROUTINE			draw_routine;                
	PKILL_ROUTINE			kill_routine;                
	PVOID					reserved1;
	UINT32					size_x_mm;           
	UINT32					size_y_mm;           
	FLOAT32					x_adjust;            
	FLOAT32					y_adjust;            
	PVOID					reserved2;              
	PVOID					reserved3;   
	PIXPOINT				position;
	PVOID					reserved4;             
	PMOUSERECT				mouse_rect;          
	PGAUGE_CALLBACK         gauge_callback;              
	UINT32					user_data;           
	char*					parameters;          
	char*					usage;               
	SINT32					reserved5;
	PVOID					reserved6;
	PIXPOINT				size;                           
	FLOAT64					user_area[10];
	FLAGS32					flags;
	PVOID					reserved7;
}
GAUGEHDR;
	
	
#define ELEMENT_INFO										\
	ELEMENT_TYPE_ENUM				element_type;           \
	ID								resource_id;            \
	PIXPOINT						position;				\
	PIXPOINT						previous_position;      \
	PIXPOINT						ofs;					\
	PGAUGEHDR						gauge_header;           \
	struct ELEMENT_HEADER			*previous_element;      \
	struct ELEMENT_HEADER			**next_element;         \
	PFAILURE_RECORD					failure_systems;        \
	FLAGS							image_flags;            \
	FLAGS							aircraft_special_instrumentation;\
	FLAGS							reserved;
	
	
typedef struct ELEMENT_HEADER
{
        ELEMENT_INFO
}
ELEMENT_HEADER;
	
	
#define HEADER union		\
{							\
	struct					\
	{						\
		ELEMENT_INFO		\
	};						\
	ELEMENT_HEADER header;	\
}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// new functions to change image flags in a linked list 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
EXTERN_C	PELEMENT_HEADER	FSAPI	get_listelement_pointer(PELEMENT_HEADER	pelement,UINT32	pos_element);
EXTERN_C	void	FSAPI	add_imagedata_to_listelement(PELEMENT_HEADER	pelement,UINT32	pos_element,FLAGS image_flags);
EXTERN_C	void	FSAPI	remove_imagedata_from_listelement(PELEMENT_HEADER	pelement,UINT32	pos_element,FLAGS image_flags);

#define		LISTELEM_DEFINE()																					\

	
typedef struct ELEMENT_STATIC_IMAGE *PELEMENT_STATIC_IMAGE,    **PPELEMENT_STATIC_IMAGE;
typedef struct ELEMENT_NEEDLE       *PELEMENT_NEEDLE,          **PPELEMENT_NEEDLE;
typedef struct ELEMENT_STRING           *PELEMENT_STRING,              **PPELEMENT_STRING;
typedef struct ELEMENT_SLIDER           *PELEMENT_SLIDER,              **PPELEMENT_SLIDER;
typedef struct ELEMENT_ICON                     *PELEMENT_ICON,                **PPELEMENT_ICON;
typedef struct ELEMENT_MOVING_IMAGE     *PELEMENT_MOVING_IMAGE,**PPELEMENT_MOVING_IMAGE;
typedef struct ELEMENT_SPRITE           *PELEMENT_SPRITE,              **PPELEMENT_SPRITE;

typedef FLOAT64 FSAPI NEEDLE_UPDATE_CALLBACK(PELEMENT_NEEDLE pelement);
typedef               NEEDLE_UPDATE_CALLBACK                  *PNEEDLE_UPDATE_CALLBACK;

typedef FLOAT64 FSAPI STRING_UPDATE_CALLBACK(PELEMENT_STRING pelement);
typedef               STRING_UPDATE_CALLBACK                  *PSTRING_UPDATE_CALLBACK;

typedef FLOAT64 FSAPI SLIDER_UPDATE_CALLBACK(PELEMENT_SLIDER pelement);
typedef               SLIDER_UPDATE_CALLBACK                  *PSLIDER_UPDATE_CALLBACK;

typedef FLOAT64 FSAPI ICON_UPDATE_CALLBACK(PELEMENT_ICON pelement);
typedef               ICON_UPDATE_CALLBACK                    *PICON_UPDATE_CALLBACK;

typedef FLOAT64 FSAPI MOVING_IMAGE_UPDATE_CALLBACK(PELEMENT_MOVING_IMAGE pelement);
typedef               MOVING_IMAGE_UPDATE_CALLBACK    *PMOVING_IMAGE_UPDATE_CALLBACK;

typedef FLOAT64 FSAPI SPRITE_UPDATE_CALLBACK(PELEMENT_SPRITE pelement);
typedef               SPRITE_UPDATE_CALLBACK                  *PSPRITE_UPDATE_CALLBACK;
	


typedef struct ELEMENT_STATIC_IMAGE
{
	HEADER;
	IMAGE_SET               image_data;
	PIMAGE                  save_buffer;
	HDC                     hdc;                                    // HDC for Win32 API DrawText
	HBITMAP                 hbmp;                                   // DibSection for Win32 API DrawText
	HBITMAP                 reserved1;
	PIMAGE                  reserved2;
}
ELEMENT_STATIC_IMAGE,*PELEMENT_STATIC_IMAGE,**PPELEMENT_STATIC_IMAGE;
	
#define	MAKE_STATIC(	NAME,														\
						RES_ID,														\
						NEXT_LIST,													\
						FAILURE,													\
						DRAW_FLAGS,													\
						ASI_FLAGS,													\
						POSITION_X,POSITION_Y )									\
																					\
	ELEMENT_STATIC_IMAGE		NAME	=											\
	{																				\
		ELEMENT_TYPE_STATIC_IMAGE,													\
		RES_ID,																		\
		POSITION_X,POSITION_Y,														\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(FAILURE),													\
		DRAW_FLAGS,																	\
		ASI_FLAGS,																	\
		0																			\
	};
	
	
	
typedef struct ELEMENT_NEEDLE
{
	HEADER;
	MODULE_VAR              source_var;             
	PMODULE_VAR_CB			source_var_cb;                                                              
	PFN_FLOAT64             display_value_fn;       
	PFN_FLOAT64             angle_fn;               
	PFN_FLOAT64             lag_fn;                 
	PIXPOINT                reference;              
	PNONLINEARITY			nonlinearity_table;             
	UINT32                  nonlinearity_table_size;
	SINT32                  max_degrees_per_tick;                                               
	FLOAT64                 readout_goal;                                                       
	FLOAT64                 theta_goal;                                                         
	FLOAT64                 theta;                                                              
	PIXPOINT                reference_old;                                                      
	PIXPOINT                o0,o1,o2,o3;                                                     
	IMAGE_SET               needle_data;                                                        
	PIMAGE                  texture_data;           
	UINT32                  texture_handle;                                                     
	PIMAGE                  save_buffer;            
	PIXPOINT                save_position;
	PNEEDLE_UPDATE_CALLBACK update_cb;
	PIXPOINT                save_size;
}
ELEMENT_NEEDLE,*PELEMENT_NEEDLE,**PPELEMENT_NEEDLE;

#define	MAKE_NEEDLE(	NAME,														\
						RES_ID,														\
						NEXT_LIST,													\
						FAILURE,													\
						DRAW_FLAGS,													\
						ASI_FLAGS,													\
						BKND_POSITION_X,BKND_POSITION_Y,							\
						NDL_POSITION_X,NDL_POSITION_Y,								\
						SOURCE_VAR,CALLBACK,										\
						NONLINEARITY_TABLE,											\
						MAX_DEG_PER_SEC )											\
																					\
	ELEMENT_NEEDLE				NAME	=											\
	{																				\
		ELEMENT_TYPE_NEEDLE,														\
		RES_ID,																		\
		BKND_POSITION_X,BKND_POSITION_Y,											\
		{0,0},																		\
		NDL_POSITION_X,NDL_POSITION_Y,												\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(FAILURE),													\
		DRAW_FLAGS,																	\
		ASI_FLAGS,																	\
		0,																			\
																					\
		{SOURCE_VAR},																\
		NULL,																		\
		NULL,																		\
		NULL,																		\
		NULL,																		\
		0,0,																		\
		NONLINEARITY_TABLE,															\
		sizeof( NONLINEARITY_TABLE )/sizeof( NONLINEARITY ),						\
		MAX_DEG_PER_SEC,															\
		0.0,																		\
		0.0,																		\
		0.0,																		\
		{0,0},																		\
		{0,0},{0,0},{0,0},{0,0},													\
		{0},																		\
		NULL,																		\
		0,																			\
		0,																			\
		{0,0},																		\
		CALLBACK,																	\
		{0,0},																		\
	};

	
	
typedef struct SEQ_REC
{
	SINT32 seq_id;
	SINT32 sel_str;
	SINT32 sel_end;
}
SEQ_REC,*PSEQ_REC,**PPSEQ_REC;

#define STR_UNSEL(ELEMENT)						((ELEMENT)->sel_end = -1,(ELEMENT)->sel_str = -1)
#define STR_SEL(ELEMENT,STR,END)				((ELEMENT)->sel_end = (END),(ELEMENT)->sel_str = (STR))
#define IS_STR_SEL(ELEMENT)						((ELEMENT)->sel_end != -1 && (ELEMENT)->sel_str != -1)
	
typedef struct ELEMENT_STRING
{
	HEADER;                                     
	MODULE_VAR              source_var[3];  
	FLOAT64                 save_var[3];    
	PMODULE_VAR_CB			source_var_cb[3];       
	PFN_FLOAT64             source_fn;      
	PCHAR                   string;         
	PCHAR                   string_old;     
	PIXPOINT                img_size;       
	PIMAGE                  string_data;    
	PIMAGE                  save_buffer;                                                
	COLORREF                fg_color;       
	COLORREF                bg_color;       
	COLORREF                hilite_color;   
	HDC						hdc;    
	HFONT                   hfont;          
	HFONT                   reserved1;      
	HBITMAP                 hbmp;           
	HBITMAP                 reserved2;       
	HBRUSH                  hbrush;         
	HBRUSH                  reserved3;     
	char                    font_name[32];  
	SINT32                  font_weight;    
	UINT8                   font_charset;   
	UINT32                  draw_text_flags;
	UINT16                  max_string_length;
	UINT16                  char_width;       
	SINT16                  sel_str;          
	SINT16                  sel_end;          
	PSEQ_REC				seq;              
	SINT32					save_global_seq;
	SINT32					char_height;
	PSTRING_UPDATE_CALLBACK update_cb;
	BOOL					use_user_color;
	COLORREF				user_color;
}
ELEMENT_STRING,*PELEMENT_STRING,**PPELEMENT_STRING;
	
#define	MAKE_STRING(	NAME,														\
						NEXT_LIST,													\
						FAILURE,													\
						DRAW_FLAGS,													\
						ASI_FLAGS,													\
						POSITION_X,POSITION_Y,										\
						SIZE_X,SIZE_Y,												\
						NUM_CHARS,													\
						SOURCE_VAR_1,												\
						SOURCE_VAR_2,												\
						SOURCE_VAR_3,												\
						FORECOLOR,													\
						BACKCOLOR,													\
						HILITECOLOR,												\
						FONT_NAME,													\
						FONT_WEIGHT,												\
						FONT_CHARSET,												\
						FONT_SIZE,													\
						DRAW_TEXT_FLAGS,											\
						HILITE_LIST,												\
						CALLBACK)													\
																					\
	ELEMENT_STRING				NAME	=											\
    {																				\
		ELEMENT_TYPE_STRING,														\
		-1,																			\
		POSITION_X,POSITION_Y,														\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(FAILURE),													\
		DRAW_FLAGS,																	\
		ASI_FLAGS,																	\
		0,																			\
																					\
		{{SOURCE_VAR_1},{SOURCE_VAR_2},{SOURCE_VAR_3}},							\
		{0,0,0},																	\
		{NULL,NULL,NULL},															\
		NULL,																		\
		NULL,																		\
		NULL,																		\
		SIZE_X,SIZE_Y,																\
		NULL,																		\
		NULL,																		\
		FORECOLOR,																	\
		BACKCOLOR,																	\
		HILITECOLOR,																\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		FONT_NAME,																	\
		FONT_WEIGHT,																\
		FONT_CHARSET,																\
		DRAW_TEXT_FLAGS,															\
		NUM_CHARS,																	\
		0,																			\
		0,																			\
		0,																			\
		HILITE_LIST,																\
		0,																			\
		FONT_SIZE,																	\
		CALLBACK,																	\
		FALSE,																		\
		0,																			\
	};
	
	
typedef struct ELEMENT_SLIDER
{
	HEADER;                              
	MODULE_VAR              source_var_x;
	PMODULE_VAR_CB			source_var_x_cb;     
	FLOAT64                 scale_x;     
	PFN_FLOAT64             lag_fn_x;                                             
	MODULE_VAR              source_var_y;
	PMODULE_VAR_CB			source_var_y_cb;     
	FLOAT64                 scale_y;     
	PFN_FLOAT64             lag_fn_y;    
	IMAGE_SET               slider_data; 
	PIMAGE                  save_buffer;
	PSLIDER_UPDATE_CALLBACK update_x_cb;
	PSLIDER_UPDATE_CALLBACK update_y_cb;
	FLOAT64 previous_x;
	FLOAT64 previous_y;
}
ELEMENT_SLIDER,*PELEMENT_SLIDER,**PPELEMENT_SLIDER;
	
#define	MAKE_SLIDER(	NAME,														\
						RES_ID,														\
						NEXT_LIST,													\
						FAILURE,													\
						DRAW_FLAGS,													\
						ASI_FLAGS,													\
						POSITION_X,POSITION_Y,										\
						SOURCE_VAR_X,CALLBACK_X,SCALE_X,							\
						SOURCE_VAR_Y,CALLBACK_Y,SCALE_Y )							\
																					\
	ELEMENT_SLIDER				NAME	=											\
	{																				\
		ELEMENT_TYPE_SLIDER,														\
		RES_ID,																		\
		POSITION_X,POSITION_Y,														\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(FAILURE),													\
		DRAW_FLAGS,																	\
		ASI_FLAGS,																	\
		0,																			\
																					\
		{SOURCE_VAR_X},																\
		NULL,																		\
		SCALE_X,																	\
		NULL,																		\
		{SOURCE_VAR_Y},																\
		NULL,																		\
		SCALE_Y,																	\
		NULL,																		\
		{0},																		\
		NULL,																		\
		CALLBACK_X,																	\
		CALLBACK_Y																	\
	};
	
	
	
// Icon Switch Types
#define ICON_SWITCH_TYPE_SET_CUR_ICON					0
#define ICON_SWITCH_TYPE_SET_CUR_USING_RANGE            1
#define ICON_SWITCH_TYPE_STEP_TO						3
#define ICON_SWITCH_TYPE_STEP_TO_USING_RANGE            4
	

typedef struct ELEMENT_ICON
{
	HEADER;                                
    MODULE_VAR				source_var;    
    PMODULE_VAR_CB			source_var_cb;         
    SINT32					switch_type;   
    SINT32					switch_count;  
    FLOAT64                 scale;         
    SINT32					offset;        
    SINT32					num_icons;     
    SINT32					cur_icon;      
    SINT32					dst_icon;      
    PIMAGE_SET              picon_data;    
    PIMAGE                  save_buffer;
    PICON_UPDATE_CALLBACK	update_cb;
    ID						reserved1;
}
ELEMENT_ICON,*PELEMENT_ICON,**PPELEMENT_ICON;
	
#define	MAKE_ICON(		NAME,														\
						RES_ID,														\
						NEXT_LIST,													\
						FAILURE,													\
						DRAW_FLAGS,													\
						ASI_FLAGS,													\
						POSITION_X,POSITION_Y,										\
						SOURCE_VAR,CALLBACK,										\
						SWITCH_TYPE,												\
						NUM_ICONS,													\
						SCALE,														\
						OFFSET )													\
																					\
    ELEMENT_ICON				NAME	=											\
    {																				\
    	ELEMENT_TYPE_ICON,															\
    	RES_ID,																		\
    	POSITION_X,POSITION_Y,														\
    	{0,0},																		\
    	{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
    	NULL,																		\
    	(PPELEMENT_HEADER)(NEXT_LIST),												\
    	(PFAILURE_RECORD)(FAILURE),													\
    	DRAW_FLAGS,																	\
    	ASI_FLAGS,																	\
    	0,																			\
																					\
		{SOURCE_VAR},																\
		NULL,																		\
		SWITCH_TYPE,																\
		0,																			\
		SCALE,																		\
		OFFSET,																		\
		NUM_ICONS,																	\
		0,																			\
		0,																			\
		NULL,																		\
		NULL,																		\
		CALLBACK,																	\
    };
	
	
	
typedef struct ELEMENT_MOVING_IMAGE
{
        HEADER;                                   
        MODULE_VAR			source_var_x;     
        PMODULE_VAR_CB		source_var_x_cb;          
        FLOAT64				min_x;            
        FLOAT64				max_x;            
        PFN_FLOAT64			lag_x;            
        MODULE_VAR			source_var_y;     
        PMODULE_VAR_CB		source_var_y_cb;          
        FLOAT64				min_y;            
        FLOAT64				max_y;            
        PFN_FLOAT64			lag_y;            
        FLOAT64				scale_x;          
        FLOAT64				scale_y;          
        FLOAT64				offset_x;         
        FLOAT64				offset_y;         
        PIXPOINT			save_position;    
        IMAGE_SET			image_data;       
        IMAGE_SET			local_mask_data;  
        PIMAGE				mask_data;        
        PIMAGE				save_buffer;
        PMOVING_IMAGE_UPDATE_CALLBACK update_x_cb;
        PMOVING_IMAGE_UPDATE_CALLBACK update_y_cb;
        FLOAT64				previous_x;
        FLOAT64				previous_y;
}
ELEMENT_MOVING_IMAGE,*PELEMENT_MOVING_IMAGE,**PPELEMENT_MOVING_IMAGE;
	
#define	MAKE_MOVING(	NAME,														\
						RES_ID,														\
						NEXT_LIST,													\
						FAILURE,													\
						DRAW_FLAGS,													\
						ASI_FLAGS,													\
						POSITION_X,POSITION_Y,										\
						SOURCE_VAR_X,CALLBACK_X,									\
						MIN_X,MAX_X,												\
						SOURCE_VAR_Y,CALLBACK_Y,									\
						MIN_Y,MAX_Y )												\
																					\
	ELEMENT_MOVING_IMAGE		NAME	=											\
	{																				\
		ELEMENT_TYPE_MOVING_IMAGE,													\
		RES_ID,																		\
		POSITION_X,POSITION_Y,														\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(FAILURE),													\
		DRAW_FLAGS,																	\
		ASI_FLAGS,																	\
		0,																			\
																					\
		{SOURCE_VAR_X},																\
		NULL,																		\
		MIN_X,																		\
		MAX_X,																		\
		NULL,																		\
		{SOURCE_VAR_Y},																\
		NULL,																		\
		MIN_Y,																		\
		MAX_Y,																		\
		NULL,																		\
		0.0,																		\
		0.0,																		\
		0.0,																		\
		0.0,																		\
		{0,0},																		\
		{0},																		\
		{0},																		\
		NULL,																		\
		NULL,																		\
		CALLBACK_X,																	\
		CALLBACK_Y																	\
	};
	
	
	
typedef struct ELEMENT_SPRITE
{
	HEADER;
	MODULE_VAR              source_var_x;      
	PMODULE_VAR_CB			source_var_x_cb;           
	FLOAT64                 scale_x;           
	MODULE_VAR              source_var_y;      
	PMODULE_VAR_CB			source_var_y_cb;           
	FLOAT64                 scale_y;           
	MODULE_VAR              source_var_0;      
	PMODULE_VAR_CB			source_var_0_cb;           
	FLOAT64                 scale_0;           
	FLOAT32					texture_scale_x; 
	FLOAT32					texture_scale_y; 
	PIXPOINT                reference;         
	PIXPOINT                reference_old;     
	PIXPOINT                o0,o1,o2,o3;    
	PIXPOINT                os0,os1,os2,os3;
	IMAGE_SET               sprite_data;       
	PIMAGE                  texture_data;      
	UINT32                  texture_handle;    
	IMAGE_SET               local_mask_data;   
	PIMAGE                  mask_data;         
	PIMAGE                  save_buffer;
	PSPRITE_UPDATE_CALLBACK update_x_cb;
	PSPRITE_UPDATE_CALLBACK update_y_cb;
	PSPRITE_UPDATE_CALLBACK update_0_cb;
	FLOAT64					previous_x;
	FLOAT64					previous_y;
	FLOAT64					previous_0;
}
ELEMENT_SPRITE,*PELEMENT_SPRITE,**PPELEMENT_SPRITE;

#define	MAKE_SPRITE(	NAME,														\
						RES_ID,														\
						NEXT_LIST,													\
						FAILURE,													\
						DRAW_FLAGS,													\
						ASI_FLAGS,													\
						BKND_POSITION_X,BKND_POSITION_Y,							\
						TEXTURE_CENTER_X,TEXTURE_CENTER_Y,							\
						TEXTURE_SCALE_X,TEXTURE_SCALE_Y,							\
						SOURCE_VAR_X,CALLBACK_X,SCALE_X,							\
						SOURCE_VAR_Y,CALLBACK_Y,SCALE_Y,							\
						SOURCE_VAR_0,CALLBACK_0,SCALE_0 )							\
																					\
	ELEMENT_SPRITE				NAME	=											\
	{																				\
		ELEMENT_TYPE_SPRITE,														\
		RES_ID,																		\
		{BKND_POSITION_X,BKND_POSITION_Y},											\
		{0,0},																		\
		{TEXTURE_CENTER_X,TEXTURE_CENTER_Y},										\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(FAILURE),													\
		DRAW_FLAGS,																	\
		ASI_FLAGS,																	\
		0,																			\
																					\
		{SOURCE_VAR_X},																\
		NULL,																		\
		SCALE_X,																	\
		{SOURCE_VAR_Y},																\
		NULL,																		\
		SCALE_Y,																	\
		{SOURCE_VAR_0},																\
		NULL,																		\
		SCALE_0,																	\
		(FLOAT32)TEXTURE_SCALE_X,(FLOAT32)TEXTURE_SCALE_Y,							\
		{0,0},																		\
		{0,0},																		\
		{0,0},{0,0},{0,0},{0,0},													\
		{0,0},{0,0},{0,0},{0,0},													\
		{0},																		\
		NULL,																		\
		0,																			\
		{0},																		\
		NULL,																		\
		NULL,																		\
		CALLBACK_X,																	\
		CALLBACK_Y,																	\
		CALLBACK_0																	\
	};



// defines for mouse sense rect stuff
typedef	enum
{
	MOUSE_RECT_EOL,
	MOUSE_RECT_PARENT,
	MOUSE_RECT_CHILD,
	MOUSE_RECT_END_PARENT,
	MOUSE_RECT_USER,
} MOUSE_RECT_TYPE;

typedef	enum
{
	CURSOR_NONE = -1,
	CURSOR_NORMAL = 0,
	CURSOR_UPARROW,
	CURSOR_DOWNARROW,
	CURSOR_LEFTARROW,
	CURSOR_RIGHTARROW,
	CURSOR_HAND,
	CURSOR_CROSSHAIR,
	CURSOR_GRAB,
} CURSOR_TYPE;
	

typedef struct	MOUSECALLBACK
{
	PIXPOINT	relative_point;
	PVOID		user_data;
	PMOUSERECT	mouse;
	PIXPOINT	screen_point;		// window client coordinates of the click
	PVOID		reserved;
} MOUSECALLBACK,*PMOUSECALLBACK,**PPMOUSECALLBACK;

// arg1 is actually pointer to MOUSECALLBACK,but for compatibility is defined here as a pointer to PIXPOINT
typedef	BOOL FSAPI	MOUSE_FUNCTION( PPIXPOINT arg1,FLAGS32 mouse_flags );		
typedef				MOUSE_FUNCTION		*PMOUSE_FUNCTION;

#define GAUGEHDR_FOR_MOUSE_CALLBACK(PIXPOINT) ((PGAUGEHDR)(((PMOUSECALLBACK)(PIXPOINT))->user_data))

typedef	struct	MOUSERECT
{
	MOUSE_RECT_TYPE	rect_type;			// type of mouse rectangle (parent,child,eol)
	PIXBOX			relative_box;		// relative sense rectangle (relative to parameter to register function)
	CURSOR_TYPE		cursor;				// cursor to display when over this window
	ID				help_id;			// pop-up help id
	FLAGS			mouse_flags;		// types of mouse activities to look for
	ID				event_id;			// event to generate if mouse_flags is satisfied (implies simple mouse_flags)
	PMOUSE_FUNCTION	mouse_function;		// function to call if mouse_flag is satisfied
	PVOID			api_data;			// data for FS6API's use
} MOUSERECT;


#define	HELP_NONE			0

// Mouse rectangle macros

#define MOUSE_RIGHTSINGLE	BIT31
#define MOUSE_MIDDLESINGLE	BIT30
#define MOUSE_LEFTSINGLE	BIT29
#define MOUSE_RIGHTDOUBLE	BIT28
#define MOUSE_MIDDLEDOUBLE	BIT27
#define MOUSE_LEFTDOUBLE	BIT26
#define MOUSE_RIGHTDRAG 	BIT25
#define MOUSE_MIDDLEDRAG	BIT24
#define MOUSE_LEFTDRAG		BIT23
#define MOUSE_MOVE			BIT22
#define MOUSE_DOWN_REPEAT	BIT21
#define MOUSE_RIGHTRELEASE	BIT19
#define MOUSE_MIDDLERELEASE BIT18
#define MOUSE_LEFTRELEASE	BIT17
#define MOUSE_WHEEL_FLIP	BIT16	// invert direction of mouse wheel
#define MOUSE_WHEEL_SKIP	BIT15	// look at next 2 rect for mouse wheel commands
#define MOUSE_WHEEL_UP		BIT14
#define MOUSE_WHEEL_DOWN	BIT13
#define MOUSE_MOVE_REPEAT	BIT12	// if used with MOUSE_*DRAG generates MOVE_MOVE every so often
#define MOUSE_LEAVE			BIT11	// to be used with MOUSE_DOWN_REPEAT
#define MOUSE_GETALL		BIT10	// used to get all mouse messages happend over the rect
#define MOUSE_LEFTALL		(MOUSE_LEFTSINGLE | MOUSE_LEFTDRAG | MOUSE_LEFTRELEASE)
#define MOUSE_RIGHTALL		(MOUSE_RIGHTSINGLE | MOUSE_RIGHTDRAG | MOUSE_RIGHTRELEASE)
#define MOUSE_MIDDLEALL 	(MOUSE_MIDDLESINGLE | MOUSE_MIDDLEDRAG | MOUSE_MIDDLERELEASE)
#define MOUSE_WHEEL			(MOUSE_WHEEL_DOWN|MOUSE_WHEEL_UP)

#define	MOUSE_NONE			0

#define MOUSE_MAXIMUM_TOOLTIP_TEXT 80

#define	MOUSE_PARENT_BEGIN( x,y,w,h,helpid )						{MOUSE_RECT_PARENT,{x,y,w,h},CURSOR_NONE,helpid,MOUSE_NONE,0,NULL,NULL},

#define	MOUSE_PARENT_END												{MOUSE_RECT_END_PARENT,{0,0,0,0},CURSOR_NONE,HELP_NONE,MOUSE_NONE,0,NULL,NULL},

#define	MOUSE_PARENT( x,y,w,h,helpid )								MOUSE_PARENT_BEGIN( x,y,w,h,helpid )					\
																		MOUSE_PARENT_END

#define	MOUSE_BEGIN( name,helpid,x,y )								MOUSERECT	name[]	= {										\
																		MOUSE_PARENT_BEGIN( x,y,0,0,helpid )

#define	MOUSE_END														MOUSE_PARENT_END											\
																		{MOUSE_RECT_EOL,{0,0,0,0},CURSOR_NONE,HELP_NONE,MOUSE_NONE,0,NULL,NULL}};

#define	MOUSE_CHILD_EVENT( x,y,w,h,cursor,mouse_flags,event_id )	{MOUSE_RECT_CHILD,{x,y,w,h},cursor,helpid,mouse_flags,event_id,NULL,NULL},
#define	MOUSE_CHILD_FUNCT( x,y,w,h,cursor,mouse_flags,function )	{MOUSE_RECT_CHILD,{x,y,w,h},cursor,HELP_NONE,mouse_flags,0,function,NULL},
#define	MOUSE_CHILD_FUNCT2( x,y,w,h,cursor,mouse_flags,helpid,function )	{MOUSE_RECT_CHILD,{x,y,w,h},cursor,helpid,mouse_flags,0,function,NULL},




	
// Dynamic tooltips' stuff

#define MOUSE_TOOLTIP_TEXT_MACRO(HELPID,HELPID_METRIC,HELPID_US,STRING,ARGS,NARG)	\
							{MOUSE_RECT_USER,{0,0,(HELPID_METRIC),(HELPID_US)},CURSOR_NONE,(HELPID),MOUSE_NONE,(ID)(NARG),(BOOL (FSAPI *)(PPIXPOINT ,FLAGS32))(PVOID)(STRING),(ARGS)}
#define MOUSE_TOOLTIP_TEXT(HELPID,HELPID_METRIC,HELPID_US,STRING,ARGS,NARG)	MOUSE_TOOLTIP_TEXT_MACRO(HELPID,HELPID_METRIC,HELPID_US,STRING,ARGS,NARG),
#define MOUSE_TOOLTIP_TEXT_3ID_NARG(HELPID,HELPID_METRIC,HELPID_US,ARGS,NARG)	MOUSE_TOOLTIP_TEXT (HELPID,HELPID_METRIC,HELPID_US,NULL,ARGS,NARG)
#define MOUSE_TOOLTIP_TEXT_ID_NARG(HELPID,ARGS,NARG)								MOUSE_TOOLTIP_TEXT_3ID_NARG ((HELPID),HELP_NONE,HELP_NONE,(ARGS),(NARG))
#define MOUSE_TOOLTIP_TEXT_STRING_NARG(STRING,ARGS,NARG)							MOUSE_TOOLTIP_TEXT (HELP_NONE,HELP_NONE,HELP_NONE,STRING,ARGS,NARG)
#define MOUSE_TOOLTIP_TEXT_ID(HELPID,ARGS)											MOUSE_TOOLTIP_TEXT_ID_NARG ((HELPID),(ARGS),sizeof(ARGS)/sizeof(MOUSE_ARG))
#define MOUSE_TOOLTIP_TEXT_STRING(STRING,ARGS)										MOUSE_TOOLTIP_TEXT_STRING_NARG ((STRING),(ARGS),sizeof(ARGS)/sizeof(MOUSE_ARG))
#define MOUSE_TOOLTIP_STRING(STRING)												MOUSE_TOOLTIP_TEXT_STRING_NARG ((STRING),NULL,0)
#define MOUSE_TOOLTIP_ID(HELPID)													MOUSE_TOOLTIP_TEXT_ID_NARG ((HELPID),NULL,0)
#define MOUSE_TOOLTIP_3ID(HELPID,HELPID_METRIC,HELPID_US)							MOUSE_TOOLTIP_TEXT_3ID_NARG ((HELPID),(HELPID_METRIC),(HELPID_US),NULL,0)
#define MOUSE_TOOLTIP_STANDARD_STRING(STRING)										MOUSE_TOOLTIP_TEXT_STRING (STRING,0)

#define MAKE_MOUSE_TOOLTIP_STANDARD_STRING(PARAMETER,UNITS)						"@" ## #PARAMETER ## "@" ## #UNITS
#define MOUSE_TOOLTIP_STANDARD(PARAMETER,UNITS)									MOUSE_TOOLTIP_STANDARD_STRING (MAKE_MOUSE_TOOLTIP_STANDARD_STRING (PARAMETER,UNITS))
	
	
typedef struct {
        FLOAT64 source;
        FLOAT64 value;
} MOUSE_ARG_NUMERIC_MAP;
typedef struct {
        SINT32 source;
        ID value;
} MOUSE_ARG_ID_MAP;
typedef struct {
        SINT32 source;
        PSTRINGZ value;
} MOUSE_ARG_STRING_MAP;

typedef struct {
        MODULE_VAR		source_var;
        FLOAT64			scale;
        MOUSE_ARG_NUMERIC_MAP *numeric_table;
        UINT			numeric_table_size;
        MOUSE_ARG_ID_MAP *id_table;
        UINT			id_table_size;
        MOUSE_ARG_STRING_MAP *string_table;
        UINT			string_table_size;
        FLOAT64			(FSAPI *numeric_callback) (FLOAT64 number,ID id,PCSTRINGZ string,MODULE_VAR *source_var,PGAUGEHDR gauge);
        ID				(FSAPI *id_callback) (FLOAT64 number,ID id,PCSTRINGZ string,MODULE_VAR *source_var,PGAUGEHDR gauge);
        PCSTRINGZ		(FSAPI *string_callback) (FLOAT64 number,ID id,PCSTRINGZ string,MODULE_VAR *source_var,PGAUGEHDR gauge);
} MOUSE_ARG;

#define MOUSE_TOOLTIP_ARGS(NAME)        MOUSE_ARG (NAME)[] = {
#define MOUSE_TOOLTIP_ARG(VAR,SCALE,NUMERIC_TABLE,ID_TABLE,STRING_TABLE,NUMERIC_CALLBACK,ID_CALLBACK,STRING_CALLBACK) \
                {{(VAR)},(SCALE),\
                 (NUMERIC_TABLE),sizeof(NUMERIC_TABLE)/sizeof(MOUSE_ARG_NUMERIC_MAP),\
                 (ID_TABLE),sizeof(ID_TABLE)/sizeof(MOUSE_ARG_ID_MAP),\
                 (STRING_TABLE),sizeof(STRING_TABLE)/sizeof(MOUSE_ARG_STRING_MAP),\
                 (NUMERIC_CALLBACK),(ID_CALLBACK),(STRING_CALLBACK)},
#define MOUSE_TOOLTIP_ARG_NUMBER(VAR,SCALE) MOUSE_TOOLTIP_ARG((VAR),(SCALE),NULL,NULL,NULL,NULL,NULL,NULL)
#define MOUSE_TOOLTIP_ARG_HELP_ID(VAR,ID_TABLE) MOUSE_TOOLTIP_ARG((VAR),1,NULL,(ID_TABLE),NULL,NULL,NULL,NULL)
#define MOUSE_TOOLTIP_ARG_NUMBER_FUNCT(VAR,FUNCT) MOUSE_TOOLTIP_ARG((VAR),1,NULL,NULL,NULL,(FUNCT),NULL,NULL)
#define MOUSE_TOOLTIP_ARG_HELP_ID_FUNCT(VAR,FUNCT) MOUSE_TOOLTIP_ARG((VAR),1,NULL,NULL,NULL,NULL,(FUNCT),NULL)
#define MOUSE_TOOLTIP_ARGS_END          };

////////////////////////////////////////////////////////////////////////////////

#define FS8LINK_VERSION 	0x0800

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
	

#define FS9LINK_VERSION 	0x0900

struct PANELS
{
	FSLINKAGE	head;
	void   		(FSAPI *reserved1) (void);	
	void   		(FSAPI *reserved2) (void);	
	void   		(FSAPI *reserved3) (void);	
	void   		(FSAPI *reserved4) (void);	
	void   		(FSAPI *reserved5) (void);	
	BOOL		(FSAPI *is_panel_window_visible_ident) (UINT32 panel_id);
	ENUM   		(FSAPI *tooltip_units_getset) (int action,ENUM type);	// action < 0 - toggle(ignore 'type'),= 0 get(ignore 'type'),> 0 set(use 'type')
	void   		(FSAPI *reserved7) (void);	
	void   		(FSAPI *reserved8) (void);	
	void   		(FSAPI *reserved9) (void);	
	void   		(FSAPI *reserved10) (void);	
	void   		(FSAPI *reserved11) (void);	
	void   		(FSAPI *reserved12) (void);	
	void   		(FSAPI *reserved13) (void);	
	void   		(FSAPI *reserved14) (void);	
	void   		(FSAPI *reserved15) (void);	
	void   		(FSAPI *reserved16) (void);	
	void   		(FSAPI *reserved17) (void);	
	void   		(FSAPI *element_list_query) ( PELEMENT_HEADER element );	
	void   		(FSAPI *element_list_install) ( PELEMENT_HEADER element,PVOID resource_file_handle );	
	void   		(FSAPI *element_list_initialize) ( PELEMENT_HEADER element );	
	void   		(FSAPI *element_list_update) ( PELEMENT_HEADER element );	
	void   		(FSAPI *element_list_generate) ( PELEMENT_HEADER element,GENERATE_PHASE phase );	
	void   		(FSAPI *element_list_plot) ( PELEMENT_HEADER element );	
	void   		(FSAPI *element_list_erase) ( PELEMENT_HEADER element );	
	void   		(FSAPI *element_list_kill) ( PELEMENT_HEADER element );	
	void   		(FSAPI *mouse_list_install) ( PMOUSERECT rect,PGAUGEHDR gauge_header,PPIXPOINT size );	
	void   		(FSAPI *mouse_list_register) ( PMOUSERECT rect,PGAUGEHDR gauge_header );	
	void   		(FSAPI *mouse_list_unregister) ( PMOUSERECT rect,PGAUGEHDR gauge_header );	
	BOOL   		(FSAPI *panel_window_toggle) (UINT32 panel_id);	
	ERR         (FSAPI *trigger_key_event) ( ID32 event_id,UINT32 value );	
	void   		(FSAPI *register_var_by_name) ( PVOID var,VAR_TYPE var_type,PSTRINGZ name );	
	void   		(FSAPI *initialize_var) ( PMODULE_VAR module_var );	
	void   		(FSAPI *initialize_var_by_name) ( PMODULE_VAR module_var,PSTRINGZ name );	
	void   		(FSAPI *lookup_var) ( PMODULE_VAR module_var );	
	void   		(FSAPI *unregister_var_by_name) ( PSTRINGZ name );	
	void   		(FSAPI *unregister_all_named_vars) (void);	
	void   		(FSAPI *reserved18) (void);	
	void   		(FSAPI *reserved19) (void);	
	BOOL   		(FSAPI *panel_window_close_ident) (UINT32 panel_id);	
	BOOL   		(FSAPI *panel_window_open_ident) (UINT32 panel_id);	
	void   		(FSAPI *panel_window_toggle_hud_color) (void);	
	void   		(FSAPI *panel_window_toggle_hud_units) (void);	
	void   		(FSAPI *radio_stack_popup) (void);	
	void   		(FSAPI *radio_stack_autoclose) (void);	
	ID			(FSAPI *check_named_variable) (PCSTRINGZ name);
	ID			(FSAPI *register_named_variable) (PCSTRINGZ name);
	FLOAT64 	(FSAPI *get_named_variable_value) (ID id);
	FLOAT64 	(FSAPI *get_named_variable_typed_value) (ID id,ENUM units);
	void		(FSAPI *set_named_variable_value) (ID id,FLOAT64 value);
	void		(FSAPI *set_named_variable_typed_value) (ID id,FLOAT64 value,ENUM units);
	void   		(FSAPI *reserved26) (void);	
	void   		(FSAPI *reserved27) (void);	
	PCSTRINGZ 	(FSAPI *get_name_of_named_variable) (ID id);
	void   		(FSAPI *reserved29) (void);	
	PCSTRINGZ	(FSAPI *panel_resource_string_get) (ID32 id);	
	BOOL   		(FSAPI *panel_window_toggle_menu_id) (ID32 menu_id);	
	void   		(FSAPI *reserved30) (void);	
	void   		(FSAPI *reserved31) (void);	
	void   		(FSAPI *element_use_color) (PELEMENT_HEADER element,BOOL override,UINT32 color);	// color actually is  COLORREF
	void		(FSAPI *set_gauge_flags) (PCSTRINGZ name,FLAGS32 newflags);		// newlags is GAUGE_FLAG_NORMAL,GAUGE_FLAG_HIDDEN... 
	FLAGS32		(FSAPI *get_gauge_flags) (PCSTRINGZ name);
	BOOL		(FSAPI *gauge_calculator_code_precompile) (PCSTRINGZ* pCompiled,UINT32* pCompiledSize,PCSTRINGZ source);
	BOOL		(FSAPI *execute_calculator_code) (PCSTRINGZ code,FLOAT64* fvalue,SINT32* ivalue,PCSTRINGZ* svalue);
	BOOL		(FSAPI *format_calculator_string) (PSTRINGZ result,UINT32 resultsize,PCSTRINGZ format);
	BOOL		(FSAPI *reserved32) (void);
	BOOL		(FSAPI *reserved33) (void);
	ENUM		(FSAPI *get_units_enum) (PCSTRINGZ unitname);
	ENUM		(FSAPI *get_aircraft_var_enum) (PCSTRINGZ simvar);
	FLOAT64 	(FSAPI *aircraft_varget) (ENUM simvar,ENUM units,SINT32 index);
	BOOL		(FSAPI *panel_register_c_callback) (PCSTRINGZ name,IPanelCCallback* pcallback);
	IPanelCCallback* (FSAPI *panel_get_registered_c_callback) (PCSTRINGZ name);
	IAircraftCCallback* (FSAPI *panel_get_aircraft_c_callback) (PCSTRINGZ name);
	void		(FSAPI *send_key_event) (ID32 event_id,UINT32 value);
	void		(FSAPI *register_key_event_handler) (GAUGE_KEY_EVENT_HANDLER handler,PVOID userdata);
	void		(FSAPI *unregister_key_event_handler) (GAUGE_KEY_EVENT_HANDLER handler,PVOID userdata);
};

#if defined(_MSC_VER) 			
#pragma warning( push )
#pragma warning( disable:4200 )
#endif

// This is the GAUGES' export table definition FSCK!!!!!
typedef struct  GAUGESLINKAGE
{
	FS6LINK Link;
	PGAUGEHDR		gauge_header_ptr[];
} GAUGESLINKAGE,*PGAUGESLINKAGE,**PPGAUGESLINKAGE;

#if defined(_MSC_VER) 
#pragma warning( pop )
#endif

EXTERN_C DECLSPEC_EXPORT	GAUGESLINKAGE	Linkage;

// Old gauge functions
static void	FSAPI query_routine_610( void )
{
//    ImportTable.pPanels->element_list_query( *Linkage.gauge_header_ptr[0]->elements_list );
}

static void	FSAPI install_routine_610( PVOID resource_file_handle )
{
//    element_list_install( *Linkage.gauge_header_ptr[0]->elements_list,resource_file_handle );	
}

static void	FSAPI initialize_routine_610( void )
{
//    element_list_initialize( *Linkage.gauge_header_ptr[0]->elements_list );
}

static void	FSAPI update_routine_610( void )
{
//   element_list_update(*Linkage.gauge_header_ptr[0]->elements_list);
}

static void	FSAPI generate_routine_610( GENERATE_PHASE phase )
{
//    element_list_generate( *Linkage.gauge_header_ptr[0]->elements_list,phase );
}

static void	FSAPI draw_routine_610( void )
{
//    element_list_plot( *Linkage.gauge_header_ptr[0]->elements_list );
}

static void	FSAPI kill_routine_610( void )
{
//    element_list_kill( *Linkage.gauge_header_ptr[0]->elements_list );
}

#define GAUGE_HEADER_FS800(				\
				gaugehdr_var_name,		\
				default_size_mm,		\
				gauge_name,				\
				element_list,			\
				pmouse_rect,			\
				pgauge_callback,		\
				user_data,				\
				usage)					\
	GAUGEHDR gaugehdr_var_name =		\
	{									\
		GAUGE_HEADER_VERSION_FS900,		\
		gauge_name,						\
		element_list,					\
		query_routine_610,				\
		install_routine_610,			\
		initialize_routine_610,			\
		update_routine_610,				\
		generate_routine_610,			\
		draw_routine_610,				\
		kill_routine_610,				\
		NULL,							\
		default_size_mm,				\
		0,								\
		0.0,							\
		0.0,							\
		NULL,							\
		NULL,							\
		{0,0},							\
		NULL,							\
		pmouse_rect,					\
		pgauge_callback,				\
		user_data,						\
		NULL,							\
		usage,							\
		0,								\
		NULL,							\
		{0,0},							\
		{0.0},							\
	};

#define GAUGE_HEADER_FS700(				\
				default_size_mm,		\
				gauge_name,				\
				element_list,			\
				pmouse_rect,			\
				pgauge_callback,		\
				user_data,				\
				parameters,				\
				usage)					\
		GAUGE_HEADER_FS800(				\
				GAUGEHDR_VAR_NAME,		\
				default_size_mm,		\
				gauge_name,				\
				element_list,			\
				pmouse_rect,			\
				pgauge_callback,		\
				user_data,				\
				usage)

#pragma pack()

#endif

