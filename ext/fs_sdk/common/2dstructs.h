/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: 2dstructs.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef __FS_SDK_2DSTRUCTS
#define __FS_SDK_2DSTRUCTS

#pragma pack(4)

typedef SINT32  UNIPIX, *PUNIPIX, **PPUNIPIX;   // universal pixel (X=0-8191    Y=0-6143}
typedef SINT32  PIXEL, *PPIXEL, **PPPIXEL;      // screen pixel
typedef VAR32  UNICOL, *PUNICOL, **PPUNICOL;    // universal color code

typedef struct  RGB15
{
    UINT16      blue:5;
    UINT16      green:5;
    UINT16      red:5;
    UINT16      alpha:1;
} RGB15, *PRGB15, **PPRGB15;

typedef struct  RGB16
{
    UINT16      blue:5;
    UINT16      green:6;
    UINT16      red:5;
} RGB16, *PRGB16, **PPRGB16;


typedef struct  RGB24
{
    UINT8       blue;
    UINT8       green;
    UINT8       red;
} RGB24, *PRGB24, **PPRGB24;

typedef struct  RGB32
{
    UINT8       blue;
    UINT8       green;
    UINT8       red;
    UINT8       junk;
} RGB32, *PRGB32, **PPRGB32;

typedef struct  RGBA
{
    UINT8       blue;
    UINT8       green;
    UINT8       red;
    UINT8       alpha;
} RGBA, *PRGBA, **PPRGBA;

typedef struct  RGB         // this is used by the 2d system and stuff
{                           // note that red and blue are backwards from
    UINT8       red;        // what windows expects
    UINT8       green;
    UINT8       blue;
} RGB, *PRGB, **PPRGB;

typedef struct  RGBAF
{
    FLOAT32     r;
    FLOAT32     g;
    FLOAT32     b;
    FLOAT32     a;
} RGBAF, *PRGBAF;

typedef struct  XY
{
    SINT32  x;
    SINT32  y;
} XY, *PXY, **PPXY;

typedef struct  UNIPOINT
{
    UNIPIX  x;
    UNIPIX  y;
} UNIPOINT, *PUNIPOINT, **PPUNIPOINT;

typedef struct  UNIBOX
{
    UNIPIX  x;
    UNIPIX  y;
    UNIPIX  size_x;
    UNIPIX  size_y;
} UNIBOX, *PUNIBOX, **PPUNIBOX;

typedef struct  UNIRECT
{
    UNIPIX  left;
    UNIPIX  top;
    UNIPIX  right;
    UNIPIX  bottom;
} UNIRECT, *PUNIRECT, **PPUNIRECT;

typedef struct  PIXPOINT
{
    PIXEL x;
    PIXEL y;
} PIXPOINT, *PPIXPOINT, **PPPIXPOINT;

typedef const PIXPOINT *PCPIXPOINT, **PPCPIXPOINT;

typedef struct  PIXBOX
{
    PIXEL x;
    PIXEL y;
    PIXEL size_x;
    PIXEL size_y;
} PIXBOX, *PPIXBOX, **PPPIXBOX;

typedef struct  PIXRECT
{
    PIXEL left;
    PIXEL top;
    PIXEL right;
    PIXEL bottom;
} PIXRECT, *PPIXRECT, **PPPIXRECT;


typedef struct  BOUNDSRECT
{
    SIF48   N;                  //  north boundary
    SIF48   S;                  //  south boundary
    ANGL48  E;                  // east boundary
    ANGL48  W;                  // west boundary

} BOUNDSRECT, *PBOUNDSRECT, **PPBOUNDSRECT;


typedef struct
{
    SINT32  north;              // north boundary
    SINT32  south;              // south boundary
    SINT32  east;               // east boundary
    SINT32  west;               // west boundary

} BOUNDSRECT32, *PBOUNDSRECT32, **PPBOUNDSRECT32;

#pragma pack()

#endif
