/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: awind.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef __FS_SDK_AWIND
#define __FS_SDK_AWIND

#pragma pack(4)

enum WINDOW_TYPE {
  WINDOW_TYPE_ALL = 0x0,
  WINDOW_TYPE_MISC = 0x1,
  WINDOW_TYPE_PANEL = 0x2,
  WINDOW_TYPE_VIEW = 0x3,
  WINDOW_TYPE_MAP = 0x4,
  WINDOW_TYPE_MESSAGE = 0x5,
  WINDOW_TYPE_AUX_VIEW = 0x6,
};

enum SERVICE_FUNCTION_CODE {
  SERVICE_UPDATE = 0x0,
  SERVICE_SIZE = 0x1,
  SERVICE_MOVE = 0x2,
  SERVICE_OPEN = 0x3,
  SERVICE_CLOSED = 0x4,
  SERVICE_HITTEST = 0x5,
  SERVICE_OFFSCREEN_CHANGED = 0x6,
};

enum VIEW_MODE {
  VIEW_MODE_INVALID = 0x0,
  VIEW_MODE_COCKPIT = 0x1,
  VIEW_MODE_VIRTUAL_COCKPIT = 0x2,
  VIEW_MODE_TOWER = 0x3,
  VIEW_MODE_SPOT = 0x4,
  VIEW_MODE_MAP = 0x5,
  VIEW_MODE_TRACK = 0x6,
  VIEW_MODE_MAX = 0x7,
  VIEW_MODE_FIRST = 0x1,
  VIEW_MODE_LAST = 0x4,
};

typedef struct MENUITEM {
    unsigned short ParentID;
    unsigned short MenuID;
    unsigned int StringID;
    unsigned int Flags;
    struct MENUITEM* Child;
    void* APIData;
}MENUITEM,*PMENUITEM,**PPMENUITEM;

typedef struct VIEWBLOCK {
	int size_x;
	int bias_x;
	int size_y;
	int bias_y;
	int full_size_x;
	int full_bias_x;
	int full_size_y;
	int full_bias_y;
	VIEW_MODE view_mode;
	VIEW_DIR view_dir;
	int view_change;
	unsigned short zoom_cockpit;
	unsigned short zoom_virtual;
	unsigned short zoom_tower;
	unsigned short zoom_track;
	unsigned short zoom_spot;
	unsigned short zoom_map;
	int free_zoom;
	unsigned short spot_dir;
	unsigned int spot_dist;
	unsigned char spot_pref;
	unsigned char spot_trans;
	int spot_alt;
	unsigned int tower_dist;
	struct MATRIXF32 view_dir_matrix;
	unsigned short fine_heading[7];
	unsigned short fine_pitch[7];
	unsigned short fine_bank[7];
	unsigned short view_transition;
	struct LATLONALT eye_location;
	struct PBH32 eye_direction;
	struct LATLONALT old_spot_goal;
	int prepare_spot;
	struct XYZF64 eye_velocity;
	unsigned short eye_zoom;
	unsigned short eye_zoom_vel;
	void* terrain_view_ptr;
	struct XYZ16 eyepoint;
	struct XYZ16 eyepoint_offset[7];
	unsigned short saved_zoom_cockpit[24];
	enum VIEW_MODE saved_view_mode;
	struct UNIBOX view_area;
	char saved[0x38];
	int view_mode_locked;
	int track_mode;
	unsigned int chase_target_containerId;
	void* mouse_data;
}VIEWBLOCK,*PVIEWBLOCK,**PPVIEWBLOCK;

typedef struct AWIND {
	char* window_title;
	enum WINDOW_TYPE window_type;
	unsigned int window_flags;
	unsigned int window_id;
	struct MENUITEM* window_menu;
	void  (FSAPI *service_function)(enum SERVICE_FUNCTION_CODE code, struct AWIND *window, unsigned int is_sizing, void *ptr);
	struct VIEWBLOCK* viewblock;
	struct UNIBOX screen_uni;
	struct PIXBOX screen_pix;
	struct PIXBOX doc_pix;
	struct PIXBOX undoc_pix;
	struct PIXPOINT min;
	struct PIXPOINT max;
	unsigned int layer;
	unsigned int offscreen_num;
	IMAGE offscreen;
	void* api_data;
	void* pOwnerInstance;
	void* pAppDevice;
	PIXPOINT parent_size;
}AWIND,*PAWIND,**PPAWIND;

#pragma pack()

#endif
