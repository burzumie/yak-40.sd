/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: fs_linkage.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef __FS_SDK_FSLINKAGE__
#define __FS_SDK_FSLINKAGE__

#include"def_types.h"

#pragma pack(4)

typedef struct FSLINKAGE
{
    unsigned long ModuleID;
    void (*ModuleInit)(void);
    void (*ModuleDeInit)(void);
    unsigned long ModulePriority;
    unsigned long ModuleFlags;
    unsigned long ModuleVersion;
}FSLINKAGE,*PFSLINKAGE,**PPFSLINKAGE;

typedef struct FS6LINK {
    int ModuleID;
    void  (FSAPI *ModuleInit)();
    void  (FSAPI *ModuleDeinit)();
    unsigned int ModuleFlags;
    unsigned int ModulePriority;
    unsigned int ModuleVersion;
}FS6LINK,*PFS6LINK,**PPFS6LINK;

#pragma pack()

#endif
