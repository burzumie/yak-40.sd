/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: container.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_CONTAINER
#define _FS_SDK_CONTAINER

#pragma pack(4)

class CSmartPtrRefCount {
public:
int m_iRefCount;
	CSmartPtrRefCount(){m_iRefCount=0;};
  inline int AddRef(){return ++m_iRefCount;};
  inline int Release(){return --m_iRefCount;};
  inline int GetRefCount(){return m_iRefCount;};
};
 
template<class T> 
class TSmartPtr 
{
public:
	T* m_ptr;
	inline	TSmartPtr(const TSmartPtr<T>& src)
	{
		m_ptr=src.m_ptr;
		AddRef();
	};
	inline	TSmartPtr(TSmartPtr<T>& src)
	{
		m_ptr=src.m_ptr;
		AddRef();
	};

	//T* & const T*
	inline TSmartPtr(T* src)
	{
		m_ptr=src;
		AddRef();
	};

	inline TSmartPtr(const T* src)
	{
		m_ptr=src;
		AddRef();
	};

	// T& and const T&
	inline TSmartPtr(T& src)
	{
		m_ptr=&src;
		AddRef();
	};

	inline TSmartPtr(const T& src) {
			m_ptr=&src;
			AddRef();
	};
	// TSmartPtr* & const TSmartPtr*
	inline TSmartPtr(const TSmartPtr<T>* src)
	{
		m_ptr=(src->operator->());
		AddRef();
	}
	inline TSmartPtr(TSmartPtr<T>* src)
	{
		m_ptr=(src->operator->());
		AddRef();
	}



	// default
inline	TSmartPtr(){m_ptr=new T;AddRef();};

inline	~TSmartPtr(){Release();};
inline	T* operator->()
	{
		return m_ptr;
	};
	
	inline	T& operator*()
	{
		return *m_ptr;
	};

	inline	operator const T*() {
		return const_cast<const T*>(m_ptr);
	};

	inline	T* operator *(void*) {
		return m_ptr;
	};

	inline const T* get() const {return m_ptr;}; 

	inline	operator const void *() {
		return const_cast<const void*>(m_ptr);
	};

	inline	operator void *(){return static_cast<void*>(m_ptr);};
	inline	bool operator!(){return m_ptr==NULL};
	inline	bool operator==(T*);
	inline	bool operator!=(T* op1){static_cast<CSmartPtrRefCount>(op1); return m_ptr!=op1;};
inline	bool operator!=(TSmartPtr<T>& op1){return m_ptr!=op1->m_ptr;};

inline	TSmartPtr<T>& operator=(TSmartPtr<T>& src)
	{
		if(m_ptr)Release();
		m_ptr=src.m_ptr;
		AddRef();
		return *this;
	}
inline	TSmartPtr<T>& operator=(TSmartPtr<T>* src)
	{
		if(m_ptr)Release();
		m_ptr=*src.m_ptr;
		AddRef();
		return *this;
	}

inline	TSmartPtr<T>& operator=(T* src)
	{
		if(m_ptr)Release();
		this->m_ptr=src;
		AddRef();
		return *this;
	};
inline	T& Attach(struct IUnknown*){};
inline	int AddRef(){return static_cast<CSmartPtrRefCount*>(m_ptr)->AddRef();};
inline	int Release()
	{
		register int r=static_cast<CSmartPtrRefCount*>(m_ptr)->Release();
		if(!r && m_ptr)
		{
			delete m_ptr;
			m_ptr=NULL;
		}
		return r;
	};
inline	int GetRefCount(){return static_cast<CSmartPtrRefCount*>(m_ptr)->GetRefCount();};
inline	T* GetRawPointer(){return new T(m_ptr);};
};


template<class T> class TVector 
{
public:
	typedef T*		 iterator;
	typedef const T* const_iterator;
	typedef T&		 reference ;
	typedef const T& const_reference;

	enum GrowthMode{LINEAR=0x00,GEOMETRIC=0x01};
	enum Flags{FLAGS_OWN_ARRAY=0x00,FLAGS_READ_ONLY=0x02,FLAGS_GEOMETRIC_GROWTH=0x04};
	enum Constants {VECTOR_DEFAULT_GROWTH_RATE = 0x5};
//#error m_arraySize IS IN ELEMENTS!!!!!!

	int m_elementCount;
	int m_arraySize;
	short m_growthRate;
	short m_flags;
	iterator m_array;

	int  Reallocate(int newElementCount, int bExact)
	{
		T* new_array=NULL;
		int old_size=m_arraySize;
		if(newElementCount<m_elementCount)
			return 0;//use other methods
		if(newElementCount==m_elementCount)
		{
			if(!bExact)
				return m_elementCount;
			if(m_arraySize>m_elementCount)
			{
				 m_arraySize=m_elementCount;
			}
		}
		else
		{
			if(bExact)
				m_arraySize=newElementCount;
			else
			{
				if(m_flags&FLAGS_GEOMETRIC_GROWTH)
					while(m_arraySize<newElementCount)
						m_arraySize*=(m_growthRate);
				else
					while(m_arraySize<newElementCount)
						m_arraySize+=m_growthRate;
			}
		}
		new_array=(T*)malloc(m_arraySize*sizeof(T));
		if(!new_array)
		{
			m_arraySize=old_size;
			return 0;
		}
		if(m_array)
		{
			memcpy(new_array,m_array,m_arraySize);
			free(m_array);
		}
		m_array=new_array;
		return newElementCount;
	};
	iterator  PartialInsert(int);// NO IDEAS
	void  Decrement();// NO IDEAS
	iterator Front(){return m_elementCount?m_array[0]:NULL;};
	const_iterator Front()const {return m_elementCount?(const_iterator)m_array[m_elementCount-1]:NULL;};
	iterator Back(){return m_elementCount?m_array[m_elementCount-1]:NULL;};
	const_iterator Back() const {return m_elementCount?(const_iterator)m_array[m_elementCount-1]:NULL;};
	TVector()
	{
		m_array=0;
		m_arraySize=m_elementCount=0;
		m_flags=0;
		m_growthRate=VECTOR_DEFAULT_GROWTH_RATE;
	};
	TVector(TVector<T>& src)
	{
		m_elementCount=src.m_elementCount;
		m_flags=src.m_flags;
		m_growthRate=src.m_growthRate;
		m_arraySize=m_elementCount;
		m_array=malloc(m_arraySize*sizeof(T));
		for(register int i=0;i<m_elementCount;i++)
			m_array[i]=new T(src.m_array[i]);
	};
	TVector(enum Flags _flags ,GrowthMode _gMode)
	{
		m_array=NULL;
		m_growthRate=VECTOR_DEFAULT_GROWTH_RATE;
		m_flags=_gMode;
		m_elementCount=0;
		m_flags=_flags;
		m_arraySize=0;

	};
	~TVector()
	{
		Clear();
		free(m_array);
	};
	void  Reserve(int ce)
	{
		Reallocate(ce,1);
	}
	void  SetGrowthMode(GrowthMode _gMode){m_flags=_gMode;};
	void  SetGrowthRate(short _rate){m_growthRate=_rate;};
	int  SetSize(int new_size)
	{
		return 0;
	};
	T&  Increment();

	reference Insert(int pos , reference what)
	{
		Reallocate(m_elementCount+1,0);
		DWORD to=(DWORD)&m_array[m_elementCount],
			  from=(DWORD)&m_array[m_elementCount-1],
			  ToMove=(m_elementCount-pos)*sizeof(T);
		_asm
		{
			pushad
			pushf
			mov esi,from
			mov edi,to
			mov ecx,ToMove
			std
			push ecx
			shr ecx,2
			rep movsd
			pop ecx
			and ecx,3
			rep movsb
			PopF 
			popad
		}
		m_elementCount++;
		At(pos)=what;
		return static_cast<reference>(m_array[pos]);
	};
	reference  Insert(int pos)
	{
		return Insert(pos,T());
	};
	int  Remove(reference what)
	{
		for(register int i=0;i<m_elementCount;i++)
		{
			if(m_array[i]==what)
			{
				Remove(i);
				return i;
			}
		}
	};
	void  Remove(int from, int how)
	{
		for(register int i=from,(i<from+how)&&(i<m_elementCount);i++)
			delete m_array[i];
		memcpy(&m_array[from],
			   &m_array[from+how],
			   (m_elementCount-(from+how))*sizeof(T));
		m_elementCount-=how;
	};
	void  Remove(int pos)
	{
		Remove(pos,1);
	};
	void  PushFront(reference what)
	{
		Insert(0,what);
	};
	void  PushBack(reference what)
	{
		Insert(m_elementCount,what);
	};
	void  PopFront(){Remove(0,1);};
	void  PopBack(){Remove(m_elementCount,1)};
	void  Clear()
	{
		for(register int i=0;i<m_elementCount;i++)
		{
			m_array[i].~T();
		}
		m_elementCount=0;
	};
	void  Compact(int);
	void  AppendVector(TVector<T>& src)
	{
		Reallocate(m_elementCount+src.Size(),0);
		for(register int i=0;i<src.Size();i++)
			m_array[m_elementCount+i]=src[i];
		m_elementCount+=src.m_elementCount;
	};
	reference  At(int pos){return static_cast<reference>(m_array[pos]);};
	const_reference  At(int pos)const{return static_cast<const_reference>(m_array[pos]);};
	reference  operator[](int pos){return static_cast<reference>(m_array[pos]);};
	const_reference  operator[](int pos)const{return static_cast<const_reference>(m_array[pos]);};
	reference  First(){return static_cast<reference>(m_array[0]);};
	const_reference  First()const{return static_cast<const_reference>(m_array[0]);};
	reference  Last(){return static_cast<reference>(m_array[m_elementCount-1]);};
	const_reference  Last()const{return static_cast<const_reference>(m_array[m_elementCount-1]);};
	iterator  Address();
	const_iterator  Address()const;
	iterator  TakeArray(){return m_array;};
	int  Size(){return m_elementCount;};
	int  Capacity(){return m_arraySize;};
	int  MaxSize();
	int  IsEmpty(){return m_elementCount!=0;};
	int  IsReadOnly(){return m_flags& FLAGS_READ_ONLY;};
};

template<int C> class TMemoryPoolBase
{
public:
	static TMemoryPoolBase<C>* ms_pFirstPool;
	TMemoryPoolBase<C>* m_pNextPool;
	
	static void __stdcall FlushAllPools();
	static TMemoryPoolBase<C>* __stdcall GetFirstPool(){return ms_pFirstPool;};
	
	TMemoryPoolBase(TMemoryPoolBase<C>&);
	TMemoryPoolBase(char*){m_pNextPool=ms_pFirstPool;ms_pFirstPool=this;};
	class TMemoryPoolBase* GetNextPool(){return m_pNextPool;};
	char* GetPoolName();
	
	// virtual functions ------------------------------
	virtual /*<vtableoff 0x0>*/ ~TMemoryPoolBase(){}	;
	virtual /*<vtableoff 0x4>*/ void Flush() = 0;
	virtual /*<vtableoff 0x8>*/ unsigned int GetNumAllocatedBlocks() = 0;
	virtual /*<vtableoff 0xc>*/ unsigned int GetNumAvailableBlocks() = 0;
	virtual /*<vtableoff 0x10>*/ unsigned int GetNumTotalBlocks() = 0;
	virtual /*<vtableoff 0x14>*/ unsigned int GetNumPages() = 0;
	virtual /*<vtableoff 0x18>*/ unsigned int GetMaxNumPages() = 0;
	virtual /*<vtableoff 0x1c>*/ unsigned int GetNumBlocksPerPage() = 0;
	virtual /*<vtableoff 0x20>*/ unsigned int GetTotalMemoryUsage() = 0;
	virtual /*<vtableoff 0x24>*/ unsigned int GetBlockSize() = 0;
};

template<class T,int PAGE_SIZE> class TMemoryPool :public TMemoryPoolBase<0>
{
public:
	struct Page 
	{	 
	public:
		unsigned int uNumFreeBlocks; //4  off:0
		unsigned short auFreeBlockIndexes[PAGE_SIZE];//2*PAGE_SIZE off:unsigned
		T/* char*/ aBlocks[PAGE_SIZE];//PAGE_SIZE*sizeof(T) off:4+2*PAGE_SIZE
	};
	TVector<Page*> m_pages;
	unsigned int m_pagesMax;
/////// METHODS////////////////////////////////////	
	TMemoryPool(TMemoryPool<T,PAGE_SIZE>&);
	TMemoryPool(char*name):TMemoryPoolBase<0>(name){};
	void* Allocate(unsigned int)
	{	int i;
		for(i=0;i<m_pages.m_elementCount;i++)
		{
			if(m_pages[i]->uNumFreeBlocks)
				break;
		}
		if( 
			(i==m_pages.m_elementCount)|| //no space in existing pages
			(!m_pages[i])// Null pointer in vector
			)
		{//need to allocate page
			//Page* new_page=malloc(sizeof(Page));// alloc.
			if(m_pages.m_elementCount<=m_pages.m_arraySize)
			{// reallocated vector to fit new page pointer
				m_pages.Reallocate(m_pages.m_elementCount+1,0);
			}
			if(!m_pages[m_pages.m_elementCount])
			{//NULL
				_asm nop // NO IDEAS
			}
			m_pages[m_pages.m_elementCount]=(Page*)malloc(sizeof(Page));
			m_pages[m_pages.m_elementCount]->uNumFreeBlocks=PAGE_SIZE;
			for(i=0;i<PAGE_SIZE;i++)
				m_pages[m_pages.m_elementCount]->auFreeBlockIndexes[i]=i;
			if( ++m_pages.m_elementCount>m_pagesMax)
				m_pagesMax=m_pages.m_elementCount;
			i=m_pages.m_elementCount-1;
		}
//no alloc needed. Have space in page i
		  m_pages[i]->uNumFreeBlocks--;
		  return &m_pages[i]->aBlocks[m_pages[i]->auFreeBlockIndexes[m_pages[i]->uNumFreeBlocks]];
	};
	void Deallocate(T* pBlock)
	{
		if(!m_pages.m_elementCount)
			return;
		for(register int i=0;i<m_pages.m_elementCount;i++)
		{
			if(pBlock<m_pages[i].aBlocks)
				continue;//not in current
			if(pBlock<&m_pages[i].aBlocks[PAGE_SIZE])
				break;//BlockFound
		}
		if(i==m_pages.m_elementCount)
			return;// not found
		//in current page
		m_pages[i].auFreeBlockIndexes[m_pages[i].uNumFreeBlocks]=(pBlock-m_pages[i].aBlocks)/sizeof(T);
		if(++m_pages[i].uNumFreeBlocks==PAGE_SIZE)
		{// kill empty page
			m_pages.Remove(i)
		}
	};
	
	// virtual functions ------------------------------
	// ALL overrides are inherited from TMemoryPoolBase<0>!!!
	virtual  ~TMemoryPool()
	{
		for(int i=0;i<m_pages.m_elementCount;i++)
		{
			delete m_pages[i];
		}
	};
	virtual  void Flush()
	{
		m_pages.m_elementCount=0;
		m_pages.Reallocate(m_pages.m_elementCount,0);
	};
	virtual  unsigned int GetNumAllocatedBlocks()
	{
		return GetNumTotalBlocks()-GetNumAvailableBlocks();
	};
	virtual  unsigned int GetNumAvailableBlocks()
	{
		unsigned int acc=0;
		 for(register int i=0;i<m_pages.m_elementCount;i++)
		 		 acc+=m_pages[i]->uNumFreeBlocks;
		return acc;
	};
	virtual  unsigned int GetNumTotalBlocks()
	{
		return m_pages.m_elementCount*PAGE_SIZE;
	};
	virtual  unsigned int GetNumPages()
	{
		return m_pages.m_elementCount;
	};
	virtual  unsigned int GetMaxNumPages()
	{
		return m_pagesMax;
	};
	virtual unsigned int GetNumBlocksPerPage()
	{
		return PAGE_SIZE;
	};
	virtual unsigned int GetTotalMemoryUsage()
	{
		return sizeof(unsigned long)+sizeof(unsigned short)*PAGE_SIZE+PAGE_SIZE*sizeof(T);
	};
	virtual unsigned int GetBlockSize()
	{
		return sizeof(T);
	};
};

#pragma pack()

#endif
