/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: module_id.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#define MID_FS6ID		0x01
#define MID_GLOBAL		0x02
#define MID_MAIN		0x03	
#define MID_DYNAMIC		0x06
#define MID_DEMO		0x07	
#define MID_WINDOW		0x0A	
#define MID_G2D			0x0D	
#define MID_G3D			0x0E	
#define MID_PANELS		0x0F	
#define MID_CONTROLS	0x10
#define MID_FE			0x12		
#define MID_UTIL		0x14	
#define MID_ACONTAIN	0x17
#define MID_ATC			0x18
#define MID_SIM1		0x19
#define MID_WEATHER		0x1C	
#define MID_REALITY		0x20	
#define MID_MULTIPLAYER 0x23
#define MID_FACILITIES	0x25
#define MID_AI_PLAYR	0x26
#define MID_SOUND		0x27	
#define MID_SYMMAP		0x2B	
#define MID_FLIGHT		0x2C	
#define MID_TERRAIN		0x2D	
#define MID_VISUALFX	0x2E	
#define MID_ABLSCPT		0x36	
#define MID_TRAFFIC		0x37	
#define MID_SIMSCHED	0x38	
#define MID_FSUI		0x80000000	
#define MID_INSTRUCTOR	0x8000000F	
#define MID_MPCHAT		-1
#define MID_TERMINATOR	0