/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: def_types.h,v $

  Last modification:
    Date:      $Date: 2005/08/29 18:00:10 $
    Version:   $Revision: 1.4 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef __FS_SDK_FSTYPES__
#define __FS_SDK_FSTYPES__

#pragma pack(4)

// Useful macros for INL files
#define CONCATENATE1(A,B) A##B
#define CONCATENATE(A,B) CONCATENATE1(A,B)
#define CONCATENATE31(A,B,C) A##B##C
#define CONCATENATE3(A,B,C) CONCATENATE31(A,B,C)
#define C_GAUGE(A) CONCATENATE(A , GAUGE_ITEM)
#define C_PLIST(A) CONCATENATE(plist_ , A)

#define LENGTHOF(array) (sizeof(array)/sizeof(array[0]))

#ifndef EXTERN_C
#ifdef __cplusplus
    #define EXTERN_C    extern "C"
#else
    #define EXTERN_C    extern
#endif
#endif


#ifndef DECLSPEC_EXPORT
#define DECLSPEC_EXPORT __declspec(dllexport)
#endif

// Flightsim calling convention
#define	FSAPI	__stdcall

#define FS_REG_BASE     "SOFTWARE\\Microsoft\\Microsoft Games\\Flight Simulator\\9.0"

//
// Common Flight Simulator Constant Definitions
//

// parameter direction tags
#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#ifndef VOID
#define VOID	void
#endif

// Logical True/False Constants
#ifndef FALSE
#define FALSE	0
#endif

#ifndef TRUE
#define TRUE	1
#endif

// Pointer Constant
#ifndef	NULL
#define NULL	((void *)0)			// empty handle
#endif

// Misc Constants
#define NULLPTR	NULL				// empty pointer
#define CR		0x0d				// ASCII carriage return
#define LF		0x0a				// ASCII line feed
#define FLAGS0	0					// no flags set in a FLAGSn type variable
#define FLAGS1	-1					// all flags set in a FLAGSn type variable

// Bit Flag Constants
#define BIT0	0x00000001
#define BIT1	0x00000002
#define BIT2	0x00000004
#define BIT3	0x00000008
#define BIT4	0x00000010
#define BIT5	0x00000020
#define BIT6	0x00000040
#define BIT7	0x00000080
#define BIT8	0x00000100
#define BIT9	0x00000200
#define BIT10	0x00000400
#define BIT11	0x00000800
#define BIT12	0x00001000
#define BIT13	0x00002000
#define BIT14	0x00004000
#define BIT15	0x00008000
#define BIT16	0x00010000
#define BIT17	0x00020000
#define BIT18	0x00040000
#define BIT19	0x00080000
#define BIT20	0x00100000
#define BIT21	0x00200000
#define BIT22	0x00400000
#define BIT23	0x00800000
#define BIT24	0x01000000
#define BIT25	0x02000000
#define BIT26	0x04000000
#define BIT27	0x08000000
#define BIT28	0x10000000
#define BIT29	0x20000000
#define BIT30	0x40000000
#define BIT31	0x80000000

// integer data types limits
#define UINT8_MIN	0
#define UINT8_MAX	255
#define SINT8_MIN	-128
#define SINT8_MAX	 127
#define UINT16_MIN	0
#define UINT16_MAX	65535
#define SINT16_MIN	-32768
#define SINT16_MAX	32767
#define UINT32_MIN	0
#define UINT32_MAX	4294967295
#define SINT32_MIN	((signed int) BIT31)
#define SINT32_MAX	2147483647

// fractional data types limits
#define UFRAC8_MIN	0
#define UFRAC8_MAX	255
#define SFRAC8_MIN	-128
#define SFRAC8_MAX	127
#define UFRAC16_MIN	0
#define UFRAC16_MAX	65535
#define SFRAC16_MIN	-32768
#define SFRAC16_MAX	32767
#define UFRAC32_MIN	0
#define UFRAC32_MAX	4294967295
#define SFRAC32_MIN	-2147483648
#define SFRAC32_MAX	2147483647

// flag values for engine_control_select variable
#define ENGINE_CONTROL_SELECT_NONE	0		// no engines
#define ENGINE_CONTROL_SELECT_1		BIT0	// engine 1
#define ENGINE_CONTROL_SELECT_2		BIT1	// engine 2
#define ENGINE_CONTROL_SELECT_3		BIT2	// engine 3
#define ENGINE_CONTROL_SELECT_4		BIT3	// engine 4

// enum values for prop_adv_sel variable
#define PROP_ADV_SEL_FIXED			0		// fixed pitch propeller
#define PROP_ADV_SEL_AUTO			1		// variable pitch propeller, automatic
#define PROP_ADV_SEL_MANUAL			2		// variable pitch propeller, manual

// realism_systems values
#define	REALISM_SYSTEMS_ELEV_TRIM	BIT1	// enable elevator trim realism (ratchet effect)
#define	REALISM_SYSTEMS_GYRO_DRIFT	BIT2	// enable gyro drift
#define	REALISM_SYSTEMS_LIGHT_BURN	BIT3	// enable light burnout if on too long during day
#define	REALISM_SYSTEMS_FAST_THROT	BIT4	// enable fast throttle kill engine
#define	REALISM_SYSTEMS_INS_LIGHTS	BIT5	// enable instrument light needed at night to see panel
#define	REALISM_SYSTEMS_BARO_DRIFT	BIT6	// enable barometric pressure drift


// defines for window layer numbers & shared buffer numbers
#define LOWEST_LAYER        0x0000
#define VIEW_LAYER          0x2000
#define MINICONTROL_LAYER   0x3000
#define AUX_VIEW_LAYER      0x3F00
#define PANEL_LAYER         0x4000
#define KNEEBOARD_LAYER			0x4500
#define MESSAGE_LAYER       0x5000
#define HIGHEST_LAYER       0xffff


// constants for the range of the UNIPIX coordinate system
#define UNIPIX_MAX_HEIGHT   6144    // max height of unipix region
#define UNIPIX_MAX_WIDTH    8192    // max width of unipix region


// vor1_tf_flag and vor2_tf_flag
#define	VOR_TF_FLAG_OFF			0
#define	VOR_TF_FLAG_TO			1
#define	VOR_TF_FLAG_FROM		2

// Back course flags for VOR gauges
#define	BC_FLAG_BACKCOURSE_AVAILABLE	BIT0
#define	BC_FLAG_LOCALIZER_TUNED_IN		BIT1
#define	BC_FLAG_ON_BACKCOURSE			BIT2
#define	BC_FLAG_STATION_ACTIVE			BIT7

// sim_speed constants
#define	SIM_SPEED_QUARTER	0x0040
#define	SIM_SPEED_HALF		0x0080
#define	SIM_SPEED_1X		0x0100
#define	SIM_SPEED_2X		0x0200
#define	SIM_SPEED_4X		0x0400
#define	SIM_SPEED_8X		0x0800
#define	SIM_SPEED_16X		0x1000
#define	SIM_SPEED_32X		0x2000
#define	SIM_SPEED_64X		0x4000
#define	SIM_SPEED_128X		0x8000

#define	SIM_SPEED_MIN		SIM_SPEED_QUARTER
#define	SIM_SPEED_MAX		SIM_SPEED_128X

// controls limits
#define	SPOILERS_POS_OFF			0		// spoilers desired position {0=retracted..16k=fully extended}
#define	SPOILERS_POS_FULL			16383
#define	FLAPS_POS_OFF				0		// 0=up, 16383 = full
#define	FLAPS_POS_FULL				16383
#define	GEAR_POS_UP					0		// 0 = up, 16K = dn
#define	GEAR_POS_DOWN				16383

// defines for VOR_INFO.CODE field
#define	VOR_CODE_IS_LOCALIZER			BIT7	// bit7 = 0= VOR  1= Localizer
#define	VOR_CODE_GLIDESLOPE			    BIT6	// bit6 = 1= Glideslope Available
#define	VOR_CODE_BACKCOURSE_UNAVAIL	    BIT5	// bit5 = 1= no localizer backcourse
#define	VOR_CODE_DME_AT_GLIDE_SLOPE	    BIT4	// bit4 = 1= DME transmitter at Glide Slope Transmitter
#define	VOR_CODE_NAV_UNAVAILABLE	    BIT3	// bit3 = 1= no nav signal available
#define	VOR_CODE_VOICE_AVAILABLE	    BIT2	// bit2 = Voice Available
#define	VOR_CODE_TACAN				    BIT1	// bit1 = TACAN
#define VOR_CODE_DME_AVAILABLE		    BIT0	// bit0 = DME

// defines for engine structure starter variable, note:  magnetos for recip. engines / starters for jet engines
#define ENG_STARTER_MAGNETO_OFF		0	// the following values for reciprocating engines
#define ENG_STARTER_MAGNETO_RIGHT	1
#define ENG_STARTER_MAGNETO_LEFT	2
#define ENG_STARTER_MAGNETO_BOTH	3
#define ENG_STARTER_MAGNETO_START	4
#define ENG_STARTER_STARTER_OFF		0	// the following values for jet engines
#define ENG_STARTER_STARTER_START	1
#define ENG_STARTER_STARTER_GEN		2


#define GST_UNUSED  0x0000
#define GST_ACTIVE  0x0001
#define GST_GEAR    0x0002
#define GST_SCRAPE  0x0004

#define GSN_CENTER_GEAR         0
#define GSN_LEFT_GEAR           1
#define GSN_RIGHT_GEAR          2
#define GSN_AUX_GEAR            3
#define GSN_RIGHT_WING_SCRAPE   4
#define GSN_LEFT_WING_SCRAPE    5
#define GSN_FUSELAGE_SCRAPE     6
#define GSN_XTAIL_SCRAPE        7
#define GSN_AUX1_SCRAPE         8
#define GSN_AUX2_SCRAPE         9

#define TANK_MAIN_RIGHT     0
#define TANK_MAIN_LEFT      1
#define TANK_AUX_RIGHT      2
#define TANK_AUX_LEFT       3
#define TANK_TIP_RIGHT      4
#define TANK_TIP_LEFT       5
#define TANK_CENTER         6
#define TANK_CENTER2        7
#define TANK_CENTER3        8
#define TANK_EXTERNAL1      9
#define TANK_EXTERNAL2      10

// gear types
#define GEAR_TYPE_FIXED         0
#define GEAR_TYPE_RETRACTABLE   1
#define GEAR_TYPE_SKIDS         2
#define GEAR_TYPE_FLOATS        3
#define GEAR_TYPE_SKIS          4


// time_of_day constants
#define TIME_OF_DAY_DAY     BIT0
#define TIME_OF_DAY_DAWN    BIT1
#define TIME_OF_DAY_DUSK    BIT1
#define TIME_OF_DAY_NIGHT   BIT2

typedef void            *PVOID;
typedef PVOID           *PPVOID;

typedef unsigned    char    UINT8, *PUINT8, **PPUINT8;
typedef signed      char    SINT8, *PSINT8, **PPSINT8;
typedef unsigned    short   UINT16, *PUINT16, **PPUINT16;
typedef signed      short   SINT16, *PSINT16, **PPSINT16;
typedef signed      int     SINT32, *PSINT32, **PPSINT32;
typedef unsigned    char    VAR8, *PVAR8, **PPVAR8;
typedef unsigned    short   VAR16, *PVAR16, **PPVAR16;
typedef unsigned    int     VAR32, *PVAR32, **PPVAR32;

#ifndef _BASETSD_H_
typedef unsigned    int     UINT32, *PUINT32;
#endif
typedef unsigned    int     **PPUINT32;


#ifndef _BASETSD_H_ 
typedef unsigned __int64 UINT64, *PUINT64, **PPUINT64;
#endif

typedef signed __int64 SINT64, *PSINT64, **PPSINT64;

// 64-bit generic structure
typedef struct  VAR64
{
    VAR32   lo;
    VAR32   hi;
} VAR64, *PVAR64, **PPVAR64;

typedef struct  UIF64
{
    UINT32  f;
    UINT32  i;
} UIF64, *PUIF4, **PPUIF64;

typedef struct  SVAR64
{
    UINT32  lo;
    SINT32  hi;
} SVAR64, *PSVAR64, **PPSVAR64;

// boolean variables - hold TRUE/FALSE
typedef int     BOOL, *PBOOL, **PPBOOL;
typedef VAR8    BOOL8, *PBOOL8, **PPBOOL8;
typedef VAR16   BOOL16, *PBOOL16, **PPBOOL16;
typedef VAR32   BOOL32, *PBOOL32, **PPBOOL32;

// array of 1-bit boolean variables
typedef int     FLAGS, *PFLAGS, **PPFLAGS;
typedef VAR8    FLAGS8, *PFLAGS8, **PPFLAGS8;
typedef VAR16   FLAGS16, *PFLAGS16, **PPFLAGS16;
typedef VAR32   FLAGS32, *PFLAGS32, **PPFLAGS32;

// Enumeration variables - domain is subset of all possible values
typedef int     ENUM, *PENUM, **PPENUM;
typedef VAR8    ENUM8, *PENUM8, **PPENUM8;
typedef VAR16   ENUM16, *PENUM16, **PPENUM16;
typedef VAR32   ENUM32, *PENUM32, **PPENUM32;

// array index variables
typedef int     INDX, *PINDX, **PPINDX;
typedef VAR8    INDX8, *PINDX8, **PPINDX8;
typedef VAR16   INDX16, *PINDX16, **PPINDX16;
typedef VAR32   INDX32, *PINDX32, **PPINDX32;

// Identifier variables
typedef int     ID, *PID, **PPID;
typedef VAR8    ID8, *PID8, **PPID8;
typedef VAR16   ID16, *PID16, **PPID16;
typedef VAR32   ID32, *PID32, **PPID32;

// Error return variables
typedef VAR32   ERR, *PERR, **PPERR;

// Fractional variables
typedef UINT8   UFRAC8, *PUFRAC8, **PPUFRAC8;    // { 0..+1}
typedef SINT8   SFRAC8, *PSFRAC8, **PPSFRAC8;    // {-1..+1}
typedef UINT16  UFRAC16, *PUFRAC16, **PPUFRAC16; // { 0..+1}
typedef SINT16  SFRAC16, *PSFRAC16, **PPSFRAC16; // {-1..+1}
typedef UINT32  UFRAC32, *PUFRAC32, **PPUFRAC32; // { 0..+1}
typedef SINT32  SFRAC32, *PSFRAC32, **PPSFRAC32; // {-1..+1}

// Floating-point types
typedef double  FLOAT64, *PFLOAT64, **PPFLOAT64;
typedef float   FLOAT32, *PFLOAT32, **PPFLOAT32;

// Integer/Fraction structures both signed & unsigned
typedef UINT16  UIF16, *PUIF16, **PPUIF16;

typedef SINT16  SIF16, *PSIF16, **PPSIF16;

typedef UINT32  UIF32, *PUIF32, **PPUIF32;

typedef SINT32  SIF32, *PSIF32, **PPSIF32;

#ifdef _H2INC
typedef struct  UIF48
{
    UINT16  pad;
    UINT16  f;
    UINT32  i;
} UIF48, *PUIF48, **PPUIF48;

typedef struct  SIF48
{
    UINT16  pad;
    UINT16  f;
    SINT32  i;
} SIF48, *PSIF48, **PPSIF48;
#else
typedef union   UIF48
{
    struct
    {
        UINT16  pad;
        UINT16  f;
        UINT32  i;
    };

    UINT64      i64;

} UIF48, *PUIF48, **PPUIF48;

typedef union   SIF48
{
    struct
    {
        UINT16  pad;
        UINT16  f;
        SINT32  i;
    };

    SINT64      i64;

} SIF48, *PSIF48, **PPSIF48;
#endif


typedef SVAR64  SIF64, *PSIF64, **PPSIF64;

#if !defined(_WINNT_)               // NOT PORTABLE!
typedef char    CHAR;
typedef char    *PCHAR;             // single character
#endif
typedef PCHAR   *PPCHAR;            // pointer to pointer to char
typedef CHAR    STRING, *PSTRING, **PPSTRING;       // generic string of unknown length
typedef CHAR    STRINGZ, *PSTRINGZ, **PPSTRINGZ;    // generic string of unknown length, zero term
typedef const CHAR  *PCSTRINGZ, **PPCSTRINGZ;   // generic string of unknown length, zero term

typedef struct SUBSTRING
{
    PSTRINGZ    string;
    UINT32      leng;
}   SUBSTRING, *PSUBSTRING, **PPSUBSTRING;

typedef VAR16   BCD16, *PBCD16, **PPBCD16;      // 4-digit BCD number (0000-9999)
typedef VAR16   BCO16, *PBCO16, **PPBCO16;      // 4-digit Octal number (0000-7777) (in BCD)
typedef VAR32   BCD32, *PBCD32, **PPBCD32;      // 8-digit BCD number (00000000-99999999)
typedef VAR32   BCO32, *PBCO32, **PPBCO32;      // 8-digit Octal number (00000000-77777777) (in BCD)

enum FILEPOSITION {
	POSITION_BEGINNING = 0x0,
	POSITION_CURRENT = 0x1,
	POSITION_END = 0x2,
};

enum FILEMODE {
	OPEN_READ = 0x0,
	OPEN_READ_RANDOM = 0x1,
	OPEN_READ_SEQUENTIAL = 0x2,
	OPEN_READ_WRITE = 0x3,
	OPEN_WRITE = 0x4,
	OPEN_APPEND = 0x5,
	OPEN_READ_HEADER = 0x6,
};

enum FILETYPE {
	TYPE_NONE = 0x0,
	TYPE_BGL = 0x1,
	TYPE_SITUATION = 0x2,
	TYPE_MODE = 0x3,
	TYPE_R8 = 0x4,
	TYPE_R81 = 0x5,
	TYPE_R82 = 0x6,
	TYPE_0 = 0x7,
	TYPE_1 = 0x8,
	TYPE_PALETTE = 0x9,
	TYPE_HAZEPALETTE = 0xa,
	TYPE_PCX = 0xb,
	TYPE_BMP = 0xc,
	TYPE_PIX = 0xd,
	TYPE_VIS = 0xe,
	TYPE_AIRCRAFT = 0xf,
	TYPE_GAUGE = 0x10,
	TYPE_ICON = 0x11,
	TYPE_BINARY = 0x12,
	TYPE_SOUND = 0x13,
	TYPE_MODEL = 0x14,
	TYPE_MODULE = 0x15,
	TYPE_FLTSIM = 0x16,
	TYPE_BOUND = 0x17,
	TYPE_NEEDLE = 0x18,
	TYPE_PANEL = 0x19,
	TYPE_ADVENTURE = 0x1a,
	TYPE_SIM = 0x1b,
	TYPE_LOG = 0x1c,
	TYPE_VIDEO = 0x1d,
	TYPE_TEMP = 0x1e,
	TYPE_TXR = 0x1f,
	TYPE_LESSON = 0x20,
	TYPE_CHALLENGE = 0x21,
	TYPE_DRIVER = 0x22,
	TYPE_SC1 = 0x23,
	TYPE_CONFIG = 0x24,
	TYPE_MISSION = 0x25,
	TYPE_QUICK_COMBAT = 0x26,
	TYPE_FLIGHT = 0x27,
	TYPE_CAMPAIGN = 0x28,
	TYPE_TEXT = 0x29,
	TYPE_FLIGHTPLAN = 0x2a,
	TYPE_DAMAGEPROFILE = 0x2b,
	TYPE_COMPILED_DAMAGEPROFILE = 0x2c,
	TYPE_DYNAMICMISSION = 0x2d,
	TYPE_WEATHERDATA = 0x2e,
	TYPE_EFFECT = 0x2f,
	TYPE_SCENERYLAYER = 0x30,
	TYPE_ABL_SCRIPT = 0x31,
	TYPE_FLIGHT_RECORDER = 0x32,
	TYPE_XML = 0x33,
	TYPE_CAB = 0x34,
	TYPE_ZIP = 0x35,
	TYPE_DAT = 0x36,
	TYPE_MIP = 0x37,
	TYPE_PDF = 0x38,
	TYPE_BRIEFING = 0x39,
	TYPE_HTML = 0x3a,
	TYPE_WEATHER_THEME_DESCRIPTOR = 0x3b,
	TYPE_WEATHER_THEME_BINARYDATA = 0x3c,
	TYPE_CLOUD_DESC = 0x3d,
	TYPE_ADDON_BASE = 0x3e,
	TYPE_ALL = 0xff,
	TYPE_DIRECTORY = 0xfe
};

typedef struct DATETIME {
    unsigned short year;
    unsigned short month;
    unsigned short day;
    unsigned short hour;
    unsigned short minute;
    unsigned short second;
}DATETIME,*PDATETIME,**PPDATETIME;

typedef	enum	VIEW_DIR {
	VIEW_DIR_FORWARD,
	VIEW_DIR_FORWARD_RIGHT,
	VIEW_DIR_RIGHT,
	VIEW_DIR_REAR_RIGHT,
	VIEW_DIR_REAR,
	VIEW_DIR_REAR_LEFT,
	VIEW_DIR_LEFT,
	VIEW_DIR_FORWARD_LEFT,
	VIEW_DIR_DOWN,
	VIEW_DIR_FORWARD_UP,
	VIEW_DIR_FORWARD_RIGHT_UP,
	VIEW_DIR_RIGHT_UP,
	VIEW_DIR_REAR_RIGHT_UP,
	VIEW_DIR_REAR_UP,
	VIEW_DIR_REAR_LEFT_UP,
	VIEW_DIR_LEFT_UP,
	VIEW_DIR_FORWARD_LEFT_UP,
	VIEW_DIR_UP,
	VIEW_DIR_AUX_00,
	VIEW_DIR_AUX_01,
	VIEW_DIR_AUX_02,
	VIEW_DIR_AUX_03,
	VIEW_DIR_AUX_04,
	VIEW_DIR_AUX_05,
	VIEW_DIR_MAX
} VIEW_DIR, *PVIEW_DIR, **PPVIEW_DIR;

enum SWITCH_ENUM {
	SWITCH_NOT_FOUND = 0x0,
	SWITCH_FOUND_NO_DATA = 0x1,
	SWITCH_FOUND_WITH_DATA = 0x2,
};

enum EVENT_SOURCE_TYPE {
	EVENT_SOURCE_PROGRAM = 0x0,
	EVENT_SOURCE_KEYBOARD = 0x1,
	EVENT_SOURCE_MOUSE_BUTTON = 0x2,
	EVENT_SOURCE_MOUSE_MOVE = 0x3,
	EVENT_SOURCE_JOYSTICK_BUTTON = 0x4,
	EVENT_SOURCE_JOYSTICK_MOVE = 0x5
};

enum CONFIG_ITEM_TYPE {
  CONFIG_ITEM_TYPE_NONE = 0x0,
  CONFIG_ITEM_TYPE_CALLBACK = 0x1,
  CONFIG_ITEM_TYPE_STRING = 0x2,
  CONFIG_ITEM_TYPE_INT8 = 0x3,
  CONFIG_ITEM_TYPE_INT16 = 0x4,
  CONFIG_ITEM_TYPE_INT32 = 0x5,
  CONFIG_ITEM_TYPE_BOOL8 = 0x6,
  CONFIG_ITEM_TYPE_BOOL16 = 0x7,
  CONFIG_ITEM_TYPE_BOOL32 = 0x8,
  CONFIG_ITEM_TYPE_BOOL8_MASK = 0x9,
  CONFIG_ITEM_TYPE_BOOL16_MASK = 0xa,
  CONFIG_ITEM_TYPE_BOOL32_MASK = 0xb,
  CONFIG_ITEM_TYPE_FLAG8 = 0xc,
  CONFIG_ITEM_TYPE_FLAG16 = 0xd,
  CONFIG_ITEM_TYPE_FLAG32 = 0xe,
  CONFIG_ITEM_TYPE_LOOKUP8 = 0xf,
  CONFIG_ITEM_TYPE_LOOKUP16 = 0x10,
  CONFIG_ITEM_TYPE_LOOKUP32 = 0x11,
  CONFIG_ITEM_TYPE_FLOAT32 = 0x12,
  CONFIG_ITEM_TYPE_FLOAT64 = 0x13,
  CONFIG_ITEM_TYPE_INT32_ARRAY = 0x14,
  CONFIG_ITEM_TYPE_FLOAT64_ARRAY = 0x15,
  CONFIG_ITEM_TYPE_A2B_FORMAT = 0x16,
  CONFIG_ITEM_TYPE_STRING_FORMAT = 0x17,
  CONFIG_ITEM_TYPE_BLOB = 0x18,
};

enum CONFIG_ITEM_SOURCE {
  CONFIG_ITEM_SOURCE_NONE = 0x0,
  CONFIG_ITEM_SOURCE_FILE = 0x1,
  CONFIG_ITEM_SOURCE_OVERRIDE = 0x2,
};

enum CONFIG_ITEM_VAR {
  CONFIG_ITEM_VAR_NONE = 0x0,
  CONFIG_ITEM_VAR_POINTER = 0x1,
  CONFIG_ITEM_VAR_GLOBAL = 0x2,
  CONFIG_ITEM_VAR_OFFSET = 0x3,
  CONFIG_ITEM_MAX = 0x4,
};

typedef struct
{
    ENUM32      code;               // code value
    PSTRINGZ    text;               // text value
} LOOKUP_TABLE, *PLOOKUP_TABLE, **PPLOOKUP_TABLE;

union CONFIG_ITEM_TYPE_DATA {
    unsigned int var_length;
    unsigned int format_code;
    char* format_string;
    unsigned int bitmask;
    struct LOOKUP_TABLE* lookup_table;
    void  (FSAPI *callback)(void*, char*, int);
};

struct CONFIG_ITEM {
    char* keyword;
    void* variable;
    enum CONFIG_ITEM_VAR var_addr;
    enum CONFIG_ITEM_TYPE var_type;
    union CONFIG_ITEM_TYPE_DATA var_type_data;
    enum CONFIG_ITEM_SOURCE source;
};

enum SP_FLIGHT_MODEL_TYPE {
	FLIGHT_MODEL_CUSTOM = 0x0,
	FLIGHT_MODEL_EASY = 0x1,
	FLIGHT_MODEL_MIN = 0x1,
	FLIGHT_MODEL_MEDIUM = 0x2,
	FLIGHT_MODEL_HARD = 0x3,
	FLIGHT_MODEL_MAX = 0x3
};

typedef struct REALISM_VARS {
	double PfactorSc;
	double TorqueSc;
	double GyroSc;
	double CrashToleranceSc;
	double GenRealismSc;
	int fUnlimitedFuel;
	int fIndicateTrueAirspeed;
	int fAutoCoordination;
	int fRealMixture;
	int fStressDamage;
	int fGEffects;
	int fGyroDrift;
	int fManualLights;
	int fCrashWithDynamic;
	unsigned short nCrashDetection;
	enum SP_FLIGHT_MODEL_TYPE FlightModel;
	int fAutoFuelManagement;
}REALISM_VARS,*PREALISM_VARS,**PPREALISM_VARS;

// surface_type constants
typedef enum SURFACE_TYPE
{
    SURFACE_TYPE_CONCRETE       = 0,    // concrete
    SURFACE_TYPE_GRASS          = 1,    // soft, bumpy ground (landable)
    SURFACE_TYPE_WATER          = 2,    // water
    SURFACE_TYPE_GRASS_BUMPY    = 3,    // very bumpy grass & mud (crashable)
    SURFACE_TYPE_ASPHALT        = 4,    // asphalt
    SURFACE_TYPE_SHORT_GRASS    = 5,    // short grass
    SURFACE_TYPE_LONG_GRASS     = 6,    // long grass
    SURFACE_TYPE_HARD_TURF      = 7,    // hard turf
    SURFACE_TYPE_SNOW           = 8,
    SURFACE_TYPE_ICE            = 9,
    SURFACE_TYPE_URBAN          = 10,
    SURFACE_TYPE_FOREST         = 11,
    SURFACE_TYPE_DIRT           = 12,   // added for dirt runways
    SURFACE_TYPE_CORAL          = 13,   // added for coral runways
    SURFACE_TYPE_GRAVEL         = 14,   // added for gravel runways
    SURFACE_TYPE_OIL_TREATED    = 15,   // added for oil treated (tar&chip) runways
    SURFACE_TYPE_STEEL_MATS     = 16,   // added for steel mats (steel mesh) temporary runways
    SURFACE_TYPE_BITUMINOUS			= 17,
    SURFACE_TYPE_BRICK          = 18,
    SURFACE_TYPE_MACADAM        = 19,
    SURFACE_TYPE_PLANKS         = 20,
    SURFACE_TYPE_SAND						= 21,
    SURFACE_TYPE_SHALE					= 22,
    SURFACE_TYPE_TARMAC					= 23,
    SURFACE_TYPE_WRIGHT_FLYER_TRACK = 24,

    SURFACE_TYPE_UNKNOWN        = 254,  // valid but unknown surface type
    SURFACE_TYPE_UNDEFINED      = 255   // invalid surface type
}
SURFACE_TYPE, *PSURFACE_TYPE;

typedef enum SURFACE_CONDITION
{
    SURFACE_CONDITION_NORMAL    = 0,    // default for the given SURFACE_TYPE
    SURFACE_CONDITION_WET,
    SURFACE_CONDITION_ICY,
    SURFACE_CONDITION_SNOW,             // this is for snow on a non-snow SURFACE_TYPE
    SURFACE_CONDITION_MAX       // this surface condition should always be last
}
SURFACE_CONDITION;

// version number data type
typedef struct  APP_VERSION
{
    UINT32  major;
    UINT32  minor;
    UINT32  build;
} APP_VERSION, *PAPP_VERSION, **PPAPP_VERSION;

// units enum values
typedef enum UNITS_OF_MEASURE
{
	ENGLISH_UNITS,
	METRIC_UNITS_ALT_FEET,
	METRIC_UNITS_ALT_METER
} UNITS_OF_MEASURE;

#pragma pack()

#endif
