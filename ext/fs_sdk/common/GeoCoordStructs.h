/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: GeoCoordStructs.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef __FS_SDK_GEOCOORD__
#define __FS_SDK_GEOCOORD__

#include "def_types.h"

#pragma pack(4)

typedef VAR8    ANGL8, *PANGL8, **PPANGL8;
typedef VAR16   ANGL16, *PANGL16, **PPANGL16;
typedef VAR32   ANGL32, *PANGL32, **PPANGL32; 

#ifdef _H2INC
typedef struct  ANGL48
{
    UINT16  pad;
    UINT16  lo;
    UINT32  hi;
} ANGL48, *PANGL48, **PPANGL48;
#else
typedef union   ANGL48
{
    struct
    {
        UINT16  pad;
        UINT16  lo;
        UINT32  hi;
    };
	
    UINT64      i64;
	
} ANGL48, *PANGL48, **PPANGL48;
#endif

// LatLon - used to store a position in 2D space
typedef struct
{
    SIF48       lat;
    ANGL48      lon;
} LATLON, *PLATLON, **PPLATLON;

// LatLon32 - used to store a position in 2D space (less accuracy)
typedef struct
{
    SINT32      lat;
    ANGL32      lon;
} LATLON32, *PLATLON32, **PPLATLON32;

typedef struct  LATLONALT
{
    SIF48       lat;
    ANGL48      lon;
    SIF48       alt;
} LATLONALT, *PLATLONALT, **PPLATLONALT;

typedef struct
{
    SINT32      lat;                // 32 bits of LAT (Meters)
    ANGL32      lon;                // 32 bits of LON
    SINT32      alt;                // 32 bits of ALT (Meters)
} LATLONALT32, *PLATLONALT32, **PPLATLONALT32;

typedef struct  _latlonalt_float32
{
    FLOAT32     lat;
    FLOAT32     lon;
    FLOAT32     alt;
} LATLONALT_FLOAT32, *PLATLONALT_FLOAT32, **PPLATLONALT_FLOAT32;

typedef struct _llf64               // llf64
{
    FLOAT64     lat;                // degrees (+/-90)
    FLOAT64     lon;                // degrees (+/-180)
} LLF64, *PLLF64, **PPLLF64;

typedef struct _llaf64              // llaf64
{
    FLOAT64     lat;                // degrees (+/-90)
    FLOAT64     lon;                // degrees (+/-180)
    FLOAT64     alt;                // meters (+/- a lot)
} LLAF64, *PLLAF64, **PPLLAF64;

// ZXY48 - used to mirror LatLonAlt with Lon as meters IIII.FF
typedef struct  ZXY48
{
    SIF48       z;
    SIF48       x;
    SIF48       y;
} ZXY48, *PZXY48, **PPZXY48;


typedef struct XYZWF32 {
    float x;
    float y;
    float z;
    float w;
}XYZWF32,*PXYZWF32,**PPXYZWF32;


// FLOAT64_VECTOR3 - <i,j,k> vector, hopefully normalized
typedef struct  _FLOAT64_VECTOR3
{
    FLOAT64     i ;
    FLOAT64     j ;
    FLOAT64     k ;
}   FLOAT64_VECTOR3, *PFLOAT64_VECTOR3 ;

// FLOAT64_VECTOR2 - <i,j> vector
typedef struct  _FLOAT64_VECTOR2
{
    FLOAT64     i ;
    FLOAT64     j ;
}   FLOAT64_VECTOR2, *PFLOAT64_VECTOR2 ;

// PBH32 & PBH16 - rotation parameters of an object

typedef struct  PBH32
{
    ANGL32  pitch;
    ANGL32  bank;
    ANGL32  heading;
} PBH32, *PPBH32, **PPPBH32;

typedef struct  PBH16
{
    ANGL16  pitch;
    ANGL16  bank;
    ANGL16  heading;
} PBH16, *PPBH16, **PPPBH16;


// LATLONBOX - a set of lat and lon values that defines a rectangular boundary
typedef struct LATLONBOX
{
    SIF48   top;
    ANGL48  left;
    SIF48   bottom;
    ANGL48  right;
} LATLONBOX, *PLATLONBOX, **PPLATLONBOX;

// LATLONRECT32 - a set of lat and lon values that defines a rectangular boundary
typedef struct LATLONRECT32
{
    SINT32  north;
    SINT32  south;
    ANGL32  east;
    ANGL32  west;
} LATLONRECT32, *PLATLONRECT32, **PPLATLONRECT32;

// LATLONRECT32 - a set of lat and lon values that defines a rectangular boundary
typedef struct LATLONRECTF64
{
    FLOAT64 north;
    FLOAT64 south;
    FLOAT64 east;
    FLOAT64 west;
} LATLONRECTF64, *PLATLONRECTF64, **PPLATLONRECTF64;

typedef struct LLAPBH
{
    LATLONALT   lla;
    PBH32       pbh;
}  LLAPBH, *PLLAPBH, **PPLLAPBH;

// LATLONALTPBH - Combination of LATLONALT & PBH32
typedef union LATLONALTPBH
{
    struct
    {
        SIF48       lat;
        ANGL48      lon;
        SIF48       alt;
        ANGL32      pitch;
        ANGL32      bank;
        ANGL32      heading;
    };

    struct
    {
        LATLONALT   lla;
        PBH32       pbh;
    };
} LATLONALTPBH, *PLATLONALTPBH, **PPLATLONALTPBH;

// LATLONALTTPO - Time, Position, Orientation using LatLonAlt for Position
typedef struct  LATLONALTTPO
{
    UINT32      time;

    union
    {
        struct
        {
            SIF48       lat;
            ANGL48      lon;
            SIF48       alt;
            ANGL32      pitch;
            ANGL32      bank;
            ANGL32      heading;
        };

        struct
        {
            LATLONALT   lla;
            PBH32       pbh;
        };

        LATLONALTPBH    llapbh;
    };
} LATLONALTTPO, *PLATLONALTTPO, **PPLATLONALTTPO;

// Data structures for BGL manipulation
typedef struct LLA2416
{
    char    stream[3+3+2];
} LLA2416, *PLLA2416, **PPLLA2416;

typedef struct LLA2424
{
    char    stream[3+3+3];
} LLA2424, *PLLA2424, **PPLLA2424;

// coordinates of a point in a cartesion system
typedef struct  XYZ16
{
    SINT16  x;
    SINT16  y;
    SINT16  z;
} XYZ16, *PXYZ16, **PPXYZ16;

typedef struct  XYZ32
{
    SINT32  x;
    SINT32  y;
    SINT32  z;
} XYZ32, *PXYZ32, **PPXYZ32;

typedef struct  _xyz_float32
{
    FLOAT32     x;
    FLOAT32     y;
    FLOAT32     z;
} XYZ_FLOAT32, *PXYZ_FLOAT32, **PPXYZ_FLOAT32;


// Same as above, but uses east, altit, north for field names
typedef struct  EAN32
{
    SINT32  east;
    SINT32  altit;
    SINT32  north;
} EAN32, *PEAN32, **PPEAN32;

// Coordinates of a point on the XZ plane

typedef struct  XZF32
{
    FLOAT32 x;
    FLOAT32 z;
} XZF32, *PXZF32, **PPXZF32;

typedef struct  XZ32
{
    SINT32  x;
    SINT32  z;
} XZ32, *PXZ32, **PPXZ32;

// same as above, but uses east, north for field names

typedef struct  EN32
{
    SINT32  east;
    SINT32  north;
} EN32, *PEN32, **PPEN32;

// ANGLSINCOS16 structure to hold data for an angle and it's trig functions
typedef struct  ANGLSINCOS16
{
    ANGL16  angle;
    SFRAC16 sine;
    SFRAC16 cosine;
} ANGLSINCOS16, *PANGLSINCOS16, **PPANGLSINCOS16;

// XYZPBH - defines the position of an object
typedef union   XYZPBH
{
    struct
    {
        SINT32  x;
        SINT32  y;
        SINT32  z;
        ANGL16  pitch;
        ANGL16  bank;
        ANGL16  heading;
    };

    struct
    {
        XYZ32   xyz;
        PBH16   pbh;
    };
} XYZPBH, *PXYZPBH, **PPXYZPBH;

// XYZTPO - same as above, but starts with a time-stamp field
// Notes: these structures will need to be reordered when
//  H2INc is fixed!
typedef struct  XYZTPO
{
    UINT32  time;

    union
    {
        struct
        {
            SINT32  x;
            SINT32  y;
            SINT32  z;
            ANGL16  pitch;
            ANGL16  bank;
            ANGL16  heading;
        };

        struct
        {
            XYZ32   xyz;
            PBH16   pbh;
        };

        XYZPBH  xyzpbh;
    };
} XYZTPO, *PXYZTPO, **PPXYZTPO;


// XYZF64_  - XYZ point(vector) in floating point
// XYZF64 - XYZ vector which includes XYZF64 as a part of union
// (needed to be changed together with XYZF64)

struct  XYZF64_
{
    FLOAT64 x;
    FLOAT64 y;
    FLOAT64 z;
};

//  XYZF64, *PXYZF64, **PPXYZF64;

typedef struct  XYZF64
{
    union
    {
        struct { FLOAT64   lon; FLOAT64 alt;      FLOAT64 lat; };
        struct { FLOAT64     x; FLOAT64  y;       FLOAT64   z; };
        struct { FLOAT64 pitch; FLOAT64 heading;  FLOAT64 bank;};
    };
}
XYZF64, *PXYZF64, **PPXYZF64,
POS3_FLOAT64,
VEL3_FLOAT64, *PVEL3_FLOAT64, **PPVEL3_FLOAT64,
ACC3_FLOAT64,
ROT3_FLOAT64,
ROV3_FLOAT64,
ROA3_FLOAT64;

//-----------------------------------------------


// XYF64 - XY point (vector in 2d space) in floating point
typedef struct XYF64                // xyf64
{
    FLOAT64 x;
    FLOAT64 y;
} XYF64, *PXYF64, **PPXYF64;


//----------------------------------------------------
// XYZF32 - XYZW point(vector) in floating point
typedef struct XYZF32 {
    FLOAT32 x;
    FLOAT32 y;
    FLOAT32 z;
    FLOAT32 w;
}   XYZF32, *PXYZF32;

#ifdef __cplusplus
struct CXYZF32:public XYZF32
{
    CXYZF32() { }
    CXYZF32(FLOAT32 _x, FLOAT32 _y, FLOAT32 _z, FLOAT32 _w=1.0f) {x = _x; y = _y; z = _z; w = _w;}
};
#endif
//---------------------------------------------------


typedef FLOAT64 ANGLF64;    // radians

// PBHF64 - Pitch/bank/heading in floating point radians
typedef struct PBHF64 {
    ANGLF64 pitch;
    ANGLF64 bank;
    ANGLF64 heading;
} PBHF64, *PPBHF64, **PPPBHF64;

// FLOAT32 matrix
typedef struct MATRIXF32
{
    union
    {
        struct { XYZF32 x,y,z,w;};
        struct
        {
            FLOAT32 m00, m01, m02, m03;
            FLOAT32 m10, m11, m12, m13;
            FLOAT32 m20, m21, m22, m23;
            FLOAT32 m30, m31, m32, m33;
        };
        struct
        {
            FLOAT32 _11, _12, _13, _14;
            FLOAT32 _21, _22, _23, _24;
            FLOAT32 _31, _32, _33, _34;
            FLOAT32 _41, _42, _43, _44;
        };
        FLOAT32 m[4][4];
        FLOAT32 mat[4][4];
    };
#ifdef __cplusplus
    FLOAT32& operator()(int iRow, int iColumn) { return m[iRow][iColumn]; }
    const FLOAT32& operator()(int iRow, int iColumn) const { return m[iRow][iColumn]; }
#endif
} MATRIXF32, *PMATRIXF32;

// map ROTMTRXF64 to a MATRIXF32
typedef MATRIXF32  ROTMTRXF64, *PROTMTRXF64;
// map QUATERN_ROTMAT to a MATRIXF32
typedef MATRIXF32  QUATERN_ROTMAT, *PQUATERN_ROTMAT;

// LLA_INFO - this struct contains a LATLONALT param and a user definable PVOID ptr
//  useful for an array of LATLONALTs that you also want to associate some additional data with
typedef struct  LLA_INFO
{
    union
    {
        struct
        {
            SIF48       lat;
            ANGL48      lon;
            SIF48       alt;
        };
        LATLONALT   lla;
    };
    PVOID               data_ptr;
} LLA_INFO, *PLLA_INFO, **PPLLA_INFO;


#pragma pack()

#endif
