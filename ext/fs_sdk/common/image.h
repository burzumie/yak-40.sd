/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: image.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef __FS_SDK_IMAGE
#define __FS_SDK_IMAGE

#pragma pack(4)

// enum constants for IMAGE.format
typedef enum IMG_FORMAT
{
    IMG_8_BIT_MONOCHROME = 0,
    IMG_8_BIT_INDEXED,
    IMG_15_BIT,             // 1555
    IMG_16_BIT,             // 565
    IMG_16A_BIT,            // 4444
    IMG_24_BIT,             // 888
    IMG_32_BIT,             // 888
    IMG_32A_BIT,            // 8888
    IMG_DXT1,               // DirectX Texture Compression DXT1
    IMG_DXT3,               // DirectX Texture Compression DXT3
    IMG_DUDV,               // Pertubation data
    IMG_MAX                 // keep this last
} IMG_FORMAT, *PIMG_FORMAT, **PPIMG_FORMAT;

// IMAGE - structure containing info pertaining to an offscreen image buffer
//         NOTE this structure should not change size compared to the FS98
//         version because gauges use this structure
typedef struct  IMAGE
{
    IMG_FORMAT  format:16;  // bit format for this image
    UINT32      flags:16;   // flags for this image
    PIXPOINT    dim;        // offscreen buffer size (viewable area)
    UINT32      pitch;      // offset to add to advance one scan line in image buffer
    RGBA*       palette;    // palette for IMG_8_BIT_INDEXED images (was rowtbl)
    UINT32      len;        // xxx
    PCHAR       image;      // pointer to image buffer
    PVOID       pdx;        // pointer to directx information
} IMAGE, *PIMAGE, **PPIMAGE;

typedef const IMAGE *PCIMAGE, **PPCIMAGE;

#define IMAGE_XY(p,x,y) ((BYTE *)(p)->image + ((p)->pitch * (y)) + (x))
#define IMAGE_PTR(p,y)  IMAGE_XY(p,0,y)

#pragma pack()

#endif
