/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: global.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_GLOBAL
#define _FS_SDK_GLOBAL

#pragma pack(4)

enum INFO_GEN {
	INFO_GEN_UNDEFINED = 0xff,
	INFO_GEN_NONE = 0x0,
	INFO_GEN_LATLON = 0x1,
	INFO_GEN_FRAMES_G = 0x2,
	INFO_GEN_LATLON_FRAMES_G = 0x3,
	INFO_GEN_MAX = 0x4,
};

enum EFIS_DENSITY {
  EFIS_DENSITY_UNKNOWN = 0xff,
  EFIS_DENSITY_THIN = 0x0,
  EFIS_DENSITY_MEDIUM = 0x1,
  EFIS_DENSITY_THICK = 0x2,
};

enum EFIS_DEPTH {
  EFIS_DEPTH_UNKNOWN = 0xff,
  EFIS_DEPTH_SHORT = 0x0,
  EFIS_DEPTH_MEDIUM = 0x1,
  EFIS_DEPTH_LONG = 0x2,
};

enum EFIS_NAVAID {
  EFIS_NAVAID_UNKNOWN = 0xff,
  EFIS_NAVAID_VOR_1 = 0x0,
  EFIS_NAVAID_VOR_2 = 0x1,
};

enum EFIS_TYPE {
  EFIS_TYPE_UNKNOWN = 0xff,
  EFIS_TYPE_RECT = 0x0,
  EFIS_TYPE_POLE = 0x1,
  EFIS_TYPE_ROAD = 0x2,
  EFIS_TYPE_HOOP = 0x3,
};

// prop type enum values
typedef enum PROP_TYPE
{
	PROP_TYPE_NONE,
	PROP_TYPE_FIXED,
	PROP_TYPE_MANUAL,
	PROP_TYPE_CHOICE
} PROP_TYPE;

typedef struct VOR_INFO_STRUCTURE {
	struct LATLONALT lla;
	unsigned short signal_strength;
	unsigned short magvar;
	unsigned char code;
	unsigned char trans_type;
	unsigned short localizer_direction;
	unsigned short localizer_width;
	unsigned short range;
	char ident[6];
	char vor_name[25];
	struct LATLONALT gs_lla;
	unsigned short ils_gs_slope;
	unsigned short dme_flags;
	struct LATLONALT dme_lla;
}VOR_INFO_STRUCTURE,*PVOR_INFO_STRUCTURE,**PPVOR_INFO_STRUCTURE;

typedef struct VOR_ADF_INFO {
	struct VOR_INFO_STRUCTURE* vor1_offset;
	struct VOR_INFO_STRUCTURE* vor2_offset;
}VOR_ADF_INFO,*PVOR_ADF_INFO,**PPVOR_ADF_INFO;

#ifndef _FS_SDK_SIM1
typedef struct _engine_fuel_vars
{
    ENUM        tank_selector;          //what tanks are selected to draw from
    FLAGS       tanks_used;             //what tanks are actually being drawn from
    UINT32      number_of_tanks_used;   //# of tanks being drawn from
    FLOAT64     fuelflow_pph;           //lbs/hr
    BOOL32      fuel_available;         //True or False in selected tanks
} ENG_FUEL_VARS, *PENG_FUEL_VARS, **PPENG_FUEL_VARS;
#endif
typedef struct  ENG
{
    SINT16      throttle_lvr_pos;       // throttle position (0-16384=>0-100%throttle position)
    UINT16      propeller_lvr_pos;      // propeller lever position (0-16384=>0-100%prop setting)
    UINT16      mixture_lvr_pos;        // mixture position (0-16384=>0-100%mixture position)
    ENUM16      starter;                // starter switch position

    BOOL16      combustion;             // combustion flag (combustion = TRUE)
    UINT16      n1_rpm;                 // gas producer RPM (0-16384=>0-100%RPM)
    UINT16      n2_rpm;                 // power RPM (0-16384=>0-100%RPM)
    UINT16      ff_pph;                 // fuel flow (0-16384=>0-100%)PPH (normalized to Lear 3500 lbs thrust)
    UINT32      ff_pph_actual;          // fuel flow (0-16384=>0-100%)PPH
    UINT16      ff_pph_ssl;             // fuel flow (0-16384=>0-100%)PPH, Standard sea level conditions
    UINT16      torque;                 // torque produced by engine (FT*LBS)
    UINT16      absorbed_torque;        // torque absorbed by propeller (FT*LBS)
    SINT32      thrust_lbs;             // thrust (LBS)
    ANGL16      blade_angle_pdeg;       // propeller blade angle (PDEG)

    BOOL16      lf_mag;                 // left magneto status (activated = 1)
    BOOL16      rt_mag;                 // right magneto status (activated = 1)
    BOOL16      anti_ice;               // anti ice switch
    UINT16      epr_bug_pos;            // epr governed engine setting (0-16384=>0-100%epr setting)
    UINT16      egt_bug_pos;            // egt governed engine setting (0-16384=>0-100%egt setting)

    UINT16      oil_tmp;                // oil temperature (degC) * 16384
    UINT16      oil_prs;                // oil pressure (PSI) * 16384
    UINT16      epr;                    // engine pressure ratio (0-65536=>0-6.4)
    UINT16      egt_degC;               // exhaust gas temperature (degC)
    UINT16      manifold_pressure;      // manifold pressure (0-65536=>0-64)

    SINT16      x_pos;                  // lateral engine position (IN)
    SINT16      y_pos;                  // vertical engine position (IN)
    SINT16      z_pos;                  // longitudinal engine position (IN)

    UINT16      rpm_scaler;             // *16384 gives actual prop rpm in dx (= 65535 for jets)
    PVOID       ext_ptr;                // engine structure extension pointer

    UINT32      oil_quantity;           //oil quantity (0-16384 => 0-100%)
    UINT32      engine_vibration;       //oil quantity (0-16384 => 0-5)
    UINT32      hydraulic_pressure;     //hydraulic pressure (0-16384 => 0-4 psi)
    UINT32      hydraulic_quantity;     //hydraulic quantity (0-16384 => 0-100%)
    FLOAT64     induced_velocity;       //induced velocity from propwash or jet blast

    FLOAT64     cyl_head_temp_degF;     //cylinder head temperature (deg F)

    UINT32      tot_ind;                // TOT * 16384, deg C
    UINT32      torque_ind;             // TQ * 16384, %
    UINT32      Wfpress_ind;            // Fuel Pressure * 16384, psi
    UINT32      electric_load;          // electrical load * 16384, % max load
    UINT32      xmsn_pressure;          // xmsn pressure * 16384, psi
    UINT32      xmsn_temperature;       // transmission temp * 16384, deg C
    UINT32      rotor_rpm;              // main rotor speed * 16384, % max

    ENG_FUEL_VARS   fuel_vars;

} ENG, *PENG, **PPENG;

typedef struct VISIBDEF {
	unsigned short visib;
}VISIBDEF,*PVISIBDEF,**PPVISIBDEF;

struct GLOBAL
{
	FSLINKAGE head;
	unsigned short UNUSED_sysmode;
	unsigned short UNUSED_sysmode_bitmap;
	int scale_overflow;
	int ground_alt;
	char startup_mode[260];
	int logflag;
	char logbook_filename[260];
	unsigned short logbook_time_day;
	unsigned short logbook_time_instrument;
	unsigned short logbook_time_night;
	unsigned short logbook_time_total;
	unsigned char clock_hour;
	unsigned char clock_minute;
	unsigned char clock_second;
	unsigned int zulu_hour;
	unsigned int zulu_minute;
	unsigned short zulu_day;
	unsigned short zulu_year;
	unsigned short sysclock_check;
	unsigned short dst_active_flag;
	unsigned short time_zone_ofs;
	unsigned short season_cycl;
	unsigned int do_chain_18;
	unsigned int do_chain_6;
	int do_chain_1;
	unsigned int elapsed_1hz_sync;
	struct UIF64 elapsed_seconds;
	int pause_request;
	int pause_flag;
	int sun_glare;
	int sim_disabled;
	unsigned int sim_frame_fract;
	int auto_coord;
	unsigned char panel_lights;
	unsigned char strobe_lights;
	unsigned char strobe_flash;
	int old_regen_flg;
	unsigned char seqsel;
	unsigned char landing_lights_available;
	int landing_lights;
	struct PBH32 UNUSED_landing_lights_pbh;
	unsigned int UNUSED_pitot_heat;
	unsigned short magvar;
	unsigned int oldtime;
	unsigned short tick18;
	short frame_fract;
	unsigned int frame_count;
	unsigned short zoom_view;
	unsigned short raw_zoom_view;
	unsigned int ground_velocity;
	unsigned int airspeed_true;
	unsigned int airspeed_indicated;
	unsigned int UNUSED_airspeed_true_calibrate;
	unsigned int UNUSED_barber_pole_aspd;
	unsigned int UNUSED_vertical_speed;
	double UNUSED_whiskey_compass_ind_deg;
	int atc_enabled;
	unsigned int color_var;
	unsigned char UNUSED01[44];
	unsigned int scale_factor;
	unsigned char UNUSED02[2];
	double dElapsedSeconds;
	unsigned short UNUSED_altimeter_barom_pressure;
	unsigned char mode_loaded_flag;
	int UNUSED_amps;
	unsigned short UNUSED_real_airframe;
	unsigned short UNUSED_real_tank_sel;
	unsigned short UNUSED_real_eng_stop;
	unsigned short UNUSED_real_flameout;
	unsigned short UNUSED_real_magnetos;
	unsigned short UNUSED_real_mixture;
	unsigned short repair_refuel;
	unsigned int repair_refuel_counter;
	unsigned short UNUSED_adf_freq;
	unsigned short UNUSED_com_freq;
	unsigned short UNUSED_nav1_freq;
	unsigned short UNUSED_nav2_freq;
	unsigned short UNUSED_transponder_code;
	unsigned short UNUSED_adf_ext_freq;
	int radios_25_khz;
	int adf_500_hz;
	unsigned char USED_com_freq_change;
	unsigned short com_text_rate;
	unsigned char emergency;
	unsigned char UNUSED_fs_clamp;
	unsigned char sim_on_ground;
	float rKeyRepeatTimer;
	unsigned char UNUSED_stall_warning;
	unsigned char UNUSED_overspeed_warning;
	char UNUSED_turn_coordinator_ball;
	unsigned short reliability_sense_unused;
	unsigned char panel_light_burnout;
	unsigned char UNUSED__navcur;
	unsigned char UNUSED__vorcur;
	int dmecur;
	unsigned short delta_heading_rate;
	int lens_flare;
	unsigned short loc_select;
	struct VOR_ADF_INFO* UNUSED_vor_info_ptr;
	unsigned char UNUSED_get_vor;
	unsigned char UNUSED_get_adf;
	unsigned char unused004;
	int g_fShutdown;
	void* g_pRadioSystemSave;
	int sky_textured;
	unsigned short ground_texture;
	unsigned short building_texture;
	int coord_set_method;
	int flDawnDuskEffects;
	unsigned short unused_bool16;
	unsigned char north_seasonal_lat;
	unsigned char south_seasonal_lat;
	short view_alt;
	unsigned short map_ground_textures;
	unsigned int old_grass;
	unsigned int old_sky;
	unsigned int old_skycol;
	unsigned int gndcol;
	unsigned int scolor_var;
	unsigned int old_citcol;
	unsigned int watcol;
	unsigned int old_mtncol;
	unsigned int old_pkcol;
	unsigned int c_water_map;
	unsigned int c_ground_map;
	unsigned int old_c_texture;
	unsigned char UNUSED03[100];
	unsigned short database_reinterrogate;
	unsigned char UNUSED04[262];
	int fulltower_force;
	unsigned short fulltower_cycl;
	unsigned short window_titles;
	unsigned short stars2;
	unsigned short see_self_cycl;
	unsigned char UNUSED05[28];
	struct LATLONALTTPO plane0time;
	struct LATLONALTTPO plane1time;
	struct LATLONALTTPO visual_time;
	unsigned char UNUSED39[24];
	int slew_scenery_edit;
	enum INFO_GEN slew_dig_gen;
	unsigned int UNUSED_slew_vel;
	enum INFO_GEN flight_dig_gen;
	unsigned short axis_cycl;
	unsigned int ac_category;
	unsigned char sim_airframe_type;
	unsigned char engine_type;
	int UNUSED_gear_type;
	unsigned char UNUSED_retractable_gear;
	unsigned short indicated_aspd;
	int UNUSED_replay_loop;
	void* UNUSED_replay_loop_ptr;
	unsigned int UNUSED_replay_loop_seconds;
	void* UNUSED_replay_play_ptr;
	void* UNUSED_replay_record_ptr;
	int replay_run;
	unsigned int replay_seconds;
	unsigned int UNUSED_replay_seconds_available;
	unsigned int UNUSED_replay_speed;
	char startup_demo[260];
	int demo_at_startup;
	void* demo_buf_ptr;
	void* demo_play_end;
	int demo_play_inhibited;
	int demo_play_keypress;
	int demo_play_loop;
	void* demo_play_ptr;
	int demo_play_run;
	int demo_rec_interval;
	int demo_rec_run;
	int UNUSED_aircraft_autopilot_available;
	int UNUSED_aircraft_vs_hold_available;
	int UNUSED_auto_throttle_aspd_available;
	int UNUSED_auto_throttle_mach_available;
	int UNUSED_auto_throttle_rpm_available;
	int UNUSED_aircraft_flaps_available;
	int UNUSED_aircraft_stall_horn_available;
	int UNUSED_aircraft_engine_mixture_available;
	int UNUSED_aircraft_carb_heat_available;
	int UNUSED_aircraft_pitot_heat_available;
	int UNUSED_aircraft_spoiler_available;
	int aircraft_is_tail_dragger;
	int UNUSED_aircraft_strobes_available;
	enum PROP_TYPE aircraft_prop_type_available;
	int aircraft_toe_brakes_available;
	int UNUSED_aircraft_nav1_available;
	int UNUSED_aircraft_nav2_available;
	int UNUSED_aircraft_markers_available;
	int UNUSED_aircraft_nav1_obs_available;
	int UNUSED_aircraft_nav2_obs_available;
	int UNUSED_aircraft_vor2_gauge_available;
	int UNUSED_aircraft_gyro_drift_available;
	int autop_master;
	int wing_leveler;
	int nav1_lock;
	int heading_lock;
	unsigned short heading_lock_dir;
	int altit_lock;
	unsigned int altit_lock_var;
	int att_hold;
	int airspeed_hold;
	unsigned int airspeed_hold_var;
	int mach_hold;
	unsigned int mach_hold_var;
	int vertical_hold;
	int vertical_hold_var;
	int rpm_hold;
	unsigned int rpm_hold_var;
	int gs_hold;
	int apr_hold;
	int bc_hold;
	int yaw_damper;
	int UNUSED_auto_takeoff_power_active;
	int UNUSED_auto_throttle_arm;
	int UNUSED_analysis_mode;
	int UNUSED_analysis_course_record;
	int UNUSED_analysis_course_display;
	int UNUSED_analysis_course_clear;
	int UNUSED_analysis_course_resolution;
	int UNUSED_analysis_course_depth;
	int crash_model_available;
	unsigned short crash_cycl;
	unsigned short crash_flag;
	int crash_type;
	unsigned char crash_seq;
	int crash_protector;
	unsigned char crash_cracks;
	short UNUSED_crash_vertical_speed;
	short UNUSED_crash_airspeed_true;
	unsigned short crash_aircraft_damage;
	unsigned short crash_off_runway;
	unsigned short crash_with_other;
	int UNUSED_land_me;
	int UNUSED_land_me_available;
	unsigned char UNUSED06[42];
	unsigned int UNUSED_fuel_flow;
	unsigned short UNUSED_prop_length;
	unsigned short oil_amount;
	int engine_control_select;
	unsigned char UNUSED07[4];
	ENG eng1;
	ENG eng2;
	ENG eng3;
	ENG eng4;
	unsigned short number_of_engines;
	int UNUSED_prop_adv_sel;
	short fuel_wgt_per_gal;
	int fuel_tank_selector;
	int cross_feed;
	short throttle_lower_limit;
	unsigned char UNUSED08[8];
	unsigned int mach_max_operate;
	int UNUSED_autopilot_altitude_manually_tunable;
	int UNUSED_autopilot_heading_manually_tunable;
	double UNUSED_suction_pressure;
	int sound_master_request;
	int sound_master;
	int light_source_x;
	int light_source_y;
	int light_source_z;
	unsigned short shadow_density;
	unsigned char UNUSED09[30];
	unsigned short moonlight_sky;
	int show_stars;
	int approach_lights;
	int image_complexity;
	unsigned char UNUSED_pp_adf;
	unsigned char UNUSED_pp_air;
	unsigned char UNUSED_pp_altim;
	unsigned char UNUSED_pp_attitude;
	unsigned char UNUSED_pp_com;
	unsigned char UNUSED_pp_compass;
	unsigned char UNUSED_pp_elect;
	unsigned char UNUSED_pp_engine;
	unsigned char UNUSED_pp_fuel_ind;
	unsigned char UNUSED_pp_heading;
	unsigned char UNUSED_pp_vert_vel;
	unsigned char UNUSED_pp_xpndr;
	unsigned char UNUSED_pp_nav;
	unsigned char UNUSED_pp_pitot;
	unsigned char UNUSED_pp_turn_coord;
	unsigned char UNUSED_pp_vacuum;
	unsigned int static_cg_to_ground;
	int demo_check;
	int sound_check;
	int unused_weather_check;
	int show_opening;
	int quicktips;
	int panel_stretching;
	int panel_masking;
	int ask_on_exit;
	int pause_on_lost_focus;
	int advanced_weather_flag;
	int UNUSED_lo_res_render;
	int perf_mode;
	int fullscreen_flag;
	unsigned short UNUSED_marker_beacon_inner;
	unsigned short UNUSED_marker_beacon_middle;
	unsigned short UNUSED_marker_beacon_outer;
	short yoke_pos_y;
	short yoke_ind_y;
	short yoke_pos_x;
	short yoke_ind_x;
	short rudped_pos;
	short rudped_ind;
	unsigned short v400_throtl_pos;
	short elvtrm_pos;
	short elvtrm_ind;
	unsigned short lbrake_pos;
	unsigned short rbrake_pos;
	unsigned short parking_brk_pos;
	unsigned short brake_ind;
	unsigned int UNUSED_spoilers_armed;
	unsigned int UNUSED_spoilers_handle_pos;
	unsigned int spoilers_left_pos;
	unsigned int spoilers_right_pos;
	unsigned int flaps_handle_pos;
	unsigned int flaps_left_pos;
	unsigned int flaps_right_pos;
	unsigned int UNUSED_gear_handle_pos;
	unsigned int gear_pos1;
	unsigned int gear_pos2;
	unsigned int gear_pos3;
	unsigned int unlimited_vis;
	unsigned int UNUSED_gear_failure;
	unsigned short kbd_brakes;
	unsigned short kbd_sensail;
	unsigned short kbd_senselev;
	unsigned short kbd_sensrud;
	unsigned char UNUSED10[8];
	unsigned short units_of_measure;
	unsigned short sim_speed;
	unsigned char UNUSED11[33];
	short UNUSED_gyro_drift_error;
	unsigned char sound_action;
	unsigned char dmespeed;
	unsigned char dmespeed2;
	unsigned char UNUSED12[3];
	unsigned short realism_systems;
	unsigned char UNUSED13[33];
	unsigned int UNUSED_adf_card_degrees;
	unsigned char UNUSED14[27];
	unsigned int unused_txtr_seg;
	unsigned short texture_flag;
	unsigned short texture_quality;
	int texture_ix;
	int texture_iy;
	int texture_iz;
	unsigned short haze_factor;
	unsigned int texture_lx;
	unsigned int texture_lz;
	unsigned int texture_ux;
	unsigned int texture_uz;
	unsigned short image_quality;
	unsigned short texture_avail;
	unsigned char UNUSED15[2];
	unsigned short brightness;
	unsigned short color_range;
	unsigned short color_range_set;
	unsigned short system_colors;
	unsigned short lcolor_intensity;
	unsigned int lcolor_reserved;
	unsigned short scolor_intensity;
	unsigned int scolor_reserved;
	unsigned char UNUSED16[2];
	unsigned short image_smoothing;
	unsigned char UNUSED17[8];
	struct XYZ32 UNUSED_visual_position;
	unsigned char UNUSED18[36];
	int shadow_draw_enable;
	int shadow_draw_enable_master;
	unsigned int g_lightStates;
	unsigned short xfrm_zoom;
	unsigned short xfrm_zoom_c;
	unsigned short xfrm_zoom_f;
	unsigned char UNUSED19[50];
	unsigned int gen_model;
	unsigned int rbias;
	union LATLONALTPBH tower_position;
	union LATLONALTPBH track_position;
	int hemisphere_NS;
	int hemisphere_EW;
	unsigned char UNUSED20[16];
	unsigned short smooth_view_trans;
	unsigned short track_plane1;
	unsigned short oriair_flg;
	unsigned short map_orientation;
	unsigned char UNUSED21[1];
	unsigned short shadow_flag;
	unsigned char UNUSED22[2];
	unsigned short map_view;
	unsigned char UNUSED23[52];
	unsigned short out_of_range;
	unsigned short visual_lat_mirror;
	unsigned short visual_lon_mirror;
	unsigned short visual_alt_mirror;
	unsigned char UNUSED24[78];
	int bEfisActive;
	enum EFIS_DENSITY eEfisDensity;
	enum EFIS_DEPTH eEfisDepth;
	unsigned int fEfisInitFlag;
	enum EFIS_NAVAID eEfisNavaid;
	enum EFIS_TYPE eEfisType;
	float fEfisAltitude;
	unsigned char UNUSED25[16];
	VISIBDEF average_visib_filtered;
	VISIBDEF average_visib;
	unsigned char UNUSED26[738];
	unsigned short time_of_day_master;
	unsigned char UNUSED27[2];
	unsigned short time_of_day;
	unsigned char UNUSED28[2];
	unsigned short beacon_counter1;
	unsigned short beacon_counter2;
	unsigned short beacon_counter3;
	unsigned short beacon_counter4;
	unsigned short beacon_counter5;
	int dyn_air_traffic;
	int dyn_ground_traffic;
	int dyn_service_traffic;
	int dyn_other_traffic;
	int dyn_scenery_density;
	PAWIND pcurrent_awind;
	PVIEWBLOCK pcurrent_viewblock;
	PAWIND pactive_awind;
	PVIEWBLOCK pactive_viewblock;
	unsigned int c_border_active_day;
	unsigned int c_border_active_night;
	unsigned int c_border_inactive;
	unsigned short enable_track_mode;
	unsigned short ground_shadows;
	unsigned short aircraft_shadows;
	unsigned char UNUSED29[16];
	unsigned short aircraft_texture;
	unsigned short UNUSED_propeller_visible;
	short UNUSED_g_force;
	unsigned short water_texture;
	unsigned short UNUSED_angle_of_attack_ind;
	short ailer_pos;
	short elev_pos;
	short rudder_pos;
	unsigned short mach;
	unsigned char UNUSED30[10];
	void* sim_state_ptr;
	int draw_user_aircraft;
	unsigned char OBSOLETE_acc_plane0[12];
	unsigned char UNUSED31[2];
	int UNUSED_desired_altitude_set;
	unsigned int UNUSED_desired_heading_set;
	union SIF48 UNUSED_desired_altitude;
	unsigned int UNUSED_desired_heading;
	unsigned char UNUSED32[9];
	unsigned short gear_smoke;
	unsigned char UNUSED33[87];
	int stn_load_aircraft_flag;
	int stn_load_dynamic_scenery_flag;
	int stn_load_weather_flag;
	int stn_load_window_positions_flag;
	int continuous_scroll_enabled;
	int UNUSED_GPS_drives_NAV1;
	unsigned char UNUSED34[2];
	unsigned short version_ident;
	unsigned char UNUSED35[18];
	unsigned int half_second_timer;
	unsigned int one_second_timer;
	unsigned int two_second_timer;
	unsigned int six_second_timer;
	unsigned int language_locale_id;
	int	foreign_translator;
	int	foreign_translator_available;
	unsigned int alpha_blend;
	unsigned char UNUSED36[2];
	unsigned int g2d_flags;
	int bContinueProcessingCurrentFiberJob;
	__int64 i64TimeToEndFiberJob;
	__int64 i64TimeOfLastFiberYieldCall;
	unsigned char UNUSED38[36];
	unsigned char nav1_commnav_test;
	unsigned char nav2_commnav_test;
	unsigned char standby_COM1_frequency_available;
	unsigned char standby_NAV1_frequency_available;
	unsigned char standby_COM2_frequency_available;
	unsigned char standby_NAV2_frequency_available;
	unsigned int texture_bandwidth_mult;
};

#pragma pack()

#endif