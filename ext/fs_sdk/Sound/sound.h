/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: sound.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_SOUND
#define _FS_SDK_SOUND

#pragma pack(4)

enum SOUND_ID {
  SOUND_ID_INVALID = 0xff,
  SOUND_WIND = 0x0,
  SOUND_MORSE = 0x1,
  SOUND_AP_DISENGAGE_WARNING = 0x2,
  SOUND_GEAR_UP_WARNING = 0x3,
  SOUND_GLIDESLOPE_WARNING = 0x4,
  SOUND_STALL_WARNING = 0x5,
  SOUND_OVERSPEED_WARNING = 0x6,
  SOUND_GROUND = 0x7,
  SOUND_CRASH = 0x8,
  SOUND_GEAR_UP = 0x9,
  SOUND_GEAR_DOWN = 0xa,
  SOUND_FLAPS = 0xb,
  SOUND_MARKERS = 0xc,
  SOUND_GROUNDROLL = 0xd,
  SOUND_NOSE = 0xe,
  SOUND_OVERSTRESS = 0xf,
  SOUND_AMPHIB_GEAR_UP_WARNING = 0x10,
  SOUND_AMPHIB_GEAR_DOWN_WARNING = 0x11,
  SOUND_GYRO = 0x12,
  SOUND_EXIT_OPEN = 0x13,
  SOUND_EXIT_CLOSE = 0x14,
  SOUND_CLUTCH = 0x15,
  SOUND_ID_COUNT = 0x16,
};

enum SOUND_ADDOBJ_ID {
  SOUND_ENGINE = 0x0,
  SOUND_THUNDER = 0x1,
  SOUND_PRECIP = 0x2,
  SOUND_AI = 0x3,
  SOUND_MACHGUN = 0x4,
  SOUND_EXPLOSION = 0x5,
  SOUND_DAMAGE = 0x6,
  SOUND_CLOSEPROX = 0x7,
  SOUND_BAILOUT = 0x8,
  SOUND_BAILOUTLOOP = 0x9,
  SOUND_STRUCTURE = 0xa,
  SOUND_ADDOBJ_COUNT = 0xb,
};

typedef struct SOUND_ADDOBJ_ARGS {
    enum SOUND_ADDOBJ_ID id;
    void* handle;
    void* params;
    unsigned int flags;
    union LATLONALTPBH* lla;
    struct XYZF64* plane0_xyz;
}SOUND_ADDOBJ_ARGS,*PSOUND_ADDOBJ_ARGS,**PPSOUND_ADDOBJ_ARGS;

typedef struct SOUND_SETTINGS {
    unsigned int uSoundQuality;
}SOUND_SETTINGS,*PSOUND_SETTINGS,**PPSOUND_SETTINGS;

struct SOUND {
	FSLINKAGE head;
    void  (FSAPI *sound_startup)();
    void  (FSAPI *sound_shutdown)();
    void  (FSAPI *sound_frame)();
    void  (FSAPI *sound_faders_set)(double*);
    void  (FSAPI *sound_faders_get)(double*);
    void  (FSAPI *sound_settings_set)(struct SOUND_SETTINGS*);
    void  (FSAPI *sound_settings_get)(struct SOUND_SETTINGS*);
    void  (FSAPI *sound_settings_default_get)(unsigned int, struct SOUND_SETTINGS*);
    int  (FSAPI *sound_morse_ident_status)();
    long  (FSAPI *sound_trigger)(enum SOUND_ID, void*);
    void  (FSAPI *sound_addobj)(struct SOUND_ADDOBJ_ARGS*);
};

#pragma pack()

#endif
