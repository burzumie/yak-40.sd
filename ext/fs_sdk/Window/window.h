/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: window.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_WINDOW
#define _FS_SDK_WINDOW

#include"..\common\awind.h"

#pragma pack(4)

enum G2DFONT {
  G2DFONT_SMALL = 0x0,
  G2DFONT_FIXED = 0x1,
  G2DFONT_CUSTOM_NORMAL = 0x2,
  G2DFONT_CUSTOM_BOLD = 0x3,
};

enum MSG_TYPE_ENUM {
  MSG_NONE = 0x0,
  MSG_COMM_SEND = 0x1,
  MSG_COMM_RECEIVE = 0x2,
  MSG_ATIS = 0x3,
  MSG_INSTRUCTOR = 0x4,
  MSG_GENERIC_PRINT = 0x5,
  MSG_GENERIC_SCROLL = 0x6,
  MSG_MENU_TITLE = 0x7,
  MSG_MENU_OPTION = 0x8,
  MSG_DAMAGE_PLAYER = 0x9,
  MSG_DAMAGE_VICTIM = 0xa,
  MSG_CFS_CDS = 0xb,
  MSG_WAVECAPTION = 0xc,
  MSG_DEBUG = 0xd,
  MSG_LESSON_GOAL = 0xe,
  MSG_MAX = 0xf,
};

typedef struct MSG_WIN_DEFAULTS {
    struct UNIBOX uni;
    struct PIXPOINT minLines;
    struct PIXPOINT maxLines;
    unsigned int maxVScrollLines;
    enum G2DFONT eFont;
    unsigned int uBackColor;
    int bEnableVScroll;
    int bTitle;
    int bTransparent;
    int bAutoSize;
    int bAutoShow;
    int bInQueueTimeouts;
    int bDontSaveStateInFlt;
}MSG_WIN_DEFAULTS,*PMSG_WIN_DEFAULTS,**PPMSG_WIN_DEFAULTS;

struct WINDOW {
	FSLINKAGE		head;
	void			(FSAPI *window_shutdown)();
    void			(FSAPI *window_mgr)(int);
    void			(FSAPI *windows_blacken)(int);
    void			(FSAPI *view_source_select_prev)();
    void			(FSAPI *view_source_select)();
    struct AWIND*	(FSAPI *view_window_open)(enum VIEW_MODE);
    struct AWIND*	(FSAPI *view_window_open_aux)(enum VIEW_MODE, int, int);
    void			(FSAPI *view_window_close)(struct AWIND*);
    void			(FSAPI *view_window_select)(struct AWIND*);
    void			(FSAPI *view_window_fullscreen_panel_snap)(struct AWIND*);
    void  			(FSAPI *view_window_fullscreen_toggle)(struct AWIND*);
    void  			(FSAPI *view_window_panel_hud_toggle)(struct AWIND*);
    void  			(FSAPI *view_window_mode_set)(struct AWIND*, enum VIEW_MODE);
    void  			(FSAPI *view_window_dir_set)(struct AWIND*, enum VIEW_DIR);
    void  			(FSAPI *view_window_camera_set)(struct AWIND*, struct LATLONALT*, struct PBH32*, struct MATRIXF32*, unsigned int);
    void  			(FSAPI *view_window_restore_saved)(struct AWIND*);
    void  			(FSAPI *view_source_set)(enum VIEW_MODE);
    void  			(FSAPI *window_on)(struct AWIND*);
    void  			(FSAPI *window_off)(struct AWIND*);
    unsigned int	(FSAPI *window_create)(struct AWIND**, char*, enum WINDOW_TYPE);
    unsigned int	(FSAPI *window_destroy)(struct AWIND**);
    unsigned int	(FSAPI *window_list_get)(struct AWIND***, unsigned int*, enum WINDOW_TYPE);
    unsigned int	(FSAPI *window_list_free)(struct AWIND***);
    unsigned int	(FSAPI *window_next)(struct AWIND**, struct AWIND*, enum WINDOW_TYPE);
    unsigned int	(FSAPI *window_previous)(struct AWIND**, struct AWIND*, enum WINDOW_TYPE);
    struct AWIND*	(FSAPI *window_find_by_id)(unsigned int, enum WINDOW_TYPE);
    void			(FSAPI *window_setup_viewblock)();
    int				(FSAPI *SupportsVirtualCockpit)();
    struct AWIND*	(FSAPI *ask_view_window_for_panel)(unsigned int);
    void			(FSAPI *open_view_window_for_panel)(enum VIEW_DIR, int);
    void			(FSAPI *notify_panel_close)();
    void			(FSAPI *panel_windows_toggle)();
    struct AWIND*	(FSAPI *view_window_start_cinematic)(enum VIEW_MODE);
    void			(FSAPI *view_window_stop_cinematic)();
    void			(FSAPI *view_window_dir_reset)(struct AWIND*);
    void			(FSAPI *view_window_snap)(struct AWIND*, int);
    void			(FSAPI *view_window_pan)(struct AWIND*, int);
    void			(FSAPI *view_window_pan_fine)(struct AWIND*, int);
    void			(FSAPI *view_window_pan_reset)(struct AWIND*);
    void			(FSAPI *view_window_tilt_set)(struct AWIND*, int);
    void			(FSAPI *view_window_pitch_axis)(struct AWIND*, unsigned int);
    void			(FSAPI *view_window_heading_axis)(struct AWIND*, unsigned int);
    void			(FSAPI *view_window_tilt_axis)(struct AWIND*, unsigned int);
    struct AWIND*	(FSAPI *get_default_3d_window)();
    unsigned int	(FSAPI *show_message)(char*, enum MSG_TYPE_ENUM, unsigned int, int);
    void			(FSAPI *clear_messages)(enum MSG_TYPE_ENUM);
    void			(FSAPI *clear_all_messages)();
    int				(FSAPI *messages_in_queue)(enum MSG_TYPE_ENUM);
    void*			(FSAPI *create_message_window)(char*, struct MSG_WIN_DEFAULTS*);
    void			(FSAPI *destroy_message_window)(void*);
    union LATLONALTPBH*  (FSAPI *ears_llapbh_get)(int*);
    void			(FSAPI *view_chase_target_set)(unsigned int);
    void			(FSAPI *view_chase_target_next)();
    void			(FSAPI *view_chase_target_prev)();
    void			(FSAPI *view_chase_toggle)();
    void			(FSAPI *view_mouse_hit_test_begin)();
    void			(FSAPI *view_mouse_hit_test_end)();
    void*			(FSAPI *ViewAnimateCreate)(void*);
    void			(FSAPI *ViewAnimateDestroy)(void*);
    unsigned short  (FSAPI *view_window_zoom_get)();
    void			(FSAPI *view_window_zoom_set)(unsigned short);
};

#pragma pack()  
                
#endif          