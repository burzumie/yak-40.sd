/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: multiplayer.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_MULTIPLAYER
#define _FS_SDK_MULTIPLAYER

#pragma pack(4)

enum MULTIPLAYER_PACKET_ID {
	MULTIPLAYER_PACKET_ID_BASE = 0x1000,
	MULTIPLAYER_PACKET_ID_PARAMS = 0x1000,
	MULTIPLAYER_PACKET_ID_REQUEST_OBSERVER_MODE = 0x1001,
	MULTIPLAYER_PACKET_ID_OBSERVER_CHANGE_REFUSED = 0x1002,
	MULTIPLAYER_PACKET_ID_CHANGE_PLANE = 0x1003,
	MULTIPLAYER_PACKET_ID_REMOTE_PLANE_UNKNOWN = 0x1004,
	MULTIPLAYER_PACKET_ID_PLAYER_CRASH = 0x1005,
	MULTIPLAYER_PACKET_ID_HOST_QUIT = 0x1006,
	MULTIPLAYER_PACKET_ID_REQUEST_SITUATION = 0x1007,
	MULTIPLAYER_PACKET_ID_SITUATION_DATA = 0x1008,
	MULTIPLAYER_PACKET_ID_TEXTURE_REQUEST_LIST = 0x1009,
	MULTIPLAYER_PACKET_ID_LEAVE_SESSION = 0x100a,
	MULTIPLAYER_PACKET_ID_SYNC_INFORMATION = 0x100b,
	MULTIPLAYER_PACKET_ID_POSITION_LLAPBH = 0x100c,
	MULTIPLAYER_PACKET_ID_POSITION_VELOCITY = 0x100d,
	MULTIPLAYER_PACKET_ID_INITIALIZE_AIRCRAFT_SEND = 0x100e,
	MULTIPLAYER_PACKET_ID_AIRCRAFT_SEGMENT = 0x100f,
	MULTIPLAYER_PACKET_ID_AIRCRAFT_CANCEL = 0x1010,
	MULTIPLAYER_PACKET_ID_INITIALIZE_TEXTURE_SEND = 0x1011,
	MULTIPLAYER_PACKET_ID_TEXTURE_SEGMENT = 0x1012,
	MULTIPLAYER_PACKET_ID_TEXTURE_CANCEL = 0x1013,
	MULTIPLAYER_PACKET_ID_TOGGLE_PAUSE_REQUEST = 0x1014,
	MULTIPLAYER_PACKET_ID_GAME_PAUSE_STATE = 0x1015,
	MULTIPLAYER_PACKET_ID_INSTRUCTOR_AIRCRAFT_DATA = 0x1016,
	MULTIPLAYER_PACKET_NON_MULTIPLAYER_PACKET_BASE = 0x1017,
	MPCHAT_PACKET_ID_CHAT_TEXT_SEND = 0x1017,
	MULTIPLAYER_PACKET_ID_ASSIGN_DAMAGE_TO_SYSTEMS = 0x1018,
	MULTIPLAYER_PACKET_ID_SHOWDAMAGE = 0x1019,
	MULTIPLAYER_PACKET_ID_WEATHER_OBSV = 0x101a,
	MULTIPLAYER_PACKET_ID_FAILURES_DATA = 0x101b,
	MULTIPLAYER_PACKET_ID_RESET_FLIGHT = 0x101c,
	MULTIPLAYER_PACKET_ID_MAX = 0x101d
};

enum MP_PACKET_CALLBACK_TYPE {
	MP_CALLBACK_DIRECTPLAY = 0x0,
	MP_CALLBACK_MP_MULTIPLAYER = 0x1,
	MP_CALLBACK_NON_MULTIPLAYER = 0x2
};

typedef struct MP_PACKET_CALLBACK_DATA {
    unsigned int from_id;
    enum MULTIPLAYER_PACKET_ID packet_id;
    void* packet_data_ptr;
    unsigned int packet_data_size;
    double receive_time;
}MP_PACKET_CALLBACK_DATA,*PMP_PACKET_CALLBACK_DATA,**PPMP_PACKET_CALLBACK_DATA;

typedef struct VELOCITY_BLOCK {
    double lat_velocity;
    double lon_velocity;
    double alt_velocity;
}VELOCITY_BLOCK,*PVELOCITY_BLOCK,**PPVELOCITY_BLOCK;

typedef struct ANGULAR_VELOCITY_BLOCK {
    double pitch_velocity;
    double bank_velocity;
    double heading_velocity;
}ANGULAR_VELOCITY_BLOCK,*PANGULAR_VELOCITY_BLOCK,**PPANGULAR_VELOCITY_BLOCK;

typedef struct MP_POSITION_INFO {
    double time;
    union LATLONALTPBH llapbh;
    struct VELOCITY_BLOCK linear_velocity;
    struct ANGULAR_VELOCITY_BLOCK angular_velocity;
    struct VELOCITY_BLOCK linear_correction_velocity;
    struct ANGULAR_VELOCITY_BLOCK angular_correction_velocity;
}MP_POSITION_INFO,*PMP_POSITION_INFO,**PPMP_POSITION_INFO;

typedef struct PLANE_INFORMATION {
    void* paircraft_container;
    unsigned int dwBackCompat[2];
    int container_in_use;
    unsigned int plane_radius;
    unsigned int Ground_velocity;
    int Engine_type;
}PLANE_INFORMATION,*PPLANE_INFORMATION,**PPPLANE_INFORMATION;

typedef struct PLAYER_INFO {
    char* player_name;
    int bPlayerDrewLastFrame;
    struct PLANE_INFORMATION plane_info;
    struct MP_POSITION_INFO plane_location;
    struct _llaf64 llaPlaneExtrapolated;
    unsigned int delta_time;
    double f_delta_time;
    unsigned int Deaths;
    unsigned int Kills;
    char* pCurrentAircraft;
    unsigned int TeamID;
    char DamageProfilePath[261];
    unsigned int time_of_last_player_type_query;
    int bHostIsRemovingThisPlayer;
    int bPlayerExited;
    int bPlayerInReadyRoom;
    int bPlayerReady;
    unsigned int last_reported_send_time;
    unsigned int last_receive_time;
    unsigned int time_samples;
    unsigned int last_location_packet_index;
    int received_velocity_is_new;
    int jk_remaining_collision_avoidance_time;
    int location_has_been_received;
    unsigned int distance_to_this_plane;
    union LATLONALTPBH expected_local_plane_location;
    struct MP_POSITION_INFO sent_plane_location;
    double fTimeHeartbeatSent;
    unsigned int StartingAngle;
    unsigned int uTimesUserEnteredSuspendedState;
}PLAYER_INFO,*PPLAYER_INFO,**PPPLAYER_INFO;

struct MULTIPLAYER {
	FSLINKAGE			head;
    int					(FSAPI *DoMPHostDlg)();
    int					(FSAPI *DoMPClientSettingsDlg)();
    int					(FSAPI *DoMPHostSettingsDlg)();
    int					(FSAPI *multiplayer_dialog_process)();
    int					(FSAPI *multiplayer_instructor_dialog_process)();
    int					(FSAPI *multiplayer_host_instructor_dialog_process)();
    int					(FSAPI *multiplayer_settings_dialog_process)();
    void*				(FSAPI *multiplayer_packet_callback_add)(enum MP_PACKET_CALLBACK_TYPE, enum MULTIPLAYER_PACKET_ID, void  (*)(struct MP_PACKET_CALLBACK_DATA*, void*, void*), void*, void*);
    void				(FSAPI *multiplayer_packet_callback_remove)(enum MP_PACKET_CALLBACK_TYPE, enum MULTIPLAYER_PACKET_ID, void*);
    unsigned int		(FSAPI *multiplayer_packet_send)(unsigned int, void*, unsigned int, unsigned int);
    unsigned int		(FSAPI *multiplayer_packet_send_EX)(unsigned int, enum MULTIPLAYER_PACKET_ID, void*, unsigned int, unsigned int);
    unsigned int		(FSAPI *multiplayer_player_count_get)();
    unsigned int		(FSAPI *multiplayer_player_list_enumerate)(void  (*)(struct PLAYER_INFO*, void*, void*), void*, void*);
    struct PLAYER_INFO* (FSAPI *multiplayer_find_player_by_id)(unsigned int);
    unsigned int		(FSAPI *multiplayer_state_get)();
    double				(FSAPI *multiplayer_time_get)();
    double				(FSAPI *multiplayer_sent_data_KBPS_get)();
    unsigned int		(FSAPI *multiplayer_dme1_speed_override)(unsigned int*);
    unsigned int		(FSAPI *multiplayer_dme1_distance_override)(unsigned int*);
    void				(FSAPI *multiplayer_send_game_pause_state)(int);
    int					(FSAPI *multiplayer_IsFlightInstructorSession)();
    int					(FSAPI *multiplayer_UserIsFlightInstructor)();
    void*				(FSAPI *multiplayer_get_network_object)();
    void*				(FSAPI *multiplayer_get_player_context)(unsigned int);
};

#pragma pack()

#endif

