/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: acontain.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_ACONTAIN
#define _FS_SDK_ACONTAIN

#pragma pack(4)

enum VISUAL_MODEL_PARAM {
  param_unknown = 0x0,
  param_radius = 0x1,
  param_product_code = 0x2,
  param_lla = 0x3,
  param_pbh = 0x4,
  param_llapbh = 0x5,
  param_gen_model = 0x6,
  param_cfs_hdr = 0x7,
  param_prop_visible = 0x8,
  param_bomb_rocket_visible = 0x9,
  param_parts_visible = 0xa,
  param_l_gear = 0xb,
  param_r_gear = 0xc,
  param_c_gear = 0xd,
  param_nnumber = 0xe,
  param_nnumber_color = 0xf,
  param_nnumber_font = 0x10,
  param_l_spoiler = 0x11,
  param_r_spoiler = 0x12,
  param_l_thrust_rev = 0x13,
  param_r_thrust_rev = 0x14,
  param_visor = 0x15,
  param_nose = 0x16,
  param_engine0 = 0x17,
  param_engine1 = 0x18,
  param_engine2 = 0x19,
  param_engine3 = 0x1a,
  param_engine0_rpm = 0x1b,
  param_engine1_rpm = 0x1c,
  param_prop0_pos = 0x1d,
  param_prop1_pos = 0x1e,
  param_elvtrim = 0x1f,
  param_elevon0 = 0x20,
  param_elevon1 = 0x21,
  param_elevon2 = 0x22,
  param_elevon3 = 0x23,
  param_elevon4 = 0x24,
  param_elevon5 = 0x25,
  param_c_wheel = 0x26,
  param_gear_smoke = 0x27,
  param_left_ailer = 0x28,
  param_right_ailer = 0x29,
  param_left_flap = 0x2a,
  param_flaps = 0x2b,
  param_right_flap = 0x2c,
  param_elevator = 0x2d,
  param_rudder = 0x2e,
  param_lightStates = 0x2f,
  param_afterburner = 0x30,
  param_bell_spotlight_pbh = 0x31,
  param_none = 0x32,
  param_l_wingfold = 0x33,
  param_r_wingfold = 0x34,
  param_l_pontoon = 0x35,
  param_r_pontoon = 0x36,
  param_cowling = 0x37,
  param_cowling1 = 0x38,
  param_f_canopy = 0x39,
  param_r_canopy = 0x3a,
  param_pilot = 0x3b,
  param_gunner0 = 0x3c,
  param_gunner1 = 0x3d,
  param_gunner2 = 0x3e,
  param_tailhook = 0x3f,
  param_l_flap_key = 0x40,
  param_r_flap_key = 0x41,
  param_userdefined0 = 0x42,
  param_userdefined1 = 0x43,
  param_userdefined2 = 0x44,
  param_userdefined3 = 0x45,
  param_userdefined4 = 0x46,
  param_userdefined5 = 0x47,
  param_userdefined6 = 0x48,
  param_userdefined7 = 0x49,
  param_userdefined8 = 0x4a,
  param_endcaps1 = 0x4b,
  param_endcaps2 = 0x4c,
  param_mount0 = 0x4d,
  param_mount1 = 0x4e,
  param_mount2 = 0x4f,
  param_mount3 = 0x50,
  param_mount4 = 0x51,
  param_mount5 = 0x52,
  param_mount6 = 0x53,
  param_mount7 = 0x54,
  param_mount8 = 0x55,
  param_mount9 = 0x56,
  param_mount10 = 0x57,
  param_mount11 = 0x58,
  param_mount12 = 0x59,
  param_mount13 = 0x5a,
  param_mount14 = 0x5b,
  param_bomb_bay = 0x5c,
  param_c_tire = 0x5d,
  param_l_tire = 0x5e,
  param_r_tire = 0x5f,
  param_barrel0 = 0x60,
  param_barrel1 = 0x61,
  param_barrel2 = 0x62,
  param_barrel3 = 0x63,
  param_barrel4 = 0x64,
  param_barrel5 = 0x65,
  param_barrel6 = 0x66,
  param_barrel7 = 0x67,
  param_barrel8 = 0x68,
  param_barrel9 = 0x69,
  param_barrel10 = 0x6a,
  param_barrel11 = 0x6b,
  param_barrel12 = 0x6c,
  param_barrel13 = 0x6d,
  param_barrel14 = 0x6e,
  param_barrel15 = 0x6f,
  param_gun0 = 0x70,
  param_gun1 = 0x71,
  param_gun2 = 0x72,
  param_gun3 = 0x73,
  param_gun4 = 0x74,
  param_gun5 = 0x75,
  param_gun6 = 0x76,
  param_gun7 = 0x77,
  param_gun8 = 0x78,
  param_gun9 = 0x79,
  param_gun10 = 0x7a,
  param_gun11 = 0x7b,
  param_gun12 = 0x7c,
  param_gun13 = 0x7d,
  param_gun14 = 0x7e,
  param_gun15 = 0x7f,
  param_damaged0 = 0x80,
  param_pylonsVisible = 0x81,
  param_bullets0 = 0x82,
  param_bullets1 = 0x83,
  param_bullets2 = 0x84,
  param_bullets3 = 0x85,
  param_bullets4 = 0x86,
  param_bullets5 = 0x87,
  param_bullets6 = 0x88,
  param_bullets7 = 0x89,
  param_cockpitDetail = 0x8a,
  param_rudder_water_deploy = 0x8b,
  param_trimtab_l_aileron = 0x8c,
  param_trimtab_r_aileron = 0x8d,
  param_trimtab_rudder = 0x8e,
  param_rudder_water_rotate = 0x8f,
  param_battery_switch = 0x90,
  param_l_pct_lead_edge_flap0 = 0x91,
  param_r_pct_lead_edge_flap0 = 0x92,
  param_l_pct_lead_edge_flap1 = 0x93,
  param_r_pct_lead_edge_flap1 = 0x94,
  param_l_pct_trail_edge_flap0 = 0x95,
  param_r_pct_trail_edge_flap0 = 0x96,
  param_l_pct_trail_edge_flap1 = 0x97,
  param_r_pct_trail_edge_flap1 = 0x98,
  param_thrust_reverser0 = 0x99,
  param_thrust_reverser1 = 0x9a,
  param_thrust_reverser2 = 0x9b,
  param_thrust_reverser3 = 0x9c,
  param_lever_throttle0 = 0x9d,
  param_lever_throttle1 = 0x9e,
  param_lever_throttle2 = 0x9f,
  param_lever_throttle3 = 0xa0,
  param_lever_prop_pitch0 = 0xa1,
  param_lever_prop_pitch1 = 0xa2,
  param_lever_prop_pitch2 = 0xa3,
  param_lever_prop_pitch3 = 0xa4,
  param_lever_mixture0 = 0xa5,
  param_lever_mixture1 = 0xa6,
  param_lever_mixture2 = 0xa7,
  param_lever_mixture3 = 0xa8,
  param_lever_water_rudder = 0xa9,
  param_lever_stick_fore_aft = 0xaa,
  param_lever_stick_l_r = 0xab,
  param_lever_pedals_l_r = 0xac,
  param_lever_collective = 0xad,
  param_lever_landing_gear = 0xae,
  param_lever_speed_brake = 0xaf,
  param_lever_parking_brake = 0xb0,
  param_lever_flap = 0xb1,
  param_lever_cowl_flaps0 = 0xb2,
  param_lever_cowl_flaps1 = 0xb3,
  param_lever_cowl_flaps2 = 0xb4,
  param_lever_cowl_flaps3 = 0xb5,
  param_cowl_flaps0 = 0xb6,
  param_cowl_flaps1 = 0xb7,
  param_cowl_flaps2 = 0xb8,
  param_cowl_flaps3 = 0xb9,
  param_aux_gear = 0xba,
  param_crash_check = 0xbb,
  param_generic = 0xbc,
  param_sim1_var = 0xbd,
  param_panel_code = 0xbe,
};

enum CONTAINER_DESTROY_FLAGS {
  CONTAINER_DESTROY_IMMEDIATE = 0x0,
  CONTAINER_DESTROY_POST_FRAME = 0x1,
};

enum FAC_TAXI_PARKING_TYPE {
  FAC_TAXI_PARKING_TYPE_NONE = 0x0,
  FAC_TAXI_PARKING_TYPE_RAMP_GA = 0x1,
  FAC_TAXI_PARKING_TYPE_RAMP_GA_SMALL = 0x2,
  FAC_TAXI_PARKING_TYPE_RAMP_GA_MEDIUM = 0x3,
  FAC_TAXI_PARKING_TYPE_RAMP_GA_LARGE = 0x4,
  FAC_TAXI_PARKING_TYPE_RAMP_CARGO = 0x5,
  FAC_TAXI_PARKING_TYPE_RAMP_MIL_CARGO = 0x6,
  FAC_TAXI_PARKING_TYPE_RAMP_MIL_COMBAT = 0x7,
  FAC_TAXI_PARKING_TYPE_GATE_SMALL = 0x8,
  FAC_TAXI_PARKING_TYPE_GATE_MEDIUM = 0x9,
  FAC_TAXI_PARKING_TYPE_GATE_HEAVY = 0xa,
  FAC_TAXI_PARKING_TYPE_DOCK_GA = 0xb,
  FAC_TAXI_PARKING_TYPE_COUNT = 0xc,
};

enum AIRCRAFT_CONTAINER_ITEM_ENUM {
  ACC_CFG = 0x0,
  ACC_DIR = 0x1,
  ACC_TITLE = 0x2,
  ACC_AIR_FILE = 0x3,
  ACC_MODEL_DIR = 0x4,
  ACC_MODEL_CFG = 0x5,
  ACC_PANEL_DIR = 0x6,
  ACC_PANEL_CFG = 0x7,
  ACC_SOUND_DIR = 0x8,
  ACC_SOUND_CFG = 0x9,
  ACC_TEXTURE_DIR = 0xa,
  ACC_TEXTURE_CFG = 0xb,
  ACC_CHECKLIST_FILE = 0xc,
  ACC_KB_CHECK_FILE = 0xd,
  ACC_KB_REF_FILE = 0xe,
  ACC_KB_NOTES_FILE = 0xf,
  ACC_CFG_SECTION = 0x10,
  ACC_ATC_ID = 0x11,
  ACC_ATC_ID_ENABLE = 0x12,
  ACC_ATC_ID_COLOR = 0x13,
  ACC_ATC_ID_FONT = 0x14,
  ACC_ATC_TYPE = 0x15,
  ACC_ATC_MODEL = 0x16,
  ACC_ATC_AIRLINE = 0x17,
  ACC_ATC_FLIGHT_NUMBER = 0x18,
  ACC_ATC_HEAVY = 0x19,
  ACC_ATC_PARKING_TYPES = 0x1a,
  ACC_ATC_PARKING_CODES = 0x1b,
  ACC_PROP_ANIM_RATIO = 0x1c,
  ACC_MANUFACTURER = 0x1d,
  ACC_TYPE = 0x1e,
  ACC_VARIATION = 0x1f,
  ACC_VISUAL_DAMAGE = 0x20,
  ACC_STATE_CFG = 0x21,
  ACC_COUNT = 0x22,
  ACC_AIRCRAFT_CFG = 0x0,
  ACC_AIRCRAFT_CFG_SECTION = 0x10,
  ACC_BOAT_CFG = 0x0,
  ACC_BOAT_CFG_SECTION = 0x10,
};

enum ENUM_AC_FOLDERS {
  AC_FOLDER_AIRCRAFT = 0x0,
  AC_FOLDER_BOATS = 0x1,
  AC_FOLDER_LIBRARY = 0x2,
  AC_FOLDER_MODELS = 0x3,
  AC_FOLDER_UNDEFINED = 0x4,
  AC_FOLDER_COUNT = 0x5,
};

enum _AC_CATEGORY {
  AC_CATEGORY_AIRPLANE = 0x0,
  AC_CATEGORY_HELICOPTER = 0x1,
  AC_CATEGORY_AIRPLANE_AI = 0x2,
  AC_CATEGORY_HELICOPTER_AI = 0x3,
  AC_CATEGORY_BOAT = 0x4,
};

enum AContainSettings_LabelContentBits {
  LABEL_CONTENT_MANUFACTURER = 0x0,
  LABEL_CONTENT_MODEL = 0x1,
  LABEL_CONTENT_TAIL_NUMBER = 0x2,
  LABEL_CONTENT_DISTANCE = 0x3,
  LABEL_CONTENT_ALTITUDE = 0x4,
  LABEL_CONTENT_AIRSPEED = 0x5,
  LABEL_CONTENT_HEADING = 0x6,
  LABEL_CONTENT_AIRLINE = 0x7,
  LABEL_CONTENT_AIRLINE_AND_FLIGHT_NUMBER = 0x8,
  LABEL_CONTENT_FLIGHT_PLAN = 0x9,
  LABEL_CONTENT_CONTAINER_ID = 0xa,
  LABEL_CONTENT_PLAYER_NAME = 0xb,
  LABEL_CONTENT_LIMIT = 0xc,
};

typedef struct FS2K_AIRCRAFT_MODEL_PARAMS {
  /*<thisrel this+0x0>*/ /*|0x4|*/ unsigned int params_length;
  /*<thisrel this+0x4>*/ /*|0x24|*/ union LATLONALTPBH llapbh;
  /*<thisrel this+0x28>*/ /*|0x18|*/ struct LATLONALT visual_lla;
  /*<thisrel this+0x40>*/ /*|0x20|*/ unsigned short color_table[16];
  /*<thisrel this+0x60>*/ /*|0x2|*/ unsigned short texture;
  /*<thisrel this+0x64>*/ /*|0x4|*/ unsigned int texture_dir;
  /*<thisrel this+0x68>*/ /*|0x4|*/ unsigned int Gen_model;
  /*<thisrel this+0x6c>*/ /*|0x2|*/ unsigned short Gear_smoke;
  /*<thisrel this+0x6e>*/ /*|0x2|*/ unsigned short left_ailer;
  /*<thisrel this+0x70>*/ /*|0x2|*/ unsigned short right_ailer;
  /*<thisrel this+0x72>*/ /*|0x2|*/ unsigned short left_flap;
  /*<thisrel this+0x72>*/ /*|0x2|*/ unsigned short flaps;
  /*<thisrel this+0x74>*/ /*|0x2|*/ unsigned short right_flap;
  /*<thisrel this+0x76>*/ /*|0x2|*/ unsigned short elevator;
  /*<thisrel this+0x78>*/ /*|0x2|*/ unsigned short rudder;
  /*<thisrel this+0x7a>*/ /*|0x2|*/ unsigned short engine_rpm;
  /*<thisrel this+0x7c>*/ /*|0x2|*/ unsigned short prop_pos;
  /*<thisrel this+0x7e>*/ /*|0x2|*/ unsigned short front_gear_p;
  /*<thisrel this+0x80>*/ /*|0x2|*/ unsigned short left_gear_p;
  /*<thisrel this+0x82>*/ /*|0x2|*/ unsigned short left_gear_b;
  /*<thisrel this+0x84>*/ /*|0x2|*/ unsigned short right_gear_p;
  /*<thisrel this+0x86>*/ /*|0x2|*/ unsigned short right_gear_b;
  /*<thisrel this+0x88>*/ /*|0x2|*/ unsigned short lights;
  /*<thisrel this+0x8a>*/ /*|0x2|*/ unsigned short strobe;
  /*<thisrel this+0x8c>*/ /*|0x2|*/ unsigned short prop_visible;
  /*<thisrel this+0x8e>*/ /*|0x2|*/ unsigned short bomb_rocket_visible;
  /*<thisrel this+0x90>*/ /*|0x2|*/ unsigned short parts_visible;
  /*<thisrel this+0x94>*/ /*|0x4|*/ unsigned int scale;
}FS2K_AIRCRAFT_MODEL_PARAMS,*PFS2K_AIRCRAFT_MODEL_PARAMS,**PPFS2K_AIRCRAFT_MODEL_PARAMS;

typedef struct AC_FILE_INFO {
  /*<thisrel this+0x0>*/ /*|0x100|*/ char title_str[256];
  /*<thisrel this+0x100>*/ /*|0x400|*/ char description[1024];
  /*<thisrel this+0x500>*/ /*|0x400|*/ char alt_description[1024];
  /*<thisrel this+0x900>*/ /*|0x104|*/ char filename[260];
  /*<thisrel this+0xa04>*/ /*|0x4|*/ int editable;
  /*<thisrel this+0xa08>*/ /*|0x104|*/ char manufacturer[260];
  /*<thisrel this+0xb0c>*/ /*|0x104|*/ char actype[260];
  /*<thisrel this+0xc10>*/ /*|0x104|*/ char variation[260];
  /*<thisrel this+0xd14>*/ /*|0x4|*/ unsigned int category;
  /*<thisrel this+0xd18>*/ /*|0x9|*/ char atc_id[9];
  /*<thisrel this+0xd24>*/ /*|0x4|*/ int atc_id_enable;
  /*<thisrel this+0xd28>*/ /*|0x4|*/ unsigned int atc_id_color;
  /*<thisrel this+0xd2c>*/ /*|0x29|*/ char atc_id_font[41];
  /*<thisrel this+0xd55>*/ /*|0x33|*/ char atc_airline[51];
}AC_FILE_INFO,*PAC_FILE_INFO,**PPAC_FILE_INFO;

typedef struct AContainSettings {
  /*<thisrel this+0x0>*/ /*|0x4|*/ int bShowLabels;
  /*<thisrel this+0x4>*/ /*|0x4|*/ int bShowUserLabel;
  /*<thisrel this+0x8>*/ /*|0x4|*/ unsigned int uLabelContent;
  /*<thisrel this+0xc>*/ /*|0x4|*/ unsigned int uLabelDelay;
  /*<thisrel this+0x10>*/ /*|0x4|*/ unsigned int uLabelColor;
  void SetLabelContentBit(enum AContainSettings_LabelContentBits, int);
  int GetLabelContentBit(enum AContainSettings_LabelContentBits);
  AContainSettings();
}AContainSettings,*PAContainSettings,**PPAContainSettings;

class ISimulateCallback {
};

class ISim {
public:
  ISim(class ISim&);
  ISim();

  // virtual functions ------------------------------
  virtual /*<vtableoff 0x0>*/ unsigned int __stdcall Init() = 0;
  virtual /*<vtableoff 0x4>*/ void __stdcall Simulate(double) = 0;
  virtual /*<vtableoff 0x10>*/ unsigned int __stdcall VarGet(enum SIMVAR_ID, void*, int) = 0;
  virtual /*<vtableoff 0x10>*/ unsigned int __stdcall VarGet(enum SIMVAR_ID, char*, unsigned int, int) = 0;
  virtual /*<vtableoff 0x10>*/ unsigned int __stdcall VarGet(enum SIMVAR_ID, unsigned int, double*, int) = 0;
  virtual /*<vtableoff 0x18>*/ unsigned int __stdcall VarSet(enum SIMVAR_ID, void*, int) = 0;
  virtual /*<vtableoff 0x18>*/ unsigned int __stdcall VarSet(enum SIMVAR_ID, unsigned int, double, int) = 0;
  virtual /*<vtableoff 0x1c>*/ int __stdcall EventProcess(unsigned int, unsigned int, int) = 0;
  virtual /*<vtableoff 0x20>*/ void __stdcall SetLLAPBH(struct LATLONALT*, struct PBH32*, int, int) = 0;
  virtual /*<vtableoff 0x24>*/ unsigned int __stdcall GetContainerId() = 0;
  virtual /*<vtableoff 0x28>*/ void __stdcall SetAICallback(class ISimulateCallback*) = 0;
  virtual /*<vtableoff 0x2c>*/ ~ISim();
};

class ISimContainer 
{
public:
  ISimContainer(class ISimContainer&);
  ISimContainer();

  virtual unsigned int __stdcall GetContainerId() = 0;
  virtual char* __stdcall GetContainerTitle() = 0;
  virtual int __stdcall IsUser() = 0;
  virtual int __stdcall GetLatLonAlt(struct LATLONALT*) = 0;
  virtual class ISim* __stdcall GetSim() = 0;
  virtual void __stdcall UpdateVisualModel(struct FS2K_AIRCRAFT_MODEL_PARAMS*) = 0;
  virtual void __stdcall UpdateVisualModel() = 0;
  virtual int __stdcall AddObj() = 0;
  virtual int __stdcall Render() = 0;
  virtual int __stdcall AddToScene() = 0;
  virtual int __stdcall RemoveFromScene() = 0;
  virtual int __stdcall GetVisualModelParam(enum VISUAL_MODEL_PARAM, void*, int) = 0;
  virtual int __stdcall SetVisualModelParam(enum VISUAL_MODEL_PARAM, void*, int) = 0;
  virtual void __stdcall Destroy(enum CONTAINER_DESTROY_FLAGS) = 0;
};

class SIM1AircraftSim {
};

class IAircraftSim : public ISim  {
public:
  IAircraftSim(class IAircraftSim&);
  IAircraftSim();

  // virtual functions ------------------------------
  virtual /*<vtableoff 0x0>*/ unsigned int __stdcall Init() = 0;
  virtual /*<vtableoff 0x30>*/ unsigned int __stdcall Load(class IAircraftContainer*, int) = 0;
  virtual /*<vtableoff 0x0>*/ void __stdcall Simulate(double) = 0;
  virtual /*<vtableoff 0x0>*/ unsigned int __stdcall VarGet(enum SIMVAR_ID, void*, int) = 0;
  virtual /*<vtableoff 0x0>*/ unsigned int __stdcall VarGet(enum SIMVAR_ID, char*, unsigned int, int) = 0;
  virtual /*<vtableoff 0x0>*/ unsigned int __stdcall VarGet(enum SIMVAR_ID, unsigned int, double*, int) = 0;
  virtual /*<vtableoff 0x0>*/ unsigned int __stdcall VarSet(enum SIMVAR_ID, void*, int) = 0;
  virtual /*<vtableoff 0x0>*/ unsigned int __stdcall VarSet(enum SIMVAR_ID, unsigned int, double, int) = 0;
  virtual /*<vtableoff 0x0>*/ int __stdcall EventProcess(unsigned int, unsigned int, int) = 0;
  virtual /*<vtableoff 0x0>*/ void __stdcall SetLLAPBH(struct LATLONALT*, struct PBH32*, int, int) = 0;
  virtual /*<vtableoff 0x34>*/ void __stdcall ResetDamageState() = 0;
  virtual /*<vtableoff 0x38>*/ PSIM_DATA __stdcall SimDataGet() = 0;
  virtual /*<vtableoff 0x3c>*/ class IAircraftContainer* __stdcall ContainerGet() = 0;
  virtual /*<vtableoff 0x0>*/ unsigned int __stdcall GetContainerId() = 0;
  virtual /*<vtableoff 0x0>*/ void __stdcall SetAICallback(class ISimulateCallback*) = 0;
  virtual /*<vtableoff 0x40>*/ class SIM1AircraftSim* __stdcall GetSIM1AircraftSim() = 0;
  virtual /*<vtableoff 0x0>*/ ~IAircraftSim();
};

class CAircraftContainer {
};

class IAircraftContainer : public ISimContainer  
{
public:
  IAircraftContainer(class IAircraftContainer&);
  IAircraftContainer();

  virtual class IAircraftSim* __stdcall GetAircraftSim() = 0;
  virtual void __stdcall SetAircraftSim(class IAircraftSim*) = 0;
  virtual void *__stdcall GetPilotAI() = 0;
  virtual void __stdcall SetPilotAI(void *) = 0;
  virtual int __stdcall GetContainerItem(enum AIRCRAFT_CONTAINER_ITEM_ENUM, char*, unsigned int) = 0;
  virtual int __stdcall SetContainerItem(enum AIRCRAFT_CONTAINER_ITEM_ENUM, char*) = 0;
  virtual void __stdcall GetCachedFileInfo() = 0;
  virtual unsigned int __stdcall GetParkingTypes(enum FAC_TAXI_PARKING_TYPE**) = 0;
  virtual unsigned int __stdcall GetParkingCodes(unsigned int**) = 0;
  virtual int __stdcall GetPanelLocals(double**, unsigned int*) = 0;
  virtual char* __stdcall GetPlayerName() = 0;
  virtual void __stdcall SetPlayerName(char*) = 0;
  virtual class IAircraftContainer* __stdcall Clone() = 0;
  virtual class CAircraftContainer* __stdcall GetPrivateData() = 0;
};

class CBoatContainer {
};

class IBoatContainer : public ISimContainer  
{
public:
  IBoatContainer(class IBoatContainer&);
  IBoatContainer();
  
  virtual CBoatContainer* __stdcall GetPrivateData()=0;
};

class ACAnimator {
  ACAnimator(class ACAnimator&);
  ACAnimator();

  virtual int __stdcall SetAircraft(char*) = 0;
  virtual void __stdcall Animate() = 0;
};

template <class X> class TIContainerList {
};

struct ACONTAIN {
	FSLINKAGE										head;
    int												(FSAPI *aircraft_create_user)(char*);
    int												(FSAPI *aircraft_destroy_user)();
    class IAircraftContainer *						(FSAPI *aircraft_create)(char*, int, int, unsigned int);
    class IBoatContainer *							(FSAPI *boat_create)(char*, int, int, unsigned int);
    int												(FSAPI *container_destroy_by_id)(unsigned int, enum CONTAINER_DESTROY_FLAGS);
    int												(FSAPI *aircraft_array_build)(enum ENUM_AC_FOLDERS, unsigned int*, struct AC_FILE_INFO**, enum _AC_CATEGORY, enum _AC_CATEGORY);
    unsigned short									(FSAPI *getCockpitDetail)();
    unsigned short									(FSAPI *setCockpitDetail)(unsigned short);
    class ACAnimator*								(FSAPI *AircraftAnimateCreate)(void*);
    void											(FSAPI *AircraftAnimateDestroy)(class ACAnimator*);
    unsigned int									(FSAPI *UserContainerIdGet)();
    class IAircraftContainer*						(FSAPI *UserContainerGet)();
    class TIContainerList<ISimContainer *>*			(FSAPI *AContain_GetSimContainerList)();
    class TIContainerList<IAircraftContainer *>*	(FSAPI *AContain_GetAircraftContainerList)();
    class IAircraftContainer*						(FSAPI *ContainerByIdGet)(unsigned int);
    class IAircraftContainer*						(FSAPI *ContainerCurrentlyRendering)();
    void											(FSAPI *AContain_SetSettings)(struct AContainSettings*);
    void											(FSAPI *AContain_GetSettings)(struct AContainSettings*);
};

#pragma pack()

#endif
