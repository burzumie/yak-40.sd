/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: g2d.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_G2D
#define _FS_SDK_G2D

#pragma pack(4)

enum e_DxBufferUsage {
	BufferDynamic = 0x0,
	BufferStatic = 0x1,
};

enum ER2TRenderModes {
	R2TMode_ModulateTexture = 0x0,
	R2TMode_ModulateTextureAndFilter = 0x1,
	R2TMode_ModulateTextureAndTentFilterU = 0x2,
	R2TMode_SaturateAlpha = 0x3,
	R2TMode_VertexAlphaFog = 0x4,
	R2TMode_Limit = 0x5,
	R2TMode_Default = 0x0,
};

enum G2DBUFFER {
	G2D_COLOR_BUFFER = 0x0,
	G2D_DEPTH_BUFFER = 0x1,
	G2D_STENCIL_BUFFER = 0x2,
};

typedef struct G2DSPRITE {
    float fX;
    float fY;
    float fZ;
    float fWidth;
    float fHeight;
    float fRadians;
    unsigned int uColor;
    float fU1;
    float fV1;
    float fU2;
    float fV2;
}G2DSPRITE,*PG2DSPRITE,**PPG2DSPRITE;

typedef struct G2DTLVERTEX {
    float sx;
    float sy;
    float sz;
    float rhw;
    unsigned int color;
    unsigned int specular;
    float tu;
    float tv;
}G2DTLVERTEX,*PG2DTLVERTEX,**PPG2DTLVERTEX;

typedef struct G2DPICKDATA {
    unsigned int uObject;
    void* pObjectData;
    unsigned int hTexture;
    struct G2DTLVERTEX vPick;
}G2DPICKDATA,*PG2DPICKDATA,**PPG2DPICKDATA;
	  
struct G2D {
	FSLINKAGE		head;
    void*			(FSAPI *g2d_CreateDevice)(void*, unsigned int, int, unsigned int, unsigned int);
    void			(FSAPI *g2d_ResetDevice)(void*);
    void			(FSAPI *g2d_OnLostDevice)(void*);
    void			(FSAPI *g2d_OnResetDevice)(void*);
    void			(FSAPI *g2d_FreeDevice)(void**);
    unsigned int	(FSAPI *g2d_SetCurrentDevice)(void*);
    void			(FSAPI *g2d_AddDelayedBlt)(void*, void*, struct PIXRECT*, void*, struct PIXPOINT*);
    unsigned int	(FSAPI *g2d_AllocateVertexBuffer)(unsigned int, enum e_DxBufferUsage, void**);
    unsigned int	(FSAPI *g2d_ReleaseVertexBuffer)(void*);
    unsigned int	(FSAPI *g2d_AllocateIndexBuffer)(unsigned int, enum e_DxBufferUsage, void**);
    unsigned int	(FSAPI *g2d_ReleaseIndexBuffer)(void*);
    unsigned int	(FSAPI *g2d_CopyDataToBuffer)(void*, void*, unsigned int);
    unsigned int	(FSAPI *g2d_BufferLock)(void*, void**, unsigned int, unsigned int, unsigned int);
    unsigned int	(FSAPI *g2d_BufferUnlock)(void*);
    unsigned int	(FSAPI *g2d_GetBufferSize)(void*);
    int				(FSAPI *g2d_IsBufferValid)(void*);
    void			(FSAPI *g2d_InvalidateBuffer)(void*);
    unsigned int	(FSAPI *g2d_CreateRenderTarget)(unsigned int, unsigned int, enum IMG_FORMAT, int, unsigned int, unsigned int*);
    unsigned int	(FSAPI *g2d_DestroyRenderTarget)(unsigned int);
    unsigned int	(FSAPI *g2d_SetRenderTargetClearColor)(unsigned int, unsigned int);
    unsigned int	(FSAPI *g2d_SetRenderTarget)(unsigned int);
    unsigned int	(FSAPI *g2d_IsValidRenderTarget)(unsigned int);
    unsigned int	(FSAPI *g2d_colormap)(unsigned int);
    void			(FSAPI *g2d_transform)(unsigned int, void*, void*, void*, unsigned int, unsigned int*, unsigned int*);
    void			(FSAPI *g2d_draw_clip)(unsigned int, void*, void*, unsigned int, unsigned short*, unsigned int);
    unsigned int	(FSAPI *g2d_begin)(struct AWIND*);
    unsigned int	(FSAPI *g2d_begin_r2t)(unsigned int, enum ER2TRenderModes);
    unsigned int	(FSAPI *g2d_end)();
    void			(FSAPI *g2d_draw)(unsigned int, unsigned int, void*, unsigned int, unsigned short*, unsigned int, unsigned int);
    void			(FSAPI *g2d_draw_vb)(unsigned int, unsigned int, void*, void*, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int);
    void			(FSAPI *g2d_sprite_draw)(struct G2DSPRITE*, unsigned int);
    void			(FSAPI *g2d_light_draw)(float, float, float, unsigned int, float, float, unsigned int, int);
    void			(FSAPI *g2d_box)(int, int, int, int, unsigned int);
    void			(FSAPI *g2d_rect)(int, int, int, int, unsigned int);
    void			(FSAPI *g2d_draw_rect)(float, float, float, float, unsigned int, float, float, float, float);
    void			(FSAPI *g2d_glare)(struct RGBA);
    void			(FSAPI *g2d_load_palette)(void*, unsigned int);
    unsigned int	(FSAPI *g2d_texture_load)(char*, unsigned int, unsigned int, struct RGBA, unsigned int, unsigned int, unsigned int);
    unsigned int	(FSAPI *g2d_texture_create)(struct IMAGE*, char*, struct RGBA);
    unsigned int	(FSAPI *g2d_texture_create_ex)(struct IMAGE*, char*, struct RGBA, struct RGBA*, unsigned int, unsigned int);
    void			(FSAPI *g2d_texture_free)(unsigned int, unsigned int);
    void			(FSAPI *g2d_texture_addref)(unsigned int, unsigned int);
    void*			(FSAPI *g2d_texture_lock)(unsigned int, struct IMAGE*, unsigned int, unsigned int);
    void			(FSAPI *g2d_texture_unlock)(unsigned int, unsigned int);
    void			(FSAPI *g2d_texture_set_fallback_color)(unsigned int, struct RGBA);
    unsigned int	(FSAPI *g2d_texture_status)(unsigned int);
    unsigned int	(FSAPI *g2d_texture_IsOnAnyDevice)(unsigned int);
    unsigned int	(FSAPI *g2d_texture_request)(unsigned int, unsigned int);
    unsigned int	(FSAPI *g2d_texture_use)(char*, unsigned int, struct RGBA, unsigned int, unsigned int*);
    void			(FSAPI *g2d_texture_copy)(struct IMAGE*, struct IMAGE*);
    void			(FSAPI *g2d_set_state)(unsigned int, void*);
    void			(FSAPI *g2d_get_state)(unsigned int, void*);
    void			(FSAPI *g2d_set_setting)(unsigned int, unsigned int);
    void			(FSAPI *g2d_get_setting)(unsigned int, unsigned int*);
    unsigned int	(FSAPI *g2d_buffer_lock)(void*, enum G2DBUFFER, struct IMAGE*);
    unsigned int	(FSAPI *g2d_buffer_unlock)(void*, enum G2DBUFFER);
    void			(FSAPI *g2d_draw_text_3d)(enum G2DFONT, float, float, float, char*, unsigned int, unsigned int);
    unsigned int	(FSAPI *g2d_draw_text_A)(enum G2DFONT, int, int, char*, int, struct PIXRECT*, struct PIXRECT*, unsigned int, unsigned int);
    unsigned int	(FSAPI *g2d_draw_text_W)(enum G2DFONT, int, int, unsigned short*, int, struct PIXRECT*, struct PIXRECT*, unsigned int, unsigned int);
    int				(FSAPI *g2d_text_extent_A)(enum G2DFONT, char*, int, unsigned int*, unsigned int*);
    int				(FSAPI *g2d_text_extent_W)(enum G2DFONT, unsigned short*, int, unsigned int*, unsigned int*);
    unsigned int	(FSAPI *g2d_create_text_buffer)(void**, char*);
    void			(FSAPI *g2d_destroy_text_buffer)(void**);
    unsigned int	(FSAPI *g2d_render_text_buffer)(void*, enum G2DFONT, int, int, struct PIXRECT*, struct PIXRECT*, unsigned int, unsigned int);
    int				(FSAPI *g2d_pick_add)(unsigned int, float, float);
    unsigned int	(FSAPI *g2d_pick_enable)(int, unsigned int);
    int				(FSAPI *g2d_pick_draw_test)();
    unsigned int	(FSAPI *g2d_pick_get)(int, struct G2DPICKDATA*);
    unsigned int	(FSAPI *g2d_pick_object)(void*);
    unsigned int	(FSAPI *g2d_create_uisurface)(void**);
    void			(FSAPI *g2d_destroy_uisurface)(void**);
    unsigned int	(FSAPI *g2d_resize_uisurface)(void*, struct PIXPOINT*);
    void*			(FSAPI *g2d_lock_uisurface)(void*);
    void			(FSAPI *g2d_unlock_uisurface)(void*);
    void			(FSAPI *g2d_render_uisurface)(void*, struct PIXPOINT*, unsigned int);
    void			(FSAPI *g2d_UpdateWindow)(void*, struct PIXRECT*, struct PIXRECT*, void*);
};

#pragma pack()
              
#endif        