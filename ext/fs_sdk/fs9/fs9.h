/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: fs6.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 13:05:34 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_FS6
#define _FS_SDK_FS6

#pragma pack(4)

enum KEYBOARDQ {
	QUERY_EVENT = 0x0,
	QUERY_KEY = 0x1,
	QUERY_INDEX = 0x2,
};

enum PERFORMANCE_MODE {
	PERF_MODE_USER_DEFINED = 0x0,
	PERF_MODE_MIN = 0x1,
	PERF_MODE_VERY_LOW = 0x2,
	PERF_MODE_LOW = 0x3,
	PERF_MODE_MID_LOW = 0x4,
	PERF_MODE_MID_HIGH = 0x5,
	PERF_MODE_HIGH = 0x6,
	PERF_MODE_ULTRA_HIGH = 0x7,
	PERF_MODE_COUNT = 0x8,
};

typedef enum CHAINID{
	CHAIN_FRAME= 0x1,
	CHAIN_18HZ_SYNC  = 0x2,
	CHAIN_1HZ_SYNC   = 0x3,
	CHAIN_4SEC_SYNC  = 0x4,
	CHAIN_6HZ_SYNC   = 0x5,
	CHAIN_8SEC_SYNC  = 0x6,
	CHAIN_DIALOG_FRAME  = 0x7,
	CHAIN_EVENT      = 0x8,
	CHAIN_KEYPRESS   = 0x9,
	CHAIN_PAUSE_FRAME  = 0x0A,
	CHAIN_POST_RADSORT  = 0x0B,
	CHAIN_PRE_CATEGORY  = 0x0C,
	CHAIN_PRE_RADSORT  = 0x0D,
	CHAIN_SOUND      = 0x0E,
	CHAIN_VISUAL_LOC_SET  = 0x0F,
	CHAIN_MESSAGE    = 0x10,
	CHAIN_WINDOW_PRE_RENDER  = 0x11,
	CHAIN_WINDOW_POST_RENDER  = 0x12,
	CHAIN_WINDOW_PRE_UPDATE  = 0x13,
	CHAIN_WINDOW_POST_UPDATE  = 0x14,
	CHAIN_PRE_WINDOW_MANAGER  = 0x15,
	CHAIN_POST_WINDOW_MANAGER  = 0x16,
	CHAIN_MULTIPLAYER  = 0x17,
	CHAIN_USER_RADIO  = 0x18,
	CHAIN_WINDOW_PRE_TEXT  = 0x19,
	CHAIN_WINDOW_TEXT  = 0x1A,
	CHAIN_WINDOW_POST_TEXT  = 0x1B,
	CHAIN_unused1    = 0x1C,
	CHAIN_unused2    = 0x1D,
	CHAIN_FLIGHT_INSTRUCTOR  = 0x1E,
	CHAIN_TERRAIN_SHADOW_REQUEST  = 0x1F,
	CHAIN_PRE_FRAME  = 0x20,
	CHAIN_POST_FRAME  = 0x21,
	CHAIN_SIM_FRAME_COMPLETE  = 0x22,
	CHAIN_TESTTOOL_FRAME  = 0x23,
	CHAIN_PROCESS_FILE_IO  = 0x24,
	CHAIN_POST_VISUAL_LOC_SET  = 0x25,
	CHAIN_ACONTAIN   = 0x26,
	CHAIN_ATC        = 0x27,
	CHAIN_ALPHA_PRE_RADSORT  = 0x28,
	CHAINID_MAX      = 0x29
};

typedef struct KEYBOARDKEY {
    unsigned int key_value;
    unsigned int modifiers;
    unsigned int event_id;
}KEYBOARDKEY,*PKEYBOARDKEY,**PPKEYBOARDKEY;

typedef struct _StringItem {
    _StringItem *psiNext;
    unsigned short wCount;
    unsigned short cchLen;
    char szName;
}_StringItem,*_PStringItem,**_PPStringItem;

typedef struct _StringTable {
    _RTL_CRITICAL_SECTION cs;
    unsigned int uBuckets;
    unsigned int ucbExtra;
    unsigned int uFlags;
    _StringItem *apsiBuckets;
}STRING_TABLE,*PSTRING_TABLE,**PPSTRING_TABLE;

typedef struct FINDDATA {
    char filename[260];
    enum FILETYPE filetype;
    unsigned int filesizeLow;
    unsigned int filesizeHigh;
    int file_writable;
    struct DATETIME filetimeModified;
}FINDDATA,*PFINDDATA,**PPFINDDATA;

typedef struct MOUSE_RAW_MESSAGE {
    struct HWND__* hwnd;
    unsigned int message;
    unsigned int wParam;
    long lParam;
}MOUSE_RAW_MESSAGE,*PMOUSE_RAW_MESSAGE,**PPMOUSE_RAW_MESSAGE;

typedef void (__stdcall *FS6_UserEnumFunc)(void *,void *);
typedef void (__stdcall *FS6_UserStrEnum)(_StringTable *pst, void*, char*, unsigned int, void*);
typedef DWORD (FSAPI *FS6_ChainHandler)(DWORD,void*,void*);
typedef void (__stdcall *FS6_UserCallback)(DWORD event_id,PVOID buffer_specific,PVOID user_buffer,PVOID user_param);
typedef void (__stdcall *FS6_UserCallbackBackground)(VOID *,VOID *);
typedef void (__stdcall *UserCallback)(DWORD event_id,VOID *buffer_specific,PVOID user_buffer,DWORD user_param);

class JobTemplate {
public:
	enum JobState {
		JobUninitialized = 0x0,
		JobQueued = 0x1,
		JobRunning = 0x2,
		JobFinished = 0x3,
		JobCanceled = 0x4,
	};
	CSmartPtrRefCount m_smartPtrRefCount;
	JobState m_jobState;
	int m_iJobQueueID;
	int __stdcall AddRef();
	int __stdcall Release();
	int __stdcall GetRefCount();
	void SmartPtrAddRef();
	void SmartPtrRelease();
	int SmartPtrGetRefCount();
	JobTemplate(JobTemplate&);
	JobTemplate(int);
	JobState GetJobState();
	int GetJobQueueID();
	int IsRunning();
	int IsQueued();
	int IsBusy();
	unsigned int CancelJob();
	unsigned int Work(int);
	void SetJobState(JobState);
	virtual ~JobTemplate();
	virtual unsigned int SubmitJob();
	virtual void RunJob() = 0;
//	virtual void *__vecDelDtor(unsigned int);
};

struct FS6
{
    FSLINKAGE head;
    	DWORD some_funcs1[0x8B];
    	unsigned int (__stdcall *menu_append)(unsigned short,unsigned short,unsigned int,unsigned int);
	DWORD some_funcs2[0x5];
    	unsigned int (__stdcall *menu_delete)(unsigned short);
	DWORD some_funcs3[0x79];
	void (__stdcall *chain_insert2)(DWORD number,UserCallback func,DWORD p1,DWORD p2);
	void (__stdcall *chain_delete2)(DWORD number,UserCallback func,DWORD p1,DWORD p2);
};

struct FS6old {
	FSLINKAGE head;
	int  (FSAPI *application_open)(struct FS6LINK *Main);
	int  (FSAPI *application_close)();
	int  (FSAPI *application_visible)();
	int  (FSAPI *application_abort)();
	int  (FSAPI *application_exit)(int bSkipConfirmDlg);
	void  (FSAPI *application_timer_disable)();
	void  (FSAPI *application_timer_enable)();
	unsigned int  (FSAPI *application_executive_loop_milliseconds_u32_get)();
	unsigned int  (FSAPI *application_elapsed_milliseconds_u32_get)();
	unsigned int  (FSAPI *application_event)(unsigned int event_id, unsigned int userdata);
	unsigned int  (FSAPI *application_event_immediate)(unsigned int event_id, unsigned int userdata);
	unsigned int  (FSAPI *application_date_time_get)(struct DATETIME *dt);
	void  (FSAPI *application_fullscreen_set)(int flag);
	unsigned int  (FSAPI *application_default_size)();
	void  (FSAPI *application_show_wait)(int flag);
	void  (FSAPI *application_data_get)(void **main_window_handle, void **app_instance);
	void*  (FSAPI *application_guid_get)();
	char*  (FSAPI *UNUSED_application_installed_to_dir_get)();
	char*  (FSAPI *UNUSED_application_installed_from_dir_get)();
	unsigned int  (FSAPI *app_window_open)(struct AWIND *window);
	void  (FSAPI *app_window_close)(struct AWIND *window);
	unsigned int  (FSAPI *app_window_lock)(struct AWIND *window);
	unsigned int  (FSAPI *app_window_unlock)(struct AWIND *window);
	unsigned int  (FSAPI *app_window_dialog_open)(void *hwnd, struct AWIND *window);
	void  (FSAPI *app_window_dialog_close)(struct AWIND *window);
	void  (FSAPI *app_window_set)(struct AWIND *window);
	void  (FSAPI *app_window_set_text)(struct AWIND *window);
	void  (FSAPI *app_window_update)(struct AWIND *window);
	void  (FSAPI *app_window_rect_add)(struct AWIND *window, struct PIXRECT *rect);
	void  (FSAPI *UNUSED_app_window_print)(struct AWIND *window, unsigned int position, unsigned int color, char *str, int cache);
	unsigned int  (FSAPI *app_window_to_front)(struct AWIND *window);
	unsigned int  (FSAPI *app_window_order_get)(struct AWIND **window, unsigned int layer, unsigned int order_enum);
	unsigned int  (FSAPI *command_line_string_get)(unsigned int arg, char *buf, unsigned int bufcount);
	unsigned int  (FSAPI *command_line_int_get)(unsigned int arg, unsigned int *var);
	unsigned int  (FSAPI *command_line_float_get)(unsigned int arg, double *var);
	enum SWITCH_ENUM  (FSAPI *command_line_switch_exists)(unsigned int arg);
	unsigned int  (FSAPI *configuration_int_get)(char *pszSection, char *pszKeyword, unsigned int *puValue);
	unsigned int  (FSAPI *configuration_int_set)(char *pszSection, char *pszKeyword, unsigned int var);
	unsigned int  (FSAPI *configuration_int_array_get)(char *pszSection, char *pszKeyword, unsigned int *puArray, unsigned int uArrayCount);
	unsigned int  (FSAPI *configuration_int_array_set)(char *pszSection, char *pszKeyword, unsigned int *int_array, unsigned int int_array_count);
	unsigned int  (FSAPI *configuration_float_get)(char *pszSection, char *pszKeyword, double *puValue);
	unsigned int  (FSAPI *configuration_float_set)(char *pszSection, char *pszKeyword, double var);
	unsigned int  (FSAPI *configuration_float_array_get)(char *pszSection, char *pszKeyword, double *pdArray, unsigned int uArrayCount);
	unsigned int  (FSAPI *configuration_float_array_set)(char *pszSection, char *pszKeyword, double *float_array, unsigned int float_array_count);
	unsigned int  (FSAPI *configuration_string_get)(char *pszSection, char *pszKeyword, char *pszBuf, unsigned int uBufCount);
	unsigned int  (FSAPI *configuration_string_set)(char *pszSection, char *pszKeyword, char *buf);
	unsigned int  (FSAPI *configuration_int_get_private)(char *pszSection, char *pszKeyword, unsigned int*, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_int_set_private)(char *pszSection, char *pszKeyword, unsigned int, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_int_array_get_private)(char *pszSection, char *pszKeyword, unsigned int*, unsigned int, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_int_array_set_private)(char *pszSection, char *pszKeyword, unsigned int*, unsigned int, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_float_get_private)(char *pszSection, char *pszKeyword, double*, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_float_set_private)(char *pszSection, char *pszKeyword, double, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_float_array_get_private)(char *pszSection, char *pszKeyword, double*, unsigned int, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_float_array_set_private)(char *pszSection, char *pszKeyword, double*, unsigned int, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_string_get_private)(char *pszSection, char *pszKeyword, char*, unsigned int, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_string_set_private)(char *pszSection, char *pszKeyword, char*, char*, enum FILETYPE);
	unsigned int  (FSAPI *configuration_section_clear)(char *section);
	unsigned int  (FSAPI *configuration_section_clear_private)(char*, char*, enum FILETYPE);
	int  (FSAPI *configuration_section_exists)(char *section);
	int  (FSAPI *configuration_section_exists_private)(char*, char*, enum FILETYPE);
	unsigned int  (FSAPI *UNUSED_dialog_add_page)(int, int);
	unsigned int  (FSAPI *UNUSED_dialog_show)(int, void*);
	unsigned int  (FSAPI *dialog_show_modeless)(struct HWND__ *hDlg, int fShow);
	void  (FSAPI *UNUSED_dialog_update_modeless)();
	unsigned int  (FSAPI *UNUSED_dialog_show_wizard)(int, void*);
	unsigned int  (FSAPI *directory_create)(char *directory);
	unsigned int  (FSAPI *direct_play_lobby_startup_test)(void**, unsigned int*, int);
	unsigned int  (FSAPI *direct_play_lobby_connect)(void*, void**);
	unsigned int  (FSAPI *direct_play_lobby_interface_create)(void**);
	unsigned int  (FSAPI *dump_screen)(struct AWIND *window);
	unsigned int  (FSAPI *file_app_dir)(char *buf);
	unsigned int  (FSAPI *file_attributes_get)(unsigned int *attributes, char *fileroot, enum FILETYPE filetype);
	unsigned int  (FSAPI *file_attributes_set)(unsigned int attributes, char *fileroot, enum FILETYPE filetype);
	unsigned int  (FSAPI *file_close)(struct IStream *file);
	void  (FSAPI *file_close_abort)(struct IStream *file);
	unsigned int  (FSAPI *file_copy)(char *dst_file, enum FILETYPE dst_filetype, char *src_file, enum FILETYPE src_filetype);
	unsigned int  (FSAPI *file_create)(struct IStream **file, char*, enum FILETYPE filetype);
	unsigned int  (FSAPI *file_delete)(char*, enum FILETYPE filetype);
	unsigned int  (FSAPI *file_disk_space_free)(char *root_path, struct VAR64 *free_bytes);
	unsigned int  (FSAPI *file_disk_type)();
	unsigned int  (FSAPI *file_exist)(char *fileroot, enum FILETYPE filetype);
	unsigned int  (FSAPI *file_find_close)(void **handle); 
	unsigned int  (FSAPI *file_find_next)(void *handle, struct FINDDATA *finddata);
	unsigned int  (FSAPI *file_find_open)(void **handle, char *path, char *pattern, enum FILETYPE filetype);
	int  (FSAPI *file_is_network_path)(char *path);
	unsigned int  (FSAPI *file_move)(char *new_file, enum FILETYPE new_filetype, char *old_file, enum FILETYPE old_filetype);
	unsigned int  (FSAPI *file_name_length)(char *filename, enum FILETYPE filetype, unsigned int *filename_length);
	unsigned int  (FSAPI *file_name_temp)(char *temp_name, char *root_path);
	unsigned int  (FSAPI *file_name_valid)(char *filename);
	unsigned int  (FSAPI *file_open)(struct IStream **file, char *fileroot, enum FILETYPE filetype, enum FILEMODE mode);
	void  (FSAPI *file_open_abort)(struct IStream **file, char *fileroot, enum FILETYPE filetype, enum FILEMODE mode);
	unsigned int  (FSAPI *file_path_make)(char *path, char *drive, char *dir, char *fname);
	unsigned int  (FSAPI *file_path_split)(char *path, char *drive, char *dir, char *fname, enum FILETYPE *filetype);
	unsigned int  (FSAPI *file_path_valid)(char *pathname);
	unsigned int  (FSAPI *file_position_get)(struct IStream *file, unsigned int *position);
	unsigned int  (FSAPI *file_position_set)(struct IStream *file, unsigned int position, enum FILEPOSITION pos_origin);
	void  (FSAPI *file_position_set_abort)(struct IStream *file, unsigned int position, enum FILEPOSITION pos_origin);
	unsigned int  (FSAPI *file_read)(IStream *file, void *buf, unsigned int  readcount, unsigned int *bytesread);
	void  (FSAPI *file_read_abort)(IStream *file, void *buf, unsigned int readcount, unsigned int *bytesread);
	unsigned int  (FSAPI *file_size_get)(IStream *file, unsigned int *position);
	unsigned int  (FSAPI *file_size_set)(IStream *file, unsigned int size);
	unsigned int  (FSAPI *file_type_register)(char *extension, enum FILETYPE *type);
	unsigned int  (FSAPI *file_write)(IStream *file, void *buf, unsigned int writecount, unsigned int *byteswritten);
	unsigned int  (FSAPI *image_create)(struct IMAGE **image, unsigned int xsize, unsigned int ysize, enum IMG_FORMAT format);
	unsigned int  (FSAPI *image_destroy)(struct IMAGE *image);
	unsigned int  (FSAPI *image_resize)(struct IMAGE *image, unsigned int xsize, unsigned int ysize);
	unsigned int  (FSAPI *old_joystick_count)();
	unsigned int  (FSAPI *old_joystick_get)(int joystick_id, void *pJoyData);
	unsigned int  (FSAPI *old_joystick_type_get)(int joystick_id, void *joy_type);
	void  (FSAPI *old_joystick_sense)(int, int, void*);
	void  (FSAPI *old_joystick_code_to_string)(int, char*, unsigned int);
	void  (FSAPI *old_joystick_string_to_code)(char*, int*);
	unsigned int  (FSAPI *keyboard_create)(unsigned int*);
	unsigned int  (FSAPI *keyboard_destroy)(unsigned int*);
	unsigned int  (FSAPI *keyboard_enumerate)(unsigned int*, unsigned int);
	unsigned int  (FSAPI *keyboard_load)(unsigned int*, unsigned int);
	unsigned int  (FSAPI *keyboard_enable)(unsigned int);
	unsigned int  (FSAPI *keyboard_disable)(unsigned int);
	unsigned int  (FSAPI *keyboard_empty)(unsigned int);
	unsigned int  (FSAPI *keyboard_key_register)(unsigned int,KEYBOARDKEY *, unsigned int);
	unsigned int  (FSAPI *keyboard_key_unregister)(unsigned int, struct KEYBOARDKEY*, unsigned int);
	unsigned int  (FSAPI *keyboard_key_query)(unsigned int, struct KEYBOARDKEY*, unsigned int, enum KEYBOARDQ, struct KEYBOARDKEY*, unsigned int*);
	void  (FSAPI *keyboard_code_to_string)(unsigned int, char*, unsigned int);
	void  (FSAPI *keyboard_string_to_code)(char*, unsigned int*);
	void  (FSAPI *keyboard_translate_stupid_numpad_keys)(unsigned int*, unsigned int*, unsigned int*, unsigned int*);
	unsigned int  (FSAPI *memory_alloc)(int MemType, unsigned int size, void **ptr, int clear);
	unsigned int  (FSAPI *memory_realloc)(int MemType, unsigned int size, void **ptr);
	unsigned int  (FSAPI *memory_free)(void** ptr);
	unsigned int  (FSAPI *memory_set)(void *ptr, unsigned char val, unsigned int length);
	unsigned int  (FSAPI *memory_copy)(void *dest, void *src, unsigned int length);
	unsigned int  (FSAPI *memory_size_get)(int MemType, unsigned int *size);
	unsigned int  (FSAPI *memory_size_set)(int MemType, unsigned int size);
	int  (FSAPI *memory_compare)(void *pMem1, void *pMem2, unsigned int count);
	void  (FSAPI *memory_sort)(void*, unsigned int, unsigned int, int  (*)(void*, void*));
	void  (FSAPI *memory_swap)(void*, void*, unsigned int);
	int  (FSAPI *memory_search)(void*, void*, unsigned int, unsigned int, int  (*)(void*, void*), void**);
	int  (FSAPI *memory_validate)();
	unsigned int  (FSAPI *menu_create)(struct MENUITEM *menutop);
	unsigned int  (FSAPI *menu_destroy)();
	unsigned int  (FSAPI *menu_append)(unsigned short parent_id, unsigned short id, unsigned int string, unsigned int flags);
	unsigned int  (FSAPI *menu_append_indirect)(unsigned short parent_id, struct MENUITEM *menutop);
	unsigned int  (FSAPI *menu_insert)(unsigned short, unsigned short, unsigned int, unsigned int);
	unsigned int  (FSAPI *menu_insert_indirect)(unsigned short, struct MENUITEM*);
	unsigned int  (FSAPI *menu_attribute_set)(unsigned short, unsigned int);
	unsigned int  (FSAPI *menu_attribute_get)(unsigned short id, unsigned int *flags);
	unsigned int  (FSAPI *menu_delete)(unsigned short );
	unsigned int  (FSAPI *menu_replace_id)(unsigned short id, unsigned short new_id);
	unsigned int  (FSAPI *menu_replace_text)(unsigned short, unsigned int);
	int  (FSAPI *menu_exists)(unsigned short);
	unsigned int  (FSAPI *module_load)(struct FS6LINK**, char*, enum FILETYPE);
	unsigned int  (FSAPI *module_unload)(struct FS6LINK**);
	unsigned int  (FSAPI *UNUSED_mouse_as_yoke_start)(void  (*)(int, int, unsigned int));
	unsigned int  (FSAPI *UNUSED_mouse_as_yoke_stop)();
	unsigned int  (FSAPI *mouse_sense_register)(struct AWIND*, struct MOUSERECT*, int, int);
	unsigned int  (FSAPI *mouse_sense_update)(struct AWIND*, struct MOUSERECT*);
	unsigned int  (FSAPI *mouse_sense_unregister)(struct AWIND *window, struct MOUSERECT *mouse_rect);
	unsigned int  (FSAPI *mouse_sense_help_set)(int);
	void  (FSAPI *palette_get)(struct RGB*);
	void  (FSAPI *palette_set)(struct RGB*, struct RGB*);
	unsigned int  (FSAPI *resource_file_register)(char *basename, void **handle);
	unsigned int  (FSAPI *resource_file_register_self)(struct FS6LINK *Linkage, void **handle);
	unsigned int  (FSAPI *resource_file_unregister)(void *hResource);
	unsigned int  (FSAPI *resource_string_get)(unsigned int resid, char *buf, unsigned int bufcount);
	unsigned int  (FSAPI *resource_string_get_alloc)(unsigned int resid, char **buf);
	char*  (FSAPI *string_copy)(char *dest, char *src);
	char*  (FSAPI *string_copy_n)(char *dest, char *src, unsigned int n);
	char*  (FSAPI *string_concatenate)(char *dest, char *src);
	char*  (FSAPI *string_duplicate)(char *src);
	unsigned int  (FSAPI *string_length)(char *str);
	int  (FSAPI *string_compare)(char *str1, char *str2);
	int  (FSAPI *string_compare_n)(char *str1, char *str2, unsigned int n);
	int  (FSAPI *string_compare_ignore_case)(char *str1, char *str2);
	unsigned int  (FSAPI *string_shrink)(char*, char*, unsigned int);
	char*  (FSAPI *string_strip_char)(char*, char);
	int  (FSAPI *sound_device_available)();
	unsigned int  (FSAPI *sound_focus)(int focused);
	void*  (FSAPI *table_create_entry)(void *tbl);
	void  (FSAPI *table_deinit)(void *tbl);
	void  (FSAPI *table_delete_entry)(void *tbl, void *entry);
	void  (FSAPI *table_delete_all)(void *tbl);
	void*  (FSAPI *table_enumerate_entry)(void *tbl, void *userdata, FS6_UserEnumFunc);
	void*  (FSAPI *table_enumerate_entry_reverse)(void *tbl, void *userdata, FS6_UserEnumFunc);
	void*  (FSAPI *table_find_entry)(void *tbl, unsigned int offset, void *value, unsigned int size, int indirect);
	void*  (FSAPI *table_find_index)(void *tbl, unsigned int index);
	void  (FSAPI *timer_register)(void *Timer, unsigned int flag);
	void  (FSAPI *timer_start)(void *pv);
	void  (FSAPI *timer_stop)(void *pv);
	unsigned int  (FSAPI *UNUSED_app_window_print_clip)(struct AWIND*, struct PIXRECT*, unsigned int, unsigned int, char*, int, unsigned int);
	HRESULT  (FSAPI *gsnd_instance_get)(struct _GUID& rclsid, struct _GUID& riid, void **ppv);
	int  (FSAPI *direct_play_available)();
	unsigned int  (FSAPI *direct_play_enumerate)(void *pCallback, void *pData);
	unsigned int  (FSAPI *direct_play_interface_create)(struct _GUID *pGuid, void **ppInterface);
	unsigned int  (FSAPI *file_memory_map)(char *fileroot, enum FILETYPE filetype, enum FILEMODE mode, void **ptr, unsigned int *size);
	unsigned int  (FSAPI *file_memory_unmap)(void **prt);
	unsigned int  (FSAPI *file_get_time)(struct IStream *file, struct VAR64 *creation_time, struct VAR64 *last_access_time, struct VAR64 *last_write_time);
	unsigned int  (FSAPI *file_replace)(char *pszFilenameOld, enum FILETYPE filetype_old, char *pszFilenameTemp, enum FILETYPE filetype_temp);
	char*  (FSAPI *string_char_next)(char *pCurrent);
	char*  (FSAPI *string_char_previous)(char *pStart, char *pCurrent);
	int  (FSAPI *configuration_keyword_exists)(char*, char*);
	int  (FSAPI *configuration_keyword_exists_private)(char*, char*, char*, enum FILETYPE);
	unsigned int  (FSAPI *app_window_colortable_set)(struct AWIND *window, struct RGB32 *ColorTable, unsigned int cnt);
	unsigned int  (FSAPI *app_window_getdc)(struct AWIND *window, void **phdc);
	unsigned int  (FSAPI *app_window_releasedc)(struct AWIND *window, void *hdc);
	unsigned int  (FSAPI *module_ref_count_increment)(struct FS6LINK **Linkage);
	unsigned int  (FSAPI *app_font_get)(unsigned int, void*);
	unsigned int  (FSAPI *configuration_flush)(char*, enum FILETYPE);
	unsigned int  (FSAPI *mouse_sense_register_param)(struct AWIND*, struct MOUSERECT*, int, int, void*);
	unsigned int  (FSAPI *mouse_sense_update_param)(struct AWIND*, struct MOUSERECT*, void*);
	unsigned int  (FSAPI *configuration_sections_get_private)(char*, unsigned int, char*, enum FILETYPE);
	unsigned int  (FSAPI *find_file_type)(char*, enum FILETYPE*);
	unsigned int  (FSAPI *find_file_extension)(enum FILETYPE, char*);
	unsigned int  (FSAPI *file_path_qualify)(char*, char*, enum FILETYPE);
	unsigned int  (FSAPI *file_disk_space_size)(char*, struct VAR64*, struct VAR64*);
	unsigned int  (FSAPI *strtbl_create)(unsigned int uBuckets, unsigned int uExtra, unsigned int uFlags, struct _StringTable **ppst);
	unsigned int  (FSAPI *strtbl_destroy)(struct _StringTable **ppst);
	char*  (FSAPI *strtbl_add)(struct _StringTable *pst, char *str);
	char*  (FSAPI *strtbl_find)(struct _StringTable *pst, char *str);
	char*  (FSAPI *strtbl_remove)(struct _StringTable *pst, char *str);
	void  (FSAPI *strtbl_purge)(struct _StringTable *pst, char *str);
	void*  (FSAPI *strtbl_get_data_ptr)(struct _StringTable *pst, char *sz);
	void  (FSAPI *strtbl_enum)(struct _StringTable *pst, FS6_UserStrEnum, void *lParam);
	unsigned int  (FSAPI *configuration_int_get_16)(char*, char*, unsigned short*);
	unsigned int  (FSAPI *configuration_int_set_16)(char*, char*, unsigned short);
	unsigned int  (FSAPI *configuration_int_get_8)(char*, char*, unsigned char*);
	unsigned int  (FSAPI *configuration_int_set_8)(char*, char*, unsigned char);
	unsigned int  (FSAPI *app_font_get_by_name)(char*, void*);
	unsigned int  (FSAPI *app_font_release_by_name)(char*);
	char*  (FSAPI *construct_section_name)(char*, unsigned int);
	void  (FSAPI *construct_section_name_1)(char*, unsigned int, char*, unsigned int);
	void  (FSAPI *construct_section_name_2)(char*, unsigned int, unsigned int, char*, unsigned int);
	void  (FSAPI *parse_substrings)(char*, struct SUBSTRING*, unsigned int);
	char*  (FSAPI *string_end)(char*);
	void  (FSAPI *display_settings_get)(int*, char*, char*, unsigned int*, unsigned int*, int*, int*, int*, unsigned int*, enum PERFORMANCE_MODE*);
	void  (FSAPI *display_settings_set)(int, char*, char*, unsigned int, int);
	void  (FSAPI *display_devices_get)(int, char**, unsigned int*);
	void  (FSAPI *display_device_modes_get)(int, char*, char**, unsigned int*);
	void  (FSAPI *display_default_settings_get)(int*, char*, char*, unsigned int*, unsigned int*, int*);
	unsigned int  (FSAPI *ensure_file_name_valid)(char*);
	void  (FSAPI *app_window_paint)(struct AWIND *window);
	void  (FSAPI *BeginUI)();
	void  (FSAPI *EndUI)();
	unsigned int  (FSAPI *set_fullscreen_UI_params)(int nWidth, int nHeight);
	void  (FSAPI *get_fullscreen_UI_params)(int *pWidth, int *pHeight);
	unsigned int  (FSAPI *mouse_sense_set_tooltip_text)(struct AWIND *window, struct MOUSERECT *mouse_rect, char *text);
	unsigned int  (FSAPI *mouse_sense_get_displayed_tooltip)(struct AWIND *window, struct MOUSERECT **mouse_rect, void **user_data);
	int  (FSAPI *BlockUserInput)(int on);
	int  (FSAPI *UserInputBlocked)();
	unsigned int  (FSAPI *KeyEventUpMatch)(unsigned int down_event, int all_ups);
	char*  (FSAPI *string_concatenate_n)(char *dest, char *src, unsigned int n);
	int  (FSAPI *string_compare_ignore_case_n)(char*, char*, unsigned int);
	void  (FSAPI *app_window_enumerate_layers)(void  (*)(struct AWIND*, void*), void*);
	unsigned int  (FSAPI *file_append_async)(char *pszFile, enum FILETYPE filetype, void *pData, unsigned int uCount);
	void*  (FSAPI *module_proc_get)(char *pszProcName, char *pszModuleName);
	unsigned __int64  (FSAPI *timer_tickrate)();
	unsigned int  (FSAPI *file_opened_memory_map)(struct IStream*, void**, unsigned int*);
	unsigned int  (FSAPI *file_opened_memory_unmap)(struct IStream*, void*);
	unsigned int  (FSAPI *directory_exist)(char*);
	unsigned int  (FSAPI *configuration_get_index_section_count)(char*, char*, enum FILETYPE);
	int  (FSAPI *FS_ShellExecute)(struct HWND__ *hwnd, char *verb, char *file, char *parameters, char *directory, int showcommand);
	HRESULT  (FSAPI *GSnd_factory)(struct _GUID& rclsid, struct _GUID& riid, struct IUnknown **ppv);
	unsigned int  (FSAPI *ascii_to_blob)(char *pszAscii, void *pvBlob, int iBlobSizeInBytes);
	unsigned int  (FSAPI *blob_to_ascii)(void *pvBlob, int iBlobSizeInBytes, char *pszAscii);
	double  (FSAPI *application_elapsed_milliseconds_f64_get)();
	void  (FSAPI *NotifyUserAboutSoftwareMode)(unsigned int dwMessageID);
	unsigned int  (FSAPI *chain_insert)(enum CHAINID chain_id, FS6_ChainHandler);
	unsigned int  (FSAPI *chain_delete)(enum CHAINID chain_id, FS6_ChainHandler);
	void  (FSAPI *chain_execute)(enum CHAINID chain_id, unsigned int variable, void *pointer);
	void 	      (FSAPI *chain_insert2)(DWORD number,UserCallback func,DWORD p1,DWORD p2);
	void 	      (FSAPI *chain_delete2)(DWORD number,UserCallback func,DWORD p1,DWORD p2);
	unsigned int  (FSAPI *chain_insert_background_thread)(FS6_UserCallbackBackground, void *pFunctionArg1, void *pFunctionArg2);
	unsigned int  (FSAPI *chain_wait_background_thread)(unsigned int dwMillisecondsTimeout);
	unsigned int  (FSAPI *chain_get_background_times)(struct SVAR64 *pi64UserTime, struct SVAR64 *pi64KernelTime);
	void  (FSAPI *chain_suspend_background_thread)();
	void  (FSAPI *chain_resume_background_thread)();
	unsigned int  (FSAPI *chain_execute_jobs)(int iTimeSliceInMicroSec);
	unsigned int  (FSAPI *chain_execute_job_queue)(int iQueueID, int iTimeSliceInMicroSec);
	unsigned int  (FSAPI *chain_set_mode)(int bStartupMode);
	int  (FSAPI *chain_fiber_yield)(int bGiveUpRemainingTime, int bAllowPrimaryFiber);
	unsigned int  (FSAPI *chain_add_job)(int iQueueID, class JobTemplate *pJob);
	unsigned int  (FSAPI *chain_cancel_job)(class JobTemplate *pJob);
	void  (FSAPI *splash_show)(int fShow);
	unsigned int  (FSAPI *mouse_redispatch_message)(struct MOUSE_RAW_MESSAGE*, struct AWIND*, struct AWIND*, struct PIXPOINT, struct MOUSERECT*, struct MOUSERECT*, void*);
	unsigned int  (FSAPI *mouse_get_current_position)(struct AWIND*, struct PIXPOINT*, unsigned int*);
	struct MOUSERECT*  (FSAPI *mouse_get_captured_rectangle)();
	unsigned int  (FSAPI *application_event_source)(unsigned int, unsigned int, enum EVENT_SOURCE_TYPE);
	unsigned int  (FSAPI *application_event_source_immediate)(unsigned int, unsigned int, enum EVENT_SOURCE_TYPE);
	void*  (FSAPI *app_window_hwnd_get)(struct AWIND *window);
	unsigned int  (FSAPI *file_shell_path_make)(int, char*, unsigned int, char*, char*);
	unsigned int  (FSAPI *directory_delete)(char *directory);
	double  (FSAPI *application_executive_loop_seconds_f64_get)();
	double  (FSAPI *application_elapsed_seconds_f64_get)();
	unsigned __int64  (FSAPI *application_elapsed_ticks_get)();
	unsigned __int64  (FSAPI *application_seconds_to_ticks_convert)(double dSeconds);
	double  (FSAPI *application_ticks_to_seconds_convert)(unsigned __int64 uTicks);
	void  (FSAPI *chain_call_on_foreground_thread)(void  (*)(void*), void *userdata);
	void  (FSAPI *application_enable_popup_menus)(int bEnable);
	int*  (FSAPI *application_exception_function_get)();
};

#pragma pack()

#endif