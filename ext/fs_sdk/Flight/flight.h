/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: flight.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.3 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_FLIGHT
#define _FS_SDK_FLIGHT

#pragma pack(4)

#include"..\common\container.h"

enum FAC_APPROACH_TYPE {
	FAC_APPROACH_TYPE_UNDEFINED = 0x0,
	FAC_APPROACH_TYPE_GPS = 0x1,
	FAC_APPROACH_TYPE_VOR = 0x2,
	FAC_APPROACH_TYPE_NDB = 0x3,
	FAC_APPROACH_TYPE_ILS = 0x4,
	FAC_APPROACH_TYPE_LOCALIZER = 0x5,
	FAC_APPROACH_TYPE_SDF = 0x6,
	FAC_APPROACH_TYPE_LDA = 0x7,
	FAC_APPROACH_TYPE_VORDME = 0x8,
	FAC_APPROACH_TYPE_NDBDME = 0x9,
	FAC_APPROACH_TYPE_RNAV = 0xa,
	FAC_APPROACH_TYPE_LOCALIZER_BACK_COURSE = 0xb,
};

enum GPS_APPR_WP_TYPE {
	GPS_APPR_WP_NONE = 0x0,
	GPS_APPR_WP_FIX = 0x1,
	GPS_APPR_WP_PROC_TURN_LEFT = 0x2,
	GPS_APPR_WP_PROC_TURN_RIGHT = 0x3,
	GPS_APPR_WP_DME_ARC_LEFT = 0x4,
	GPS_APPR_WP_DME_ARC_RIGHT = 0x5,
	GPS_APPR_WP_HOLDING_LEFT = 0x6,
	GPS_APPR_WP_HOLDING_RIGHT = 0x7,
	GPS_APPR_WP_DISTANCE = 0x8,
	GPS_APPR_WP_ALTITUDE = 0x9,
	GPS_APPR_WP_MANUAL_SEQ = 0xa,
	GPS_APPR_WP_VECTORS_TO_FINAL = 0xb,
};

enum GPS_APPR_TYPE {
	GPS_APPR_NONE = 0x0,
	GPS_APPR_TRANSITION = 0x1,
	GPS_APPR_FINAL = 0x2,
	GPS_APPR_MISSED = 0x3,
};

enum GPS_APPR_SEGMENT_TYPE {
	GPS_APPR_SEGMENT_LINE = 0x0,
	GPS_APPR_SEGMENT_ARC_CW = 0x1,
	GPS_APPR_SEGMENT_ARC_CCW = 0x2,
};

enum ICAO_TYPE {
	ICAO_TYPE_NONE = 0x0,
	ICAO_TYPE_AIRPORT = 0x41,
	ICAO_TYPE_RUNWAY = 0x52,
	ICAO_TYPE_LOCALIZER = 0x4c,
	ICAO_TYPE_VOR = 0x56,
	ICAO_TYPE_NDB = 0x4e,
	ICAO_TYPE_TERMINAL_NDB = 0x58,
	ICAO_TYPE_WAYPOINT = 0x57,
	ICAO_TYPE_TERMINAL_WAYPOINT = 0x54,
	ICAO_TYPE_MARKER = 0x4d,
};

enum TRANSITION_TO_APPROACH_TYPE {
	TRANSITION_TO_APPROACH_NONE = 0x0,
	TRANSITION_TO_APPROACH_FROM_CURRENT = 0x1,
	TRANSITION_TO_APPROACH_FROM_FLIGHT_PLAN = 0x2,
	TRANSITION_TO_APPROACH_FROM_CURRENT_WHEN_ACTIVATED = 0x3,
	TRANSITION_TO_APPROACH_FROM_FLIGHT_PLAN_CURRENT_WHEN_ACTIVATED = 0x4,
};

typedef struct GPS_DATA {
  /*<thisrel this+0x0>*/ /*|0x4|*/ unsigned int flags;
  /*<thisrel this+0x4>*/ /*|0x4|*/ int id_FlightPlan;
  /*<thisrel this+0x8>*/ /*|0x4|*/ int time;
  /*<thisrel this+0xc>*/ /*|0x18|*/ struct _llaf64 m_position;
  /*<thisrel this+0x24>*/ /*|0x8|*/ double Magvar;
  /*<thisrel this+0x2c>*/ /*|0x8|*/ double grnd_spd;
  /*<thisrel this+0x34>*/ /*|0x8|*/ double grnd_hdg;
  /*<thisrel this+0x3c>*/ /*|0x8|*/ double grnd_trk;
  /*<thisrel this+0x44>*/ /*|0x8|*/ double wp_distance;
  /*<thisrel this+0x4c>*/ /*|0x8|*/ double wp_bearing;
  /*<thisrel this+0x54>*/ /*|0x8|*/ double wp_cross_trk;
  /*<thisrel this+0x5c>*/ /*|0x8|*/ double wp_req_hdg;
  /*<thisrel this+0x64>*/ /*|0x8|*/ double wp_angle_error;
  /*<thisrel this+0x6c>*/ /*|0x8|*/ double wp_final_heading;
  /*<thisrel this+0x74>*/ /*|0x8|*/ double wp_vertical_speed;
  /*<thisrel this+0x7c>*/ /*|0x1|*/ bool wp_prev_valid;
  /*<thisrel this+0x7d>*/ /*|0x8|*/ char wp_prev_name[8];
  /*<thisrel this+0x88>*/ /*|0x18|*/ struct _llaf64 wp_prev;
  /*<thisrel this+0xa0>*/ /*|0x8|*/ char wp_next_name[8];
  /*<thisrel this+0xa8>*/ /*|0x18|*/ struct _llaf64 wp_next;
  /*<thisrel this+0xc0>*/ /*|0x8|*/ double wp_target;
  /*<thisrel this+0xc8>*/ /*|0x18|*/ struct _llaf64 wp_app_next;
  /*<thisrel this+0xe0>*/ /*|0x4|*/ int wp_ete;
  /*<thisrel this+0xe4>*/ /*|0x4|*/ int wp_eta;
  /*<thisrel this+0xe8>*/ /*|0x8|*/ double segm_distance;
  /*<thisrel this+0xf0>*/ /*|0x8|*/ double segm_length;
  /*<thisrel this+0xf8>*/ /*|0x4|*/ enum GPS_APPR_TYPE app_mode;
  /*<thisrel this+0xfc>*/ /*|0x4|*/ enum GPS_APPR_WP_TYPE app_wp_type;
  /*<thisrel this+0x100>*/ /*|0x4|*/ enum GPS_APPR_SEGMENT_TYPE app_segment_type;
  /*<thisrel this+0x104>*/ /*|0x1|*/ bool app_is_runway;
  /*<thisrel this+0x108>*/ /*|0x8|*/ double ap_req_magnetic_heading;
}GPS_DATA,*PGPS_DATA,**PPGPS_DATA; // <size 0x110>

typedef struct GPS_WPINFO {
   struct _llaf64 lla;
   char name[8];
   double true_hdg;
   double magnetic_hdg;
   double gs_estimate;
   double gs_actual;
   double height;
   double dist_leg;
   double dist_total;
   double dist_rem;
   double fRemainingDistance;
   double fRemainingTotalDistance;
   int time_zone_deviation;
   int ete;
   int ate;
   int est_time_remaining;
   int eta_cur;
   double fuel_rem_when_arrived;
   double fuel_leg_estimate;
   double fuel_leg_actual;
}GPS_WPINFO,*PGPS_WPINFO,**PPGPS_WPINFO;

class ICAO {
public:
  unsigned char m_aData[12];
  unsigned char m_eType;
  char m_sRegion[2];
  char m_sAirport[4];
  char m_sIdent[5];
  ICAO(char*);
  ICAO(enum ICAO_TYPE,char*,char*,char*);
  ICAO();
  int  Compare(class ICAO&);
  int  CompareRegions(class ICAO&);
  int  CompareIdents(char*);
  int  CompareIdents(class ICAO&);
  int  CompareAirports(class ICAO&);
  bool IsEqual(class ICAO&);
  bool IsTypeEqual(class ICAO&);
  bool IsRegionEqual(class ICAO&);
  bool IsIdentEqual(class ICAO&);
  bool IsAirportEqual(class ICAO&);
  bool IsInvalid();
  bool IsAirportBlank();
  bool operator<(class ICAO&);
  bool operator>(class ICAO&);
  bool operator==(class ICAO&);
  bool operator<=(class ICAO&);
  bool operator>=(class ICAO&);
  bool operator!=(class ICAO&);
  void ClearAirport();
  enum ICAO_TYPE GetType();
  void GetIcaoString(char*,unsigned int);
  void GetRegion(char*,unsigned int);
  void GetIdent(char*,unsigned int);
  void GetAirport(char*,unsigned int);
  bool PartialIdentMatch(char*,unsigned int);
  void MakeUpper();
};

typedef struct GPS_APPR_SEGMENT_INFO {
  enum GPS_APPR_SEGMENT_TYPE eType;
  struct _llf64 llStart;
  struct _llf64 llEnd;
  struct _llf64 llOrigin;
  double fLength;
}GPS_APPR_SEGMENT_INFO,*PGPS_APPR_SEGMENT_INFO,**PPGPS_APPR_SEGMENT_INFO;

struct GPS_APPR_WPINFO {
	enum GPS_APPR_WP_TYPE eType;
	enum GPS_APPR_TYPE eApprType;
	char szName[8];
	ICAO Icao;
	struct _llaf64 llaPosition;
	double fTarget;
	double fLegDistance;
	double fLegTotalDistance;
	double fLegFromDistance;
	double fRemainingDistance;
	double fRemainingTotalDistance;
	double fCourse;
	double fAltitude;
	struct GPS_APPR_SEGMENT_INFO* pSegments;
	unsigned int nSegments;
	GPS_APPR_WPINFO();
};

typedef struct _FLIGHTPLANNONALLOCED {
  /*<thisrel this+0x0>*/ /*|0x4|*/ enum _fptype_ m_fptype;
  /*<thisrel this+0x4>*/ /*|0x4|*/ unsigned int m_uRouteType;
  /*<thisrel this+0x8>*/ /*|0xb|*/ char m_szDepartureId[11];
  /*<thisrel this+0x14>*/ /*|0x18|*/ struct LATLONALT m_llaDeparture;
  /*<thisrel this+0x2c>*/ /*|0xb|*/ char m_szDestinationId[11];
  /*<thisrel this+0x38>*/ /*|0x18|*/ struct LATLONALT m_llaDestination;
  /*<thisrel this+0x50>*/ /*|0xb|*/ char m_szAlternateId[11];
  /*<thisrel this+0x5c>*/ /*|0x18|*/ struct LATLONALT m_llaAlternate;
  /*<thisrel this+0x74>*/ /*|0x4|*/ unsigned int m_utcDepartureTime;
  /*<thisrel this+0x78>*/ /*|0x4|*/ int m_altCruising;
  /*<thisrel this+0x7c>*/ /*|0x104|*/ char m_szTitle[260];
  /*<thisrel this+0x180>*/ /*|0x104|*/ char m_szFilename[260];
  /*<thisrel this+0x284>*/ /*|0x10|*/ char m_szDeparturePos[16];
}_FLIGHTPLANNONALLOCED,*P_FLIGHTPLANNONALLOCED,**PP_FLIGHTPLANNONALLOCED;

class ATC_Waypoint {
public:
  static TMemoryPool<ATC_Waypoint,50>& ms_memoryPool;
  CSmartPtrRefCount m_smartPtrRefCount;
  char m_szNameId[11];
  char m_szAirwayId[11];
  enum ATC_WAYPOINT_TYPE m_waypttype;
  struct LATLONALT m_lla;
  int m_altMin;
  int m_bWaypointFreqQueried;
  int m_iFpIndex;
  unsigned int m_uFreq;
  class ICAO m_icao;
  //static void* __stdcall res1; //operator new(unsigned int);
  //static void __stdcall res2; //operator delete(void*);
  static class TSmartPtr<ATC_Waypoint> __stdcall New(struct LATLONALT&,class ICAO&,char*,char*,int,int);
  static class TSmartPtr<ATC_Waypoint> __stdcall New(struct LATLONALT&,enum ATC_WAYPOINT_TYPE,char*,char*,int,int);
  static class TSmartPtr<ATC_Waypoint> __stdcall Load(char*,unsigned int);
  static class TSmartPtr<ATC_Waypoint> __stdcall LoadFs7(char*,unsigned int);
  int __stdcall AddRef();
  int __stdcall Release();
  int __stdcall GetRefCount();
  void SmartPtrAddRef();
  void SmartPtrRelease();
  int SmartPtrGetRefCount();
  char* NameIdGet();
  char* AirwayIdGet();
  enum ATC_WAYPOINT_TYPE WaypttypeGet();
  struct LATLONALT& LlaGet();
  int AltMin();
  int FpIndexGet();
  void FpIndexSet(int);
  unsigned int FreqGet();
  class ICAO& IcaoGet();
  void IcaoSet(class ICAO&);
  ATC_Waypoint(class ATC_Waypoint&);
  ATC_Waypoint(struct LATLONALT&,class ICAO&,char*,char*,int,int);
  ATC_Waypoint(struct LATLONALT&,enum ATC_WAYPOINT_TYPE,char*,char*,int,int);

  virtual /*<vtableoff 0x0>*/ ~ATC_Waypoint();
  virtual /*<vtableoff 0x4>*/ TSmartPtr<ATC_Waypoint> Clone();
  virtual /*<vtableoff 0x8>*/ int LlaQuery();
  virtual /*<vtableoff 0xc>*/ int FreqQuery();
  virtual /*<vtableoff 0x10>*/ void Save(char*,unsigned int);
};

class FacilityAirport {
};

class FacilityStart {
};

class FacilityTaxiParking {
};

template<class T> struct v3d {
  T x;
  T y;
  T z;
  v3d<T> operator-(v3d<T>&);
  v3d<T> operator-();
  v3d<T> operator+(v3d<T>&);
  v3d<T>& operator+=(v3d<T>&);
  v3d<T>& operator-=(v3d<T>&);
  bool operator==(v3d<T>&);
  bool operator!=(v3d<T>&);
  v3d<T>& zero();
  T length();
  T norm();
  T normz();
};

class ATC_FlightPlan {
public:
  static TMemoryPool<ATC_FlightPlan,50>& ms_memoryPool;
  class CSmartPtrRefCount m_smartPtrRefCount;
  _FLIGHTPLANNONALLOCED m_fpna;
  char* m_pdeparture_name;
  char* m_pdestination_name;
  char* m_palternate_name;
  char* m_pszDescription;
//  TVector<TSmartPtr<ATC_Waypoint>> m_waypoints;
  //static void* __stdcall operator new(unsigned int);
  //static void __stdcall operator delete(void*);
  static class TSmartPtr<ATC_FlightPlan> __stdcall New();
  int __stdcall AddRef();
  int __stdcall Release();
  int __stdcall GetRefCount();
  void SmartPtrAddRef();
  void SmartPtrRelease();
  int SmartPtrGetRefCount();
  unsigned int WaypointCount();
  class TVector<TSmartPtr<ATC_Waypoint> >& WaypointArrayGet();
  unsigned int DepartureTimeGet();
  void DepartureTimeSet(unsigned int);
  int CruisingAltitudeGet();
  int CruisingAltitudeSet(int);
  char* TitleGet();
  char* FilenameGet();
  char* DescriptionGet();
  char* DepartureIdGet();
  char* DepartureNameGet();
  void DepartureLlaGet(struct LATLONALT*);
  void DepartureLlaSet(struct LATLONALT*);
  void DestinationLlaGet(struct LATLONALT*);
  void DestinationLlaSet(struct LATLONALT*);
  void AlternateLlaGet(struct LATLONALT*);
  void AlternateLlaSet(struct LATLONALT*);
  char* DestinationIdGet();
  char* DestinationNameGet();
  char* AlternateIdGet();
  char* AlternateNameGet();
  int IsVFR();
  int IsIFR();
  enum _fptype_ TypeGet();
  void TypeSet(enum _fptype_);
  unsigned int RouteTypeGet();
  void RouteTypeSet(unsigned int);
  ATC_FlightPlan(class ATC_FlightPlan&);
  ATC_FlightPlan();
  int _AirportInfoLoad(struct FLIGHT_FILE_INFO*,char*,char*,struct LATLONALT*,char*,int);
  virtual  ~ATC_FlightPlan();
  virtual  class TSmartPtr<ATC_FlightPlan> Clone();
  virtual  unsigned int Load(struct FLIGHT_FILE_INFO*,char*);
  virtual  unsigned int Load(char*);
  virtual  unsigned int Save(struct FLIGHT_FILE_INFO*,char*);
  virtual  unsigned int Save(char*);
  virtual  unsigned int Validate();
  virtual  void WaypointAdd(class ATC_Waypoint*,unsigned int);
  virtual  void WaypointAdd(struct LATLONALT&,class ICAO&,char*,char*,int,unsigned int);
  virtual  void WaypointAdd(struct LATLONALT&,char*,char*,enum ATC_WAYPOINT_TYPE,int,unsigned int);
  virtual  unsigned int WaypointDelete(unsigned int);
  virtual  class TSmartPtr<ATC_Waypoint> WaypointGet(unsigned int);
  virtual  void TitleSet(char*);
  virtual  void FilenameSet(char*);
  virtual  int DescriptionSet(char*);
  virtual  void DepartureIdSet(char*);
  virtual  void DepartureNameSet(char*);
  virtual  void DestinationIdSet(char*);
  virtual  void DestinationNameSet(char*);
  virtual  void AlternateIdSet(char*);
  virtual  void AlternateNameSet(char*);
  virtual  char* DeparturePosGet();
  virtual  class TSmartPtr<FacilityAirport> DeparturePosGet(class FacilityStart**,class FacilityTaxiParking**);
  virtual  void DeparturePosSetStart(class FacilityAirport&,class FacilityStart&);
  virtual  void DeparturePosSetParking(class FacilityAirport&,class FacilityTaxiParking&);
  virtual  int RouteFind(int,enum _AirwayType);
  virtual  int DepartureAirportPositionGet(union LATLONALTPBH*,union LATLONALTPBH*);
  virtual  void CruisingAltitudeCompute();
  virtual  void MSAAdjust();
  virtual  int StartWaypointCompute(struct LATLONALT&);
  virtual  void DeparturePosSet(char*);
};

class CGPS_Engine : public GPS_DATA  {
public:
//   TSmartPtr<ATC_FlightPlan> m_spFlightPlan;
//   TSmartPtr<ATC_FlightPlan> m_spSavedFlightPlan;
   struct GPS_WPINFO* pWPInfo;
   unsigned int m_nWayPoints;
   struct GPS_APPR_WPINFO* m_pApproachWP;
   unsigned int m_nApproachWP;
   struct GPS_APPR_SEGMENT_INFO* m_pApproachSegments;
   class ICAO m_ApproachAirportIcao;
   int m_ApproachAirportApproach;
   char m_ApproachAirportApproachName[14];
   int m_ApproachAirportTransition;
   char m_ApproachAirportTransitionName[8];
   bool m_ApproachMissed;
   enum FAC_APPROACH_TYPE m_ApproachType;
   enum TRANSITION_TO_APPROACH_TYPE m_ApproachTransitionToApproach;
   int m_ApproachTimeZoneDeviation;
   unsigned int m_nNextWayPoint;
   unsigned int m_nNextApproachPoint;
   unsigned int m_nNextApproachSegment;
   v3d<double> vectorAutoWP;
   int time_wp;
   int arrive_time;
   int route_ete;
   int route_eta;
   double total_distance;
   double total_est_fuel;
   int timeFlightPath;
   unsigned int nMaxFlightPathPoints;
   unsigned int nCountFlightPathPoints;
   struct _llaf64* pFlightPath;
   static class CGPS_Engine* __stdcall New(class TSmartPtr<ATC_FlightPlan>);
  void initialize_WPInfo(double,double,double);
  void DeleteCurrentFlightPath();
  void add_point_flight_path();
  double _MpsAdjustForWinds(double,double,double,struct _llaf64&);
  void SetDirectTo(struct _llaf64&,char*);
  void SetDirectTo(struct _llaf64&,class ICAO&);
  void SetDirectTo(struct LATLONALT&,char*,int);
  void SetDirectTo(class ATC_Waypoint*);
  void AddWaypoint(int,struct _llaf64&,char*);
  void AddWaypoint(int,struct _llaf64&,class ICAO&);
  void AddWaypoint(int,class ATC_Waypoint*);
  void SetNextApproachSegment(unsigned int,bool);
  void CalculateApproachWaypoints();
  void SetUpdateWPInfo();
  void SetActiveWaypointLock(bool);
  bool isUpdatedWPInfo();
  bool isFlightPlanActive();
  bool isActiveWayPoint();
  bool isDirectToFlightPlan();
  bool isActiveWaypointLocked();
  bool isArrived();
  bool isPlaneStarted();
  bool isApproachLoaded();
  bool isApproachActive();
  bool isApproachFinished();
  CGPS_Engine(class CGPS_Engine&);
  CGPS_Engine(class TSmartPtr<ATC_FlightPlan>);
  void newPosition(int,struct LATLONALT&,double,double,double,int);
  void OnDeleteFlightPlan();
  void OnSetFlightPlan(class TSmartPtr<ATC_FlightPlan>,bool);
  void SaveToFlightFile(void*);
  void LoadFromFlightFile(void*);
  void RecalcFuelAndSpeed();
  unsigned int GetNextFlightPlanWP();
  unsigned int GetCountFlightPlanWP();
  unsigned int GetCountFlightPathPoints();
  double GetCurrentGS();
  double GetTotalDistance();
  int GetArriveTime();
  double GetTotalEstFuel();
  int GetRouteETE();
  int GetRouteETA();
  void CleanFlightPath();
  class ICAO& GetApproachAirportIcao();
  void GetApproachAirportIdent(char*,unsigned int);
  int GetApproachAirportApproach();
  char* GetApproachAirportApproachName();
  int GetApproachAirportTransition();
  char* GetApproachAirportTransitionName();
  bool GetIsApproachMissed();
  bool GetIsApproachFinal();
  unsigned int GetApproachWPIndex();
  unsigned int GetApproachWPCount();
  enum FAC_APPROACH_TYPE GetApproachType();
  int GetApproachTimeZoneDeviation();
  virtual ~CGPS_Engine();
  virtual void UpdateWPInfo();
  virtual void SetNextWayPoint(unsigned int);
  virtual void SetNextApproachWaypoint(unsigned int);
  virtual bool GetWPInfo(unsigned int,struct GPS_WPINFO*);
  virtual void CancelDirectTo();
  virtual void NewFlightPlan(char*);
  virtual void DeleteWaypoint(int);
  virtual class TSmartPtr<ATC_FlightPlan> GetFlightPlan();
  virtual double GetDirForAP();
  virtual bool GetApproachWPInfo(unsigned int,struct GPS_APPR_WPINFO*);
  virtual void UnloadApproach();
  virtual void LoadApproach(class FacilityAirport*,int,char*,int,char*,bool,void*,unsigned int,enum TRANSITION_TO_APPROACH_TYPE);
  virtual void ActivateApproach();
  virtual void DeactivateApproach();
  virtual double GetAircraftTurnRadius();
  virtual double GetAircraftSpeed();
  virtual double GetAircraftClimbRate();
};

struct FLIGHT {
	FSLINKAGE head;
    int  (FSAPI *DlgFlightPlannerMain)();
    void  (FSAPI *create_temp_flight)();
    void  (FSAPI *delete_temp_flight)();
    void*  (FSAPI *DoFacilityPickerDlg)(int,void*);
    unsigned int  (FSAPI *flight_reset)(int);
    unsigned int  (FSAPI *flight_load)(struct FILEDATA*,enum FLIGHTTYPE);
    unsigned int  (FSAPI *flight_save)(char*,char*);
    unsigned int  (FSAPI *flight_save_by_name)(struct FILEDATA*,enum FLIGHTTYPE,char*,char*);
    unsigned int  (FSAPI *flight_get)(struct FLIGHT_FILE_INFO*);
    enum FLIGHTTYPE  (FSAPI *flight_get_type)();
    unsigned int  (FSAPI *flight_items_read)(char*,struct CONFIG_ITEM*,void*,struct FLIGHT_FILE_INFO*);
    unsigned int  (FSAPI *flight_items_write)(char*,struct CONFIG_ITEM*,void*,struct FLIGHT_FILE_INFO*);
    unsigned int  (FSAPI *flight_items_read_current)(char*,struct CONFIG_ITEM*,void*);
    unsigned int  (FSAPI *flight_item_read)(char*,struct CONFIG_ITEM*,void*,struct FLIGHT_FILE_INFO*);
    unsigned int  (FSAPI *flight_item_write)(char*,struct CONFIG_ITEM*,void*,struct FLIGHT_FILE_INFO*);
    unsigned int  (FSAPI *flight_read)(char*,char*,void*,enum CONFIG_ITEM_TYPE,union CONFIG_ITEM_TYPE_DATA,struct FLIGHT_FILE_INFO*);
    unsigned int  (FSAPI *flight_write)(char*,char*,void*,enum CONFIG_ITEM_TYPE,union CONFIG_ITEM_TYPE_DATA,struct FLIGHT_FILE_INFO*);
    void*  (FSAPI *flight_get_gps_engine)();
    void*  (FSAPI *flight_get_gps_data)();
    CGPS_Engine* (FSAPI *CGPS_Engine_New)(TSmartPtr<ATC_FlightPlan>);
    TSmartPtr<ATC_FlightPlan>  (FSAPI *get_active_flight_plan)();
    void  (FSAPI *set_active_flight_plan)(TSmartPtr<ATC_FlightPlan>);
    TSmartPtr<ATC_FlightPlan>  (FSAPI *ATC_FlightPlan_New)();
    TSmartPtr<ATC_Waypoint>  (FSAPI *ATC_Waypoint_NewByName)(struct LATLONALT&,enum ATC_WAYPOINT_TYPE,char*,char*,int,int);
    TSmartPtr<ATC_Waypoint>  (FSAPI *ATC_Waypoint_NewByIcao)(struct LATLONALT&,class ICAO&,char*,char*,int,int);
    TSmartPtr<ATC_Waypoint>  (FSAPI *ATC_Waypoint_Load)(char*,unsigned int);
    void  (FSAPI *UpdateFlightPlan)(class TSmartPtr<ATC_FlightPlan>,void*,int,unsigned int,void*,class TSmartPtr<ATC_FlightPlan>*);
};

#pragma pack()

#endif
