/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: sound.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_FSUI
#define _FS_SDK_FSUI

#pragma pack(4)

struct FSUI {
    FSLINKAGE head;
    void  (FSAPI *_FSLoadCursor)(int);
    void  (FSAPI *Linkage01)();
    void  (FSAPI *Linkage02)();
    void  (FSAPI *Linkage03)();
    void  (FSAPI *Linkage04)();
    void  (FSAPI *Linkage05)();
    void  (FSAPI *Linkage06)();
    void  (FSAPI *Linkage07)();
    void  (FSAPI *Linkage08)();
    void  (FSAPI *Linkage09)();
    void  (FSAPI *Linkage10)();
    void  (FSAPI *Linkage11)();
    void  (FSAPI *Linkage12)();
    void  (FSAPI *Linkage13)();
    void  (FSAPI *Linkage14)();
    void  (FSAPI *Linkage15)();
    void  (FSAPI *Linkage16)();
    void  (FSAPI *Linkage17)();
    void  (FSAPI *Linkage18)();
    void  (FSAPI *Linkage19)();
    void  (FSAPI *Linkage20)();
    void  (FSAPI *Linkage21)();
    void  (FSAPI *Linkage22)();
    void  (FSAPI *Linkage23)();
};

#pragma pack()

#endif
