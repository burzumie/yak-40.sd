/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: controls.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_CONTROLS
#define _FS_SDK_CONTROLS

#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

#pragma pack(4)

enum FORCE_TRIGGER_ID {
	FORCE_NULL = 0x0,
	FORCE_FEEDBACK_STATUS_GET = 0x1,
	FORCE_FEEDBACK_STATUS_SET = 0x2,
	FORCE_STICK_XY = 0x3,
	FORCE_STICK_SHAKER = 0x4,
	FORCE_GEAR_NOSE_BUMP = 0x5,
	FORCE_GEAR_LEFT_BUMP = 0x6,
	FORCE_GEAR_RIGHT_BUMP = 0x7,
	FORCE_CRASH = 0x8,
	FORCE_GROUND_BUMPS = 0x9,
	FORCE_MACHINEGUN = 0xa,
	FORCE_BULLETDAMAGE = 0xb,
	FORCE_ROCKET = 0xc,
	FORCE_BOMB = 0xd,
	FORCE_FLAK = 0xe,
};

typedef struct EVENTCODE {
    unsigned int event_id;
    unsigned int code;
}EVENTCODE,*PEVENTCODE,**PPEVENTCODE;
/*
typedef struct DIDEVCAPS {
	DWORD   dwSize;
	DWORD   dwFlags;
	DWORD   dwDevType;
	DWORD   dwAxes;
	DWORD   dwButtons;
	DWORD   dwPOVs;
#if(DIRECTINPUT_VERSION >= 0x0500)
	DWORD   dwFFSamplePeriod;
	DWORD   dwFFMinTimeResolution;
	DWORD   dwFirmwareRevision;
	DWORD   dwHardwareRevision;
	DWORD   dwFFDriverVersion;
#endif 
} DIDEVCAPS, *LPDIDEVCAPS;
*/
struct stJoyInfo {
	GUID					deviceGUID;
	LPDIRECTINPUTDEVICE8A	lpDirectInputDevice;
	DIDEVCAPS	            diDevCaps;

	unsigned long bAxisPresenceFlags;
	unsigned long bPOVPresenceFlags;

	char _unknown1[512];

	unsigned long X_axis_keyevent;
	unsigned long Y_axis_keyevent;
	unsigned long Rx_axis_keyevent;
	unsigned long Ry_axis_keyevent;
	unsigned long Rz_axis_keyevent;
	unsigned long Z_axis_keyevent;
	unsigned long Slider1_axis_keyevent;
	unsigned long Slider2_axis_keyevent;

	signed   long X_axis_scale; // 1 .. 127 - if negative axis is reversed. 0 - if axis is unused
	signed   long Y_axis_scale;
	signed   long Rx_axis_scale;
	signed   long Ry_axis_scale;
	signed   long Rz_axis_scale;
	signed   long Z_axis_scale;
	signed   long Slider1_axis_scale;
	signed   long Slider2_axis_scale;

	unsigned long X_axis_nullzone; // 1 .. 127 (min .. max). 0 - if axis is unused
	unsigned long Y_axis_nullzone;
	unsigned long Rx_axis_nullzone;
	unsigned long Ry_axis_nullzone;
	unsigned long Rz_axis_nullzone;
	unsigned long Z_axis_nullzone;
	unsigned long Slider1_axis_nullzone;
	unsigned long Slider2_axis_nullzone;
};

struct stJoyInfoPtrArray {
	stJoyInfo*       pJoyInfo[16];
	unsigned __int32 nCount;
};

struct CONTROLS {
	FSLINKAGE head;
    int  (FSAPI *sensitivity_dialog_process)();
    int  (FSAPI *assignment_dialog_process)();
    int  (FSAPI *forces_dialog_process)();
    int  (FSAPI *calibrate_dialog_process)(void*);
    void  (FSAPI *controls_startup)();
    void  (FSAPI *controls_shutdown)();
    void  (FSAPI *controls_keyboard_code_get)(unsigned int, struct EVENTCODE*);
    unsigned int  (FSAPI *controls_get_event_id)(char*);
    char*  (FSAPI *controls_get_event_name)(unsigned int);
    unsigned int  (FSAPI *controls_get_event_display_name)(unsigned int, char*, unsigned int);
    unsigned int  (FSAPI *controls_get_event_display_keycode)(unsigned int, char*, unsigned int);
    void  (FSAPI *controls_force_trigger)(enum FORCE_TRIGGER_ID, void*);
    void  (FSAPI *EnableJoystick)(int);
    void  (FSAPI *EnableForceFeedJoystick)(int);
    int  (FSAPI *IsJoystickEnabled)();
    int  (FSAPI *IsForceFeedJoystickEnabled)();
    int  (FSAPI *IsForceFeedJoystickAvail)();
    unsigned int  (FSAPI *joystick_count)();
    void  (FSAPI *SetSlewInputsActive)(int);
    void  (FSAPI *joysticks_capture)(void*);

    char  _dummy1[0x46A0];
    
    stJoyInfoPtrArray JoyInfoPtrArray;
};

#pragma pack()

#endif
