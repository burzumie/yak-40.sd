/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: fs_sdk_common.h,v $

  Last modification:
    Date:      $Date: 2005/08/11 13:30:42 $
    Version:   $Revision: 1.4 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_COMMON_INCLUDE
#define _FS_SDK_COMMON_INCLUDE

#include <windows.h>
#include "common\def_types.h"
#include "common\module_id.h"
#include "common\fs_linkage.h"
#include "common\GeoCoordStructs.h"
#include "common\2dstructs.h"
#include "common\image.h"
#include "common\awind.h"
#include "common\helpid.h"
#include "common\tooltips.h"
#include "common\keyid.h"
#include "common\xmltokens.h"
#include "common\ctokens.h"
#include "common\container.h"

#pragma pack(1)

typedef struct _CABLE_INFO {
	int cCable;
	LATLONALT allaCable[8];
}CABLE_INFO,*PCABLE_INFO,**PPCABLE_INFO;

#pragma pack(1)

typedef struct _SURFACE_INFO {
	double dElevation;
	SURFACE_TYPE eSurfaceType;
	SURFACE_CONDITION eSurfaceCondition;
	_FLOAT64_VECTOR3 vecNormal;
	XYZF64 vecVelocity;
	XYZF64 rotVelocity;
	int bOnPlatform;
	_CABLE_INFO cables;
}SURFACE_INFO,*PSURFACE_INFO,**PPSURFACE_INFO;

#define CREATE_CLASS(n,c)	{if(n==NULL)n=new c;}
#define DELETE_CLASS(n)		{if(n!=NULL){delete n;n=NULL;}}

#endif
