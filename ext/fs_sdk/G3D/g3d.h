/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: g2d.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_G3D
#define _FS_SDK_G3D

#pragma pack(4)
	  
__inline SIF48 FLOAT64_TO_SIF48 (FLOAT64 number)
{
	SIF48 r;
	r.i64 = (SINT64)(number * 4294967296.0);
	return r;
}
__inline ANGL48 FLOAT64_TO_ANGL48 (const FLOAT64 angle)
{
	ANGL48 r;
	r.i64 = (SINT64)(angle * 4294967296.0);
	return r;
}
__inline ANGL48 DEGREES_TO_ANGL48 (const FLOAT64 angle)
{
	return FLOAT64_TO_ANGL48(angle * ((65536.0 * 65536.0) / 360.0));
}
__inline SIF48 LAT_RADIANS_TO_METERS48(const FLOAT64 radians)
{
	return FLOAT64_TO_SIF48(radians / ((1.0 / (40007000.0/(3.14159265358*2)))));
}
struct G3D
{
	FSLINKAGE head;
	DWORD some_funcs[0x0D];
	DWORD (__stdcall *g3d_get_surface)(LATLONALT *,SURFACE_INFO *,DWORD Unknown); 
};

#pragma pack()
              
#endif        