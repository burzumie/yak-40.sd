/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: sound.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_FE
#define _FS_SDK_FE

#pragma pack(4)

struct FE {
    FSLINKAGE head;
    void  (FSAPI *Linkage01)();
    void  (FSAPI *Linkage02)();
    void  (FSAPI *Linkage03)();
    void  (FSAPI *Linkage04)();
    void  (FSAPI *Linkage05)();
    void  (FSAPI *Linkage06)();
    void  (FSAPI *Linkage07)();
    void  (FSAPI *Linkage08)();
    void  (FSAPI *Linkage09)();
    void  (FSAPI *Linkage10)();
    void  (FSAPI *Linkage11)();
    void  (FSAPI *Linkage12)();
    void  (FSAPI *Linkage13)();
    void  (FSAPI *Linkage14)();
    void  (FSAPI *Linkage15)();
    void  (FSAPI *Linkage16)();
    void  (FSAPI *Linkage17)();
    void  (FSAPI *Linkage18)();
    void  (FSAPI *Linkage19)();
    void  (FSAPI *Linkage20)();
    void  (FSAPI *Linkage21)();
    void  (FSAPI *Linkage22)();
    void  (FSAPI *Linkage23)();
    void  (FSAPI *Linkage24)();
    void  (FSAPI *Linkage25)();
    void  (FSAPI *Linkage26)();
    void  (FSAPI *Linkage27)();
    void  (FSAPI *Linkage28)();
    void  (FSAPI *Linkage29)();
    void  (FSAPI *Linkage30)();
    void  (FSAPI *Linkage31)();
    void  (FSAPI *Linkage32)();
    void  (FSAPI *Linkage33)();
    void  (FSAPI *Linkage34)();
    void  (FSAPI *Linkage35)();
    void  (FSAPI *Linkage36)();
    void  (FSAPI *Linkage37)();
    void  (FSAPI *Linkage38)();
    void  (FSAPI *Linkage39)();
};

#pragma pack()

#endif
