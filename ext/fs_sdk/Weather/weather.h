/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: weather.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_WEATHER
#define _FS_SDK_WEATHER

#pragma pack(4)

enum _turb_ {
  TURBULENCE_NONE = 0x0,
  TURBULENCE_LIGHT = 0x1,
  TURBULENCE_MODERATE = 0x2,
  TURBULENCE_HEAVY = 0x3,
  TURBULENCE_SEVERE = 0x4,
  TURBULENCE_MAX = 0x5,
};

enum _shear_ {
  WIND_SHEAR_GRADUAL = 0x0,
  WIND_SHEAR_MODERATE = 0x1,
  WIND_SHEAR_STEEP = 0x2,
  WIND_SHEAR_INSTANTANEOUS = 0x3,
  WIND_SHEAR_MAX = 0x4,
};

enum _cloud_type_enum {
  CLOUD_TYPE_NONE = 0x0,
  CLOUD_TYPE_CIRRUS = 0x1,
  CLOUD_TYPE_STRATUS = 0x8,
  CLOUD_TYPE_CUMULUS = 0x9,
  CLOUD_TYPE_CUMULONIMBUS = 0xa,
  CLOUD_TYPE_THUNDERSTORM = 0xa,
  CLOUD_TYPE_USER_DEF = 0x9,
  CLOUD_TYPE_MAX = 0xa,
};

enum _cloud_cover_enum {
  CLOUD_COVER_CLEAR = 0x0,
  CLOUD_COVER_FEW__1_8 = 0x1,
  CLOUD_COVER_FEW__2_8 = 0x2,
  CLOUD_COVER_SCATTERED__3_8 = 0x3,
  CLOUD_COVER_SCATTERED__4_8 = 0x4,
  CLOUD_COVER_BROKEN__5_8 = 0x5,
  CLOUD_COVER_BROKEN__6_8 = 0x6,
  CLOUD_COVER_BROKEN__7_8 = 0x7,
  CLOUD_COVER_OVERCAST__8_8 = 0x8,
  CLD_COVER_MAX = 0x9,
};

enum _cloud_top_enum {
  CLOUD_TOP_FLAT = 0x0,
  CLOUD_TOP_ROUND = 0x1,
  CLOUD_TOP_ANVIL = 0x2,
  CLOUD_TOP_MAX = 0x3,
};

enum _precip_type_enum {
  PRECIP_TYPE_NONE = 0x0,
  PRECIP_TYPE_RAIN = 0x1,
  PRECIP_TYPE_SNOW = 0x2,
  PRECIP_TYPE_MAX = 0x3,
};

enum _precip_rate_enum {
  PRECIP_RATE_VLOW = 0x0,
  PRECIP_RATE_LOW = 0x1,
  PRECIP_RATE_MODERATE = 0x2,
  PRECIP_RATE_HIGH = 0x3,
  PRECIP_RATE_VHIGH = 0x4,
  PRECIP_RATE_MAX = 0x5,
};

typedef struct _timestamp {
    unsigned short hours;
    unsigned short minutes;
}TIMESTAMP,*PTIMESTAMP,**PPTIMESTAMP;

typedef struct _obsv_status {
    struct _timestamp time;
    char ident[4];
    int flags;
}OBSV_STATUS,*POBSV_STATUS,**PPOBSV_STATUS;

typedef struct _wind_surface {
    float depth;
    float speed;
    float gusts;
    float direction;
    float variance;
    enum _turb_ turbulence;
    enum _shear_ shear;
    float fHeightMSL;
}WIND_SURFACE,*PWIND_SURFACE,**PPWIND_SURFACE;

typedef struct _wind_aloft_layer {
    struct _wind_aloft_layer* next_layer;
    float alt;
    float speed;
    float gusts;
    float direction;
    float variance;
    enum _turb_ turbulence;
    enum _shear_ shear;
}WIND_ALOFT_LAYER,*PWIND_ALOFT_LAYER,**PPWIND_ALOFT_LAYER;

typedef struct _visibility {
    struct _visibility* next_layer;
    float visibility;
    float base;
    float tops;
}VISIBILITY,*PVISIBILITY,**PPVISIBILITY;

typedef struct _cloud_layer {
    struct _cloud_layer* next_layer;
    enum _cloud_type_enum cloud_type;
    float cloud_base;
    float cloud_tops;
    float cloud_deviation;
    enum _cloud_cover_enum cloud_coverage;
    enum _cloud_top_enum cloud_top;
    enum _turb_ cloud_turbulence;
    enum _precip_type_enum precip_type;
    float precip_base;
    enum _precip_rate_enum precip_rate;
    enum _ice_ icing_rate;
}CLOUD_LAYER,*PCLOUD_LAYER,**PPCLOUD_LAYER;

typedef struct _temp_layer {
    struct _temp_layer* next_layer;
    float alt;
    float temp;
    float range;
    float dew_point;
}TEMP_LAYER,*PTEMP_LAYER,**PPTEMP_LAYER;

typedef struct _pressure {
    float pressure_at_alt;
    float pressure_at_SL;
    float range;
}PRESSURE,*PPRESSURE,**PPPRESSURE;

typedef struct _obsv {
    struct _obsv_status status;
    struct _wind_surface wind_surface;
    struct _wind_aloft_layer* wind_aloft;
    struct _visibility* visibility;
    struct _cloud_layer* cloud;
    struct _temp_layer* temperature;
    struct _pressure pressure;
}OBSV,*POBSV,**PPOBSV;

typedef struct _wthset_ {
    enum _cloud_cover_enum coverage;
    double meterBase;
    enum _precip_rate_enum precip;
    double meterVisibility;
    double knotsWindSpeed;
    double degreeWindDir;
    double degcTemperature;
    double mbPressure;
}WEATHER_SETTINGS,*PWEATHER_SETTINGS,**PPWEATHER_SETTINGS;

typedef struct _icao_lla {
    char ident[4];
    struct _latlonalt_float32 pos;
    float lat;
    float lon;
    float alt;
}ICAO_LLA,*PICAO_LLA,**PPICAO_LLA;

struct WEATHER {
	FSLINKAGE		head;
    unsigned int	(FSAPI *Weather_reset)();
    struct _obsv*	(FSAPI *Weather_conditions_at_lla_fixed_point)(struct LATLONALT*, int);
    unsigned int	(FSAPI *Weather_GetPrecipConditionsAtLla)(struct LATLONALT*, int*, int*);
    unsigned int	(FSAPI *Weather_GetWthdataAtLla)(struct LATLONALT*, int, struct _wthdata_*);
    unsigned int	(FSAPI *Weather_init_wthset)(struct _wthset_*);
    unsigned int	(FSAPI *Weather_set_weather)(char*, struct _wthset_*);
    int				(FSAPI *Weather_DownloadLatestWeather)(int);
    unsigned int	(FSAPI *Weather_send_obsv_to_player)(unsigned int, struct _obsv*);
    void			(FSAPI *Weather_standard_atm_lookup)(double*, double*, double);
    double			(FSAPI *Weather_standard_atm_get_altitude)(double);
    void			(FSAPI *Weather_SetInCloud)(int);
    void			(FSAPI *Weather_SetCloudDrawDistance)(unsigned int);
    unsigned int	(FSAPI *Weather_GetCloudDrawDistance)();
    void			(FSAPI *Weather_Set3DCloudPercent)(unsigned int);
    unsigned int	(FSAPI *Weather_Get3DCloudPercent)();
    void			(FSAPI *Weather_SetCloudCoverageDensity)(unsigned int);
    unsigned int	(FSAPI *Weather_GetCloudCoverageDensity)();
    void			(FSAPI *Weather_SetDetailedClouds)(int);
    int				(FSAPI *Weather_GetDetailedClouds)();
    void			(FSAPI *Weather_ReloadClouds)();
    float			(FSAPI *GetGlobalCloudCoverage)(struct LATLONALT*);
    unsigned int	(FSAPI *Weather_DrawFlatCloudsAndRadsortNonAlpha)();
    unsigned int	(FSAPI *Weather_RadsortAlpha)();
    unsigned int	(FSAPI *Weather_LoadTheme)(struct FILEDATA*);
    void			(FSAPI *Weather_GetThemeName)(char**);
    void			(FSAPI *Weather_DoPreChange)(unsigned int*);
    void			(FSAPI *Weather_DoPostChange)(unsigned int*);
    void			(FSAPI *Weather_Load)();
    void			(FSAPI *Weather_ProcessTheme)();
    struct _obsv*	(FSAPI *obsv_alloc)(struct _obsv**);
    struct _obsv*	(FSAPI *obsv_alloc_standard)(struct _obsv**);
    struct _obsv*	(FSAPI *obsv_copy)(struct _obsv*);
    void			(FSAPI *obsv_free)(struct _obsv*);
    struct _cloud_layer*		(FSAPI *obsv_cloud_alloc)(struct _cloud_layer**);
    struct _cloud_layer*		(FSAPI *obsv_cloud_add)(struct _obsv*, struct _cloud_layer*);
    struct _cloud_layer*		(FSAPI *obsv_cloud_free)(struct _obsv*, struct _cloud_layer*, int);
    struct _temp_layer*			(FSAPI *obsv_temperature_alloc)(struct _temp_layer**);
    struct _temp_layer*			(FSAPI *obsv_temperature_add)(struct _obsv*, struct _temp_layer*);
    struct _temp_layer*			(FSAPI *obsv_temperature_free)(struct _obsv*, struct _temp_layer*);
    struct _visibility*			(FSAPI *obsv_visibility_alloc)(struct _visibility**);
    struct _visibility*			(FSAPI *obsv_visibility_add)(struct _obsv*, struct _visibility*);
    struct _visibility*			(FSAPI *obsv_visibility_free)(struct _obsv*, struct _visibility*);
    struct _wind_aloft_layer*	(FSAPI *obsv_wind_aloft_alloc)(struct _wind_aloft_layer**);
    struct _wind_aloft_layer*	(FSAPI *obsv_wind_aloft_add)(struct _obsv*, struct _wind_aloft_layer*);
    struct _wind_aloft_layer*	(FSAPI *obsv_wind_aloft_free)(struct _obsv*, struct _wind_aloft_layer*);
    unsigned int	(FSAPI *GetNearestStation)(float, float, struct _icao_lla*);
    int				(FSAPI *GetNumStationsGridSq)(int, int);
    unsigned int	(FSAPI *GetVecIndexForGridSq)(int, int, int, struct _icao_lla*, struct _obsv**);
    int				(FSAPI *GetNumFronts)();
    void*			(FSAPI *GetFrontByIndex)(int);
    int				(FSAPI *GetNumPressurePts)();
    unsigned int	(FSAPI *GetPressurePt)(int, void**, float*, float*);
    char*			(FSAPI *GetStationName)(char*);
    void			(FSAPI *ResetStations)();
    int				(FSAPI *DoesStationExist)(char*);
    struct _obsv*	(FSAPI *FindStationObsv)(char*);
    unsigned int	(FSAPI *AddOrUpdateObsv)(struct _obsv*);
    struct _obsv*	(FSAPI *GetGlobalObsv)();
    int				(FSAPI *IsGraphDataInDialog)();
    void			(FSAPI *ObsvMetricToEnglish)(struct _obsv*);
    void			(FSAPI *ObsvEnglishToMetric)(struct _obsv*);
    void			(FSAPI *Weather_DisableTurbulence)(int);
    void			(FSAPI *SetDynamicWeather)(int);
    int				(FSAPI *GetDynamicWeather)(int);
    void			(FSAPI *SetWeatherType)(int);
    int				(FSAPI *GetWeatherType)();
};

#pragma pack()

#endif
