/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: Util_classes.h,v $

  Last modification:
    Date:      $Date: 2005/08/11 16:25:15 $
    Version:   $Revision: 1.9 $
    Author:    $Author: andrew $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/


#ifndef __FS9SDK_UTIL
#define __FS9SDK_UTIL
#include <common/GeoCoordStructs.h>
/* class ImportedClass
Helper class. Indicates that members/vtable are imported from a module via GetProcAddress

*/


void Util_init();

/*
class _array. A strange indexed storage with "current" element.
���������� � ���, ��� ������ �������� ��������� �� ��������.
���� ��������� �� ���������� � �� �������������. ������� ��� 
����������� TSmatrPtr<>
*/
class _array
{
public:
	void** m_ptr;//+00
	DWORD m_current;//+4
	DWORD m_size;//+8
	DWORD m_capacity;//0C

	DWORD _indexOf(void);//returns Index of current element - this->ciurrent
	DWORD size(void);// returns useful size - this->size
	void* _curr(void);// get current element's pointer
	void* _next(void);// shift to next & get it
	void* _prev(void);// shift to prev & get it
	void* _first(void);// get first el-t. Sets current to 0
	void* _last(void);//get last el-t. Sets current to size-1
	void* _append(void *);// jmp insert()
	void* _elem(unsigned int index);//index operator. Doesn't set current
	_array& operator=(_array const & src);// copies fields only!
	void* setAt(unsigned int index,void *pItem);//sets at given index. Reallocs if out_of_range
	DWORD _indexOf(void *pTarget);// returns index of target. Doesn't set current
	void* _find(void* pTarget);// sets target as current if found. If not found- last is the result
	void* _remove(void);// removes current and returns pointer to it.
	void *_insert(void* pNewItem);// inserts pNewItem before(?) current. Result is the pNewItem
	void empty(void);// zeroes size & current. Zeroes 0th data
	_array(int size);// like stl's reserve. Allocates buffer for size pointers
	_array();//like stl's reserve. Allocates buffer for 10 pointers
	~_array();// frees memory
};

/*
class _list 
{
public:
	_first(void);
	_curr(void);
	_next(void);
	_indexOf(void);
	operator=(_list const &);
	swap(void);
	_prev(void);
	_last(void);
	_find(void *); //link*
	_indexOf(void *);
	_elem(unsigned int);
	_remove(void);
	_append(void *);//link
	_insert(void* );	//link * 
	empty(void);
	size(void);
	_list();
};

class _ArrList
{
public:
	_next(ArrListNode *);
	_prev(ArrListNode *);
	operator=(_ArrList const &);
	_freeall(void);
	_free(ArrListNode *);
	AllocNodes(int);
	_freetail(ArrListNode *);
	_newtail(void);
	_newhead(void);
	_ArrList(unsigned int,int,int);
	~_ArrList();
};

class ListElement
{
public:
	ListElement();
	ListElement(ListElement const &);
	~ListElement();
	SetPrev(ListElement *);
	SetNext(ListElement *);
	operator=(ListElement const &);
	//const `vftable';
};

class List
{
public:
	UnhookFromList(ListElement *);
	DeleteByPointer(ListElement *);
	Add(ListElement *);
	Insert(ListElement *);
	List(void);
	GetByPos(unsigned int);
	Free(void);
	InsertAfter(ListElement *,ListElement *);
	GetNext(ListElement const *);
	GetNext(ListElement *);
	GetPrev(ListElement const *);
	GetPrev(ListElement *);
	~List(void);
	InsertBefore(ListElement *,ListElement *);
};

class CLineHelper
{
public:
	List *_s_pLinesList;
	SetLLAPBH(LATLONALTPBH const *);
	SetLLAPBH(LATLONALT const *,PBH32 const *);
	SetLLA(LATLONALT *);
	SetPBH(PBH32 *);
	SetHeading(unsigned int);
	SetDegreeHeading(double);
	operator=(CLineHelper const &);
	SetColor(unsigned int);
	AddSegments(CLineHelper::LineSegment *,int);
	Draw(void);
	Enable(int);
	PostRadsortHandler(unsigned int,void *);
	ChainMessageHandler(unsigned int,void *);
	CLineHelper(void);
	CLineHelper(CLineHelper const &);
	~CLineHelper(void);
	//const `vftable';
};	*/


/*
3� ������ ������ � �������� ��� ���
*/
class Vector3D
{
public:
	double x;
	double y;
	double z;
	Vector3D(LATLONALT const & point1 ,LATLONALT const & point2);
	Vector3D(LATLONALT const & point);
	Vector3D operator-(void);// inverts all coords
	Vector3D& operator+=(Vector3D const & p1);
	Vector3D& operator-=(Vector3D const & p1);
	Vector3D& operator*=(double);//
	Vector3D& operator/=(double);
	double norm(void);// �����
	double dot(Vector3D const &);//��������� ������������
	Vector3D cross(Vector3D const & to_cross);
	double length(void);
	double length_sqr(void);
};
  
class XYZ;

class LLA
{
public:
	double Lat;
	double Lon;
	double Alt;
	LLA(LLA& src);
	LLA(LATLONALT& src);
	void Zero(void);//zero the class	
	void GetAltitude(void);
	void SetAltitude(double);
	void ToMeters(LLA const &,XYZ &);
	char* LatToStr(char *);
	char* LonToStr(char *);
	char* AltToStr(char *);
	void StrToLat(char const *);
	void StrToLon(char const *);
	void StrToAlt(char const *);
	LLA& operator=(int);// ZEROes the class
	operator LATLONALT const(void);// UNCHECKED STACK
	LLA& operator+=(Vector3D const &);
	LATLONALT& ToLATLONALT(void);
};

class XYZ
{
public:
	double X,Y,Z;
	XYZF64& ToXYZF64(void);
	XYZ(double,double,double);
	void Init(double,double,double);
	void Zero(void);
	void ToLLA(LLA const &,LLA &);
	XYZ& operator=(int);
	XYZ& operator=(XYZF64 const &);
	XYZ& operator=(XYZ const &);
	XYZ(LATLONALT const &);
	XYZ(LATLONALT const &,LATLONALT const &);
};
/*
class CPlatform	 :public ImportedClass
{
public:
	NotifyVelocity(XYZF64 const *,XYZF64 const *);
	SetSurfaceType(SURFACE_TYPE);
	SetSurfaceCondition(SURFACE_CONDITION);
	operator=(CPlatform const &);
	RemoveFromFrameList(void);
	UnregisterFromSimulation(void);
	PointOnPlaneInPlatformPolygon(XYZF64 const *);
	ProjectToA2DPrimaryPlane(void);
	RotateToBodyAxis(XYZF64 *);
	RotateToWorldAxis(XYZF64 *);
	SetLLAPBH(LATLONALT const *,PBH32 const *);
	SetLLAPBH(LATLONALTPBH const *);
	ReleaseResources(void);
	CalcPlatformConstants(void);
	RecalcRotationMatrix(void);
	InitPlatform(LATLONALTPBH const *,XYZF32 const *,int,SURFACE_TYPE,CABLE_INFO_M const *,MATRIXF32 const * const);
	GetCableInfo(LATLONALT const *,_CABLE_INFO *);
	GetPlatformInfo(LATLONALT const *,_SURFACE_INFO *,uint);
	staticPlatformCallback(LATLONALT const *,_SURFACE_INFO *,unsigned int,void *);
	AddToFrameList(void);
	RegisterWithSimulation(void);
	CPlatform(void);
	~CPlatform(void);
};
*/
class Matrix3D
{
public:
	double matrix[9];// 3x3 matrix of doubles
	Matrix3D();
	Matrix3D& operator=(Matrix3D const &src);// copy 12h dwords from src to this
	Matrix3D(Matrix3D const &src);// copy 12h dwords from src to this
	void normYZX(void);// does nothing
	void normZYX(void);// does nothing
	void initWorld2Body(double,double,double);
	void initBody2World(double,double,double);
};

class Segments
{
public:
	int __stdcall FindIntersectionPoint(LLA const &,LLA const &,LLA const &,LLA const &,LLA &);
	LLA __stdcall ExtendSegment(LLA const &,LLA const &);
	int __stdcall VectorsIntersect(LLA const &,LLA const &,LLA const &,LLA const &,LLA &);
	void __stdcall SegmentsIntersect(LLA const &,LLA const &,LLA const &,LLA const &);
};

class QuadMeshCoord
{
public:
	double U;
	double V;
static	double MAX_LATTITUDE_DELTA;
static	double MAX_LONGITUDE_DELTA;
	QuadMeshCoord(double const &U,double const &V);
	QuadMeshCoord();
	double GetU(void);
	double GetV(void);
	QuadMeshCoord& SetU(double U);
	QuadMeshCoord& SetV(double V);
	QuadMeshCoord& SetLatLonRad(double const &Lat ,double const &Lon);
	QuadMeshCoord& SetLatLonDeg(double const &Lat,double const &Lon);
	QuadMeshCoord& operator+=(QuadMeshCoord const &);
	QuadMeshCoord& operator-=(QuadMeshCoord const &);
	QuadMeshCoord& operator*=(double const &);
	QuadMeshCoord operator+(QuadMeshCoord const &);
	QuadMeshCoord operator-(QuadMeshCoord const &);
	QuadMeshCoord operator*(double const &);
	bool operator==(QuadMeshCoord const &);
	bool operator!=(QuadMeshCoord const &);
	void GetLatLonDeg(double &Lat,double &Lon);
	void GetLatLonRad(double &Lat,double &Lon);
	double GetLonDeg(void);
	double GetLatDeg(void);
	QuadMeshCoord FromLatLonDeg(double const &Lat,double const &Lon);
	QuadMeshCoord FromLatLonRad(double const &Lat,double const &Lon);
};

class QuadMeshIntegerCoord
{
public:
	int U;
	int V;
	static QuadMeshIntegerCoord FromLatLonAlt48( LATLONALT const &);
	QuadMeshIntegerCoord& SetLatLonDeg(double const &,double const &);
	QuadMeshIntegerCoord& operator+=(QuadMeshIntegerCoord const &);
	QuadMeshIntegerCoord& operator-=(QuadMeshIntegerCoord const &);
	QuadMeshIntegerCoord& operator*=(int const &);
	QuadMeshIntegerCoord& operator/=(int const &);
	QuadMeshIntegerCoord& operator>>=(int const &);
	QuadMeshIntegerCoord& operator<<=(int const &);
	QuadMeshIntegerCoord operator+(QuadMeshIntegerCoord const &);
	QuadMeshIntegerCoord operator-(QuadMeshIntegerCoord const &);
	QuadMeshIntegerCoord operator*(int const &);
	QuadMeshIntegerCoord operator/(int const &);
	QuadMeshIntegerCoord operator>>(int const &);
	QuadMeshIntegerCoord operator<<(int const &);
	bool operator==(QuadMeshIntegerCoord const &);
	bool operator!=(QuadMeshIntegerCoord const &);
	void GetLatLonDeg(double &,double &);
	void GetLatLonRad(double &,double &);
	void NormalizeToLocation(QuadMeshIntegerCoord const &);
	void SetMidPoint(QuadMeshIntegerCoord const &,QuadMeshIntegerCoord const &);
	LATLONALT AsLatLonAlt48(void);
//	LATLON AsLatLon48(void);//Strange error here
	int GetU(void);
	int GetV(void);
	void SetU(int);
	void SetV(int);
	QuadMeshIntegerCoord Normalized(void);
	double GetLonDeg(void);
	double GetLatDeg(void);
static QuadMeshIntegerCoord FromLatLonDeg(double const &,double const &);
};

class QuadMeshID
{
public:
		unsigned int U,V,LOD;
static	double *m_dUVtoQMCScale;
static	unsigned long * m_dwLimitU;
static	double const * m_dOOLimitU;
static	unsigned long * m_dwLimitV;
static	double * m_dOOLimitV;
	int GetAdjU(void)const{return U;};
	int GetAdjV(void)const{return V;};
	int GetOppositeNeighborIndex(unsigned int);
	QuadMeshID& operator=(QuadMeshID const &);
	QuadMeshID(void);
	QuadMeshID(QuadMeshCoord const &coord,unsigned int LOD);
	QuadMeshID(QuadMeshIntegerCoord const &coord,unsigned int LOD);
	QuadMeshID(unsigned int U,unsigned int V,unsigned int LOD);
	void Init(unsigned int U,unsigned int V,unsigned int LOD);
	void Invalidate(void);// ����� � ����....
	void MakeWholeWorldID(void);// ��������. LOD=0 ��� ������������ ����
	int IsValid(void);// �������� �� -1
	int IsWholeWorld(void);// return LOD==0
	QuadMeshID& AdjustLevel(unsigned int LOD);// sets LOD & shifts U;V
	int IsLatEqual(QuadMeshID const &);
	int IsLonEqual(QuadMeshID const &);
	int operator==(QuadMeshID const &);
	int operator!=(QuadMeshID const &);
	int operator<(QuadMeshID const &);
	int operator<=(QuadMeshID const &);
	int operator>(QuadMeshID const &);
	int operator>=(QuadMeshID const &);
	int GetBranch(unsigned int);
	QuadMeshIntegerCoord GetNWQmicCoord();
	QuadMeshIntegerCoord GetNEQmicCoord();
	QuadMeshIntegerCoord GetSWQmicCoord();
	QuadMeshIntegerCoord GetSEQmicCoord();
	QuadMeshIntegerCoord GetCenterQmicCoord();
	QuadMeshCoord GetUpperLeftCoord();
	QuadMeshCoord GetUpperRightCoord();
	QuadMeshCoord GetSWCoord();
	QuadMeshCoord GetCenterCoord();
	QuadMeshCoord GetSECoord();
	double GetLatitudeN(void);
	double GetLatitudeS(void);
	double GetLongitudeW(void);
	double GetLongitudeE(void);
	QuadMeshID GetNNeighbor();
	QuadMeshID GetSNeighbor();
	QuadMeshID GetENeighbor();
	QuadMeshID GetWNeighbor();
	QuadMeshID GetNWNeighbor();
	QuadMeshID GetNENeighbor();
	QuadMeshID GetSWNeighbor();
	QuadMeshID GetSENeighbor();
	QuadMeshID GetUpperLeftChild();
	QuadMeshID GetUpperRightChild();
	QuadMeshID GetSWChild();
	QuadMeshID GetSEChild();
	QuadMeshID GetParent();
	int GetPolarityOfBlock(void);
	int IsParentOf(QuadMeshID const &);
	int IsChildOf(QuadMeshID const &);
	int DoesIntersect(QuadMeshID const &);
	double GetLatitudeExtentInMeters(unsigned int LOD);
	double GetLimitU(unsigned int LOD);
	double GetLimitV(unsigned int LOD);
	double GetOOLimitU(unsigned int LOD);
	double GetOOLimitV(unsigned int LOD);
	static int GetOppositeNeighborIndex(int);
	int BuildQMIDArray(QuadMeshID const &,QuadMeshID const &,QuadMeshID * * const,unsigned int * const);
	QuadMeshID GetChild(int);
	QuadMeshID GetNeighbor( int);
	void ConvertToLatLonRect32(LATLONRECT32 &);
	void GetExtents(float *,float *,double);
};

/*
void multNT3d(Matrix3D const &,Matrix3D const &,Matrix3D &);
void mult3d(Matrix3D const &,Matrix3D const &,Matrix3D &);
void mult3d(Matrix3D const &,Vector3D const &,Vector3D &);
void operator==(LLA const &,LLA const &);
void Angl_2PI(double);
void Angl_PI(double);
void Angl_180(double);
void Angl_360(double);
void multTN3d(Matrix3D const &,Matrix3D const &,Matrix3D &);
void multT3d(Matrix3D const &,Vector3D const &,Vector3D &);
void LLA2Meters(LATLONALT const &,LATLONALT const &,XYZF64 &);
void Meters2LLA(XYZF64 const &,LATLONALT const &,LATLONALT &);
void getOrtV3d(Vector3D const &,Vector3D &);
void getOrtV3d(Vector3D const &,Vector3D const &,Vector3D &);
void GetDist(LATLONALT const &,LATLONALT const &);
*/

#endif
