/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
	Module:  $RCSfile: Util_classes.cpp,v $
	
	  Last modification:
	  Date:      $Date: 2005/08/11 16:25:15 $
	  Version:   $Revision: 1.9 $
	  Author:    $Author: andrew $
	  
		Copyright (c) 2005 by Nick Sharmanzhinov [except]
		Copyright (c) 2005 by Andrew Egorovsky
		
		  All rights reserved.
		  
=========================================================================*/

#include <windows.h>
#include "Util_classes.h"

static char *UtilExptsNames[]={
	"?_indexOf@_array@@QBEHXZ",
		"?size@_array@@QBEHXZ",
		"?_next@_ArrList@@IAEPAVArrListNode@@PAV2@@Z",
		"?_prev@_ArrList@@IAEPAVArrListNode@@PAV2@@Z",
		"??4_ArrList@@QAEAAV0@ABV0@@Z",
		"??0ListElement@@QAE@XZ",
		"??1ListElement@@UAE@XZ",
		"?SetPrev@ListElement@@QAEXPAV1@@Z",
		"?SetNext@ListElement@@QAEXPAV1@@Z",
		"??0ListElement@@QAE@ABV0@@Z",
		"??4ListElement@@QAEAAV0@ABV0@@Z",
		"?_curr@_array@@QBEPAXXZ",
		"?_next@_array@@QBEPAXXZ",
		"?_prev@_array@@QBEPAXXZ",
		"?_first@_array@@QBEPAXXZ",
		"?_last@_array@@QBEPAXXZ",
		"?_append@_array@@QAEPAXPAX@Z",
		"?_elem@_array@@QBEPAXI@Z",
		"??4_array@@QAEAAV0@ABV0@@Z",
		"??_F_array@@QAEXXZ",
		"?_first@_list@@IBEPAUlink@@XZ",
		"?_curr@_list@@IBEPAUlink@@XZ",
		"?_next@_list@@IBEPAUlink@@XZ",
		"??0_list@@QAE@XZ",
		"?_indexOf@_list@@QBEHXZ",
		"?SetLLAPBH@CLineHelper@@QAEXPBTLATLONALTPBH@@@Z",
		"?SetLLAPBH@CLineHelper@@QAEXPBULATLONALT@@PBUPBH32@@@Z",
		"?SetLLA@CLineHelper@@QAEXPAULATLONALT@@@Z",
		"?SetPBH@CLineHelper@@QAEXPAUPBH32@@@Z",
		"?SetHeading@CLineHelper@@QAEXI@Z",
		"?SetDegreeHeading@CLineHelper@@QAEXN@Z",
		"??0CLineHelper@@QAE@ABV0@@Z",
		"??4CLineHelper@@QAEAAV0@ABV0@@Z",
		"?NotifyVelocity@CPlatform@@QAEXPBUXYZF64@@0@Z",
		"?SetSurfaceType@CPlatform@@QAEXW4SURFACE_TYPE@@@Z",
		"?SetSurfaceCondition@CPlatform@@QAEXW4SURFACE_CONDITION@@@Z",
		"??4CPlatform@@QAEAAV0@ABV0@@Z",
		"?ToXYZF64@XYZ@@QAEAAUXYZF64@@XZ",
		"??0XYZ@@QAE@NNN@Z",
		"?Init@XYZ@@QAEXNNN@Z",
		"?Zero@XYZ@@QAEXXZ",
		"?ToLLA@XYZ@@QBEXABVLLA@@AAV2@@Z",
		"??4XYZ@@QAEAAV0@H@Z",
		"??4XYZ@@QAEAAV0@ABUXYZF64@@@Z",
		"??0Vector3D@@QAE@ABULATLONALT@@0@Z",
		"??0Vector3D@@QAE@ABULATLONALT@@@Z",
		"??GVector3D@@QBE?BV0@XZ",
		"??YVector3D@@QAEAAV0@ABV0@@Z",
		"??ZVector3D@@QAEAAV0@ABV0@@Z",
		"??XVector3D@@QAEAAV0@N@Z",
		"??_0Vector3D@@QAEAAV0@N@Z",
		"?norm@Vector3D@@QAENXZ",
		"?dot@Vector3D@@QAENABV1@@Z",
		"?cross@Vector3D@@QAE?AV1@ABV1@@Z",
		"?length@Vector3D@@QBENXZ",
		"?length_sqr@Vector3D@@QBENXZ",
		"?Zero@LLA@@QAEXXZ",
		"?GetAltitude@LLA@@QBENXZ",
		"?SetAltitude@LLA@@QAEXN@Z",
		"?ToMeters@LLA@@QBEXABV1@AAVXYZ@@@Z",
		"?LatToStr@LLA@@QBEPADPAD@Z",
		"?LonToStr@LLA@@QBEPADPAD@Z",
		"?AltToStr@LLA@@QBEPADPAD@Z",
		"?StrToLat@LLA@@QAE?AW4ASCII_PARSE_ERROR_TYPE@@PBD@Z",
		"?StrToLon@LLA@@QAE?AW4ASCII_PARSE_ERROR_TYPE@@PBD@Z",
		"?StrToAlt@LLA@@QAE?AW4ASCII_PARSE_ERROR_TYPE@@PBD@Z",
		"??4LLA@@QAEAAV0@H@Z",
		"??BLLA@@QBE?BULATLONALT@@XZ",
		"??YLLA@@QAEAAV0@ABVVector3D@@@Z",
		"??4XYZ@@QAEAAV0@ABV0@@Z",
		"??4Matrix3D@@QAEAAV0@ABV0@@Z",
		"?normZYX@Matrix3D@@QAEXXZ",
		"??0QuadMeshCoord@@QAE@ABN0@Z",
		"?GetU@QuadMeshCoord@@QBENXZ",
		"?GetV@QuadMeshCoord@@QBENXZ",
		"?SetU@QuadMeshCoord@@QAEAAV1@N@Z",
		"?SetV@QuadMeshCoord@@QAEAAV1@N@Z",
		"??4_list@@QAEAAV0@ABV0@@Z",
		"?SetLatLonRad@QuadMeshCoord@@QAEAAV1@ABN0@Z",
		"?SetLatLonDeg@QuadMeshCoord@@QAEAAV1@ABN0@Z",
		"??YQuadMeshCoord@@QAEAAV0@ABV0@@Z",
		"??ZQuadMeshCoord@@QAEAAV0@ABV0@@Z",
		"??XQuadMeshCoord@@QAEAAV0@ABN@Z",
		"??HQuadMeshCoord@@QBE?AV0@ABV0@@Z",
		"??GQuadMeshCoord@@QBE?AV0@ABV0@@Z",
		"??DQuadMeshCoord@@QBE?AV0@ABN@Z",
		"??8QuadMeshCoord@@QBE_NABV0@@Z",
		"??9QuadMeshCoord@@QBE_NABV0@@Z",
		"?GetLatLonDeg@QuadMeshCoord@@QBEXAAN0@Z",
		"?FromLatLonAlt48@QuadMeshIntegerCoord@@SG?AV1@ABULATLONALT@@@Z",
		"?SetLatLonDeg@QuadMeshIntegerCoord@@QAEAAV1@ABN0@Z",
		"??YQuadMeshIntegerCoord@@QAEAAV0@ABV0@@Z",
		"??ZQuadMeshIntegerCoord@@QAEAAV0@ABV0@@Z",
		"??XQuadMeshIntegerCoord@@QAEAAV0@ABH@Z",
		"??_0QuadMeshIntegerCoord@@QAEAAV0@ABH@Z",
		"??_2QuadMeshIntegerCoord@@QAEAAV0@ABH@Z",
		"??_3QuadMeshIntegerCoord@@QAEAAV0@ABH@Z",
		"??HQuadMeshIntegerCoord@@QBE?AV0@ABV0@@Z",
		"??GQuadMeshIntegerCoord@@QBE?AV0@ABV0@@Z",
		"??DQuadMeshIntegerCoord@@QBE?AV0@ABH@Z",
		"??KQuadMeshIntegerCoord@@QBE?AV0@ABH@Z",
		"??5QuadMeshIntegerCoord@@QBE?AV0@ABH@Z",
		"??6QuadMeshIntegerCoord@@QBE?AV0@ABH@Z",
		"??8QuadMeshIntegerCoord@@QBE_NABV0@@Z",
		"??9QuadMeshIntegerCoord@@QBE_NABV0@@Z",
		"?GetLatLonDeg@QuadMeshIntegerCoord@@QBEXAAN0@Z",
		"?NormalizeToLocation@QuadMeshIntegerCoord@@QAEXABV1@@Z",
		"?SetMidPoint@QuadMeshIntegerCoord@@QAEXABV1@0@Z",
		"?AsLatLonAlt48@QuadMeshIntegerCoord@@QBE?AULATLONALT@@XZ",
		"?AsLatLon48@QuadMeshIntegerCoord@@QBE?AULATLON@@XZ",
		"??4QuadMeshID@@QAEAAV0@ABV0@@Z",
		"??0QuadMeshID@@QAE@XZ",
		"??0QuadMeshID@@QAE@ABVQuadMeshCoord@@I@Z",
		"??0QuadMeshID@@QAE@ABVQuadMeshIntegerCoord@@I@Z",
		"??0QuadMeshID@@QAE@III@Z",
		"?Init@QuadMeshID@@QAEXIII@Z",
		"?Invalidate@QuadMeshID@@QAEXXZ",
		"?MakeWholeWorldID@QuadMeshID@@QAEXXZ",
		"?IsValid@QuadMeshID@@QBEHXZ",
		"?IsWholeWorld@QuadMeshID@@QBEHXZ",
		"?AdjustLevel@QuadMeshID@@QAEAAV1@I@Z",
		"?GetU@QuadMeshIntegerCoord@@QBEHXZ",
		"?SetU@QuadMeshIntegerCoord@@QAEAAV1@H@Z",
		"?SetV@QuadMeshIntegerCoord@@QAEAAV1@H@Z",
		"?IsLatEqual@QuadMeshID@@QBEHABV1@@Z",
		"?IsLonEqual@QuadMeshID@@QBEHABV1@@Z",
		"??8QuadMeshID@@QBEHABV0@@Z",
		"??9QuadMeshID@@QBEHABV0@@Z",
		"??MQuadMeshID@@QBEHABV0@@Z",
		"??NQuadMeshID@@QBEHABV0@@Z",
		"??OQuadMeshID@@QBEHABV0@@Z",
		"??PQuadMeshID@@QBEHABV0@@Z",
		"?GetBranch@QuadMeshID@@QBE?AW4Branch@1@I@Z",
		"?GetNWQmicCoord@QuadMeshID@@QBE?AVQuadMeshIntegerCoord@@XZ",
		"?GetNEQmicCoord@QuadMeshID@@QBE?AVQuadMeshIntegerCoord@@XZ",
		"?GetSWQmicCoord@QuadMeshID@@QBE?AVQuadMeshIntegerCoord@@XZ",
		"?GetSEQmicCoord@QuadMeshID@@QBE?AVQuadMeshIntegerCoord@@XZ",
		"?GetCenterQmicCoord@QuadMeshID@@QBE?AVQuadMeshIntegerCoord@@XZ",
		"?GetUpperLeftCoord@QuadMeshID@@QBE?AVQuadMeshCoord@@XZ",
		"?GetUpperRightCoord@QuadMeshID@@QBE?AVQuadMeshCoord@@XZ",
		"?GetSWCoord@QuadMeshID@@QBE?AVQuadMeshCoord@@XZ",
		"?GetCenterCoord@QuadMeshID@@QBE?AVQuadMeshCoord@@XZ",
		"?GetSECoord@QuadMeshID@@QBE?AVQuadMeshCoord@@XZ",
		"?GetLatitudeN@QuadMeshID@@QBENXZ",
		"?GetLatitudeS@QuadMeshID@@QBENXZ",
		"?GetLongitudeW@QuadMeshID@@QBENXZ",
		"?GetLongitudeE@QuadMeshID@@QBENXZ",
		"?GetNNeighbor@QuadMeshID@@QBE?AV1@XZ",
		"?GetSNeighbor@QuadMeshID@@QBE?AV1@XZ",
		"?GetENeighbor@QuadMeshID@@QBE?AV1@XZ",
		"?GetWNeighbor@QuadMeshID@@QBE?AV1@XZ",
		"?GetNWNeighbor@QuadMeshID@@QBE?AV1@XZ",
		"?GetNENeighbor@QuadMeshID@@QBE?AV1@XZ",
		"?GetSWNeighbor@QuadMeshID@@QBE?AV1@XZ",
		"?GetSENeighbor@QuadMeshID@@QBE?AV1@XZ",
		"?GetUpperLeftChild@QuadMeshID@@QBE?AV1@XZ",
		"?GetUpperRightChild@QuadMeshID@@QBE?AV1@XZ",
		"?GetSWChild@QuadMeshID@@QBE?AV1@XZ",
		"?GetSEChild@QuadMeshID@@QBE?AV1@XZ",
		"?GetParent@QuadMeshID@@QBE?AV1@XZ",
		"?GetPolarityOfBlock@QuadMeshID@@QBE?AW4Polarity@1@XZ",
		"?IsParentOf@QuadMeshID@@QBEHABV1@@Z",
		"?IsChildOf@QuadMeshID@@QBEHABV1@@Z",
		"?DoesIntersect@QuadMeshID@@QBEHABV1@@Z",
		"?GetLatitudeExtentInMeters@QuadMeshID@@SGNI@Z",
		"?GetLimitU@QuadMeshID@@SGKI@Z",
		"?GetLimitV@QuadMeshID@@SGKI@Z",
		"?GetOOLimitU@QuadMeshID@@SGNI@Z",
		"?GetOOLimitV@QuadMeshID@@SGNI@Z",
		"?GetOppositeNeighborIndex@QuadMeshID@@SGHH@Z",
		"?BuildQMIDArray@QuadMeshID@@SGHABV1@0QAPAV1@QAI@Z",
		"?GetChild@QuadMeshID@@QBE?AV1@H@Z",
		"?GetNeighbor@QuadMeshID@@QBE?AV1@H@Z",
		"?ConvertToLatLonRect32@QuadMeshID@@QBEXAAULATLONRECT32@@@Z",
		"?GetExtents@QuadMeshID@@QBEXPAM0N@Z",
		"?Normalized@QuadMeshIntegerCoord@@QBE?AV1@XZ",
		"?GetLonDeg@QuadMeshIntegerCoord@@QBENXZ",
		"?GetLatDeg@QuadMeshIntegerCoord@@QBENXZ",
		"?GetLatLonRad@QuadMeshIntegerCoord@@QBEXAAN0@Z",
		"?GetLonDeg@QuadMeshCoord@@QBENXZ",
		"?GetLatDeg@QuadMeshCoord@@QBENXZ",
		"?GetLatLonRad@QuadMeshCoord@@QBEXAAN0@Z",
		"?FromLatLonDeg@QuadMeshIntegerCoord@@SG?AV1@ABN0@Z",
		"?FromLatLonDeg@QuadMeshCoord@@SG?AV1@ABN0@Z",
		"?FromLatLonRad@QuadMeshCoord@@SG?AV1@ABN0@Z",
		"??0XYZ@@QAE@ABULATLONALT@@@Z",
		"?multNT3d@@YGXABVMatrix3D@@0AAV1@@Z",
		"?mult3d@@YGXABVMatrix3D@@0AAV1@@Z",
		"?mult3d@@YGXABVMatrix3D@@ABVVector3D@@AAV2@@Z",
		"?initWorld2Body@Matrix3D@@QAEXNNN@Z",
		"?initBody2World@Matrix3D@@QAEXNNN@Z",
		"??8@YGHABVLLA@@0@Z",
		"?Angl_2PI@@YGNN@Z",
		"?Angl_PI@@YGNN@Z",
		"?Angl_180@@YGNN@Z",
		"?Angl_360@@YGNN@Z",
		"?multTN3d@@YGXABVMatrix3D@@0AAV1@@Z",
		"?multT3d@@YGXABVMatrix3D@@ABVVector3D@@AAV2@@Z",
		"??0XYZ@@QAE@ABULATLONALT@@0@Z",
		"?LLA2Meters@@YGXABULATLONALT@@0AAUXYZF64@@@Z",
		"?Meters2LLA@@YGXABUXYZF64@@ABULATLONALT@@AAU2@@Z",
		"?getOrtV3d@@YGXABVVector3D@@AAV1@@Z",
		"?getOrtV3d@@YGXABVVector3D@@0AAV1@@Z",
		"?GetDist@@YGNABULATLONALT@@0@Z",
		"?_freeall@_ArrList@@IAEXXZ",
		"?_free@_ArrList@@IAEPAVArrListNode@@PAV2@@Z",
		"?AllocNodes@_ArrList@@AAEXH@Z",
		"??1_ArrList@@IAE@XZ",
		"??0_ArrList@@IAE@IHH@Z",
		"?_freetail@_ArrList@@IAEPAVArrListNode@@PAV2@@Z",
		"?_newtail@_ArrList@@IAEPAVArrListNode@@XZ",
		"?_newhead@_ArrList@@IAEPAVArrListNode@@XZ",
		"?setAt@_array@@QAEPAXIPAX@Z",
		"?_indexOf@_array@@QBEHPAX@Z",
		"?_find@_array@@QBEPAXPAX@Z",
		"?_remove@_array@@QAEPAXXZ",
		"?_insert@_array@@QAEPAXPAX@Z",
		"?empty@_array@@QAEXXZ",
		"??1_array@@QAE@XZ",
		"??0_array@@QAE@H@Z",
		"?swap@_list@@QAEXXZ",
		"?_prev@_list@@IBEPAUlink@@XZ",
		"?_last@_list@@IBEPAUlink@@XZ",
		"?_find@_list@@IBEPAUlink@@PAU2@@Z",
		"?_indexOf@_list@@QBEHPAX@Z",
		"?_elem@_list@@IBEPAUlink@@I@Z",
		"?_remove@_list@@IAEPAUlink@@XZ",
		"?_append@_list@@IAEPAUlink@@PAU2@@Z",
		"?_insert@_list@@IAEPAUlink@@PAU2@@Z",
		"?empty@_list@@QAEXXZ",
		"?size@_list@@QAEHXZ",
		"?UnhookFromList@List@@QAEHPAVListElement@@@Z",
		"?DeleteByPointer@List@@QAEHPAVListElement@@@Z",
		"?Add@List@@QAEXPAVListElement@@@Z",
		"?Insert@List@@QAEXPAVListElement@@@Z",
		"??0List@@QAE@XZ",
		"?GetByPos@List@@QAEPAVListElement@@I@Z",
		"?Free@List@@QAEXXZ",
		"?InsertAfter@List@@QAEXPAVListElement@@0@Z",
		"?GetNext@List@@QBEPBVListElement@@PBV2@@Z",
		"?GetNext@List@@QAEPAVListElement@@PAV2@@Z",
		"?GetPrev@List@@QBEPBVListElement@@PBV2@@Z",
		"?GetPrev@List@@QAEPAVListElement@@PAV2@@Z",
		"??1List@@QAE@XZ",
		"?InsertBefore@List@@QAEXPAVListElement@@0@Z",
		"?SetColor@CLineHelper@@QAEXI@Z",
		"?AddSegments@CLineHelper@@QAEHPAULineSegment@1@H@Z",
		"?Draw@CLineHelper@@IAEXXZ",
		"?Enable@CLineHelper@@QAEXH@Z",
		"??1CLineHelper@@UAE@XZ",
		"?PostRadsortHandler@CLineHelper@@KGXIPAX@Z",
		"?ChainMessageHandler@CLineHelper@@KGXIPAX@Z",
		"??0CLineHelper@@QAE@XZ",
		"?RemoveFromFrameList@CPlatform@@QAEXXZ",
		"?UnregisterFromSimulation@CPlatform@@QAEXXZ",
		"?PointOnPlaneInPlatformPolygon@CPlatform@@IAEHPBUXYZF64@@@Z",
		"?ProjectToA2DPrimaryPlane@CPlatform@@IAEXXZ",
		"?RotateToBodyAxis@CPlatform@@IAEXPAUXYZF64@@@Z",
		"?RotateToWorldAxis@CPlatform@@IAEXPAUXYZF64@@@Z",
		"?SetLLAPBH@CPlatform@@QAEXPBULATLONALT@@PBUPBH32@@@Z",
		"?SetLLAPBH@CPlatform@@QAEXPBTLATLONALTPBH@@@Z",
		"?ReleaseResources@CPlatform@@IAEXXZ",
		"??0CPlatform@@QAE@XZ",
		"?CalcPlatformConstants@CPlatform@@IAEXXZ",
		"?RecalcRotationMatrix@CPlatform@@IAEXXZ",
		"??1CPlatform@@QAE@XZ",
		"?InitPlatform@CPlatform@@QAEHPBTLATLONALTPBH@@PBUXYZF32@@HW4SURFACE_TYPE@@PBUCABLE_INFO_M@@QBUMATRIXF32@@@Z",
		"?GetCableInfo@CPlatform@@QAEHPBULATLONALT@@PAU_CABLE_INFO@@@Z",
		"?GetPlatformInfo@CPlatform@@IAEHPBULATLONALT@@PAU_SURFACE_INFO@@I@Z",
		"?staticPlatformCallback@CPlatform@@KGHPBULATLONALT@@PAU_SURFACE_INFO@@IPAX@Z",
		"?AddToFrameList@CPlatform@@QAEHXZ",
		"?RegisterWithSimulation@CPlatform@@QAEHXZ",
		"?FindIntersectionPoint@Segments@@YGHABVLLA@@000AAV2@@Z",
		"?ExtendSegment@Segments@@YG?AVLLA@@ABV2@0@Z",
		"?VectorsIntersect@Segments@@YGHABVLLA@@000AAV2@@Z",
		"?SegmentsIntersect@Segments@@YGHABVLLA@@000@Z",
		"?m_dUVtoQMCScale@QuadMeshID@@1QBNB",
		"?m_dwLimitU@QuadMeshID@@1QBKB",
		"?m_dOOLimitU@QuadMeshID@@1QBNB",
		"?m_dwLimitV@QuadMeshID@@1QBKB",
		"?m_dOOLimitV@QuadMeshID@@1QBNB",
		"?MAX_LATTITUDE_DELTA@QuadMeshCoord@@2NB",
		"??_7ListElement@@6B@",
		"??_7CLineHelper@@6B@",
		"ImportTable",
		"Linkage",
		"?_s_pLinesList@CLineHelper@@1PAVList@@A",
		"?MAX_LONGTITUDE_DELTA@QuadMeshCoord@@2NB"
};


static unsigned long Util_MethodMap[sizeof(UtilExptsNames)/sizeof(char*)];
static unsigned int IsInitialized=0;
static void Util_init_engine();

static void __declspec(naked)Method_Invoke(unsigned long Index,unsigned long* MethodTable)
{_asm{
	push esi
	push ecx
	mov ecx,IsInitialized
	dec ecx
	jcxz cont 
	call Util_init_engine
cont:
	mov ecx,[esp+8]
	mov esi,[esp+0x0C]
	lea esi,[esi+ecx*4]
	mov esi,[esi]
	mov [esp+0x0C],esi
	pop ecx
	pop esi
	add esp,4
	ret
	int 3
}}



DWORD __declspec(naked) _array::_indexOf(void)
{
	_asm {
		push offset Util_MethodMap
			push 0
			jmp Method_Invoke
	}
}

DWORD __declspec(naked) _array::size(void)
{
	_asm {
		push offset Util_MethodMap
			push 1
			jmp Method_Invoke
	}
}
/*
void __declspec(naked) _ArrList::_next(ArrListNode *)
{
_asm {
push offset Util_MethodMap
push 2
jmp Method_Invoke
}
}

  void __declspec(naked) _ArrList::_prev(ArrListNode *)
  {
  _asm {
  push offset Util_MethodMap
  push 3
  jmp Method_Invoke
  }
  }
  
	void __declspec(naked) _ArrList::operator=(_ArrList const &)
	{
	_asm {
	push offset Util_MethodMap
	push 4
	jmp Method_Invoke
	}
	}
	
	  void __declspec(naked) ListElement::ListElement(void)
	  {
	  _asm {
      push offset Util_MethodMap
      push 5
      jmp Method_Invoke
	  }
	  }
	  
		void __declspec(naked) ListElement::~ListElement(void)
		{
		_asm {
		push offset Util_MethodMap
		push 6
		jmp Method_Invoke
		}
		}
		
		  void __declspec(naked) ListElement::SetPrev(ListElement *)
		  {
		  _asm {
		  push offset Util_MethodMap
		  push 7
		  jmp Method_Invoke
		  }
		  }
		  
			void __declspec(naked) ListElement::SetNext(ListElement *)
			{
			_asm {
			push offset Util_MethodMap
			push 8
			jmp Method_Invoke
			}
			}
			
			  void __declspec(naked) ListElement::ListElement(ListElement const &)
			  {
			  _asm {
			  push offset Util_MethodMap
			  push 9
			  jmp Method_Invoke
			  }
			  }
			  
				void __declspec(naked) ListElement::operator=(ListElement const &)
				{
				_asm {
				push offset Util_MethodMap
				push 10
				jmp Method_Invoke
				}
				}
*/

__declspec(naked) void*  _array::_curr(void)
{
	_asm {
		push offset Util_MethodMap
			push 11
			jmp Method_Invoke
	}
}

__declspec(naked) void*  _array::_next(void)
{
	_asm {
		push offset Util_MethodMap
			push 12
			jmp Method_Invoke
	}
}

__declspec(naked) void*  _array::_prev(void)
{
	_asm {
		push offset Util_MethodMap
			push 13
			jmp Method_Invoke
	}
}

__declspec(naked) void*  _array::_first(void)
{
	_asm {
		push offset Util_MethodMap
			push 14
			jmp Method_Invoke
	}
}

__declspec(naked) void*  _array::_last(void)
{
	_asm {
		push offset Util_MethodMap
			push 15
			jmp Method_Invoke
	}
}

__declspec(naked) void* _array::_append(void *)
{
	_asm {
		push offset Util_MethodMap
			push 16
			jmp Method_Invoke
	}
}

__declspec(naked) void*  _array::_elem(unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 17
			jmp Method_Invoke
	}
}

__declspec(naked) _array&  _array::operator=(_array const &)
{
	_asm {
		push offset Util_MethodMap
			push 18
			jmp Method_Invoke
	}
}

__declspec(naked) _array::~_array(void)
{
	_asm {
		push offset Util_MethodMap
			push 19
			jmp Method_Invoke
	}
}

/*
void __declspec(naked) _list::_first(void)
{
_asm {
push offset Util_MethodMap
push 20
jmp Method_Invoke
}
}

  void __declspec(naked) _list::_curr(void)
  {
  _asm {
  push offset Util_MethodMap
  push 21
  jmp Method_Invoke
  }
  }
  
	void __declspec(naked) _list::_next(void)
	{
	_asm {
	push offset Util_MethodMap
	push 22
	jmp Method_Invoke
	}
	}
	
	  void __declspec(naked) _list::_list(void)
	  {
	  _asm {
      push offset Util_MethodMap
      push 23
      jmp Method_Invoke
	  }
	  }
	  
		void __declspec(naked) _list::_indexOf(void)
		{
		_asm {
		push offset Util_MethodMap
		push 24
		jmp Method_Invoke
		}
		}
		
		  void __declspec(naked) CLineHelper::SetLLAPBH(LATLONALTPBH const *)
		  {
		  _asm {
		  push offset Util_MethodMap
		  push 25
		  jmp Method_Invoke
		  }
		  }
		  
			void __declspec(naked) CLineHelper::SetLLAPBH(LATLONALT const *,PBH32 const *)
			{
			_asm {
			push offset Util_MethodMap
			push 26
			jmp Method_Invoke
			}
			}
			
			  void __declspec(naked) CLineHelper::SetLLA(LATLONALT *)
			  {
			  _asm {
			  push offset Util_MethodMap
			  push 27
			  jmp Method_Invoke
			  }
			  }
			  
				void __declspec(naked) CLineHelper::SetPBH(PBH32 *)
				{
				_asm {
				push offset Util_MethodMap
				push 28
				jmp Method_Invoke
				}
				}
				
				  void __declspec(naked) CLineHelper::SetHeading(uint)
				  {
				  _asm {
				  push offset Util_MethodMap
				  push 29
				  jmp Method_Invoke
				  }
				  }
				  
					void __declspec(naked) CLineHelper::SetDegreeHeading(double)
					{
					_asm {
					push offset Util_MethodMap
					push 30
					jmp Method_Invoke
					}
					}
					
					  void __declspec(naked) CLineHelper::CLineHelper(CLineHelper const &)
					  {
					  _asm {
					  push offset Util_MethodMap
					  push 31
					  jmp Method_Invoke
					  }
					  }
					  
						void __declspec(naked) CLineHelper::operator=(CLineHelper const &)
						{
						_asm {
						push offset Util_MethodMap
						push 32
						jmp Method_Invoke
						}
						}
						
						  void __declspec(naked) CPlatform::NotifyVelocity(XYZF64 const *,XYZF64 const *)
						  {
						  _asm {
						  push offset Util_MethodMap
						  push 33
						  jmp Method_Invoke
						  }
						  }
						  
							void __declspec(naked) CPlatform::SetSurfaceType(SURFACE_TYPE)
							{
							_asm {
							push offset Util_MethodMap
							push 34
							jmp Method_Invoke
							}
							}
							
							  void __declspec(naked) CPlatform::SetSurfaceCondition(SURFACE_CONDITION)
							  {
							  _asm {
							  push offset Util_MethodMap
							  push 35
							  jmp Method_Invoke
							  }
							  }
							  
								void __declspec(naked) CPlatform::operator=(CPlatform const &)
								{
								_asm {
								push offset Util_MethodMap
								push 36
								jmp Method_Invoke
								}
								}
*/
__declspec(naked)XYZF64& XYZ::ToXYZF64(void)
{
	_asm {
		push offset Util_MethodMap
			push 37
			jmp Method_Invoke
	}
}

__declspec(naked) XYZ::XYZ(double,double,double)
{
	_asm {
		push offset Util_MethodMap
			push 38
			jmp Method_Invoke
	}
}

void __declspec(naked) XYZ::Init(double,double,double)
{
	_asm {
		push offset Util_MethodMap
			push 39
			jmp Method_Invoke
	}
}

void __declspec(naked) XYZ::Zero(void)
{
	_asm {
		push offset Util_MethodMap
			push 40
			jmp Method_Invoke
	}
}

void __declspec(naked) XYZ::ToLLA(LLA const &,LLA &)
{
	_asm {
		push offset Util_MethodMap
			push 41
			jmp Method_Invoke
	}
}

__declspec(naked)XYZ& XYZ::operator=(int)
{
	_asm {
		push offset Util_MethodMap
			push 42
			jmp Method_Invoke
	}
}

__declspec(naked)XYZ& XYZ::operator=(XYZF64 const &)
{
	_asm {
		push offset Util_MethodMap
			push 43
			jmp Method_Invoke
	}
}

__declspec(naked) Vector3D::Vector3D(LATLONALT const &,LATLONALT const &)
{
	_asm {
		push offset Util_MethodMap
			push 44
			jmp Method_Invoke
	}
}

__declspec(naked) Vector3D::Vector3D(LATLONALT const &)
{
	_asm {
		push offset Util_MethodMap
			push 45
			jmp Method_Invoke
	}
}

Vector3D __declspec(naked) Vector3D::operator-()
{
	_asm {
		push offset Util_MethodMap
			push 46
			jmp Method_Invoke
	}
}

__declspec(naked)Vector3D& Vector3D::operator+=(Vector3D const &)
{
	_asm {
		push offset Util_MethodMap
			push 47
			jmp Method_Invoke
	}
}

__declspec(naked)Vector3D& Vector3D::operator-=(Vector3D const &)
{
	_asm {
		push offset Util_MethodMap
			push 48
			jmp Method_Invoke
	}
}

__declspec(naked)Vector3D& Vector3D::operator*=(double)
{
	_asm {
		push offset Util_MethodMap
			push 49
			jmp Method_Invoke
	}
}

__declspec(naked)Vector3D& Vector3D::operator/=(double)
{
	_asm {
		push offset Util_MethodMap
			push 50
			jmp Method_Invoke
	}
}

double __declspec(naked) Vector3D::norm(void)
{
	_asm {
		push offset Util_MethodMap
			push 51
			jmp Method_Invoke
	}
}

double __declspec(naked) Vector3D::dot(Vector3D const &)
{
	_asm {
		push offset Util_MethodMap
			push 52
			jmp Method_Invoke
	}
}

__declspec(naked)Vector3D Vector3D::cross(Vector3D const & to_cross)
{
	_asm {
		push offset Util_MethodMap
			push 53
			jmp Method_Invoke
	}
}

double __declspec(naked) Vector3D::length(void)
{
	_asm {
		push offset Util_MethodMap
			push 54
			jmp Method_Invoke
	}
}

double __declspec(naked) Vector3D::length_sqr(void)
{
	_asm {
		push offset Util_MethodMap
			push 55
			jmp Method_Invoke
	}
}

void __declspec(naked) LLA::Zero(void)
{
	_asm {
		push offset Util_MethodMap
			push 56
			jmp Method_Invoke
	}
}

void __declspec(naked) LLA::GetAltitude(void)
{
	_asm {
		push offset Util_MethodMap
			push 57
			jmp Method_Invoke
	}
}

void __declspec(naked) LLA::SetAltitude(double)
{
	_asm {
		push offset Util_MethodMap
			push 58
			jmp Method_Invoke
	}
}

void __declspec(naked) LLA::ToMeters(LLA const &,XYZ &)
{
	_asm {
		push offset Util_MethodMap
			push 59
			jmp Method_Invoke
	}
}

__declspec(naked)char* LLA::LatToStr(char *)
{
	_asm {
		push offset Util_MethodMap
			push 60
			jmp Method_Invoke
	}
}

__declspec(naked)char* LLA::LonToStr(char *)
{
	_asm {
		push offset Util_MethodMap
			push 61
			jmp Method_Invoke
	}
}

__declspec(naked)char* LLA::AltToStr(char *)
{
	_asm {
		push offset Util_MethodMap
			push 62
			jmp Method_Invoke
	}
}

void __declspec(naked) LLA::StrToLat(char const *)
{
	_asm {
		push offset Util_MethodMap
			push 63
			jmp Method_Invoke
	}
}

void __declspec(naked) LLA::StrToLon(char const *)
{
	_asm {
		push offset Util_MethodMap
			push 64
			jmp Method_Invoke
	}
}

void __declspec(naked) LLA::StrToAlt(char const *)
{
	_asm {
		push offset Util_MethodMap
			push 65
			jmp Method_Invoke
	}
}

__declspec(naked) LLA& LLA::operator=(int)
{
	_asm {
		push offset Util_MethodMap
			push 66
			jmp Method_Invoke
	}
}

__declspec(naked) LLA::operator LATLONALT const(void)
{
	_asm {
		push offset Util_MethodMap
			push 67
			jmp Method_Invoke
	}
}

__declspec(naked)LLA& LLA::operator+=(Vector3D const &)
{
	_asm {
		push offset Util_MethodMap
			push 68
			jmp Method_Invoke
	}
}

__declspec(naked)XYZ& XYZ::operator=(XYZ const &)
{
	_asm {
		push offset Util_MethodMap
			push 69
			jmp Method_Invoke
	}
}

__declspec(naked)Matrix3D& Matrix3D::operator=(Matrix3D const &)
{
	_asm {
		push offset Util_MethodMap
			push 70
			jmp Method_Invoke
	}
}

void __declspec(naked) Matrix3D::normZYX(void)
{
	_asm {
		push offset Util_MethodMap
			push 71
			jmp Method_Invoke
	}
}

__declspec(naked) QuadMeshCoord::QuadMeshCoord(double const &,double const &)
{
	_asm {
		push offset Util_MethodMap
			push 72
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshCoord::GetU(void)
{
	_asm {
		push offset Util_MethodMap
			push 73
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshCoord::GetV(void)
{
	_asm {
		push offset Util_MethodMap
			push 74
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord& QuadMeshCoord::SetU(double)
{
	_asm {
		push offset Util_MethodMap
			push 75
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord& QuadMeshCoord::SetV(double)
{
	_asm {
		push offset Util_MethodMap
			push 76
			jmp Method_Invoke
	}
}

/*void __declspec(naked) _list::operator=(_list const &)
{
_asm {
push offset Util_MethodMap
push 77
jmp Method_Invoke
}
} */

__declspec(naked)QuadMeshCoord& QuadMeshCoord::SetLatLonRad(double const &,double const &)
{
	_asm {
		push offset Util_MethodMap
			push 78
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord& QuadMeshCoord::SetLatLonDeg(double const &,double const &)
{
	_asm {
		push offset Util_MethodMap
			push 79
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord& QuadMeshCoord::operator+=(QuadMeshCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 80
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord& QuadMeshCoord::operator-=(QuadMeshCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 81
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord& QuadMeshCoord::operator*=(double const &)
{
	_asm {
		push offset Util_MethodMap
			push 82
			jmp Method_Invoke
	}
}

QuadMeshCoord __declspec(naked) QuadMeshCoord::operator+(QuadMeshCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 83
			jmp Method_Invoke
	}
}

QuadMeshCoord __declspec(naked) QuadMeshCoord::operator-(QuadMeshCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 84
			jmp Method_Invoke
	}
}

QuadMeshCoord __declspec(naked) QuadMeshCoord::operator*(double const &)
{
	_asm {
		push offset Util_MethodMap
			push 85
			jmp Method_Invoke
	}
}

bool __declspec(naked) QuadMeshCoord::operator==(QuadMeshCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 86
			jmp Method_Invoke
	}
}

bool __declspec(naked) QuadMeshCoord::operator!=(QuadMeshCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 87
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshCoord::GetLatLonDeg(double &,double &)
{
	_asm {
		push offset Util_MethodMap
			push 88
			jmp Method_Invoke
	}
}

QuadMeshIntegerCoord __declspec(naked) QuadMeshIntegerCoord::FromLatLonAlt48(LATLONALT const &)
{
	_asm {
		push offset Util_MethodMap
			push 89
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord& QuadMeshIntegerCoord::SetLatLonDeg(double const &,double const &)
{
	_asm {
		push offset Util_MethodMap
			push 90
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord& QuadMeshIntegerCoord::operator+=(QuadMeshIntegerCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 91
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord& QuadMeshIntegerCoord::operator-=(QuadMeshIntegerCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 92
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord& QuadMeshIntegerCoord::operator*=(int const &)
{
	_asm {
		push offset Util_MethodMap
			push 93
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord& QuadMeshIntegerCoord::operator/=(int const &)
{
	_asm {
		push offset Util_MethodMap
			push 94
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord& QuadMeshIntegerCoord::operator>>=(int const &)
{
	_asm {
		push offset Util_MethodMap
			push 95
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord& QuadMeshIntegerCoord::operator<<=(int const &)
{
	_asm {
		push offset Util_MethodMap
			push 96
			jmp Method_Invoke
	}
}

QuadMeshIntegerCoord __declspec(naked) QuadMeshIntegerCoord::operator+(QuadMeshIntegerCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 97
			jmp Method_Invoke
	}
}

QuadMeshIntegerCoord __declspec(naked) QuadMeshIntegerCoord::operator-(QuadMeshIntegerCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 98
			jmp Method_Invoke
	}
}

QuadMeshIntegerCoord __declspec(naked) QuadMeshIntegerCoord::operator*(int const &)
{
	_asm {
		push offset Util_MethodMap
			push 99
			jmp Method_Invoke
	}
}

QuadMeshIntegerCoord __declspec(naked) QuadMeshIntegerCoord::operator/(int const &)
{
	_asm {
		push offset Util_MethodMap
			push 100
			jmp Method_Invoke
	}
}

QuadMeshIntegerCoord __declspec(naked) QuadMeshIntegerCoord::operator>>(int const &)
{
	_asm {
		push offset Util_MethodMap
			push 101
			jmp Method_Invoke
	}
}

QuadMeshIntegerCoord __declspec(naked) QuadMeshIntegerCoord::operator<<(int const &)
{
	_asm {
		push offset Util_MethodMap
			push 102
			jmp Method_Invoke
	}
}

bool __declspec(naked) QuadMeshIntegerCoord::operator==(QuadMeshIntegerCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 103
			jmp Method_Invoke
	}
}

bool __declspec(naked) QuadMeshIntegerCoord::operator!=(QuadMeshIntegerCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 104
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshIntegerCoord::GetLatLonDeg(double &,double &)
{
	_asm {
		push offset Util_MethodMap
			push 105
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshIntegerCoord::NormalizeToLocation(QuadMeshIntegerCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 106
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshIntegerCoord::SetMidPoint(QuadMeshIntegerCoord const &,QuadMeshIntegerCoord const &)
{
	_asm {
		push offset Util_MethodMap
			push 107
			jmp Method_Invoke
	}
}

LATLONALT __declspec(naked) QuadMeshIntegerCoord::AsLatLonAlt48(void)
{
	_asm {
		push offset Util_MethodMap
			push 108
			jmp Method_Invoke
	}
}

/*
__declspec(naked)LATLON QuadMeshIntegerCoord::AsLatLon48()
{
_asm {
push offset Util_MethodMap
push 109
jmp Method_Invoke
}
}*/


__declspec(naked)QuadMeshID& QuadMeshID::operator=(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 110
			jmp Method_Invoke
	}
}

__declspec(naked) QuadMeshID::QuadMeshID(void)
{
	_asm {
		push offset Util_MethodMap
			push 111
			jmp Method_Invoke
	}
}

__declspec(naked) QuadMeshID::QuadMeshID(QuadMeshCoord const &,unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 112
			jmp Method_Invoke
	}
}

__declspec(naked) QuadMeshID::QuadMeshID(QuadMeshIntegerCoord const &,unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 113
			jmp Method_Invoke
	}
}

__declspec(naked) QuadMeshID::QuadMeshID(unsigned int,unsigned int,unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 114
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshID::Init(unsigned int,unsigned int,unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 115
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshID::Invalidate(void)
{
	_asm {
		push offset Util_MethodMap
			push 116
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshID::MakeWholeWorldID(void)
{
	_asm {
		push offset Util_MethodMap
			push 117
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::IsValid(void)
{
	_asm {
		push offset Util_MethodMap
			push 118
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::IsWholeWorld(void)
{
	_asm {
		push offset Util_MethodMap
			push 119
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshID& QuadMeshID::AdjustLevel(unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 120
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshIntegerCoord::GetU(void)
{
	_asm {
		push offset Util_MethodMap
			push 121
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshIntegerCoord::SetU(int)
{
	_asm {
		push offset Util_MethodMap
			push 122
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshIntegerCoord::SetV(int)
{
	_asm {
		push offset Util_MethodMap
			push 123
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::IsLatEqual(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 124
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::IsLonEqual(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 125
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::operator==(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 126
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::operator!=(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 127
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::operator<(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 128
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::operator<=(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 129
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::operator>(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 130
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::operator>=(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 131
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::GetBranch(unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 132
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord QuadMeshID::GetNWQmicCoord()
{
	_asm {
		push offset Util_MethodMap
			push 133
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord QuadMeshID::GetNEQmicCoord()
{
	_asm {
		push offset Util_MethodMap
			push 134
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord QuadMeshID::GetSWQmicCoord()
{
	_asm {
		push offset Util_MethodMap
			push 135
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord QuadMeshID::GetSEQmicCoord()
{
	_asm {
		push offset Util_MethodMap
			push 136
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshIntegerCoord QuadMeshID::GetCenterQmicCoord()
{
	_asm {
		push offset Util_MethodMap
			push 137
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord QuadMeshID::GetUpperLeftCoord()
{
	_asm {
		push offset Util_MethodMap
			push 138
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord QuadMeshID::GetUpperRightCoord()
{
	_asm {
		push offset Util_MethodMap
			push 139
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord QuadMeshID::GetSWCoord()
{
	_asm {
		push offset Util_MethodMap
			push 140
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord QuadMeshID::GetCenterCoord()
{
	_asm {
		push offset Util_MethodMap
			push 141
			jmp Method_Invoke
	}
}

__declspec(naked)QuadMeshCoord QuadMeshID::GetSECoord()
{
	_asm {
		push offset Util_MethodMap
			push 142
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshID::GetLatitudeN(void)
{
	_asm {
		push offset Util_MethodMap
			push 143
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshID::GetLatitudeS(void)
{
	_asm {
		push offset Util_MethodMap
			push 144
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshID::GetLongitudeW(void)
{
	_asm {
		push offset Util_MethodMap
			push 145
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshID::GetLongitudeE(void)
{
	_asm {
		push offset Util_MethodMap
			push 146
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetNNeighbor()
{
	_asm {
		push offset Util_MethodMap
			push 147
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetSNeighbor()
{
	_asm {
		push offset Util_MethodMap
			push 148
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetENeighbor()
{
	_asm {
		push offset Util_MethodMap
			push 149
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetWNeighbor()
{
	_asm {
		push offset Util_MethodMap
			push 150
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetNWNeighbor()
{
	_asm {
		push offset Util_MethodMap
			push 151
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetNENeighbor()
{
	_asm {
		push offset Util_MethodMap
			push 152
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetSWNeighbor()
{
	_asm {
		push offset Util_MethodMap
			push 153
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetSENeighbor()
{
	_asm {
		push offset Util_MethodMap
			push 154
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetUpperLeftChild()
{
	_asm {
		push offset Util_MethodMap
			push 155
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetUpperRightChild()
{
	_asm {
		push offset Util_MethodMap
			push 156
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetSWChild()
{
	_asm {
		push offset Util_MethodMap
			push 157
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetSEChild()
{
	_asm {
		push offset Util_MethodMap
			push 158
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetParent()
{
	_asm {
		push offset Util_MethodMap
			push 159
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::GetPolarityOfBlock(void)
{
	_asm {
		push offset Util_MethodMap
			push 160
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::IsParentOf(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 161
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::IsChildOf(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 162
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::DoesIntersect(QuadMeshID const &)
{
	_asm {
		push offset Util_MethodMap
			push 163
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshID::GetLatitudeExtentInMeters(unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 164
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshID::GetLimitU(unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 165
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshID::GetLimitV(unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 166
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshID::GetOOLimitU(unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 167
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshID::GetOOLimitV(unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 168
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::GetOppositeNeighborIndex(unsigned int)
{
	_asm {
		push offset Util_MethodMap
			push 169
			jmp Method_Invoke
	}
}

int __declspec(naked) QuadMeshID::BuildQMIDArray(QuadMeshID const &,QuadMeshID const &,QuadMeshID * * const,unsigned int * const)
{
	_asm {
		push offset Util_MethodMap
			push 170
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetChild(int)
{
	_asm {
		push offset Util_MethodMap
			push 171
			jmp Method_Invoke
	}
}

QuadMeshID __declspec(naked) QuadMeshID::GetNeighbor(int)
{
	_asm {
		push offset Util_MethodMap
			push 172
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshID::ConvertToLatLonRect32(LATLONRECT32 &)
{
	_asm {
		push offset Util_MethodMap
			push 173
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshID::GetExtents(float *,float *,double)
{
	_asm {
		push offset Util_MethodMap
			push 174
			jmp Method_Invoke
	}
}

QuadMeshIntegerCoord __declspec(naked) QuadMeshIntegerCoord::Normalized(void)
{
	_asm {
		push offset Util_MethodMap
			push 175
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshIntegerCoord::GetLonDeg(void)
{
	_asm {
		push offset Util_MethodMap
			push 176
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshIntegerCoord::GetLatDeg(void)
{
	_asm {
		push offset Util_MethodMap
			push 177
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshIntegerCoord::GetLatLonRad(double &,double &)
{
	_asm {
		push offset Util_MethodMap
			push 178
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshCoord::GetLonDeg(void)
{
	_asm {
		push offset Util_MethodMap
			push 179
			jmp Method_Invoke
	}
}

double __declspec(naked) QuadMeshCoord::GetLatDeg(void)
{
	_asm {
		push offset Util_MethodMap
			push 180
			jmp Method_Invoke
	}
}

void __declspec(naked) QuadMeshCoord::GetLatLonRad(double &,double &)
{
	_asm {
		push offset Util_MethodMap
			push 181
			jmp Method_Invoke
	}
}

QuadMeshIntegerCoord __declspec(naked) QuadMeshIntegerCoord::FromLatLonDeg(double const &,double const &)
{
	_asm {
		push offset Util_MethodMap
			push 182
			jmp Method_Invoke
	}
}

QuadMeshCoord __declspec(naked) QuadMeshCoord::FromLatLonDeg(double const &,double const &)
{
	_asm {
		push offset Util_MethodMap
			push 183
			jmp Method_Invoke
	}
}

QuadMeshCoord __declspec(naked) QuadMeshCoord::FromLatLonRad(double const &,double const &)
{
	_asm {
		push offset Util_MethodMap
			push 184
			jmp Method_Invoke
	}
}

__declspec(naked) XYZ::XYZ(LATLONALT const &)
{
	_asm {
		push offset Util_MethodMap
			push 185
			jmp Method_Invoke
	}
}

void __declspec(naked) multNT3d(Matrix3D const &,Matrix3D const &,Matrix3D &)
{
	_asm {
		push offset Util_MethodMap
			push 186
			jmp Method_Invoke
	}
}

void __declspec(naked) mult3d(Matrix3D const &,Matrix3D const &,Matrix3D &)
{
	_asm {
		push offset Util_MethodMap
			push 187
			jmp Method_Invoke
	}
}

void __declspec(naked) mult3d(Matrix3D const &,Vector3D const &,Vector3D &)
{
	_asm {
		push offset Util_MethodMap
			push 188
			jmp Method_Invoke
	}
}

void __declspec(naked) Matrix3D::initWorld2Body(double,double,double)
{
	_asm {
		push offset Util_MethodMap
			push 189
			jmp Method_Invoke
	}
}

void __declspec(naked) Matrix3D::initBody2World(double,double,double)
{
	_asm {
		push offset Util_MethodMap
			push 190
			jmp Method_Invoke
	}
}

void __declspec(naked) operator==(LLA const &,LLA const &)
{
	_asm {
		push offset Util_MethodMap
			push 191
			jmp Method_Invoke
	}
}

void __declspec(naked) Angl_2PI(double)
{
	_asm {
		push offset Util_MethodMap
			push 192
			jmp Method_Invoke
	}
}

void __declspec(naked) Angl_PI(double)
{
	_asm {
		push offset Util_MethodMap
			push 193
			jmp Method_Invoke
	}
}

void __declspec(naked) Angl_180(double)
{
	_asm {
		push offset Util_MethodMap
			push 194	
			jmp Method_Invoke
	}
}

void __declspec(naked) Angl_360(double)
{
	_asm {
		push offset Util_MethodMap
			push 195
			jmp Method_Invoke
	}
}

void __declspec(naked) multTN3d(Matrix3D const &,Matrix3D const &,Matrix3D &)
{
	_asm {
		push offset Util_MethodMap
			push 196
			jmp Method_Invoke
	}
}

void __declspec(naked) multT3d(Matrix3D const &,Vector3D const &,Vector3D &)
{
	_asm {
		push offset Util_MethodMap
			push 197
			jmp Method_Invoke
	}
}

__declspec(naked) XYZ::XYZ(LATLONALT const &,LATLONALT const &)
{
	_asm {
		push offset Util_MethodMap
			push 198
			jmp Method_Invoke
	}
}

void __declspec(naked) LLA2Meters(LATLONALT const &,LATLONALT const &,XYZF64 &)
{
	_asm {
		push offset Util_MethodMap
			push 199
			jmp Method_Invoke
	}
}

void __declspec(naked) Meters2LLA(XYZF64 const &,LATLONALT const &,LATLONALT &)
{
	_asm {
		push offset Util_MethodMap
			push 200
			jmp Method_Invoke
	}
}

void __declspec(naked) getOrtV3d(Vector3D const &,Vector3D &)
{
	_asm {
		push offset Util_MethodMap
			push 201
			jmp Method_Invoke
	}
}

void __declspec(naked) getOrtV3d(Vector3D const &,Vector3D const &,Vector3D &)
{
	_asm {
		push offset Util_MethodMap
			push 202
			jmp Method_Invoke
	}
}

void __declspec(naked) GetDist(LATLONALT const &,LATLONALT const &)
{
	_asm {
		push offset Util_MethodMap
			push 203
			jmp Method_Invoke
	}
}

/*void __declspec(naked) _ArrList::_freeall(void)
{
_asm {
push offset Util_MethodMap
push 204
jmp Method_Invoke
}
}

  void __declspec(naked) _ArrList::_free(ArrListNode *)
  {
  _asm {
  push offset Util_MethodMap
  push 205
  jmp Method_Invoke
  }
  }
  
	void __declspec(naked) _ArrList::AllocNodes(int)
	{
	_asm {
	push offset Util_MethodMap
	push 206
	jmp Method_Invoke
	}
	}
	
	  void __declspec(naked) _ArrList::~_ArrList(void)
	  {
	  _asm {
      push offset Util_MethodMap
      push 207
      jmp Method_Invoke
	  }
	  }
	  
		void __declspec(naked) _ArrList::_ArrList(uint,int,int)
		{
		_asm {
		push offset Util_MethodMap
		push 208
		jmp Method_Invoke
		}
		}
		
		  void __declspec(naked) _ArrList::_freetail(ArrListNode *)
		  {
		  _asm {
		  push offset Util_MethodMap
		  push 209
		  jmp Method_Invoke
		  }
		  }
		  
			void __declspec(naked) _ArrList::_newtail(void)
			{
			_asm {
			push offset Util_MethodMap
			push 210
			jmp Method_Invoke
			}
			}
			
			  void __declspec(naked) _ArrList::_newhead(void)
			  {
			  _asm {
			  push offset Util_MethodMap
			  push 211
			  jmp Method_Invoke
			  }
			  }
			  
				void __declspec(naked) _array::setAt(uint,void *)
				{
				_asm {
				push offset Util_MethodMap
				push 212
				jmp Method_Invoke
				}
				}
				
				  void __declspec(naked) _array::_indexOf(void *)
				  {
				  _asm {
				  push offset Util_MethodMap
				  push 213
				  jmp Method_Invoke
				  }
				  }
				  
					void __declspec(naked) _array::_find(void *)
					{
					_asm {
					push offset Util_MethodMap
					push 214
					jmp Method_Invoke
					}
					}
					
					  void __declspec(naked) _array::_remove(void)
					  {
					  _asm {
					  push offset Util_MethodMap
					  push 215
					  jmp Method_Invoke
					  }
					  }
					  
						void __declspec(naked) _array::_insert(void *)
						{
						_asm {
						push offset Util_MethodMap
						push 216
						jmp Method_Invoke
						}
						}
						
						  void __declspec(naked) _array::empty(void)
						  {
						  _asm {
						  push offset Util_MethodMap
						  push 217
						  jmp Method_Invoke
						  }
						  }
						  
							void __declspec(naked) _array::~_array(void)
							{
							_asm {
							push offset Util_MethodMap
							push 218
							jmp Method_Invoke
							}
							}
							
							  void __declspec(naked) _array::_array(int)
							  {
							  _asm {
							  push offset Util_MethodMap
							  push 219
							  jmp Method_Invoke
							  }
							  }
							  
								void __declspec(naked) _list::swap(void)
								{
								_asm {
								push offset Util_MethodMap
								push 220
								jmp Method_Invoke
								}
								}
								
								  void __declspec(naked) _list::_prev(void)
								  {
								  _asm {
								  push offset Util_MethodMap
								  push 221
								  jmp Method_Invoke
								  }
								  }
								  
									void __declspec(naked) _list::_last(void)
									{
									_asm {
									push offset Util_MethodMap
									push 222
									jmp Method_Invoke
									}
									}
									
									  void __declspec(naked) _list::_find(link *)
									  {
									  _asm {
									  push offset Util_MethodMap
									  push 223
									  jmp Method_Invoke
									  }
									  }
									  
										void __declspec(naked) _list::_indexOf(void *)
										{
										_asm {
										push offset Util_MethodMap
										push 224
										jmp Method_Invoke
										}
										}
										
										  void __declspec(naked) _list::_elem(uint)
										  {
										  _asm {
										  push offset Util_MethodMap
										  push 225
										  jmp Method_Invoke
										  }
										  }
										  
											void __declspec(naked) _list::_remove(void)
											{
											_asm {
											push offset Util_MethodMap
											push 226
											jmp Method_Invoke
											}
											}
											
											  void __declspec(naked) _list::_append(link *)
											  {
											  _asm {
											  push offset Util_MethodMap
											  push 227
											  jmp Method_Invoke
											  }
											  }
											  
												void __declspec(naked) _list::_insert(link *)
												{
												_asm {
												push offset Util_MethodMap
												push 228
												jmp Method_Invoke
												}
												}
												
												  void __declspec(naked) _list::empty(void)
												  {
												  _asm {
												  push offset Util_MethodMap
												  push 229
												  jmp Method_Invoke
												  }
												  }
												  
													void __declspec(naked) _list::size(void)
													{
													_asm {
													push offset Util_MethodMap
													push 230
													jmp Method_Invoke
													}
													}
													
													  void __declspec(naked) List::UnhookFromList(ListElement *)
													  {
													  _asm {
													  push offset Util_MethodMap
													  push 231
													  jmp Method_Invoke
													  }
													  }
													  
														void __declspec(naked) List::DeleteByPointer(ListElement *)
														{
														_asm {
														push offset Util_MethodMap
														push 232
														jmp Method_Invoke
														}
														}
														
														  void __declspec(naked) List::Add(ListElement *)
														  {
														  _asm {
														  push offset Util_MethodMap
														  push 233
														  jmp Method_Invoke
														  }
														  }
														  
															void __declspec(naked) List::Insert(ListElement *)
															{
															_asm {
															push offset Util_MethodMap
															push 234
															jmp Method_Invoke
															}
															}
															
															  void __declspec(naked) List::List(void)
															  {
															  _asm {
															  push offset Util_MethodMap
															  push 235
															  jmp Method_Invoke
															  }
															  }
															  
																void __declspec(naked) List::GetByPos(uint)
																{
																_asm {
																push offset Util_MethodMap
																push 236
																jmp Method_Invoke
																}
																}
																
																  void __declspec(naked) List::Free(void)
																  {
																  _asm {
																  push offset Util_MethodMap
																  push 237
																  jmp Method_Invoke
																  }
																  }
																  
																	void __declspec(naked) List::InsertAfter(ListElement *,ListElement *)
																	{
																	_asm {
																	push offset Util_MethodMap
																	push 238
																	jmp Method_Invoke
																	}
																	}
																	
																	  void __declspec(naked) List::GetNext(ListElement const *)
																	  {
																	  _asm {
																	  push offset Util_MethodMap
																	  push 239
																	  jmp Method_Invoke
																	  }
																	  }
																	  
																		void __declspec(naked) List::GetNext(ListElement *)
																		{
																		_asm {
																		push offset Util_MethodMap
																		push 240
																		jmp Method_Invoke
																		}
																		}
																		
																		  void __declspec(naked) List::GetPrev(ListElement const *)
																		  {
																		  _asm {
																		  push offset Util_MethodMap
																		  push 241
																		  jmp Method_Invoke
																		  }
																		  }
																		  
																			void __declspec(naked) List::GetPrev(ListElement *)
																			{
																			_asm {
																			push offset Util_MethodMap
																			push 242
																			jmp Method_Invoke
																			}
																			}
																			
																			  void __declspec(naked) List::~List(void)
																			  {
																			  _asm {
																			  push offset Util_MethodMap
																			  push 243
																			  jmp Method_Invoke
																			  }
																			  }
																			  
																				void __declspec(naked) List::InsertBefore(ListElement *,ListElement *)
																				{
																				_asm {
																				push offset Util_MethodMap
																				push 244
																				jmp Method_Invoke
																				}
																				}
																				
																				  void __declspec(naked) CLineHelper::SetColor(uint)
																				  {
																				  _asm {
																				  push offset Util_MethodMap
																				  push 245
																				  jmp Method_Invoke
																				  }
																				  }
																				  
																					void __declspec(naked) CLineHelper::AddSegments(CLineHelper::LineSegment *,int)
																					{
																					_asm {
																					push offset Util_MethodMap
																					push 246
																					jmp Method_Invoke
																					}
																					}
																					
																					  void __declspec(naked) CLineHelper::Draw(void)
																					  {
																					  _asm {
																					  push offset Util_MethodMap
																					  push 247
																					  jmp Method_Invoke
																					  }
																					  }
																					  
																						void __declspec(naked) CLineHelper::Enable(int)
																						{
																						_asm {
																						push offset Util_MethodMap
																						push 248
																						jmp Method_Invoke
																						}
																						}
																						
																						  void __declspec(naked) CLineHelper::~CLineHelper(void)
																						  {
																						  _asm {
																						  push offset Util_MethodMap
																						  push 249
																						  jmp Method_Invoke
																						  }
																						  }
																						  
																							void __declspec(naked) CLineHelper::PostRadsortHandler(uint,void *)
																							{
																							_asm {
																							push offset Util_MethodMap
																							push 250
																							jmp Method_Invoke
																							}
																							}
																							
																							  void __declspec(naked) CLineHelper::ChainMessageHandler(uint,void *)
																							  {
																							  _asm {
																							  push offset Util_MethodMap
																							  push 251
																							  jmp Method_Invoke
																							  }
																							  }
																							  
																								void __declspec(naked) CLineHelper::CLineHelper(void)
																								{
																								_asm {
																								push offset Util_MethodMap
																								push 252
																								jmp Method_Invoke
																								}
																								}
																								
																								  void __declspec(naked) CPlatform::RemoveFromFrameList(void)
																								  {
																								  _asm {
																								  push offset Util_MethodMap
																								  push 253
																								  jmp Method_Invoke
																								  }
																								  }
																								  
																									void __declspec(naked) CPlatform::UnregisterFromSimulation(void)
																									{
																									_asm {
																									push offset Util_MethodMap
																									push 254
																									jmp Method_Invoke
																									}
																									}
																									
																									  void __declspec(naked) CPlatform::PointOnPlaneInPlatformPolygon(XYZF64 const *)
																									  {
																									  _asm {
																									  push offset Util_MethodMap
																									  push 255
																									  jmp Method_Invoke
																									  }
																									  }
																									  
																										void __declspec(naked) CPlatform::ProjectToA2DPrimaryPlane(void)
																										{
																										_asm {
																										push offset Util_MethodMap
																										push 256
																										jmp Method_Invoke
																										}
																										}
																										
																										  void __declspec(naked) CPlatform::RotateToBodyAxis(XYZF64 *)
																										  {
																										  _asm {
																										  push offset Util_MethodMap
																										  push 257
																										  jmp Method_Invoke
																										  }
																										  }
																										  
																											void __declspec(naked) CPlatform::RotateToWorldAxis(XYZF64 *)
																											{
																											_asm {
																											push offset Util_MethodMap
																											push 258
																											jmp Method_Invoke
																											}
																											}
																											
																											  void __declspec(naked) CPlatform::SetLLAPBH(LATLONALT const *,PBH32 const *)
																											  {
																											  _asm {
																											  push offset Util_MethodMap
																											  push 259
																											  jmp Method_Invoke
																											  }
																											  }
																											  
																												void __declspec(naked) CPlatform::SetLLAPBH(LATLONALTPBH const *)
																												{
																												_asm {
																												push offset Util_MethodMap
																												push 260
																												jmp Method_Invoke
																												}
																												}
																												
																												  void __declspec(naked) CPlatform::ReleaseResources(void)
																												  {
																												  _asm {
																												  push offset Util_MethodMap
																												  push 261
																												  jmp Method_Invoke
																												  }
																												  }
																												  
																													void __declspec(naked) CPlatform::CPlatform(void)
																													{
																													_asm {
																													push offset Util_MethodMap
																													push 262
																													jmp Method_Invoke
																													}
																													}
																													
																													  void __declspec(naked) CPlatform::CalcPlatformConstants(void)
																													  {
																													  _asm {
																													  push offset Util_MethodMap
																													  push 263
																													  jmp Method_Invoke
																													  }
																													  }
																													  
																														void __declspec(naked) CPlatform::RecalcRotationMatrix(void)
																														{
																														_asm {
																														push offset Util_MethodMap
																														push 264
																														jmp Method_Invoke
																														}
																														}
																														
																														  void __declspec(naked) CPlatform::~CPlatform(void)
																														  {
																														  _asm {
																														  push offset Util_MethodMap
																														  push 265
																														  jmp Method_Invoke
																														  }
																														  }
																														  
																															void __declspec(naked) CPlatform::InitPlatform(LATLONALTPBH const *,XYZF32 const *,int,SURFACE_TYPE,CABLE_INFO_M const *,MATRIXF32 const * const)
																															{
																															_asm {
																															push offset Util_MethodMap
																															push 266
																															jmp Method_Invoke
																															}
																															}
																															
																															  void __declspec(naked) CPlatform::GetCableInfo(LATLONALT const *,_CABLE_INFO *)
																															  {
																															  _asm {
																															  push offset Util_MethodMap
																															  push 267
																															  jmp Method_Invoke
																															  }
																															  }
																															  
																																void __declspec(naked) CPlatform::GetPlatformInfo(LATLONALT const *,_SURFACE_INFO *,uint)
																																{
																																_asm {
																																push offset Util_MethodMap
																																push 268
																																jmp Method_Invoke
																																}
																																}
																																
																																  void __declspec(naked) CPlatform::staticPlatformCallback(LATLONALT const *,_SURFACE_INFO *,uint,void *)
																																  {
																																  _asm {
																																  push offset Util_MethodMap
																																  push 269
																																  jmp Method_Invoke
																																  }
																																  }
																																  
																																	void __declspec(naked) CPlatform::AddToFrameList(void)
																																	{
																																	_asm {
																																	push offset Util_MethodMap
																																	push 270
																																	jmp Method_Invoke
																																	}
																																	}
																																	
																																	  void __declspec(naked) CPlatform::RegisterWithSimulation(void)
																																	  {
																																	  _asm {
																																	  push offset Util_MethodMap
																																	  push 271
																																	  jmp Method_Invoke
																																	  }
} */

int __declspec(naked) Segments::FindIntersectionPoint(LLA const &,LLA const &,LLA const &,LLA const &,LLA &)
{
	_asm {
		push offset Util_MethodMap
			push 272
			jmp Method_Invoke
	}
}

LLA __declspec(naked)__stdcall Segments::ExtendSegment(LLA const &,LLA const &)
{
	_asm {
		push offset Util_MethodMap
			push 273
			jmp Method_Invoke
	}
}

int __declspec(naked) __stdcall Segments::VectorsIntersect(LLA const &,LLA const &,LLA const &,LLA const &,LLA &)
{
	_asm {
		push offset Util_MethodMap
			push 274
			jmp Method_Invoke
	}
}

void __declspec(naked)__stdcall Segments::SegmentsIntersect(LLA const &,LLA const &,LLA const &,LLA const &)
{
	_asm {
		push offset Util_MethodMap
			push 275
			jmp Method_Invoke
	}
}





/*
void __declspec(naked) const ListElement::`vftable'
{
_asm {
push offset Util_MethodMap
push 282
jmp Method_Invoke
}
}
*/
/*
void __declspec(naked) const CLineHelper::`vftable'
{
_asm {
push offset Util_MethodMap
push 283
jmp Method_Invoke
}
}
*/

/*
void __declspec(naked) List * CLineHelper::_s_pLinesList
{
_asm {
push offset Util_MethodMap
push 286
jmp Method_Invoke
}
}	*/


double * QuadMeshID::m_dUVtoQMCScale=0;
unsigned long * QuadMeshID::m_dwLimitU=0;
double const * QuadMeshID::m_dOOLimitU=0;
unsigned long * QuadMeshID::m_dwLimitV=0;
double * QuadMeshID::m_dOOLimitV=0;
double QuadMeshCoord::MAX_LATTITUDE_DELTA=0;
double QuadMeshCoord::MAX_LONGITUDE_DELTA=0;

void __declspec(naked) Util_init()
{_asm{
	pushad
	mov ecx,IsInitialized
	dec ecx
	jcxz done
	call Util_init_engine
done:
	popad
	ret 
}}

static void Util_init_engine()
{
	unsigned int index=0;
	HMODULE hUtil=GetModuleHandle("util.dll");
	if(!hUtil)
	{
		
#ifndef NO_SIM_LOADED
		MessageBox(NULL,"INIT:Util.dll is not loaded yet exiting.Add <extern void Util_init();Util_init();> to your ModuleInit()","Warning",MB_TOPMOST);
		ExitProcess(0);
#endif
		if(!(hUtil=LoadLibrary("f:\\msfs\\modules\\util.dll")))
		{
			MessageBox(NULL,"INIT:Correct %FS%\\modules path in Util_classes.cpp","Error",MB_TOPMOST);
			ExitProcess(0);
		}
		
	}
	for(;index<sizeof(UtilExptsNames)/sizeof(char*);index++)
	{
		if(!(Util_MethodMap[index]=(unsigned long)GetProcAddress(hUtil,UtilExptsNames[index])))
		{
			MessageBox(NULL,"INIT:Bad util.dll. Exiting","FATAL",MB_TOPMOST);
			ExitProcess(0);//No method in util.dll. It's fatal.
		}
	}
	// static data init
	QuadMeshID::m_dUVtoQMCScale=(double*)GetProcAddress(hUtil,"?m_dUVtoQMCScale@QuadMeshID@@1QBNB");
	QuadMeshID::m_dwLimitU=(unsigned long*)GetProcAddress(hUtil,"?m_dwLimitU@QuadMeshID@@1QBKB");
	QuadMeshID::m_dOOLimitU=(double*)GetProcAddress(hUtil,"?m_dOOLimitU@QuadMeshID@@1QBNB");
	QuadMeshID::m_dwLimitV=(unsigned long*)GetProcAddress(hUtil,"?m_dwLimitV@QuadMeshID@@1QBKB");
	QuadMeshID::m_dOOLimitV=(double *)GetProcAddress(hUtil,"?m_dOOLimitV@QuadMeshID@@1QBNB");
	
	QuadMeshCoord::MAX_LATTITUDE_DELTA=*(unsigned int*)GetProcAddress(hUtil,"?MAX_LATTITUDE_DELTA@QuadMeshCoord@@2NB");
	QuadMeshCoord::MAX_LONGITUDE_DELTA=*(double*)GetProcAddress(hUtil,"?MAX_LONGTITUDE_DELTA@QuadMeshCoord@@2NB");
	
	IsInitialized=1;
};

