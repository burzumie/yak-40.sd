/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: util.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_UTIL
#define _FS_SDK_UTIL

#include "util_classes.h"
#include "..\\common\\fs_linkage.h"

#pragma pack(4)

enum ASCII_PARSE_ERROR_TYPE {
	ASCII_PARSE_NO_ERROR = 0x0,
	ASCII_PARSE_INVALID_CONVERSION = 0x1,
	ASCII_PARSE_NULL_DESTINATION = 0x2,
	ASCII_PARSE_INVALID_MIN_MAX_RANGE = 0x3,
	ASCII_PARSE_INVALID_CHARACTER = 0x4,
	ASCII_PARSE_VALUE_TOO_SMALL = 0x5,
	ASCII_PARSE_VALUE_TOO_LARGE = 0x6,
	ASCII_PARSE_DECIMAL_NOT_ALLOWED = 0x7,
	ASCII_PARSE_INVALID_SYNTAX = 0x8,
	ASCII_PARSE_NOT_A_NUMBER = 0x9,
	ASCII_PARSE_NON_INTEGER = 0xa,
	ASCII_PARSE_MAGNITUDE_TOO_LARGE = 0xb,
	ASCII_PARSE_MAGNITUDE_TOO_SMALL = 0xc,
	ASCII_PARSE_LOSS_OF_PRECISION_WARNING = 0xd,
	ASCII_PARSE_NEGATIVE_NUMBER = 0xe,
	ASCII_PARSE_INVALID_TRANSPONDER_FREQUENCY = 0xf,
	ASCII_PARSE_INVALID_HEMISPHERE_DESIGNATOR = 0x10,
};

typedef struct DIRSCAN {
    char filename[260];
    unsigned int filetype;
    unsigned int filesizeLow;
    unsigned int filesizeHigh;
    struct DATETIME filetimeModified;
}DIRSCAN,*PDIRSCAN,**PPDIRSCAN;

struct UTIL {
	FSLINKAGE  head;
    unsigned int	(FSAPI *sqrt_64)(struct VAR64*);
    unsigned short  (FSAPI *sqrt_32)(unsigned int);
    unsigned char	(FSAPI *sqrt_16)(unsigned short);
    unsigned int	(FSAPI *hypot32)(int, int);
    void			(FSAPI *sincos16)(short, short*, short*);
    void			(FSAPI *rotate_2d)(int, int, short, short, int*, int*);
    unsigned short  (FSAPI *arcs36)(short, short);
    unsigned short  (FSAPI *arc_tangent32)(int, int);
    void			(FSAPI *meters32_to_lon48)(int*, union SIF48*, union ANGL48*);
    void			(FSAPI *meters48_to_lon48)(union SIF48*, union SIF48*, union ANGL48*);
    void			(FSAPI *lon48_to_meters48)(union ANGL48*, union SIF48*, union SIF48*);
    void			(FSAPI *lat_lon_alt_24_to_48)(struct LLA2416*, struct LATLONALT*);
    double			(FSAPI *LatCos)(double);
    double			(FSAPI *DmetersFromDdegreesLongitude)(double, double);
    double			(FSAPI *DdegreesLongitudeFromDmeters)(double, double);
    void			(FSAPI *AddMetersToLatLon)(struct _llf64*, struct XYF64*, struct _llf64*);
    void			(FSAPI *xyf64FromLatLons)(struct _llf64*, struct _llf64*, struct XYF64*);
    double			(FSAPI *NormalizeAngleRadians)(double);
    void			(FSAPI *latlonalt_distance_direction)(struct LATLONALT*, struct LATLONALT*, unsigned char, double*, double*);
    unsigned int	(FSAPI *ran32)();
    void			(FSAPI *latlonalt_add)(struct LATLONALT*, struct LATLONALT*, struct LATLONALT*);
    void			(FSAPI *latlonalt_sub)(struct LATLONALT*, struct LATLONALT*, struct LATLONALT*);
    void			(FSAPI *latlonalt_mov)(struct LATLONALT*, struct LATLONALT*);
    int				(FSAPI *latlonalt_cmp)(struct LATLONALT*, struct LATLONALT*);
    void			(FSAPI *sar48)(union SIF48*, union SIF48*, unsigned int);
    unsigned int	(FSAPI *BCD32_from_BCD16)(unsigned short);
    unsigned short  (FSAPI *BCD16_from_BCD32)(unsigned int);
    void			(FSAPI *util_dir_scan)(struct DIRSCAN**, unsigned int*, char*, char*, enum FILETYPE, int, int);
    int				(FSAPI *directory_exists)(char*);
    unsigned int	(FSAPI *show_alert)(int, int, void*, void*, void*, void*, void*, int);
    void			(FSAPI *show_message_start)(void**, int, void*, void*, void*, void*, void*, int);
    void			(FSAPI *show_message_stop)(void**);
    double			(FSAPI *random)();
    void			(FSAPI *unsigned_double_to_48_bit)(double*, void*);
    void			(FSAPI *unsigned_double_from_48_bit)(double*, void*);
    void			(FSAPI *signed_double_to_48_bit)(double*, void*);
    void			(FSAPI *signed_double_from_48_bit)(double*, void*);
    void			(FSAPI *ll_to_anglf64)(struct LATLON*, double*, double*);
    double			(FSAPI *lla_distance_get)(struct LATLONALT*, struct LATLONALT*);
    void			(FSAPI *lla_to_xyzf64)(struct LATLONALT*, struct LATLONALT*, struct XYZF64*);
    void			(FSAPI *lla_vector_get)(struct XYZF64*, double*, struct LATLONALT*, struct LATLONALT*);
    void			(FSAPI *float_lla_vector_get)(struct XYZF64*, double*, struct _llaf64*, struct _llaf64*);
    void			(FSAPI *LLAtoLLAFloat)(struct LATLONALT*, struct _latlonalt_float32*);
    void			(FSAPI *LLAFloattoLLA)(struct _latlonalt_float32*, struct LATLONALT*);
    void			(FSAPI *LLAF64_to_LATLONALT)(struct _llaf64*, struct LATLONALT*);
    void			(FSAPI *LATLONALT_to_LLAF64)(struct LATLONALT*, struct _llaf64*);
    void			(FSAPI *xyzf64_to_lla)(struct XYZF64*, struct LATLONALT*, struct LATLONALT*);
    void			(FSAPI *xyzf64_to_pbhf64)(struct XYZF64*, struct PBHF64*);
    void			(FSAPI *xyzf64_to_rotated)(struct XYZF64*, struct MATRIXF32*, struct XYZF64*);
    void			(FSAPI *rotated_to_xyzf64)(struct XYZF64*, struct MATRIXF32*, struct XYZF64*);
    void			(FSAPI *pbh16_to_pbh32)(struct PBH16*, struct PBH32*);
    void			(FSAPI *pbh32_to_pbh16)(struct PBH32*, struct PBH16*);
    void			(FSAPI *pbh32_to_pbhf64)(struct PBH32*, struct PBHF64*);
    void			(FSAPI *pbhf64_to_pbh32)(struct PBHF64*, struct PBH32*);
    void			(FSAPI *pbhf64_to_rotation_matrix)(struct MATRIXF32*, struct PBHF64*);
    void			(FSAPI *pbh32_to_xyzf64)(struct PBH32*, struct XYZF64*);
    void			(FSAPI *rotate_vector_body_axis_to_world)(double*, double*, double*, struct MATRIXF32*);
    void			(FSAPI *rotate_vector_world_to_body_axis)(double*, double*, double*, struct MATRIXF32*);
    void			(FSAPI *BodyToWorldRotationalVelocity)(struct XYZF64*, struct XYZF64*, double, double, double, double);
    void			(FSAPI *WorldToBodyRotationalVelocity)(struct XYZF64*, struct XYZF64*, double, double, double, double);
    void			(FSAPI *matrix_product_4x4)(struct MATRIXF32*, struct MATRIXF32*, struct MATRIXF32*);
    void			(FSAPI *matrix_product_4x3)(struct MATRIXF32*, struct MATRIXF32*, struct MATRIXF32*);
    void			(FSAPI *matrix_product_3x3)(struct MATRIXF32*, struct MATRIXF32*, struct MATRIXF32*);
    void			(FSAPI *matrix_rotate)(struct MATRIXF32*, double, double, double);
    void			(FSAPI *matrix_rotate_pbh32)(struct MATRIXF32*, struct PBH32*);
    void			(FSAPI *matrix_rotate_pbh64)(struct MATRIXF32*, struct PBHF64*);
    void			(FSAPI *matrix_extract_pbh)(struct MATRIXF32*, struct PBHF64*);
    void			(FSAPI *matrix_inverse)(struct MATRIXF32*, struct MATRIXF32*);
    void			(FSAPI *matrix_translate)(struct MATRIXF32*, double, double, double);
    void			(FSAPI *matrix_scale)(struct MATRIXF32*, double, double, double);
    void			(FSAPI *matrix_identity)(struct MATRIXF32*);
    void			(FSAPI *matrix_transpose)(struct MATRIXF32*, struct MATRIXF32*);
    void			(FSAPI *create_rotation_matrix_pbh32)(struct MATRIXF32*, struct PBH32*);
    void			(FSAPI *vector_add)(struct XYZWF32*, struct XYZWF32*, struct XYZWF32*);
    void			(FSAPI *vector_sub)(struct XYZWF32*, struct XYZWF32*, struct XYZWF32*);
    void			(FSAPI *vector_scalar_mul)(struct XYZWF32*, struct XYZWF32*, float);
    float			(FSAPI *vector_magnitude)(struct XYZWF32*);
    float			(FSAPI *vector_square_magnitude)(struct XYZWF32*);
    void			(FSAPI *vector_normalize)(struct XYZWF32*, struct XYZWF32*);
    float			(FSAPI *vector_dot_product)(struct XYZWF32*, struct XYZWF32*);
    void			(FSAPI *vector_cross_product)(struct XYZWF32*, struct XYZWF32*, struct XYZWF32*);
    double			(FSAPI *xyzf64_magnitude)(struct XYZF64*);
    void			(FSAPI *vector_transform)(struct XYZWF32*, struct XYZWF32*, struct MATRIXF32*, unsigned int);
    void			(FSAPI *vector_rotate)(struct XYZWF32*, struct XYZWF32*, struct MATRIXF32*, unsigned int);
    void			(FSAPI *vector_inverse_transform)(struct XYZWF32*, struct XYZWF32*, struct MATRIXF32*, unsigned int);
    void			(FSAPI *vector_inverse_rotate)(struct XYZWF32*, struct XYZWF32*, struct MATRIXF32*, unsigned int);
    void			(FSAPI *vector_transform_xyz64)(struct XYZF64*, struct XYZF64*, struct MATRIXF32*, unsigned int);
    double			(FSAPI *InterpolateValue)(double, double, double, double, double);
    double			(FSAPI *InterpolateAndLimitValue)(double, double, double, double, double);
    int				(FSAPI *GetRandomNumber)(int, int);
    double			(FSAPI *GetDistanceSquared)(struct LATLONALT*, struct LATLONALT*);
    double			(FSAPI *GetDistance)(struct LATLONALT*, struct LATLONALT*);
    void*			(FSAPI *memcpy_fast)(void*, void*, unsigned int);
    void			(FSAPI *great_circle_heading_distance_degrees_meters)(struct _llf64*, struct _llf64*, double*, double*);
    void			(FSAPI *great_circle_heading_distance_Meter)(struct LATLON*, struct LATLON*, double*, double*);
    void			(FSAPI *great_circle_heading_distance_Nm)(struct LATLON*, struct LATLON*, double*, double*);
    void			(FSAPI *great_circle_interpolate)(struct LATLON*, struct LATLON*, int, double, struct LATLON*);
    void			(FSAPI *great_circle_extrapolate_degrees_meters)(struct _llf64*, double, double, struct _llf64*);
    double			(FSAPI *great_circle_cross_track_degrees_meters)(struct _llf64*, double, struct _llf64*);
    double			(FSAPI *great_circle_cross_track_line_degrees_meters)(struct _llf64*, struct _llf64*, struct _llf64*);
    double			(FSAPI *great_circle_cross_track_arc_degrees_meters)(struct _llf64*, struct _llf64*, int, struct _llf64*);
    int				(FSAPI *great_circle_intersect_radials)(struct _llf64*, double, struct _llf64*, double, struct _llf64*);
    void			(FSAPI *great_circle_trace_line)(struct _llf64*, struct _llf64*, double, int  (*)(void*, struct _llf64*, struct _llf64*), void*);
    double			(FSAPI *cross_track_compute)(struct LATLONALT*, struct LATLONALT*, struct LATLONALT*);
    unsigned int	(FSAPI *config_items_read)(char*, struct CONFIG_ITEM*, void*, char*, enum FILETYPE);
    unsigned int	(FSAPI *config_items_write)(char*, struct CONFIG_ITEM*, void*, char*, enum FILETYPE);
    unsigned int	(FSAPI *config_item_read)(char*, struct CONFIG_ITEM*, void*, char*, enum FILETYPE);
    unsigned int	(FSAPI *config_item_write)(char*, struct CONFIG_ITEM*, void*, char*, enum FILETYPE);
    unsigned int	(FSAPI *config_read)(char*, char*, void*, enum CONFIG_ITEM_TYPE, union CONFIG_ITEM_TYPE_DATA, char*, enum FILETYPE);
    unsigned int	(FSAPI *config_write)(char*, char*, void*, enum CONFIG_ITEM_TYPE, union CONFIG_ITEM_TYPE_DATA, char*, enum FILETYPE);
    void*			(FSAPI *compute_var_address)(struct CONFIG_ITEM*, unsigned int);
    unsigned int	(FSAPI *config_item_lookup_code)(struct LOOKUP_TABLE*, char*);
    char*			(FSAPI *config_item_lookup_text)(struct LOOKUP_TABLE*, unsigned int);
    void			(FSAPI *config_item_lookup)(struct LOOKUP_TABLE*, unsigned int*, char*, int);
    unsigned int	(FSAPI *crc32)(void*, unsigned int);
    unsigned int	(FSAPI *compress_method_1)(void*, unsigned int, void*, unsigned int);
    unsigned int	(FSAPI *compress_method_2)(void*, unsigned int, void*, unsigned int);
    unsigned int	(FSAPI *decompress_method_1)(void*, unsigned int, void*, unsigned int);
    unsigned int	(FSAPI *decompress_method_2)(void*, unsigned int, void*, unsigned int);
    unsigned int	(FSAPI *ascii_to_morse)(char*, char*, unsigned int);
    double			(FSAPI *AddAngleMax360)(double, double);
    double			(FSAPI *AddAngleMax180)(double, double);
    double			(FSAPI *AddAngleMaxTwoPI)(double, double);
    double			(FSAPI *AddAngleMaxPI)(double, double);
    double			(FSAPI *ConvertUnits)(double, int, int);
    double			(FSAPI *ConvertUnitsString)(double, char*, char*);
    int				(FSAPI *GetUnitsEnum)(char*);
    char*			(FSAPI *GetUnitsString)(int);
    int				(FSAPI *ConversionValid)(int, int);
    int*			(FSAPI *CreateAndShuffleIntArray)(int, unsigned int);
    char*			(FSAPI *binary_to_ascii)(void*, int, char*);
    enum ASCII_PARSE_ERROR_TYPE  (FSAPI *ascii_to_binary)(char*, int, void*);
    enum ASCII_PARSE_ERROR_TYPE  (FSAPI *ascii_to_binary_ranged)(char*, int, void*, void*);
};

#pragma pack()

#endif