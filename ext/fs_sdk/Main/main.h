/*=========================================================================

  FS SDK - Microsoft Flight Simulator 2004 Custom SDK
  
  Module:  $RCSfile: main.h,v $

  Last modification:
    Date:      $Date: 2005/08/06 14:06:50 $
    Version:   $Revision: 1.2 $
    Author:    $Author: except $
  
  Copyright (c) 2005 by Nick Sharmanzhinov [except]
  Copyright (c) 2005 by Andrew Egorovsky

  All rights reserved.

=========================================================================*/

#ifndef _FS_SDK_MAIN
#define _FS_SDK_MAIN

#pragma pack(4)

enum RISE_SET_EVENT {
	RISE_SET_SUNRISE = 0x0,
	RISE_SET_DAY = 0x1,
	RISE_SET_SUNSET = 0x2,
	RISE_SET_NIGHT = 0x3,
};

enum MAIN_LOADING_STATE {
	MAIN_LOADING_STATE_BEGIN = 0x0,
	MAIN_LOADING_STATE_BGL = 0x1,
	MAIN_LOADING_STATE_TERRAIN = 0x2,
	MAIN_LOADING_STATE_SCENEDB = 0x3,
	MAIN_LOADING_STATE_WEATHER = 0x4,
	MAIN_LOADING_STATE_ATC = 0x5,
	MAIN_LOADING_STATE_TRAFFIC = 0x6,
	MAIN_LOADING_STATE_COMPLETE = 0x7,
};

enum _moonPhase {
	MOONPHASE_NEWMOON = 0x0,
	MOONPHASE_WAXINGCRESCENT = 0x1,
	MOONPHASE_FIRSTQUARTER = 0x2,
	MOONPHASE_WAXINGGIBBOUS = 0x3,
	MOONPHASE_FULLMOON = 0x4,
	MOONPHASE_WANINGGIBBOUS = 0x5,
	MOONPHASE_LASTQUARTER = 0x6,
	MOONPHASE_WANINGCRESCENT = 0x7,
	MOONPHASE_MAX = 0x8,
};

enum InstrumentFailures {
	FAIL_ATTITUDE = 0x0,
	FAIL_DIRGYRO = 0x1,
	FAIL_VERTSPEED = 0x2,
	FAIL_ALTIMETER = 0x3,
	FAIL_AIRSPEED = 0x4,
	FAIL_TURNCOORD = 0x5,
	FAIL_INSTR_NUM = 0x6,
};

enum GLOBDATA {
	GLOBDATA_NONE = 0x0,
	GLOBDATA_ABSOLUTE_TIME = 0x1,
	GLOBDATA_ZULU_TIME = 0x2,
	GLOBDATA_ZULU_DAY_OF_WEEK = 0x3,
	GLOBDATA_ZULU_DAY_OF_MONTH = 0x4,
	GLOBDATA_ZULU_MONTH_OF_YEAR = 0x5,
	GLOBDATA_ZULU_DAY_OF_YEAR = 0x6,
	GLOBDATA_ZULU_YEAR = 0x7,
	GLOBDATA_LOCAL_TIME = 0x8,
	GLOBDATA_LOCAL_DAY_OF_WEEK = 0x9,
	GLOBDATA_LOCAL_DAY_OF_MONTH = 0xa,
	GLOBDATA_LOCAL_MONTH_OF_YEAR = 0xb,
	GLOBDATA_LOCAL_DAY_OF_YEAR = 0xc,
	GLOBDATA_LOCAL_YEAR = 0xd,
	GLOBDATA_TIME_ZONE_OFFSET = 0xe,
	GLOBDATA_TIME_OF_DAY = 0xf,
	GLOBDATA_TOOLTIP_UNITS = 0x10,
	GLOBDATA_SIMULATION_RATE = 0x11,
	GLOBDATA_UNITS_OF_MEASURE = 0x12,
	GLOBDATA_ACTIVE_VIEW_MODE = 0x13,
	GLOBDATA_ACTIVE_VIEW_DIR = 0x14,
	GLOBDATA_DEFAULT_VIEW_MODE = 0x15,
	GLOBDATA_DEFAULT_VIEW_DIR = 0x16,
	GLOBDATA_LAST = 0x17,
};

typedef struct CHECK_STRUCTURE {
    int lat;
    int lon;
    int alt;
    unsigned int lat_lon_delta;
    unsigned int alt_delta;
    void* action;
}CHECK_STRUCTURE,*PCHECK_STRUCTURE,**PPCHECK_STRUCTURE;

typedef struct _moonValues {
    struct XYZF64 vMoon;
    enum _moonPhase ePhase;
    double dRightAscension;
    double dDeclination;
}MOON_VALUES,*PMOON_VALUES,**PPMOON_VALUES;

typedef struct _SUNVALUES {
    struct XYZF64 vSun;
    double dDeclintation;
    double dGreenwichMeanHour;
    double dAltitude;
    double dAzimuth;
    float fDirectionalRed;
    float fDirectionalGreen;
    float fDirectionalBlue;
    float fAmbientRed;
    float fAmbientGreen;
    float fAmbientBlue;
    float fGroundAmbientRed;
    float fGroundAmbientGreen;
    float fGroundAmbientBlue;
    float fCloudDirectionalRed;
    float fCloudDirectionalGreen;
    float fCloudDirectionalBlue;
    float fCloudAmbientRed;
    float fCloudAmbientGreen;
    float fCloudAmbientBlue;
}SUNVALUES,*PSUNVALUES,**PPSUNVALUES;

typedef struct CLOUDLIGHTINGVALUES {
    float fAngle;
    float fMedianLine;
    float fMinIntensity;
    float fMedianIntensity;
}CLOUDLIGHTINGVALUES,*PCLOUDLIGHTINGVALUES,**PPCLOUDLIGHTINGVALUES;

typedef struct LIGHTCOLOR {
    float fAngle;
    float fDirectionalRed;
    float fDirectionalGreen;
    float fDirectionalBlue;
    float fAmbientRed;
    float fAmbientGreen;
    float fAmbientBlue;
}LIGHTCOLOR,*PLIGHTCOLOR,**PPLIGHTCOLOR;

typedef struct FAILURE_DATA {
}FAILURE_DATA,*PFAILURE_DATA,**PPFAILURE_DATA;

struct MAIN {
	FSLINKAGE	head;
    void		(FSAPI *executive)();
    void		(FSAPI *lat_limiter)();
    double		(FSAPI *GetFrameTimeSeconds)();
    short		(FSAPI *GetFrameTimeFraction)();
    int			(FSAPI *event_proc)(unsigned int, unsigned int, enum EVENT_SOURCE_TYPE);
    void		(FSAPI *timer_proc)();
    void		(FSAPI *fs_startup)(int);
    void		(FSAPI *fs_shutdown)();
    void		(FSAPI *menu_disable)();
    void		(FSAPI *menu_enable)();
    int			(FSAPI *startup_flight_load)(int);
    void		(FSAPI *main_begin_loading)();
    void		(FSAPI *main_set_loading_state)(enum MAIN_LOADING_STATE);
    enum MAIN_LOADING_STATE  (FSAPI *main_get_loading_state)();
    int			(FSAPI *main_is_loading)();
    int			(FSAPI *main_is_loading_complete)();
    int			(FSAPI *GetUpperFramerateLimit)();
    void		(FSAPI *SetUpperFramerateLimit)(int);
    int			(FSAPI *IsFramerateLocked)();
    void		(FSAPI *get_time_zone)(int, unsigned short);
    void		(FSAPI *check_for_movement)(struct CHECK_STRUCTURE*);
    void		(FSAPI *logbook_logtime_check)();
    void		(FSAPI *compute_light_vector)();
    void		(FSAPI *ComputeSunValues)();
    struct _moonValues*				(FSAPI *GetMoonValues)();
    struct _SUNVALUES*				(FSAPI *GetSunValues)();
    struct _SUNVALUES*				(FSAPI *GetLightValues)();
    struct CLOUDLIGHTINGVALUES*		(FSAPI *GetCloudLightingValues)();
    void			(FSAPI *main_SetLightingValues)(struct LIGHTCOLOR*, struct LIGHTCOLOR*, struct LIGHTCOLOR*, struct LIGHTCOLOR*, struct CLOUDLIGHTINGVALUES*, struct CLOUDLIGHTINGVALUES*);
    void			(FSAPI *ComputeSunriseSunsetTime)(struct DATETIME*, enum RISE_SET_EVENT, struct DATETIME*);
    unsigned int	(FSAPI *TimeOfDayFromDateAndTime)(struct DATETIME*, int*);
    void			(FSAPI *add_to_clock)(unsigned int);
    void			(FSAPI *efis_plot)();
    unsigned int	(FSAPI *ini_read)();
    unsigned int	(FSAPI *ini_write)();
    unsigned int	(FSAPI *ini_default)(int);
    void			(FSAPI *generate_axis)(struct AWIND*);
    char*			(FSAPI *current_date_time_string_get)();
    unsigned int	(FSAPI *DoSettingsDisplayDlg)();
    int				(FSAPI *DoTimeSeasonDlg)();
    unsigned int	(FSAPI *ABLFlightExitCodeGet)();
    unsigned int	(FSAPI *month_and_day_from_date)(unsigned short*, unsigned short*, unsigned char*, unsigned short*);
    unsigned int	(FSAPI *date_from_month_and_day)(unsigned short*, unsigned short*, unsigned char*, unsigned short*);
    unsigned int	(FSAPI *set_exact_zulu_date_time)(double);
    void			(FSAPI *fine_adjust_time)(double);
    void			(FSAPI *zulu_to_local)(unsigned short*, unsigned short*);
    void			(FSAPI *BlankInstrument)(enum InstrumentFailures, int);
    void			(FSAPI *FailInstrument)(enum InstrumentFailures, int);
    void			(FSAPI *ToggleGaugeState)(enum InstrumentFailures, unsigned int);
    int				(FSAPI *failures_dialog_process)();
    unsigned int	(FSAPI *send_failures_to_player)(unsigned int, struct FAILURE_DATA*, int);
    void			(FSAPI *GetModifiedFailures)(struct FAILURE_DATA**, int*);
    void			(FSAPI *Failures_SetFlagToSyncFailuresWithSim)(int);
    int				(FSAPI *Failure_UpdateFailureData)(struct FAILURE_DATA*, int);
    int				(FSAPI *get_time_zone_deviation)(struct LATLONALT*);
    void			(FSAPI *clock_from_zulu)(unsigned int, unsigned int, unsigned char*, unsigned char*);
    void			(FSAPI *local_to_zulu)(unsigned short, unsigned short, unsigned char, unsigned char);
    void			(FSAPI *ini_realism_read)(char*, char*, enum FILETYPE);
    void			(FSAPI *ini_realism_write)(char*, char*, enum FILETYPE);
    struct REALISM_VARS*  (FSAPI *GetGlobalRealism)();
    void			(FSAPI *SetGlobalRealism)(struct REALISM_VARS*);
    unsigned int	(FSAPI *GlobalDataGet)(enum GLOBDATA, int, double*);
    enum GLOBDATA	(FSAPI *String2GLOBDATA)(char*);
    char*			(FSAPI *GLOBDATA2String)(enum GLOBDATA);
};

#pragma pack()

#endif