/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Krm.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

enum KRM {
	// P0
	KRM_AGB_MAIN0					,	// "sd_6015_p0_201"
	KRM_AGB_AUXL					,	// "sd_6015_p0_202"
	KRM_RV3M                        ,	// "sd_6015_p0_203"
	KRM_KPPMS0                      ,	// "sd_6015_p0_204"
	KRM_UVID0                       ,	// "sd_6015_p0_205"
	
	// P1
	KRM_AGB_MAIN1					,	// "sd_6015_p1_201"
	KRM_UVID1                       ,	// "sd_6015_p1_202"
	KRM_KPPMS1                      ,	// "sd_6015_p1_203"

	//
	KRM_MAX							
};

