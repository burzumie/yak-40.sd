/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Btn.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Btn.h"
#include "ApiName.h"

#ifdef _DEBUG
#define sd_6015_pa_101	""                                
#define sd_6015_pa_102	""                                
#define sd_6015_pa_103	"MAN FIRE EXTNG ENG BAY1"         
#define sd_6015_pa_104	"MAN FIRE EXTNG ENG BAY2"         
#define sd_6015_pa_105	"MAN FIRE EXTNG ENG BAY3"         
#define sd_6015_pa_106	"MAN FIRE EXTNG ENG1"             
#define sd_6015_pa_107	"MAN FIRE EXTNG ENG2"             
#define sd_6015_pa_108	"MAN FIRE EXTNG ENG3"             
#define sd_6015_pa_109	"MAN FIRE EXTNG APU"              
#define sd_6015_pa_110	"LTS TEST"                        
#define sd_6015_pa_111	"LTS TEST"                        
															
 															
#define sd_6015_p0_101	"LTS TEST"                        
#define sd_6015_p0_102	"DIS FIRE ALARM HORN"             
#define sd_6015_p0_103	"DIS ALARM HYD PUMP 1 FAIL"       
#define sd_6015_p0_104	"DIS ALARM HYD PUMP 2 FAIL"       
#define sd_6015_p0_105	""                                
#define sd_6015_p0_106	""                                
#define sd_6015_p0_107	"ZERO FUEL TEST"                  
#define sd_6015_p0_108	"FULL FUEL TEST"                  
#define sd_6015_p0_109	"GFORCE RESET"                    
															
 															
#define sd_6015_p1_101	"LTS TEST"                        
#define sd_6015_p1_102	""                                
															
 															
#define sd_6015_p2_101	"ELEV TRIM MAIN"                  
#define sd_6015_p2_102	"ELEV TRIM EMERGENCY"             
#define sd_6015_p2_103	"AP DISCONNECT"                   
#define sd_6015_p2_104	"COM RADIO TRANSMIT"              
#define sd_6015_p2_105	"REMOVE YOKE PANEL"               
															
 															
#define sd_6015_p3_101	"ELEV TRIM MAIN"                  
#define sd_6015_p3_102	"ELEV TRIM EMERGENCY"             
#define sd_6015_p3_103	"AP DISCONNECT"                   
#define sd_6015_p3_104	"COM RADIO TRANSMIT"              
#define sd_6015_p3_105	"REMOVE YOKE PANEL"               
															
 															
#define sd_6015_p4_101	"LTS TEST"                        
#define sd_6015_p4_102	"X-FEED SYS TEST"                 
#define sd_6015_p4_103	"ENG1 MID-AIR START"              
#define sd_6015_p4_104	"ENG2 MID-AIR START"              
#define sd_6015_p4_105	"ENG3 MID-AIR START"              
#define sd_6015_p4_106	"EMERG GEAR RETR"                 
															
 															
#define sd_6015_p6_101	"2-WAY COMM TEST"                 
#define sd_6015_p6_102	"ID TRANSMIT"                     
#define sd_6015_p6_103	"SYS TEST-1"                      
#define sd_6015_p6_104	"SYS TEST-2"                      
#define sd_6015_p6_105	"SYS TEST-3"                      
#define sd_6015_p6_106	"APU START"                       
#define sd_6015_p6_107	"APU STOP"                        
#define sd_6015_p6_108	"ENG START"                       
#define sd_6015_p6_109	"ENG STOP"                        
#define sd_6015_p6_110	"NAV1 IDENT: %(V:OFF|ON)%"        
															
 															
#define sd_6015_p7_101	"NAV2 IDENT: %(V:OFF|ON)%"        
#define sd_6015_p7_102	"SYS TEST-1"                      
#define sd_6015_p7_103	"SYS TEST-2"                      
#define sd_6015_p7_104	"SYS TEST-3"                      
#define sd_6015_p7_105	"LTS TEST"                        

#else
#define sd_6015_pa_101	""
#define sd_6015_pa_102	""
#define sd_6015_pa_103	""
#define sd_6015_pa_104	""
#define sd_6015_pa_105	""
#define sd_6015_pa_106	""
#define sd_6015_pa_107	""
#define sd_6015_pa_108	""
#define sd_6015_pa_109	""
#define sd_6015_pa_110	""
#define sd_6015_pa_111	""
							
							
#define sd_6015_p0_101	""
#define sd_6015_p0_102	""
#define sd_6015_p0_103	""
#define sd_6015_p0_104	""
#define sd_6015_p0_105	""
#define sd_6015_p0_106	""
#define sd_6015_p0_107	""
#define sd_6015_p0_108	""
#define sd_6015_p0_109	""
							
							
#define sd_6015_p1_101	""
#define sd_6015_p1_102	""
							
							
#define sd_6015_p2_101	""
#define sd_6015_p2_102	""
#define sd_6015_p2_103	""
#define sd_6015_p2_104	""
#define sd_6015_p2_105	""
							
							
#define sd_6015_p3_101	""
#define sd_6015_p3_102	""
#define sd_6015_p3_103	""
#define sd_6015_p3_104	""
#define sd_6015_p3_105	""
							
							
#define sd_6015_p4_101	""
#define sd_6015_p4_102	""
#define sd_6015_p4_103	""
#define sd_6015_p4_104	""
#define sd_6015_p4_105	""
#define sd_6015_p4_106	""
							
							
#define sd_6015_p6_101	""
#define sd_6015_p6_102	""
#define sd_6015_p6_103	""
#define sd_6015_p6_104	""
#define sd_6015_p6_105	""
#define sd_6015_p6_106	""
#define sd_6015_p6_107	""
#define sd_6015_p6_108	""
#define sd_6015_p6_109	""
#define sd_6015_p6_110	""
							
							
#define sd_6015_p7_101	""
#define sd_6015_p7_102	""
#define sd_6015_p7_103	""
#define sd_6015_p7_104	""
#define sd_6015_p7_105	""

#endif

ApiName BtnNames[BTN_MAX]={
	// �����
	{"sd_6015_pa_101",sd_6015_pa_101,"",0,0,false,false},	// BTN_CLOCK_HRONO					
	{"sd_6015_pa_102",sd_6015_pa_102,"",0,0,false,false},	// BTN_CLOCK_START					
	{"sd_6015_pa_103",sd_6015_pa_103,"",0,0,false,false},	// BTN_MAN_FIRE_EXTNG_ENG_BAY1		
	{"sd_6015_pa_104",sd_6015_pa_104,"",0,0,false,false},	// BTN_MAN_FIRE_EXTNG_ENG_BAY2		
	{"sd_6015_pa_105",sd_6015_pa_105,"",0,0,false,false},	// BTN_MAN_FIRE_EXTNG_ENG_BAY3		
	{"sd_6015_pa_106",sd_6015_pa_106,"",0,0,false,false},	// BTN_MAN_FIRE_EXTNG_ENG1			
	{"sd_6015_pa_107",sd_6015_pa_107,"",0,0,false,false},	// BTN_MAN_FIRE_EXTNG_ENG2			
	{"sd_6015_pa_108",sd_6015_pa_108,"",0,0,false,false},	// BTN_MAN_FIRE_EXTNG_ENG3			
	{"sd_6015_pa_109",sd_6015_pa_109,"",0,0,false,false},	// BTN_MAN_FIRE_EXTNG_APU			
	{"sd_6015_pa_110",sd_6015_pa_110,"",0,0,false,false},	// BTN_FIRE_LIGHTS_TEST			
	{"sd_6015_pa_111",sd_6015_pa_111,"",0,0,false,false},	// BTN_GEAR_LIGHTS_TEST			
												 
	// P0										 
	{"sd_6015_p0_101",sd_6015_p0_101,"",0,0,false,false},	// BTN_LIGHTS_TEST					
	{"sd_6015_p0_102",sd_6015_p0_102,"",0,0,false,false},	// BTN_DIS_FIRE_ALARM				
	{"sd_6015_p0_103",sd_6015_p0_103,"",0,0,false,false},	// BTN_DIS_ALARM_HYD_PUMP_1_FAIL	
	{"sd_6015_p0_104",sd_6015_p0_104,"",0,0,false,false},	// BTN_DIS_ALARM_HYD_PUMP_2_FAIL	
	{"sd_6015_p0_105",sd_6015_p0_105,"",0,0,false,false},	// BTN_AGB_MAIN_ARRET				
	{"sd_6015_p0_106",sd_6015_p0_106,"",0,0,false,false},	// BTN_AGB_AUXL_ARRET				
	{"sd_6015_p0_107",sd_6015_p0_107,"",0,0,false,false},	// BTN_PPTIZ_TEST_0				
	{"sd_6015_p0_108",sd_6015_p0_108,"",0,0,false,false},	// BTN_PPTIZ_TEST_4000				
	{"sd_6015_p0_109",sd_6015_p0_109,"",0,0,false,false},	// BTN_GFORCE_RESET				
												 
	// P1										 
    {"sd_6015_p1_101",sd_6015_p1_101,"",0,0,false,false},	// BTN_LIGHTS_TEST   
    {"sd_6015_p1_102",sd_6015_p1_102,"",0,0,false,false},	// BTN_AGB_MAIN_ARRET
												 
	// P2										 
	{"sd_6015_p2_101",sd_6015_p2_101,"",0,0,false,false},	// BTN_TRIMMAIN 
	{"sd_6015_p2_102",sd_6015_p2_102,"",0,0,false,false},	// BTN_TRIMEMERG
	{"sd_6015_p2_103",sd_6015_p2_103,"",0,0,false,false},	// BTN_APOFF    
	{"sd_6015_p2_104",sd_6015_p2_104,"",0,0,false,false},	// BTN_COM      
	{"sd_6015_p2_105",sd_6015_p2_105,"",0,0,false,false},	// BTN_CLOSE
												 
	// P3										 
	{"sd_6015_p3_101",sd_6015_p3_101,"",0,0,false,false},	// BTN_TRIMMAIN 
	{"sd_6015_p3_102",sd_6015_p3_102,"",0,0,false,false},	// BTN_TRIMEMERG
	{"sd_6015_p3_103",sd_6015_p3_103,"",0,0,false,false},	// BTN_APOFF    
	{"sd_6015_p3_104",sd_6015_p3_104,"",0,0,false,false},	// BTN_COM      
	{"sd_6015_p3_105",sd_6015_p3_105,"",0,0,false,false},	// BTN_CLOSE
												 
	// P4										 
	{"sd_6015_p4_101",sd_6015_p4_101,"",0,0,false,false},	// BTN_CONTRLAMP
	{"sd_6015_p4_102",sd_6015_p4_102,"",0,0,false,false},	// BTN_ACT_TEST
	{"sd_6015_p4_103",sd_6015_p4_103,"",0,0,false,false},	// BTN_ENGSTART1
	{"sd_6015_p4_104",sd_6015_p4_104,"",0,0,false,false},	// BTN_ENGSTART2
	{"sd_6015_p4_105",sd_6015_p4_105,"",0,0,false,false},	// BTN_ENGSTART3
	{"sd_6015_p4_106",sd_6015_p4_106,"",0,0,false,false},	// BTN_SHASSIAVARUBOR
												 
	// P6										 
	{"sd_6015_p6_101",sd_6015_p6_101,"",0,0,false,false},	// BTN_SO72CONTROL	
	{"sd_6015_p6_102",sd_6015_p6_102,"",0,0,false,false},	// BTN_SO72ID		
	{"sd_6015_p6_103",sd_6015_p6_103,"",0,0,false,false},	// BTN_KMP1TEST1	
	{"sd_6015_p6_104",sd_6015_p6_104,"",0,0,false,false},	// BTN_KMP1TEST2	
	{"sd_6015_p6_105",sd_6015_p6_105,"",0,0,false,false},	// BTN_KMP1TEST3	
	{"sd_6015_p6_106",sd_6015_p6_106,"",0,0,false,false},	// BTN_STARTAI9	
	{"sd_6015_p6_107",sd_6015_p6_107,"",0,0,false,false},	// BTN_STOPAI9		
	{"sd_6015_p6_108",sd_6015_p6_108,"",0,0,false,false},	// BTN_STARTAI25	
	{"sd_6015_p6_109",sd_6015_p6_109,"",0,0,false,false},	// BTN_STOPAI25	
	{"sd_6015_p6_110",sd_6015_p6_110,"",0,0,false,false},	// BTN_KMP1ID		
												 
	// P7										 
	{"sd_6015_p7_101",sd_6015_p7_101,"",0,0,false,false},	// BTN_KMP2ID		
	{"sd_6015_p7_102",sd_6015_p7_102,"",0,0,false,false},	// BTN_KMP2TEST1	
	{"sd_6015_p7_103",sd_6015_p7_103,"",0,0,false,false},	// BTN_KMP2TEST2	
	{"sd_6015_p7_104",sd_6015_p7_104,"",0,0,false,false},	// BTN_KMP2TEST3	
	{"sd_6015_p7_105",sd_6015_p7_105,"",0,0,false,false},	// BTN_LIGHTS_TEST		
														   
};														   
														   
