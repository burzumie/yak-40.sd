/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Pwr.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Snd.h"

#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_sn_001	""
#define sd_6015_sn_002	""
#define sd_6015_sn_003	""
#define sd_6015_sn_004	""
#define sd_6015_sn_005	""
#define sd_6015_sn_006	""
#define sd_6015_sn_007	""
#define sd_6015_sn_008	""
#define sd_6015_sn_009	""
#define sd_6015_sn_010	""
#define sd_6015_sn_011	""
#define sd_6015_sn_012	""
#define sd_6015_sn_013	""
#define sd_6015_sn_014	""
#define sd_6015_sn_015	""
#define sd_6015_sn_016	""
#define sd_6015_sn_017	""
#define sd_6015_sn_018	""
#define sd_6015_sn_019	""
#define sd_6015_sn_020	""
#define sd_6015_sn_020	""
#define sd_6015_sn_021	""
#define sd_6015_sn_022	""
#define sd_6015_sn_023	""
#define sd_6015_sn_024	""
#define sd_6015_sn_025	""
#define sd_6015_sn_026	""
#define sd_6015_sn_027	""
#define sd_6015_sn_028	""
#define sd_6015_sn_029	""
#define sd_6015_sn_030	""
#define sd_6015_sn_031	""
#define sd_6015_sn_032	""
#define sd_6015_sn_033	""
#define sd_6015_sn_034	""
#define sd_6015_sn_035	""
#define sd_6015_sn_036	""
#define sd_6015_sn_037	""
#define sd_6015_sn_038	""
#define sd_6015_sn_039	""
#define sd_6015_sn_040	""
#define sd_6015_sn_041	""
#define sd_6015_sn_042	""
#define sd_6015_sn_043	""
#define sd_6015_sn_044	""
#define sd_6015_sn_045	""
#define sd_6015_sn_046	""
#define sd_6015_sn_047	""
#define sd_6015_sn_048	""
#define sd_6015_sn_049	""
#define sd_6015_sn_050	""
#define sd_6015_sn_051	""
#define sd_6015_sn_052	""
#define sd_6015_sn_053	""
#define sd_6015_sn_054	""
#define sd_6015_sn_055	""
#define sd_6015_sn_056	""
#define sd_6015_sn_057	""
#define sd_6015_sn_058	""
#define sd_6015_sn_059	""
#define sd_6015_sn_060	""
#define sd_6015_sn_061	""
#define sd_6015_sn_062	""
#define sd_6015_sn_063	""
#define sd_6015_sn_064	""
#define sd_6015_sn_065	""
#define sd_6015_sn_066	""
#define sd_6015_sn_067	""
#define sd_6015_sn_068	""
#define sd_6015_sn_069	""
#define sd_6015_sn_070	""
#define sd_6015_sn_071	""
#define sd_6015_sn_072	""
#define sd_6015_sn_073	""
#define sd_6015_sn_074	""
#define sd_6015_sn_075	""
#define sd_6015_sn_076	""
#define sd_6015_sn_077	""
#define sd_6015_sn_078	""
#define sd_6015_sn_079	""
#define sd_6015_sn_080	""
#define sd_6015_sn_081	""
#define sd_6015_sn_082	""
#define sd_6015_sn_083	""
#define sd_6015_sn_084	""
#define sd_6015_sn_085	""
#define sd_6015_sn_086	""
#define sd_6015_sn_087	""
#define sd_6015_sn_088	""
#define sd_6015_sn_089	""
#define sd_6015_sn_090	""
#define sd_6015_sn_091	""
#define sd_6015_sn_092	""
#define sd_6015_sn_093	""
#define sd_6015_sn_094	""
#define sd_6015_sn_095	""
#define sd_6015_sn_096	""
#define sd_6015_sn_097	""
#define sd_6015_sn_098	""
#define sd_6015_sn_099	""
#define sd_6015_sn_100	""
#define sd_6015_sn_101	""
#define sd_6015_sn_102	""
#define sd_6015_sn_103	""
#define sd_6015_sn_104	""
#define sd_6015_sn_105	""
#define sd_6015_sn_106	""
#define sd_6015_sn_107	""
#define sd_6015_sn_108	""
#define sd_6015_sn_109	""
#define sd_6015_sn_110	""
#define sd_6015_sn_111	""
#define sd_6015_sn_112	""
#define sd_6015_sn_113	""
#define sd_6015_sn_114	""
#define sd_6015_sn_115	""
#define sd_6015_sn_116	""
#define sd_6015_sn_117	""
#define sd_6015_sn_118	""
#define sd_6015_sn_119	""
#define sd_6015_sn_120	""
#define sd_6015_sn_121	""
#define sd_6015_sn_122	""
#define sd_6015_sn_123	""
#define sd_6015_sn_124	""
#define sd_6015_sn_125	""
#define sd_6015_sn_126	""
					  	
#else				  	
					  	
#define sd_6015_sn_001	""
#define sd_6015_sn_002	""
#define sd_6015_sn_003	""
#define sd_6015_sn_004	""
#define sd_6015_sn_005	""
#define sd_6015_sn_006	""
#define sd_6015_sn_007	""
#define sd_6015_sn_008	""
#define sd_6015_sn_009	""
#define sd_6015_sn_010	""
#define sd_6015_sn_011	""
#define sd_6015_sn_012	""
#define sd_6015_sn_013	""
#define sd_6015_sn_014	""
#define sd_6015_sn_015	""
#define sd_6015_sn_016	""
#define sd_6015_sn_017	""
#define sd_6015_sn_018	""
#define sd_6015_sn_019	""
#define sd_6015_sn_020	""
#define sd_6015_sn_020	""
#define sd_6015_sn_021	""
#define sd_6015_sn_022	""
#define sd_6015_sn_023	""
#define sd_6015_sn_024	""
#define sd_6015_sn_025	""
#define sd_6015_sn_026	""
#define sd_6015_sn_027	""
#define sd_6015_sn_028	""
#define sd_6015_sn_029	""
#define sd_6015_sn_030	""
#define sd_6015_sn_031	""
#define sd_6015_sn_032	""
#define sd_6015_sn_033	""
#define sd_6015_sn_034	""
#define sd_6015_sn_035	""
#define sd_6015_sn_036	""
#define sd_6015_sn_037	""
#define sd_6015_sn_038	""
#define sd_6015_sn_039	""
#define sd_6015_sn_040	""
#define sd_6015_sn_041	""
#define sd_6015_sn_042	""
#define sd_6015_sn_043	""
#define sd_6015_sn_044	""
#define sd_6015_sn_045	""
#define sd_6015_sn_046	""
#define sd_6015_sn_047	""
#define sd_6015_sn_048	""
#define sd_6015_sn_049	""
#define sd_6015_sn_050	""
#define sd_6015_sn_051	""
#define sd_6015_sn_052	""
#define sd_6015_sn_053	""
#define sd_6015_sn_054	""
#define sd_6015_sn_055	""
#define sd_6015_sn_056	""
#define sd_6015_sn_057	""
#define sd_6015_sn_058	""
#define sd_6015_sn_059	""
#define sd_6015_sn_060	""
#define sd_6015_sn_061	""
#define sd_6015_sn_062	""
#define sd_6015_sn_063	""
#define sd_6015_sn_064	""
#define sd_6015_sn_065	""
#define sd_6015_sn_066	""
#define sd_6015_sn_067	""
#define sd_6015_sn_068	""
#define sd_6015_sn_069	""
#define sd_6015_sn_070	""
#define sd_6015_sn_071	""
#define sd_6015_sn_072	""
#define sd_6015_sn_073	""
#define sd_6015_sn_074	""
#define sd_6015_sn_075	""
#define sd_6015_sn_076	""
#define sd_6015_sn_077	""
#define sd_6015_sn_078	""
#define sd_6015_sn_079	""
#define sd_6015_sn_080	""
#define sd_6015_sn_081	""
#define sd_6015_sn_082	""
#define sd_6015_sn_083	""
#define sd_6015_sn_084	""
#define sd_6015_sn_085	""
#define sd_6015_sn_086	""
#define sd_6015_sn_087	""
#define sd_6015_sn_088	""
#define sd_6015_sn_089	""
#define sd_6015_sn_090	""
#define sd_6015_sn_091	""
#define sd_6015_sn_092	""
#define sd_6015_sn_093	""
#define sd_6015_sn_094	""
#define sd_6015_sn_095	""
#define sd_6015_sn_096	""
#define sd_6015_sn_097	""
#define sd_6015_sn_098	""
#define sd_6015_sn_099	""
#define sd_6015_sn_100	""
#define sd_6015_sn_101	""
#define sd_6015_sn_102	""
#define sd_6015_sn_103	""
#define sd_6015_sn_104	""
#define sd_6015_sn_105	""
#define sd_6015_sn_106	""
#define sd_6015_sn_107	""
#define sd_6015_sn_108	""
#define sd_6015_sn_109	""
#define sd_6015_sn_110	""
#define sd_6015_sn_111	""
#define sd_6015_sn_112	""
#define sd_6015_sn_113	""
#define sd_6015_sn_114	""
#define sd_6015_sn_115	""
#define sd_6015_sn_116	""
#define sd_6015_sn_117	""
#define sd_6015_sn_118	""
#define sd_6015_sn_119	""
#define sd_6015_sn_120	""
#define sd_6015_sn_121	""
#define sd_6015_sn_122	""
#define sd_6015_sn_123	""
#define sd_6015_sn_124	""
#define sd_6015_sn_125	""
#define sd_6015_sn_126	""

#endif

ApiName SndNames[SND_MAX]={
	{"sd_6015_sn_001",sd_6015_sn_001,"",0,0,false,false},  // 
	{"sd_6015_sn_002",sd_6015_sn_002,"",0,0,false,false},  // 
	{"sd_6015_sn_003",sd_6015_sn_003,"",0,0,false,false},  // 
	{"sd_6015_sn_004",sd_6015_sn_004,"",0,0,false,false},  // 
	{"sd_6015_sn_005",sd_6015_sn_005,"",0,0,false,false},  // 
	{"sd_6015_sn_006",sd_6015_sn_006,"",0,0,false,false},  // 
	{"sd_6015_sn_007",sd_6015_sn_007,"",0,0,false,false},  // 
	{"sd_6015_sn_008",sd_6015_sn_008,"",0,0,false,false},  // 
	{"sd_6015_sn_009",sd_6015_sn_009,"",0,0,false,false},  // 
	{"sd_6015_sn_010",sd_6015_sn_010,"",0,0,false,false},  // 
	{"sd_6015_sn_011",sd_6015_sn_011,"",0,0,false,false},  // 
	{"sd_6015_sn_012",sd_6015_sn_012,"",0,0,false,false},  // 
	{"sd_6015_sn_013",sd_6015_sn_013,"",0,0,false,false},  // 
	{"sd_6015_sn_014",sd_6015_sn_014,"",0,0,false,false},  // 
	{"sd_6015_sn_015",sd_6015_sn_015,"",0,0,false,false},  // 
	{"sd_6015_sn_016",sd_6015_sn_016,"",0,0,false,false},  // 
	{"sd_6015_sn_017",sd_6015_sn_017,"",0,0,false,false},  // 
	{"sd_6015_sn_018",sd_6015_sn_018,"",0,0,false,false},  // 
	{"sd_6015_sn_019",sd_6015_sn_019,"",0,0,false,false},  // 
	{"sd_6015_sn_020",sd_6015_sn_020,"",0,0,false,false},  // 
	{"sd_6015_sn_020",sd_6015_sn_020,"",0,0,false,false},  // 
	{"sd_6015_sn_021",sd_6015_sn_021,"",0,0,false,false},  // 
	{"sd_6015_sn_022",sd_6015_sn_022,"",0,0,false,false},  // 
	{"sd_6015_sn_023",sd_6015_sn_023,"",0,0,false,false},  // 
	{"sd_6015_sn_024",sd_6015_sn_024,"",0,0,false,false},  // 
	{"sd_6015_sn_025",sd_6015_sn_025,"",0,0,false,false},  // 
	{"sd_6015_sn_026",sd_6015_sn_026,"",0,0,false,false},  // 
	{"sd_6015_sn_027",sd_6015_sn_027,"",0,0,false,false},  // 
	{"sd_6015_sn_028",sd_6015_sn_028,"",0,0,false,false},  // 
	{"sd_6015_sn_029",sd_6015_sn_029,"",0,0,false,false},  // 
	{"sd_6015_sn_030",sd_6015_sn_030,"",0,0,false,false},  // 
	{"sd_6015_sn_031",sd_6015_sn_031,"",0,0,false,false},  // 
	{"sd_6015_sn_032",sd_6015_sn_032,"",0,0,false,false},  // 
	{"sd_6015_sn_033",sd_6015_sn_033,"",0,0,false,false},  // 
	{"sd_6015_sn_034",sd_6015_sn_034,"",0,0,false,false},  // 
	{"sd_6015_sn_035",sd_6015_sn_035,"",0,0,false,false},  // 
	{"sd_6015_sn_036",sd_6015_sn_036,"",0,0,false,false},  // 
	{"sd_6015_sn_037",sd_6015_sn_037,"",0,0,false,false},  // 
	{"sd_6015_sn_038",sd_6015_sn_038,"",0,0,false,false},  // 
	{"sd_6015_sn_039",sd_6015_sn_039,"",0,0,false,false},  // 
	{"sd_6015_sn_042",sd_6015_sn_042,"",0,0,false,false},  // 
	{"sd_6015_sn_043",sd_6015_sn_043,"",0,0,false,false},  // 
	{"sd_6015_sn_044",sd_6015_sn_044,"",0,0,false,false},  // 
	{"sd_6015_sn_045",sd_6015_sn_045,"",0,0,false,false},  // 
	{"sd_6015_sn_046",sd_6015_sn_046,"",0,0,false,false},  // 
	{"sd_6015_sn_047",sd_6015_sn_047,"",0,0,false,false},  // 
	{"sd_6015_sn_048",sd_6015_sn_048,"",0,0,false,false},  // 
	{"sd_6015_sn_049",sd_6015_sn_049,"",0,0,false,false},  // 
	{"sd_6015_sn_050",sd_6015_sn_050,"",0,0,false,false},  // 
	{"sd_6015_sn_051",sd_6015_sn_051,"",0,0,false,false},  // 
	{"sd_6015_sn_052",sd_6015_sn_052,"",0,0,false,false},  // 
	{"sd_6015_sn_053",sd_6015_sn_053,"",0,0,false,false},  // 
	{"sd_6015_sn_054",sd_6015_sn_054,"",0,0,false,false},  // 
	{"sd_6015_sn_055",sd_6015_sn_055,"",0,0,false,false},  // 
	{"sd_6015_sn_056",sd_6015_sn_056,"",0,0,false,false},  // 
	{"sd_6015_sn_057",sd_6015_sn_057,"",0,0,false,false},  // 
	{"sd_6015_sn_058",sd_6015_sn_058,"",0,0,false,false},  // 
	{"sd_6015_sn_059",sd_6015_sn_059,"",0,0,false,false},  // 
	{"sd_6015_sn_060",sd_6015_sn_060,"",0,0,false,false},  // 
	{"sd_6015_sn_061",sd_6015_sn_061,"",0,0,false,false},  // 
	{"sd_6015_sn_062",sd_6015_sn_062,"",0,0,false,false},  // 
	{"sd_6015_sn_063",sd_6015_sn_063,"",0,0,false,false},  // 
	{"sd_6015_sn_064",sd_6015_sn_064,"",0,0,false,false},  // 
	{"sd_6015_sn_065",sd_6015_sn_065,"",0,0,false,false},  // 
	{"sd_6015_sn_066",sd_6015_sn_066,"",0,0,false,false},  // 
	{"sd_6015_sn_067",sd_6015_sn_067,"",0,0,false,false},  // 
	{"sd_6015_sn_068",sd_6015_sn_068,"",0,0,false,false},  // 
	{"sd_6015_sn_069",sd_6015_sn_069,"",0,0,false,false},  // 
	{"sd_6015_sn_072",sd_6015_sn_072,"",0,0,false,false},  // 
	{"sd_6015_sn_073",sd_6015_sn_073,"",0,0,false,false},  // 
	{"sd_6015_sn_074",sd_6015_sn_074,"",0,0,false,false},  // 
	{"sd_6015_sn_075",sd_6015_sn_075,"",0,0,false,false},  // 
	{"sd_6015_sn_076",sd_6015_sn_076,"",0,0,false,false},  // 
	{"sd_6015_sn_077",sd_6015_sn_077,"",0,0,false,false},  // 
	{"sd_6015_sn_078",sd_6015_sn_078,"",0,0,false,false},  // 
	{"sd_6015_sn_079",sd_6015_sn_079,"",0,0,false,false},  // 
	{"sd_6015_sn_080",sd_6015_sn_080,"",0,0,false,false},  // 
	{"sd_6015_sn_081",sd_6015_sn_081,"",0,0,false,false},  // 
	{"sd_6015_sn_082",sd_6015_sn_082,"",0,0,false,false},  // 
	{"sd_6015_sn_083",sd_6015_sn_083,"",0,0,false,false},  // 
	{"sd_6015_sn_084",sd_6015_sn_084,"",0,0,false,false},  // 
	{"sd_6015_sn_085",sd_6015_sn_085,"",0,0,false,false},  // 
	{"sd_6015_sn_086",sd_6015_sn_086,"",0,0,false,false},  // 
	{"sd_6015_sn_087",sd_6015_sn_087,"",0,0,false,false},  // 
	{"sd_6015_sn_088",sd_6015_sn_088,"",0,0,false,false},  // 
	{"sd_6015_sn_089",sd_6015_sn_089,"",0,0,false,false},  // 
	{"sd_6015_sn_090",sd_6015_sn_090,"",0,0,false,false},  // 
	{"sd_6015_sn_091",sd_6015_sn_091,"",0,0,false,false},  // 
	{"sd_6015_sn_092",sd_6015_sn_092,"",0,0,false,false},  // 
	{"sd_6015_sn_093",sd_6015_sn_093,"",0,0,false,false},  // 
	{"sd_6015_sn_094",sd_6015_sn_094,"",0,0,false,false},  // 
	{"sd_6015_sn_095",sd_6015_sn_095,"",0,0,false,false},  // 
	{"sd_6015_sn_096",sd_6015_sn_096,"",0,0,false,false},  // 
	{"sd_6015_sn_097",sd_6015_sn_097,"",0,0,false,false},  // 
	{"sd_6015_sn_098",sd_6015_sn_098,"",0,0,false,false},  // 
	{"sd_6015_sn_099",sd_6015_sn_099,"",0,0,false,false},  // 
	{"sd_6015_sn_100",sd_6015_sn_100,"",0,0,false,false},  // 
	{"sd_6015_sn_101",sd_6015_sn_101,"",0,0,false,false},  // 
	{"sd_6015_sn_102",sd_6015_sn_102,"",0,0,false,false},  // 
	{"sd_6015_sn_103",sd_6015_sn_103,"",0,0,false,false},  // 
	{"sd_6015_sn_104",sd_6015_sn_104,"",0,0,false,false},  // 
	{"sd_6015_sn_105",sd_6015_sn_105,"",0,0,false,false},  // 
	{"sd_6015_sn_106",sd_6015_sn_106,"",0,0,false,false},  // 
	{"sd_6015_sn_107",sd_6015_sn_107,"",0,0,false,false},  // 
	{"sd_6015_sn_108",sd_6015_sn_108,"",0,0,false,false},  // 
	{"sd_6015_sn_109",sd_6015_sn_109,"",0,0,false,false},  // 
	{"sd_6015_sn_110",sd_6015_sn_110,"",0,0,false,false},  // 
	{"sd_6015_sn_111",sd_6015_sn_111,"",0,0,false,false},  // 
	{"sd_6015_sn_112",sd_6015_sn_112,"",0,0,false,false},  // 
	{"sd_6015_sn_113",sd_6015_sn_113,"",0,0,false,false},  // 
	{"sd_6015_sn_114",sd_6015_sn_114,"",0,0,false,false},  // 
	{"sd_6015_sn_115",sd_6015_sn_115,"",0,0,false,false},  // 
	{"sd_6015_sn_116",sd_6015_sn_116,"",0,0,false,false},  // 
	{"sd_6015_sn_117",sd_6015_sn_117,"",0,0,false,false},  // 
	{"sd_6015_sn_118",sd_6015_sn_118,"",0,0,false,false},  // 
	{"sd_6015_sn_119",sd_6015_sn_119,"",0,0,false,false},  // 
	{"sd_6015_sn_120",sd_6015_sn_120,"",0,0,false,false},  // 
	{"sd_6015_sn_121",sd_6015_sn_121,"",0,0,false,false},  // 
	{"sd_6015_sn_122",sd_6015_sn_122,"",0,0,false,false},  // 
	{"sd_6015_sn_123",sd_6015_sn_123,"",0,0,false,false},  // 
	{"sd_6015_sn_124",sd_6015_sn_124,"",0,0,false,false},  // 
	{"sd_6015_sn_125",sd_6015_sn_125,"",0,0,false,false},  // 
	{"sd_6015_sn_126",sd_6015_sn_126,"",0,0,false,false},  // 
};

