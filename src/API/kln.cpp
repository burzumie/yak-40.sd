/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Hnd.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Kln.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_kl_001	"POWER: %(V:OFF|ON)%"
#define sd_6015_kl_002	"BRIGHTNESS"
#define sd_6015_kl_003	"BRIGHTNESS"
#define sd_6015_kl_004	"LEFT CURSOR: %(V:OFF|ON)%"
#define sd_6015_kl_005	"RIGHT CURSOR: %(V:OFF|ON)%"
#define sd_6015_kl_006	"LEFT OUTER"
#define sd_6015_kl_007	"LEFT OUTER"
#define sd_6015_kl_008	"LEFT INNER"
#define sd_6015_kl_009	"LEFT INNER"
#define sd_6015_kl_010	"RIGHT OUTER"
#define sd_6015_kl_011	"RIGHT OUTER"
#define sd_6015_kl_012	"RIGHT INNER"
#define sd_6015_kl_013	"RIGHT INNER"
#define sd_6015_kl_014	"PULL SCAN: %(V:OFF|ON)%"
#define sd_6015_kl_015	"MESSAGE"
#define sd_6015_kl_016	"ALTITUDE"
#define sd_6015_kl_017	"DIRECT TO"
#define sd_6015_kl_018	"CLEAR"
#define sd_6015_kl_019	"ENTER"

#else

#define sd_6015_kl_001	""                                
#define sd_6015_kl_002	""                                
#define sd_6015_kl_003	""                                
#define sd_6015_kl_004	""                                
#define sd_6015_kl_005	""                                
#define sd_6015_kl_006	""                                
#define sd_6015_kl_007	""                                
#define sd_6015_kl_008	""                                
#define sd_6015_kl_009	""                                
#define sd_6015_kl_010	""                                
#define sd_6015_kl_011	""                                
#define sd_6015_kl_012	""                                
#define sd_6015_kl_013	""                                
#define sd_6015_kl_014	""                                
#define sd_6015_kl_015	""                                
#define sd_6015_kl_016	""                                
#define sd_6015_kl_017	""                                
#define sd_6015_kl_018	""                                
#define sd_6015_kl_019	""                                

#endif

ApiName KlnNames[KLN_MAX]={
	{"sd_6015_kl_001",sd_6015_kl_001,"",  0,  0,false,false},  // KLN_PWR					
	{"sd_6015_kl_002",sd_6015_kl_002,"",  0,  0,false,false},  // KLN_BRT_LEFT			
	{"sd_6015_kl_003",sd_6015_kl_003,"",  0,  0,false,false},  // KLN_BRT_RIGHT			
	{"sd_6015_kl_004",sd_6015_kl_004,"",  0,  0,false,false},  // KLN_LEFT_CRSR			
	{"sd_6015_kl_005",sd_6015_kl_005,"",  0,  0,false,false},  // KLN_RIGHT_CRSR			
	{"sd_6015_kl_006",sd_6015_kl_006,"",  0,  0,false,false},  // KLN_LEFT_OUTER_LEFT		
	{"sd_6015_kl_007",sd_6015_kl_007,"",  0,  0,false,false},  // KLN_LEFT_OUTER_RIGHT	
	{"sd_6015_kl_008",sd_6015_kl_008,"",  0,  0,false,false},  // KLN_LEFT_INNER_LEFT		
	{"sd_6015_kl_009",sd_6015_kl_009,"",  0,  0,false,false},  // KLN_LEFT_INNER_RIGHT	
	{"sd_6015_kl_010",sd_6015_kl_010,"",  0,  0,false,false},  // KLN_RIGHT_OUTER_LEFT	
	{"sd_6015_kl_011",sd_6015_kl_011,"",  0,  0,false,false},  // KLN_RIGHT_OUTER_RIGHT	
	{"sd_6015_kl_012",sd_6015_kl_012,"",  0,  0,false,false},  // KLN_RIGHT_INNER_LEFT	
	{"sd_6015_kl_013",sd_6015_kl_013,"",  0,  0,false,false},  // KLN_RIGHT_INNER_RIGHT	
	{"sd_6015_kl_014",sd_6015_kl_014,"",  0,  0,false,false},  // KLN_PULL_SCAN        	
	{"sd_6015_kl_015",sd_6015_kl_015,"",  0,  0,false,false},  // KLN_MESSAGE          	
	{"sd_6015_kl_016",sd_6015_kl_016,"",  0,  0,false,false},  // KLN_ALTITUDE         	
	{"sd_6015_kl_017",sd_6015_kl_017,"",  0,  0,false,false},  // KLN_DIRECT_TO        	
	{"sd_6015_kl_018",sd_6015_kl_018,"",  0,  0,false,false},  // KLN_CLEAR            	
	{"sd_6015_kl_019",sd_6015_kl_019,"",  0,  0,false,false},  // KLN_ENTER            	
										  							 
};

