/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Hnd.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Key.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_pk_001	""                                
#define sd_6015_pk_002	""                                
#define sd_6015_pk_003	""                                

#else

#define sd_6015_pk_001	""                                
#define sd_6015_pk_002	""                                
#define sd_6015_pk_003	""                                

#endif

ApiName KeyNames[KEY_MAX]={
	{"sd_6015_pk_001",sd_6015_pk_001,"",  0,  0,false,false},  // KEY_AILERON_CENTER
	{"sd_6015_pk_002",sd_6015_pk_002,"",  0,  0,false,false},  // KEY_RUDDER_CENTER
	{"sd_6015_pk_003",sd_6015_pk_003,"",  0,  0,false,false},  // KEY_GEAR
										  							 
};

