/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/ServerDef.h $

  Last modification:
    $Date: 19.02.06 5:54 $
    $Revision: 4 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#define AZS_GET(V)		g_SDAPIEntrys.SDAZS[V].m_Val[0]
#define AZS_GETPREV(V)	g_SDAPIEntrys.SDAZS[V].m_Val[2]
#define AZS_INC(V)		g_SDAPIEntrys.SDAZS[V].Inc()
#define AZS_DEC(V)		g_SDAPIEntrys.SDAZS[V].Dec()
#define AZS_INC2(V,P)	g_SDAPIEntrys.SDAZS[V].Inc(P)
#define AZS_DEC2(V,P)	g_SDAPIEntrys.SDAZS[V].Dec(P)
#define AZS_TGL(V)		g_SDAPIEntrys.SDAZS[V].Toggle()
#define AZS_SET(V,P)	g_SDAPIEntrys.SDAZS[V].Set(P)
#define AZS_TT(V)		return g_SDAPIEntrys.SDAZS[V].m_TTDst;
#define AZS_TTGET(V)	g_SDAPIEntrys.SDAZS[V].m_TTDst
#define AZS_TTSET(V,S)	g_SDAPIEntrys.SDAZS[V].SetTT(S)
#define AZS_CHG(V)		g_SDAPIEntrys.SDAZS[V].IsChanged()
#define AZS_CHGNR(V)	g_SDAPIEntrys.SDAZS[V].IsChanged(false)

#define BTN_GET(V)		g_SDAPIEntrys.SDBTN[V].m_Val[0]
#define BTN_GETPREV(V)	g_SDAPIEntrys.SDBTN[V].m_Val[2]
#define BTN_INC(V)		g_SDAPIEntrys.SDBTN[V].Inc()
#define BTN_DEC(V)		g_SDAPIEntrys.SDBTN[V].Dec()
#define BTN_INC2(V,P)	g_SDAPIEntrys.SDBTN[V].Inc(P)
#define BTN_DEC2(V,P)	g_SDAPIEntrys.SDBTN[V].Dec(P)
#define BTN_TGL(V)		g_SDAPIEntrys.SDBTN[V].Toggle()
#define BTN_SET(V,P)	g_SDAPIEntrys.SDBTN[V].Set(P)
#define BTN_TT(V)		return g_SDAPIEntrys.SDBTN[V].m_TTDst;
#define BTN_TTGET(V)	g_SDAPIEntrys.SDBTN[V].m_TTDst
#define BTN_TTSET(V,S)	g_SDAPIEntrys.SDBTN[V].SetTT(S)
#define BTN_CHG(V)		g_SDAPIEntrys.SDBTN[V].IsChanged()
#define BTN_CHGNR(V)	g_SDAPIEntrys.SDBTN[V].IsChanged(false)

#define GLT_GET(V)		g_SDAPIEntrys.SDGLT[V].m_Val[0]
#define GLT_GETPREV(V)	g_SDAPIEntrys.SDGLT[V].m_Val[2]
#define GLT_INC(V)		g_SDAPIEntrys.SDGLT[V].Inc()
#define GLT_DEC(V)		g_SDAPIEntrys.SDGLT[V].Dec()
#define GLT_INC2(V,P)	g_SDAPIEntrys.SDGLT[V].Inc(P)
#define GLT_DEC2(V,P)	g_SDAPIEntrys.SDGLT[V].Dec(P)
#define GLT_TGL(V)		g_SDAPIEntrys.SDGLT[V].Toggle()
#define GLT_SET(V,P)	g_SDAPIEntrys.SDGLT[V].Set(P)
#define GLT_TT(V)		return g_SDAPIEntrys.SDGLT[V].m_TTDst;
#define GLT_TTGET(V)	g_SDAPIEntrys.SDGLT[V].m_TTDst
#define GLT_TTSET(V,S)	g_SDAPIEntrys.SDGLT[V].SetTT(S)
#define GLT_CHG(V)		g_SDAPIEntrys.SDGLT[V].IsChanged()
#define GLT_CHGNR(V)	g_SDAPIEntrys.SDGLT[V].IsChanged(false)

#define KRM_GET(V)		g_SDAPIEntrys.SDKRM[V].m_Val[0]
#define KRM_GETPREV(V)	g_SDAPIEntrys.SDKRM[V].m_Val[2]
#define KRM_INC(V)		g_SDAPIEntrys.SDKRM[V].Inc()
#define KRM_DEC(V)		g_SDAPIEntrys.SDKRM[V].Dec()
#define KRM_INC2(V,P)	g_SDAPIEntrys.SDKRM[V].Inc(P)
#define KRM_DEC2(V,P)	g_SDAPIEntrys.SDKRM[V].Dec(P)
#define KRM_TGL(V)		g_SDAPIEntrys.SDKRM[V].Toggle()
#define KRM_SET(V,P)	g_SDAPIEntrys.SDKRM[V].Set(P)
#define KRM_TT(V)		return g_SDAPIEntrys.SDKRM[V].m_TTDst;
#define KRM_TTGET(V)	g_SDAPIEntrys.SDKRM[V].m_TTDst
#define KRM_TTSET(V,S)	g_SDAPIEntrys.SDKRM[V].SetTT(S)
#define KRM_CHG(V)		g_SDAPIEntrys.SDKRM[V].IsChanged()
#define KRM_CHGNR(V)	g_SDAPIEntrys.SDKRM[V].IsChanged(false)

#define HND_GET(V)		g_SDAPIEntrys.SDHND[V].m_Val[0]
#define HND_INC(V)		g_SDAPIEntrys.SDHND[V].Inc()
#define HND_DEC(V)		g_SDAPIEntrys.SDHND[V].Dec()
#define HND_INC2(V,P)	g_SDAPIEntrys.SDHND[V].Inc(P)
#define HND_DEC2(V,P)	g_SDAPIEntrys.SDHND[V].Dec(P)
#define HND_TGL(V)		g_SDAPIEntrys.SDHND[V].Toggle()
#define HND_SET(V,P)	g_SDAPIEntrys.SDHND[V].Set(P)
#define HND_TT(V)		return g_SDAPIEntrys.SDHND[V].m_TTDst;
#define HND_TTGET(V)	g_SDAPIEntrys.SDHND[V].m_TTDst
#define HND_TTSET(V,S)	g_SDAPIEntrys.SDHND[V].SetTT(S)
#define HND_CHG(V)		g_SDAPIEntrys.SDHND[V].IsChanged()
#define HND_CHGNR(V)	g_SDAPIEntrys.SDHND[V].IsChanged(false)

#define LMP_GET(V)		g_SDAPIEntrys.SDLMP[V].m_Val[0]
#define LMP_INC(V)		g_SDAPIEntrys.SDLMP[V].Inc()
#define LMP_DEC(V)		g_SDAPIEntrys.SDLMP[V].Dec()
#define LMP_INC2(V,P)	g_SDAPIEntrys.SDLMP[V].Inc(P)
#define LMP_DEC2(V,P)	g_SDAPIEntrys.SDLMP[V].Dec(P)
#define LMP_TGL(V)		g_SDAPIEntrys.SDLMP[V].Toggle()
#define LMP_SET(V,P)	g_SDAPIEntrys.SDLMP[V].Set(P)
#define LMP_TT(V)		return g_SDAPIEntrys.SDLMP[V].m_TTDst;
#define LMP_TTGET(V)	g_SDAPIEntrys.SDLMP[V].m_TTDst
#define LMP_TTSET(V,S)	g_SDAPIEntrys.SDLMP[V].SetTT(S)
#define LMP_CHG(V)		g_SDAPIEntrys.SDLMP[V].IsChanged()
#define LMP_CHGNR(V)	g_SDAPIEntrys.SDLMP[V].IsChanged(false)

#define LMPE_GET(V)		g_SDAPIEntrys.SDLMPE[V].m_Val[0]
#define LMPE_INC(V)		g_SDAPIEntrys.SDLMPE[V].Inc()
#define LMPE_DEC(V)		g_SDAPIEntrys.SDLMPE[V].Dec()
#define LMPE_INC2(V,P)	g_SDAPIEntrys.SDLMPE[V].Inc(P)
#define LMPE_DEC2(V,P)	g_SDAPIEntrys.SDLMPE[V].Dec(P)
#define LMPE_TGL(V)		g_SDAPIEntrys.SDLMPE[V].Toggle()
#define LMPE_SET(V,P)	g_SDAPIEntrys.SDLMPE[V].Set(P)
#define LMPE_TT(V)		return g_SDAPIEntrys.SDLMPE[V].m_TTDst;
#define LMPE_TTGET(V)	g_SDAPIEntrys.SDLMPE[V].m_TTDst
#define LMPE_TTSET(V,S)	g_SDAPIEntrys.SDLMPE[V].SetTT(S)
#define LMPE_CHG(V)		g_SDAPIEntrys.SDLMPE[V].IsChanged()
#define LMPE_CHGNR(V)	g_SDAPIEntrys.SDLMPE[V].IsChanged(false)

#define NDL_GET(V)		g_SDAPIEntrys.SDNDL[V].m_Val[0]
#define NDL_INC(V)		g_SDAPIEntrys.SDNDL[V].Inc()
#define NDL_DEC(V)		g_SDAPIEntrys.SDNDL[V].Dec()
#define NDL_INC2(V,P)	g_SDAPIEntrys.SDNDL[V].Inc(P)
#define NDL_DEC2(V,P)	g_SDAPIEntrys.SDNDL[V].Dec(P)
#define NDL_TGL(V)		g_SDAPIEntrys.SDNDL[V].Toggle()
#define NDL_SET(V,P)	g_SDAPIEntrys.SDNDL[V].Set(P)
#define NDL_TT(V)		return g_SDAPIEntrys.SDNDL[V].m_TTDst;
#define NDL_TTGET(V)	g_SDAPIEntrys.SDNDL[V].m_TTDst
#define NDL_TTSET(V,S)	g_SDAPIEntrys.SDNDL[V].SetTT(S)
#define NDL_CHG(V)		g_SDAPIEntrys.SDNDL[V].IsChanged()
#define NDL_CHGNR(V)	g_SDAPIEntrys.SDNDL[V].IsChanged(false)

#define POS_GET(V)		g_SDAPIEntrys.SDPOS[V].m_Val[0]
#define POS_INC(V)		g_SDAPIEntrys.SDPOS[V].Inc()
#define POS_DEC(V)		g_SDAPIEntrys.SDPOS[V].Dec()
#define POS_INC2(V,P)	g_SDAPIEntrys.SDPOS[V].Inc(P)
#define POS_DEC2(V,P)	g_SDAPIEntrys.SDPOS[V].Dec(P)
#define POS_TGL(V)		g_SDAPIEntrys.SDPOS[V].Toggle()
#define POS_SET(V,P)	g_SDAPIEntrys.SDPOS[V].Set(P)
#define POS_TT(V)		return g_SDAPIEntrys.SDPOS[V].m_TTDst;
#define POS_TTGET(V)	g_SDAPIEntrys.SDPOS[V].m_TTDst
#define POS_TTSET(V,S)	g_SDAPIEntrys.SDPOS[V].SetTT(S)
#define POS_CHG(V)		g_SDAPIEntrys.SDPOS[V].IsChanged()
#define POS_CHGNR(V)	g_SDAPIEntrys.SDPOS[V].IsChanged(false)

#define TBG_GET(V)		g_SDAPIEntrys.SDTBG[V].m_Val[0]
#define TBG_INC(V)		g_SDAPIEntrys.SDTBG[V].Inc()
#define TBG_DEC(V)		g_SDAPIEntrys.SDTBG[V].Dec()
#define TBG_INC2(V,P)	g_SDAPIEntrys.SDTBG[V].Inc(P)
#define TBG_DEC2(V,P)	g_SDAPIEntrys.SDTBG[V].Dec(P)
#define TBG_TGL(V)		g_SDAPIEntrys.SDTBG[V].Toggle()
#define TBG_SET(V,P)	g_SDAPIEntrys.SDTBG[V].Set(P)
#define TBG_TT(V)		return g_SDAPIEntrys.SDTBG[V].m_TTDst;
#define TBG_TTGET(V)	g_SDAPIEntrys.SDTBG[V].m_TTDst
#define TBG_TTSET(V,S)	g_SDAPIEntrys.SDTBG[V].SetTT(S)
#define TBG_CHG(V)		g_SDAPIEntrys.SDTBG[V].IsChanged()
#define TBG_CHGNR(V)	g_SDAPIEntrys.SDTBG[V].IsChanged(false)

#define TBGE_GET(V)		g_SDAPIEntrys.SDTBGE[V].m_Val[0]
#define TBGE_INC(V)		g_SDAPIEntrys.SDTBGE[V].Inc()
#define TBGE_DEC(V)		g_SDAPIEntrys.SDTBGE[V].Dec()
#define TBGE_INC2(V,P)	g_SDAPIEntrys.SDTBGE[V].Inc(P)
#define TBGE_DEC2(V,P)	g_SDAPIEntrys.SDTBGE[V].Dec(P)
#define TBGE_TGL(V)		g_SDAPIEntrys.SDTBGE[V].Toggle()
#define TBGE_SET(V,P)	g_SDAPIEntrys.SDTBGE[V].Set(P)
#define TBGE_TT(V)		return g_SDAPIEntrys.SDTBGE[V].m_TTDst;
#define TBGE_TTGET(V)	g_SDAPIEntrys.SDTBGE[V].m_TTDst
#define TBGE_TTSET(V,S)	g_SDAPIEntrys.SDTBGE[V].SetTT(S)
#define TBGE_CHG(V)		g_SDAPIEntrys.SDTBGE[V].IsChanged()
#define TBGE_CHGNR(V)	g_SDAPIEntrys.SDTBGE[V].IsChanged(false)

#define TBL_GET(V)		g_SDAPIEntrys.SDTBL[V].m_Val[0]
#define TBL_INC(V)		g_SDAPIEntrys.SDTBL[V].Inc()
#define TBL_DEC(V)		g_SDAPIEntrys.SDTBL[V].Dec()
#define TBL_INC2(V,P)	g_SDAPIEntrys.SDTBL[V].Inc(P)
#define TBL_DEC2(V,P)	g_SDAPIEntrys.SDTBL[V].Dec(P)
#define TBL_TGL(V)		g_SDAPIEntrys.SDTBL[V].Toggle()
#define TBL_SET(V,P)	g_SDAPIEntrys.SDTBL[V].Set(P)
#define TBL_TT(V)		return g_SDAPIEntrys.SDTBL[V].m_TTDst;
#define TBL_TTGET(V)	g_SDAPIEntrys.SDTBL[V].m_TTDst
#define TBL_TTSET(V,S)	g_SDAPIEntrys.SDTBL[V].SetTT(S)
#define TBL_CHG(V)		g_SDAPIEntrys.SDTBL[V].IsChanged()
#define TBL_CHGNR(V)	g_SDAPIEntrys.SDTBL[V].IsChanged(false)

#define TBLE_GET(V)		g_SDAPIEntrys.SDTBLE[V].m_Val[0]
#define TBLE_INC(V)		g_SDAPIEntrys.SDTBLE[V].Inc()
#define TBLE_DEC(V)		g_SDAPIEntrys.SDTBLE[V].Dec()
#define TBLE_INC2(V,P)	g_SDAPIEntrys.SDTBLE[V].Inc(P)
#define TBLE_DEC2(V,P)	g_SDAPIEntrys.SDTBLE[V].Dec(P)
#define TBLE_TGL(V)		g_SDAPIEntrys.SDTBLE[V].Toggle()
#define TBLE_SET(V,P)	g_SDAPIEntrys.SDTBLE[V].Set(P)
#define TBLE_TT(V)		return g_SDAPIEntrys.SDTBLE[V].m_TTDst;
#define TBLE_TTGET(V)	g_SDAPIEntrys.SDTBLE[V].m_TTDst
#define TBLE_TTSET(V,S)	g_SDAPIEntrys.SDTBLE[V].SetTT(S)
#define TBLE_CHG(V)		g_SDAPIEntrys.SDTBLE[V].IsChanged()
#define TBLE_CHGNR(V)	g_SDAPIEntrys.SDTBLE[V].IsChanged(false)

#define PWR_GET(V)		g_SDAPIEntrys.SDPWR[V].m_Val[0]
#define PWR_INC(V)		g_SDAPIEntrys.SDPWR[V].Inc()
#define PWR_DEC(V)		g_SDAPIEntrys.SDPWR[V].Dec()
#define PWR_INC2(V,P)	g_SDAPIEntrys.SDPWR[V].Inc(P)
#define PWR_DEC2(V,P)	g_SDAPIEntrys.SDPWR[V].Dec(P)
#define PWR_TGL(V)		g_SDAPIEntrys.SDPWR[V].Toggle()
#define PWR_SET(V,P)	g_SDAPIEntrys.SDPWR[V].Set(P)
#define PWR_TT(V)		return g_SDAPIEntrys.SDPWR[V].m_TTDst;
#define PWR_TTGET(V)	g_SDAPIEntrys.SDPWR[V].m_TTDst
#define PWR_TTSET(V,S)	g_SDAPIEntrys.SDPWR[V].SetTT(S)
#define PWR_CHG(V)		g_SDAPIEntrys.SDPWR[V].IsChanged()
#define PWR_CHGNR(V)	g_SDAPIEntrys.SDPWR[V].IsChanged(false)

#define CFG_GET(V)		g_SDAPIEntrys.SDCFG[V].m_Val[0]
#define CFG_INC(V)		g_SDAPIEntrys.SDCFG[V].Inc()
#define CFG_DEC(V)		g_SDAPIEntrys.SDCFG[V].Dec()
#define CFG_INC2(V,P)	g_SDAPIEntrys.SDCFG[V].Inc(P)
#define CFG_DEC2(V,P)	g_SDAPIEntrys.SDCFG[V].Dec(P)
#define CFG_TGL(V)		g_SDAPIEntrys.SDCFG[V].Toggle()
#define CFG_SET(V,P)	g_SDAPIEntrys.SDCFG[V].Set(P)
#define CFG_TT(V)		return g_SDAPIEntrys.SDCFG[V].m_TTDst;
#define CFG_TTGET(V)	g_SDAPIEntrys.SDCFG[V].m_TTDst
#define CFG_TTSET(V,S)	g_SDAPIEntrys.SDCFG[V].SetTT(S)
#define CFG_CHG(V)		g_SDAPIEntrys.SDCFG[V].IsChanged()
#define CFG_CHGNR(V)	g_SDAPIEntrys.SDCFG[V].IsChanged(false)

#define SND_GET(V)		g_SDAPIEntrys.SDSND[V].m_Val[0]
#define SND_INC(V)		g_SDAPIEntrys.SDSND[V].Inc()
#define SND_DEC(V)		g_SDAPIEntrys.SDSND[V].Dec()
#define SND_INC2(V,P)	g_SDAPIEntrys.SDSND[V].Inc(P)
#define SND_DEC2(V,P)	g_SDAPIEntrys.SDSND[V].Dec(P)
#define SND_TGL(V)		g_SDAPIEntrys.SDSND[V].Toggle()
#define SND_SET(V,P)	g_SDAPIEntrys.SDSND[V].Set(P)
#define SND_TT(V)		return g_SDAPIEntrys.SDSND[V].m_TTDst;
#define SND_TTGET(V)	g_SDAPIEntrys.SDSND[V].m_TTDst
#define SND_TTSET(V,S)	g_SDAPIEntrys.SDSND[V].SetTT(S)
#define SND_CHG(V)		g_SDAPIEntrys.SDSND[V].IsChanged()
#define SND_CHGNR(V)	g_SDAPIEntrys.SDSND[V].IsChanged(false)

#define CFG_STR_GET(V)	g_SDAPIEntrys.SDCFGSTR[V]
#define CFG_STR_SET(V,S)	strcpy(g_SDAPIEntrys.SDCFGSTR[V],S);

#define KEY_GET(V)		g_SDAPIEntrys.SDKEY[V].m_Val[0]
#define KEY_INC(V)		g_SDAPIEntrys.SDKEY[V].Inc()
#define KEY_DEC(V)		g_SDAPIEntrys.SDKEY[V].Dec()
#define KEY_INC2(V,P)	g_SDAPIEntrys.SDKEY[V].Inc(P)
#define KEY_DEC2(V,P)	g_SDAPIEntrys.SDKEY[V].Dec(P)
#define KEY_TGL(V)		g_SDAPIEntrys.SDKEY[V].Toggle()
#define KEY_SET(V,P)	g_SDAPIEntrys.SDKEY[V].Set(P)
#define KEY_TT(V)		return g_SDAPIEntrys.SDKEY[V].m_TTDst;
#define KEY_TTGET(V)	g_SDAPIEntrys.SDKEY[V].m_TTDst
#define KEY_TTSET(V,S)	g_SDAPIEntrys.SDKEY[V].SetTT(S)
#define KEY_CHG(V)		g_SDAPIEntrys.SDKEY[V].IsChanged()
#define KEY_CHGNR(V)	g_SDAPIEntrys.SDKEY[V].IsChanged(false)

#define KLN_GET(V)		g_SDAPIEntrys.SDKLN[V].m_Val[0]
#define KLN_GETPREV(V)	g_SDAPIEntrys.SDKLN[V].m_Val[2]
#define KLN_INC(V)		g_SDAPIEntrys.SDKLN[V].Inc()
#define KLN_DEC(V)		g_SDAPIEntrys.SDKLN[V].Dec()
#define KLN_INC2(V,P)	g_SDAPIEntrys.SDKLN[V].Inc(P)
#define KLN_DEC2(V,P)	g_SDAPIEntrys.SDKLN[V].Dec(P)
#define KLN_TGL(V)		g_SDAPIEntrys.SDKLN[V].Toggle()
#define KLN_SET(V,P)	g_SDAPIEntrys.SDKLN[V].Set(P)
#define KLN_TT(V)		return g_SDAPIEntrys.SDKLN[V].m_TTDst;
#define KLN_TTGET(V)	g_SDAPIEntrys.SDKLN[V].m_TTDst
#define KLN_TTSET(V,S)	g_SDAPIEntrys.SDKLN[V].SetTT(S)
#define KLN_CHG(V)		g_SDAPIEntrys.SDKLN[V].IsChanged()
#define KLN_CHGNR(V)	g_SDAPIEntrys.SDKLN[V].IsChanged(false)
