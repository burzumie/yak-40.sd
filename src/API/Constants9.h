/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Constants.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

const string REGISTRY_SIM_PATH				= "EXE Path";
const string SIM_FLT_EXTENSION				= "flt";
const string SIM_FLT_GENERATED				= "ui generated flight."+SIM_FLT_EXTENSION;
const string SIM_FLT_PREVIOUS				= "previous flight."+SIM_FLT_EXTENSION;
const string SIM_FLT_MY_SECTION				= "SD Yak-40 6015";
const string SIM_FSUI_MODULE				= "fsui.dll";
const string SIM_LANG_MODULE				= "language.dll";
const string SIM_MODULES_DIR				= "Modules";

const string MY_GAUGE_PATH_PLAIN			= "..\\..\\..\\";
const string MY_PATH						= "SuprunovDesign\\Yak-40\\";
const string MY_SOUND						= "Sound";
const string MY_NAVBASE						= "NavBase";
const string MY_SOUND_PATH					= MY_PATH+MY_SOUND;
const string MY_CONFIG_PATH					= MY_PATH+"Config";
const string MY_NAVBASE_PATH				= MY_NAVBASE+MY_NAVBASE;
const string MY_CONFIG_FILE					= "config.ini";
const string MY_SOUND_CONFIG_FILE			= "sound.ini";

