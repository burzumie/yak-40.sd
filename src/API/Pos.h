/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Pos.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

enum POS {
	// �����
	POS_ACHS_BLENKER                ,	// "sd_6015_pa_401"
	POS_ACHS_TIMER                  ,	// "sd_6015_pa_402"
	POS_PANEL_STATE					,   // "sd_6015_pa_403"
	POS_PANEL_LANG 					,   // "sd_6015_pa_404"
	POS_TIME_AFTER_LOAD				,	// "sd_6015_pa_405"
	POS_MAIN_FLAPS_WORK				,	// "sd_6015_pa_406"
	POS_AUXL_FLAPS_WORK				,	// "sd_6015_pa_407"
	POS_OPEN_CLOSE_WINDOW			,	// "sd_6015_pa_408"
	POS_GROUND_STARTUP   			,	// "sd_6015_pa_409"
	POS_ENGINE1_MIXTURE  			,	// "sd_6015_pa_410"
	POS_ENGINE2_MIXTURE  			,	// "sd_6015_pa_411"
	POS_ENGINE3_MIXTURE  			,	// "sd_6015_pa_412"
	POS_TARGET_SALON_TEMP			,	// "sd_6015_pa_413"
	POS_TARGET_DUCT_TEMP			,	// "sd_6015_pa_414"
	POS_TIME_OF_DAY					,	// "sd_6015_pa_415"
	POS_ACTIVE_VIEW_MODE			,	// "sd_6015_pa_416"
	POS_SIM_PAUSED					,	// "sd_6015_pa_417"
	POS_SIM_SPEED					,	// "sd_6015_pa_418"
	POS_DESCENT						,	// "sd_6015_pa_419"
	POS_GMK_READY					,	// "sd_6015_pa_420"
	POS_MAIN_HYDRO_WORK				,	// "sd_6015_pa_421"

	// P0
	POS_CRS0_01						,	// "sd_6015_p0_401"
	POS_CRS0_02						,	// "sd_6015_p0_402"
	POS_CRS0_03						,	// "sd_6015_p0_403"
	POS_CRS0_04						,	// "sd_6015_p0_404"
	POS_YOKE0_ICON					,	// "sd_6015_p0_405"
	POS_AGB0_MAIN_PITCH             ,   // "sd_6015_p0_406"
	POS_AGB_AUXL_PITCH              ,   // "sd_6015_p0_407"
	POS_KPPMS0_BLK_COURSE           ,   // "sd_6015_p0_408"
	POS_KPPMS0_BLK_GLIDE            ,   // "sd_6015_p0_409"
	POS_RV3M_LAMP                   ,   // "sd_6015_p0_410"
	POS_UVID0_PSI_100_MM            ,   // "sd_6015_p0_411"
	POS_UVID0_PSI_10_MM				,   // "sd_6015_p0_412"
	POS_UVID0_PSI_1_MM              ,   // "sd_6015_p0_413"
	POS_UVID0_ALT_10000             ,   // "sd_6015_p0_414"
	POS_UVID0_ALT_1000              ,   // "sd_6015_p0_415"
	POS_UVID0_ALT_100               ,   // "sd_6015_p0_416"
	POS_UVID0_ALT_10                ,   // "sd_6015_p0_417"
	POS_UVID0_MB                    ,   // "sd_6015_p0_418"
	POS_UVID0_HG                    ,   // "sd_6015_p0_419"
	POS_UVID0_MM                    ,   // "sd_6015_p0_420"
	POS_UVID0_ALT_10000_EN          ,   // "sd_6015_p0_421"
	POS_UVID0_ALT_1000_EN           ,   // "sd_6015_p0_422"
	POS_UVID0_ALT_100_EN            ,   // "sd_6015_p0_423"
	POS_UVID0_ALT_10_EN             ,   // "sd_6015_p0_424"
	POS_UVID0_PSI_1000_MB           ,   // "sd_6015_p0_425"
	POS_UVID0_PSI_100_MB            ,   // "sd_6015_p0_426"
	POS_UVID0_PSI_10_MB				,   // "sd_6015_p0_427"
	POS_UVID0_PSI_1_MB              ,   // "sd_6015_p0_428"
	POS_UVID0_PSI_1000_HG           ,   // "sd_6015_p0_429"
	POS_UVID0_PSI_100_HG            ,   // "sd_6015_p0_430"
	POS_UVID0_PSI_10_HG				,   // "sd_6015_p0_431"
	POS_UVID0_PSI_1_HG              ,   // "sd_6015_p0_432"

	// P1
	POS_CRS1_01						,	// "sd_6015_p1_401"
	POS_CRS1_02						,	// "sd_6015_p1_402"
	POS_CRS1_03						,	// "sd_6015_p1_403"
	POS_CRS1_04						,	// "sd_6015_p1_404"
	POS_YOKE1_ICON					,	// "sd_6015_p1_405"
	POS_AGB1_MAIN_PITCH             ,   // "sd_6015_p1_406"
	POS_KPPMS1_BLK_COURSE           ,   // "sd_6015_p1_407"
	POS_KPPMS1_BLK_GLIDE            ,   // "sd_6015_p1_408"
	POS_UVID1_MB                    ,   // "sd_6015_p1_409"
	POS_UVID1_HG                    ,   // "sd_6015_p1_410"
	POS_UVID1_MM                    ,   // "sd_6015_p1_411"
	POS_UVID1_PSI_100_MM            ,   // "sd_6015_p1_412"
	POS_UVID1_PSI_10_MM				,   // "sd_6015_p1_413"
	POS_UVID1_PSI_1_MM              ,   // "sd_6015_p1_414"
	POS_UVID1_PSI_1000_MB           ,   // "sd_6015_p1_415"
	POS_UVID1_PSI_100_MB            ,   // "sd_6015_p1_416"
	POS_UVID1_PSI_10_MB				,   // "sd_6015_p1_417"
	POS_UVID1_PSI_1_MB              ,   // "sd_6015_p1_418"
	POS_UVID1_PSI_1000_HG           ,   // "sd_6015_p1_419"
	POS_UVID1_PSI_100_HG            ,   // "sd_6015_p1_420"
	POS_UVID1_PSI_10_HG				,   // "sd_6015_p1_421"
	POS_UVID1_PSI_1_HG              ,   // "sd_6015_p1_422"

	// P4
	POS_CRS4_01                      ,	// "sd_6015_p4_401"
	POS_CRS4_02                      ,	// "sd_6015_p4_402"
	POS_CRS4_03                      ,	// "sd_6015_p4_403"
	POS_STOPOR                      ,	// "sd_6015_p4_404"
	POS_ENGSTART1                   ,	// "sd_6015_p4_405"
	POS_ENGSTART2                   ,	// "sd_6015_p4_406"
	POS_ENGSTART3                   ,	// "sd_6015_p4_407"
	POS_RUD1                        ,   // "sd_6015_p4_408"
	POS_RUD2                        ,   // "sd_6015_p4_409"
	POS_RUD3                        ,   // "sd_6015_p4_410"
	POS_PRKBRK                      ,   // "sd_6015_p4_411"
	POS_STOPOR_TRIGGER              ,   // "sd_6015_p4_412"
	POS_BRAKE_TRIGGER               ,   // "sd_6015_p4_413"
	POS_AP_PITCH_MODE               ,   // "sd_6015_p4_414"
	POS_AP_BANK_MODE                ,   // "sd_6015_p4_415"
	POS_AP_HAND_MODE                ,   // "sd_6015_p4_416"
	POS_AP_MOUSE_HND_PRESS          ,   // "sd_6015_p4_417"
	POS_RUD1_STOP                   ,   // "sd_6015_p4_418"
	POS_RUD2_STOP                   ,   // "sd_6015_p4_419"
	POS_RUD3_STOP                   ,   // "sd_6015_p4_420"

	// P5
	POS_CRS5_01						,	// "sd_6015_p5_401"
	POS_CRS5_02						,	// "sd_6015_p5_402"
	POS_CRS5_03						,	// "sd_6015_p5_403"
	POS_SCLVHF1LEFT			        ,	// "sd_6015_p5_404"
	POS_SCLVHF2LEFT			        ,	// "sd_6015_p5_405"
	POS_SCLVHF1MID                  ,   // "sd_6015_p5_406"
	POS_SCLVHF2MID                  ,   // "sd_6015_p5_407"
	POS_SCLVHF1RIGHT                ,   // "sd_6015_p5_408"
	POS_SCLVHF2RIGHT                ,   // "sd_6015_p5_409"
	POS_ADF1LFREQ                   ,   // "sd_6015_p5_410"
	POS_ADF1RFREQ                   ,   // "sd_6015_p5_411"
	POS_ADF2LFREQ                   ,   // "sd_6015_p5_412"
	POS_ADF2RFREQ                   ,   // "sd_6015_p5_413"

	// P6
	POS_CRS6_01		                ,	// "sd_6015_p6_401"
	POS_XPDR1		                ,	// "sd_6015_p6_402"
	POS_XPDR2		                ,	// "sd_6015_p6_403"
	POS_XPDR3		                ,	// "sd_6015_p6_404"
	POS_XPDR4		                ,	// "sd_6015_p6_405"
	POS_NAV6_FREQ1	                ,   // "sd_6015_p6_406"
	POS_NAV6_FREQ2	                ,   // "sd_6015_p6_407"
	POS_NAV6_FREQ3	                ,   // "sd_6015_p6_408"
	POS_NAV6_FREQ4	                ,	// "sd_6015_p6_409"
	POS_NAV6_FREQ5	                ,	// "sd_6015_p6_410"
	POS_OBS61		                ,	// "sd_6015_p6_411"
	POS_OBS62		                ,	// "sd_6015_p6_412"
	POS_OBS63		                ,	// "sd_6015_p6_413"
	POS_DME61		                ,   // "sd_6015_p6_414"
	POS_DME62		                ,   // "sd_6015_p6_415"
	POS_DME63		                ,   // "sd_6015_p6_416"
	POS_DME64		                ,	// "sd_6015_p6_417"
	POS_DME65		                ,	// "sd_6015_p6_418"
	POS_DME61M		                ,	// "sd_6015_p6_419"
	POS_DME62M		                ,	// "sd_6015_p6_420"
	POS_DME63M		                ,	// "sd_6015_p6_421"
	POS_DME64M		                ,   // "sd_6015_p6_422"
	POS_DME65M		                ,   // "sd_6015_p6_423"

	// P7
	POS_CRS7_01		                ,	// "sd_6015_p7_401"
	POS_NAV7_FREQ1	                ,	// "sd_6015_p7_402"
	POS_NAV7_FREQ2	                ,	// "sd_6015_p7_403"
	POS_NAV7_FREQ3	                ,	// "sd_6015_p7_404"
	POS_NAV7_FREQ4	                ,	// "sd_6015_p7_405"
	POS_NAV7_FREQ5	                ,   // "sd_6015_p7_406"
	POS_OBS71		                ,   // "sd_6015_p7_407"
	POS_OBS72		                ,   // "sd_6015_p7_408"
	POS_OBS73		                ,	// "sd_6015_p7_409"
	POS_PRESSRATE	                ,	// "sd_6015_p7_410"
	POS_DME71		                ,   // "sd_6015_p7_411"
	POS_DME72		                ,   // "sd_6015_p7_412"
	POS_DME73		                ,   // "sd_6015_p7_413"
	POS_DME74		                ,	// "sd_6015_p7_414"
	POS_DME75		                ,	// "sd_6015_p7_415"
	POS_DME71M		                ,	// "sd_6015_p7_416"
	POS_DME72M		                ,	// "sd_6015_p7_417"
	POS_DME73M		                ,	// "sd_6015_p7_418"
	POS_DME74M		                ,   // "sd_6015_p7_419"
	POS_DME75M		                ,   // "sd_6015_p7_420"

	// P23
	POS_NAV_P0		                ,	// "sd_6015_p23_401"
	POS_NAV_P1		                ,	// "sd_6015_p23_402"
	POS_NAV_P4		                ,	// "sd_6015_p23_403"
	POS_NAV_P5		                ,	// "sd_6015_p23_404"
	POS_NAV_P6		                ,	// "sd_6015_p23_405"
	POS_NAV_P7		                ,	// "sd_6015_p23_406"
	POS_NAV_P8		                ,	// "sd_6015_p23_407"
	POS_NAV_P9		                ,	// "sd_6015_p23_408"

	//
	POS_MAX
};
