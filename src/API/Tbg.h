/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Tbg.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

enum TBG {                       	
	TBG_WARN_GEAR			    ,	// "sd_6015_pa_801"
	TBG_GEAR1_UP			    ,	// "sd_6015_pa_802"
	TBG_GEAR2_UP			    ,	// "sd_6015_pa_803"
	TBG_GEAR3_UP			    ,	// "sd_6015_pa_804"
	TBG_WARN_FLAP			    ,	// "sd_6015_pa_805"
	TBG_GEAR1_DOWN			    ,	// "sd_6015_pa_806"
	TBG_GEAR2_DOWN			    ,	// "sd_6015_pa_807"
	TBG_GEAR3_DOWN			    ,	// "sd_6015_pa_808"
	TBG_MAX
};

