/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Lmp.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

enum LMP {
	// ����� 
	LMP_ENG1_BAY_ON_FIRE			,   // "sd_6015_pa_901"
	LMP_ENG2_BAY_ON_FIRE        	,	// "sd_6015_pa_902"
	LMP_ENG3_BAY_ON_FIRE        	,	// "sd_6015_pa_903"
	LMP_ENG1_ON_FIRE            	,	// "sd_6015_pa_904"
	LMP_ENG2_ON_FIRE            	,	// "sd_6015_pa_905"
	LMP_ENG3_ON_FIRE            	,	// "sd_6015_pa_906"
	LMP_EXTING1_VALVE_OPEN1     	,	// "sd_6015_pa_907"
	LMP_EXTING2_VALVE_OPEN1     	,	// "sd_6015_pa_908"
	LMP_EXTING3_VALVE_OPEN1     	,	// "sd_6015_pa_909"
	LMP_EXTING1_VALVE_OPEN2     	,	// "sd_6015_pa_910"
	LMP_EXTING2_VALVE_OPEN2     	,	// "sd_6015_pa_911"
	LMP_EXTING3_VALVE_OPEN2     	,	// "sd_6015_pa_912"
	LMP_FIRE_SND_ALARM_OFF      	,	// "sd_6015_pa_913"
	LMP_STAIRS_DWN              	,	// "sd_6015_pa_914"
	LMP_INV_115V_FAIL           	,	// "sd_6015_pa_915"
	LMP_GROUND_ELEC_CONNECTED   	,	// "sd_6015_pa_916"
	LMP_DIF_AIR_PRESS_DROP      	,	// "sd_6015_pa_917"
	LMP_AIR_PRESSBCKCNTRUNITON  	,	// "sd_6015_pa_918"
	LMP_AIR_PRESS_COND_SYS_OFF  	,	// "sd_6015_pa_919"
	LMP_AIR_CONDSYSTURBOCOOLERON	,	// "sd_6015_pa_920"
	LMP_GMK_ZAP                 	,	// "sd_6015_pa_921"
	LMP_GMK_OSN                 	,	// "sd_6015_pa_922"
	LMP_FIRE_EXTING1_RDY        	,	// "sd_6015_pa_923"
	LMP_FIRE_EXTING2_RDY        	,	// "sd_6015_pa_924"
	LMP_FIRE_EXTING3_RDY        	,	// "sd_6015_pa_925"
	LMP_FIRE_EXTING4_RDY        	,	// "sd_6015_pa_926"
	LMP_ENG1PK                  	,	// "sd_6015_pa_927"
	LMP_ENG2PK                  	,	// "sd_6015_pa_928"
	LMP_ENG3PK                  	,	// "sd_6015_pa_929"

	// P4
	LMP_REVERSEON                   ,	// "sd_6015_p4_901"
	LMP_REVERSEOFF                  ,	// "sd_6015_p4_902"
	LMP_CONTR_REVERSEON             ,	// "sd_6015_p4_903"
	LMP_CONTR_REVERSEOFF            ,	// "sd_6015_p4_904"
	LMP_TOPL_LEV                    ,	// "sd_6015_p4_905"
	LMP_TOPL_PRAV                   ,	// "sd_6015_p4_906"
	LMP_OBYED                       ,	// "sd_6015_p4_907"
	LMP_KOLTSEV                     ,	// "sd_6015_p4_908"
	LMP_ACT_TEST_FAIL               ,	// "sd_6015_p4_909"
	LMP_ACT_TEST_OK                 ,	// "sd_6015_p4_910"
	LMP_ZAR_AVAR_TORM               ,	// "sd_6015_p4_911"
	LMP_TRIM_ELERON                 ,	// "sd_6015_p4_912"
	LMP_TRIM_RUDDER                 ,	// "sd_6015_p4_913"
	LMP_AP_RDY                      ,	// "sd_6015_p4_914"
	LMP_K1                          ,	// "sd_6015_p4_915"
	LMP_G1                          ,	// "sd_6015_p4_916"
	LMP_K2                          ,	// "sd_6015_p4_917"
	LMP_G2                          ,	// "sd_6015_p4_918"

	// P7
	LMP_ENG1_DEICE                  ,	// "sd_6015_p7_901"
	LMP_ENG2_DEICE                  ,	// "sd_6015_p7_902"
	LMP_ENG3_DEICE                  ,	// "sd_6015_p7_903"

	//
	LMP_MAX                      	 
};                               
