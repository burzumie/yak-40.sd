/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Azs.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Azs.h"
#include "ApiName.h"

#ifdef _DEBUG
#define sd_6015_p0_001	"FIRE SOUND ALARM: %(V:OFF|ON)%"                          
#define sd_6015_p0_002	"GEAR WARNING: %(V:ENABLED|DISABLED)%"                    
#define sd_6015_p0_003	"GEAR WARNING: %(V:ENABLED|DISABLED)%"                    
#define sd_6015_p0_004	"A-SKID: %(V:OFF|ON)%"                                    
#define sd_6015_p0_005	"ADI BCKUP PWR: %(V:AUTO|MAN)%"                           
#define sd_6015_p0_006	"ADI BCKUP PWR: %(V:AUTO|MAN)%"                           
#define sd_6015_p0_007	"APU FUEL VALVE: %(V:CLOSED|OPENED)%"                     
#define sd_6015_p0_008	"WARN LTS: %(V:BRT|DIM)%"                                 
#define sd_6015_p0_009	"EMERG EXIT LT: %(V:OFF|ON)%"                             
#define sd_6015_p0_010	"WHT DECK FLOOD LT: %(V:OFF|ON)%"							
#define sd_6015_p0_011	"RED PNL LT: %(V:OFF|ON)%"								
#define sd_6015_p0_012	"FUEL IND: %(V:SUM|L TANK|R TANK)%"						
																					
																					
#define sd_6015_p1_001	"ELEC: %(V:OFF|BATTERY|GROUND PWR)%"                      
#define sd_6015_p1_002	"INV1 36V: %(V:OFF|ON)%"                                  
#define sd_6015_p1_003	"INV2 36V: %(V:OFF|ON)%"                                  
#define sd_6015_p1_004	"115V BCKUP: %(V:AUTO|MAN)%"                              
#define sd_6015_p1_005	"115V BCKUP: %(V:AUTO|MAN)%"                              
#define sd_6015_p1_006	"INV1 115V: %(V:OFF|ON)%"                                 
#define sd_6015_p1_007	"INV2 115V: %(V:OFF|ON)%"                                 
#define sd_6015_p1_008	"AIR PRESS DROP EMERG: %(V:OFF|ON)%"                      
#define sd_6015_p1_009	"AIR PRESS DROP EMERG: %(V:OFF|ON)%"                      
#define sd_6015_p1_010	"AIR PRESS CONTROL BCKUP: %(V:OFF|ON)%"                   
#define sd_6015_p1_011	"AIR PRESS CONTROL BCKUP: %(V:OFF|ON)%"                   
#define sd_6015_p1_012	"AIR PRESS SYS MASTER EMERG: %(V:OFF|ON)%"                
#define sd_6015_p1_013	"AIR PRESS SYS MASTER EMERG: %(V:OFF|OPEN|CLOSE)%"
#define sd_6015_p1_014	"AIR PRESS SYS SOUND ALARM: %(V:OFF|ON)%"
#define sd_6015_p1_015	"AIR PRESS SYS MASTER: %(V:OFF|OPEN|CLOSE)%"
#define sd_6015_p1_016	"AIR COND SYS TEMP CNTRL CABIN: %(V:OFF|WARM|COLD|AUTO)%" 
#define sd_6015_p1_017	"AIR COND SYS TEMP CNTRL DUCT: %(V:OFF|WARM|COLD|AUTO)%"  
#define sd_6015_p1_018	"BLEEDAIR EXTRACT VALVE: %(V:OFF|OPEN|CLOSE|AUTO)%"       
#define sd_6015_p1_019	"BLEEDAIR EXTRACT RATIO: %(V:LOW|HIGH)%"                  
#define sd_6015_p1_020	"DECK COND: %(V:LOW|HIGH)%"                               
#define sd_6015_p1_021	"STAIRS PWR: %(V:OFF|ON)%"                                
#define sd_6015_p1_022	"STAIRS: %(V:RETRACT|EXTEND)%"                            
#define sd_6015_p1_023	"STAIRS: %(V:RETRACT|EXTEND)%"                            
#define sd_6015_p1_024	"NO SMOKING SIGNS: %(V:OFF|ON)%"                          
#define sd_6015_p1_025	"RED SPOT LTS: %(V:OFF|ON)%"                              
#define sd_6015_p1_026	"RED SPOT LTS: %(V:OFF|ON)%"                              
#define sd_6015_p1_027	"GENERATOR1: %(V:OFF|ON)%"                                
#define sd_6015_p1_028	"GENERATOR2: %(V:OFF|ON)%"                                
#define sd_6015_p1_029	"GENERATOR3: %(V:OFF|ON)%"                                
#define sd_6015_p1_030	"HEMISPHERE SWITCH: %(V:|NORTH|SOUTH)%"                   
#define sd_6015_p1_031	"MODE SELECTOR: %(V:|MK|GPK)%"                            
#define sd_6015_p1_032	"TEST"                                                    
#define sd_6015_p1_033	"COURSE SELECTOR"                                         
#define sd_6015_p1_034	"GYROUNIT SWITCH: %(V:|AUXL|MAIN)%"                       
																					
																					
#define sd_6015_p4_001	"EMERG ENG1 CUT"											
#define sd_6015_p4_002	"EMERG ENG2 CUT"											
#define sd_6015_p4_003	"EMERG ENG3 CUT"											
#define sd_6015_p4_004	"X-FEED: %(V:OFF|ON)%"									
#define sd_6015_p4_005	"L FUEL PUMP: %(V:OFF|ON)%"								
#define sd_6015_p4_006	"R FUEL PUMP: %(V:OFF|ON)%"								
#define sd_6015_p4_007	"R FUEL PUMP EMERG: %(V:OFF|OFF|ON|ON)%"					
#define sd_6015_p4_008	"PRESS INSTR PWR: %(V:OFF|ON)%"							
#define sd_6015_p4_009	"STAB MAIN CONTR: %(V:ON|ON|OFF)%"						
#define sd_6015_p4_010	"EMERG GEAR EXT: %(V:OFF|OFF|ON|ON)%"         			
#define sd_6015_p4_011	"GEAR: %(V:NEUTRAL|DOWN RDY|DOWN|UP RDY|UP)%" 			
#define sd_6015_p4_012	"FLAPS MAIN: %(V:OFF|NEUTR|EXT|RETRACT)%"     			
#define sd_6015_p4_013	"FLAPS EMERG EXT: %(V:OFF|OFF|ON)%"           			
#define sd_6015_p4_014	"THR REVERSE: %(V:OFF|RDY|OPEN|CLOSE)%"       			
#define sd_6015_p4_015	"FUEL PUMP LO: %(V:OFF|RIGHT|LEFT)%"          			
#define sd_6015_p4_016	"X-FEED TANKS: %(V:OFF|OFF|ON|ON)%"           			
#define sd_6015_p4_017	"X-FEED PUMPS: %(V:OFF|OFF|ON|ON)%"           			
#define sd_6015_p4_018	"HYD PUMP: %(V:AUTO|AUTO|MAN)%"							
#define sd_6015_p4_019	"AP PWR: %(V:OFF|ON)%"									
#define sd_6015_p4_020	"AP PITCH HOLD: %(V:OFF|ON)%"								
#define sd_6015_p4_021	"AP CMD: %(V:OFF|ON)%"									
#define sd_6015_p4_022	"AP ALT HLD: %(V:OFF|ON)%"								
#define sd_6015_p4_023	"TRIM AILER"												
#define sd_6015_p4_024	"TRIM RUDDER"												
#define sd_6015_p4_025	"NAV RCVR: %(V:ILS|VOR)%"														
#define sd_6015_p4_026	"IND LIGHTS: %(V:BRT|DIM)%"														
#define sd_6015_p4_027	""														
#define sd_6015_p4_028	""
#define sd_6015_p4_029	""
#define sd_6015_p4_030	""
#define sd_6015_p4_031	""
#define sd_6015_p4_032	""
#define sd_6015_p4_033	""
#define sd_6015_p4_034	""
#define sd_6015_p4_035	""
#define sd_6015_p4_036	""
#define sd_6015_p4_037	""
#define sd_6015_p4_038	""
#define sd_6015_p4_039	""
#define sd_6015_p4_040	""
#define sd_6015_p4_041	""
																					
																					
#define sd_6015_p5_001	"NOISE FLTR: %(V:OFF|ON)%"                    			
#define sd_6015_p5_002	"NOISE FLTR: %(V:OFF|ON)%"                    			
#define sd_6015_p5_003	"L LIGHT LEFT: %(V:OFF|LAND|TAXI)%"           			
#define sd_6015_p5_004	"L LIGHT RIGHT: %(V:OFF|LAND|TAXI)%"  					
#define sd_6015_p5_005	"L LIGHTS %(V:RETRACT|EXTEND)"        					
#define sd_6015_p5_006	"LIGHTS NAV: %(V:OFF|ON)%"            					
#define sd_6015_p5_007	"LIGHTS BCN: %(V:OFF|ON)%"            					
#define sd_6015_p5_008	"ENG1 FUEL VLV: %(V:OFF|OFF|ON|ON)%"  					
#define sd_6015_p5_009	"ENG2 FUEL VLV: %(V:OFF|OFF|ON|ON)%"  					
#define sd_6015_p5_010	"ENG3 FUEL VLV: %(V:OFF|OFF|ON|ON)%"  					
#define sd_6015_p5_011	"WNDSHLD DEICE L: %(V:OFF|ON)%"       					
#define sd_6015_p5_012	"WNDSHLD DEICE L: %(V:OFF|ON)%"       					
#define sd_6015_p5_013	"WNDSHLD DEICE RATE: %(V:LOW|HIGH)%"  					
#define sd_6015_p5_014	"ADF1 ACTIVE FREQ: %(V:LEFT|RIGHT)%"						
#define sd_6015_p5_015	"ADF2 ACTIVE FREQ: %(V:LEFT|RIGHT)%"						
#define sd_6015_p5_016	"ANT L-R"													
#define sd_6015_p5_017	"ANT L-R"													
#define sd_6015_p5_018	"RCVR: %(V:CW|AM)%"										
#define sd_6015_p5_019	"RCVR: %(V:CW|AM)%"										
																					
																					
#define sd_6015_p6_001	"RAD ALTIMETER PWR: %(V:OFF|ON)%"							
#define sd_6015_p6_002	"XDPR PWR: %(V:OFF|ON)%"									
#define sd_6015_p6_003	"HIJACK ALARM: %(V:OFF|ON)%"								
#define sd_6015_p6_004	"MODE: %(V:ILS|VOR-DME)%"									
#define sd_6015_p6_005	"PWR: %(V:OFF|ON)%"										
#define sd_6015_p6_006	"APU STARTER ELEC: %(V:OFF|ON)%"							
#define sd_6015_p6_007	"APU STARTER MODE: %(V:OFF|START|DUMP FUEL|COLD SPIN)%"	
#define sd_6015_p6_008	"ENG STARTER ELEC: %(V:OFF|ON)%"							
#define sd_6015_p6_009	"ENG STARTER MODE: %(V:OFF|COLD SPIN|START|DUMP FUEL)%"	
#define sd_6015_p6_010	"ENG SELECT: %(V:NONE|2|1|3)%"							
#define sd_6015_p6_011	"MAYDAY TX: %(V:OFF|RDY|ON)%"								
																					
																					
#define sd_6015_p7_001	"MODE: %(V:ILS|VOR-DME)%"         						
#define sd_6015_p7_002	"PWR: %(V:OFF|ON)%"               						
#define sd_6015_p7_003	"ENG1 DEICE: %(V:OFF|ON)%"        						
#define sd_6015_p7_004	"ENG2 DEICE: %(V:OFF|ON)%"        						
#define sd_6015_p7_005	"ENG3 DEICE: %(V:OFF|ON)%"        						
#define sd_6015_p7_006	"PRESSURIZATION SYS: %(V:OFF|ON)%"						
#define sd_6015_p7_007	"PITOT HEAT L: %(V:OFF|ON)%"      						
#define sd_6015_p7_008	"PITOT HEAT R: %(V:OFF|ON)%"      						
#define sd_6015_p7_009	"ENG1 ICE WARN: %(V:OFF|ON)%"     						
#define sd_6015_p7_010	"ENG2 ICE WARN: %(V:OFF|ON)%"     						
																					
																					
#define sd_6015_p8_001	"COM1 BUS: %(V:OFF|ON)%"                  				
#define sd_6015_p8_002	"CAPT VSI BUS: %(V:OFF|ON)%"              				
#define sd_6015_p8_003	"CAPT ADI MAIN BUS: %(V:OFF|ON)%"         				
#define sd_6015_p8_004	"FUEL IND BUS: %(V:OFF|ON)%"              				
#define sd_6015_p8_005	"LAMP TEST BUS: %(V:OFF|ON)%"             				
#define sd_6015_p8_006	"ENG1 IND BUS: %(V:OFF|ON)%"              				
#define sd_6015_p8_007	"ENG2 IND BUS: %(V:OFF|ON)%"              				
#define sd_6015_p8_008	"ENG3 IND BUS: %(V:OFF|ON)%"              				
#define sd_6015_p8_009	"CAPT ADI BACKUP BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_010	"ADF1 BUS: %(V:OFF|ON)%"                  				
#define sd_6015_p8_011	"XPDR BUS: %(V:OFF|ON)%"                  				
#define sd_6015_p8_012	"ELEC 36V BACKUP BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_013	"INV1 115V BUS: %(V:OFF|ON)%"             				
#define sd_6015_p8_014	"INV2 115V BUS: %(V:OFF|ON)%"             				
#define sd_6015_p8_015	"AUTO BACKUP 115V BUS: %(V:OFF|ON)%"      				
#define sd_6015_p8_016	"ENG1 FIRE ALARM BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_017	"ENG2 FIRE ALARM BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_018	"ENG3 FIRE ALARM BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_019	"STALL WARN BUS: %(V:OFF|ON)%"            				
#define sd_6015_p8_020	"FUEL PUMP AUTO BACKUP BUS: %(V:OFF|ON)%" 				
#define sd_6015_p8_021	"FUEL X-FEED BUS: %(V:OFF|ON)%"           				
#define sd_6015_p8_022	"ENG1 FUEL VALVE BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_023	"ENG2 FUEL VALVE BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_024	"ENG3 FUEL VALVE BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_025	"FIRE EXT 1 BUS: %(V:OFF|ON)%"            				
#define sd_6015_p8_026	"FIRE EXT 2 BUS: %(V:OFF|ON)%"            				
#define sd_6015_p8_027	"FIRE EXT 3 BUS: %(V:OFF|ON)%"            				
#define sd_6015_p8_028	"AIR STARTER IND BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_029	"GEAR UP HORN BUS: %(V:OFF|ON)%"          				
#define sd_6015_p8_030	"APU IGNIT BUS: %(V:OFF|ON)%"             				
#define sd_6015_p8_031	"ENG1 IGNIT BUS: %(V:OFF|ON)%"            				
#define sd_6015_p8_032	"ENG2 IGNIT BUS: %(V:OFF|ON)%"            				
#define sd_6015_p8_033	"ENG3 IGNIT BUS: %(V:OFF|ON)%"            				
#define sd_6015_p8_034	"FIRE EXT1 VALVE BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_035	"FIRE EXT2 VALVE BUS: %(V:OFF|ON)%"       				
#define sd_6015_p8_036	"FIRE EXT3 VALVE BUS: %(V:OFF|ON)%"       				
																					
																					
#define sd_6015_p9_001	"COM2 BUS: %(V:OFF|ON)%"                  				
#define sd_6015_p9_002	"GROUND PWR BUS: %(V:OFF|ON)%"            				
#define sd_6015_p9_003	"COPLT ADI BUS: %(V:OFF|ON)%"             				
#define sd_6015_p9_004	"GMK COURSE SYS BUS: %(V:OFF|ON)%"        				
#define sd_6015_p9_005	"HYDRO SYS BUS: %(V:OFF|ON)%"             				
#define sd_6015_p9_006	"AUTOPILOT BUS: %(V:OFF|ON)%"             				
#define sd_6015_p9_007	"GEAR IND BUS: %(V:OFF|ON)%"              				
#define sd_6015_p9_008	"SIGNAL ROCKETS BUS: %(V:OFF|ON)%"        				
#define sd_6015_p9_009	"ILS SYS BUS: %(V:OFF|ON)%"               				
#define sd_6015_p9_010	"ADF2 BUS: %(V:OFF|ON)%"                  				
#define sd_6015_p9_011	"INV 115V EMERG SWITCH BUS: %(V:OFF|ON)%" 				
#define sd_6015_p9_012	"STAB EMERG BUS: %(V:OFF|ON)%"            				
#define sd_6015_p9_013	"FLAPS EMERG BUS: %(V:OFF|ON)%"           				
#define sd_6015_p9_014	"GEAR EMERG BUS: %(V:OFF|ON)%"            				
#define sd_6015_p9_015	"ANTI ICE SYS BUS: %(V:OFF|ON)%"          				
#define sd_6015_p9_016	"EXTREME BANK IND BUS: %(V:OFF|ON)%"      				
#define sd_6015_p9_017	"AIR COND SYS BUS: %(V:OFF|ON)%"          				
#define sd_6015_p9_018	"STAIRS SERVO BUS: %(V:OFF|ON)%"          				
#define sd_6015_p9_019	"TURBO COOLER UNIT BUS: %(V:OFF|ON)%"     				
#define sd_6015_p9_020	"NOSE LEG STEER BUS: %(V:OFF|ON)%"        				
#define sd_6015_p9_021	"FUEL X-FEED BUS: %(V:OFF|ON)%"           				
#define sd_6015_p9_022	"ENG1 DE ICE BUS: %(V:OFF|ON)%"           				
#define sd_6015_p9_023	"ENG2 DE ICE BUS: %(V:OFF|ON)%"           				
#define sd_6015_p9_024	"ENG3 DE ICE BUS: %(V:OFF|ON)%"           				
#define sd_6015_p9_025	"BRAKE SYS BUS: %(V:OFF|ON)%"             				
#define sd_6015_p9_026	"DECK HEATER BUS: %(V:OFF|ON)%"           				
#define sd_6015_p9_027	"CABIN HEATER BUS: %(V:OFF|ON)%"          				
#define sd_6015_p9_028	"TAXI LIGHT L BUS: %(V:OFF|ON)%"          				
#define sd_6015_p9_029	"LANDING LIGHT L BUS: %(V:OFF|ON)%"       				
#define sd_6015_p9_030	"TAXI LIGHT R BUS: %(V:OFF|ON)%"          				
#define sd_6015_p9_031	"LANDING LIGHT R BUS: %(V:OFF|ON)%"       				
#define sd_6015_p9_032	"ALER TRIM BUS: %(V:OFF|ON)%"             				
#define sd_6015_p9_033	"RUDDER TRIM BUS: %(V:OFF|ON)%"           				
#define sd_6015_p9_034	"STAB MAIN BUS: %(V:OFF|ON)%"             				
#define sd_6015_p9_035	"FLAPS MAIN BUS: %(V:OFF|ON)%"            				
#define sd_6015_p9_036	"GEAR MAIN BUS: %(V:OFF|ON)%"  

#define sd_6015_p23_01	""
#define sd_6015_p23_02	""

#define sd_6015_pa_001	""

#else

#define sd_6015_p0_001	""
#define sd_6015_p0_002	""
#define sd_6015_p0_003	""
#define sd_6015_p0_004	""
#define sd_6015_p0_005	""
#define sd_6015_p0_006	""
#define sd_6015_p0_007	""
#define sd_6015_p0_008	""
#define sd_6015_p0_009	""
#define sd_6015_p0_010	""
#define sd_6015_p0_011	""
#define sd_6015_p0_012	""
							
 							
#define sd_6015_p1_001	""
#define sd_6015_p1_002	""
#define sd_6015_p1_003	""
#define sd_6015_p1_004	""
#define sd_6015_p1_005	""
#define sd_6015_p1_006	""
#define sd_6015_p1_007	""
#define sd_6015_p1_008	""
#define sd_6015_p1_009	""
#define sd_6015_p1_010	""
#define sd_6015_p1_011	""
#define sd_6015_p1_012	""
#define sd_6015_p1_013	""
#define sd_6015_p1_014	""
#define sd_6015_p1_015	""
#define sd_6015_p1_016	""
#define sd_6015_p1_017	""
#define sd_6015_p1_018	""
#define sd_6015_p1_019	""
#define sd_6015_p1_020	""
#define sd_6015_p1_021	""
#define sd_6015_p1_022	""
#define sd_6015_p1_023	""
#define sd_6015_p1_024	""
#define sd_6015_p1_025	""
#define sd_6015_p1_026	""
#define sd_6015_p1_027	""
#define sd_6015_p1_028	""
#define sd_6015_p1_029	""
#define sd_6015_p1_030	""
#define sd_6015_p1_031	""
#define sd_6015_p1_032	""
#define sd_6015_p1_033	""
#define sd_6015_p1_034	""
							
							
#define sd_6015_p4_001	""
#define sd_6015_p4_002	""
#define sd_6015_p4_003	""
#define sd_6015_p4_004	""
#define sd_6015_p4_005	""
#define sd_6015_p4_006	""
#define sd_6015_p4_007	""
#define sd_6015_p4_008	""
#define sd_6015_p4_009	""
#define sd_6015_p4_010	""
#define sd_6015_p4_011	""
#define sd_6015_p4_012	""
#define sd_6015_p4_013	""
#define sd_6015_p4_014	""
#define sd_6015_p4_015	""
#define sd_6015_p4_016	""
#define sd_6015_p4_017	""
#define sd_6015_p4_018	""
#define sd_6015_p4_019	""
#define sd_6015_p4_020	""
#define sd_6015_p4_021	""
#define sd_6015_p4_022	""
#define sd_6015_p4_023	""
#define sd_6015_p4_024	""
#define sd_6015_p4_025	""
#define sd_6015_p4_026	""
#define sd_6015_p4_027	""
#define sd_6015_p4_028	""
#define sd_6015_p4_029	""
#define sd_6015_p4_030	""
#define sd_6015_p4_031	""
#define sd_6015_p4_032	""
#define sd_6015_p4_033	""
#define sd_6015_p4_034	""
#define sd_6015_p4_035	""
#define sd_6015_p4_036	""
#define sd_6015_p4_037	""
#define sd_6015_p4_038	""
#define sd_6015_p4_039	""
#define sd_6015_p4_040	""
#define sd_6015_p4_041	""
							
							
#define sd_6015_p5_001	""
#define sd_6015_p5_002	""
#define sd_6015_p5_003	""
#define sd_6015_p5_004	""
#define sd_6015_p5_005	""
#define sd_6015_p5_006	""
#define sd_6015_p5_007	""
#define sd_6015_p5_008	""
#define sd_6015_p5_009	""
#define sd_6015_p5_010	""
#define sd_6015_p5_011	""
#define sd_6015_p5_012	""
#define sd_6015_p5_013	""
#define sd_6015_p5_014	""
#define sd_6015_p5_015	""
#define sd_6015_p5_016	""
#define sd_6015_p5_017	""
#define sd_6015_p5_018	""
#define sd_6015_p5_019	""
							
 							
#define sd_6015_p6_001	""
#define sd_6015_p6_002	""
#define sd_6015_p6_003	""
#define sd_6015_p6_004	""
#define sd_6015_p6_005	""
#define sd_6015_p6_006	""
#define sd_6015_p6_007	""
#define sd_6015_p6_008	""
#define sd_6015_p6_009	""
#define sd_6015_p6_010	""
#define sd_6015_p6_011	""
							
							
#define sd_6015_p7_001	""
#define sd_6015_p7_002	""
#define sd_6015_p7_003	""
#define sd_6015_p7_004	""
#define sd_6015_p7_005	""
#define sd_6015_p7_006	""
#define sd_6015_p7_007	""
#define sd_6015_p7_008	""
#define sd_6015_p7_009	""
#define sd_6015_p7_010	""
							
							
#define sd_6015_p8_001	""
#define sd_6015_p8_002	""
#define sd_6015_p8_003	""
#define sd_6015_p8_004	""
#define sd_6015_p8_005	""
#define sd_6015_p8_006	""
#define sd_6015_p8_007	""
#define sd_6015_p8_008	""
#define sd_6015_p8_009	""
#define sd_6015_p8_010	""
#define sd_6015_p8_011	""
#define sd_6015_p8_012	""
#define sd_6015_p8_013	""
#define sd_6015_p8_014	""
#define sd_6015_p8_015	""
#define sd_6015_p8_016	""
#define sd_6015_p8_017	""
#define sd_6015_p8_018	""
#define sd_6015_p8_019	""
#define sd_6015_p8_020	""
#define sd_6015_p8_021	""
#define sd_6015_p8_022	""
#define sd_6015_p8_023	""
#define sd_6015_p8_024	""
#define sd_6015_p8_025	""
#define sd_6015_p8_026	""
#define sd_6015_p8_027	""
#define sd_6015_p8_028	""
#define sd_6015_p8_029	""
#define sd_6015_p8_030	""
#define sd_6015_p8_031	""
#define sd_6015_p8_032	""
#define sd_6015_p8_033	""
#define sd_6015_p8_034	""
#define sd_6015_p8_035	""
#define sd_6015_p8_036	""
							
							
#define sd_6015_p9_001	""
#define sd_6015_p9_002	""
#define sd_6015_p9_003	""
#define sd_6015_p9_004	""
#define sd_6015_p9_005	""
#define sd_6015_p9_006	""
#define sd_6015_p9_007	""
#define sd_6015_p9_008	""
#define sd_6015_p9_009	""
#define sd_6015_p9_010	""
#define sd_6015_p9_011	""
#define sd_6015_p9_012	""
#define sd_6015_p9_013	""
#define sd_6015_p9_014	""
#define sd_6015_p9_015	""
#define sd_6015_p9_016	""
#define sd_6015_p9_017	""
#define sd_6015_p9_018	""
#define sd_6015_p9_019	""
#define sd_6015_p9_020	""
#define sd_6015_p9_021	""
#define sd_6015_p9_022	""
#define sd_6015_p9_023	""
#define sd_6015_p9_024	""
#define sd_6015_p9_025	""
#define sd_6015_p9_026	""
#define sd_6015_p9_027	""
#define sd_6015_p9_028	""
#define sd_6015_p9_029	""
#define sd_6015_p9_030	""
#define sd_6015_p9_031	""
#define sd_6015_p9_032	""
#define sd_6015_p9_033	""
#define sd_6015_p9_034	""
#define sd_6015_p9_035	""
#define sd_6015_p9_036	""

#define sd_6015_p23_01	""
#define sd_6015_p23_02	""

#define sd_6015_pa_001	""

#endif

ApiName AzsNames[AZS_MAX]={
	// P0
	{"sd_6015_p0_001",sd_6015_p0_001,"",0,0,false,false},   // AZS_FIRE_SOUND_ALARM		
	{"sd_6015_p0_002",sd_6015_p0_002,"",0,0,false,false},   // AZS_GEAR_WARNING_COV		
	{"sd_6015_p0_003",sd_6015_p0_003,"",0,0,false,false},   // AZS_GEAR_WARNING			
	{"sd_6015_p0_004",sd_6015_p0_004,"",0,0,false,false},   // AZS_AUTO_SKID				
	{"sd_6015_p0_005",sd_6015_p0_005,"",0,0,false,false},   // AZS_ADI_BACKUP_POWER_COV	
	{"sd_6015_p0_006",sd_6015_p0_006,"",0,0,false,false},   // AZS_ADI_BACKUP_POWER		
	{"sd_6015_p0_007",sd_6015_p0_007,"",0,0,false,false},   // AZS_APU_FUEL_VALVE			
	{"sd_6015_p0_008",sd_6015_p0_008,"",0,0,false,false},   // AZS_WARNING_LIGHTS			
	{"sd_6015_p0_009",sd_6015_p0_009,"",0,0,false,false},   // AZS_EMERG_EXIT_LIGHTS		
	{"sd_6015_p0_010",sd_6015_p0_010,"",0,0,false,false},   // AZS_WHITE_DECK_FLOOD_LIGHT	
	{"sd_6015_p0_011",sd_6015_p0_011,"",0,0,false,false},   // AZS_RED_PANEL_LIGHT			
	{"sd_6015_p0_012",sd_6015_p0_012,"",0,0,false,false},   // AZS_FUEL_IND				
												 
	// P1										 
	{"sd_6015_p1_001",sd_6015_p1_001,"",0,2,false,false},   // AZS_POWERSOURCE                 	
	{"sd_6015_p1_002",sd_6015_p1_002,"",0,0,false,false},   // AZS_INV1_36V                    	
	{"sd_6015_p1_003",sd_6015_p1_003,"",0,0,false,false},   // AZS_INV2_36V                    	
	{"sd_6015_p1_004",sd_6015_p1_004,"",0,0,false,false},   // AZS_115V_BCKUP_COV              	
	{"sd_6015_p1_005",sd_6015_p1_005,"",0,0,false,false},   // AZS_115V_BCKUP                  	
	{"sd_6015_p1_006",sd_6015_p1_006,"",0,0,false,false},   // AZS_INV1_115V                   	
	{"sd_6015_p1_007",sd_6015_p1_007,"",0,0,false,false},   // AZS_INV2_115V                   	
	{"sd_6015_p1_008",sd_6015_p1_008,"",0,0,false,false},   // AZS_AIR_PRESS_DROP_EMERG_COV    	
	{"sd_6015_p1_009",sd_6015_p1_009,"",0,0,false,false},   // AZS_AIR_PRESS_DROP_EMERG        	
	{"sd_6015_p1_010",sd_6015_p1_010,"",0,0,false,false},   // AZS_AIR_PRESS_CONTROL_BCKUP_COV 	
	{"sd_6015_p1_011",sd_6015_p1_011,"",0,0,false,false},   // AZS_AIR_PRESS_CONTROL_BCKUP     	
	{"sd_6015_p1_012",sd_6015_p1_012,"",0,0,false,false},   // AZS_AIR_PRESS_SYS_MASTER_EMERG_COV	
	{"sd_6015_p1_013",sd_6015_p1_013,"",0,0,false,false},   // AZS_AIR_PRESS_SYS_MASTER_EMERG  	
	{"sd_6015_p1_014",sd_6015_p1_014,"",0,0,false,false},   // AZS_AIR_PRESS_SYS_SOUND_ALARM   	
	{"sd_6015_p1_015",sd_6015_p1_015,"",0,0,false,false},   // AZS_AIR_PRESS_SYS_MASTER        	
	{"sd_6015_p1_016",sd_6015_p1_016,"",0,0,false,false},   // AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN	
	{"sd_6015_p1_017",sd_6015_p1_017,"",0,0,false,false},   // AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT 	
	{"sd_6015_p1_018",sd_6015_p1_018,"",0,0,false,false},   // AZS_BLEEDAIR_EXTRACT_VALVE      
	{"sd_6015_p1_019",sd_6015_p1_019,"",0,0,false,false},   // AZS_BLEEDAIR_EXTRACT_RATIO      
	{"sd_6015_p1_020",sd_6015_p1_020,"",0,0,false,false},   // AZS_DECK_COND                   
	{"sd_6015_p1_021",sd_6015_p1_021,"",0,0,false,false},   // AZS_STAIRS_PWR                  
	{"sd_6015_p1_022",sd_6015_p1_022,"",0,0,false,false},   // AZS_STAIRS_COV                  
	{"sd_6015_p1_023",sd_6015_p1_023,"",0,0,false,false},   // AZS_STAIRS                      
	{"sd_6015_p1_024",sd_6015_p1_024,"",0,0,false,false},   // AZS_NO_SMOKING_SIGNS            
	{"sd_6015_p1_025",sd_6015_p1_025,"",0,0,false,false},   // AZS_REDLIGHT1                   
	{"sd_6015_p1_026",sd_6015_p1_026,"",0,0,false,false},   // AZS_REDLIGHT2                   
	{"sd_6015_p1_027",sd_6015_p1_027,"",0,0,false,false},   // AZS_GENERATOR1                  
	{"sd_6015_p1_028",sd_6015_p1_028,"",0,0,false,false},   // AZS_GENERATOR2                  
	{"sd_6015_p1_029",sd_6015_p1_029,"",0,0,false,false},   // AZS_GENERATOR3                  
	{"sd_6015_p1_030",sd_6015_p1_030,"",0,0,false,false},   // AZS_GMK_HEMISPHERE	                      
	{"sd_6015_p1_031",sd_6015_p1_031,"",0,0,false,false},   // AZS_GMK_MODE		                      
	{"sd_6015_p1_032",sd_6015_p1_032,"",0,0,false,false},   // AZS_GMK_TEST		                      
	{"sd_6015_p1_033",sd_6015_p1_033,"",0,0,false,false},   // AZS_GMK_COURSE		                      
	{"sd_6015_p1_034",sd_6015_p1_034,"",0,0,false,false},   // AZS_GMK_PRI_AUX		                      
												 
	// P4										 
	{"sd_6015_p4_001",sd_6015_p4_001,"",0,0,false,false},	// AZS_EMERG_ENG1_CUT               
	{"sd_6015_p4_002",sd_6015_p4_002,"",0,0,false,false},	// AZS_EMERG_ENG2_CUT               
	{"sd_6015_p4_003",sd_6015_p4_003,"",0,0,false,false},	// AZS_EMERG_ENG3_CUT               
	{"sd_6015_p4_004",sd_6015_p4_004,"",0,0,false,false},	// AZS_ACT                          
	{"sd_6015_p4_005",sd_6015_p4_005,"",0,0,false,false},	// AZS_TOPL_LEV                     
	{"sd_6015_p4_006",sd_6015_p4_006,"",0,0,false,false},	// AZS_TOPL_PRAV                    
	{"sd_6015_p4_007",sd_6015_p4_007,"",0,0,false,false},	// AZS_TOPL_PRAV_AVAR               
	{"sd_6015_p4_008",sd_6015_p4_008,"",0,0,false,false},	// AZS_MANOM                        
	{"sd_6015_p4_009",sd_6015_p4_009,"",0,0,false,false},	// AZS_STAB_MAIN_CONTR              
	{"sd_6015_p4_010",sd_6015_p4_010,"",0,0,false,false},	// AZS_EMERG_GEAR                   	
	{"sd_6015_p4_011",sd_6015_p4_011,"",0,0,false,false},	// AZS_GEAR                         	
	{"sd_6015_p4_012",sd_6015_p4_012,"",0,0,false,false},	// AZS_FLAPS                        
	{"sd_6015_p4_013",sd_6015_p4_013,"",0,0,false,false},	// AZS_EMERG_FLAPS                  
	{"sd_6015_p4_014",sd_6015_p4_014,"",0,0,false,false},	// AZS_REVERSE                      
	{"sd_6015_p4_015",sd_6015_p4_015,"",0,0,false,false},	// AZS_FUEL_PUMP_LO                 
	{"sd_6015_p4_016",sd_6015_p4_016,"",0,0,false,false},	// AZS_XFEED_TANKS                  
	{"sd_6015_p4_017",sd_6015_p4_017,"",0,0,false,false},	// AZS_XFEED_PUMPS                  
	{"sd_6015_p4_018",sd_6015_p4_018,"",0,0,false,false},	// AZS_HYD_PUMP                     
	{"sd_6015_p4_019",sd_6015_p4_019,"",0,0,false,false},	// AZS_AP_PWR                       
	{"sd_6015_p4_020",sd_6015_p4_020,"",0,0,false,false},	// AZS_AP_PITCH_HOLD                
	{"sd_6015_p4_021",sd_6015_p4_021,"",0,0,false,false},	// AZS_AP_CMD                       
	{"sd_6015_p4_022",sd_6015_p4_022,"",0,0,false,false},	// AZS_AP_ALT_HOLD                  
	{"sd_6015_p4_023",sd_6015_p4_023,"",0,0,false,false},	// AZS_TRIM_ELERON                  
	{"sd_6015_p4_024",sd_6015_p4_024,"",0,0,false,false},	// AZS_TRIM_RUDDER                  
	{"sd_6015_p4_025",sd_6015_p4_025,"",0,0,false,false},	// AZS_KMP_RCVR                     
	{"sd_6015_p4_026",sd_6015_p4_026,"",0,0,false,false},	// AZS_KMP_DAYNIGHT                 
	{"sd_6015_p4_027",sd_6015_p4_027,"",0,0,false,false},	// AZS_KMP_RSBN_DME                 
	{"sd_6015_p4_028",sd_6015_p4_028,"",0,0,false,false},	// AZS_EMERG_ENG1_CUT_COV	               
	{"sd_6015_p4_029",sd_6015_p4_029,"",0,0,false,false},	// AZS_EMERG_ENG2_CUT_COV	               
	{"sd_6015_p4_030",sd_6015_p4_030,"",0,0,false,false},	// AZS_EMERG_ENG3_CUT_COV	               
	{"sd_6015_p4_031",sd_6015_p4_031,"",0,0,false,false},	// AZS_TOPL_PRAV_AVAR_COV	               
	{"sd_6015_p4_032",sd_6015_p4_032,"",0,0,false,false},	// AZS_STAB_MAIN_CONTR_COV	               
	{"sd_6015_p4_033",sd_6015_p4_033,"",0,0,false,false},	// AZS_EMERG_GEAR_COV		               
	{"sd_6015_p4_034",sd_6015_p4_034,"",0,0,false,false},	// AZS_FLAPS_COV			               
	{"sd_6015_p4_035",sd_6015_p4_035,"",0,0,false,false},	// AZS_EMERG_FLAPS_COV		               
	{"sd_6015_p4_036",sd_6015_p4_036,"",0,0,false,false},	// AZS_XFEED_TANKS_COV		               
	{"sd_6015_p4_037",sd_6015_p4_037,"",0,0,false,false},	// AZS_XFEED_PUMPS_COV		               
	{"sd_6015_p4_038",sd_6015_p4_038,"",0,0,false,false},	// AZS_HYD_PUMP_COV		               
	{"sd_6015_p4_039",sd_6015_p4_039,"",0,0,false,false},	// AZS_PKAI251_COV			               
	{"sd_6015_p4_040",sd_6015_p4_040,"",0,0,false,false},	// AZS_PKAI252_COV			               
	{"sd_6015_p4_041",sd_6015_p4_041,"",0,0,false,false},	// AZS_PKAI253_COV			               
												 
	// P5										 
	{"sd_6015_p5_001",sd_6015_p5_001,"",0,0,false,false},   // AZS_VHF1ON       
	{"sd_6015_p5_002",sd_6015_p5_002,"",0,0,false,false},   // AZS_VHF2ON       
	{"sd_6015_p5_003",sd_6015_p5_003,"",0,0,false,false},   // AZS_LLIGHTLEFT   
	{"sd_6015_p5_004",sd_6015_p5_004,"",0,0,false,false},   // AZS_LLIGHTRIGHT  
	{"sd_6015_p5_005",sd_6015_p5_005,"",0,0,false,false},   // AZS_LLIGHTSMOTOR 
	{"sd_6015_p5_006",sd_6015_p5_006,"",0,0,false,false},   // AZS_LIGHTSNAV    
	{"sd_6015_p5_007",sd_6015_p5_007,"",0,0,false,false},   // AZS_LIGHTSBCN    
	{"sd_6015_p5_008",sd_6015_p5_008,"",0,0,false,false},   // AZS_PKAI251      
	{"sd_6015_p5_009",sd_6015_p5_009,"",0,0,false,false},   // AZS_PKAI252      
	{"sd_6015_p5_010",sd_6015_p5_010,"",0,0,false,false},   // AZS_PKAI253      
	{"sd_6015_p5_011",sd_6015_p5_011,"",0,0,false,false},   // AZS_WINDOWHEATL  
	{"sd_6015_p5_012",sd_6015_p5_012,"",0,0,false,false},   // AZS_WINDOWHEATR
	{"sd_6015_p5_013",sd_6015_p5_013,"",0,0,false,false},   // AZS_WINDOWHEATPWR
	{"sd_6015_p5_014",sd_6015_p5_014,"",0,0,false,false},   // AZS_ARK1LEFTRIGHT
	{"sd_6015_p5_015",sd_6015_p5_015,"",0,0,false,false},   // AZS_ARK2LEFTRIGHT
	{"sd_6015_p5_016",sd_6015_p5_016,"",0,0,false,false},   // AZS_ARK1DIR     
	{"sd_6015_p5_017",sd_6015_p5_017,"",0,0,false,false},   // AZS_ARK2DIR      
	{"sd_6015_p5_018",sd_6015_p5_018,"",0,0,false,false},   // AZS_ARK1CW       
	{"sd_6015_p5_019",sd_6015_p5_019,"",0,0,false,false},   // AZS_ARK2CW       
												 
	// P6										 
	{"sd_6015_p6_001",sd_6015_p6_001,"",0,0,false,false},   // AZS_RADALT		  	
	{"sd_6015_p6_002",sd_6015_p6_002,"",0,0,false,false},   // AZS_SO72PWR		  	
	{"sd_6015_p6_003",sd_6015_p6_003,"",0,0,false,false},   // AZS_SO72EMERG	  	
	{"sd_6015_p6_004",sd_6015_p6_004,"",0,0,false,false},   // AZS_KMP1VOR		  	
	{"sd_6015_p6_005",sd_6015_p6_005,"",0,0,false,false},   // AZS_KMP1PWR		  	
	{"sd_6015_p6_006",sd_6015_p6_006,"",0,0,false,false},   // AZS_STARTAI9PWR	  	
	{"sd_6015_p6_007",sd_6015_p6_007,"",0,0,false,false},   // AZS_STARTAI9MODE  	
	{"sd_6015_p6_008",sd_6015_p6_008,"",0,0,false,false},   // AZS_STARTAI25PWR  	
	{"sd_6015_p6_009",sd_6015_p6_009,"",0,0,false,false},   // AZS_STARTAI25MODE 	
	{"sd_6015_p6_010",sd_6015_p6_010,"",0,0,false,false},   // AZS_STARTAI25SEL  	
	{"sd_6015_p6_011",sd_6015_p6_011,"",0,0,false,false},   // AZS_XPDRMAYDAY	  	
												 
	// P7										 
	{"sd_6015_p7_001",sd_6015_p7_001,"",0,0,false,false},   // AZS_KMP2VOR		 	
	{"sd_6015_p7_002",sd_6015_p7_002,"",0,0,false,false},   // AZS_KMP2PWR		 	
	{"sd_6015_p7_003",sd_6015_p7_003,"",0,0,false,false},   // AZS_ENG1HEAT	 	
	{"sd_6015_p7_004",sd_6015_p7_004,"",0,0,false,false},   // AZS_ENG2HEAT	 	
	{"sd_6015_p7_005",sd_6015_p7_005,"",0,0,false,false},   // AZS_ENG3HEAT	 	
	{"sd_6015_p7_006",sd_6015_p7_006,"",0,0,false,false},   // AZS_2077PWR   	 	
	{"sd_6015_p7_007",sd_6015_p7_007,"",0,0,false,false},   // AZS_PITOTHEAT1		
	{"sd_6015_p7_008",sd_6015_p7_008,"",0,0,false,false},   // AZS_PITOTHEAT2		
	{"sd_6015_p7_009",sd_6015_p7_009,"",0,0,false,false},   // AZS_ICEWARN1		
	{"sd_6015_p7_010",sd_6015_p7_010,"",0,0,false,false},	// AZS_ICEWARN2		
												 
	// P8										 
	{"sd_6015_p8_001",sd_6015_p8_001,"",0,0,false,false},	// AZS_COM1_BUS                    
	{"sd_6015_p8_002",sd_6015_p8_002,"",0,0,false,false},	// AZS_CAPT_VSI_BUS                
	{"sd_6015_p8_003",sd_6015_p8_003,"",0,0,false,false},	// AZS_CAPT_ADI_MAIN_BUS           
	{"sd_6015_p8_004",sd_6015_p8_004,"",0,0,false,false},	// AZS_FUEL_IND_BUS                
	{"sd_6015_p8_005",sd_6015_p8_005,"",0,0,false,false},	// AZS_LAMP_TEST_BUS               
	{"sd_6015_p8_006",sd_6015_p8_006,"",0,0,false,false},	// AZS_ENG1_IND_BUS                
	{"sd_6015_p8_007",sd_6015_p8_007,"",0,0,false,false},	// AZS_ENG2_IND_BUS                
	{"sd_6015_p8_008",sd_6015_p8_008,"",0,0,false,false},	// AZS_ENG3_IND_BUS                
	{"sd_6015_p8_009",sd_6015_p8_009,"",0,0,false,false},	// AZS_CAPT_ADI_BACKUP_BUS         
	{"sd_6015_p8_010",sd_6015_p8_010,"",0,0,false,false},	// AZS_ADF1_BUS                    
	{"sd_6015_p8_011",sd_6015_p8_011,"",0,0,false,false},	// AZS_XPDR_BUS                    
	{"sd_6015_p8_012",sd_6015_p8_012,"",0,0,false,false},	// AZS_ELEC_36V_BACKUP_BUS         
	{"sd_6015_p8_013",sd_6015_p8_013,"",0,0,false,false},	// AZS_INV1_115V_BUS               
	{"sd_6015_p8_014",sd_6015_p8_014,"",0,0,false,false},	// AZS_INV2_115V_BUS               
	{"sd_6015_p8_015",sd_6015_p8_015,"",0,0,false,false},	// AZS_AUTO_BACKUP_115V_BUS        
	{"sd_6015_p8_016",sd_6015_p8_016,"",0,0,false,false},	// AZS_ENG1_FIRE_ALARM_BUS         
	{"sd_6015_p8_017",sd_6015_p8_017,"",0,0,false,false},	// AZS_ENG2_FIRE_ALARM_BUS         
	{"sd_6015_p8_018",sd_6015_p8_018,"",0,0,false,false},	// AZS_ENG3_FIRE_ALARM_BUS         
	{"sd_6015_p8_019",sd_6015_p8_019,"",0,0,false,false},	// AZS_STALL_WARN_BUS              
	{"sd_6015_p8_020",sd_6015_p8_020,"",0,0,false,false},	// AZS_FUEL_PUMP_AUTO_BACKUP_BUS   
	{"sd_6015_p8_021",sd_6015_p8_021,"",0,0,false,false},	// AZS_FUEL_X_FEED8_BUS             
	{"sd_6015_p8_022",sd_6015_p8_022,"",0,0,false,false},	// AZS_ENG1_FUEL_VALVE_BUS         
	{"sd_6015_p8_023",sd_6015_p8_023,"",0,0,false,false},	// AZS_ENG2_FUEL_VALVE_BUS         
	{"sd_6015_p8_024",sd_6015_p8_024,"",0,0,false,false},	// AZS_ENG3_FUEL_VALVE_BUS         
	{"sd_6015_p8_025",sd_6015_p8_025,"",0,0,false,false},	// AZS_FIRE_EXT_1_BUS              
	{"sd_6015_p8_026",sd_6015_p8_026,"",0,0,false,false},	// AZS_FIRE_EXT_2_BUS              
	{"sd_6015_p8_027",sd_6015_p8_027,"",0,0,false,false},	// AZS_FIRE_EXT_3_BUS              
	{"sd_6015_p8_028",sd_6015_p8_028,"",0,0,false,false},	// AZS_AIR_STARTER_IND_BUS         
	{"sd_6015_p8_029",sd_6015_p8_029,"",0,0,false,false},	// AZS_GEAR_UP_HORN_BUS            
	{"sd_6015_p8_030",sd_6015_p8_030,"",0,0,false,false},	// AZS_APU_IGNIT_BUS               
	{"sd_6015_p8_031",sd_6015_p8_031,"",0,0,false,false},	// AZS_ENG1_IGNIT_BUS              
	{"sd_6015_p8_032",sd_6015_p8_032,"",0,0,false,false},	// AZS_ENG2_IGNIT_BUS              
	{"sd_6015_p8_033",sd_6015_p8_033,"",0,0,false,false},	// AZS_ENG3_IGNIT_BUS              
	{"sd_6015_p8_034",sd_6015_p8_034,"",0,0,false,false},	// AZS_FIRE_EXT1_VALVE_BUS         
	{"sd_6015_p8_035",sd_6015_p8_035,"",0,0,false,false},	// AZS_FIRE_EXT2_VALVE_BUS         
	{"sd_6015_p8_036",sd_6015_p8_036,"",0,0,false,false},	// AZS_FIRE_EXT3_VALVE_BUS         
												 
	// P9										 
	{"sd_6015_p9_001",sd_6015_p9_001,"",0,0,false,false},	// AZS_COM2_BUS                    
	{"sd_6015_p9_002",sd_6015_p9_002,"",0,0,false,false},	// AZS_GROUND_PWR_BUS              
	{"sd_6015_p9_003",sd_6015_p9_003,"",0,0,false,false},	// AZS_COPLT_ADI_BUS               
	{"sd_6015_p9_004",sd_6015_p9_004,"",0,0,false,false},	// AZS_GMK_COURSE_SYS_BUS          
	{"sd_6015_p9_005",sd_6015_p9_005,"",0,0,false,false},	// AZS_HYDRO_SYS_BUS               
	{"sd_6015_p9_006",sd_6015_p9_006,"",0,0,false,false},	// AZS_AUTOPILOT_BUS               
	{"sd_6015_p9_007",sd_6015_p9_007,"",0,0,false,false},	// AZS_GEAR_IND_BUS                
	{"sd_6015_p9_008",sd_6015_p9_008,"",0,0,false,false},	// AZS_SIGNAL_ROCKETS_BUS          
	{"sd_6015_p9_009",sd_6015_p9_009,"",0,0,false,false},	// AZS_ILS_SYS_BUS                 
	{"sd_6015_p9_010",sd_6015_p9_010,"",0,0,false,false},	// AZS_ADF2_BUS                    
	{"sd_6015_p9_011",sd_6015_p9_011,"",0,0,false,false},	// AZS_INV_115V_EMERG_SWITCH_BUS   
	{"sd_6015_p9_012",sd_6015_p9_012,"",0,0,false,false},	// AZS_STAB_EMERG_BUS              
	{"sd_6015_p9_013",sd_6015_p9_013,"",0,0,false,false},	// AZS_FLAPS_EMERG_BUS             
	{"sd_6015_p9_014",sd_6015_p9_014,"",0,0,false,false},	// AZS_GEAR_EMERG_BUS              
	{"sd_6015_p9_015",sd_6015_p9_015,"",0,0,false,false},	// AZS_ANTI_ICE_SYS_BUS            
	{"sd_6015_p9_016",sd_6015_p9_016,"",0,0,false,false},	// AZS_EXTREME_BANK_IND_BUS        
	{"sd_6015_p9_017",sd_6015_p9_017,"",0,0,false,false},	// AZS_AIR_COND_SYS_BUS            
	{"sd_6015_p9_018",sd_6015_p9_018,"",0,0,false,false},	// AZS_STAIRS_SERVO_BUS            
	{"sd_6015_p9_019",sd_6015_p9_019,"",0,0,false,false},	// AZS_TURBO_COOLER_UNIT_BUS       
	{"sd_6015_p9_020",sd_6015_p9_020,"",0,0,false,false},	// AZS_NOSE_LEG_STEER_BUS          
	{"sd_6015_p9_021",sd_6015_p9_021,"",0,0,false,false},	// AZS_FUEL_X_FEED9_BUS             
	{"sd_6015_p9_022",sd_6015_p9_022,"",0,0,false,false},	// AZS_ENG1_DE_ICE_BUS             
	{"sd_6015_p9_023",sd_6015_p9_023,"",0,0,false,false},	// AZS_ENG2_DE_ICE_BUS             
	{"sd_6015_p9_024",sd_6015_p9_024,"",0,0,false,false},	// AZS_ENG3_DE_ICE_BUS             
	{"sd_6015_p9_025",sd_6015_p9_025,"",0,0,false,false},	// AZS_BRAKE_SYS_BUS               
	{"sd_6015_p9_026",sd_6015_p9_026,"",0,0,false,false},	// AZS_DECK_HEATER_BUS             
	{"sd_6015_p9_027",sd_6015_p9_027,"",0,0,false,false},	// AZS_CABIN_HEATER_BUS            
	{"sd_6015_p9_028",sd_6015_p9_028,"",0,0,false,false},	// AZS_TAXI_LIGHT_L_BUS            
	{"sd_6015_p9_029",sd_6015_p9_029,"",0,0,false,false},	// AZS_LANDING_LIGHT_L_BUS         
	{"sd_6015_p9_030",sd_6015_p9_030,"",0,0,false,false},	// AZS_TAXI_LIGHT_R_BUS            
	{"sd_6015_p9_031",sd_6015_p9_031,"",0,0,false,false},	// AZS_LANDING_LIGHT_R_BUS         
	{"sd_6015_p9_032",sd_6015_p9_032,"",0,0,false,false},	// AZS_ALER_TRIM_BUS               
	{"sd_6015_p9_033",sd_6015_p9_033,"",0,0,false,false},	// AZS_RUDDER_TRIM_BUS             
	{"sd_6015_p9_034",sd_6015_p9_034,"",0,0,false,false},	// AZS_STAB_MAIN_BUS               
	{"sd_6015_p9_035",sd_6015_p9_035,"",0,0,false,false},	// AZS_FLAPS_MAIN_BUS              
	{"sd_6015_p9_036",sd_6015_p9_036,"",0,0,false,false},	// AZS_GEAR_MAIN_BUS               

	// P23
	{"sd_6015_p23_01",sd_6015_p23_01,"",0,0,false,false},	// AZS_EXT_PWR
	{"sd_6015_p23_02",sd_6015_p23_02,"",0,0,false,false},	// AZS_EXT_AIR

	// MISC
	{"sd_6015_pa_001",sd_6015_pa_001,"",0,0,false,false},	// AZS_DUMMY

};																				
