/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Hnd.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Hnd.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_p1_601	"LATITUDE SELECTOR: @(%(L:sd_6015_pa_533,integer)%>0?norm90(90-abs(%(L:sd_6015_pa_533,integer)%)):norm90(abs(%(L:sd_6015_pa_533,integer)%)))@ degrees"


#define sd_6015_p4_601	""                                
#define sd_6015_p4_602	""                                
#define sd_6015_p4_603	""                                
															
															
#define sd_6015_p5_601	""                                
#define sd_6015_p5_602	""                                
#define sd_6015_p5_603	""                                
#define sd_6015_p5_604	""                                
															
															
#define sd_6015_p6_601	""                                
#define sd_6015_p6_602	""                                
#define sd_6015_p6_603	""                                
#define sd_6015_p6_604	""                                
#define sd_6015_p6_605	""                                
#define sd_6015_p6_606	""                                
#define sd_6015_p6_607	""                                
															
															
#define sd_6015_p6_601	""                                
#define sd_6015_p6_602	""                                
#define sd_6015_p7_605	""                                
#define sd_6015_p7_606	""                                
#define sd_6015_p7_607	""                                
#define sd_6015_p7_608	""                                

#else

#define sd_6015_p1_601	""


#define sd_6015_p4_601	""                                
#define sd_6015_p4_602	""                                
#define sd_6015_p4_603	""                                
															
															
#define sd_6015_p5_601	""                                
#define sd_6015_p5_602	""                                
#define sd_6015_p5_603	""                                
#define sd_6015_p5_604	""                                
															
															
#define sd_6015_p6_601	""                                
#define sd_6015_p6_602	""                                
#define sd_6015_p6_603	""                                
#define sd_6015_p6_604	""                                
#define sd_6015_p6_605	""                                
#define sd_6015_p6_606	""                                
#define sd_6015_p6_607	""                                
															
															
#define sd_6015_p6_601	""                                
#define sd_6015_p6_602	""                                
#define sd_6015_p7_605	""                                
#define sd_6015_p7_606	""                                
#define sd_6015_p7_607	""                                
#define sd_6015_p7_608	""                                

#endif

ApiName HndNames[HND_MAX]={
	// P1
	{"sd_6015_p1_601",sd_6015_p1_601,"",  0, 89,true ,false},  // HND_GMK
					  
	// P4			  
	{"sd_6015_p4_601",sd_6015_p4_601,"",  0,  0,false,false},  // HND_AP_HOR			            
	{"sd_6015_p4_602",sd_6015_p4_602,"",  0,  0,false,false},  // HND_AP_HOR_CORRECTED            
	{"sd_6015_p4_603",sd_6015_p4_603,"",  0,  0,false,false},  // HND_AP_VER			            
					  				 
	// P5			  				 
	{"sd_6015_p5_601",sd_6015_p5_601,"",  0, 19,true ,false},  // HND_KNBVHF2RIGHT   
	{"sd_6015_p5_602",sd_6015_p5_602,"",  0, 19,true ,false},  // HND_KNBVHF2LEFT    
	{"sd_6015_p5_603",sd_6015_p5_603,"",  0, 19,true ,false},  // HND_KNBVHF1RIGHT   
	{"sd_6015_p5_604",sd_6015_p5_604,"",  0, 19,true ,false},  // HND_KNBVHF1LEFT    
					  					  
	// P6			  					  
	{"sd_6015_p6_601",sd_6015_p6_601,"",  0, 19,true ,false},  // HND_OBS6		
	{"sd_6015_p6_602",sd_6015_p6_602,"",  0, 19,true ,false},  // HND_KMP1FREQ	
	{"sd_6015_p6_603",sd_6015_p6_603,"",  0,  9,true ,false},  // HND_XPDR1		
	{"sd_6015_p6_604",sd_6015_p6_604,"",  0,  9,true ,false},  // HND_XPDR2		
	{"sd_6015_p6_605",sd_6015_p6_605,"",  0,  9,true ,false},  // HND_XPDR3		
	{"sd_6015_p6_606",sd_6015_p6_606,"",  0,  9,true ,false},  // HND_XPDR4		
	{"sd_6015_p6_607",sd_6015_p6_607,"",  0, 19,true ,false},  // HND_KMP1FREQ2	
					  				
	// P7			  				
	{"sd_6015_p6_601",sd_6015_p6_601,"",  0, 19,true ,false},  // HND_OBS7		      
	{"sd_6015_p6_602",sd_6015_p6_602,"",  0, 19,true ,false},  // HND_KMP2FREQ	      
	{"sd_6015_p7_605",sd_6015_p7_605,"",  0, 45,true ,false},  // HND_PRESS_CHANGE_RATE 
	{"sd_6015_p7_606",sd_6015_p7_606,"",  0, 19,true ,false},  // HND_PRESS_DIFF        
	{"sd_6015_p7_607",sd_6015_p7_607,"",  0, 19,true ,false},  // HND_PRESS_BEGIN	      
	{"sd_6015_p7_608",sd_6015_p7_608,"",  0, 19,true ,false},  // HND_KMP2FREQ2	
										  							 
};

