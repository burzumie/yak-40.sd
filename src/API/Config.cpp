/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Config.cpp $

  Last modification:
    $Date: 18.02.06 16:44 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Config.h"

std::list<SDConfigBase*>	SDConfigBase::m_ChildList;
CIniFile					*SDConfigBase::m_Ini=NULL;
char						SDConfigBase::m_ConfigPath[MAX_PATH];
char						SDConfigBase::m_ConfigFile[MAX_PATH];

