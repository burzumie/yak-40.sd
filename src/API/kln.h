/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Hnd.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

enum KLN {
	KLN_PWR					,		
	KLN_BRT_LEFT			,	
	KLN_BRT_RIGHT			,	
	KLN_LEFT_CRSR			,
	KLN_RIGHT_CRSR			,
	KLN_LEFT_OUTER_LEFT		,	
	KLN_LEFT_OUTER_RIGHT	,	
	KLN_LEFT_INNER_LEFT		,	
	KLN_LEFT_INNER_RIGHT	,	
	KLN_RIGHT_OUTER_LEFT	,	
	KLN_RIGHT_OUTER_RIGHT	,	
	KLN_RIGHT_INNER_LEFT	,	
	KLN_RIGHT_INNER_RIGHT	,	
	KLN_PULL_SCAN        	,	
	KLN_MESSAGE          	,	
	KLN_ALTITUDE         	,	
	KLN_DIRECT_TO        	,	
	KLN_CLEAR            	,	
	KLN_ENTER            	,	
	KLN_MAX

};