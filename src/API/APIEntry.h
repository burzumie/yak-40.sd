/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/APIEntry.h $

  Last modification:
    $Date: 19.02.06 10:43 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Common/CommonSys.h"
#ifdef PA_EXPORTS
#include "../Common/CommonLogic.h"
#else
#include "../Common/CommonVisual.h"
#endif
#include "../Lib/Tools.h"

#include "Azs.h"
#include "Btn.h"
#include "Krm.h"
#include "Glt.h"
#include "Pos.h"
#include "Ndl.h"
#include "Hnd.h"
#include "Tbl.h"
#include "Tbg.h"
#include "Lmp.h"
#include "Pwr.h"
#include "Cfg.h"
#include "Snd.h"
#include "Key.h"
#include "Kln.h"

enum VAL_HISTORY {
	VAL_CUR=0,
	VAL_OLD,
	VAL_PREV,
	VAL_MAX
};

template <typename T>
struct SDAPIEntry {
	T m_Val[VAL_MAX];
	T m_Min;
	T m_Max;
	char	m_TTDst[BUFSIZ];
	int		m_Changed;
	int		m_ChangedSound;
	bool	m_IsLimit;
	bool	m_IsLimitLoop;

	SDAPIEntry() {
		m_Val[VAL_CUR]=m_Val[VAL_OLD]=0;
		m_Changed=m_ChangedSound=0;
		m_IsLimit=m_IsLimitLoop=false;
		m_Min=m_Max=0;
	};

	~SDAPIEntry() {
	};

	void Save() {
		m_Val[VAL_PREV]=m_Val[VAL_OLD];
		m_Val[VAL_OLD]=m_Val[VAL_CUR];
	}

	void Check() {
		if(m_Val[VAL_CUR]!=m_Val[VAL_OLD]) {
			if(m_Val[VAL_CUR]<m_Val[VAL_OLD])
				m_Changed=m_ChangedSound=-1;
			else if(m_Val[VAL_CUR]>m_Val[VAL_OLD])
				m_Changed=m_ChangedSound=1;
			CheckLimit();
			Save();
		}
	};

	void Update() {
		Check();
	};

	T Set(T val) {
		m_Val[VAL_CUR]=val;
		Check();
		return m_Val[VAL_CUR];
	};

	T Inc(T val=1) {
		m_Val[VAL_CUR]+=val;
		Check();
		return m_Val[VAL_CUR];
	};

	T Dec(T val=1) {
		m_Val[VAL_CUR]-=val;
		Check();
		return m_Val[VAL_CUR];
	};

	T Toggle() {
		m_Val[VAL_CUR]=!m_Val[VAL_OLD];
		Check();
		return m_Val[VAL_CUR];
	};

	T Get() {
		return m_Val[VAL_CUR];
	};

	void SetTT(char *val) {
		strncpy(m_TTDst,val,BUFSIZ);
		//m_TTDst=val;
	};

	char *GetTT() {
		return m_TTDst;
	};

	int IsChanged(bool reset=true) {
		if(reset) {
			int ret=m_Changed;
			m_Changed=0;
			return ret;
		} else return m_Changed;
	};

	int IsChangedSound(bool reset=true) {
		if(reset) {
			int ret=m_ChangedSound;
			m_ChangedSound=0;
			return ret;
		} else return m_ChangedSound;
	};

	void SetLimit(T min,T max,bool isloop) {
		if(min==max||min>max) {
			m_IsLimit=false;
			return;
		}
		m_Min=min;
		m_Max=max;
		m_IsLimit=true;
		m_IsLimitLoop=isloop;
	};

	void CheckLimit() {
		if(m_IsLimit) {
			if(!m_IsLimitLoop) {
				m_Val[VAL_CUR]=LimitValue<T>(m_Val[VAL_CUR],m_Min,m_Max);
			} else {
				if(m_Val[VAL_CUR]<m_Min)
					m_Val[VAL_CUR]=m_Max;
				else if(m_Val[VAL_CUR]>m_Max)
					m_Val[VAL_CUR]=m_Min;
			}
		}
	};

};

struct SDAPIEntrys {
	SDAPIEntry<int>		SDAZS	[AZS_MAX];
	SDAPIEntry<int>		SDBTN	[BTN_MAX];
	SDAPIEntry<int>		SDGLT	[GLT_MAX];
	SDAPIEntry<int>		SDKRM	[KRM_MAX];
	SDAPIEntry<int>		SDHND	[HND_MAX];
	SDAPIEntry<int>		SDLMP	[LMP_MAX];
	SDAPIEntry<int>		SDLMPE	[LMP_MAX];
	SDAPIEntry<double>	SDNDL	[NDL_MAX];
	SDAPIEntry<double>	SDPOS	[POS_MAX];
	SDAPIEntry<int>		SDTBG	[TBG_MAX];
	SDAPIEntry<int>		SDTBGE	[TBG_MAX];
	SDAPIEntry<int>		SDTBL	[TBL_MAX];
	SDAPIEntry<int>		SDTBLE	[TBL_MAX];
	SDAPIEntry<int>		SDPWR	[PWR_MAX];
	SDAPIEntry<double>	SDCFG	[CFG_MAX];
	SDAPIEntry<int>		SDSND	[SND_MAX];
	char				SDCFGSTR[CFG_STR_MAX][BUFSIZ];
	SDAPIEntry<int>		SDKEY	[KEY_MAX];
	SDAPIEntry<int>		SDKLN	[KLN_MAX];
};

extern SDAPIEntrys		g_SDAPIEntrys;
extern SDAPIEntrys*		g_pSDAPIEntrys;
extern MODULE_VAR		MVSDAPIEntrys;
extern MODULE_VAR		MVSDAPI;
