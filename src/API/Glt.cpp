/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Glt.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Glt.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_p0_301	"IKU: %(V:ADF1|VOR1|VOR2|ADF2)%"			
#define sd_6015_p0_302	"IKU: %(V:ADF1|VOR1|VOR2|ADF2)%"			
																	
 																	
#define sd_6015_p1_301	""										
#define sd_6015_p1_302	"27V-meter: %(V:GENERATOR1|GENERATOR2|GENERATOR3|GROUND|BATTERY RIGHT|MAIN BUS|BATTERY LEFT)%"
#define sd_6015_p1_303	"36V-meter: %(V:INV R Phase 1-2|INV R Phase 1-3|INV R Phase 2-3|INV ADI RES Phase 1-2|INV ADI RES Phase 1-3|INV ADI RES Phase 2-3|INV L Phase 2-3|INV L Phase 1-3|INV L Phase 1-2|INV DA30 Phase 2-3|INV DA30 Phase 1-3|INV DA30 Phase 1-2)%"										
#define sd_6015_p1_304	"115V-meter: %(V:INV R|INV L|GROUND)%"										
																	
 																	
#define sd_6015_p4_301	"NAV SYS MODE: %(V:ILS|KATET|SP-50)%"										
																	
 																	
#define sd_6015_p5_301	"ADF1: %(V:OFF|R-COMP|ANT|MAN)%"			
#define sd_6015_p5_302	"ADF2: %(V:OFF|R-COMP|ANT|MAN)%"			
#define sd_6015_p5_303	"ADF1 L: %(L:sd_6015_p5_410,integer)%"    
#define sd_6015_p5_304	"ADF2 L: %(L:sd_6015_p5_412,integer)%"    
#define sd_6015_p5_305	"ADF1 R: %(L:sd_6015_p5_411,integer)%"    
#define sd_6015_p5_306	"ADF2 R: %(L:sd_6015_p5_413,integer)%"    
#define sd_6015_p5_307  ""
#define sd_6015_p5_308  ""
#define sd_6015_p5_309  ""
#define sd_6015_p5_310  ""
#define sd_6015_p5_311  ""
#define sd_6015_p5_312  ""
#define sd_6015_p5_313  ""
#define sd_6015_p5_314  ""
#define sd_6015_p5_315  ""
#define sd_6015_p5_316  ""
#define sd_6015_p5_317  ""
#define sd_6015_p5_318  ""
#define sd_6015_p5_319  ""
#define sd_6015_p5_320  ""
#define sd_6015_p5_321  ""
#define sd_6015_p5_322  ""
																	
 																	
#define sd_6015_p6_301	"UNITS: %(V:NM|KM)%"						
#define sd_6015_p6_302	"LEFT VHF RADIO: %(V:COM1|COM2|OFF|ADF1 IDENT|ADF2 IDENT)%"										
#define sd_6015_p6_303	""										
																	
 																	
#define sd_6015_p7_301	"UNITS: %(V:NM|KM)%"						
#define sd_6015_p7_302	"RIGHT VHF RADIO: %(V:COM1|COM2|OFF|ADF1 IDENT|ADF2 IDENT)%"										

#else

#define sd_6015_p0_301	""
#define sd_6015_p0_302	""
							
							
#define sd_6015_p1_301	""
#define sd_6015_p1_302	""
#define sd_6015_p1_303	""
#define sd_6015_p1_304	""
							
							
#define sd_6015_p4_301	""
							
							
#define sd_6015_p5_301	""
#define sd_6015_p5_302	""
#define sd_6015_p5_303	""
#define sd_6015_p5_304	""
#define sd_6015_p5_305	""
#define sd_6015_p5_306	""
#define sd_6015_p5_307	""
#define sd_6015_p5_308	""
#define sd_6015_p5_309	""
#define sd_6015_p5_310	""
#define sd_6015_p5_311	""
#define sd_6015_p5_312	""
#define sd_6015_p5_313	""
#define sd_6015_p5_314	""
#define sd_6015_p5_315	""
#define sd_6015_p5_316	""
#define sd_6015_p5_317	""
#define sd_6015_p5_318	""
#define sd_6015_p5_319	""
#define sd_6015_p5_320	""
#define sd_6015_p5_321	""
#define sd_6015_p5_322	""
							
							
#define sd_6015_p6_301	""
#define sd_6015_p6_302	""
#define sd_6015_p6_303	""
							
							
#define sd_6015_p7_301	""
#define sd_6015_p7_302	""

#endif

ApiName GltNames[GLT_MAX]={                
	// P0
	{"sd_6015_p0_301",sd_6015_p0_301,"",  0,  3,true ,false},  // GLT_IKU_LEFT
	{"sd_6015_p0_302",sd_6015_p0_302,"",  0,  3,true ,false},  // GLT_IKU_RIGHT
												  
	// P1										  
	{"sd_6015_p1_301",sd_6015_p1_301,"",  0,  2,false,false},  // GLT_TEMP_DUCT
	{"sd_6015_p1_302",sd_6015_p1_302,"",  0,  6,true ,false},  // GLT_VOLT27   
	{"sd_6015_p1_303",sd_6015_p1_303,"",  0, 11,true ,false},  // GLT_VOLT36   
	{"sd_6015_p1_304",sd_6015_p1_304,"",  0,  2,false,false},  // GLT_VOLT115  
												  
	// P4										  
	{"sd_6015_p4_301",sd_6015_p4_301,"",  0,  2,false,false},	// GLT_KMPSYS
												  
	// P5										  
	{"sd_6015_p5_301",sd_6015_p5_301,"",  0,  3,false,false},  // GLT_ARK1           
	{"sd_6015_p5_302",sd_6015_p5_302,"",  0,  3,false,false},  // GLT_ARK2           
	{"sd_6015_p5_303",sd_6015_p5_303,"",  0, 11,false,false},  // GLT_ARK1LEFT100    
	{"sd_6015_p5_304",sd_6015_p5_304,"",  0, 11,false,false},  // GLT_ARK2LEFT100    
	{"sd_6015_p5_305",sd_6015_p5_305,"",  0, 11,false,false},  // GLT_ARK1RIGHT100   
	{"sd_6015_p5_306",sd_6015_p5_306,"",  0, 11,false,false},  // GLT_ARK2RIGHT100   
	{"sd_6015_p5_307",sd_6015_p5_307,"",  0,  9,true ,false},  // GLT_ARK1LEFT10     
	{"sd_6015_p5_308",sd_6015_p5_308,"",  0,  9,true ,false},  // GLT_ARK2LEFT10     
	{"sd_6015_p5_309",sd_6015_p5_309,"",  0,  9,true ,false},  // GLT_ARK1RIGHT10    
	{"sd_6015_p5_310",sd_6015_p5_310,"",  0,  9,true ,false},  // GLT_ARK2RIGHT10    
	{"sd_6015_p5_311",sd_6015_p5_311,"",  0,  9,false,false},  // GLT_ARK1LEFT1      
	{"sd_6015_p5_312",sd_6015_p5_312,"",  0,  9,false,false},  // GLT_ARK2LEFT1      
	{"sd_6015_p5_313",sd_6015_p5_313,"",  0,  9,false,false},  // GLT_ARK1RIGHT1     
	{"sd_6015_p5_314",sd_6015_p5_314,"",  0,  9,false,false},  // GLT_ARK2RIGHT1     
	{"sd_6015_p5_315",sd_6015_p5_315,"",  0,  9,true ,false},  // GLT_ARK1LEFTHND    
	{"sd_6015_p5_316",sd_6015_p5_316,"",  0,  9,true ,false},  // GLT_ARK2LEFTHND    
	{"sd_6015_p5_317",sd_6015_p5_317,"",  0,  9,true ,false},  // GLT_ARK1RIGHTHND   
	{"sd_6015_p5_318",sd_6015_p5_318,"",  0,  9,true ,false},  // GLT_ARK2RIGHTHND   
	{"sd_6015_p5_319",sd_6015_p5_319,"",  0,  9,false,false},  // GLT_ARK1LEFT1KNB   
	{"sd_6015_p5_320",sd_6015_p5_320,"",  0,  9,false,false},  // GLT_ARK2LEFT1KNB   
	{"sd_6015_p5_321",sd_6015_p5_321,"",  0,  9,false,false},  // GLT_ARK1RIGHT1KNB  
	{"sd_6015_p5_322",sd_6015_p5_322,"",  0,  9,false,false},  // GLT_ARK2RIGHT1KNB  
										  		  
	// P6								  		  
	{"sd_6015_p6_301",sd_6015_p6_301,"",  0,  0,false,false},  // GLT_DME6	
	{"sd_6015_p6_302",sd_6015_p6_302,"",  0,  4,false,false},  // GLT_SGU6	
	{"sd_6015_p6_303",sd_6015_p6_303,"",  0,  4,false,false},  // GLT_XPDRMODE
										  		  
	// P7										  
	{"sd_6015_p7_301",sd_6015_p7_301,"",  0,  0,false,false},  // GLT_DME7		
	{"sd_6015_p7_302",sd_6015_p7_302,"",  0,  4,false,false},  // GLT_SGU7		
										  						   
};										  						   
