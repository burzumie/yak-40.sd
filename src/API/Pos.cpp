/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Pos.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Pos.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_pa_401	""
#define sd_6015_pa_402	""
#define sd_6015_pa_403	""
#define sd_6015_pa_404	"RUS/INTL system (In 2D Subpanels only)"
#define sd_6015_pa_405	""
#define sd_6015_pa_406	""
#define sd_6015_pa_407	""
#define sd_6015_pa_408	"Open/Close Window"
#define sd_6015_pa_409	"Ground startup"
#define sd_6015_pa_410	""
#define sd_6015_pa_411	""
#define sd_6015_pa_412	""
#define sd_6015_pa_413	""
#define sd_6015_pa_414	""
#define sd_6015_pa_415	""
#define sd_6015_pa_416	""
#define sd_6015_pa_417	""
#define sd_6015_pa_418	""
#define sd_6015_pa_419	""
#define sd_6015_pa_420	""
#define sd_6015_pa_421	""
															
															
#define sd_6015_p0_401	"Switch to Left Panel"                                
#define sd_6015_p0_402	"Switch to Overhead"                                
#define sd_6015_p0_403	"Switch to Central Pedestal"                                
#define sd_6015_p0_404	"Switch to Copilot Seat"                                
#define sd_6015_p0_405	""                                
#define sd_6015_p0_406	""                                
#define sd_6015_p0_407	""                                
#define sd_6015_p0_408	""                                
#define sd_6015_p0_409	""                                
#define sd_6015_p0_410	""                                
#define sd_6015_p0_411	""                                
#define sd_6015_p0_412	""                                
#define sd_6015_p0_413	""                                
#define sd_6015_p0_414	""                                
#define sd_6015_p0_415	""                                
#define sd_6015_p0_416	""                                
#define sd_6015_p0_417	""                                
#define sd_6015_p0_418	""                                
#define sd_6015_p0_419	""                                
#define sd_6015_p0_420	""                                
#define sd_6015_p0_421	""                                
#define sd_6015_p0_422	""                                
#define sd_6015_p0_423	""                                
#define sd_6015_p0_424	""                                
#define sd_6015_p0_425	""                                
#define sd_6015_p0_426	""                                
#define sd_6015_p0_427	""                                
#define sd_6015_p0_428	""                                
#define sd_6015_p0_429	""                                
#define sd_6015_p0_430	""                                
#define sd_6015_p0_431	""                                
#define sd_6015_p0_432	""                                
															
															
#define sd_6015_p1_401	"Switch to Right Panel"                                
#define sd_6015_p1_402	"Switch to Overhead"                                
#define sd_6015_p1_403	"Switch to Central Pedestal"                                
#define sd_6015_p1_404	"Switch to Captain Seat"                                
#define sd_6015_p1_405	""                                
#define sd_6015_p1_406	""                                
#define sd_6015_p1_407	""                                
#define sd_6015_p1_408	""                                
#define sd_6015_p1_409	""                                
#define sd_6015_p1_410	""                                
#define sd_6015_p1_411	""                                
#define sd_6015_p1_412	""                                
#define sd_6015_p1_413	""                                
#define sd_6015_p1_414	""                                
#define sd_6015_p1_415	""                                
#define sd_6015_p1_416	""                                
#define sd_6015_p1_417	""                                
#define sd_6015_p1_418	""                                
#define sd_6015_p1_419	""                                
#define sd_6015_p1_420	""                                
#define sd_6015_p1_421	""                                
#define sd_6015_p1_422	""                                
															
															
#define sd_6015_p4_401	"Switch to Captain Seat"                                
#define sd_6015_p4_402	"Switch to Overhead"                                
#define sd_6015_p4_403	"Switch to Copilot Seat"                                
#define sd_6015_p4_404	""                                
#define sd_6015_p4_405	""                                
#define sd_6015_p4_406	""                                
#define sd_6015_p4_407	""                                
#define sd_6015_p4_408	""                                
#define sd_6015_p4_409	""                                
#define sd_6015_p4_410	""                                
#define sd_6015_p4_411	"PARKING BRAKE: %(V:OFF| | | | | | | | | | | | | |ON)%"
#define sd_6015_p4_412	""                                
#define sd_6015_p4_413	""                                
#define sd_6015_p4_414	""                                
#define sd_6015_p4_415	""                                
#define sd_6015_p4_416	""                                
#define sd_6015_p4_417	""                                
#define sd_6015_p4_418	""                                
#define sd_6015_p4_419	""                                
#define sd_6015_p4_420	""                                
															
															
#define sd_6015_p5_401	"Switch to Captain Seat"                                
#define sd_6015_p5_402	"Switch to Central Pedestal"                                
#define sd_6015_p5_403	"Switch to Copilot Seat"                                
#define sd_6015_p5_404	""                                
#define sd_6015_p5_405	""                                
#define sd_6015_p5_406	""                                
#define sd_6015_p5_407	""                                
#define sd_6015_p5_408	""                                
#define sd_6015_p5_409	""                                
#define sd_6015_p5_410	""                                
#define sd_6015_p5_411	""                                
#define sd_6015_p5_412	""                                
#define sd_6015_p5_413	""                                
 															
															
#define sd_6015_p6_401	"Switch to Captain Seat"                                
#define sd_6015_p6_402	""                                
#define sd_6015_p6_403	""                                
#define sd_6015_p6_404	""                                
#define sd_6015_p6_405	""                                
#define sd_6015_p6_406	""                                
#define sd_6015_p6_407	""                                
#define sd_6015_p6_408	""                                
#define sd_6015_p6_409	""                                
#define sd_6015_p6_410	""                                
#define sd_6015_p6_411	""                                
#define sd_6015_p6_412	""                                
#define sd_6015_p6_413	""                                
#define sd_6015_p6_414	""                                
#define sd_6015_p6_415	""                                
#define sd_6015_p6_416	""                                
#define sd_6015_p6_417	""                                
#define sd_6015_p6_418	""                                
#define sd_6015_p6_419	""                                
#define sd_6015_p6_420	""                                
#define sd_6015_p6_421	""                                
#define sd_6015_p6_422	""                                
#define sd_6015_p6_423	""                                
															
															
#define sd_6015_p7_401	"Switch to Copilot Seat"                                
#define sd_6015_p7_402	""                                
#define sd_6015_p7_403	""                                
#define sd_6015_p7_404	""                                
#define sd_6015_p7_405	""                                
#define sd_6015_p7_406	""                                
#define sd_6015_p7_407	""                                
#define sd_6015_p7_408	""                                
#define sd_6015_p7_409	""                                
#define sd_6015_p7_410	""                                
#define sd_6015_p7_411	""                                
#define sd_6015_p7_412	""                                
#define sd_6015_p7_413	""                                
#define sd_6015_p7_414	""                                
#define sd_6015_p7_415	""                                
#define sd_6015_p7_416	""                                
#define sd_6015_p7_417	""                                
#define sd_6015_p7_418	""                                
#define sd_6015_p7_419	""                                
#define sd_6015_p7_420	""                                

#define sd_6015_p23_401	"Switch to Captain Seat"                                
#define sd_6015_p23_402	"Switch to Copilot Seat"                                
#define sd_6015_p23_403	"Switch to Central Pedestal"                                
#define sd_6015_p23_404	"Switch to Overhead"                                
#define sd_6015_p23_405	"Switch to Left Panel"                                
#define sd_6015_p23_406	"Switch to Right Panel"                                
#define sd_6015_p23_407	"Toggle Left AZS Panel"                                
#define sd_6015_p23_408	"Toggle Right AZS Panel"                                

#else

#define sd_6015_pa_401	""                                
#define sd_6015_pa_402	""                                
#define sd_6015_pa_403	""                                
#define sd_6015_pa_404	""                                
#define sd_6015_pa_405	""                                
#define sd_6015_pa_406	""                                
#define sd_6015_pa_407	""                                
#define sd_6015_pa_408	""                                
#define sd_6015_pa_409	""                                
#define sd_6015_pa_410	""
#define sd_6015_pa_411	""
#define sd_6015_pa_412	""
#define sd_6015_pa_413	""
#define sd_6015_pa_414	""
#define sd_6015_pa_415	""
#define sd_6015_pa_416	""
#define sd_6015_pa_417	""
#define sd_6015_pa_418	""
#define sd_6015_pa_419	""
#define sd_6015_pa_420	""
#define sd_6015_pa_421	""


#define sd_6015_p0_401	""                                
#define sd_6015_p0_402	""                                
#define sd_6015_p0_403	""                                
#define sd_6015_p0_404	""                                
#define sd_6015_p0_405	""                                
#define sd_6015_p0_406	""                                
#define sd_6015_p0_407	""                                
#define sd_6015_p0_408	""                                
#define sd_6015_p0_409	""                                
#define sd_6015_p0_410	""                                
#define sd_6015_p0_411	""                                
#define sd_6015_p0_412	""                                
#define sd_6015_p0_413	""                                
#define sd_6015_p0_414	""                                
#define sd_6015_p0_415	""                                
#define sd_6015_p0_416	""                                
#define sd_6015_p0_417	""                                
#define sd_6015_p0_418	""                                
#define sd_6015_p0_419	""                                
#define sd_6015_p0_420	""                                
#define sd_6015_p0_421	""                                
#define sd_6015_p0_422	""                                
#define sd_6015_p0_423	""                                
#define sd_6015_p0_424	""                                
#define sd_6015_p0_425	""                                
#define sd_6015_p0_426	""                                
#define sd_6015_p0_427	""                                
#define sd_6015_p0_428	""                                
#define sd_6015_p0_429	""                                
#define sd_6015_p0_430	""                                
#define sd_6015_p0_431	""                                
#define sd_6015_p0_432	""                                


#define sd_6015_p1_401	""                                
#define sd_6015_p1_402	""                                
#define sd_6015_p1_403	""                                
#define sd_6015_p1_404	""                                
#define sd_6015_p1_405	""                                
#define sd_6015_p1_406	""                                
#define sd_6015_p1_407	""                                
#define sd_6015_p1_408	""                                
#define sd_6015_p1_409	""                                
#define sd_6015_p1_410	""                                
#define sd_6015_p1_411	""                                
#define sd_6015_p1_412	""                                
#define sd_6015_p1_413	""                                
#define sd_6015_p1_414	""                                
#define sd_6015_p1_415	""                                
#define sd_6015_p1_416	""                                
#define sd_6015_p1_417	""                                
#define sd_6015_p1_418	""                                
#define sd_6015_p1_419	""                                
#define sd_6015_p1_420	""                                
#define sd_6015_p1_421	""                                
#define sd_6015_p1_422	""                                


#define sd_6015_p4_401	""                                
#define sd_6015_p4_402	""                                
#define sd_6015_p4_403	""                                
#define sd_6015_p4_404	""                                
#define sd_6015_p4_405	""                                
#define sd_6015_p4_406	""                                
#define sd_6015_p4_407	""                                
#define sd_6015_p4_408	""                                
#define sd_6015_p4_409	""                                
#define sd_6015_p4_410	""                                
#define sd_6015_p4_411	""
#define sd_6015_p4_412	""                                
#define sd_6015_p4_413	""                                
#define sd_6015_p4_414	""                                
#define sd_6015_p4_415	""                                
#define sd_6015_p4_416	""                                
#define sd_6015_p4_417	""                                
#define sd_6015_p4_418	""                                
#define sd_6015_p4_419	""                                
#define sd_6015_p4_420	""                                


#define sd_6015_p5_401	""                                
#define sd_6015_p5_402	""                                
#define sd_6015_p5_403	""                                
#define sd_6015_p5_404	""                                
#define sd_6015_p5_405	""                                
#define sd_6015_p5_406	""                                
#define sd_6015_p5_407	""                                
#define sd_6015_p5_408	""                                
#define sd_6015_p5_409	""                                
#define sd_6015_p5_410	""                                
#define sd_6015_p5_411	""                                
#define sd_6015_p5_412	""                                
#define sd_6015_p5_413	""                                


#define sd_6015_p6_401	""                                
#define sd_6015_p6_402	""                                
#define sd_6015_p6_403	""                                
#define sd_6015_p6_404	""                                
#define sd_6015_p6_405	""                                
#define sd_6015_p6_406	""                                
#define sd_6015_p6_407	""                                
#define sd_6015_p6_408	""                                
#define sd_6015_p6_409	""                                
#define sd_6015_p6_410	""                                
#define sd_6015_p6_411	""                                
#define sd_6015_p6_412	""                                
#define sd_6015_p6_413	""                                
#define sd_6015_p6_414	""                                
#define sd_6015_p6_415	""                                
#define sd_6015_p6_416	""                                
#define sd_6015_p6_417	""                                
#define sd_6015_p6_418	""                                
#define sd_6015_p6_419	""                                
#define sd_6015_p6_420	""                                
#define sd_6015_p6_421	""                                
#define sd_6015_p6_422	""                                
#define sd_6015_p6_423	""                                


#define sd_6015_p7_401	""                                
#define sd_6015_p7_402	""                                
#define sd_6015_p7_403	""                                
#define sd_6015_p7_404	""                                
#define sd_6015_p7_405	""                                
#define sd_6015_p7_406	""                                
#define sd_6015_p7_407	""                                
#define sd_6015_p7_408	""                                
#define sd_6015_p7_409	""                                
#define sd_6015_p7_410	""                                
#define sd_6015_p7_411	""                                
#define sd_6015_p7_412	""                                
#define sd_6015_p7_413	""                                
#define sd_6015_p7_414	""                                
#define sd_6015_p7_415	""                                
#define sd_6015_p7_416	""                                
#define sd_6015_p7_417	""                                
#define sd_6015_p7_418	""                                
#define sd_6015_p7_419	""                                
#define sd_6015_p7_420	""                                

#define sd_6015_p23_401	""                                
#define sd_6015_p23_402	""                                
#define sd_6015_p23_403	""                                
#define sd_6015_p23_404	""                                
#define sd_6015_p23_405	""                                
#define sd_6015_p23_406	""                                
#define sd_6015_p23_407	""                                
#define sd_6015_p23_408	""                                

#endif

ApiName PosNames[POS_MAX]={
	// ����� 
	{"sd_6015_pa_401",sd_6015_pa_401,"",  0,  2,true ,false},	// POS_ACHS_BLENKER    
	{"sd_6015_pa_402",sd_6015_pa_402,"",  0,  2,true ,false},	// POS_ACHS_TIMER      
	{"sd_6015_pa_403",sd_6015_pa_403,"",  0,  0,false,false},	// POS_PANEL_STATE            
	{"sd_6015_pa_404",sd_6015_pa_404,"",  0,  0,false,false},	// POS_PANEL_LANG
	{"sd_6015_pa_405",sd_6015_pa_405,"",  0,  0,false,false},	// POS_TIME_AFTER_LOAD
	{"sd_6015_pa_406",sd_6015_pa_406,"",  0,  0,false,false},	// POS_MAIN_FLAPS_WORK
	{"sd_6015_pa_407",sd_6015_pa_407,"",  0,  0,false,false},	// POS_AUXL_FLAPS_WORK
	{"sd_6015_pa_408",sd_6015_pa_408,"",  0,  0,false,false},	// POS_OPEN_CLOSE_WINDOW
	{"sd_6015_pa_409",sd_6015_pa_409,"",  0,  0,false,false},	// POS_GROUND_STARTUP
	{"sd_6015_pa_410",sd_6015_pa_410,"",  0,  0,false,false},	// POS_ENGINE1_MIXTURE
	{"sd_6015_pa_411",sd_6015_pa_411,"",  0,  0,false,false},	// POS_ENGINE2_MIXTURE
	{"sd_6015_pa_412",sd_6015_pa_412,"",  0,  0,false,false},	// POS_ENGINE3_MIXTURE
	{"sd_6015_pa_413",sd_6015_pa_413,"",  0,  0,false,false},	// POS_TARGET_SALON_TEMP			
	{"sd_6015_pa_414",sd_6015_pa_414,"",  0,  0,false,false},	// POS_TARGET_DUCT_TEMP			
	{"sd_6015_pa_415",sd_6015_pa_415,"",  0,  0,false,false},	// POS_TIME_OF_DAY
	{"sd_6015_pa_416",sd_6015_pa_416,"",  0,  0,false,false},	// POS_ACTIVE_VIEW_MODE
	{"sd_6015_pa_417",sd_6015_pa_417,"",  0,  0,false,false},	// POS_SIM_PAUSED
	{"sd_6015_pa_418",sd_6015_pa_418,"",  0,  0,false,false},	// POS_SIM_SPEED
	{"sd_6015_pa_419",sd_6015_pa_419,"",  0,  0,false,false},	// POS_DESCENT
	{"sd_6015_pa_420",sd_6015_pa_420,"",  0,  0,false,false},	// POS_GMK_READY
	{"sd_6015_pa_421",sd_6015_pa_421,"",  0,  0,false,false},	// POS_MAIN_HYDRO_WORK
											
	// P0									
	{"sd_6015_p0_401",sd_6015_p0_401,"",  0,  0,false,false},   // POS_CRS_01			
	{"sd_6015_p0_402",sd_6015_p0_402,"",  0,  0,false,false},   // POS_CRS_02			
	{"sd_6015_p0_403",sd_6015_p0_403,"",  0,  0,false,false},   // POS_CRS_03			
	{"sd_6015_p0_404",sd_6015_p0_404,"",  0,  0,false,false},   // POS_CRS_04			
	{"sd_6015_p0_405",sd_6015_p0_405,"",  0,  0,false,false},   // POS_YOKE_ICON		
	{"sd_6015_p0_406",sd_6015_p0_406,"",  0,  0,false,false},   // POS_AGB_MAIN_PITCH  
	{"sd_6015_p0_407",sd_6015_p0_407,"",  0,  0,false,false},   // POS_AGB_AUXL_PITCH  
	{"sd_6015_p0_408",sd_6015_p0_408,"",  0,  0,false,false},   // POS_KPPMS_BLK_COURSE
	{"sd_6015_p0_409",sd_6015_p0_409,"",  0,  0,false,false},   // POS_KPPMS_BLK_GLIDE 
	{"sd_6015_p0_410",sd_6015_p0_410,"",  0,  0,false,false},   // POS_RV3M_LAMP       
	{"sd_6015_p0_411",sd_6015_p0_411,"",  0,  0,false,false},   // POS_UVID_PSI_100_MM    
	{"sd_6015_p0_412",sd_6015_p0_412,"",  0,  0,false,false},   // POS_UVID_PSI_10_MM     
	{"sd_6015_p0_413",sd_6015_p0_413,"",  0,  0,false,false},   // POS_UVID_PSI_1_MM      
	{"sd_6015_p0_414",sd_6015_p0_414,"",  0,  0,false,false},   // POS_UVID_ALT_10000  
	{"sd_6015_p0_415",sd_6015_p0_415,"",  0,  0,false,false},   // POS_UVID_ALT_1000   
	{"sd_6015_p0_416",sd_6015_p0_416,"",  0,  0,false,false},   // POS_UVID_ALT_100    
	{"sd_6015_p0_417",sd_6015_p0_417,"",  0,  0,false,false},   // POS_UVID_ALT_10     
	{"sd_6015_p0_418",sd_6015_p0_418,"",  0,  0,false,false},   // POS_UVID_MB     
	{"sd_6015_p0_419",sd_6015_p0_419,"",20.46,36.,false,false},   // POS_UVID_HG     
	{"sd_6015_p0_420",sd_6015_p0_420,"",  0,  0,false,false},   // POS_UVID_MM     
	{"sd_6015_p0_421",sd_6015_p0_421,"",  0,  0,false,false},   // POS_UVID_ALT_10000_EN
	{"sd_6015_p0_422",sd_6015_p0_422,"",  0,  0,false,false},   // POS_UVID_ALT_1000_EN
	{"sd_6015_p0_423",sd_6015_p0_423,"",  0,  0,false,false},   // POS_UVID_ALT_100_EN 
	{"sd_6015_p0_424",sd_6015_p0_424,"",  0,  0,false,false},   // POS_UVID_ALT_10_EN  
	{"sd_6015_p0_425",sd_6015_p0_425,"",  0,  0,false,false},   // POS_UVID_PSI_1000_MB    
	{"sd_6015_p0_426",sd_6015_p0_426,"",  0,  0,false,false},   // POS_UVID_PSI_100_MB    
	{"sd_6015_p0_427",sd_6015_p0_427,"",  0,  0,false,false},   // POS_UVID_PSI_10_MB     
	{"sd_6015_p0_428",sd_6015_p0_428,"",  0,  0,false,false},   // POS_UVID_PSI_1_MB      
	{"sd_6015_p0_429",sd_6015_p0_429,"",  0,  0,false,false},   // POS_UVID_PSI_1000_HG    
	{"sd_6015_p0_430",sd_6015_p0_430,"",  0,  0,false,false},   // POS_UVID_PSI_100_HG    
	{"sd_6015_p0_431",sd_6015_p0_431,"",  0,  0,false,false},   // POS_UVID_PSI_10_HG     
	{"sd_6015_p0_432",sd_6015_p0_432,"",  0,  0,false,false},   // POS_UVID_PSI_1_HG      
															   
	// P1													   
	{"sd_6015_p1_401",sd_6015_p1_401,"",  0,  0,false,false},   // POS_CRS_01			
	{"sd_6015_p1_402",sd_6015_p1_402,"",  0,  0,false,false},   // POS_CRS_02			
	{"sd_6015_p1_403",sd_6015_p1_403,"",  0,  0,false,false},   // POS_CRS_03			
	{"sd_6015_p1_404",sd_6015_p1_404,"",  0,  0,false,false},   // POS_CRS_04			
	{"sd_6015_p1_405",sd_6015_p1_405,"",  0,  0,false,false},   // POS_YOKE_ICON		
	{"sd_6015_p1_406",sd_6015_p1_406,"",  0,  0,false,false},   // POS_AGB_MAIN_PITCH  
	{"sd_6015_p1_407",sd_6015_p1_407,"",  0,  0,false,false},   // POS_KPPMS_BLK_COURSE
	{"sd_6015_p1_408",sd_6015_p1_408,"",  0,  0,false,false},   // POS_KPPMS_BLK_GLIDE 
	{"sd_6015_p1_409",sd_6015_p1_409,"",  0,  0,false,false},   // POS_UVID_MB     
	{"sd_6015_p1_410",sd_6015_p1_410,"",26.40,31.30,false,false},   // POS_UVID_HG     
	{"sd_6015_p1_411",sd_6015_p1_411,"",  0,  0,false,false},   // POS_UVID_MM     
	{"sd_6015_p0_412",sd_6015_p0_412,"",  0,  0,false,false},   // POS_UVID_PSI_100_MM    
	{"sd_6015_p0_413",sd_6015_p0_413,"",  0,  0,false,false},   // POS_UVID_PSI_10_MM     
	{"sd_6015_p0_414",sd_6015_p0_414,"",  0,  0,false,false},   // POS_UVID_PSI_1_MM      
	{"sd_6015_p0_415",sd_6015_p0_415,"",  0,  0,false,false},   // POS_UVID_PSI_1000_MB    
	{"sd_6015_p0_416",sd_6015_p0_416,"",  0,  0,false,false},   // POS_UVID_PSI_100_MB    
	{"sd_6015_p0_417",sd_6015_p0_417,"",  0,  0,false,false},   // POS_UVID_PSI_10_MB     
	{"sd_6015_p0_418",sd_6015_p0_418,"",  0,  0,false,false},   // POS_UVID_PSI_1_MB      
	{"sd_6015_p0_419",sd_6015_p0_419,"",  0,  0,false,false},   // POS_UVID_PSI_1000_HG    
	{"sd_6015_p0_420",sd_6015_p0_420,"",  0,  0,false,false},   // POS_UVID_PSI_100_HG    
	{"sd_6015_p0_421",sd_6015_p0_421,"",  0,  0,false,false},   // POS_UVID_PSI_10_HG     
	{"sd_6015_p0_422",sd_6015_p0_422,"",  0,  0,false,false},   // POS_UVID_PSI_1_HG      
											
	// P4									
	{"sd_6015_p4_401",sd_6015_p4_401,"",  0,  0,false,false},	// POS_CRS_01                      
	{"sd_6015_p4_402",sd_6015_p4_402,"",  0,  0,false,false},	// POS_CRS_02                      
	{"sd_6015_p4_403",sd_6015_p4_403,"",  0,  0,false,false},	// POS_CRS_03                      
	{"sd_6015_p4_404",sd_6015_p4_404,"",  0,  0,false,false},	// POS_STOPOR                      
	{"sd_6015_p4_405",sd_6015_p4_405,"",  0,  0,false,false},	// POS_ENGSTART1                   
	{"sd_6015_p4_406",sd_6015_p4_406,"",  0,  0,false,false},	// POS_ENGSTART2                   
	{"sd_6015_p4_407",sd_6015_p4_407,"",  0,  0,false,false},	// POS_ENGSTART3                   
	{"sd_6015_p4_408",sd_6015_p4_408,"",  0,  0,false,false},	// POS_RUD1                        
	{"sd_6015_p4_409",sd_6015_p4_409,"",  0,  0,false,false},	// POS_RUD2                        
	{"sd_6015_p4_410",sd_6015_p4_410,"",  0,  0,false,false},	// POS_RUD3                        
	{"sd_6015_p4_411",sd_6015_p4_411,"",  0,  0,false,false},	// POS_PRKBRK                      
	{"sd_6015_p4_412",sd_6015_p4_412,"",  0,  0,false,false},	// POS_STOPOR_TRIGGER              
	{"sd_6015_p4_413",sd_6015_p4_413,"",  0,  0,false,false},	// POS_BRAKE_TRIGGER               
	{"sd_6015_p4_414",sd_6015_p4_414,"",  0,  0,false,false},	// POS_AP_PITCH_MODE             
	{"sd_6015_p4_415",sd_6015_p4_415,"",  0,  0,false,false},	// POS_AP_BANK_MODE              
	{"sd_6015_p4_416",sd_6015_p4_416,"",  0,  0,false,false},	// POS_AP_HAND_MODE              
	{"sd_6015_p4_417",sd_6015_p4_417,"",  0,  0,false,false},	// POS_AP_MOUSE_HND_PRESS        
	{"sd_6015_p4_418",sd_6015_p4_418,"",  0,  0,false,false},	// POS_RUD1_STOP                        
	{"sd_6015_p4_419",sd_6015_p4_419,"",  0,  0,false,false},	// POS_RUD2_STOP                        
	{"sd_6015_p4_420",sd_6015_p4_420,"",  0,  0,false,false},	// POS_RUD3_STOP                        
										
	// P5								
	{"sd_6015_p5_401",sd_6015_p5_401,"",  0,  0,false,false},   // POS_CRS_01		
	{"sd_6015_p5_402",sd_6015_p5_402,"",  0,  0,false,false},   // POS_CRS_02		
	{"sd_6015_p5_403",sd_6015_p5_403,"",  0,  0,false,false},   // POS_CRS_03		
	{"sd_6015_p5_404",sd_6015_p5_404,"",  0, 17,true ,false},   // POS_SCLVHF1LEFT	
	{"sd_6015_p5_405",sd_6015_p5_405,"",  0, 17,true ,false},   // POS_SCLVHF2LEFT	
	{"sd_6015_p5_406",sd_6015_p5_406,"",  0,  9,true ,false},   // POS_SCLVHF1MID  
	{"sd_6015_p5_407",sd_6015_p5_407,"",  0,  9,true ,false},   // POS_SCLVHF2MID  
	{"sd_6015_p5_408",sd_6015_p5_408,"",  0,  3,true ,false},   // POS_SCLVHF1RIGHT
	{"sd_6015_p5_409",sd_6015_p5_409,"",  0,  3,true ,false},   // POS_SCLVHF2RIGHT
	{"sd_6015_p5_410",sd_6015_p5_410,"",  0,  0,false,false},   // POS_ADF1LFREQ
	{"sd_6015_p5_411",sd_6015_p5_411,"",  0,  0,false,false},   // POS_ADF1RFREQ
	{"sd_6015_p5_412",sd_6015_p5_412,"",  0,  0,false,false},   // POS_ADF2LFREQ
	{"sd_6015_p5_413",sd_6015_p5_413,"",  0,  0,false,false},   // POS_ADF2RFREQ
										
	// P6								
	{"sd_6015_p6_401",sd_6015_p6_401,"",  0,  0,false,false},   // POS_CRS_01		
	{"sd_6015_p6_402",sd_6015_p6_402,"",  0,  7,true ,false},   // POS_XPDR1		
	{"sd_6015_p6_403",sd_6015_p6_403,"",  0,  7,true ,false},   // POS_XPDR2		
	{"sd_6015_p6_404",sd_6015_p6_404,"",  0,  7,true ,false},   // POS_XPDR3		
	{"sd_6015_p6_405",sd_6015_p6_405,"",  0,  7,true ,false},   // POS_XPDR4		
	{"sd_6015_p6_406",sd_6015_p6_406,"",  0,  0,false,false},   // POS_NAV_FREQ1	
	{"sd_6015_p6_407",sd_6015_p6_407,"",  0,  0,false,false},   // POS_NAV_FREQ2	
	{"sd_6015_p6_408",sd_6015_p6_408,"",  0,  0,false,false},   // POS_NAV_FREQ3	
	{"sd_6015_p6_409",sd_6015_p6_409,"",  0,  0,false,false},   // POS_NAV_FREQ4	
	{"sd_6015_p6_410",sd_6015_p6_410,"",  0,  0,false,false},   // POS_NAV_FREQ5	
	{"sd_6015_p6_411",sd_6015_p6_411,"",  0,  0,false,false},   // POS_OBS1		
	{"sd_6015_p6_412",sd_6015_p6_412,"",  0,  0,false,false},   // POS_OBS2		
	{"sd_6015_p6_413",sd_6015_p6_413,"",  0,  0,false,false},   // POS_OBS3		
	{"sd_6015_p6_414",sd_6015_p6_414,"",  0,  0,false,false},   // POS_DME1		
	{"sd_6015_p6_415",sd_6015_p6_415,"",  0,  0,false,false},   // POS_DME2		
	{"sd_6015_p6_416",sd_6015_p6_416,"",  0,  0,false,false},   // POS_DME3		
	{"sd_6015_p6_417",sd_6015_p6_417,"",  0,  0,false,false},   // POS_DME4		
	{"sd_6015_p6_418",sd_6015_p6_418,"",  0,  0,false,false},   // POS_DME5		
	{"sd_6015_p6_419",sd_6015_p6_419,"",  0,  0,false,false},   // POS_DME1M		
	{"sd_6015_p6_420",sd_6015_p6_420,"",  0,  0,false,false},   // POS_DME2M		
	{"sd_6015_p6_421",sd_6015_p6_421,"",  0,  0,false,false},   // POS_DME3M		
	{"sd_6015_p6_422",sd_6015_p6_422,"",  0,  0,false,false},   // POS_DME4M		
	{"sd_6015_p6_423",sd_6015_p6_423,"",  0,  0,false,false},   // POS_DME5M		
															   
	// P7													   
	{"sd_6015_p7_401",sd_6015_p7_401,"",  0,  0,false,false},   // POS_CRS_01		
	{"sd_6015_p7_402",sd_6015_p7_402,"",  0,  0,false,false},   // POS_NAV_FREQ1	
	{"sd_6015_p7_403",sd_6015_p7_403,"",  0,  0,false,false},   // POS_NAV_FREQ2	
	{"sd_6015_p7_404",sd_6015_p7_404,"",  0,  0,false,false},   // POS_NAV_FREQ3	
	{"sd_6015_p7_405",sd_6015_p7_405,"",  0,  0,false,false},   // POS_NAV_FREQ4	
	{"sd_6015_p7_406",sd_6015_p7_406,"",  0,  0,false,false},   // POS_NAV_FREQ5	
	{"sd_6015_p7_407",sd_6015_p7_407,"",  0,  0,false,false},   // POS_OBS1		
	{"sd_6015_p7_408",sd_6015_p7_408,"",  0,  0,false,false},   // POS_OBS2		
	{"sd_6015_p7_409",sd_6015_p7_409,"",  0,  0,false,false},   // POS_OBS3		
	{"sd_6015_p7_410",sd_6015_p7_410,"",  0, 45,false,false},   // POS_PRESSRATE	
	{"sd_6015_p7_411",sd_6015_p7_411,"",  0,  0,false,false},   // POS_DME1
	{"sd_6015_p7_412",sd_6015_p7_412,"",  0,  0,false,false},   // POS_DME2
	{"sd_6015_p7_413",sd_6015_p7_413,"",  0,  0,false,false},   // POS_DME3
	{"sd_6015_p7_414",sd_6015_p7_414,"",  0,  0,false,false},   // POS_DME4
	{"sd_6015_p7_415",sd_6015_p7_415,"",  0,  0,false,false},   // POS_DME5
	{"sd_6015_p7_416",sd_6015_p7_416,"",  0,  0,false,false},   // POS_DME1M
	{"sd_6015_p7_417",sd_6015_p7_417,"",  0,  0,false,false},   // POS_DME2M
	{"sd_6015_p7_418",sd_6015_p7_418,"",  0,  0,false,false},   // POS_DME3M
	{"sd_6015_p7_419",sd_6015_p7_419,"",  0,  0,false,false},   // POS_DME4M
	{"sd_6015_p7_420",sd_6015_p7_420,"",  0,  0,false,false},   // POS_DME5M

	// P23
	{"sd_6015_p23_401",sd_6015_p23_401,"",  0,  0,false,false},   // POS_NAV_P0
	{"sd_6015_p23_402",sd_6015_p23_402,"",  0,  0,false,false},   // POS_NAV_P1
	{"sd_6015_p23_403",sd_6015_p23_403,"",  0,  0,false,false},   // POS_NAV_P4
	{"sd_6015_p23_404",sd_6015_p23_404,"",  0,  0,false,false},   // POS_NAV_P5
	{"sd_6015_p23_405",sd_6015_p23_405,"",  0,  0,false,false},   // POS_NAV_P6
	{"sd_6015_p23_406",sd_6015_p23_406,"",  0,  0,false,false},   // POS_NAV_P7
	{"sd_6015_p23_407",sd_6015_p23_407,"",  0,  0,false,false},   // POS_NAV_P8
	{"sd_6015_p23_408",sd_6015_p23_408,"",  0,  0,false,false},   // POS_NAV_P9

};														  
														  
