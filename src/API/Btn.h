/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Btn.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

enum BTN {
	// �����
	BTN_CLOCK_HRONO					,	// "sd_6015_pa_101"
	BTN_CLOCK_START					,	// "sd_6015_pa_102"
	BTN_MAN_FIRE_EXTNG_ENG_BAY1		,	// "sd_6015_pa_103"
	BTN_MAN_FIRE_EXTNG_ENG_BAY2		,	// "sd_6015_pa_104"
	BTN_MAN_FIRE_EXTNG_ENG_BAY3		,	// "sd_6015_pa_105"
	BTN_MAN_FIRE_EXTNG_ENG1			,	// "sd_6015_pa_106"
	BTN_MAN_FIRE_EXTNG_ENG2			,	// "sd_6015_pa_107"
	BTN_MAN_FIRE_EXTNG_ENG3			,	// "sd_6015_pa_108"
	BTN_MAN_FIRE_EXTNG_APU			,	// "sd_6015_pa_109"
	BTN_FIRE_LIGHTS_TEST			,	// "sd_6015_pa_110"
	BTN_GEAR_LIGHTS_TEST			,	// "sd_6015_pa_111"

	// P0
	BTN_LIGHTS_TEST0				,	// "sd_6015_p0_101"
	BTN_DIS_FIRE_ALARM				,	// "sd_6015_p0_102"
	BTN_DIS_ALARM_HYD_PUMP_1_FAIL	,	// "sd_6015_p0_103"
	BTN_DIS_ALARM_HYD_PUMP_2_FAIL	,	// "sd_6015_p0_104"
	BTN_AGB_MAIN_ARRET0				,	// "sd_6015_p0_105"
	BTN_AGB_AUXL_ARRET				,	// "sd_6015_p0_106"
	BTN_PPTIZ_TEST_0				,	// "sd_6015_p0_107"
	BTN_PPTIZ_TEST_4000				,	// "sd_6015_p0_108"
	BTN_GFORCE_RESET				,	// "sd_6015_p0_109"

	// P1
	BTN_LIGHTS_TEST1                ,   // "sd_6015_p1_101"
	BTN_AGB_MAIN_ARRET1             ,   // "sd_6015_p1_102"

	// P2
	BTN_TRIMMAIN2                   ,   // "sd_6015_p2_101"
	BTN_TRIMEMERG2                  ,   // "sd_6015_p2_102"
	BTN_APOFF2                      ,   // "sd_6015_p2_103"
	BTN_COM2                        ,   // "sd_6015_p2_104"
	BTN_CLOSE2 						,	// "sd_6015_p2_105"

	// P3
	BTN_TRIMMAIN3                   ,   // "sd_6015_p3_101"
	BTN_TRIMEMERG3                  ,   // "sd_6015_p3_102"
	BTN_APOFF3                      ,   // "sd_6015_p3_103"
	BTN_COM3                        ,   // "sd_6015_p3_104"
	BTN_CLOSE3 						,	// "sd_6015_p3_105"

	// P4
	BTN_LIGHTS_TEST4	   	        ,	// "sd_6015_p4_101"	
	BTN_ACT_TEST			        ,	// "sd_6015_p4_102"	
	BTN_ENGSTART1		            ,	// "sd_6015_p4_103"	
	BTN_ENGSTART2		            ,	// "sd_6015_p4_104"	
	BTN_ENGSTART3		            ,	// "sd_6015_p4_105"	
	BTN_SHASSIAVARUBOR	            ,	// "sd_6015_p4_106"

	// P6
	BTN_SO72CONTROL	                ,   // "sd_6015_p6_101"
	BTN_SO72ID		                ,   // "sd_6015_p6_102"
	BTN_KMP1TEST1	                ,   // "sd_6015_p6_103"
	BTN_KMP1TEST2	                ,   // "sd_6015_p6_104"
	BTN_KMP1TEST3	                ,   // "sd_6015_p6_105"
	BTN_STARTAI9	                ,   // "sd_6015_p6_106"
	BTN_STOPAI9		                ,   // "sd_6015_p6_107"
	BTN_STARTAI25	                ,   // "sd_6015_p6_108"
	BTN_STOPAI25	                ,   // "sd_6015_p6_109"
	BTN_KMP1ID		                ,   // "sd_6015_p6_110"

	// P7
	BTN_KMP2ID		                ,   // "sd_6015_p7_101"
	BTN_KMP2TEST1	                ,   // "sd_6015_p7_102"
	BTN_KMP2TEST2	                ,   // "sd_6015_p7_103"
	BTN_KMP2TEST3	                ,   // "sd_6015_p7_104"
	BTN_LIGHTS_TEST7                ,   // "sd_6015_p7_105"

	//
	BTN_MAX
};
