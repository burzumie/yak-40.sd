/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Tbg.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Tbg.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_pa_801	"WARN GEAR"                       
#define sd_6015_pa_802	""                                
#define sd_6015_pa_803	""                                
#define sd_6015_pa_804	""                                
#define sd_6015_pa_805	"WARN FLAPS"                      
#define sd_6015_pa_806	""                                
#define sd_6015_pa_807	""                                
#define sd_6015_pa_808	""                                

#else

#define sd_6015_pa_801	""
#define sd_6015_pa_802	""
#define sd_6015_pa_803	""
#define sd_6015_pa_804	""
#define sd_6015_pa_805	""
#define sd_6015_pa_806	""
#define sd_6015_pa_807	""
#define sd_6015_pa_808	""

#endif

ApiName TbgNames[TBG_MAX]={
	{"sd_6015_pa_801",sd_6015_pa_801,"",0,0,false,false},  // TBG_WARN_GEAR
	{"sd_6015_pa_802",sd_6015_pa_802,"",0,0,false,false},  // TBG_GEAR1_UP	
	{"sd_6015_pa_803",sd_6015_pa_803,"",0,0,false,false},  // TBG_GEAR2_UP	
	{"sd_6015_pa_804",sd_6015_pa_804,"",0,0,false,false},  // TBG_GEAR3_UP	
	{"sd_6015_pa_805",sd_6015_pa_805,"",0,0,false,false},  // TBG_WARN_FLAP
	{"sd_6015_pa_806",sd_6015_pa_806,"",0,0,false,false},  // TBG_GEAR1_DOWN
	{"sd_6015_pa_807",sd_6015_pa_807,"",0,0,false,false},  // TBG_GEAR2_DOWN
	{"sd_6015_pa_808",sd_6015_pa_808,"",0,0,false,false},  // TBG_GEAR3_DOWN
};                    

ApiName TbgNamesE[TBG_MAX]={
	{"sd_6015_pa_801e","","",0,0,false,false},  // TBG_WARN_GEAR
	{"sd_6015_pa_802e","","",0,0,false,false},  // TBG_GEAR1_UP	
	{"sd_6015_pa_803e","","",0,0,false,false},  // TBG_GEAR2_UP	
	{"sd_6015_pa_804e","","",0,0,false,false},  // TBG_GEAR3_UP	
	{"sd_6015_pa_805e","","",0,0,false,false},  // TBG_WARN_FLAP
	{"sd_6015_pa_806e","","",0,0,false,false},  // TBG_GEAR1_DOWN
	{"sd_6015_pa_807e","","",0,0,false,false},  // TBG_GEAR2_DOWN
	{"sd_6015_pa_808e","","",0,0,false,false},  // TBG_GEAR3_DOWN
};                    
