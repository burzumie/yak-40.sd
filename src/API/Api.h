/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Api.h $

  Last modification:
    $Date: 19.02.06 10:43 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Common/CommonSys.h"
#include "../Lib/IniFile.h"
#include "../Lib/Strings.h"
#include "../Lib/NamedVar.h"
/*
#include "../Lib/ctimer.h"
#include "../Lib/EString.h"
#include "../Lib/Undoc.h"
*/

#include "Constants9.h"
#include "Config.h"
#include "SimVars.h"
#include "APIEntry.h"

using namespace std;

class SDAPITooltipBase
{
private:
	static std::list<SDAPITooltipBase*> m_ChildList;

public:
	SDAPITooltipBase() {		
		m_ChildList.push_back(this);
	};

	~SDAPITooltipBase() {
		m_ChildList.remove(this);
	};

	virtual void Update() = 0;

	static void UpdateAll() {
		std::list<SDAPITooltipBase*>::const_iterator end(m_ChildList.end());
		for(std::list<SDAPITooltipBase*>::const_iterator i=m_ChildList.begin();i!=end;++i) 
			(*i)->Update();
	};
};

#define MAX_VPARAMS	255
#define MAX_LPARAMS	255
#define MAX_CPARAMS	255
#define MAX_APARAMS	255

class SDAPITooltip : public SDAPITooltipBase {
private:

	std::wstring m_SrcToolTip;
	char m_ReadyToolTip[BUFSIZ];
	bool m_NeedParse;
	bool m_NeedMathParse;

	std::map<int,std::wstring> m_VParams[MAX_VPARAMS];
	bool m_VParamRead[MAX_VPARAMS];
	int m_TotalVParams;

	CNamedVar *m_LParams[MAX_LPARAMS];
	bool m_LParamRead[MAX_LPARAMS];
	int m_TotalLParams;

	size_t GetVal(WCHAR *val, WCHAR *dest, size_t start, double simval);
	size_t GetMathResult(CHAR *val, CHAR *dest, size_t start);
	void GetVParam(WCHAR *in, WCHAR *dest, double simval, int param);
	void GetLParam(WCHAR *in, WCHAR *dest, int param);
	void GetCParam(WCHAR *in, WCHAR *dest);
	void GetAParam(WCHAR *in, WCHAR *dest);
	void PrintVal(WCHAR *dest,const char *type,double val);
	void MathParse();

/*
	Type:
	1 - SDAZS	
	2 - SDBTN	
	3 - SDGLT	
	4 - SDKRM	
	5 - SDHND	
	6 - SDLMP	
	7 - SDLMPE	
	8 - SDNDL	
	9 - SDPOS	
   10 - SDTBG	
   11 - SDTBGE	
   12 - SDTBL	
   13 - SDTBLE	
   14 - SDPWR	
   15 - SDCFG	
   16 - SDKEY	
   16 - SDKLN	
*/
	int m_Type;	
	int m_NumInApi;

public:
	SDAPITooltip(char *name);
	~SDAPITooltip();

	void SetTT(std::wstring val, int type, int num);
	void Update();
	void SetToApi();
	double GetFromApi();
	void Parse(double val);

};

/*
class CTimerTT : public CTimer
{
private:
	int m_Tick;

public:
	CTimerTT(FS6 *var):CTimer(var){m_Tick=0;};
	virtual ~CTimerTT(){StopTimer();};
protected:
	void fnHandler() {
		SDAPITooltipBase::UpdateAll();
	};
};
*/

class SDAPI : public SDSimVars
{
private:
	//auto_ptr<CTimerTT>		m_TimerTT;
	bool					m_UpdateTT;

public:
	auto_ptr<SDAPITooltip>	m_Azs[AZS_MAX];
	auto_ptr<SDAPITooltip>	m_Btn[BTN_MAX];
	auto_ptr<SDAPITooltip>	m_Glt[GLT_MAX];
	auto_ptr<SDAPITooltip>	m_Krm[KRM_MAX];
	auto_ptr<SDAPITooltip>	m_Hnd[HND_MAX];
	auto_ptr<SDAPITooltip>	m_Lmp[LMP_MAX];
	auto_ptr<SDAPITooltip>	m_Ndl[NDL_MAX];
	auto_ptr<SDAPITooltip>	m_Pos[POS_MAX];
	auto_ptr<SDAPITooltip>	m_Tbg[TBG_MAX];
	auto_ptr<SDAPITooltip>	m_Tbl[TBL_MAX];
	auto_ptr<SDAPITooltip>	m_Key[KEY_MAX];
	auto_ptr<SDAPITooltip>	m_Kln[KLN_MAX];

public:
	char		m_SimPath	[MAX_PATH];
	char		m_GaugePath	[MAX_PATH];
	char		m_SoundPath	[MAX_PATH];
	char		m_NavBasePath[MAX_PATH];
	SDConfig	m_Config;

	SDAPI(bool logic=false);
	~SDAPI();

	void Update();
	void Prepare();
	void UpdateAPI();
	void PrepareTT();
};

extern SDAPI *g_pSDAPI;

