/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/ApiName.h $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

typedef struct ApiName {
	char *name;
	char *tt;
	char *desc;
	double min;
	double max;
	bool loop;
	bool isexprt;
}ApiName;
