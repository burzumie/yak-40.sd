/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Lmp.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Lmp.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_pa_901	"ENG1 BAY ON FIRE"                
#define sd_6015_pa_902	"ENG2 BAY ON FIRE"                
#define sd_6015_pa_903	"ENG3 BAY ON FIRE"                
#define sd_6015_pa_904	"ENG1 ON FIRE"                    
#define sd_6015_pa_905	"ENG2 ON FIRE"                    
#define sd_6015_pa_906	"ENG3 ON FIRE"                    
#define sd_6015_pa_907	"EXTING1 VALVE OPEN"              
#define sd_6015_pa_908	"EXTING2 VALVE OPEN"              
#define sd_6015_pa_909	"EXTING3 VALVE OPEN"              
#define sd_6015_pa_910	"EXTING1 VALVE OPEN"              
#define sd_6015_pa_911	"EXTING2 VALVE OPEN"              
#define sd_6015_pa_912	"EXTING3 VALVE OPEN"              
#define sd_6015_pa_913	"FIRE SND ALARM OFF"              
#define sd_6015_pa_914	"STAIRS DWN"                      
#define sd_6015_pa_915	"INV 115V FAIL"                   
#define sd_6015_pa_916	"GROUND ELEC CONNECTED"           
#define sd_6015_pa_917	"DIF AIR PRESS DROP"              
#define sd_6015_pa_918	"AIR PRESS BCKUP CNTRL UNIT ON"   
#define sd_6015_pa_919	"AIR PRESS-COND SYS OFF"          
#define sd_6015_pa_920	"AIR COND SYS TURBO COOLER ON"    
#define sd_6015_pa_921	""                                
#define sd_6015_pa_922	""                                
#define sd_6015_pa_923	"FIRE EXTING1 RDY"                
#define sd_6015_pa_924	"FIRE EXTING2 RDY"                
#define sd_6015_pa_925	"FIRE EXTING3 RDY"                
#define sd_6015_pa_926	"FIRE EXTING4 RDY"                
#define sd_6015_pa_927	""                                
#define sd_6015_pa_928	""                                
#define sd_6015_pa_929	""                                
															
															
#define sd_6015_p4_901	"THR REVERSE ON"                  
#define sd_6015_p4_902	"THR REVERSE OFF"                 
#define sd_6015_p4_903	"THR REVERSE ON"                  
#define sd_6015_p4_904	"THR REVERSE OFF"                 
#define sd_6015_p4_905	"L FUEL PUMP ON"                  
#define sd_6015_p4_906	"R FUEL PUMP ON"                  
#define sd_6015_p4_907	"F TANKS L-R CONNECTED"           
#define sd_6015_p4_908	"F PUMPS L-R CONNECTED"           
#define sd_6015_p4_909	"X-FEED SYS FAIL"                 
#define sd_6015_p4_910	"X-FEED SYS OK"                   
#define sd_6015_p4_911	"CHARGE EMERG BRAKES SYS"         
#define sd_6015_p4_912	"AILER TRIM NEUTR"                
#define sd_6015_p4_913	"RUDDER TRIM NEUTR"               
#define sd_6015_p4_914	"AP RDY"                          
#define sd_6015_p4_915	""                                
#define sd_6015_p4_916	""                                
#define sd_6015_p4_917	""                                
#define sd_6015_p4_918	""                                
															
															
#define sd_6015_p7_901	"ENG1 DEICE"                      
#define sd_6015_p7_902	"ENG2 DEICE"                      
#define sd_6015_p7_903	"ENG3 DEICE"                      

#else

#define sd_6015_pa_901	""
#define sd_6015_pa_902	""
#define sd_6015_pa_903	""
#define sd_6015_pa_904	""
#define sd_6015_pa_905	""
#define sd_6015_pa_906	""
#define sd_6015_pa_907	""
#define sd_6015_pa_908	""
#define sd_6015_pa_909	""
#define sd_6015_pa_910	""
#define sd_6015_pa_911	""
#define sd_6015_pa_912	""
#define sd_6015_pa_913	""
#define sd_6015_pa_914	""
#define sd_6015_pa_915	""
#define sd_6015_pa_916	""
#define sd_6015_pa_917	""
#define sd_6015_pa_918	""
#define sd_6015_pa_919	""
#define sd_6015_pa_920	""
#define sd_6015_pa_921	""
#define sd_6015_pa_922	""
#define sd_6015_pa_923	""
#define sd_6015_pa_924	""
#define sd_6015_pa_925	""
#define sd_6015_pa_926	""
#define sd_6015_pa_927	""
#define sd_6015_pa_928	""
#define sd_6015_pa_929	""
							
							
#define sd_6015_p4_901	""
#define sd_6015_p4_902	""
#define sd_6015_p4_903	""
#define sd_6015_p4_904	""
#define sd_6015_p4_905	""
#define sd_6015_p4_906	""
#define sd_6015_p4_907	""
#define sd_6015_p4_908	""
#define sd_6015_p4_909	""
#define sd_6015_p4_910	""
#define sd_6015_p4_911	""
#define sd_6015_p4_912	""
#define sd_6015_p4_913	""
#define sd_6015_p4_914	""
#define sd_6015_p4_915	""
#define sd_6015_p4_916	""
#define sd_6015_p4_917	""
#define sd_6015_p4_918	""
							
							
#define sd_6015_p7_901	""
#define sd_6015_p7_902	""
#define sd_6015_p7_903	""

#endif

ApiName LmpNames[LMP_MAX]={
	// �����
	{"sd_6015_pa_901",sd_6015_pa_901,"",0,0,false,false},	// LMP_ENG1_BAY_ON_FIRE        
	{"sd_6015_pa_902",sd_6015_pa_902,"",0,0,false,false},	// LMP_ENG2_BAY_ON_FIRE        
	{"sd_6015_pa_903",sd_6015_pa_903,"",0,0,false,false},	// LMP_ENG3_BAY_ON_FIRE        
	{"sd_6015_pa_904",sd_6015_pa_904,"",0,0,false,false},	// LMP_ENG1_ON_FIRE            
	{"sd_6015_pa_905",sd_6015_pa_905,"",0,0,false,false},	// LMP_ENG2_ON_FIRE            
	{"sd_6015_pa_906",sd_6015_pa_906,"",0,0,false,false},	// LMP_ENG3_ON_FIRE            
	{"sd_6015_pa_907",sd_6015_pa_907,"",0,0,false,false},	// LMP_EXTING1_VALVE_OPEN1     
	{"sd_6015_pa_908",sd_6015_pa_908,"",0,0,false,false},	// LMP_EXTING2_VALVE_OPEN1     
	{"sd_6015_pa_909",sd_6015_pa_909,"",0,0,false,false},	// LMP_EXTING3_VALVE_OPEN1     
	{"sd_6015_pa_910",sd_6015_pa_910,"",0,0,false,false},	// LMP_EXTING1_VALVE_OPEN2     
	{"sd_6015_pa_911",sd_6015_pa_911,"",0,0,false,false},	// LMP_EXTING2_VALVE_OPEN2     
	{"sd_6015_pa_912",sd_6015_pa_912,"",0,0,false,false},	// LMP_EXTING3_VALVE_OPEN2     
	{"sd_6015_pa_913",sd_6015_pa_913,"",0,0,false,false},	// LMP_FIRE_SND_ALARM_OFF      
	{"sd_6015_pa_914",sd_6015_pa_914,"",0,0,false,false},	// LMP_STAIRS_DWN              
	{"sd_6015_pa_915",sd_6015_pa_915,"",0,0,false,false},	// LMP_INV_115V_FAIL           
	{"sd_6015_pa_916",sd_6015_pa_916,"",0,0,false,false},	// LMP_GROUND_ELEC_CONNECTED   
	{"sd_6015_pa_917",sd_6015_pa_917,"",0,0,false,false},	// LMP_DIF_AIR_PRESS_DROP      
	{"sd_6015_pa_918",sd_6015_pa_918,"",0,0,false,false},	// LMP_AIR_PRESSBCKCNTRUNITON  
	{"sd_6015_pa_919",sd_6015_pa_919,"",0,0,false,false},	// LMP_AIR_PRESS_COND_SYS_OFF  
	{"sd_6015_pa_920",sd_6015_pa_920,"",0,0,false,false},	// LMP_AIR_CONDSYSTURBOCOOLERON
	{"sd_6015_pa_921",sd_6015_pa_921,"",0,0,false,false},	// LMP_GMK_ZAP                 
	{"sd_6015_pa_922",sd_6015_pa_922,"",0,0,false,false},	// LMP_GMK_OSN                 
	{"sd_6015_pa_923",sd_6015_pa_923,"",0,0,false,false},	// LMP_FIRE_EXTING1_RDY        
	{"sd_6015_pa_924",sd_6015_pa_924,"",0,0,false,false},	// LMP_FIRE_EXTING2_RDY        
	{"sd_6015_pa_925",sd_6015_pa_925,"",0,0,false,false},	// LMP_FIRE_EXTING3_RDY        
	{"sd_6015_pa_926",sd_6015_pa_926,"",0,0,false,false},	// LMP_FIRE_EXTING4_RDY        
	{"sd_6015_pa_927",sd_6015_pa_927,"",0,0,false,false},	// LMP_ENG1PK                  
	{"sd_6015_pa_928",sd_6015_pa_928,"",0,0,false,false},	// LMP_ENG2PK                  
	{"sd_6015_pa_929",sd_6015_pa_929,"",0,0,false,false},	// LMP_ENG3PK                  
												 
	// P4										 
	{"sd_6015_p4_901",sd_6015_p4_901,"",0,0,false,false},	// LMP_REVERSEON                   
	{"sd_6015_p4_902",sd_6015_p4_902,"",0,0,false,false},	// LMP_REVERSEOFF                  
	{"sd_6015_p4_903",sd_6015_p4_903,"",0,0,false,false},	// LMP_CONTR_REVERSEON             
	{"sd_6015_p4_904",sd_6015_p4_904,"",0,0,false,false},	// LMP_CONTR_REVERSEOFF            
	{"sd_6015_p4_905",sd_6015_p4_905,"",0,0,false,false},	// LMP_TOPL_LEV                    
	{"sd_6015_p4_906",sd_6015_p4_906,"",0,0,false,false},	// LMP_TOPL_PRAV                   
	{"sd_6015_p4_907",sd_6015_p4_907,"",0,0,false,false},	// LMP_OBYED                       
	{"sd_6015_p4_908",sd_6015_p4_908,"",0,0,false,false},	// LMP_KOLTSEV                     
	{"sd_6015_p4_909",sd_6015_p4_909,"",0,0,false,false},	// LMP_ACT_TEST_FAIL               
	{"sd_6015_p4_910",sd_6015_p4_910,"",0,0,false,false},	// LMP_ACT_TEST_OK                 
	{"sd_6015_p4_911",sd_6015_p4_911,"",0,0,false,false},	// LMP_ZAR_AVAR_TORM               
	{"sd_6015_p4_912",sd_6015_p4_912,"",0,0,false,false},	// LMP_TRIM_ELERON                 
	{"sd_6015_p4_913",sd_6015_p4_913,"",0,0,false,false},	// LMP_TRIM_RUDDER                 
	{"sd_6015_p4_914",sd_6015_p4_914,"",0,0,false,false},	// LMP_AP_RDY                      
	{"sd_6015_p4_915",sd_6015_p4_915,"",0,0,false,false},	// LMP_K1                          
	{"sd_6015_p4_916",sd_6015_p4_916,"",0,0,false,false},	// LMP_G1                          
	{"sd_6015_p4_917",sd_6015_p4_917,"",0,0,false,false},	// LMP_K2                          
	{"sd_6015_p4_918",sd_6015_p4_918,"",0,0,false,false},	// LMP_G2                          
												 
	// P7										 
	{"sd_6015_p7_901",sd_6015_p7_901,"",0,0,false,false},	// LMP_ENG1_DEICE	
	{"sd_6015_p7_902",sd_6015_p7_902,"",0,0,false,false},	// LMP_ENG2_DEICE	
	{"sd_6015_p7_903",sd_6015_p7_903,"",0,0,false,false},	// LMP_ENG3_DEICE	
														   
};														   
														  
ApiName LmpNamesE[LMP_MAX]={
	// �����
	{"sd_6015_pa_901e","","",0,0,false,false},	// LMP_ENG1_BAY_ON_FIRE        
	{"sd_6015_pa_902e","","",0,0,false,false},	// LMP_ENG2_BAY_ON_FIRE        
	{"sd_6015_pa_903e","","",0,0,false,false},	// LMP_ENG3_BAY_ON_FIRE        
	{"sd_6015_pa_904e","","",0,0,false,false},	// LMP_ENG1_ON_FIRE            
	{"sd_6015_pa_905e","","",0,0,false,false},	// LMP_ENG2_ON_FIRE            
	{"sd_6015_pa_906e","","",0,0,false,false},	// LMP_ENG3_ON_FIRE            
	{"sd_6015_pa_907e","","",0,0,false,false},	// LMP_EXTING1_VALVE_OPEN1     
	{"sd_6015_pa_908e","","",0,0,false,false},	// LMP_EXTING2_VALVE_OPEN1     
	{"sd_6015_pa_909e","","",0,0,false,false},	// LMP_EXTING3_VALVE_OPEN1     
	{"sd_6015_pa_910e","","",0,0,false,false},	// LMP_EXTING1_VALVE_OPEN2     
	{"sd_6015_pa_911e","","",0,0,false,false},	// LMP_EXTING2_VALVE_OPEN2     
	{"sd_6015_pa_912e","","",0,0,false,false},	// LMP_EXTING3_VALVE_OPEN2     
	{"sd_6015_pa_913e","","",0,0,false,false},	// LMP_FIRE_SND_ALARM_OFF      
	{"sd_6015_pa_914e","","",0,0,false,false},	// LMP_STAIRS_DWN              
	{"sd_6015_pa_915e","","",0,0,false,false},	// LMP_INV_115V_FAIL           
	{"sd_6015_pa_916e","","",0,0,false,false},	// LMP_GROUND_ELEC_CONNECTED   
	{"sd_6015_pa_917e","","",0,0,false,false},	// LMP_DIF_AIR_PRESS_DROP      
	{"sd_6015_pa_918e","","",0,0,false,false},	// LMP_AIR_PRESSBCKCNTRUNITON  
	{"sd_6015_pa_919e","","",0,0,false,false},	// LMP_AIR_PRESS_COND_SYS_OFF  
	{"sd_6015_pa_920e","","",0,0,false,false},	// LMP_AIR_CONDSYSTURBOCOOLERON
	{"sd_6015_pa_921e","","",0,0,false,false},	// LMP_GMK_ZAP                 
	{"sd_6015_pa_922e","","",0,0,false,false},	// LMP_GMK_OSN                 
	{"sd_6015_pa_923e","","",0,0,false,false},	// LMP_FIRE_EXTING1_RDY        
	{"sd_6015_pa_924e","","",0,0,false,false},	// LMP_FIRE_EXTING2_RDY        
	{"sd_6015_pa_925e","","",0,0,false,false},	// LMP_FIRE_EXTING3_RDY        
	{"sd_6015_pa_926e","","",0,0,false,false},	// LMP_FIRE_EXTING4_RDY        
	{"sd_6015_pa_927e","","",0,0,false,false},	// LMP_ENG1PK                  
	{"sd_6015_pa_928e","","",0,0,false,false},	// LMP_ENG2PK                  
	{"sd_6015_pa_929e","","",0,0,false,false},	// LMP_ENG3PK                  

	// P4							 
	{"sd_6015_p4_901e","","",0,0,false,false},	// LMP_REVERSEON                   
	{"sd_6015_p4_902e","","",0,0,false,false},	// LMP_REVERSEOFF                  
	{"sd_6015_p4_903e","","",0,0,false,false},	// LMP_CONTR_REVERSEON             
	{"sd_6015_p4_904e","","",0,0,false,false},	// LMP_CONTR_REVERSEOFF            
	{"sd_6015_p4_905e","","",0,0,false,false},	// LMP_TOPL_LEV                    
	{"sd_6015_p4_906e","","",0,0,false,false},	// LMP_TOPL_PRAV                   
	{"sd_6015_p4_907e","","",0,0,false,false},	// LMP_OBYED                       
	{"sd_6015_p4_908e","","",0,0,false,false},	// LMP_KOLTSEV                     
	{"sd_6015_p4_909e","","",0,0,false,false},	// LMP_ACT_TEST_FAIL               
	{"sd_6015_p4_910e","","",0,0,false,false},	// LMP_ACT_TEST_OK                 
	{"sd_6015_p4_911e","","",0,0,false,false},	// LMP_ZAR_AVAR_TORM               
	{"sd_6015_p4_912e","","",0,0,false,false},	// LMP_TRIM_ELERON                 
	{"sd_6015_p4_913e","","",0,0,false,false},	// LMP_TRIM_RUDDER                 
	{"sd_6015_p4_914e","","",0,0,false,false},	// LMP_AP_RDY                      
	{"sd_6015_p4_915e","","",0,0,false,false},	// LMP_K1                          
	{"sd_6015_p4_916e","","",0,0,false,false},	// LMP_G1                          
	{"sd_6015_p4_917e","","",0,0,false,false},	// LMP_K2                          
	{"sd_6015_p4_918e","","",0,0,false,false},	// LMP_G2                          

	// P7							 
	{"sd_6015_p7_901e","","",0,0,false,false},	// LMP_ENG1_DEICE	
	{"sd_6015_p7_902e","","",0,0,false,false},	// LMP_ENG2_DEICE	
	{"sd_6015_p7_903e","","",0,0,false,false},	// LMP_ENG3_DEICE	

};														   
