/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/Glt.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

enum GLT {
	// P0
	GLT_IKU_LEFT					,	// "sd_6015_p0_301"
	GLT_IKU_RIGHT					,	// "sd_6015_p0_302"

	// P1
	GLT_TEMP_DUCT                   ,	// "sd_6015_p1_301"
	GLT_VOLT27                      ,   // "sd_6015_p1_302"
	GLT_VOLT36                      ,	// "sd_6015_p1_303"
	GLT_VOLT115                     ,	// "sd_6015_p1_304"

	// P4
	GLT_KMPSYS			            ,	// "sd_6015_p4_301"

	// P5
	GLT_ARK1                        ,	// "sd_6015_p5_301"
	GLT_ARK2                        ,   // "sd_6015_p5_302"
	GLT_ARK1LEFT100                 ,	// "sd_6015_p5_303"
	GLT_ARK2LEFT100                 ,	// "sd_6015_p5_304"
	GLT_ARK1RIGHT100                ,	// "sd_6015_p5_305"
	GLT_ARK2RIGHT100                ,   // "sd_6015_p5_306"
	GLT_ARK1LEFT10                  ,	// "sd_6015_p5_307"
	GLT_ARK2LEFT10                  ,	// "sd_6015_p5_308"
	GLT_ARK1RIGHT10                 ,	// "sd_6015_p5_309"
	GLT_ARK2RIGHT10                 ,	// "sd_6015_p5_310"
	GLT_ARK1LEFT1                   ,	// "sd_6015_p5_311"
	GLT_ARK2LEFT1                   ,   // "sd_6015_p5_312"
	GLT_ARK1RIGHT1                  ,	// "sd_6015_p5_313"
	GLT_ARK2RIGHT1                  ,	// "sd_6015_p5_314"
	GLT_ARK1LEFTHND                 ,	// "sd_6015_p5_315"
	GLT_ARK2LEFTHND                 ,   // "sd_6015_p5_316"
	GLT_ARK1RIGHTHND                ,	// "sd_6015_p5_317"
	GLT_ARK2RIGHTHND                ,	// "sd_6015_p5_318"
	GLT_ARK1LEFT1KNB                ,	// "sd_6015_p5_319"
	GLT_ARK2LEFT1KNB                ,	// "sd_6015_p5_320"
	GLT_ARK1RIGHT1KNB               ,	// "sd_6015_p5_321"
	GLT_ARK2RIGHT1KNB               ,   // "sd_6015_p5_322"

	// P6
	GLT_DME6			            ,	// "sd_6015_p6_301"
	GLT_SGU6			            ,	// "sd_6015_p6_302"
	GLT_XPDRMODE	                ,	// "sd_6015_p6_303"

	// P7
	GLT_DME7			            ,	// "sd_6015_p7_301"
	GLT_SGU7						,	// "sd_6015_p7_302"

	//
	GLT_MAX
};
