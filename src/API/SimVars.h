/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Api/SimVars.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Common/CommonSYS.h"
#ifdef PA_EXPORTS
#include "../Common/CommonLogic.h"
#else
#include "../Common/CommonVisual.h"
#endif

class SDSimVars
{
private:
	bool		m_StdtokensInited;
	MODULE_VAR	m_StdTokens[C_GAUGE_TOKEN];
	double		m_XmlTokens[GAUGE_XML_TOKEN];

public:
	SDSimVars() {
		m_StdtokensInited=false;
	}

	void InitVars() {
		if(!m_StdtokensInited) {
			for(int i=1;i<C_GAUGE_TOKEN;i++) {
				m_StdTokens[i].id=(GAUGE_TOKEN)i;
				ImportTable.pPanels->initialize_var(&m_StdTokens[i]);
			}
			m_StdtokensInited=true;
		}
	}

	void UpdateVars() {
		if(m_StdtokensInited) {
			for(int i=1;i<C_GAUGE_TOKEN;i++) {
				ImportTable.pPanels->lookup_var(&m_StdTokens[i]);
			}
		}
	}

	void UpdateVars(GAUGE_TOKEN token) {
		if(m_StdtokensInited) {
			ImportTable.pPanels->lookup_var(&m_StdTokens[token]);
		}
	}

	inline double GetN(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.n;	}
	inline ENUM   GetE(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.e;	}
	inline FLAGS  GetF(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.f;	}
	inline VAR32  GetD(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.d;	}
	inline BOOL   GetB(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.b;	}
	inline VAR32  GetO(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.o;	}
	inline PVOID  GetP(GAUGE_TOKEN var)	{ return m_StdTokens[var].var_value.p;	}

	inline double GetXML(XML_TOKEN var,char *type,int num=0) {
		ENUM e=ImportTable.pPanels->get_units_enum(type);
		m_XmlTokens[var]=ImportTable.pPanels->aircraft_varget(var,e,num);
		return m_XmlTokens[var];
	}

};
