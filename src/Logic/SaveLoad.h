/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/SaveLoad.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "Main.h"

class SDSaveThread 
{
private:
	auto_ptr<FLIGHT_FILE_INFO> m_FlightInfo;
	HANDLE dwChangeHandles;
	DWORD dwWaitStatus;

public:
	char m_sFileName[MAX_PATH];
	bool m_bFileChanged;

	SDSaveThread() {
		m_bFileChanged=false;
		m_FlightInfo=auto_ptr<FLIGHT_FILE_INFO>(new FLIGHT_FILE_INFO);
		//ImportTable.pFlight->flight_get(m_FlightInfo.get());
		GetFlightFileInfo(m_FlightInfo.get());
		strcpy(m_sFileName,m_FlightInfo->FlightFile.file_name);
		m_sFileName[strrchr(m_sFileName,'\\')-m_sFileName+1]='\0';
		dwChangeHandles=FindFirstChangeNotificationA(m_sFileName,FALSE,FILE_NOTIFY_CHANGE_FILE_NAME);
	}

	~SDSaveThread() {
	}

	void Update() {
		dwWaitStatus=WaitForSingleObject(dwChangeHandles,0/*INFINITE*/); 
		switch(dwWaitStatus) { 
			case WAIT_OBJECT_0: 
				//ImportTable.pFlight->flight_get(m_FlightInfo.get());
				GetFlightFileInfo(m_FlightInfo.get());
				sprintf(m_sFileName,"%s.%s",m_FlightInfo->FlightFile.file_name,SIM_FLT_EXTENSION.c_str());
				FindNextChangeNotification(dwChangeHandles);
				m_bFileChanged=true;
			break; 
		}
	}
};

class SDTimerSave : public CTimer
{
public:
	SDSaveThread m_SaveThread;
	SDTimerSave() : CTimer(ImportTable.pFS6) { StartTimer(CHAIN_PROCESS_FILE_IO); };
	virtual ~SDTimerSave(){ StopTimer(); };
protected:
	void fnHandler(){ m_SaveThread.Update(); };
};
