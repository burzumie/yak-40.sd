/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/HydroSystem.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "HydroSystem.h"

float SDHydroSystemBase::m_BrakeCoeffDefault;

void SDHydroSystemMain::Update()
{
	if(!AZS_GET(AZS_HYDRO_SYS_BUS)||!PWR_GET(PWR_BUS27)) {
		SetGear1FailFlag(1);
		SetGear1FailFlag(1);
		SetGear1FailFlag(1);
		SetHydraulicSystemIntegrity(0);
		SetBrakingCf(0);

		m_Quantity-=0.1;
		if(m_Quantity<-10)m_Quantity=-10;
		POS_SET(POS_MAIN_HYDRO_WORK,0);
		m_Played=false;
	} else {
		SetGear1FailFlag(0);
		SetGear1FailFlag(0);
		SetGear1FailFlag(0);
		SetHydraulicSystemIntegrity(1);
		SetBrakingCf(m_BrakeCoeffDefault);

		m_Quantity=((g_pSDAPI->GetN(HYDRAULICS1_PRESSURE_PSF)+g_pSDAPI->GetN(HYDRAULICS2_PRESSURE_PSF))/2)/2048;
		POS_SET(POS_MAIN_HYDRO_WORK,1);

		if(m_Quantity>89) {
			if(!m_Played) {
				SND_SET(SND_HYDRO_NORMAL,1);
				m_Played=true;
			}
		}
	}

	if(m_Quantity<30) {
		SetGear1FailFlag(1);
		SetGear1FailFlag(1);
		SetGear1FailFlag(1);
		SetHydraulicSystemIntegrity(0);
		SetBrakingCf(0);
		SetLandinGearHydraulicPressure(0);
		POS_SET(POS_MAIN_HYDRO_WORK,0);
		m_Played=false;
	} else {
		SetGear1FailFlag(0);
		SetGear1FailFlag(0);
		SetGear1FailFlag(0);
		SetHydraulicSystemIntegrity(1);
		SetBrakingCf(m_BrakeCoeffDefault);
		AnimateGearSwitch();
		POS_SET(POS_MAIN_HYDRO_WORK,1);
	}

	if((m_Quantity<30||GetHydraulicSystemIntegrity()==0)&&!GetParkingBrake()) {
		SetBrakingCf(0);
		m_BrakeLeftQuantity=0;
		m_BrakeRightQuantity=0;
	}

	if(GetHydraulicSystemIntegrity()==1&&GetLeftBrakePos()>0.2&&GetParkingBrake()==0) {
		SetBrakingCf(m_BrakeCoeffDefault);
		m_BrakeLeftQuantity=GetLeftBrakePos()*100;
		m_Quantity=m_Quantity-(m_Quantity/100*(m_BrakeLeftQuantity/6.6));
	} else {
		m_BrakeLeftQuantity=0;
	}

	if(GetHydraulicSystemIntegrity()==1&&GetRightBrakePos()>0.2&&GetParkingBrake()==0) {
		SetBrakingCf(m_BrakeCoeffDefault);
		m_BrakeRightQuantity=GetRightBrakePos()*100;
		m_Quantity=m_Quantity-(m_Quantity/100*(m_BrakeRightQuantity/6.6));
	} else {
		m_BrakeRightQuantity=0;
	}

	if(m_Gear->GetState(0)!=MOVEMENT_NONE) {
		m_Quantity=m_Quantity-(m_Quantity/100*15);
	}
	if(m_Gear->GetState(0)!=MOVEMENT_NONE) {
		m_Quantity=m_Quantity-(m_Quantity/100*15);
	}
	if(m_Gear->GetState(0)!=MOVEMENT_NONE) {
		m_Quantity=m_Quantity-(m_Quantity/100*15);
	}

	if(m_Flaps->GetLeftState()!=FLAPS_STATE_NOT_MOVING&&!POS_GET(POS_AUXL_FLAPS_WORK)) {
		m_Quantity=m_Quantity-(m_Quantity/100*15);
	}
	if(m_Flaps->GetRightState()!=FLAPS_STATE_NOT_MOVING&&!POS_GET(POS_AUXL_FLAPS_WORK)) {
		m_Quantity=m_Quantity-(m_Quantity/100*15);
	}

	if(m_Htail->IsWorking()) {
		m_Quantity=m_Quantity-(m_Quantity/100*10);
	}

	if(m_RTU->IsWorking()) {
		m_Quantity=m_Quantity-(m_Quantity/100*10);
	}

	NDL_SET(NDL_MAIN_HYD,m_Quantity);
	NDL_SET(NDL_MAIN_BRAKE_HYD_LEFT,m_BrakeLeftQuantity);
	NDL_SET(NDL_MAIN_BRAKE_HYD_RIGHT,m_BrakeRightQuantity);
}

//////////////////////////////////////////////////////////////////////////

void SDHydroSystemEmer::Update()
{
	int hydpump=AZS_GET(AZS_HYD_PUMP);
	int gearem=AZS_GET(AZS_EMERG_GEAR);
	int flapsem=AZS_GET(AZS_EMERG_FLAPS);
	int emergst=(BTN_GET(BTN_TRIMEMERG2)==1||BTN_GET(BTN_TRIMEMERG2)==2||BTN_GET(BTN_TRIMEMERG3)==1||BTN_GET(BTN_TRIMEMERG3)==2);

//	if ((((hydpump==0||hydpump==1) && ((gearem==1||gearem==2) || (flapsem==1||flapsem==2) || emergst))||hydpump==2) && PWR_GET(PWR_BUS27)) {
		m_Available=true;
		//m_Quantity=150;
		m_Quantity+=1.0;
		if(m_Quantity>150)m_Quantity=150;
/*
	} else {
		m_Quantity-=1.0;
		if(m_Quantity<-10)m_Quantity=-10;
		m_Available=false;
		//m_Quantity=-10;
	}
*/

//	int flapsm=AZS_GET(AZS_EMERG_FLAPS);

/*
	if(flapsm==2) {
		if(m_Quantity>145) {
			m_Flaps->ReturnFlapTab(2);
			POS_SET(POS_AUXL_FLAPS_WORK,1);
		} else {
			m_Flaps->CalculateFlapTab();
		}
	} else {
		POS_SET(POS_AUXL_FLAPS_WORK,0);
	}
*/

	double prk=0;
	if(g_pSDAPI->GetN(PARKING_BRAKE_POS)) {
		prk=50;
		m_BrakeLeftQuantity=100;
		m_BrakeRightQuantity=100;
	} else {
		m_BrakeLeftQuantity=0;
		m_BrakeRightQuantity=0;
	}

	double gear=0;
	if(gearem==2||gearem==3) {
		if(m_Gear->GetPos(0)<16000||m_Gear->GetPos(1)<16000||m_Gear->GetPos(2)<16000) {
			ImportTable.pPanels->send_key_event(KEY_GEAR_DOWN,0);
			gear=30;
		}
	}

	if(m_Htail->IsWorkingEmerg()&&m_Quantity>40) {
		NDL_SET(NDL_EMERG_HYD,135-prk-gear);
	} else if((m_Flaps->GetLeftState()!=FLAPS_STATE_NOT_MOVING&&m_Flaps->GetRightState()!=FLAPS_STATE_NOT_MOVING)&&POS_GET(POS_AUXL_FLAPS_WORK)) {
		NDL_SET(NDL_EMERG_HYD,110-prk-gear);
	} else if((m_Flaps->GetLeftState()!=FLAPS_STATE_NOT_MOVING&&m_Flaps->GetRightState()==FLAPS_STATE_NOT_MOVING)&&POS_GET(POS_AUXL_FLAPS_WORK)) {
		NDL_SET(NDL_EMERG_HYD,130-prk-gear);
	} else if((m_Flaps->GetLeftState()==FLAPS_STATE_NOT_MOVING&&m_Flaps->GetRightState()!=FLAPS_STATE_NOT_MOVING)&&POS_GET(POS_AUXL_FLAPS_WORK)) {
		NDL_SET(NDL_EMERG_HYD,130-prk-gear);
	} else {
		NDL_SET(NDL_EMERG_HYD,m_Quantity-prk-gear);
	}

	if(m_Quantity<110) {
		LMP_SET(LMP_ZAR_AVAR_TORM,1);
	} else {
		LMP_SET(LMP_ZAR_AVAR_TORM,0);
	}

	if(m_Quantity<100&&g_pSDAPI->GetN(PARKING_BRAKE_POS)) {
		ImportTable.pPanels->trigger_key_event(KEY_PARKING_BRAKES,0);
		m_BrakeLeftQuantity=0;
		m_BrakeRightQuantity=0;
	}

	NDL_SET(NDL_EMERG_BRAKE_HYD_LEFT,m_BrakeLeftQuantity);
	NDL_SET(NDL_EMERG_BRAKE_HYD_RIGHT,m_BrakeRightQuantity);

}

//////////////////////////////////////////////////////////////////////////

SDSystemHydro::SDSystemHydro()
{
}

SDSystemHydro::~SDSystemHydro()
{
}

void SDSystemHydro::Init()
{
	xml11=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_514",ImportTable.pPanels));
	xml12=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_515",ImportTable.pPanels));
	xml21=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_516",ImportTable.pPanels));
	xml22=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_517",ImportTable.pPanels));
	xml31=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_518",ImportTable.pPanels));
	xml32=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_519",ImportTable.pPanels));

	VCAnimHydPri			=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_hyd_main",ImportTable.pPanels));
	VCAnimHydEmerg			=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_hyd_auxl",ImportTable.pPanels));
	VCAnimBrakesPriLeft		=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_brake_hyd_main_left",ImportTable.pPanels));
	VCAnimBrakesPriRight	=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_brake_hyd_main_right",ImportTable.pPanels));
	VCAnimBrakesEmergLeft	=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_brake_hyd_auxl_left",ImportTable.pPanels));
	VCAnimBrakesEmergRight	=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_brake_hyd_auxl_right",ImportTable.pPanels));

	m_BrakeCoeffDefault		=GetBrakingCf();

}

void SDSystemHydro::Update()
{
	SDHydroSystemMain::Update();
	SDHydroSystemEmer::Update();

	xml11->set_number(NDL_GET(NDL_MAIN_BRAKE_HYD_LEFT));
	xml12->set_number(NDL_GET(NDL_MAIN_BRAKE_HYD_RIGHT));
	xml21->set_number(NDL_GET(NDL_EMERG_BRAKE_HYD_LEFT));
	xml22->set_number(NDL_GET(NDL_EMERG_BRAKE_HYD_RIGHT));
	xml31->set_number(NDL_GET(NDL_MAIN_HYD));
	xml32->set_number(NDL_GET(NDL_EMERG_HYD));

	VCAnimBrakesPriLeft->set_degree(dgrd(NDL_GET(NDL_MAIN_BRAKE_HYD_LEFT)*-0.92));
	VCAnimBrakesPriRight->set_degree(dgrd(NDL_GET(NDL_MAIN_BRAKE_HYD_RIGHT)*0.92));
	VCAnimBrakesEmergLeft->set_degree(dgrd(NDL_GET(NDL_EMERG_BRAKE_HYD_LEFT)*-0.92));
	VCAnimBrakesEmergRight->set_degree(dgrd(NDL_GET(NDL_EMERG_BRAKE_HYD_RIGHT)*0.92));
	VCAnimHydPri->set_degree(dgrd(NDL_GET(NDL_MAIN_HYD)*-0.92));
	VCAnimHydEmerg->set_degree(dgrd(NDL_GET(NDL_EMERG_HYD)*0.92));
}

void SDSystemHydro::Load(CIniFile *ini)
{
}

void SDSystemHydro::Save(CIniFile *ini)
{
}

void SDHydroSystemMain::AnimateGearSwitch()
{
	static int gds=0;
	static int gus=0;
	static int counter2=0;

	//m_Gear->Update();

	if(m_Gear->GetState(0)==MOVEMENT_EXTENDING) {
		if(counter2++>2) {
			switch(gds) {
				case 0:
					AZS_SET(AZS_GEAR,0);
					gds++;
					break;
				case 1:
					AZS_SET(AZS_GEAR,1);
					gds++;
					break;
				case 2:
					AZS_SET(AZS_GEAR,2);
					gds++;
					break;
			}
			counter2=0;
		}
		/*
		TBG_SET(PA,TBG_GEAR1_UP,0);
		TBG_SET(PA,TBG_GEAR2_UP,0);
		TBG_SET(PA,TBG_GEAR3_UP,0);
		TBG_SET(PA,TBG_GEAR1_DOWN,1);
		TBG_SET(PA,TBG_GEAR2_DOWN,1);
		TBG_SET(PA,TBG_GEAR3_DOWN,1);
		*/
	} else if(m_Gear->GetState(0)==MOVEMENT_RETRACTING) {
		if(counter2++>2) {
			switch(gus) {
				case 0:
					AZS_SET(AZS_GEAR,0);
					gus++;
					break;
				case 1:
					AZS_SET(AZS_GEAR,3);
					gus++;
					break;
				case 2:
					AZS_SET(AZS_GEAR,4);
					gus++;
					break;
			}
			counter2=0;
		}
		/*
		TBG_SET(PA,TBG_GEAR1_UP,1);
		TBG_SET(PA,TBG_GEAR2_UP,1);
		TBG_SET(PA,TBG_GEAR3_UP,1);
		TBG_SET(PA,TBG_GEAR1_DOWN,0);
		TBG_SET(PA,TBG_GEAR2_DOWN,0);
		TBG_SET(PA,TBG_GEAR3_DOWN,0);
		*/
	} else {
		if(counter2++>2) {
			switch((int)AZS_GET(AZS_GEAR)) {
				case 4:
				case 2:
					if(gds!=0) {
						AZS_SET(AZS_GEAR,1);
						gds=1;
					}
					if(gus!=0) {
						AZS_SET(AZS_GEAR,3);
						gus=1;
					}
					break;
				case 3:
				case 1:
					AZS_SET(AZS_GEAR,0);
					gds=0;
					gus=0;
					break;
			}
			counter2=0;
		}
		/*
		if(!BTN_GET(P0,BTN_GEAR_LIGHTS_TEST)) {
		TBG_SET(PA,TBG_GEAR1_UP,0);
		TBG_SET(PA,TBG_GEAR2_UP,0);
		TBG_SET(PA,TBG_GEAR3_UP,0);
		TBG_SET(PA,TBG_GEAR1_DOWN,0);
		TBG_SET(PA,TBG_GEAR2_DOWN,0);
		TBG_SET(PA,TBG_GEAR3_DOWN,0);
		}
		*/
	}
}
