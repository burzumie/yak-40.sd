/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/MiscSystem.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "MiscSystem.h"

#define PRKBRK_ZERO	0
#define PRKBRK_FULL 14

#define STOPOR_ZERO 0
#define STOPOR_FULL 9

#define GEAR_OFF	0
#define GEAR_UP1	1
#define GEAR_UP2	2
#define GEAR_DN1	3
#define GEAR_DN2	4

void SDSystemMisc::Init() 
{
	m_Beacon=0;
	m_Brkpos[0]=m_Brkpos[1]=0;
	m_Stopor[0]=m_Stopor[1]=0;
	if(g_pSDAPI->GetN(PARKING_BRAKE_POS)) {
		POS_SET(POS_PRKBRK,PRKBRK_FULL);
	} else {
		POS_SET(POS_PRKBRK,PRKBRK_ZERO);
	}
	m_Gear=CGear::Instance();
	m_RTU=CRTU::Instance();
	VCAnimStairs=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_stairs",ImportTable.pPanels,false));
	VCAnimFrt=std::auto_ptr<CNamedVar>(new CNamedVar("SD6015 anmfrt",ImportTable.pPanels,false));

	VCAnimKnb02=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_knb_DH",ImportTable.pPanels,false));
	VCAnimKnb04=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_knb_AGB1Pitch",ImportTable.pPanels,false));
	VCAnimKnb10=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_knb_AGB2Pitch",ImportTable.pPanels,false));
	VCAnimKnb12=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_knb_AGB3Pitch",ImportTable.pPanels,false));

	VCAnimKnb03=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_knb_AGB1Ar",ImportTable.pPanels,false));
	VCAnimKnb09=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_knb_AGB2Ar",ImportTable.pPanels,false));
	VCAnimKnb11=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_knb_AGB3Ar",ImportTable.pPanels,false));
	VCAnimKnb05=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_knb_UVID",ImportTable.pPanels,false));
	VCAnimKnb13=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_knb_VD10",ImportTable.pPanels,false));

	VCAnimBeacon=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_beacon",ImportTable.pPanels,false));
		
	VCAnimLLightTaxi =std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_llight_taxi",ImportTable.pPanels,false));
	VCAnimRLightTaxi =std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_rlight_taxi",ImportTable.pPanels,false));
	VCAnimLLightLand =std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_llight_land",ImportTable.pPanels,false));
	VCAnimRLightLand =std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_rlight_land",ImportTable.pPanels,false));
	VCAnimLightExtend=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_light_extend",ImportTable.pPanels,false));

	PSIM_DATA p=ImportTable.pSIM1->sim1_get_UserSimData();

    memcpy(&m_OldSmoke[0].xyz,&p->pStaticData->pSmokePointList->xyz,sizeof(XYZF64));
	memcpy(&m_OldSmoke[1].xyz,&p->pStaticData->pSmokePointList->pNext->xyz,sizeof(XYZF64));
	memcpy(&m_OldSmoke[2].xyz,&p->pStaticData->pSmokePointList->pNext->pNext->xyz,sizeof(XYZF64));

	memcpy(&m_OldSmoke[3].xyz,&p->pStaticData->pSmokePointList->xyz,sizeof(XYZF64));

	m_OldSmoke[3].xyz.alt=999999;
	m_OldSmoke[3].xyz.bank=999999;
	m_OldSmoke[3].xyz.heading=999999;
	m_OldSmoke[3].xyz.lat=999999;
	m_OldSmoke[3].xyz.lon=999999;
	m_OldSmoke[3].xyz.pitch=999999;
	m_OldSmoke[3].xyz.x=999999;
	m_OldSmoke[3].xyz.y=999999;
	m_OldSmoke[3].xyz.z=999999;

	m_Speed[0]=m_Speed[1]=0;

	m_FrtPlayed=false;
	m_BrakeCheckedSound=false;
	m_ArksOnSound=false;
	m_Alt0SetSound=false;
};

void SDSystemMisc::Update()
{
	if(AZS_CHG(AZS_MANOM)) {
		if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_MANOM))
			SND_SET(SND_MANOMETERS_ON,1);
	}

	if(KEY_GET(KEY_AILERON_CENTER_TRIM)) {
		SetAileronTrimPos(0);
		KEY_SET(KEY_AILERON_CENTER_TRIM,0);
	}

	if(KEY_GET(KEY_RUDDER_CENTER_TRIM)) {
		SetRudderTrimPos(0);
		KEY_SET(KEY_RUDDER_CENTER_TRIM,0);
	}

	if(g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)&&NDL_GET(NDL_KUS_IAS_EN)>15&&POS_GET(POS_BRAKE_TRIGGER)&&!m_BrakeCheckedSound) {
		SND_SET(SND_BRAKES_CHECKED,1);
		m_BrakeCheckedSound=true;
	}

	if(PWR_GET(PWR_ADF1)&&PWR_GET(PWR_ADF2)&&AZS_GET(AZS_ARK1LEFTRIGHT)&&!AZS_GET(AZS_ARK2LEFTRIGHT)&&!m_ArksOnSound) {
		SND_SET(SND_ARK_ON,1);
		m_ArksOnSound=true;
	}

	if(!PWR_GET(PWR_ADF1)&&!PWR_GET(PWR_ADF2)) {
		m_ArksOnSound=false;
	}

	double a0=NDL_GET(NDL_UVID0_HIDE);
	double a1=NDL_GET(NDL_UVID1);
	double dh=NDL_GET(NDL_RV3M_DH);
	double p115=PWR_GET(PWR_BUS115);
	double p27=PWR_GET(PWR_BUS27);
	double arv=AZS_GET(AZS_RADALT);

	if(a0>-15&&a0<15&&a1>-15&&a1<15&&p115&&arv&&p27&&dh>95&&dh<105&&!m_Alt0SetSound) {
		SND_SET(SND_RV3M_DH100,1);
		m_Alt0SetSound=true;
	}

	CheckPanelState();
	CheckAllPower();
	MakeAnimation();
	ProcessTablo();
	UpdateAZSVC();
	Smoke();
	Stairs();
	VCKrm();
	BSPK();
	Beacons();
	SoundSpeed();
	Lights();
	SoundAlt();
}

void SDSystemMisc::CheckPanelState()
{	
	if(AZS_GET(AZS_WHITE_DECK_FLOOD_LIGHT)&&(AZS_GET(AZS_RED_PANEL_LIGHT)||AZS_GET(AZS_REDLIGHT1))&&PWR_GET(PWR_BUS27)) {
		if(GetTimeOfDay()!=TIME_OF_DAY_DAY)
			POS_SET(POS_PANEL_STATE,PANEL_LIGHT_MIX);
		else
			POS_SET(POS_PANEL_STATE,PANEL_LIGHT_DAY);
		if(!g_pSDAPI->GetN(LOGO_LIGHTS))
			ImportTable.pPanels->trigger_key_event(KEY_TOGGLE_LOGO_LIGHTS,0);
		if(!g_pSDAPI->GetN(WING_LIGHTS))
			ImportTable.pPanels->trigger_key_event(KEY_TOGGLE_WING_LIGHTS,0);
	} else if(!AZS_GET(AZS_WHITE_DECK_FLOOD_LIGHT)&&(AZS_GET(AZS_RED_PANEL_LIGHT)||AZS_GET(AZS_REDLIGHT1))&&PWR_GET(PWR_BUS27)) {
		if(GetTimeOfDay()!=TIME_OF_DAY_DAY)
			POS_SET(POS_PANEL_STATE,PANEL_LIGHT_RED);
		else
			POS_SET(POS_PANEL_STATE,PANEL_LIGHT_DAY);
		if(!g_pSDAPI->GetN(WING_LIGHTS))
			ImportTable.pPanels->trigger_key_event(KEY_TOGGLE_WING_LIGHTS,0);
		if(g_pSDAPI->GetN(LOGO_LIGHTS)) 
			ImportTable.pPanels->trigger_key_event(KEY_TOGGLE_LOGO_LIGHTS,0);
	} else if(AZS_GET(AZS_WHITE_DECK_FLOOD_LIGHT)&&(!AZS_GET(AZS_RED_PANEL_LIGHT)||!AZS_GET(AZS_REDLIGHT1))&&PWR_GET(PWR_BUS27)) {
		if(GetTimeOfDay()!=TIME_OF_DAY_DAY)
			POS_SET(POS_PANEL_STATE,PANEL_LIGHT_PLF);
		else
			POS_SET(POS_PANEL_STATE,PANEL_LIGHT_DAY);
		if(!g_pSDAPI->GetN(LOGO_LIGHTS))
			ImportTable.pPanels->trigger_key_event(KEY_TOGGLE_LOGO_LIGHTS,0);
		if(g_pSDAPI->GetN(WING_LIGHTS)) 
			ImportTable.pPanels->trigger_key_event(KEY_TOGGLE_WING_LIGHTS,0);
	} else {
		POS_SET(POS_PANEL_STATE,PANEL_LIGHT_DAY);
		if(g_pSDAPI->GetN(LOGO_LIGHTS)) 
			ImportTable.pPanels->trigger_key_event(KEY_TOGGLE_LOGO_LIGHTS,0);
		if(g_pSDAPI->GetN(WING_LIGHTS)) 
			ImportTable.pPanels->trigger_key_event(KEY_TOGGLE_WING_LIGHTS,0);
	}

}

void SDSystemMisc::MakeAnimation()
{
	AnimateStopor();
	AnimateParkingBrake();
	//AnimateGearSwitch();

	if(!VCAnimFrt->get_number()) {
		if(!m_FrtPlayed) {
			SND_SET(SND_WINDOW_CLOSED,1);
			m_FrtPlayed=true;
		}
	} else {
		m_FrtPlayed=false;
	}
}

void SDSystemMisc::AnimateStopor()
{
	static bool stoporanimstart=false;
	static bool stoporwason=false;
	static int counter3=0;

	int p4_pos_stopor=(int)POS_GET(POS_STOPOR);
	m_Stopor[0]=(int)POS_GET(POS_STOPOR_TRIGGER);

	if(m_Stopor[1]!=m_Stopor[0]) {
		if(m_Stopor[1]==false)stoporwason=false;
		else stoporwason=true;
		stoporanimstart=true;
		m_Stopor[1]=m_Stopor[0];
	}

	if(stoporanimstart) {
		//if(counter3++>1) {
		if(stoporwason) {
			p4_pos_stopor--;
			if(p4_pos_stopor<0) {
				p4_pos_stopor=0;
				stoporanimstart=false;
			}
		} else {
			p4_pos_stopor++;
			if(p4_pos_stopor==1) {
				m_FixThrottlePos[0]=g_pSDAPI->GetN(GENERAL_ENGINE1_THROTTLE_LEVER_POS);
				m_FixThrottlePos[1]=g_pSDAPI->GetN(GENERAL_ENGINE2_THROTTLE_LEVER_POS);
				m_FixThrottlePos[2]=g_pSDAPI->GetN(GENERAL_ENGINE3_THROTTLE_LEVER_POS);
			}
			if(p4_pos_stopor>9) {
				stoporanimstart=false;
				p4_pos_stopor=9;
			}
		}
		counter3=0;
		//}
	}

	if(p4_pos_stopor>0&&!stoporanimstart) {
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE1_SET,(UINT32)(16383*m_FixThrottlePos[0]));
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE2_SET,(UINT32)(16383*m_FixThrottlePos[1]));
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE3_SET,(UINT32)(16383*m_FixThrottlePos[2]));
	}

	POS_SET(POS_STOPOR,p4_pos_stopor);
}

void SDSystemMisc::AnimateParkingBrake()
{
	static bool animstart=false;
	static bool wason=false;

	m_Brkpos[0]=g_pSDAPI->GetN(PARKING_BRAKE_POS)?1:0;
	if(m_Brkpos[0]!=m_Brkpos[1]) {
		if(m_Brkpos[1]==0)wason=false;
		else wason=true;
		animstart=true;
		m_Brkpos[1]=m_Brkpos[0];
	} 

	if(animstart) {
		if(wason) {
			POS_DEC(POS_PRKBRK);
			if(POS_GET(POS_PRKBRK)<PRKBRK_ZERO) {
				POS_SET(POS_PRKBRK,PRKBRK_ZERO);
				animstart=false;
			}
		} else {
			POS_INC(POS_PRKBRK);
			if(POS_GET(POS_PRKBRK)>PRKBRK_FULL) {
				POS_SET(POS_PRKBRK,PRKBRK_FULL);
				SND_SET(SND_PARKING_BRAKE,1);
				animstart=false;
			}
		}
	}

}

void SDSystemMisc::AnimateGearSwitch()
{
	static int gds=0;
	static int gus=0;
	static int counter2=0;

	//m_Gear->Update();

	if(m_Gear->GetState(0)==MOVEMENT_EXTENDING) {
		if(counter2++>2) {
			switch(gds) {
				case 0:
					AZS_SET(AZS_GEAR,0);
					gds++;
					break;
				case 1:
					AZS_SET(AZS_GEAR,1);
					gds++;
					break;
				case 2:
					AZS_SET(AZS_GEAR,2);
					gds++;
					break;
			}
			counter2=0;
		}
/*
		TBG_SET(PA,TBG_GEAR1_UP,0);
		TBG_SET(PA,TBG_GEAR2_UP,0);
		TBG_SET(PA,TBG_GEAR3_UP,0);
		TBG_SET(PA,TBG_GEAR1_DOWN,1);
		TBG_SET(PA,TBG_GEAR2_DOWN,1);
		TBG_SET(PA,TBG_GEAR3_DOWN,1);
*/
	} else if(m_Gear->GetState(0)==MOVEMENT_RETRACTING) {
		if(counter2++>2) {
			switch(gus) {
				case 0:
					AZS_SET(AZS_GEAR,0);
					gus++;
					break;
				case 1:
					AZS_SET(AZS_GEAR,3);
					gus++;
					break;
				case 2:
					AZS_SET(AZS_GEAR,4);
					gus++;
					break;
			}
			counter2=0;
		}
/*
		TBG_SET(PA,TBG_GEAR1_UP,1);
		TBG_SET(PA,TBG_GEAR2_UP,1);
		TBG_SET(PA,TBG_GEAR3_UP,1);
		TBG_SET(PA,TBG_GEAR1_DOWN,0);
		TBG_SET(PA,TBG_GEAR2_DOWN,0);
		TBG_SET(PA,TBG_GEAR3_DOWN,0);
*/
	} else {
		if(counter2++>2) {
			switch((int)AZS_GET(AZS_GEAR)) {
				case 4:
				case 2:
					if(gds!=0) {
						AZS_SET(AZS_GEAR,1);
						gds=1;
					}
					if(gus!=0) {
						AZS_SET(AZS_GEAR,3);
						gus=1;
					}
					break;
				case 3:
				case 1:
					AZS_SET(AZS_GEAR,0);
					gds=0;
					gus=0;
					break;
			}
			counter2=0;
		}
/*
		if(!BTN_GET(P0,BTN_GEAR_LIGHTS_TEST)) {
			TBG_SET(PA,TBG_GEAR1_UP,0);
			TBG_SET(PA,TBG_GEAR2_UP,0);
			TBG_SET(PA,TBG_GEAR3_UP,0);
			TBG_SET(PA,TBG_GEAR1_DOWN,0);
			TBG_SET(PA,TBG_GEAR2_DOWN,0);
			TBG_SET(PA,TBG_GEAR3_DOWN,0);
		}
*/
	}
}

void SDSystemMisc::CheckAllPower()
{
	// SO72
	if(AZS_GET(AZS_XPDR_BUS)&&AZS_GET(AZS_SO72PWR)&&PWR_GET(PWR_BUS27)) {
		PWR_SET(PWR_SO72,1);
	} else {
		PWR_SET(PWR_SO72,0);
	}

	// ������ 1
	if(AZS_GET(AZS_KMP1PWR)&&PWR_GET(PWR_BUS27)) {
		PWR_SET(PWR_KURSMP1,1);
	} else {
		PWR_SET(PWR_KURSMP1,0);
	}

	// ������ 2
	if(AZS_GET(AZS_KMP2PWR)&&PWR_GET(PWR_BUS27)) {
		PWR_SET(PWR_KURSMP2,1);
	} else {
		PWR_SET(PWR_KURSMP2,0);
	}

	// ADF1
	if(AZS_GET(AZS_ADF1_BUS)&&PWR_GET(PWR_BUS115)&&GLT_GET(GLT_ARK1)>0&&PWR_GET(PWR_BUS27)) {
		PWR_SET(PWR_ADF1,1);
	} else {
		PWR_SET(PWR_ADF1,0);
	}

	// ADF2
	if(AZS_GET(AZS_ADF2_BUS)&&PWR_GET(PWR_BUS115)&&GLT_GET(GLT_ARK2)>0&&PWR_GET(PWR_BUS27)) {
		PWR_SET(PWR_ADF2,1);
	} else {
		PWR_SET(PWR_ADF2,0);
	}

	// COM1
	if(AZS_GET(AZS_COM1_BUS)&&PWR_GET(PWR_BUS115)&&PWR_GET(PWR_BUS27)) {
		PWR_SET(PWR_COM1,1);
	} else {
		PWR_SET(PWR_COM1,0);
	}

	// COM2
	if(AZS_GET(AZS_COM2_BUS)&&PWR_GET(PWR_BUS115)&&PWR_GET(PWR_BUS27)) {
		PWR_SET(PWR_COM2,1);
	} else {
		PWR_SET(PWR_COM2,0);
	}

	// ���������
	if(AZS_GET(AZS_AUTOPILOT_BUS)&&PWR_GET(PWR_BUS36)&&PWR_GET(PWR_BUS27)) {
		PWR_SET(PWR_AP,1);
	} else {
		PWR_SET(PWR_AP,0);
	}

	// ���
	PWR_SET(PWR_GMK,(PWR_GET(PWR_BUS36)&&PWR_GET(PWR_BUS27)&&AZS_GET(AZS_GMK_COURSE_SYS_BUS)));

}

void SDSystemMisc::ProcessTablo()
{
	double e1=g_pSDAPI->GetN(TURB_ENGINE_1_CORRECTED_N2);
	double e2=g_pSDAPI->GetN(TURB_ENGINE_2_CORRECTED_N2);
	double e3=g_pSDAPI->GetN(TURB_ENGINE_3_CORRECTED_N2);
	double vs=NDL_GET(NDL_VS);
	double rh=g_pSDAPI->GetN(RADIO_HEIGHT);
	double g1=g_pSDAPI->GetN(GEAR_POS_LEFT);
	double g2=g_pSDAPI->GetN(GEAR_POS_NOSE);
	double g3=g_pSDAPI->GetN(GEAR_POS_RIGHT);
	double si=NDL_GET(NDL_KUS_IAS);
	double bn=NDL_GET(NDL_AGB0_MAIN_PLANE);
	double bn3=NDL_GET(NDL_AGB1_MAIN_PLANE);
	double rb=360-(bn*-1);
	double rb3=360-(bn3*-1);
	double lk=LimitValue(g_pSDAPI->GetXML(XML_FUEL_TANK_LEFT_MAIN_QUANTITY,"liters")*.8,0.,4000.);
	double rk=LimitValue(g_pSDAPI->GetXML(XML_FUEL_TANK_RIGHT_MAIN_QUANTITY,"liters")*.8,0.,4000.);
	double egt1=(g_pSDAPI->GetN(GENERAL_ENGINE1_EGT)-459.67-32)*5/9+((-273-g_pSDAPI->GetN(AMBIENT_TEMP_DEGREES_C))*0.3);
	double egt2=(g_pSDAPI->GetN(GENERAL_ENGINE1_EGT)-459.67-32)*5/9+((-273-g_pSDAPI->GetN(AMBIENT_TEMP_DEGREES_C))*0.3);
	double egt3=(g_pSDAPI->GetN(GENERAL_ENGINE1_EGT)-459.67-32)*5/9+((-273-g_pSDAPI->GetN(AMBIENT_TEMP_DEGREES_C))*0.3);
	double e1f=g_pSDAPI->GetN(ENGINE1_ON_FIRE);
	double e2f=g_pSDAPI->GetN(ENGINE2_ON_FIRE);
	double e3f=g_pSDAPI->GetN(ENGINE3_ON_FIRE);
	int e1fb=(int)AZS_GET(AZS_ENG1_FIRE_ALARM_BUS);
	int e2fb=(int)AZS_GET(AZS_ENG2_FIRE_ALARM_BUS);
	int e3fb=(int)AZS_GET(AZS_ENG3_FIRE_ALARM_BUS);
	double ta=rddg(g_pSDAPI->GetN(AILERON_TRIM));
	double tr=rddg(g_pSDAPI->GetN(RUDDER_TRIM));
	double flaps=g_pSDAPI->GetN(TRAILING_EDGE_FLAPS0_LEFT_ANGLE);

	double aaa=g_pSDAPI->GetN(TURB_ENGINE_1_VIBRATION);

	TBL_SET(TBL_ASPD_LOW,			g_pSDAPI->GetN(STALL_WARNING)&&AZS_GET(AZS_STALL_WARN_BUS));
	TBL_SET(TBL_EXIT_OPEN,			(int)g_pSDAPI->GetN(MAIN_EXIT_OPEN));
	TBL_SET(TBL_WARN_GEAR,			((e1<92&&e2<92&&e3<92)&&vs<-2&&rh<600&&(g1<16383||g2<16383||g3<16383))&&flaps);
	TBG_SET(TBG_WARN_GEAR,			TBL_GET(TBL_WARN_GEAR));
	TBL_SET(TBL_BANK_L_HIGH,		si>230&&(bn<-32&&bn>-180)?1:si<230&&(bn<-15&&bn>-180)?1:0);
	TBL_SET(TBL_BANK_R_HIGH,		si>230&&(rb>32&&rb<180)?1:si<230&&(rb>15&&rb<180)?1:0);
	TBL_SET(TBL_BANK3_L_HIGH,		si>230&&(bn3<-32&&bn3>-180)?1:si<230&&(bn3<-15&&bn3>-180)?1:0);
	TBL_SET(TBL_BANK3_R_HIGH,		si>230&&(rb3>32&&rb3<180)?1:si<230&&(rb3>15&&rb3<180)?1:0);
	TBL_SET(TBL_MARKER,				(g_pSDAPI->GetN(MARKER_BEACON_STATE)&&!AZS_GET(AZS_KMP_RCVR)));
	TBL_SET(TBL_OIL_PRESS_LOW,		(int)g_pSDAPI->GetN(WARNING_OIL_PSI));
	TBL_SET(TBL_FUEL_LOW_L,			lk<250);
	TBL_SET(TBL_FUEL_LOW_R,			rk<250);
	TBL_SET(TBL_ENG1_VIB_HIGH,		(g_pSDAPI->GetN(TURB_ENGINE_1_VIBRATION)>2.80)||(NDL_GET(NDL_UVID0)>4000&&g_pSDAPI->GetN(GENERAL_ENGINE1_THROTTLE_LEVER_POS)>=0.99));
	TBL_SET(TBL_ENG2_VIB_HIGH,		(g_pSDAPI->GetN(TURB_ENGINE_2_VIBRATION)>2.80)||(NDL_GET(NDL_UVID0)>4000&&g_pSDAPI->GetN(GENERAL_ENGINE2_THROTTLE_LEVER_POS)>=0.99)||(m_RTU->IsWorking()&&si<50));
	TBL_SET(TBL_ENG3_VIB_HIGH,		(g_pSDAPI->GetN(TURB_ENGINE_3_VIBRATION)>2.80)||(NDL_GET(NDL_UVID0)>4000&&g_pSDAPI->GetN(GENERAL_ENGINE3_THROTTLE_LEVER_POS)>=0.99));
	TBL_SET(TBL_STALL,				g_pSDAPI->GetN(STALL_WARNING)&&AZS_GET(AZS_STALL_WARN_BUS));
	TBL_SET(TBL_OUTER_MARKER,		(g_pSDAPI->GetN(MARKER_BEACON_STATE)==1&&!AZS_GET(AZS_KMP_RCVR)));
	TBL_SET(TBL_MIDDLE_MARKER,		(g_pSDAPI->GetN(MARKER_BEACON_STATE)==2&&!AZS_GET(AZS_KMP_RCVR)));
	TBL_SET(TBL_INNER_MARKER,		(g_pSDAPI->GetN(MARKER_BEACON_STATE)==3&&!AZS_GET(AZS_KMP_RCVR)));
	TBL_SET(TBL_APU_FUEL_VALVE_OPEN,AZS_GET(AZS_APU_FUEL_VALVE));
	TBL_SET(TBL_HYD_PUMP_ENG1_FAIL,	g_pSDAPI->GetN(HYDRAULICS1_PRESSURE_PSF)/2048<30||!GetHydraulicSystemIntegrity());
	TBL_SET(TBL_HYD_PUMP_ENG2_FAIL,	g_pSDAPI->GetN(HYDRAULICS2_PRESSURE_PSF)/2048<30||!GetHydraulicSystemIntegrity());
	TBL_SET(TBL_ENG_OVERHEAT,		(egt1>750||egt2>750||egt3>750));
	TBL_SET(TBL_FIRE,				((e1f&&e1fb)||(e2f&&e2fb)||(e3f&&e3fb)||TBL_GET(TBL_APU_ON_FIRE)));
	TBG_SET(TBG_WARN_FLAP,			((e1>95&&e2>95&&e3>95)&&g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)&&(g_pSDAPI->GetN(TRAILING_EDGE_FLAPS0_LEFT_ANGLE)<=19||g_pSDAPI->GetN(TRAILING_EDGE_FLAPS0_LEFT_ANGLE)>23)));

	LMP_SET(LMP_FIRE_EXTING1_RDY,	AZS_GET(AZS_FIRE_EXT_1_BUS));
	LMP_SET(LMP_FIRE_EXTING2_RDY,	AZS_GET(AZS_FIRE_EXT_2_BUS));
	LMP_SET(LMP_FIRE_EXTING3_RDY,	AZS_GET(AZS_FIRE_EXT_3_BUS));
	LMP_SET(LMP_FIRE_EXTING4_RDY,	AZS_GET(AZS_APU_IGNIT_BUS));
	LMP_SET(LMP_FIRE_SND_ALARM_OFF,	!AZS_GET(AZS_FIRE_SOUND_ALARM));
	LMP_SET(LMP_TRIM_ELERON,		(ta>-0.1&&ta<0.1));
	LMP_SET(LMP_TRIM_RUDDER,		(tr>-0.1&&tr<0.1));
	//LMP_SET(LMP_TOPL_LEV,			AZS_GET(AZS_TOPL_LEV));
	//LMP_SET(LMP_TOPL_PRAV,			AZS_GET(AZS_TOPL_PRAV));

	if(
		(
		  (
		    NDL_GET(NDL_MAIN_HYD)>30&&AZS_GET(AZS_GEAR_MAIN_BUS)
		  ) || (
		    NDL_GET(NDL_EMERG_HYD)>30&&AZS_GET(AZS_GEAR_EMERG_BUS)
		  )
		) 
		&&AZS_GET(AZS_GEAR_IND_BUS)&&PWR_GET(PWR_BUS27)
	  ) {
		GEAR_MOVEMENT_TYPE gs1=m_Gear->GetState(0);
		GEAR_MOVEMENT_TYPE gs2=m_Gear->GetState(1);
		GEAR_MOVEMENT_TYPE gs3=m_Gear->GetState(2);

		if(g1==0) {
			TBG_SET(TBG_GEAR1_UP,1);
			TBG_SET(TBG_GEAR1_DOWN,0);
		} else if(g1>=16382) {
			TBG_SET(TBG_GEAR1_DOWN,1);
			TBG_SET(TBG_GEAR1_UP,0);
		} else {
			TBG_SET(TBG_GEAR1_UP,0);
			TBG_SET(TBG_GEAR1_DOWN,0);
		}

		if(g2==0) {
			TBG_SET(TBG_GEAR2_UP,1);
			TBG_SET(TBG_GEAR2_DOWN,0);
		} else if(g2>=16382) {
			TBG_SET(TBG_GEAR2_DOWN,1);
			TBG_SET(TBG_GEAR2_UP,0);
		} else {
			TBG_SET(TBG_GEAR2_UP,0);
			TBG_SET(TBG_GEAR2_DOWN,0);
		}

		if(g3==0) {
			TBG_SET(TBG_GEAR3_UP,1);
			TBG_SET(TBG_GEAR3_DOWN,0);
		} else if(g3>=16382) {
			TBG_SET(TBG_GEAR3_DOWN,1);
			TBG_SET(TBG_GEAR3_UP,0);
		} else {
			TBG_SET(TBG_GEAR3_UP,0);
			TBG_SET(TBG_GEAR3_DOWN,0);
		}
	} else {
		TBG_SET(TBG_GEAR1_UP,0);
		TBG_SET(TBG_GEAR1_DOWN,0);
		TBG_SET(TBG_GEAR2_UP,0);
		TBG_SET(TBG_GEAR2_DOWN,0);
		TBG_SET(TBG_GEAR3_UP,0);
		TBG_SET(TBG_GEAR3_DOWN,0);
	}

	static bool GreenPlayed=false;
	static bool RedPlayed=false;

	if(NDL_GET(NDL_MAIN_HYD)>30&&AZS_GET(AZS_GEAR_MAIN_BUS)&&TBG_GET(TBG_GEAR1_UP)&&TBG_GET(TBG_GEAR2_UP)&&TBG_GET(TBG_GEAR3_UP)&&!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
		if(!GreenPlayed) {
			SND_SET(SND_GEAR_UP_3_RED_ON,1);
			GreenPlayed=true;
		}
	} else {
		GreenPlayed=false;
	}

	if(NDL_GET(NDL_MAIN_HYD)>30&&AZS_GET(AZS_GEAR_MAIN_BUS)&&TBG_GET(TBG_GEAR1_DOWN)&&TBG_GET(TBG_GEAR2_DOWN)&&TBG_GET(TBG_GEAR3_DOWN)&&!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)&&KEY_GET(KEY_GEAR)) {
		if(!RedPlayed) {
			SND_SET(SND_GEAR_DOWN_3_GREEN_ON,1);
			KEY_SET(KEY_GEAR,0);
			RedPlayed=true;
			m_Gear->m_RetrPlayed=false;
		}
	} else {
		RedPlayed=false;
	}

}

void SDSystemMisc::UpdateAZSVC()
{
	AZS_SET(AZS_REDLIGHT2,AZS_GET(AZS_REDLIGHT1));
}

void SDSystemMisc::Smoke()
{
	ImportTable.pPanels->send_key_event(KEY_SMOKE_OFF,0);

	PSIM_DATA p=ImportTable.pSIM1->sim1_get_UserSimData();

	if(g_pSDAPI->GetN(TURB_ENGINE_1_CORRECTED_N2)>78&&NDL_GET(NDL_KUS_IAS_EN)>40&&NDL_GET(NDL_UVID0)<5000) {
		memcpy(&p->pStaticData->pSmokePointList->xyz,&m_OldSmoke[0].xyz,sizeof(XYZF64));
	} else {
		memcpy(&p->pStaticData->pSmokePointList->xyz,&m_OldSmoke[3].xyz,sizeof(XYZF64));
	}

	if(g_pSDAPI->GetN(TURB_ENGINE_2_CORRECTED_N2)>78&&NDL_GET(NDL_KUS_IAS_EN)>40&&NDL_GET(NDL_UVID0)<5000) {
		memcpy(&p->pStaticData->pSmokePointList->pNext->xyz,&m_OldSmoke[1].xyz,sizeof(XYZF64));
	} else {
		memcpy(&p->pStaticData->pSmokePointList->pNext->xyz,&m_OldSmoke[3].xyz,sizeof(XYZF64));
	}

	if(g_pSDAPI->GetN(TURB_ENGINE_3_CORRECTED_N2)>78&&NDL_GET(NDL_KUS_IAS_EN)>40&&NDL_GET(NDL_UVID0)<5000) {
		memcpy(&p->pStaticData->pSmokePointList->pNext->pNext->xyz,&m_OldSmoke[2].xyz,sizeof(XYZF64));
	} else {								  
		memcpy(&p->pStaticData->pSmokePointList->pNext->pNext->xyz,&m_OldSmoke[3].xyz,sizeof(XYZF64));
	}

	ImportTable.pPanels->send_key_event(KEY_SMOKE_ON,0);

}

void SDSystemMisc::Stairs()
{
	if(AZS_GET(AZS_STAIRS_PWR)&&AZS_GET(AZS_STAIRS_SERVO_BUS)&&PWR_GET(PWR_BUS27)) {
		if(AZS_CHG(AZS_STAIRS)) {
			if(AZS_GET(AZS_STAIRS)) {
				VCAnimStairs->set_bool(true);
				SND_SET(SND_STAIRS_DOWN,1);
			} else {
				VCAnimStairs->set_bool(false);
				SND_SET(SND_STAIRS_UP,1);
				SND_SET(SND_STAIRS_RETRACTED,1);
			}
		}
	}
	if(VCAnimStairs->get_bool()) {
		LMP_SET(LMP_STAIRS_DWN,1);
	} else {
		LMP_SET(LMP_STAIRS_DWN,0);
	}
}

void SDSystemMisc::VCKrm()
{
	VCAnimKnb02->set_degree(dgrd(KRM_GET(KRM_RV3M)*18));
	VCAnimKnb04->set_degree(dgrd(KRM_GET(KRM_AGB_MAIN0)*18));
	VCAnimKnb10->set_degree(dgrd(KRM_GET(KRM_AGB_AUXL)*18));
	VCAnimKnb12->set_degree(dgrd(KRM_GET(KRM_AGB_MAIN1)*18));

	VCAnimKnb03->set_number(BTN_GET(BTN_AGB_MAIN_ARRET0));
	VCAnimKnb09->set_number(BTN_GET(BTN_AGB_AUXL_ARRET ));
	VCAnimKnb11->set_number(BTN_GET(BTN_AGB_MAIN_ARRET1));

	VCAnimKnb05->set_degree(dgrd(KRM_GET(KRM_UVID0)*18));
	VCAnimKnb13->set_degree(dgrd(KRM_GET(KRM_UVID1)*18));

}

void SDSystemMisc::BSPK()
{
	if(AZS_GET(AZS_EXTREME_BANK_IND_BUS)&&PWR_GET(PWR_BUS27)) {
		if(!PWR_GET(PWR_BUS36)||!AZS_GET(AZS_CAPT_ADI_MAIN_BUS)||!AZS_GET(AZS_CAPT_ADI_BACKUP_BUS)||!AZS_GET(AZS_COPLT_ADI_BUS)||fabs(NDL_GET(NDL_AGB0_MAIN_PLANE)-NDL_GET(NDL_AGB1_MAIN_PLANE))>7) {
			TBL_SET(TBL_ADI_FAIL,1);
		} else {
			TBL_SET(TBL_ADI_FAIL,0);
		}
	}
}

void SDSystemMisc::Beacons()
{
	if(g_pSDAPI->GetN(BEACON_LIGHTS)) {
		m_Beacon+=20;

		if(m_Beacon>360)
			m_Beacon-=360;

		VCAnimBeacon->set_degree(dgrd(m_Beacon));
	}
}

void SDSystemMisc::SoundSpeed()
{
	static bool s25p=false;
	static bool s50p=false;
	static bool s60p=false;
	static bool s70p=false;
	static bool s80p=false;
	static bool s90p=false;
	static bool s100p=false;
	static bool s110p=false;
	static bool s120p=false;
	static bool s130p=false;
	static bool s135p=false;
	static bool s140p=false;
	static bool s150p=false;
	static bool s160p=false;
	static bool s170p=false;
	static bool s175p=false;

	if(!CFG_GET(CFG_SOUND_SPEED_IN_KNOTS)) {
		if(!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
			m_Speed[0]=0;
			m_Speed[1]=1;
			goto dechg;
		} else
			m_Speed[0]=NDL_GET(NDL_KUS_IAS);

		double spd=m_Speed[0];

		if(spd<170) {
			s50p=false;
			s100p=false;
			s110p=false;
			s120p=false;
			s130p=false;
			s135p=false;
			s140p=false;
			s150p=false;
			s160p=false;
			s170p=false;
			s175p=false;
		}

		if(m_Speed[0]>m_Speed[1]) {
			if(spd>49&&spd<51) {
				if(!s50p) {
					SND_SET(SND_SPEED_50,1);
					s50p=true;
				}
			} else if(spd>99&&spd<101) {
				if(!s100p) {
					SND_SET(SND_SPEED_100,1);
					s100p=true;
				}
			} else if(spd>109&&spd<111) {
				if(!s110p) {
					SND_SET(SND_SPEED_110,1);
					s110p=true;
				}
			} else if(spd>119&&spd<121) {
				if(!s120p) {
					SND_SET(SND_SPEED_120,1);
					s120p=true;
				}
			} else if(spd>128&&spd<130) {
				if(!s130p) {
					SND_SET(SND_SPEED_130,1);
					s130p=true;
				}
			} else if(spd>135&&spd<137) {
				if(!s135p) {
					SND_SET(SND_SPEED_135,1);
					s135p=true;
				}
			} else if(spd>141&&spd<143) {
				if(!s140p) {
					SND_SET(SND_SPEED_140,1);
					s140p=true;
				}
			} else if(spd>147&&spd<149) {
				if(!s150p) {
					SND_SET(SND_SPEED_150,1);
					s150p=true;
				}
			} else if(spd>161&&spd<163) {
				if(!s160p) {
					SND_SET(SND_SPEED_160,1);
					s160p=true;
				}
			} else if(spd>170&&spd<172) {
				if(!s170p) {
					SND_SET(SND_SPEED_170,1);
					s170p=true;
				}
			} else if(spd>175&&spd<177) {
				if(!s175p) {
					SND_SET(SND_SPEED_175,1);
					s175p=true;
				}
			}
		} 
	} else {
		if(!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
			m_Speed[0]=0;
			m_Speed[1]=1;
			goto dechg;
		} else
			m_Speed[0]=NDL_GET(NDL_KUS_IAS_EN);

		double spd=m_Speed[0];

		if(spd<90) {
			s25p=false;
			s50p=false;
			s60p=false;
			s70p=false;
			s80p=false;
			s90p=false;
			s100p=false;
		}

		if(m_Speed[0]>m_Speed[1]) {
			if(spd>24&&spd<26) {
				if(!s25p) {
					SND_SET(SND_SPEED_25KN,1);
					s25p=true;
				}
			} else if(spd>49&&spd<51) {
				if(!s50p) {
					SND_SET(SND_SPEED_50KN,1);
					s50p=true;
				}
			} else if(spd>59&&spd<61) {
				if(!s60p) {
					SND_SET(SND_SPEED_60KN,1);
					s60p=true;
				}
			} else if(spd>69&&spd<71) {
				if(!s70p) {
					SND_SET(SND_SPEED_70KN,1);
					s70p=true;
				}
			} else if(spd>79&&spd<81) {
				if(!s80p) {
					SND_SET(SND_SPEED_80KN,1);
					s80p=true;
				}
			} else if(spd>89&&spd<91) {
				if(!s90p) {
					SND_SET(SND_SPEED_90KN,1);
					s90p=true;
				}
			} else if(spd>99&&spd<101) {
				if(!s100p) {
					SND_SET(SND_SPEED_100KN,1);
					s100p=true;
				}
			}
		} 
	}

	m_Speed[1]=m_Speed[0];

dechg:
	static bool DecisionPlayed=false;

	if(g_pSDAPI->GetN(VERTICAL_SPEED)>256&&g_pSDAPI->GetN(RADIO_HEIGHT)>10&&g_pSDAPI->GetN(RADIO_HEIGHT)<50) {
		if(!DecisionPlayed) {
			SND_SET(SND_SAFE_ALTITUDE,1);
			DecisionPlayed=true;
		}
	} else {
		DecisionPlayed=false;
	}

}

void SDSystemMisc::Lights()
{
	VCAnimLightExtend->set_number(AZS_GET(AZS_LLIGHTSMOTOR)&&PWR_GET(PWR_BUS27));

	if(AZS_GET(AZS_LLIGHTLEFT)==1&&!AZS_GET(AZS_LLIGHTSMOTOR)&&PWR_GET(PWR_BUS27)) {
		VCAnimLLightLand->set_number(1);
		VCAnimLLightTaxi->set_number(0);
		ImportTable.pPanels->send_key_event(KEY_STROBES_ON,0);
		ImportTable.pPanels->send_key_event(KEY_PANEL_LIGHTS_OFF,0);
	} else if(AZS_GET(AZS_LLIGHTLEFT)==2&&!AZS_GET(AZS_LLIGHTSMOTOR)&&PWR_GET(PWR_BUS27)) {
		VCAnimLLightLand->set_number(0);
		VCAnimLLightTaxi->set_number(1);
		ImportTable.pPanels->send_key_event(KEY_PANEL_LIGHTS_ON,0);
		ImportTable.pPanels->send_key_event(KEY_STROBES_OFF,0);
	} else {
		VCAnimLLightLand->set_number(0);
		VCAnimLLightTaxi->set_number(0);
		ImportTable.pPanels->send_key_event(KEY_STROBES_OFF,0);
		ImportTable.pPanels->send_key_event(KEY_PANEL_LIGHTS_OFF,0);
	}

	if(AZS_GET(AZS_LLIGHTRIGHT)==1&&!AZS_GET(AZS_LLIGHTSMOTOR)&&PWR_GET(PWR_BUS27)) {
		VCAnimRLightLand->set_number(1);
		VCAnimRLightTaxi->set_number(0);
		if(!g_pSDAPI->GetN(RECOGNITION_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_RECOGNITION_LIGHTS,0);
		if(g_pSDAPI->GetXML(XML_LIGHT_CABIN,"number"))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_CABIN_LIGHTS,0);
	} else if(AZS_GET(AZS_LLIGHTRIGHT)==2&&!AZS_GET(AZS_LLIGHTSMOTOR)&&PWR_GET(PWR_BUS27)) {
		VCAnimRLightLand->set_number(0);
		VCAnimRLightTaxi->set_number(1);
		if(!g_pSDAPI->GetXML(XML_LIGHT_CABIN,"number"))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_CABIN_LIGHTS,0);
		if(g_pSDAPI->GetN(RECOGNITION_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_RECOGNITION_LIGHTS,0);
	} else {
		VCAnimRLightLand->set_number(0);
		VCAnimRLightTaxi->set_number(0);
		if(g_pSDAPI->GetN(RECOGNITION_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_RECOGNITION_LIGHTS,0);
		if(g_pSDAPI->GetXML(XML_LIGHT_CABIN,"number"))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_CABIN_LIGHTS,0);
	}

	if((AZS_GET(AZS_LLIGHTLEFT)==0&&AZS_GET(AZS_LLIGHTRIGHT)==0)||AZS_GET(AZS_LLIGHTSMOTOR)||!PWR_GET(PWR_BUS27)) {
		ImportTable.pPanels->send_key_event(KEY_LANDING_LIGHTS_OFF,0);
		if(g_pSDAPI->GetN(TAXI_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_TAXI_LIGHTS,0);
	} else if(((AZS_GET(AZS_LLIGHTLEFT)==1&&AZS_GET(AZS_LLIGHTRIGHT)==2)||(AZS_GET(AZS_LLIGHTLEFT)==2&&AZS_GET(AZS_LLIGHTRIGHT)==1))&&!AZS_GET(AZS_LLIGHTSMOTOR)&&PWR_GET(PWR_BUS27)) {
		ImportTable.pPanels->send_key_event(KEY_LANDING_LIGHTS_ON,0);
		if(!g_pSDAPI->GetN(TAXI_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_TAXI_LIGHTS,0);
	} else if(((AZS_GET(AZS_LLIGHTLEFT)==1&&AZS_GET(AZS_LLIGHTRIGHT)==1)||(AZS_GET(AZS_LLIGHTLEFT)==1&&AZS_GET(AZS_LLIGHTRIGHT)==0)||(AZS_GET(AZS_LLIGHTLEFT)==0&&AZS_GET(AZS_LLIGHTRIGHT)==1))&&!AZS_GET(AZS_LLIGHTSMOTOR)&&PWR_GET(PWR_BUS27)) {
		ImportTable.pPanels->send_key_event(KEY_LANDING_LIGHTS_ON,0);
		if(g_pSDAPI->GetN(TAXI_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_TAXI_LIGHTS,0);
	} else if(((AZS_GET(AZS_LLIGHTLEFT)==2&&AZS_GET(AZS_LLIGHTRIGHT)==2)||(AZS_GET(AZS_LLIGHTLEFT)==2&&AZS_GET(AZS_LLIGHTRIGHT)==0)||(AZS_GET(AZS_LLIGHTLEFT)==0&&AZS_GET(AZS_LLIGHTRIGHT)==2))&&!AZS_GET(AZS_LLIGHTSMOTOR)&&PWR_GET(PWR_BUS27)) {
		ImportTable.pPanels->send_key_event(KEY_LANDING_LIGHTS_OFF,0);
		if(!g_pSDAPI->GetN(TAXI_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_TAXI_LIGHTS,0);
	}


	if(AZS_GET(AZS_LIGHTSNAV)) {
		if(!g_pSDAPI->GetN(NAV_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_NAV_LIGHTS,0);
	} else {
		if(g_pSDAPI->GetN(NAV_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_NAV_LIGHTS,0);
	}

	if(AZS_GET(AZS_LIGHTSBCN)) {
		if(!g_pSDAPI->GetN(BEACON_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_BEACON_LIGHTS,0);
	} else {
		if(g_pSDAPI->GetN(BEACON_LIGHTS))
			ImportTable.pPanels->send_key_event(KEY_TOGGLE_BEACON_LIGHTS,0);
	}

	if(PWR_GET(PWR_BUS27)) {
		if(AZS_CHG(AZS_LLIGHTSMOTOR)||AZS_CHG(AZS_LLIGHTLEFT)||AZS_CHG(AZS_LLIGHTRIGHT)) {
			double ll=AZS_GET(AZS_LLIGHTLEFT);
			double lr=AZS_GET(AZS_LLIGHTRIGHT);
			double le=!AZS_GET(AZS_LLIGHTSMOTOR);

			if(le&&ll==1&&lr==1) {
				SND_SET(SND_LIGHTS_EXTENDED,1);
			} else if(!le&&ll==0&&lr==0) {
				SND_SET(SND_LIGHTS_RETRACTED,1);
			}
		}
	}

}

void SDSystemMisc::SoundAlt()
{
	if(!TBG_GET(TBG_GEAR1_DOWN)||!TBG_GET(TBG_GEAR2_DOWN)||!TBG_GET(TBG_GEAR3_DOWN)||g_pSDAPI->GetN(TRAILING_EDGE_FLAPS0_LEFT_ANGLE)<30||g_pSDAPI->GetN(VERTICAL_SPEED)>=0||g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
		POS_SET(POS_DESCENT,0);
		return;
	}

	static bool a200=false;
	static bool a180=false;
	static bool a160=false;
	static bool a140=false;
	static bool a120=false;
	static bool a100=false;
	static bool a90=false;
	static bool a80=false;
	static bool a70=false;
	static bool a60=false;
	static bool a50=false;
	static bool a40=false;
	static bool a30=false;
	static bool a20=false;
	static bool a10=false;

	static bool a1000=false;
	static bool a500=false;
	static bool a400=false;
	static bool a300=false;

	POS_SET(POS_DESCENT,1);

	if(!CFG_GET(CFG_SOUND_ALT_IN_FEET)) {
		double alt=NDL_GET(NDL_UVID0_HIDE);

		if(alt>210||alt<10) {
			a200=false;
			a180=false;
			a160=false;
			a140=false;
			a120=false;
			a100=false;
			a90=false;
			a80=false;
			a70=false;
			a60=false;
			a50=false;
			a40=false;
			a30=false;
			a20=false;
		}

		if(alt>199&&alt<201) {
			if(!a200) {
				SND_SET(SND_ALT_200,1);
				a200=true;
			}
		} else if(alt>179&&alt<181) {
			if(!a180) {
				SND_SET(SND_ALT_180,1);
				a180=true;
			}
		} else if(alt>159&&alt<161) {
			if(!a160) {
				SND_SET(SND_ALT_160,1);
				a160=true;
			}
		} else if(alt>139&&alt<141) {
			if(!a140) {
				SND_SET(SND_ALT_140,1);
				a140=true;
			}
		} else if(alt>119&&alt<121) {
			if(!a120) {
				SND_SET(SND_ALT_120,1);
				a120=true;
			}
		} else if(alt>99&&alt<101) {
			if(!a100) {
				SND_SET(SND_ALT_100,1);
				a100=true;
			}
		} else if(alt>89&&alt<91) {
			if(!a90) {
				SND_SET(SND_ALT_90,1);
				a90=true;
			}
		} else if(alt>79&&alt<81) {
			if(!a80) {
				SND_SET(SND_ALT_80,1);
				a80=true;
			}
		} else if(alt>69&&alt<71) {
			if(!a70) {
				SND_SET(SND_ALT_70,1);
				a70=true;
			}
		} else if(alt>59&&alt<61) {
			if(!a60) {
				SND_SET(SND_ALT_60,1);
				a60=true;
			}
		} else if(alt>49&&alt<51) {
			if(!a50) {
				SND_SET(SND_ALT_50,1);
				a50=true;
			}
		} else if(alt>39&&alt<41) {
			if(!a40) {
				SND_SET(SND_ALT_40,1);
				a40=true;
			}
		} else if(alt>29&&alt<31) {
			if(!a30) {
				SND_SET(SND_ALT_30,1);
				a30=true;
			}
		} else if(alt>19&&alt<21) {
			if(!a20) {
				SND_SET(SND_ALT_20,1);
				a20=true;
			}
		} 
	} else {
		double alt=NDL_GET(NDL_UVID0_EN_HIDE);

		if(alt>1010||alt<5) {
			a1000=false;
			a500=false;
			a400=false;
			a300=false;
			a200=false;
			a100=false;
			a50=false;
			a40=false;
			a30=false;
			a10=false;
		}

		if(alt>999&&alt<1001) {
			if(!a1000) {
				SND_SET(SND_ALT_1000FT,1);
				a1000=true;
			}
		} else if(alt>499&&alt<501) {
			if(!a500) {
				SND_SET(SND_ALT_500FT,1);
				a500=true;
			}
		} else if(alt>399&&alt<401) {
			if(!a400) {
				SND_SET(SND_ALT_400FT,1);
				a400=true;
			}
		} else if(alt>299&&alt<301) {
			if(!a300) {
				SND_SET(SND_ALT_300FT,1);
				a300=true;
			}
		} else if(alt>199&&alt<201) {
			if(!a200) {
				SND_SET(SND_ALT_200FT,1);
				a200=true;
			}
		} else if(alt>99&&alt<101) {
			if(!a100) {
				SND_SET(SND_ALT_100FT,1);
				a100=true;
			}
		} else if(alt>49&&alt<51) {
			if(!a50) {
				SND_SET(SND_ALT_50FT,1);
				a50=true;
			}
		} else if(alt>39&&alt<41) {
			if(!a40) {
				SND_SET(SND_ALT_40FT,1);
				a40=true;
			}
		} else if(alt>29&&alt<31) {
			if(!a30) {
				SND_SET(SND_ALT_30FT,1);
				a30=true;
			}
		} else if(alt>9&&alt<11) {
			if(!a10) {
				SND_SET(SND_ALT_10FT,1);
				a10=true;
			}
		} 
	}

}
