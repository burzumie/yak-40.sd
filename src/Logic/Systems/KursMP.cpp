/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "KursMP.h"

SDSystemKMP::SDSystemKMP()
{
}

SDSystemKMP::~SDSystemKMP()
{
}

void SDSystemKMP::Init()
{
	m_KMP1.Init();
	m_KMP2.Init();
}

void SDSystemKMP::Update()
{
	m_KMP1.Update();
	m_KMP2.Update();

	if(/*GLT_GET(GLT_KMPSYS)==1*/AZS_GET(AZS_KMP_RCVR)&&!BTN_GET(BTN_LIGHTS_TEST4)) {
		LMP_SET(LMP_K1,0);
		LMP_SET(LMP_G1,0);
		LMP_SET(LMP_K2,0);
		LMP_SET(LMP_G2,0);
	} else {
		FLAGS c1=g_pSDAPI->GetF(VOR1_CODE);
		FLAGS c2=g_pSDAPI->GetF(VOR2_CODE);
		if(c1&VOR_CODE_IS_LOCALIZER)LMP_SET(LMP_K1,1);
		else LMP_SET(LMP_K1,0);
		if(c1&VOR_CODE_GLIDESLOPE)LMP_SET(LMP_G1,1);
		else LMP_SET(LMP_G1,0);
		if(c2&VOR_CODE_IS_LOCALIZER)LMP_SET(LMP_K2,1);
		else LMP_SET(LMP_K2,0);
		if(c2&VOR_CODE_GLIDESLOPE)LMP_SET(LMP_G2,1);
		else LMP_SET(LMP_G2,0);
	}

}

void SDSystemKMP::Load(CIniFile *ini)
{
}

void SDSystemKMP::Save(CIniFile *ini)
{
}

void CKursMP1::Update()
{
	VCAnimHndObs->set_degree(dgrd(HND_GET(HND_OBS6)*18));
	VCAnimHndMhz->set_degree(dgrd(HND_GET(HND_KMP1FREQ)*18));
	VCAnimBtnAud->set_number(BTN_GET(BTN_KMP1ID));
	VCAnimHndKhz->set_degree(dgrd(HND_GET(HND_KMP1FREQ2)*18));

	m_VOROBS=(UINT32)(POS_GET(POS_OBS61)*100+POS_GET(POS_OBS62)*10+POS_GET(POS_OBS63));
	m_NAVFreq=(UINT32)(POS_GET(POS_NAV6_FREQ1)*10000+POS_GET(POS_NAV6_FREQ2)*1000+POS_GET(POS_NAV6_FREQ3)*100+POS_GET(POS_NAV6_FREQ4)*10+POS_GET(POS_NAV6_FREQ5));
	m_NAVFreq=Dec2Bcd(m_NAVFreq);
	m_DMEDist=g_pSDAPI->GetN(DME1_DISTANCE);
	if(GLT_GET(GLT_DME6)==1)
		m_DMEDist=NAUTIC_MILE_TO_METER(m_DMEDist)/1000;

	if(g_pSDAPI->GetE(VOR1_TF_FLAG)==VOR_TF_FLAG_FROM&&PWR_GET(PWR_KURSMP1)&&AZS_GET(AZS_KMP_RCVR)) {
		TBL_SET(TBL_VOR1_TO,0);
		TBL_SET(TBL_VOR1_FROM,1);
	} else if(g_pSDAPI->GetE(VOR1_TF_FLAG)==VOR_TF_FLAG_TO&&PWR_GET(PWR_KURSMP1)&&AZS_GET(AZS_KMP_RCVR)) {
		TBL_SET(TBL_VOR1_TO,1);
		TBL_SET(TBL_VOR1_FROM,0);
	} else {
		TBL_SET(TBL_VOR1_TO,0);
		TBL_SET(TBL_VOR1_FROM,0);
	}

	ImportTable.pPanels->send_key_event(KEY_VOR1_SET,m_VOROBS);
	ImportTable.pPanels->send_key_event(KEY_NAV1_RADIO_SET,m_NAVFreq);

	if(!AZS_GET(AZS_KMP1PWR)||AZS_GET(AZS_KMP_RSBN_DME)||!PWR_GET(PWR_BUS27)) {
		POS_SET(POS_DME61M,-1);
		POS_SET(POS_DME61 ,-1);
		POS_SET(POS_DME62M,-1);
		POS_SET(POS_DME62 ,-1);
		POS_SET(POS_DME63M,-1);
		POS_SET(POS_DME63 ,-1);
		POS_SET(POS_DME64M,-1);
		POS_SET(POS_DME64 ,-1);
		POS_SET(POS_DME65M,-1);
		POS_SET(POS_DME65 ,-1);
		return;
	}

	if(BTN_GET(BTN_KMP1ID)) {
		ImportTable.pPanels->send_key_event(KEY_RADIO_VOR1_IDENT_ENABLE,0);
	} else {                                                                                                                 
		ImportTable.pPanels->send_key_event(KEY_RADIO_VOR1_IDENT_DISABLE,0);
	}

	if(m_DMEDist<0||AZS_GET(AZS_KMP1VOR)==0||GLT_GET(GLT_KMPSYS)==2) {
		POS_SET(POS_DME61M, 0);
		POS_SET(POS_DME61 ,-1);
		POS_SET(POS_DME62M, 0);
		POS_SET(POS_DME62 ,-1);
		POS_SET(POS_DME63M, 0);
		POS_SET(POS_DME63 ,-1);
		POS_SET(POS_DME64M, 0);
		POS_SET(POS_DME64 ,-1);
		POS_SET(POS_DME65M, 0);
		POS_SET(POS_DME65 ,-1);
		return;
	}

	char tmp[256];
	sprintf(tmp,"%6.2f",m_DMEDist);

	if(tmp[0]==' ') {
		POS_SET(POS_DME61M,-1);
		POS_SET(POS_DME61,0);
	} else {
		POS_SET(POS_DME61M,-1);
		POS_SET(POS_DME61,(int)(tmp[0]-48));
	}

	if(tmp[1]==' ') {
		POS_SET(POS_DME62M,-1);
		POS_SET(POS_DME62,0);
	} else {
		POS_SET(POS_DME62M,-1);
		POS_SET(POS_DME62,(int)(tmp[1]-48));
	}

	if(tmp[2]==' ')	{
		POS_SET(POS_DME63M,-1);
		POS_SET(POS_DME63,0);
	} else {
		POS_SET(POS_DME63M,-1);
		POS_SET(POS_DME63,(int)(tmp[2]-48));
	}

	if(tmp[4]==' ')	{
		POS_SET(POS_DME64M,-1);
		POS_SET(POS_DME64,0);
	} else {
		POS_SET(POS_DME64M,-1);
		POS_SET(POS_DME64,(int)(tmp[4]-48));
	}

	if(tmp[5]==' ') {
		POS_SET(POS_DME65M,-1);
		POS_SET(POS_DME65,0);
	} else {
		POS_SET(POS_DME65M,-1);
		POS_SET(POS_DME65,(int)(tmp[5]-48));
	}

}

void CKursMP2::Update()
{
	VCAnimHndObs->set_degree(dgrd(HND_GET(HND_OBS7)*18));
	VCAnimHndMhz->set_degree(dgrd(HND_GET(HND_KMP2FREQ)*18));
	VCAnimHndKhz->set_degree(dgrd(HND_GET(HND_KMP2FREQ2)*18));
	VCAnimBtnAud->set_number(BTN_GET(BTN_KMP2ID));

	m_VOROBS=(UINT32)(POS_GET(POS_OBS71)*100+POS_GET(POS_OBS72)*10+POS_GET(POS_OBS73));
	m_NAVFreq=(UINT32)(POS_GET(POS_NAV7_FREQ1)*10000+POS_GET(POS_NAV7_FREQ2)*1000+POS_GET(POS_NAV7_FREQ3)*100+POS_GET(POS_NAV7_FREQ4)*10+POS_GET(POS_NAV7_FREQ5));
	m_NAVFreq=Dec2Bcd(m_NAVFreq);
	m_DMEDist=g_pSDAPI->GetN(DME2_DISTANCE);
	if(GLT_GET(GLT_DME7)==1)
		m_DMEDist=NAUTIC_MILE_TO_METER(m_DMEDist)/1000;

	if(g_pSDAPI->GetE(VOR2_TF_FLAG)==VOR_TF_FLAG_FROM&&PWR_GET(PWR_KURSMP2)&&AZS_GET(AZS_KMP_RCVR)) {
		TBL_SET(TBL_VOR2_TO,0);
		TBL_SET(TBL_VOR2_FROM,1);
	} else if(g_pSDAPI->GetE(VOR2_TF_FLAG)==VOR_TF_FLAG_TO&&PWR_GET(PWR_KURSMP2)&&AZS_GET(AZS_KMP_RCVR)) {
		TBL_SET(TBL_VOR2_TO,1);
		TBL_SET(TBL_VOR2_FROM,0);
	} else {
		TBL_SET(TBL_VOR2_TO,0);
		TBL_SET(TBL_VOR2_FROM,0);
	}

	ImportTable.pPanels->send_key_event(KEY_VOR2_SET,m_VOROBS);
	ImportTable.pPanels->send_key_event(KEY_NAV2_RADIO_SET,m_NAVFreq);

	if(!AZS_GET(AZS_KMP2PWR)||AZS_GET(AZS_KMP_RSBN_DME)||!PWR_GET(PWR_BUS27)) {
		POS_SET(POS_DME71M,-1);
		POS_SET(POS_DME71 ,-1);
		POS_SET(POS_DME72M,-1);
		POS_SET(POS_DME72 ,-1);
		POS_SET(POS_DME73M,-1);
		POS_SET(POS_DME73 ,-1);
		POS_SET(POS_DME74M,-1);
		POS_SET(POS_DME74 ,-1);
		POS_SET(POS_DME75M,-1);
		POS_SET(POS_DME75 ,-1);
		return;
	}

	if(BTN_GET(BTN_KMP2ID)) {
		ImportTable.pPanels->trigger_key_event(KEY_RADIO_VOR2_IDENT_ENABLE,0);
	} else {                                                                                                                 
		ImportTable.pPanels->trigger_key_event(KEY_RADIO_VOR2_IDENT_DISABLE,0);
	}

	if(m_DMEDist<0||AZS_GET(AZS_KMP2VOR)==0||GLT_GET(GLT_KMPSYS)==2) {
		POS_SET(POS_DME71M, 0);
		POS_SET(POS_DME71 ,-1);
		POS_SET(POS_DME72M, 0);
		POS_SET(POS_DME72 ,-1);
		POS_SET(POS_DME73M, 0);
		POS_SET(POS_DME73 ,-1);
		POS_SET(POS_DME74M, 0);
		POS_SET(POS_DME74 ,-1);
		POS_SET(POS_DME75M, 0);
		POS_SET(POS_DME75 ,-1);
		return;
	}

	char tmp[256];
	sprintf(tmp,"%6.2f",m_DMEDist);

	if(tmp[0]==' ') {
		POS_SET(POS_DME71M,-1);
		POS_SET(POS_DME71,0);
	} else {
		POS_SET(POS_DME71M,-1);
		POS_SET(POS_DME71,(int)(tmp[0]-48));
	}

	if(tmp[1]==' ') {
		POS_SET(POS_DME72M,-1);
		POS_SET(POS_DME72,0);
	} else {
		POS_SET(POS_DME72M,-1);
		POS_SET(POS_DME72,(int)(tmp[1]-48));
	}

	if(tmp[2]==' ')	{
		POS_SET(POS_DME73M,-1);
		POS_SET(POS_DME73,0);
	} else {
		POS_SET(POS_DME73M,-1);
		POS_SET(POS_DME73,(int)(tmp[2]-48));
	}

	if(tmp[4]==' ')	{
		POS_SET(POS_DME74M,-1);
		POS_SET(POS_DME74,0);
	} else {
		POS_SET(POS_DME74M,-1);
		POS_SET(POS_DME74,(int)(tmp[4]-48));
	}

	if(tmp[5]==' ') {
		POS_SET(POS_DME75M,-1);
		POS_SET(POS_DME75,0);
	} else {
		POS_SET(POS_DME75M,-1);
		POS_SET(POS_DME75,(int)(tmp[5]-48));
	}
}

void CKursMP1::Init()
{
	char tmp4[2];
	tmp4[1]='\0';

	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sOBS1Default[0];
	int o1=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sOBS1Default[1];
	int o2=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sOBS1Default[2];
	int o3=atoi(tmp4);

	POS_SET(POS_OBS61,o1);
	POS_SET(POS_OBS62,o2);
	POS_SET(POS_OBS63,o3);

	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV1Default[0];
	int f1=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV1Default[1];
	int f2=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV1Default[2];
	int f3=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV1Default[4];
	int f4=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV1Default[5];
	int f5=atoi(tmp4);

	POS_SET(POS_NAV6_FREQ1,f1);
	POS_SET(POS_NAV6_FREQ2,f2);
	POS_SET(POS_NAV6_FREQ3,f3);
	POS_SET(POS_NAV6_FREQ4,f4);
	POS_SET(POS_NAV6_FREQ5,f5);

	VCAnimHndMhz=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_kmpl_l",ImportTable.pPanels,false));
	VCAnimHndKhz=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_kmpl_r",ImportTable.pPanels,false));
	VCAnimHndObs=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_obsl",ImportTable.pPanels,false));
	VCAnimBtnAud=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_kmpl_btn",ImportTable.pPanels,false));

}

void CKursMP2::Init()
{
	char tmp4[2];
	tmp4[1]='\0';

	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sOBS2Default[0];
	int o1=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sOBS2Default[1];
	int o2=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sOBS2Default[2];
	int o3=atoi(tmp4);

	POS_SET(POS_OBS71,o1);
	POS_SET(POS_OBS72,o2);
	POS_SET(POS_OBS73,o3);

	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV2Default[0];
	int f1=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV2Default[1];
	int f2=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV2Default[2];
	int f3=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV2Default[4];
	int f4=atoi(tmp4);
	tmp4[0]=g_pSDAPI->m_Config.m_CfgNav.m_sNAV2Default[5];
	int f5=atoi(tmp4);

	POS_SET(POS_NAV7_FREQ1,f1);
	POS_SET(POS_NAV7_FREQ2,f2);
	POS_SET(POS_NAV7_FREQ3,f3);
	POS_SET(POS_NAV7_FREQ4,f4);
	POS_SET(POS_NAV7_FREQ5,f5);

	VCAnimHndMhz=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_kmpr_l",ImportTable.pPanels,false));
	VCAnimHndKhz=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_kmpr_r",ImportTable.pPanels,false));
	VCAnimHndObs=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_obsr",ImportTable.pPanels,false));
	VCAnimBtnAud=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_kmpr_btn",ImportTable.pPanels,false));
}
