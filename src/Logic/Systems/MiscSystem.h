/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/MiscSystem.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../../Lib/PanelsTools.h"
#include "../../Lib/NamedVar.h"
#include "../Main.h"
#include "../LogicBase.h"
#include "Gear.h"
#include "RTU.h"

class SDSystemMisc : public SDLogicBase
{
private:
	unsigned short m_Brkpos[2];
	int m_Stopor[2];
	double m_FixThrottlePos[3];
	CGear *m_Gear;
	CRTU *m_RTU;
	double m_Beacon;
	double m_Speed[2];
	bool m_BrakeCheckedSound;
	bool m_ArksOnSound;
	bool m_Alt0SetSound;

	bool m_FrtPlayed;

	_smokePoint m_OldSmoke[4];
	int m_SmokeState[3];

	std::auto_ptr<CNamedVar> VCAnimFrt;
	std::auto_ptr<CNamedVar> VCAnimStairs;
	std::auto_ptr<CNamedVar> VCAnimKnb02;
	std::auto_ptr<CNamedVar> VCAnimKnb04;
	std::auto_ptr<CNamedVar> VCAnimKnb10;
	std::auto_ptr<CNamedVar> VCAnimKnb12;
	std::auto_ptr<CNamedVar> VCAnimKnb03;
	std::auto_ptr<CNamedVar> VCAnimKnb09;
	std::auto_ptr<CNamedVar> VCAnimKnb11;
	std::auto_ptr<CNamedVar> VCAnimKnb05;
	std::auto_ptr<CNamedVar> VCAnimKnb13;

	std::auto_ptr<CNamedVar> VCAnimBeacon;

	std::auto_ptr<CNamedVar> VCAnimLLightTaxi;
	std::auto_ptr<CNamedVar> VCAnimRLightTaxi;
	std::auto_ptr<CNamedVar> VCAnimLLightLand;
	std::auto_ptr<CNamedVar> VCAnimRLightLand;
	std::auto_ptr<CNamedVar> VCAnimLightExtend;

private:
	void CheckPanelState();
	void MakeAnimation();
	void AnimateStopor();
	void AnimateParkingBrake();
	void AnimateGearSwitch();
	void CheckAllPower();
	void ProcessTablo();
	void UpdateAZSVC();
	void Smoke();
	void Stairs();
	void VCKrm();
	void BSPK();
	void Beacons();
	void SoundSpeed();
	void Lights();
	void SoundAlt();

public:
	SDSystemMisc() {
	};
	virtual ~SDSystemMisc() {
	};

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini) {
		VCAnimStairs->set_bool(ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),"STAIRS",VCAnimStairs->get_bool()));
	};
	virtual void Save(CIniFile *ini) {
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),"STAIRS",VCAnimStairs->get_bool());
	};

};
