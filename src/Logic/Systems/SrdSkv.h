/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"
#include "AirStarter.h"

enum SKV_MODE {
	SKV_MODE_2,
	SKV_MODE_1
};

class SDSystemSrdSkv : public SDLogicBase
{
private:
	auto_ptr<CNamedVar> XmlAnimSalonTemp;
	auto_ptr<CNamedVar> XmlAnimDuctTemp;
	auto_ptr<CNamedVar> XmlAnimUrvk;
	auto_ptr<CNamedVar> XmlAnimUvpdAlt;
	auto_ptr<CNamedVar> XmlAnimUvpdDP;

	CAirStarter*	m_Airstarter;		// ��������� �������

	double	SetUrvkToVC(double ndl);
	double	SetUvpdAltToVC(double ndl);
	double	SetUvpdDPToVC(double ndl);
	void	SetNeedlesToVC();
	void	GetData();

	//////////////////////////////////////////////////////////////////////////

	double	m_BeginPressuration;		// ������ ������������
	double	m_SuperfluousPSI;			// ���������� ��������
	double	m_ChangeRatePSI;			// �������� ��� ���� (0.15-0.30 �� �� ��/���)

	double	m_CurrentCabinPressuration;	// ������� �������� � ������
	double	m_CurrentCabinAlt;			// ������� �������� � ������

public:
	SDSystemSrdSkv();
	virtual ~SDSystemSrdSkv();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};

