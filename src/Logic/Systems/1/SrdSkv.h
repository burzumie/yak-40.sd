/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"
#include "AirStarter.h"

const double UVPD_ALT_COEFF		= 8000.0/2300.0;

enum SKV_MODE {
	SKV_MODE_2,
	SKV_MODE_1
};

class SDSystemSrdSkv : public SDLogicBase
{
private:
	auto_ptr<CNamedVar> XmlAnimSalonTemp;
	auto_ptr<CNamedVar> XmlAnimDuctTemp;
	auto_ptr<CNamedVar> XmlAnimUrvk;
	auto_ptr<CNamedVar> XmlAnimUvpdAlt;
	auto_ptr<CNamedVar> XmlAnimUvpdDP;

	CAirStarter*	m_Airstarter;		// ��������� �������

	// External
	int		m_AzsConditioner;			// ����������� ���� | ���								���					������ ����� ���
	int		m_Swt2077Power;				// �������� �������� | ������� | �������� �����������	�������������		��� 2077 (������ �����)			
	double	m_PosSuperfluousPSI;		// ���������� ��������									����������			��� 2077 (������ �����)			
	double	m_PosHermeticSealing;		// ������ ������������									����������			��� 2077 (������ �����)			
	double	m_PosSpeedChangePSI;		// �������� ��������� ��������							����������			��� 2077 (������ �����)			
	int		m_AzsSystem;				// ��� ���� ���� ����� | ���� | ���						���					������ ��			
	int		m_AzsSystemChanged;			// 
	int		m_AzsSoundAlarm;			// ���� ������ ���� | ���								���					������ ��			
	int		m_AzsSystemEmerg;			// ���� ��� ���� ����� | ���� | ���						���					������ ��			
	int		m_AzsSystemEmergChanged;	// 
	int		m_AzsDoublerRegPSI;			// ������ ��� ���� ���� ���� | ���						���					������ ��			
	int		m_AzsDumpPSI;				// ����� ���� ���� ���� | ���							���					������ ��			
	int		m_AzsTempAirInInterior;		// ���� ���� � ������ ���� | ����� | ����� | �����		���					������ ��			
	int		m_AzsTempAirOutTurboholod;	// ���� ���� �� �������� ���� | ����� | ����� | �����	���					������ ��			
	int		m_AzsChargeAirInSystem;		// ������ ���� � ������� ���� | ����� | ���� | ����		���					������ ��			
	int		m_AzsChargeAirInSystemMode;	// ������ ���� � ������� 1 ��� | 2 ���					���					������ ��			
	int		m_AzsAirToCrew;				// ���� ������� ������ | ������							���					������ ��			
	int		m_GltTempDuct;				// ��� ������ | ����� | ������ �������					�������� ������		������ ��			
	int		m_PosTargetTempDuct;		// �������� ����������� �� ���
	int		m_PosTargetTempSalon;		// �������� ����������� � ������

	// Internal
	int		m_Powered;					// �������������� �������								
	double	m_SelectionAirPCT;			// ��������� �������� ������ ������� (0-100%)			
	int   	m_SelectionAirReq;			// 1 - ������ �� ��������, 2 - ������ �� ��������
	int		m_Status;					// ������ ������� ��� ���								
	double	m_SelectionAirFromEngine;	// ����� ������� �� ���������� (0-2)
	double	m_SelectionAirFromEngineMax;// ����� ������� �� ���������� (0-2)
	double	m_SelectionAirFromEngineMax1Const;// �������� ��������� ��������� ��� ������������� ������ �������
	double	m_SelectionAirFromEngineMax2Const;// �������� ��������� ��������� ��� ������������� ������ �������
	double	m_ChargeAirInSystem;		// ������ ������� � ������� (1-10 �������� ������)
	double	m_ChargeAirInSystemMax;		// ������������ ������ ������� � ������� (1-10 �������� ������)
	double	m_ChargeAirInSystemMax1Const;// �������� ��������� ��������� ��� ������������� ������� �������
	double	m_ChargeAirInSystemMax2Const;// �������� ��������� ��������� ��� ������������� ������� �������
	double	m_SuperfluousPSI;			// ���������� ��������
	double	m_OnboardPSI;				// �������� �� ����� �������� (�� �� ��)
	double	m_OuterHeight;				// ������ ������� (�)
	double	m_CabinHeight;				// ������ � ������ (�)
	double	m_SpeedChangeHeightInCabin;	// �������� ��������� ������ � ������ (�/�)

	// Indication
	double	m_NdlCabinHeight;			// ������ � ������ (-400-5000 �)
	double	m_NdlSuperfluousPSI;		// ���������� �������� �������� (0.0-0.6 ���/�� ��)
	double	m_NdlHermeticSealing;		// ������ ������������ (430-810 �� �� ��)
	double	m_NdlSpeedChangePSI;		// �������� ��� ���� (0.15-0.30 �� �� ��/���)
	double	m_NdlSalonTemp;				// ����������� ������� � ������
	double	m_NdlDuctTemp;				// ����������� ������� � ������

	void	GetData();
	void	SetData();
	double	SetUrvkToVC(double ndl);
	double	SetUvpdAltToVC(double ndl);
	double	SetUvpdDPToVC(double ndl);
	void	SrdUpdate();

public:
	SDSystemSrdSkv();
	virtual ~SDSystemSrdSkv();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};

