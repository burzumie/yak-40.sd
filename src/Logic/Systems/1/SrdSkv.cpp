/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "SrdSkv.h"

/*
SDSystemSrdSkv::SDSystemSrdSkv()
{
	m_AzsConditioner=0; 
	m_Swt2077Power=0;
	m_PosSuperfluousPSI=0;
	m_PosHermeticSealing=0;
	m_PosSpeedChangePSI=0;
	m_AzsSystem=0;
	m_AzsSoundAlarm=0;
	m_AzsSystemEmerg=0;
	m_AzsDoublerRegPSI=0;
	m_AzsDumpPSI=0;
	m_AzsTempAirInInterior=0;
	m_AzsTempAirOutTurboholod=0;
	m_AzsChargeAirInSystem=0;
	m_AzsChargeAirInSystemMode=0;
	m_AzsAirToCrew=0;
	m_GltTempDuct=0;
	m_Powered=0;
	m_SelectionAirPCT=0;
	m_Status=0;
	m_SelectionAirFromEngine=0;
	m_ChargeAirInSystem=0;
	m_SuperfluousPSI=0;
	m_OnboardPSI=0;
	m_CabinHeight=0;
	m_SpeedChangeHeightInCabin=0;
	m_NdlCabinHeight=0;
	m_NdlSuperfluousPSI=0;
	m_NdlHermeticSealing=430;
	m_NdlSpeedChangePSI=0;
	m_SelectionAirFromEngineMax=0;
	m_ChargeAirInSystemMax=0;
	m_SelectionAirFromEngineMax1Const=4;
	m_SelectionAirFromEngineMax2Const=3;
	m_ChargeAirInSystemMax1Const=8;
	m_ChargeAirInSystemMax2Const=5;
}

SDSystemSrdSkv::~SDSystemSrdSkv()
{
}

void SDSystemSrdSkv::Init()
{
	m_Airstarter=CAirStarter::Instance();

	NDL_SET(NDL_2077PRESSBEGIN,650);
	NDL_SET(NDL_2077DIFFPRESS,0.4);
	POS_SET(POS_PRESSRATE,9);

	POS_SET(POS_TARGET_DUCT_TEMP,10);
	POS_SET(POS_TARGET_SALON_TEMP,22);

	m_NdlSalonTemp=g_pSDAPI->GetN(AMBIENT_TEMP_DEGREES_C)/10+15;

	XmlAnimSalonTemp=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_temp_salon",ImportTable.pPanels));
	XmlAnimDuctTemp=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_temp_duct",ImportTable.pPanels));
	XmlAnimUrvk=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_urvk",ImportTable.pPanels));
	XmlAnimUvpdAlt=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_uvpd_alt",ImportTable.pPanels));
	XmlAnimUvpdDP=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_uvpd_press",ImportTable.pPanels));
}

void SDSystemSrdSkv::SrdUpdate()
{
}

// 1 �.���. = 1 ��/��^2 = 735 �� �� ��

void SDSystemSrdSkv::Update()
{
	GetData();

	//m_NdlCabinHeight=m_OuterHeight/UVPD_ALT_COEFF; //g_pSDAPI->GetN(AMBIENT_PRES_MBAR)/16*0.75;

	static double s0=0;
	static double germ=0; 
	static double s1=0; //germ/10-650*10+1300;
	
	if(germ==0) {
		m_NdlCabinHeight=m_OuterHeight;
		germ=g_pSDAPI->GetXML(XML_AMBIENT_PRESSURE,"mbar")*7.5;
	}

	s1=fabs((germ/10-m_PosHermeticSealing)*10+1300);

	if(m_OuterHeight>s1&&m_Swt2077Power) {
		if(m_NdlCabinHeight>s1+2) {
			s0=m_PosSpeedChangePSI*-0.001;
		} else if(m_NdlCabinHeight<s1-2) {
			s0=m_PosSpeedChangePSI*0.001;
		} else {
			s0=0;
		}
		m_NdlCabinHeight+=s0*10;
		m_SpeedChangeHeightInCabin=400*s0;
	} else {
		m_SpeedChangeHeightInCabin=0;
		s1=m_OuterHeight-m_NdlCabinHeight;
		if(s1<-1) {
			m_NdlCabinHeight-=0.5;
			m_SpeedChangeHeightInCabin=-20;
		}
		if(s1>1) {
			m_NdlCabinHeight+=0.5;
			m_SpeedChangeHeightInCabin=20;
		}
	}

	m_SuperfluousPSI=(m_OuterHeight-m_NdlCabinHeight)/20;

	if(m_SuperfluousPSI>m_PosSuperfluousPSI) {
		m_SuperfluousPSI=m_PosSuperfluousPSI;
	}

	m_SuperfluousPSI=m_SuperfluousPSI*0.07;

	m_SpeedChangeHeightInCabin=m_SpeedChangeHeightInCabin+g_pSDAPI->GetXML(XML_VERTICAL_SPEED,"meters/second");
	m_SpeedChangeHeightInCabin=m_SpeedChangeHeightInCabin*0.03;

	if(m_NdlCabinHeight>2500) {
		SND_SET(SND_SRD_ALRAM,1);
	} else {
		SND_SET(SND_SRD_ALRAM,0);
	}

	SetData();
}

void SDSystemSrdSkv::Load(CIniFile *ini)
{
}

void SDSystemSrdSkv::Save(CIniFile *ini)
{
}

int g_ChangeRate[]={15,15,16,16,16,17,17,17,18,18,18,19,19,19,20,20,20,21,21,21,22,22,22,23,23,23,24,24,24,25,25,25,26,26,26,27,27,27,28,28,28,29,29,29,30,30};

void SDSystemSrdSkv::GetData()
{
	m_AzsConditioner			= AZS_GET(AZS_AIR_COND_SYS_BUS);
	m_Swt2077Power				= AZS_GET(AZS_2077PWR);
	m_AzsSystem					= AZS_GET(AZS_AIR_PRESS_SYS_MASTER);
	m_AzsSystemChanged			= AZS_CHG(AZS_AIR_PRESS_SYS_MASTER);
	m_AzsSoundAlarm				= AZS_GET(AZS_AIR_PRESS_SYS_SOUND_ALARM);
	m_AzsSystemEmerg			= AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG);
	m_AzsSystemEmergChanged		= AZS_CHG(AZS_AIR_PRESS_SYS_MASTER_EMERG);
	m_AzsDoublerRegPSI			= AZS_GET(AZS_AIR_PRESS_CONTROL_BCKUP);
	m_AzsDumpPSI				= AZS_GET(AZS_AIR_PRESS_DROP_EMERG);
	m_AzsTempAirInInterior		= AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN);
	m_AzsTempAirOutTurboholod	= AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT);
	m_AzsAirToCrew				= AZS_GET(AZS_DECK_COND);
	m_AzsChargeAirInSystem		= AZS_GET(AZS_BLEEDAIR_EXTRACT_VALVE);
	m_AzsChargeAirInSystemMode	= AZS_GET(AZS_BLEEDAIR_EXTRACT_RATIO);
	m_GltTempDuct				= GLT_GET(GLT_TEMP_DUCT);
	m_NdlHermeticSealing		= NDL_GET(NDL_2077PRESSBEGIN);
	m_NdlSuperfluousPSI			= NDL_GET(NDL_2077DIFFPRESS);
	m_NdlSpeedChangePSI			= POS_GET(POS_PRESSRATE);
	m_PosTargetTempDuct			= (int)POS_GET(POS_TARGET_DUCT_TEMP);
	m_PosTargetTempSalon		= (int)POS_GET(POS_TARGET_SALON_TEMP);
	m_OuterHeight				= g_pSDAPI->GetXML(XML_PLANE_ALTITUDE,"meters");
	m_PosSuperfluousPSI			= NDL_GET(NDL_2077DIFFPRESS)*100;
	m_PosSpeedChangePSI			= (double)g_ChangeRate[int(POS_GET(POS_PRESSRATE))];
	m_PosHermeticSealing		= NDL_GET(NDL_2077PRESSBEGIN);
}

void SDSystemSrdSkv::SetData()
{
	LMP_SET(LMP_AIR_PRESS_COND_SYS_OFF,!m_Status);
	m_Airstarter->RequestAir(m_SelectionAirFromEngine,AIR_REQUESTER_SKV);
	NDL_SET(NDL_TEMP_SALON,m_NdlSalonTemp);
	NDL_SET(NDL_URVK,m_ChargeAirInSystem);
	NDL_SET(NDL_UVPD_ALT,m_NdlCabinHeight);
	NDL_SET(NDL_UVPD_DIFPRESS,m_SuperfluousPSI);
	NDL_SET(NDL_VS_SRD,m_SpeedChangeHeightInCabin);

	XmlAnimSalonTemp->set_degree(dgrd(m_NdlSalonTemp*1.015));
	XmlAnimDuctTemp->set_degree(dgrd(NDL_GET(NDL_DUCT)*0.8));

	XmlAnimUrvk->set_degree(dgrd(SetUrvkToVC(NDL_GET(NDL_URVK)))); 
	XmlAnimUvpdAlt->set_degree(dgrd(SetUvpdAltToVC(NDL_GET(NDL_UVPD_ALT)))); 
	XmlAnimUvpdDP->set_degree(dgrd(SetUvpdDPToVC(NDL_GET(NDL_UVPD_DIFPRESS)))); 
}

double SDSystemSrdSkv::SetUrvkToVC(double ndl)
{
	double var=ndl;

	if(var<=5)				return var*30;
	if(var> 5&&var<=7)		return 5*30+(var-5)*31;
	if(var> 7)           	return 5*30+(  7-5)*31+(var-7)*25.3;

	return 0;
}

double SDSystemSrdSkv::SetUvpdAltToVC(double ndl)
{
	double var=ndl;

	if(var<=2000)				return var*0.06;
	if(var> 2000&&var<=3000)	return 2000*0.06+(var-2000)*0.055;
	if(var> 3000)           	return 2000*0.06+(3000-2000)*0.055+(var-3000)*0.0585;

	return 0;
}

double SDSystemSrdSkv::SetUvpdDPToVC(double ndl)
{
	double var=ndl;

	if(var<=0.4)			return var*312.5;
	if(var> 0.4)           	return 0.4*312.5+(var-0.4)*337.5;

	return 0;
}
*/

int g_ChangeRate[]={15,15,16,16,16,17,17,17,18,18,18,19,19,19,20,20,20,21,21,21,22,22,22,23,23,23,24,24,24,25,25,25,26,26,26,27,27,27,28,28,28,29,29,29,30,30};

SDSystemSrdSkv::SDSystemSrdSkv()
{
	m_AzsConditioner=0; 
	m_Swt2077Power=0;
	m_PosSuperfluousPSI=0;
	m_PosHermeticSealing=0;
	m_PosSpeedChangePSI=0;
	m_AzsSystem=0;
	m_AzsSoundAlarm=0;
	m_AzsSystemEmerg=0;
	m_AzsDoublerRegPSI=0;
	m_AzsDumpPSI=0;
	m_AzsTempAirInInterior=0;
	m_AzsTempAirOutTurboholod=0;
	m_AzsChargeAirInSystem=0;
	m_AzsChargeAirInSystemMode=0;
	m_AzsAirToCrew=0;
	m_GltTempDuct=0;
	m_Powered=0;
	m_SelectionAirPCT=0;
	m_Status=0;
	m_SelectionAirFromEngine=0;
	m_ChargeAirInSystem=0;
	m_SuperfluousPSI=0;
	m_OnboardPSI=0;
	m_CabinHeight=0;
	m_SpeedChangeHeightInCabin=0;
	m_NdlCabinHeight=0;
	m_NdlSuperfluousPSI=0;
	m_NdlHermeticSealing=430;
	m_NdlSpeedChangePSI=0;
	m_SelectionAirFromEngineMax=0;
	m_ChargeAirInSystemMax=0;
	m_SelectionAirFromEngineMax1Const=4;
	m_SelectionAirFromEngineMax2Const=3;
	m_ChargeAirInSystemMax1Const=8;
	m_ChargeAirInSystemMax2Const=5;

}

SDSystemSrdSkv::~SDSystemSrdSkv()
{
}

void SDSystemSrdSkv::Init()
{
	m_Airstarter=CAirStarter::Instance();

	NDL_SET(NDL_2077PRESSBEGIN,650);
	NDL_SET(NDL_2077DIFFPRESS,0.4);
	POS_SET(POS_PRESSRATE,9);

	POS_SET(POS_TARGET_DUCT_TEMP,10);
	POS_SET(POS_TARGET_SALON_TEMP,22);

	m_NdlSalonTemp=g_pSDAPI->GetN(AMBIENT_TEMP_DEGREES_C)/10+15;

	XmlAnimSalonTemp=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_temp_salon",ImportTable.pPanels));
	XmlAnimDuctTemp=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_temp_duct",ImportTable.pPanels));
	XmlAnimUrvk=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_urvk",ImportTable.pPanels));
	XmlAnimUvpdAlt=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_uvpd_alt",ImportTable.pPanels));
	XmlAnimUvpdDP=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_uvpd_press",ImportTable.pPanels));

	int tmp=rand()%25;
	m_ChargeAirInSystemMax2Const=3+tmp/10;
	
	tmp=rand()%30;
	m_ChargeAirInSystemMax1Const=5+tmp/10;

}

void SDSystemSrdSkv::SrdUpdate()
{
	if(m_NdlCabinHeight>2500) {
		SND_SET(SND_SRD_ALRAM,1);
	} else {
		SND_SET(SND_SRD_ALRAM,0);
	}

	m_SuperfluousPSI=(m_OuterHeight-m_NdlCabinHeight)/20;

	if(m_SuperfluousPSI>m_PosSuperfluousPSI) {
		m_SuperfluousPSI=m_PosSuperfluousPSI;
	}

	m_SuperfluousPSI=m_SuperfluousPSI*0.07;
}

void SDSystemSrdSkv::Update()
{
	GetData();

	SrdUpdate();

	m_Powered=m_AzsConditioner&&PWR_GET(PWR_BUS27)&&AZS_GET(AZS_AIR_COND_SYS_BUS);

	if(m_AzsSystemChanged) {
		m_SelectionAirReq=m_AzsSystem;
	}

	if(m_AzsSystemEmergChanged) {
		m_SelectionAirReq=m_AzsSystemEmerg;
	}

	if(m_Powered) {
		if(m_SelectionAirReq==1) {
			m_SelectionAirPCT++;
			if(m_SelectionAirPCT>100)
				m_SelectionAirPCT=100;
		} else if(m_SelectionAirReq==2) {
			m_SelectionAirPCT--;
			if(m_SelectionAirPCT<0)
				m_SelectionAirPCT=0;
		}
	} else {
		m_SelectionAirPCT--;
		if(m_SelectionAirPCT<0)
			m_SelectionAirPCT=0;
	}

	if(m_SelectionAirPCT>0)
		m_Status=1;
	else
		m_Status=0;


	if(m_Status) {
		if(m_AzsChargeAirInSystemMode==SKV_MODE_1) {
			m_ChargeAirInSystemMax=m_ChargeAirInSystemMax1Const;
			m_SelectionAirFromEngineMax=m_SelectionAirFromEngineMax1Const;
		} else if(m_AzsChargeAirInSystemMode==SKV_MODE_2) {
			m_ChargeAirInSystemMax=m_ChargeAirInSystemMax2Const;
			m_SelectionAirFromEngineMax=m_SelectionAirFromEngineMax2Const;
		}
		
		if(m_SelectionAirFromEngine>m_SelectionAirFromEngineMax)
			m_SelectionAirFromEngine-=0.0001*m_SelectionAirPCT;
		else if(m_SelectionAirFromEngine<m_SelectionAirFromEngineMax)
			m_SelectionAirFromEngine+=0.0001*m_SelectionAirPCT;
		else
			m_SelectionAirFromEngine=m_SelectionAirFromEngineMax;
		
	} else {
		m_ChargeAirInSystemMax=0;
		m_SelectionAirFromEngineMax=0;
		if(m_SelectionAirFromEngine>m_SelectionAirFromEngineMax)
			m_SelectionAirFromEngine-=0.01;
	}

	if(m_Airstarter->GetAir()>0) {
		if(m_ChargeAirInSystem>m_ChargeAirInSystemMax) 
			m_ChargeAirInSystem-=0.02;
		if(m_ChargeAirInSystem<m_ChargeAirInSystemMax) 
			m_ChargeAirInSystem+=0.02;
		//else
		//	m_ChargeAirInSystem=m_ChargeAirInSystemMax;
	}

	//m_ChargeAirInSystem=LimitValue<double>(m_ChargeAirInSystem,0,10);

	SetData();
}

void SDSystemSrdSkv::Load(CIniFile *ini)
{
}

void SDSystemSrdSkv::Save(CIniFile *ini)
{
}

void SDSystemSrdSkv::GetData()
{
	m_AzsConditioner			= AZS_GET(AZS_AIR_COND_SYS_BUS);
	m_Swt2077Power				= AZS_GET(AZS_2077PWR);
	m_AzsSystem					= AZS_GET(AZS_AIR_PRESS_SYS_MASTER);
	m_AzsSystemChanged			= AZS_CHG(AZS_AIR_PRESS_SYS_MASTER);
	m_AzsSoundAlarm				= AZS_GET(AZS_AIR_PRESS_SYS_SOUND_ALARM);
	m_AzsSystemEmerg			= AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG);
	m_AzsSystemEmergChanged		= AZS_CHG(AZS_AIR_PRESS_SYS_MASTER_EMERG);
	m_AzsDoublerRegPSI			= AZS_GET(AZS_AIR_PRESS_CONTROL_BCKUP);
	m_AzsDumpPSI				= AZS_GET(AZS_AIR_PRESS_DROP_EMERG);
	m_AzsTempAirInInterior		= AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN);
	m_AzsTempAirOutTurboholod	= AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT);
	m_AzsAirToCrew				= AZS_GET(AZS_DECK_COND);
	m_AzsChargeAirInSystem		= AZS_GET(AZS_BLEEDAIR_EXTRACT_VALVE);
	m_AzsChargeAirInSystemMode	= AZS_GET(AZS_BLEEDAIR_EXTRACT_RATIO);
	m_GltTempDuct				= GLT_GET(GLT_TEMP_DUCT);
	m_NdlHermeticSealing		= NDL_GET(NDL_2077PRESSBEGIN);
	m_NdlSuperfluousPSI			= NDL_GET(NDL_2077DIFFPRESS);
	m_NdlSpeedChangePSI			= POS_GET(POS_PRESSRATE);
	m_PosTargetTempDuct			= (int)POS_GET(POS_TARGET_DUCT_TEMP);
	m_PosTargetTempSalon		= (int)POS_GET(POS_TARGET_SALON_TEMP);
	m_OuterHeight				= g_pSDAPI->GetXML(XML_PLANE_ALTITUDE,"meters");
	m_PosSuperfluousPSI			= NDL_GET(NDL_2077DIFFPRESS)*100;
	m_PosSpeedChangePSI			= (double)g_ChangeRate[int(POS_GET(POS_PRESSRATE))];
	m_PosHermeticSealing		= NDL_GET(NDL_2077PRESSBEGIN);
	//m_NdlCabinHeight			= g_pSDAPI->GetXML(XML_PLANE_ALTITUDE,"meters");
}

void SDSystemSrdSkv::SetData()
{
	LMP_SET(LMP_AIR_PRESS_COND_SYS_OFF,!m_Status);
	m_Airstarter->RequestAir(m_SelectionAirFromEngine,AIR_REQUESTER_SKV);
	NDL_SET(NDL_TEMP_SALON,m_NdlSalonTemp);
	NDL_SET(NDL_URVK,m_ChargeAirInSystem);
	NDL_SET(NDL_UVPD_ALT,m_NdlCabinHeight);
	NDL_SET(NDL_UVPD_DIFPRESS,m_SuperfluousPSI);
	NDL_SET(NDL_VS_SRD,m_SpeedChangeHeightInCabin);

	XmlAnimSalonTemp->set_degree(dgrd(m_NdlSalonTemp*1.015));
	XmlAnimDuctTemp->set_degree(dgrd(NDL_GET(NDL_DUCT)*0.8));

	XmlAnimUrvk->set_degree(dgrd(SetUrvkToVC(NDL_GET(NDL_URVK)))); 
	XmlAnimUvpdAlt->set_degree(dgrd(SetUvpdAltToVC(NDL_GET(NDL_UVPD_ALT)))); 
	XmlAnimUvpdDP->set_degree(dgrd(SetUvpdDPToVC(NDL_GET(NDL_UVPD_DIFPRESS)))); 
}

double SDSystemSrdSkv::SetUrvkToVC(double ndl)
{
	double var=ndl;

	if(var<=5)				return var*30;
	if(var> 5&&var<=7)		return 5*30+(var-5)*31;
	if(var> 7)           	return 5*30+(  7-5)*31+(var-7)*25.3;

	return 0;
}

double SDSystemSrdSkv::SetUvpdAltToVC(double ndl)
{
	double var=ndl;

	if(var<=2000)				return var*0.06;
	if(var> 2000&&var<=3000)	return 2000*0.06+(var-2000)*0.055;
	if(var> 3000)           	return 2000*0.06+(3000-2000)*0.055+(var-3000)*0.0585;

	return 0;
}

double SDSystemSrdSkv::SetUvpdDPToVC(double ndl)
{
	double var=ndl;

	if(var<=0.4)			return var*312.5;
	if(var> 0.4)           	return 0.4*312.5+(var-0.4)*337.5;

	return 0;
}

/*
������:

������ ����������� �������� ���������� ������� ����.
����-�� �� ������� ������ ������. ���� ������. ����� ������ ������.
������ ������������� � ��������� (� ������ ���� ���/��� ����).
����� ��������� ������� ������� ��� �����. ��� ��� ������������� �������� � ���� ��� ���������.

�����:

�� ��������� ������� �������� "������ ������������", "���������� ��������" � "�������� ���������".
������ ������������ - �������� ��� ������� "������ � ������" ��������� �� ����������� �������� 
(650 �� �� �� ������������� 1300�). ��� ������������� ��� ���� 650 �� �� ��, ���������� �������� 0.4 
� �������� ��������� 0.18(��� ��� ���� �������� ������ ����� ������� �� ��� � ����� ��������) 
���� ��������� ����������� �� ������ 6000�. ������ 650 ��������� �� ��������� 0.18 � ������ 4000 � 
����������� �� 6000. ��� ���� ��������� ������ ���������� ��������, ������� ��������� 0.4 � 6000. 
����� ���� ����������� �� ������ ����� �������� ���������� �����, �� ��� ����������� � ��������� �������. 
��� ������ �������� �������� ������ ���������. ���� ��� �������� ������ � ������ ���������� ������������� 
����� 6 ���, �� ��� ���������� 0.4 �������� � ������ ������� �� ����� ��������� 
�� ������ 1300 (650 �� �� ��) � ��� �������� ������ ������ (������ � ������ ������). 
���������� �������� ���� � ��� � ������� ������������ ���. � ������ ������������� ��������� 
������ ������ ����� ������� �������� ����������� ��������. ����� ���������� �������� ���������� ���� 0.48 
����������� �������� ������������ (���, ��� ��� ������� �������, ������ ���������� �������������) 
� ����� ����������». ���� ������ � ������ ��������� 3250 � (510 �� �� �� � ������), �� ���������� 
����� ���������������߻ � ����������� ��� �� ��������� �������� ������. ��� ���� �� (510 � ����) �������� 
� ������ �� ����� (�������� �� ������������ ���������) ������������ ��������� � �� �����. ��� �� ������ �����, 
���� �� �������� ��� � ������ ��� ��������� � �������, ������� �������� (����, �����, ����) � �.�. � � ����� 
���� �������� �������� 510 � ���� (���������� � ���������). ������������ �������� ���������� �� ����, 
�������� ��� ��� ��� (������ 28� ���������). ��� ������ � ������������ ���������� �� ��������� � 
������� ��� ����� ���ͻ � ������� ������, ����� �� ������ � �������� ����� ������ � ��������� ���. 
��������������, �������� ��������� ���������� �������� ��������� ��������, �� � �/c (0.15 = 1.5 �/c, 0.30= 4 �/�). 
���� ���������� ������� � ����������� �� ����� ������-�������� (�������� �� ��, ��� ������ ��������� 
�������� 0.18 �� �� ��/���) � �� ���� ������ ���������� �����-�������� (2 �/� ��� ���������� ������� �� �����)


�.�. � ������:

1. �� ����� ������� 750 �� �� �� �� ������.
2. ���������: ��� ���� 650, ���� ��� 0.18 �� ��/��, ��� ���� 0.4 ��/�� �� � ��������.
3. �������� ����� ����� ������ �� ������. ������� ��, ���� ��� ��������, �� ���� ������ �������, 
   ��� ������������� �������� 0.18 �� ��/���.

(��� � ���� ����� �������� �� ������? - ��� ������� ����, ������� � ����� ��� ������ ����� ����������. 
Ambient_Pressure ���������� ��-����� �� �� ������ � �� ������ � ������ �������� ��������, ����� ��������� 
�������, ���� � �� ��������) � ������ ������� ���� ambient_temp, ������� ������ � ������� � ����� 
� ambient_pressure �� ��� ����? ����� ��� ���������� � ���������).

4. ������ � ������ �������� �������� �� ��������� ����������� ���������� (�� ������ ��� ������ ��������� 
   ��� ����������� �������) �� ������ �������� 1300 � (650 �� �� ��). ���� ����������� ������� �������� 
   ������ ������������, �� ������ � ������ ����������� �������������� ������.

� ����� ��� ���� ��� � ������ �������� � ���� �� ����������. ������ ��� ����.
�������, ����� ��� ���� ���� ��� � �����, �� ����������� ������������ �������� �������. ������������ ���� 
��������� ������� ��� ������������ ���������� �������������� � �������� � ������������ ������������ 
(����� �� �������� �� ��� ���������).

������� ��������� ��������� ������� �� ���, ������� �������� �� ������ � ������ (������, ��� ��� ����� 
���������, ���� �� ������ ����������):


1.    ����� �������� (������ ������� ����� 3250 �)
2.    ����� �������� ���������������� (���� � ������ � ���� �� ���) � ������ �����, ������ ��� ����������� 
      ����������� ������� ������ ������ ���������� ����������� ����� � ����������.
3.    ����� ����������� ����� � 1-�� �� 2-� (���� �� ������ ��� ������ ���� 4000)
4.    ����� ��������� 1-� ������ ��� ���������� ��� (�� ����������)
5.    ����� �������� � ���������� ��� (���� ����)
6.    ����� ������ �� ������ (������ ������� ����� 3250 � � ������)
7.    ����� ����� ������ 80% �� ������ ����� 4000� (�� ������ �������� ������� �� ������� ���� ��� ������ ��� � 
      ������ � ������ ����� �����, �������� � ������ ������ ����� ������)
8.    �� �������� ��� ��� ���� ���������� ������� (������ ����������� ���, ��� ��� ������)
9.    ������ �������� (����, �����, ���) ��� ������� ����� ����� ���˻ �� ������ ����� 3250 (������ �������)


����������� ������ �������� 22� � ������ � 11 �� ������������������ � ����������� (Duct) ��� ���������� 
������������ ����� � ����� ����. ��� ����� �� ���������� � ������ ���������� ���� ��� ������.

�� ����� ����� ����������� �������� ������ �� �������� ���������� ��������� � ��������� �������, ������� 
��������� �������� ���������� ������� � ������ (��� ���-�� ���� ����� ���� � ���� ����� �������� ���������) 
(����� �������� ��� �������) � � ������ � ������ ���� �� �������� � �� ��������� � ��� ����� ���� - ��� 
������������ ������ �������� ������ � ������.
*/

/*
Excel:

��� ��� ��-40		��� ����� � ��������� ������ �������				
                    lag	                �������� ������ ��������			
                    IF, AND, OR, ELS	��������� ��������� � ������� ����������� ���������� ����������������			
                    [ ]	                ��������� ���������� ��������			
                    ( )	                ��������� �������			

������ 
����������						
����� 
���������� 
� ��������� 
[ ]			��������											���					������������			
_____________________________________________________________________________________________________________
1			����������� ���� | ���								���					������ ����� ���			
2			�������� | ������� | �������� �����������			�������������		��� 2077 (������ �����)			
3			���������� ��������									����������			��� 2077 (������ �����)			
4			������ ������������									����������			��� 2077 (������ �����)			
5			�������� ��������� ��������							����������			��� 2077 (������ �����)			
6			��� ���� ���� ����� | ���� | ���					���					������ ��			
7			���� ������ ���� | ���								���					������ ��			
8			���� ��� ���� ����� | ���� | ���					���					������ ��			
9			������ ��� ���� ���� ���� | ���						���					������ ��			
10			����� ���� ���� ���� | ���							���					������ ��			
11			���� ���� � ������ ���� | ����� | ����� | �����		���					������ ��			
12			���� ���� �� �������� ���� | ����� | ����� | �����	���					������ ��			
13			������ ���� � ������� ���� | ����� | ���� | ����	���					������ ��			
14			������ ���� � ������� 1 ��� | 2 ���					���					������ ��			
15			���� ������� ������ | ������						���					������ ��			
16			��� ������ | ����� | ������ �������					�������� ������		������ ��			

���������� ����������						

17	�������������� �������								IF ([28V]=1) and ([1]=1), THEN [17]=1, ELS [17]=0				

18	��������� �������� ������ ������� (0-100%)			������� �� [6] ��� [8] ��� ������� [17]=1. 
														��� [6] � [8] ��������� ����� ��� ���� 
														����� - ������� - �������� �����, ����� ����� ������� 
														� �������� (� ����� �������� � ��������� ���). ������ 
														���� �������� �������� 6 ���.				

19	������ ������� ��� ���								IF ([18]=1), THEN ([19]=1), ELS ([19]=0)				

20	����� ������� �� ���������� (0-100%)				IF ([19]=1) and (0<[13]<3) THEN 
    ����: [20] ����� �������������� ���					([20]=[����� ������� bleed_air_psi 1-2-3] 
	������������ ����������� ��������					����������� � ����������� ����, ��� max=100% min=0%) lag 30%/��� 
	����������� ����� ����. �� �������� 
	� �� ������ ���� ��������.							IF ([19]=1) and ([13]=3) THEN ([20] ������ 
														����������� �� �������� �������� �� 0) lag 10%/���
	
														IF ([19]=1) and ([13]=0) THEN ([20]=[20])				

														IF ([19]=0) THEN ([20]=0)				

21	������ ������� � ������� (1-10 �������� ������)		IF ([14]=1), THEN ([21]=[20]*0.08), 
														ELS [21]=[20]*0.05 lag (1 �.�/���)				

22	���������� ��������					

23	�������� �� ����� �������� (�� �� ��)				IF ([19]=1) and ([21]>3) THEN ([23] ���������� �� 
														��������� [5] �� ���������� �������� [4]), 
														ELS ([23] ������������ � ��������� �� ��������� [5]).				

23	������ � ������ (�)					

24	�������� ��������� ������ � ������ (�/�)			����������� �� �������� ��������� [22]				

25						

26														IF [16]=1, THEN [18]=[����� ���� ����� 3-� ����] lag 1 ���, 
														ELS [18]=[���� �� ������] lag 1 ���				

���������						

19	������ � ������ (-400-5000 �)						�������		���� ������ ��			[19]=		
16	���������� �������� �������� (0.0-0.6 ���/�� ��)	�������		��� 2077 (������ �����)	[16]=[3] ���� [1]>0 � [2]>0 ,else [16]=[16]		
17	������ ������������ (430-810 �� �� ��)				�������		��� 2077 (������ �����)	[17]=[4] ���� [1]>0 � [2]>0 ,else [17]=[17]		
18	�������� ��� ���� (0.15-0.30 �� �� ��/���)			���� �����	��� 2077 (������ �����)	[18]=[5] ���� [1]>0 � [2]>0 ,else [18]=[18]		
*/
