/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/Electrosystem.h $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"
#include "../../Lib/EString.h"

#define CONNECT_TIME	15*18

class SDElectricalUnit
{
protected:
	double m_Volts;
	double m_Amps;

public:
	SDElectricalUnit() { m_Volts = 0; m_Amps = 0; };
	SDElectricalUnit(double vlt, double amp) { m_Volts = vlt; m_Amps = amp; };
	inline double GetVolts() const { return m_Volts; };
	inline double GetAmps() const { return m_Amps; };
};

class SDGroundUnit : public SDElectricalUnit
{
private:
	bool m_Connector;
	bool m_RequestConnect;
	bool m_RequestDisconnect;
	int m_Timer;
	bool m_TimerReseted;
	double m_Vltd,m_Ampd;

public:
	SDGroundUnit() {
		m_RequestConnect = false; 
		m_RequestDisconnect = false;  
		m_Connector = false;  
		m_TimerReseted = false; 
	}

	void Connect(double vlt, double amp) {
		m_RequestDisconnect = false; 
		m_RequestConnect = true; 
		m_Timer=0;
		m_TimerReseted = true; 
		m_Vltd = vlt;
		m_Ampd = amp;
	}

	void Disconnect() {
		m_RequestConnect = false; 
		m_RequestDisconnect = true; 
		m_Timer=0;
		m_TimerReseted = true; 
	}

	inline bool IsConnected() const {return m_Connector;}

	virtual void Update() {
		m_Timer++;
		if(m_RequestConnect && !m_Connector && m_TimerReseted) {
			if(m_Timer>CONNECT_TIME) {
				m_RequestConnect = false;
				m_TimerReseted = false;
				m_Connector = true;
				m_Volts=m_Vltd;
				m_Amps=m_Ampd;
			}
		}

		if(m_RequestDisconnect && m_Connector && m_TimerReseted) {
			if(m_Timer>CONNECT_TIME) {
				m_RequestDisconnect = false;
				m_TimerReseted = false;
				m_Connector = false;
				m_Volts = 0;
				m_Amps = 0;
			}
		}
	}
	virtual void Load(CIniFile *ini,EString name) {
		EString buf;
		buf="Cn"+name;
		m_Connector			= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="RC"+name;
		m_RequestConnect	= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="RD"+name;
		m_RequestDisconnect	= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="Tm"+name;
		m_Timer				= ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		buf="TR"+name;
		m_TimerReseted		= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="V"+name;
		m_Vltd				= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		buf="A"+name;
		m_Ampd				= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
	};

	virtual void Save(CIniFile *ini, EString name){
		EString buf;
		buf="Cn"+name;
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Connector		);
		buf="RC"+name;
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_RequestConnect	);
		buf="RD"+name;
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_RequestDisconnect);
		buf="Tm"+name;
		ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Timer			);
		buf="TR"+name;
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_TimerReseted	);
		buf="V"+name;
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Vltd			);
		buf="A"+name;
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Ampd			);
	};
};

class SDGroundSupply
{
private:
	SDGroundUnit m_Shrap27;
	SDGroundUnit m_Shrap115;

public:
	enum SHRAP {
		SHRAP_27V,
		SHRAP_115V,
		SHRAP_MAX
	};

	void RequestConnect(SHRAP shrap) {
		switch(shrap) {
			case SHRAP_27V:
				m_Shrap27.Connect(27, 28.5);
			break;
			case SHRAP_115V:
				m_Shrap115.Connect(115, 0);
			break;
		}
	}

	void RequestDisconnect(SHRAP shrap) {
		switch(shrap) {
			case SHRAP_27V:
				m_Shrap27.Disconnect();
			break;
			case SHRAP_115V:
				m_Shrap115.Disconnect();
			break;
		}
	}

	bool RequestStatus(SHRAP shrap)	{
		bool ret=false;
		switch(shrap) {
			case SHRAP_27V:
				ret=m_Shrap27.IsConnected();
			break;
			case SHRAP_115V:
				ret=m_Shrap115.IsConnected();
			break;
		}
		return ret;
	}

	double RequestVolts(SHRAP shrap) {
		double ret=0;
		switch(shrap) {
			case SHRAP_27V:
				ret=m_Shrap27.GetVolts();
			break;
			case SHRAP_115V:
				ret=m_Shrap115.GetVolts();
			break;
		}
		return ret;
	}

	double RequestAmps(SHRAP shrap) {
		double ret=0;
		switch(shrap) {
			case SHRAP_27V:
				ret=m_Shrap27.GetAmps();
			break;
			case SHRAP_115V:
				ret=m_Shrap115.GetAmps();
			break;
		}
		return ret;
	}

	virtual void Update() {
		m_Shrap27.Update();
		m_Shrap115.Update();
	}

	static SDGroundSupply *Instance() {
		if(!m_Self)
			m_Self=new SDGroundSupply();
		return m_Self;
	};

	static void Release() {
		if(m_Self) {
			delete m_Self;
			m_Self=NULL;
		}
	};

	virtual void Load(CIniFile *ini) {
		m_Shrap27.Load(ini,"27");
		m_Shrap115.Load(ini,"115");
	};

	virtual void Save(CIniFile *ini) {
		m_Shrap27.Save(ini,"27");
		m_Shrap115.Save(ini,"115");
	};

protected:
	static SDGroundSupply *m_Self;

	SDGroundSupply(){};

};

class SDBatteryUnit : public SDElectricalUnit
{
protected:
	bool m_Available;

public:
	SDBatteryUnit() {
		m_Volts=GetBatteryVoltage();
		m_Amps=28.5;
		m_Available=true;
	}
	
	virtual void Update() {
		m_Volts=GetBatteryVoltage();
	}

	virtual bool IsAvailable() const {return m_Available;}

	virtual double RequestVolts() const {
		return m_Volts;
	}
	virtual double RequestAmps() const {
		return m_Amps;
	}

};

class SDBatterys
{
private:
	SDBatteryUnit m_Bat1;
	SDBatteryUnit m_Bat2;

public:
	static SDBatterys *Instance() {
		if(!m_Self)
			m_Self=new SDBatterys();
		return m_Self;
	};

	static void Release() {
		if(m_Self) {
			delete m_Self;
			m_Self=NULL;
		}
	};

	virtual void Update() {
		m_Bat1.Update();
		m_Bat2.Update();
	};

	virtual bool IsAvailable() const {return m_Bat1.IsAvailable()||m_Bat2.IsAvailable();}

	double RequestVolts(int batnum=0) {
		double ret=0;
		switch(batnum) {
			case 0:
				if(m_Bat1.IsAvailable()) 
					ret=m_Bat1.RequestVolts();
				else if(m_Bat2.IsAvailable())
					ret=m_Bat2.RequestVolts();
			break;
			case 1:
				ret=m_Bat1.RequestVolts();
			break;
			case 2:
				ret=m_Bat2.RequestVolts();
			break;
		}

		return ret;
	};

	double RequestAmps(int batnum=0) {
		double ret=0;

		switch(batnum) {
			case 0:
				if(m_Bat1.IsAvailable()) 
					ret=m_Bat1.RequestAmps();
				else if(m_Bat2.IsAvailable())
					ret=m_Bat2.RequestAmps();
			break;
			case 1:
				ret=m_Bat1.RequestAmps();
			break;
			case 2:
				ret=m_Bat2.RequestAmps();
			break;
		}

		return ret;
	};

protected:
	static SDBatterys *m_Self;

	SDBatterys(){};

};

class SDBus27 : public SDElectricalUnit
{
private:
	bool m_Available;
	SDGroundSupply *m_GroundSupply;
	SDBatterys *m_Batterys;

	double m_TotalLoad;

public:
	SDBus27() {
		m_GroundSupply=SDGroundSupply::Instance();
		m_Batterys=SDBatterys::Instance();
		m_TotalLoad=0;
	};

	virtual ~SDBus27() {
		//SDGroundSupply::Release();
		//SDBatterys::Release();
	};

	void CalculateLoad(double load115,double load36);

	virtual void Update();

	inline bool IsAvailable() const { return m_Available;	};

	inline double GetRapVolts27() const { return m_GroundSupply->RequestVolts(SDGroundSupply::SHRAP_27V); };
	inline double GetRapVolts115() const { return m_GroundSupply->RequestVolts(SDGroundSupply::SHRAP_115V); };
	inline double GetBatVolts27(int batnum=0) { 
		double ret=m_Batterys->RequestVolts(batnum); 
		return ret;
	};

	inline double GetRapAmps27() const { return m_GroundSupply->RequestAmps(SDGroundSupply::SHRAP_27V); };
	inline double GetRapAmps115() const { return m_GroundSupply->RequestAmps(SDGroundSupply::SHRAP_115V); };
	inline double GetBatAmps27(int batnum=0) const { 
		double ret=0;

		if(!batnum)
			ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad; 
		if(batnum==1) {
			if(m_Batterys->RequestVolts(2)<1) 
				ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad; 
			else 
				ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad/2; 
		} else if(batnum==2) {
			if(m_Batterys->RequestVolts(1)<1) 
				ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad; 
			else 
				ret=m_Batterys->RequestAmps(batnum)-m_TotalLoad/2; 
		}

		return ret;
	};

	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};

class SDBus115 : public SDElectricalUnit
{
private:
	bool m_Available;
	SDGroundSupply *m_GroundSupply;
	SDBus27 *m_Bus27;
	double m_TotalLoad;

public:
	SDBus115(SDBus27 *b27) {
		m_GroundSupply=SDGroundSupply::Instance();
		m_Available=false;
		m_Volts=115;
		m_Amps=0;
		m_Bus27=b27;
		m_TotalLoad=0;
	};

	virtual ~SDBus115() {
	};

	virtual void Update() {

		// Input
		int p1_inv1_115v       = (int)AZS_GET(AZS_INV1_115V);
		int p1_inv2_115v       = (int)AZS_GET(AZS_INV2_115V); 
		int p8_inv1_115v_bus   = (int)AZS_GET(AZS_INV1_115V_BUS);
		int p8_inv2_115v_bus   = (int)AZS_GET(AZS_INV2_115V_BUS);

		// Output
		int pa_tbl_inv_115v_fail=0;
		int pa_lmp_inv_115v_fail=0;

		// Code
		bool bus115;
		if((((p1_inv1_115v&&p8_inv1_115v_bus)||(p1_inv2_115v&&p8_inv2_115v_bus))&&m_Bus27->IsAvailable())||m_GroundSupply->RequestStatus(SDGroundSupply::SHRAP_115V)) {
			bus115=true;
			m_Volts=115;
		} else {
			bus115=false;
			m_Volts=0;
		}

		if(((p1_inv1_115v&&p8_inv1_115v_bus)&&(p1_inv2_115v&&p8_inv2_115v_bus))&&m_Bus27->IsAvailable()) {
			m_TotalLoad=2.6;
			pa_tbl_inv_115v_fail = 0;
		} else if(((p1_inv1_115v&&p8_inv1_115v_bus)&&(!p1_inv2_115v||!p8_inv2_115v_bus))&&m_Bus27->IsAvailable()) {
			m_TotalLoad=1.3;
			pa_tbl_inv_115v_fail = 1;
		} else if(((!p1_inv1_115v||!p8_inv1_115v_bus)&&(p1_inv2_115v&&p8_inv2_115v_bus))&&m_Bus27->IsAvailable()) {
			m_TotalLoad=1.3;
			pa_tbl_inv_115v_fail = 1;
		} else {
			m_TotalLoad=0;
			pa_tbl_inv_115v_fail = 1;
		}

		pa_lmp_inv_115v_fail = pa_tbl_inv_115v_fail;

		if((m_GroundSupply->RequestStatus(SDGroundSupply::SHRAP_115V)&&AZS_GET(AZS_GROUND_PWR_BUS))||bus115) {
			m_Available=true;
		} else {
			m_Available=false;
		}

		TBL_SET(TBL_INV_115V_FAIL,pa_tbl_inv_115v_fail);
		LMP_SET(LMP_INV_115V_FAIL,pa_lmp_inv_115v_fail);

	};

	inline bool IsAvailable() const { return m_Available;	};

	double GetVolts115(int num) {
		// Input
		int p1_inv1_115v       = (int)AZS_GET(AZS_INV1_115V);
		int p1_inv2_115v       = (int)AZS_GET(AZS_INV2_115V); 
		int p8_inv1_115v_bus   = (int)AZS_GET(AZS_INV1_115V_BUS);
		int p8_inv2_115v_bus   = (int)AZS_GET(AZS_INV2_115V_BUS);
		int p1_powersource     = (int)AZS_GET(AZS_POWERSOURCE);
		double ret=0;

		// Code
		switch(num) {
			case 1:
				if(p1_inv1_115v&&p8_inv1_115v_bus)
					ret=m_Volts;
				else
					ret=0;
			break;
			case 0:
				if(p1_inv2_115v&&p8_inv2_115v_bus)
					ret=m_Volts;
				else
					ret=0;
			break;
			case 2:
				if(/*p1_powersource==POWER_SOURCE_RAP&&*/m_GroundSupply->RequestStatus(SDGroundSupply::SHRAP_115V)) 
					ret=m_Volts;
				else 
					ret=0;
			break;
		}
		return ret;
	}

	double GetTotalLoad() const {return m_TotalLoad;};

	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};

class SDBus36 : public SDElectricalUnit
{
private:
	bool m_Available;
	SDBus27 *m_Bus27;
	double m_TotalLoad;

public:
	SDBus36(SDBus27 *b27) {
		m_Available=false;
		m_Volts=36;
		m_Amps=0;
		m_Bus27=b27;
		m_TotalLoad=0;
	};

	virtual ~SDBus36() {
	};

	virtual void Update() {
		// Input
		int p1_inv1_36v        = (int)AZS_GET(AZS_INV1_36V);
		int p1_inv2_36v        = (int)AZS_GET(AZS_INV2_36V);

		// Output
		bool pa_tbl_inv_36v_fail=false;

		// Code
		bool bus36;
		if((p1_inv1_36v||p1_inv2_36v)&&m_Bus27->IsAvailable()) {
			bus36=true;
			m_Volts=36;
		} else {
			bus36=false;
			m_Volts=0;
		}

		if((p1_inv1_36v&&p1_inv2_36v)&&m_Bus27->IsAvailable()) {
			m_TotalLoad=1.6;
			pa_tbl_inv_36v_fail = 0;
		} else if((p1_inv1_36v&&!p1_inv2_36v)&&m_Bus27->IsAvailable()) {
			m_TotalLoad=0.8;
			pa_tbl_inv_36v_fail = 1;
		} else if((!p1_inv1_36v&&p1_inv2_36v)&&m_Bus27->IsAvailable()) {
			m_TotalLoad=0.8;
			pa_tbl_inv_36v_fail = 1;
		} else {
			m_TotalLoad=0;
			pa_tbl_inv_36v_fail = 1;
		}

		if(bus36) {
			m_Available=true;
		} else {
			m_Available=false;
		}

		TBL_SET(TBL_INV_36V_FAIL,pa_tbl_inv_36v_fail);

	};

	inline bool IsAvailable() const { return m_Available;	};

	double GetVolts36(int num) {
		// Input
		int p1_inv1_36v        = (int)AZS_GET(AZS_INV1_36V);
		int p1_inv2_36v        = (int)AZS_GET(AZS_INV2_36V);
		int p8_capt_adi        = (int)AZS_GET(AZS_CAPT_ADI_BACKUP_BUS);
		int p8_capt_vsi        = (int)AZS_GET(AZS_CAPT_VSI_BUS);

		double ret=0;

		// Code
		switch(num) {
			case 0:
			case 1:
			case 2:
				if(p1_inv2_36v&&m_Available) 
					ret=m_Volts;
				else
					ret=0;
			break;
			case 3:
			case 4:
			case 5:
				if(p8_capt_adi&&m_Available) 
					ret=m_Volts;
				else
					ret=0;
			break;
			case 6:
			case 7:
			case 8:
				if(p1_inv1_36v&&m_Available) 
					ret=m_Volts;
				else
					ret=0;
			break;
			case 9:
			case 10:	
			case 11:
				if(p8_capt_vsi&&m_Available) 
					ret=m_Volts;
				else
					ret=0;
			break;
		}
		return ret;
	}

	double GetTotalLoad() const {return m_TotalLoad;};

	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};

class SDSystemElectro : public SDLogicBase
{
protected:
	SDBus27 m_Bus27;
	SDBus115 m_Bus115;
	SDBus36 m_Bus36;

	auto_ptr<CNamedVar> xml1;
	auto_ptr<CNamedVar> xml2;
	auto_ptr<CNamedVar> xml3;
	auto_ptr<CNamedVar> xml4;
	auto_ptr<CNamedVar> xml5;
	auto_ptr<CNamedVar> xml6;
	auto_ptr<CNamedVar> xml7;

	auto_ptr<CNamedVar> VCAnimAmps27;
	auto_ptr<CNamedVar> VCAnimAmpsGen1;
	auto_ptr<CNamedVar> VCAnimAmpsGen2;
	auto_ptr<CNamedVar> VCAnimAmpsGen3;
	auto_ptr<CNamedVar> VCAnimVolt27;
	auto_ptr<CNamedVar> VCAnimVolt36;
	auto_ptr<CNamedVar> VCAnimVolt115;

	double GetVCVolts36(double ndl);
	double GetVCVolts115(double ndl);

public:
	SDSystemElectro();
	virtual ~SDSystemElectro();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
	
};
