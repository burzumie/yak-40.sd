/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/Fuel.cpp $

  Last modification:
	$Date: 18.02.06 14:24 $
	$Revision: 2 $
	$Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Fuel.h"

/*
�����, �� ������ ��襣� ��᫥����� ���� � ࠧ���� "����" 

1) ���਩��� ��⠭�� �ࠢ��� ���� ࠡ�⠥�, ���� �� ᯮ��. 

2) ��������� ॠ�������� ���������, ⮦� ��� ��直� ᮬ�����. � ����� �ᮬ. ��� ����� ��� ���: 
-------------------------------------------------------------------------------------- 
�� ࠡ����� �������� �����ய�⠭�� ������� ��� �����稢���� ���� (���. 463), � �� �⪠�� ��� ������஢ ࠡ�⠥� ⮫쪮 ���� ����, �� �⮬ ��� ���ᯥ祭�� ��⠭�� ⮯����� �ࠢ��� �����⥫� ����室��� ������ ������ ��४���⥫�� �� �।��� ���� �࠭ ����楢���� ⮯����. 
-------------------------------------------------------------------------------------- 
����� ����筮 � �⮬ ��砥 � ���਩��� ��⠭�� �ࠢ��� ���� �������, ��� ����砥��� � ࠡ�⠥� � ������. � ����� ������ "�࠭ ����楢����", ����� ᮥ����� ��� ��⪨ ⮯������ ������ࠫ�. �� ⮣�� � ������ �ࠢ� �����⥫� ���孥�, �⮡� �⮣� �� �ந��諮 � ������ �������� �� �ᥬ� ��室���� ���뢠�� "�࠭ ��ꥤ������" ���ᮭ��, �� ᮢᥬ ����易⥫쭮 ��� ࠡ��� �ࠢ��� �����⥫�. ��㣮� ����, �� � �⮩ ���樨 ⮯���� �� �ࠢ��� ���ᮭ� ��ࠡ��뢠���� �� �㤥� � �⮡� �� ����稫�� ��ᡠ���� � �७� ��ࠢ�, ��� ४������� �⪫���� ��� � ������ "�࠭ ��ꥤ������" ���ᮭ�� ��� �����ঠ��� �ਬ�୮ ����������� �஢�� ⮯���� ᫥�� � �ࠢ�. 
�� ��� �����, ���� ⠪: 
�� �⪠�� 3� ������஢ ���� ⮯����� ���� ��⮬���᪨ ���室�� �� ��⠭�� �� ���⮢�� �������஢, �ࠢ� ᮮ⢥��⢥��� �⪫�砥���. ���⮬� �� ��� ���� ���਩�� ����砥� (ॠ��������), ���� ���뢠�� ������ �࠭ ����楢����, ����� ᮥ����� ��� ��⪨ ⮯������ ������ࠫ� (�� �� �࠭ ��ꥤ������ ���ᮭ��). � � ⮬ � � ��㣮� ��砥 �ࠢ� �����⥫� ࠡ�⠥�. ����� �⪫�砥� ���. �᫨ � ���쭥�襬 �� �� ���뢠�� �࠭ ��ꥤ������ ���ᮭ��, � ⮯���� ��ࠡ��뢠���� ⮫쪮 �� ������ ���ᮭ� � ᮧ������ ��ᡠ���� � �祢���묨 ��᫥��⢨ﬨ. �ࠢ�� �� ��������� ������ �� 㫥���, �� �� ��. ���� ���� 6000 � (����� �������� ࠡ��� �����⥫�� �� "ᠬ�⥪�") ᮣ��᭮ ��� ����� � ��� ���ᮢ �����, �᫨ �ᮡ� �� ������஢���. 

3) ����� (F11), �ࠢ�� (F12) � ������ (.) �����ﬨ �ମ��� ��ଠ�쭮, �᫨ � 2D. 
����� ���室��� � 3D, ���� �ମ� � ��� �����६���� ࠡ���� ⮦� �����⥫쭮, �� ࠡ�⠥� ⮫쪮 �ࠢ� �� ����⨨ (F12). 

4) � ���᭮� �⠭樥� � ����: � ��� � ������ �� ��� ��������� ��४���⥫� "�몫�祭�", ���� ⮫쪮 "����祭�" � "��⮬��". �ࠢ��쭮? ����� � ��宦� � ColdDarkCocpit �ࠧ� �� ��᫥ ����㧪� ������, � ����稢 ���� ���� ��������� � ����� ���ᨬ��쭮� �������� � ���਩��� ��. �� ���� ����砥��� � ������ �������� ����筮� �������� � ���਩��� �� 类�� �� �।��饣� �����. 
-------------------------------------------------------------------------------------- 
� ������ ������, � �ਭ樯� ���������� ��ࠢ��� �������� �� ���਩��� ��, ����� �ᮡ�����⥩ ������஢���� �����யਢ����� ���᭮� �⠭樨 � ��砫��� �᫮��� �� ����㧪� ������ � ���. ��� ��� ��� �����ய�⠭�� �������� � ���਩��� �� ������� � ����稨 㦥 �� 㬮�砭��. �� ����� �����ய�⠭�� �� ������ �⠭��, ����� �⮨� ��� �����窮� � ०��� "��⮬��", �������� ॣ㫨����� ��⮬���᪨, � ᠬ� �� ����砥��� �� ����⨨ �।��࠭�⥫��� ���襪 ���਩��� ��४���⥫�� ����뫪�� ��� ��� ��� 
�� ����⨨ ��४���⥫� ���਩���� �ࠢ����� �⠡������஬. �� ��������� ��४���⥫� �� � ��������� "����祭�", �������� �㤥� � �������. ���? 

� ��� �� ⠪�� 䨣�� ����⨫: ���� ������� ���譥� 誠�� ⮯������� 2*100 �� - ��� �㬬�୮�� ���-�� ⮯���� = ���+�ࠢ 
���� ������� ����७��� 誠�� ⮯������� ��� ���-�� ⮯���� �� ���ᮭ�� ⮦� 2*100 ��, ��� ������ ���� 1*100 ��. ����� ��ࠧ��, �� ���ࠢ�� 1500+1500=3000 ��५�� ⮯������� ����� �� 3000 �� �஢�થ �� ���ᮭ�� �� �⮨� �� ���� ��� ������ ����, � �室�� ����� �� ���祭�� �� ����७��� 誠�� 750 ���� � 750 �ࠢ�. ����� �஢����. � ���� ������ ⠪ � �ந�室��. ��� � ��� ⮯�������� ����� �㬬�, ����, �ࠢ� ������� ⮫쪮 �� ���譥� 誠��. ����� ������⭮ ��祬 �㦭� ����७���. 

�����, ������� �᫨ � ��� "�����", �� � ��쥧�� �ᥩ ��让 �� �஥�� �����.
*/

SDSystemFuel::SDSystemFuel()
{
}

SDSystemFuel::~SDSystemFuel()
{
}

void SDSystemFuel::Init()
{
	m_Tick=0;
	m_ACTPlayed=false;
}

void SDSystemFuel::Update()
{
	GetAllParams();

	ACTCheck();
	PumpsCheck();
	Work();

	SetAllParams();
}

void SDSystemFuel::Load(CIniFile *ini)
{
}

void SDSystemFuel::Save(CIniFile *ini)
{
}

void SDSystemFuel::GetAllParams()
{
	m_Bus27				= (int)PWR_GET(PWR_BUS27);
	m_Generators[0]		= g_pSDAPI->GetN(GENERATOR_ALTERNATOR_1_BUS_AMPS);
	m_Generators[1]		= g_pSDAPI->GetN(GENERATOR_ALTERNATOR_2_BUS_AMPS);
	m_Generators[2]		= g_pSDAPI->GetN(GENERATOR_ALTERNATOR_3_BUS_AMPS);

	m_aACT				= (int)AZS_GET(AZS_ACT);
	m_aACTManual		= (int)AZS_GET(AZS_FUEL_PUMP_LO);

	m_aLeftPump			= (int)AZS_GET(AZS_TOPL_LEV);
	m_aRightPump		= (int)AZS_GET(AZS_TOPL_PRAV);
	m_aRightPumpEmerge	= (int)AZS_GET(AZS_TOPL_PRAV_AVAR);

	m_aXFeedTank		= (int)AZS_GET(AZS_XFEED_TANKS);
	m_aXFeedPump		= (int)AZS_GET(AZS_XFEED_PUMPS);

	m_LeftTankQuantityGl= g_pSDAPI->GetN(FUEL_QUANTITY_LEFT);
	m_RightTankQuantityGl=g_pSDAPI->GetN(FUEL_QUANTITY_RIGHT);

	m_LeftTankQuantityKg= g_pSDAPI->GetN(FUEL_TANK_LEFT_MAIN_LEVEL)/FUEL_LEVEL_PCT*20;
	m_RightTankQuantityKg=g_pSDAPI->GetN(FUEL_TANK_RIGHT_MAIN_LEVEL)/FUEL_LEVEL_PCT*20;

}

void SDSystemFuel::SetAllParams()
{
}

void SDSystemFuel::ACTCheck()
{
	if(m_aACT) {
		m_bACT=ACT_AUTO;
	} else {
		if(m_aACTManual==1)
			m_bACT=ACT_RIGHT;
		else if(m_aACTManual==2)
			m_bACT=ACT_LEFT;
		else
			m_bACT=ACT_OFF;
	}

}

void SDSystemFuel::PumpsCheck()
{
	if(m_aLeftPump&&m_Bus27)
		m_bLeftPump=1;
	else
		m_bLeftPump=0;

	double genSum=m_Generators[0]+m_Generators[1]+m_Generators[2];

	if(m_aRightPump) {
		if(genSum>0||PWR_GET(PWR_RAP_AVAIL)) {
			m_bRightPump=1;
		} else {
			if((m_aRightPumpEmerge==2||m_aRightPumpEmerge==3)&&m_Bus27)
				m_bRightPump=1;
			else {
				// �������� ������� ����� �������� ��������. ���� ������ ������ 10 ������, 
				// �� �� ������ ������ ������ ��� ���������� ������ ������
				if(POS_GET(POS_TIME_AFTER_LOAD)>360)
					m_bRightPump=0;
				else
					m_bRightPump=1;
			}
		}
	} else {
		if((m_aRightPumpEmerge==2||m_aRightPumpEmerge==3)&&m_Bus27)
			m_bRightPump=1;
		else
			m_bRightPump=0;
	}

	LMP_SET(LMP_TOPL_LEV,m_bLeftPump);
	LMP_SET(LMP_TOPL_PRAV,m_bRightPump);
	PWR_SET(PWR_LEFT_PUMP,m_bLeftPump);
	PWR_SET(PWR_RIGHT_PUMP,m_bRightPump);

	if((m_aXFeedPump==2||m_aXFeedPump==3)&&AZS_GET(AZS_FUEL_PUMP_AUTO_BACKUP_BUS))
		m_bXFeedPump=1;
	else
		m_bXFeedPump=0;

	if((m_aXFeedTank==2||m_aXFeedTank==3)&&(AZS_GET(AZS_FUEL_X_FEED8_BUS)||AZS_GET(AZS_FUEL_X_FEED9_BUS)))
		m_bXFeedTank=1;
	else
		m_bXFeedTank=0;

	LMP_SET(LMP_KOLTSEV,m_bXFeedPump);
	LMP_SET(LMP_OBYED,m_bXFeedTank);
}

void SDSystemFuel::Work()
{
	//
	// ���� ������ ������ �������� ���, �� ��������� �������� �� ������ � �� ��������� �� �������
	// ������� 250 ��.
	//
	if(BTN_CHG(BTN_ACT_TEST)) {
		if(BTN_GET(BTN_ACT_TEST)) {
			if(m_bLeftPump&&m_bRightPump&&fabs(m_RightTankQuantityKg-m_LeftTankQuantityKg)<250) {
				LMP_SET(LMP_ACT_TEST_OK,1);
				LMP_SET(LMP_ACT_TEST_FAIL,0);
			} else {
				LMP_SET(LMP_ACT_TEST_OK,0);
				LMP_SET(LMP_ACT_TEST_FAIL,1);
			}
		} else {
			LMP_SET(LMP_ACT_TEST_OK,0);
			LMP_SET(LMP_ACT_TEST_FAIL,0);
		}
	}

	// �������� ��� ������
	if(m_bLeftPump&&m_bRightPump) {
		if(m_bACT==ACT_OFF) {		
			// ��� ���������. �������� � ������� ������. ����� ��������� � ������ ����
			// ������� � �����. ����� � ������� ����
			m_ACTPlayed=false;
			ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_LEFT,0);
			ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_ALL,0);
			ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_RIGHT,0);
			LMP_SET(LMP_ACT_TEST_OK,0);
		} else if(m_bACT==ACT_LEFT) {
			// ��� � ������ ������ � ����������� ������ ����.
			// ������ ������ ��� ��������� � ������� �����, ��������� � ������ �� ������ ���
			m_ACTPlayed=false;
			m_Tick=++m_Tick%6;
			if(!m_Tick) {
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_LEFT,0);
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_ALL,0);
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_RIGHT,0);
			} else {
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_RIGHT,0);
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_RIGHT,0);
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_RIGHT,0);
			}
		} else if(m_bACT==ACT_RIGHT) {
			// ��� � ������ ������ � ����������� ������� ����.
			// ������ ������ ��� ��������� � ������� �����, ��������� � ������ �� ����� ���
			m_ACTPlayed=false;
			m_Tick=++m_Tick%6;
			if(!m_Tick) {
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_LEFT,0);
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_ALL,0);
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_RIGHT,0);
			} else {
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_LEFT,0);
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_LEFT,0);
				ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_LEFT,0);
			}
		} else if(m_bACT==ACT_AUTO) {
			// ��� � ��������. 
			if(m_bLeftPump&&m_bRightPump&&fabs(m_RightTankQuantityKg-m_LeftTankQuantityKg)<250) {
				if(!m_ACTPlayed) {
					SND_SET(SND_ACT_ON,1);
					m_ACTPlayed=true;
				}
				LMP_SET(LMP_ACT_TEST_OK,1);
				LMP_SET(LMP_ACT_TEST_FAIL,0);
				m_Tick=++m_Tick%6;
				if(m_LeftTankQuantityKg<m_RightTankQuantityKg) {
					if(!m_Tick) {
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_LEFT,0);
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_ALL,0);
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_RIGHT,0);
					} else {
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_RIGHT,0);
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_RIGHT,0);
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_RIGHT,0);
					}
				} else if(m_LeftTankQuantityKg>m_RightTankQuantityKg) {
					if(!m_Tick) {
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_LEFT,0);
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_ALL,0);
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_RIGHT,0);
					} else {
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_LEFT,0);
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_LEFT,0);
						ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_LEFT,0);
					}
				} else {
					ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_LEFT,0);
					ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_ALL,0);
					ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_RIGHT,0);
				}
			} else {
				m_ACTPlayed=false;
				LMP_SET(LMP_ACT_TEST_OK,0);
				LMP_SET(LMP_ACT_TEST_FAIL,1);
			}
		}
	} else if(m_bLeftPump&&!m_bRightPump) {
		m_ACTPlayed=false;
		ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_LEFT,0);
		ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_LEFT,0);

		if(m_bXFeedPump/*&&m_bXFeedTank*/)
			ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_LEFT,0);
		else
			ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_OFF,0);

	} else if(!m_bLeftPump&&m_bRightPump) {
		m_ACTPlayed=false;

		if(m_bXFeedPump/*&&m_bXFeedTank*/)
			ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_RIGHT,0);
		else
			ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_OFF,0);

		ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_RIGHT,0);
		ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_RIGHT,0);
	} else {
		m_ACTPlayed=false;
		ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_OFF,0);
		ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_2_OFF,0);
		ImportTable.pPanels->trigger_key_event(KEY_FUEL_SELECTOR_3_OFF,0);
	}
}

