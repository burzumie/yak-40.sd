/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "SrdSkv.h"

SDSystemSrdSkv::SDSystemSrdSkv()
{
}

SDSystemSrdSkv::~SDSystemSrdSkv()
{
}

void SDSystemSrdSkv::Init()
{
	NDL_SET(NDL_2077PRESSBEGIN,650);
	NDL_SET(NDL_2077DIFFPRESS,0.4);
	POS_SET(POS_PRESSRATE,9);

	POS_SET(POS_TARGET_DUCT_TEMP,10);
	POS_SET(POS_TARGET_SALON_TEMP,22);

	m_Airstarter				= CAirStarter::Instance();

	XmlAnimSalonTemp			= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_temp_salon",ImportTable.pPanels));
	XmlAnimDuctTemp				= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_temp_duct",ImportTable.pPanels));
	XmlAnimUrvk					= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_urvk",ImportTable.pPanels));
	XmlAnimUvpdAlt				= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_uvpd_alt",ImportTable.pPanels));
	XmlAnimUvpdDP				= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_uvpd_press",ImportTable.pPanels));

	m_CurrentCabinPressuration	= g_pSDAPI->GetXML(XML_AMBIENT_PRESSURE,"mbar")*0.75;
	m_CurrentCabinAlt			= g_pSDAPI->GetXML(XML_PLANE_ALTITUDE,"meters");
}

void SDSystemSrdSkv::Update()
{
	GetData();

	SetNeedlesToVC();
}

void SDSystemSrdSkv::Load(CIniFile *ini)
{
}

void SDSystemSrdSkv::Save(CIniFile *ini)
{
}

double SDSystemSrdSkv::SetUrvkToVC(double ndl)
{
	double var=ndl;
	if(var<=5)				return var*30;
	if(var> 5&&var<=7)		return 5*30+(var-5)*31;
	if(var> 7)           	return 5*30+(  7-5)*31+(var-7)*25.3;
	return 0;
}

double SDSystemSrdSkv::SetUvpdAltToVC(double ndl)
{
	double var=ndl;
	if(var<=2000)			return var*0.06;
	if(var> 2000&&var<=3000)return 2000*0.06+(var-2000)*0.055;
	if(var> 3000)           return 2000*0.06+(3000-2000)*0.055+(var-3000)*0.0585;
	return 0;
}

double SDSystemSrdSkv::SetUvpdDPToVC(double ndl)
{
	double var=ndl;
	if(var<=0.4)			return var*312.5;
	if(var> 0.4)           	return 0.4*312.5+(var-0.4)*337.5;
	return 0;
}

void SDSystemSrdSkv::SetNeedlesToVC()
{
	//XmlAnimSalonTemp->set_degree(dgrd(m_NdlSalonTemp*1.015));
	XmlAnimDuctTemp->set_degree(dgrd(NDL_GET(NDL_DUCT)*0.8));
	XmlAnimUrvk->set_degree(dgrd(SetUrvkToVC(NDL_GET(NDL_URVK)))); 
	XmlAnimUvpdAlt->set_degree(dgrd(SetUvpdAltToVC(NDL_GET(NDL_UVPD_ALT)))); 
	XmlAnimUvpdDP->set_degree(dgrd(SetUvpdDPToVC(NDL_GET(NDL_UVPD_DIFPRESS)))); 
}

void SDSystemSrdSkv::GetData()
{
	int g_ChangeRate[]	= {15,15,16,16,16,17,17,17,18,18,18,19,19,19,20,20,20,21,21,21,22,22,22,23,23,23,24,24,24,25,25,25,26,26,26,27,27,27,28,28,28,29,29,29,30,30};
	m_BeginPressuration	= NDL_GET(NDL_2077PRESSBEGIN);
	m_SuperfluousPSI	= NDL_GET(NDL_2077DIFFPRESS);
	m_ChangeRatePSI		= (double)g_ChangeRate[int(POS_GET(POS_PRESSRATE))];
}

