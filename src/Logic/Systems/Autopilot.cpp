/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/Autopilot.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Autopilot.h"

void SDSystemAutopilot::OnTimer(unsigned int uID)
{
	if(POS_GET(POS_SIM_PAUSED))
		return;

	GetTimerDeltaStart();
	GetData();

	if(!m_HandMode&&m_Powered) {
		if(HND_GET(HND_AP_VER)==2&&POS_GET(POS_AP_MOUSE_HND_PRESS)==1) {
			DecPitch(0.01);
		} else if(HND_GET(HND_AP_VER)==2&&POS_GET(POS_AP_MOUSE_HND_PRESS)==2) {
			DecPitch(0.02);
		} else if(HND_GET(HND_AP_VER)==1&&POS_GET(POS_AP_MOUSE_HND_PRESS)==1) {
			IncPitch(0.01);
		} else if(HND_GET(HND_AP_VER)==1&&POS_GET(POS_AP_MOUSE_HND_PRESS)==2) {
			IncPitch(0.02);
		}
		BankHandleMode();
		PitchHandleMode();
		AltitudeMode();
		AutoHtail();
	} else if(m_HandMode) {
		AZS_SET(AZS_AP_ALT_HOLD,0);
		m_AltHoldMode=false;
		m_AltitudeToHoldSet=false;
		//pitchhandle=pitch_cur;
		if(m_PitchMode) {
			m_PitchHandle=m_Pitch[APCUR];
		}
	}

}

SDSystemAutopilot::SDSystemAutopilot()
{
}

SDSystemAutopilot::~SDSystemAutopilot()
{
}

void SDSystemAutopilot::Init()
{
	VCAnimBank=auto_ptr<CNamedVar>(new CNamedVar("sd6015_aphandle_bank",ImportTable.pPanels));
	VCAnimPitch=auto_ptr<CNamedVar>(new CNamedVar("sd6015_aphandle_pitch",ImportTable.pPanels));

	m_Htail=CHtail::Instance();
	m_Timer=0;
	m_Powered=false;
	m_TimerReseted=false;
	m_Ready=false;
	m_HandMode=false;
	m_BankMode=false;
	m_PitchMode=false;
	m_AltHoldMode=false;
	m_PitchHandle=0;
	m_AutoCoordSet=false;
	m_AltitudeToHoldSet=false;
	ImportTable.pSIM1->GetSimDataP(m_pSimData,&m_OldAutoCoord,SIMDATA_AUTO_COORD);
	for(int i=0;i<APMAX;i++) {
		m_Bank[i]=0;
		m_Pitch[i]=0;
		m_VS[i]=0;
		m_AltHold[i]=0;
	}
	m_ApOnPlayed=false;
	SetTimer(0,20);
}

void SDSystemAutopilot::Update()
{
	VCAnimBank->set_degree(dgrd(HND_GET(HND_AP_HOR_CORRECTED)*6.7));
	VCAnimPitch->set_degree(dgrd(HND_GET(HND_AP_VER)==1?6:HND_GET(HND_AP_VER)==2?-6:0));

	m_Timer++;

	POS_SET(POS_AP_PITCH_MODE,	m_PitchMode);
	POS_SET(POS_AP_BANK_MODE,	m_BankMode);
	POS_SET(POS_AP_HAND_MODE,	m_HandMode);

	if(AZS_CHG(AZS_AP_PWR))
		On();

	if(m_BankMode) {
		int tmp=1;
		if(!m_AutoCoordSet) {
			ImportTable.pSIM1->SetSimDataP(m_pSimData,&tmp,SIMDATA_AUTO_COORD);
			m_AutoCoordSet=true;
		}
	} else {
		if(m_AutoCoordSet) {
			ImportTable.pSIM1->SetSimDataP(m_pSimData,&m_OldAutoCoord,SIMDATA_AUTO_COORD);
			m_AutoCoordSet=false;
		}
	}

	if(m_TimerReseted) {
		if(m_Timer>TIME_TO_POWER) {
			m_Powered=true;
			m_TimerReseted=false;
		}
	}

/*
	GetTimerDeltaStart();
	GetData();
*/

	if(!PWR_GET(PWR_AP)||!m_Powered||BTN_GET(BTN_APOFF2)||BTN_GET(BTN_APOFF3)) {
		AZS_SET(AZS_AP_CMD,0);
		AZS_SET(AZS_AP_ALT_HOLD,0);
		LMP_SET(LMP_AP_RDY,0);
		m_BankMode=false;
		m_PitchMode=false;
		m_AltHoldMode=false;
		m_AltitudeToHoldSet=false;
		m_Powered=false;
		m_Ready=false;
		m_HandMode=false;
		return;
	}

	if(!AZS_GET(AZS_AP_PITCH_HOLD)) {
		m_PitchMode=false;
		m_AltHoldMode=false;
		m_AltitudeToHoldSet=false;
		AZS_SET(AZS_AP_ALT_HOLD,0);
	}

	if(POS_GET(POS_BRAKE_TRIGGER))
		m_HandMode=true;
	else
		m_HandMode=false;

	if(m_Powered&&!m_Ready)
		LMP_SET(LMP_AP_RDY,1);

	if(AZS_GET(AZS_AP_CMD)&&m_Powered) {
		LMP_SET(LMP_AP_RDY,0);
		m_Ready=true;
		if(!m_ApOnPlayed) {
			SND_SET(SND_AUTOPILOT_ON,1);
			m_ApOnPlayed=true;
		}
	}

	if(m_HandMode&&m_Powered&&m_Ready) {
		LMP_SET(LMP_AP_RDY,1);
		AZS_SET(AZS_AP_CMD,0);
	} else if(!m_HandMode&&m_Powered&&m_Ready) {
		LMP_SET(LMP_AP_RDY,0);
		AZS_SET(AZS_AP_CMD,1);
	}

	if(m_Powered&&m_Ready) 
		m_BankMode=true;
	else
		m_BankMode=false;

	if(m_Powered&&m_Ready&&AZS_GET(AZS_AP_PITCH_HOLD)) {
		m_PitchMode=true;
	} else {
		m_PitchMode=false;
		m_PitchHandle=m_Pitch[APCUR];
	}

	if(m_Powered&&m_Ready&&AZS_GET(AZS_AP_PITCH_HOLD)&&AZS_GET(AZS_AP_ALT_HOLD)) {
		if(!m_AltitudeToHoldSet) {
			double tmp;
			if(g_pSDAPI->GetN(DISPLAY_UNITS)==ENGLISH_UNITS||g_pSDAPI->GetN(DISPLAY_UNITS)==METRIC_UNITS_ALT_FEET)
				tmp=FEET_TO_METER(g_pSDAPI->GetN(ALT_FROM_BAROMETRIC_PRESSURE));
			else
				tmp=g_pSDAPI->GetN(ALT_FROM_BAROMETRIC_PRESSURE);
			m_AltitudeToHold=tmp;
			m_AltitudeToHoldSet=true;
		}
		m_AltHoldMode=true;
	} else {
		m_AltitudeToHoldSet=false;
		m_AltHoldMode=false;
	}

/*
	if(!m_HandMode&&m_Powered) {
		if(HND_GET(HND_AP_VER)==2&&POS_GET(POS_AP_MOUSE_HND_PRESS)==1) {
			DecPitch(0.01);
		} else if(HND_GET(HND_AP_VER)==2&&POS_GET(POS_AP_MOUSE_HND_PRESS)==2) {
			DecPitch(0.02);
		} else if(HND_GET(HND_AP_VER)==1&&POS_GET(POS_AP_MOUSE_HND_PRESS)==1) {
			IncPitch(0.01);
		} else if(HND_GET(HND_AP_VER)==1&&POS_GET(POS_AP_MOUSE_HND_PRESS)==2) {
			IncPitch(0.02);
		}
		BankHandleMode();
		PitchHandleMode();
		AltitudeMode();
		AutoHtail();
	} else if(m_HandMode) {
		AZS_SET(AZS_AP_ALT_HOLD,0);
		m_AltHoldMode=false;
		m_AltitudeToHoldSet=false;
		//pitchhandle=pitch_cur;
		if(m_PitchMode) {
			m_PitchHandle=m_Pitch[APCUR];
		}
	}
*/
}

void SDSystemAutopilot::GetData()
{
	m_Pitch[APCUR]=g_pSDAPI->GetN(PLANE_PITCH_DEGREES);
	if(m_Pitch[APCUR]!=m_Pitch[APOLD])
		m_Pitch[APDIF]=2.5*(m_Pitch[APCUR]-m_Pitch[APOLD])/m_TimeDelta;
	m_Pitch[APDIF]=LimitValue(m_Pitch[APDIF],-15.,15.);
	m_Pitch[APOLD]=m_Pitch[APCUR];

	m_Bank[APCUR]=g_pSDAPI->GetN(PLANE_BANK_DEGREES);
	if(m_Bank[APCUR]>180)
		m_Bank[APCUR]-=360;
	if(m_Bank[APCUR]!=m_Bank[APOLD])
		m_Bank[APDIF]=2.5*(m_Bank[APCUR]-m_Bank[APOLD])/m_TimeDelta;
	m_Bank[APDIF]=LimitValue(m_Bank[APDIF],-50.,50.);
	m_Bank[APOLD]=m_Bank[APCUR];

	m_VS[APCUR]=g_pSDAPI->GetN(VERTICAL_VELOCITY)*0.3048;
	if(m_VS[APCUR]!=m_VS[APOLD])
		m_VS[APDIF]=10*(m_VS[APCUR]-m_VS[APOLD])/m_TimeDelta;
	m_VS[APDIF]=LimitValue(m_VS[APDIF],-5.,5.);
	m_VS[APOLD]=m_VS[APCUR];
}

void SDSystemAutopilot::GetTimerDeltaStart()
{
	QueryPerformanceFrequency(&m_TimeFreq);
	QueryPerformanceCounter(&m_TimeStart);
	m_TimeDelta=(double)(m_TimeStart.QuadPart-m_TimeEnd.QuadPart)/m_TimeFreq.QuadPart;
	if(m_TimeDelta>1) //0.55)
		m_TimeDelta=0.055;
	m_TimeEnd=m_TimeStart;
}

void SDSystemAutopilot::GetTimerDeltaEnd()
{
	QueryPerformanceCounter(&m_TimeEnd);
}

void SDSystemAutopilot::BankPlane(double val)
{
	double deg=LimitValue(val,-g_pSDAPI->m_Config.m_CfgAutopilot.m_dBankLimit,g_pSDAPI->m_Config.m_CfgAutopilot.m_dBankLimit);

	double otkl=m_Bank[APCUR]-deg;
	m_Bank[APINT]=m_Bank[APINT]+g_pSDAPI->m_Config.m_CfgAutopilot.m_dBankCoeffI*otkl*m_Ready*!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)/1000;
	m_Bank[APINT]=LimitValue(m_Bank[APINT],-3.,3.);
	m_Bank[APINT]=m_Bank[APINT]*m_Ready*!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND);

	if(m_Ready) {
		double elupr=1-g_pSDAPI->m_Config.m_CfgAutopilot.m_dBankCoeffP*(otkl+m_Bank[APINT])-g_pSDAPI->m_Config.m_CfgAutopilot.m_dBankCoeffD*m_Bank[APDIF];
		elupr=LimitValue(elupr,-g_pSDAPI->m_Config.m_CfgAutopilot.m_dAileronLimitPCT*163.84,g_pSDAPI->m_Config.m_CfgAutopilot.m_dAileronLimitPCT*163.84);
		ImportTable.pPanels->send_key_event(KEY_AILERON_SET,(UINT32)elupr);
	}
}

void SDSystemAutopilot::PitchPlane(double val)
{
	double pitch_diff=m_Pitch[APCUR]-val;

	m_Pitch[APINT]=m_Pitch[APINT]+g_pSDAPI->m_Config.m_CfgAutopilot.m_dPitchCoeffI*AZS_GET(AZS_AP_PITCH_HOLD)*pitch_diff*0.3;
	m_Pitch[APINT]=LimitValue(m_Pitch[APINT],-8000.,8000.);
	m_Pitch[APINT]=m_Pitch[APINT]*AZS_GET(AZS_AP_PITCH_HOLD)*!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND);

	if(AZS_GET(AZS_AP_PITCH_HOLD)) {
		double res=1*(1-100*g_pSDAPI->m_Config.m_CfgAutopilot.m_dPitchCoeffP*pitch_diff-m_Pitch[APINT]-g_pSDAPI->m_Config.m_CfgAutopilot.m_dPitchCoeffD*m_Pitch[APDIF]);
		res=LimitValue(res,-g_pSDAPI->m_Config.m_CfgAutopilot.m_dElevatorLimitPCT*163.84,g_pSDAPI->m_Config.m_CfgAutopilot.m_dElevatorLimitPCT*163.84);
		ImportTable.pPanels->send_key_event(KEY_ELEVATOR_SET,(UINT32)res);
	}
}

void SDSystemAutopilot::AutoHtail()
{
	if(m_PitchMode||m_AltHoldMode) {
		double elev=rddg(g_pSDAPI->GetN(ELEVATOR_DEFLECTION));
		if(elev>1.0) {
			m_Htail->Dec(0,0.002);
		}
		if(elev<-1.0) {
			m_Htail->Inc(0,0.002);
		}
	}
}

void SDSystemAutopilot::On()
{
	if(!AZS_GET(AZS_AP_PWR)) {
		m_Powered=false;
		m_TimerReseted=false;
		SND_SET(SND_AUTOPILOT_OFF,1);
		m_ApOnPlayed=false;
		return;
	}
	if(!m_Powered&&!m_TimerReseted) {
		if(!m_TimerReseted) {
			m_Timer=0;
			m_TimerReseted=true;
		}
	} else {
		m_TimerReseted=false;
	}
}

void SDSystemAutopilot::BankHandleMode()
{
	if(m_BankMode) {
		BankPlane(HND_GET(HND_AP_HOR_CORRECTED)*2.5*-1);
	}
}

void SDSystemAutopilot::PitchHandleMode()
{
	if(m_PitchMode&&!m_AltHoldMode) {
		PitchPlane(m_PitchHandle);
	} else {
		m_PitchHandle=m_Pitch[APCUR];
	}
}

void SDSystemAutopilot::AltitudeMode()
{
	double tmp;
	if(g_pSDAPI->GetN(DISPLAY_UNITS)==ENGLISH_UNITS||g_pSDAPI->GetN(DISPLAY_UNITS)==METRIC_UNITS_ALT_FEET)
		tmp=FEET_TO_METER(g_pSDAPI->GetN(ALT_FROM_BAROMETRIC_PRESSURE));
	else
		tmp=g_pSDAPI->GetN(ALT_FROM_BAROMETRIC_PRESSURE);

	double altdif=tmp-m_AltitudeToHold;
	double vszad=-0.001*g_pSDAPI->m_Config.m_CfgAutopilot.m_dAltHoldCoeffP*altdif;
	vszad=LimitValue(vszad,-15.,5.);
	double vsdif=m_VS[APCUR]-vszad;
	vsdif=LimitValue(vsdif,-2.,2.);
	double vsintegr=0.000055*vsdif;
	if(m_PitchMode&&m_AltHoldMode) {
		m_AltHold[APINT]+=g_pSDAPI->m_Config.m_CfgAutopilot.m_dAltHoldCoeffI*vsintegr+0.000005*m_VS[APDIF]*g_pSDAPI->m_Config.m_CfgAutopilot.m_dAltHoldCoeffD;
		PitchPlane(m_AltHold[APINT]);			
		m_PitchHandle=m_AltHold[APINT];
	} else {
		m_AltHold[APINT]=m_Pitch[APCUR];
	}
}

const string AP_INI_OAC="APOAC";
const string AP_INI_ACS="APACS";
const string AP_INI_TMR="APTMR";
const string AP_INI_PWR="APPWR";
const string AP_INI_TR ="APTR";
const string AP_INI_RDY="APRDY";
const string AP_INI_HM ="APHM";
const string AP_INI_BM ="APBM";
const string AP_INI_PM ="APPM";
const string AP_INI_AHM="APAHM";
const string AP_INI_B0 ="APB0";
const string AP_INI_B1 ="APB1";
const string AP_INI_B2 ="APB2";
const string AP_INI_B3 ="APB3";
const string AP_INI_P0 ="APP0";
const string AP_INI_P1 ="APP1";
const string AP_INI_P2 ="APP2";
const string AP_INI_P3 ="APP3";
const string AP_INI_V0 ="APV0";
const string AP_INI_V1 ="APV1";
const string AP_INI_V2 ="APV2";
const string AP_INI_V3 ="APV3";
const string AP_INI_A0 ="APA0";
const string AP_INI_A1 ="APA1";
const string AP_INI_A2 ="APA2";
const string AP_INI_A3 ="APA3";
const string AP_INI_PH ="APPH";
const string AP_INI_AH ="APAH";
const string AP_INI_AHS="APAHS";

void SDSystemAutopilot::Load(CIniFile *ini)
{
	m_OldAutoCoord		= ini->ReadInt	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_OAC.c_str()	,m_OldAutoCoord		);
	m_AutoCoordSet		= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_ACS.c_str()	,m_AutoCoordSet		);
	m_Timer				= ini->ReadInt	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_TMR.c_str()	,m_Timer			);
	m_Powered			= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_PWR.c_str()	,m_Powered			);
	m_TimerReseted		= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_TR.c_str()	,m_TimerReseted		);
	m_Ready				= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_RDY.c_str()	,m_Ready			);
	m_HandMode			= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_HM.c_str()	,m_HandMode			);
	m_BankMode			= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_BM.c_str()	,m_BankMode			);
	m_PitchMode			= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_PM.c_str()	,m_PitchMode		);
	m_AltHoldMode		= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_AHM.c_str()	,m_AltHoldMode		);
	m_Bank[0]			= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_B0.c_str()	,m_Bank[0]			);
	m_Bank[1]			= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_B1.c_str()	,m_Bank[1]			);
	m_Bank[2]			= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_B2.c_str()	,m_Bank[2]			);
	m_Bank[3]			= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_B3.c_str()	,m_Bank[3]			);
	m_Pitch[0]			= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_P0.c_str()	,m_Pitch[0]			);
	m_Pitch[1]			= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_P1.c_str()	,m_Pitch[1]			);
	m_Pitch[2]			= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_P2.c_str()	,m_Pitch[2]			);
	m_Pitch[3]			= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_P3.c_str()	,m_Pitch[3]			);
	m_VS[0]				= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_V0.c_str()	,m_VS[0]			);
	m_VS[1]				= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_V1.c_str()	,m_VS[1]			);
	m_VS[2]				= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_V2.c_str()	,m_VS[2]			);
	m_VS[3]				= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_V3.c_str()	,m_VS[3]			);
	m_AltHold[0]		= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_A0.c_str()	,m_AltHold[0]		);
	m_AltHold[1]		= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_A1.c_str()	,m_AltHold[1]		);
	m_AltHold[2]		= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_A2.c_str()	,m_AltHold[2]		);
	m_AltHold[3]		= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_A3.c_str()	,m_AltHold[3]		);
	m_PitchHandle		= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_PH.c_str()	,m_PitchHandle		);
	m_AltitudeToHold	= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_AH.c_str()	,m_AltitudeToHold	);
	m_AltitudeToHoldSet	= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),AP_INI_AHS.c_str()	,m_AltitudeToHoldSet);
}

void SDSystemAutopilot::Save(CIniFile *ini)
{
	ini->WriteInt	(SIM_FLT_MY_SECTION.c_str(),AP_INI_OAC.c_str()	,m_OldAutoCoord		);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),AP_INI_ACS.c_str()	,m_AutoCoordSet		);
	ini->WriteInt	(SIM_FLT_MY_SECTION.c_str(),AP_INI_TMR.c_str()	,m_Timer			);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),AP_INI_PWR.c_str()	,m_Powered			);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),AP_INI_TR.c_str()	,m_TimerReseted		);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),AP_INI_RDY.c_str()	,m_Ready			);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),AP_INI_HM.c_str()	,m_HandMode			);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),AP_INI_BM.c_str()	,m_BankMode			);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),AP_INI_PM.c_str()	,m_PitchMode		);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),AP_INI_AHM.c_str()	,m_AltHoldMode		);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_B0.c_str()	,m_Bank[0]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_B1.c_str()	,m_Bank[1]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_B2.c_str()	,m_Bank[2]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_B3.c_str()	,m_Bank[3]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_P0.c_str()	,m_Pitch[0]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_P1.c_str()	,m_Pitch[1]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_P2.c_str()	,m_Pitch[2]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_P3.c_str()	,m_Pitch[3]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_V0.c_str()	,m_VS[0]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_V1.c_str()	,m_VS[1]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_V2.c_str()	,m_VS[2]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_V3.c_str()	,m_VS[3]			);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_A0.c_str()	,m_AltHold[0]		);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_A1.c_str()	,m_AltHold[1]		);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_A2.c_str()	,m_AltHold[2]		);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_A3.c_str()	,m_AltHold[3]		);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_PH.c_str()	,m_PitchHandle		);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),AP_INI_AH.c_str()	,m_AltitudeToHold	);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),AP_INI_AHS.c_str()	,m_AltitudeToHoldSet);
}

