/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

class SDSystemPOS : public SDLogicBase
{
private:
	int		m_InPOSPower;	// ��� ��������� ��� �� ������ ����� ���
	int		m_InEng[3];		// ��� �������� �������� ���������� �� ������ ������ 
	int		m_InPDD[2];		// ������� ��� ���/���� �� ������ ������
	int		m_InSignal[2];	// ������ �����������

	double	m_InTempC;		// ����������� �� �������
	double	m_InDewPoint;	// ����� ����
	double	m_InCoeff;		// ����������� ����������
	double	m_InDelta;		
	double	m_InDeltaEng;

	int		m_OutEngHeat[3];// ����� ��������� �������� ����������
	double	m_Result;		// ��������� ����������
	double	m_ResultFinal;	// ��������� ����������
	double	m_ResultDelta;
	double	m_TempDewDiff;	

	bool	m_PDDONPlayed;

	void	GetData();
	void	SetData();

public:
	SDSystemPOS();
	virtual ~SDSystemPOS();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};

