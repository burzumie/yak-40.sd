/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Pos.h"

SDSystemPOS::SDSystemPOS()
{
}

SDSystemPOS::~SDSystemPOS()
{
}

void SDSystemPOS::Init()
{
	m_InCoeff=5.0;
	m_InDelta=0.1;
	m_InDeltaEng=0.12;
	m_ResultFinal=0;
	m_ResultDelta=0.001;
	m_PDDONPlayed=false;
}

void SDSystemPOS::Update()
{
	GetData();


	if(m_InTempC>0) {
		m_Result=0;
	} else {
		m_TempDewDiff=fabs(m_InTempC-m_InDewPoint);
		if(m_TempDewDiff>5) {
			m_Result=0;
		} else {
			m_Result=m_InCoeff/(1+m_TempDewDiff);
		}
	}

	if(m_InPOSPower&&m_InEng[0]&&PWR_GET(PWR_BUS27)&&AZS_GET(AZS_ENG1_DE_ICE_BUS)) {
		m_OutEngHeat[0]=1;
		ImportTable.pPanels->send_key_event(KEY_ANTI_ICE_SET_ENG1,1);
	} else {
		m_OutEngHeat[0]=0;
		ImportTable.pPanels->send_key_event(KEY_ANTI_ICE_SET_ENG1,0);
	}

	if(m_InPOSPower&&m_InEng[1]&&PWR_GET(PWR_BUS27)&&AZS_GET(AZS_ENG2_DE_ICE_BUS)) {
		m_OutEngHeat[1]=1;
		ImportTable.pPanels->send_key_event(KEY_ANTI_ICE_SET_ENG2,1);
	} else {
		m_OutEngHeat[1]=0;
		ImportTable.pPanels->send_key_event(KEY_ANTI_ICE_SET_ENG2,0);
	}

	if(m_InPOSPower&&m_InEng[2]&&PWR_GET(PWR_BUS27)&&AZS_GET(AZS_ENG3_DE_ICE_BUS)) {
		m_OutEngHeat[2]=1;
		ImportTable.pPanels->send_key_event(KEY_ANTI_ICE_SET_ENG3,1);
	} else {
		m_OutEngHeat[2]=0;
		ImportTable.pPanels->send_key_event(KEY_ANTI_ICE_SET_ENG3,0);
	}

	int tmp=!m_InEng[0]+!m_InEng[1]+!m_InEng[2];
	m_InDeltaEng=tmp*0.04;

	if(m_Result) {
		if(m_ResultFinal<m_Result)
			m_ResultFinal+=m_ResultDelta;
		if(m_ResultFinal>m_Result)
			m_ResultFinal-=m_ResultDelta;

		//if(m_ResultFinal<0)
		//	m_ResultFinal=0;
	}

	if(g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
		SetJetThrustScalar(1.0-m_ResultFinal*m_InDeltaEng);
	} else {
		SetJetThrustScalar(1.0);
	}

	if(m_Result&&!m_OutEngHeat[0]) {
		TBL_SET(TBL_ICE_ENG_L,1);
	} else {
		TBL_SET(TBL_ICE_ENG_L,0);
	}

	if(m_Result&&!m_OutEngHeat[2]) {
		TBL_SET(TBL_ICE_ENG_R,1);
	} else {
		TBL_SET(TBL_ICE_ENG_R,0);
	}

	if(m_InPOSPower&&PWR_GET(PWR_BUS27)) {
		SetYawStabilityScalar(1.0);
		SetRollStabilityScalar(1.0);
		SetPitchStabilityScalar(1.0);
		SetRudderEffScalar(1.0);
		SetAileronEffScalar(1.0);
		SetElevatorEffScalar(1.0);
		SetParasiticDragScalar(1.0);
		SetZeroAlphaLiftScalar(1.0);
	} else {
		double tmpres=m_ResultFinal*m_InDelta;
		SetYawStabilityScalar(1.0-tmpres);
		SetRollStabilityScalar(1.0-tmpres);
		SetPitchStabilityScalar(1.0-tmpres);
		SetRudderEffScalar(1.0-tmpres);
		SetAileronEffScalar(1.0-tmpres);
		SetElevatorEffScalar(1.0-tmpres);
		SetParasiticDragScalar(1.0-tmpres);
		SetZeroAlphaLiftScalar(1.0-tmpres);
	}

	if(!m_InPDD[0]&&!m_InPDD[1]) {
		ImportTable.pPanels->send_key_event(KEY_PITOT_HEAT_OFF,0);
	} else {
		if(m_InPOSPower&&PWR_GET(PWR_BUS27)) {
			ImportTable.pPanels->send_key_event(KEY_PITOT_HEAT_ON,0);
			if(!m_PDDONPlayed) {
				SND_SET(SND_PDD_ON,1);
				m_PDDONPlayed=true;
			}
		} else {
			ImportTable.pPanels->send_key_event(KEY_PITOT_HEAT_OFF,0);
			m_PDDONPlayed=false;
		}
	}

	SetData();
}

void SDSystemPOS::Load(CIniFile *ini)
{
}

void SDSystemPOS::Save(CIniFile *ini)
{
}

void SDSystemPOS::GetData()
{
	m_InPOSPower	= AZS_GET(AZS_ANTI_ICE_SYS_BUS);
	m_InEng[0]		= AZS_GET(AZS_ENG1HEAT);
	m_InEng[1]		= AZS_GET(AZS_ENG2HEAT);
	m_InEng[2]		= AZS_GET(AZS_ENG3HEAT);
	m_InPDD[0]		= AZS_GET(AZS_PITOTHEAT1);
	m_InPDD[1]		= AZS_GET(AZS_PITOTHEAT2);
	m_InSignal[0]	= AZS_GET(AZS_ICEWARN1);
	m_InSignal[1]	= AZS_GET(AZS_ICEWARN1);

	m_InTempC		= GetTempC(); //g_pSDAPI->GetN(TOTAL_AIR_TEMP)*256;
	m_InDewPoint	= GetDewPoint();
}

void SDSystemPOS::SetData()
{
	LMP_SET(LMP_ENG1_DEICE,m_OutEngHeat[0]);
	LMP_SET(LMP_ENG2_DEICE,m_OutEngHeat[1]);
	LMP_SET(LMP_ENG3_DEICE,m_OutEngHeat[2]);
}

