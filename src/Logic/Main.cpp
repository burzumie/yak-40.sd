/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"
#include "Global.h"

#ifdef _DEBUG
_CrtMemState memoryState; 
#endif

HINSTANCE g_hInstance=NULL;
sImportTab ImportTable;

BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
#ifdef _DEBUG
	_CrtMemCheckpoint(&memoryState);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
#endif
	g_hInstance=(HINSTANCE)hModule;
	switch(ul_reason_for_call)	{
		case DLL_PROCESS_ATTACH: 
#ifdef _DEBUG
			AllocConsole();
#endif
			DisableThreadLibraryCalls((HMODULE)hModule);
		break;
		case DLL_THREAD_ATTACH: 
		break;
		case DLL_THREAD_DETACH:
		break;
		case DLL_PROCESS_DETACH:
#ifdef _DEBUG
			FreeConsole();
#endif
		break;
	}
	return TRUE;
}

void UpdatePanel()
{
	UndocUpdate(); 

	if(g_pSDAPI) 
		g_pSDAPI->Update();

	if(g_pGlobal)
		g_pGlobal->Update();

}

void FSAPI fnInitPanel(void)
{
}

void FSAPI fnDeinitPanel(void)
{
#ifdef _DEBUG
	CHECK_MEM_LEAK("F:\\MemLeaks\\Yak40\\PA.log");
#endif

}

#ifdef GAUGE_NAME
#undef GAUGE_NAME
#endif

#ifdef GAUGEHDR_VAR_NAME
#undef GAUGEHDR_VAR_NAME
#endif

#ifdef GAUGE_W
#undef GAUGE_W
#endif

#define GAUGE_NAME         "logic"
#define GAUGEHDR_VAR_NAME  PA_hdr
#define GAUGE_W            MISC_DUMMY_SX

extern PELEMENT_HEADER logic_list;
GAUGE_CALLBACK logic_cb;
GAUGE_HEADER_FS700(GAUGE_W, GAUGE_NAME, &logic_list, 0, logic_cb, 0, 0, 0);
MAKE_STATIC(logic_bg,MISC_DUMMY,NULL,NULL,IMAGE_USE_TRANSPARENCY,0,0,0)
PELEMENT_HEADER	logic_list = &logic_bg.header;

void FSAPI logic_cb( PGAUGEHDR pgauge, SINT32 service_id, UINT32 extra_data ) 
{
	static bool m_AlreadyLoaded=false;

	switch(service_id) {
		case PANEL_SERVICE_CONNECT_TO_WINDOW:
			UndocUpdate(); 
			ImportTable.pPanels->register_var_by_name(&g_SDAPIEntrys,TYPE_PVOID,SDAPISIGN);
			if(!g_pSDAPI) {
				g_pSDAPI=new SDAPI(true);
				if(g_pSDAPI) {
					g_pSDAPI->Prepare();
					g_pSDAPI->Update();
					g_pGlobal=new SDGlobal();
					if(g_pGlobal) {
						g_pGlobal->Init();
					}
				}
			}
			CFG_SET(CFG_OBS_ZERO_START,g_pSDAPI->m_Config.m_CfgNav.m_bOBSZeroStart);
			UpdatePanel();
		break;
		case PANEL_SERVICE_DISCONNECT:
			UndocReset();
			if(g_pGlobal) {
				delete g_pGlobal;
				g_pGlobal=NULL;
			}

			if(g_pSDAPI) {
				delete g_pSDAPI;
				g_pSDAPI=NULL;
			}
			ImportTable.pPanels->unregister_all_named_vars();
			m_AlreadyLoaded=false;
		break;
		case PANEL_SERVICE_PRE_DRAW:
		break;
		case PANEL_SERVICE_PRE_UPDATE:
			if(!m_AlreadyLoaded) {
				ImportTable.pPanels->panel_window_open_ident(IDENT_P1);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P1);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P2);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P2);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P3);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P3);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P4);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P4);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P5);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P5);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P6);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P6);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P7);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P7);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P8);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P8);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P9);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P9);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P10);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P10);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P11);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P11);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P12);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P12);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P13);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P13);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P14);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P14);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P15);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P15);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P16);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P16);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P22);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P22);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P122);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P122);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P110);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P110);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P17);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P17);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P18);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P18);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P19);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P19);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P24);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P24);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P25);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P25);
				ImportTable.pPanels->panel_window_open_ident(IDENT_P23);
				ImportTable.pPanels->panel_window_close_ident(IDENT_P23);
				m_AlreadyLoaded=true;
			}
			UpdatePanel();
		break;
	}
}

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&PA_hdr),
		0
	}
};
