/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "Global.h"
#include "LogicBase.h"

#define UI_GENERATEDFLIGHT_STRING_RES	30000
#define PREVIOUSFLIGHT_STRING_RES		8706

SDGlobal *g_pGlobal=NULL;
HHOOK	g_hKeyHook;
bool	g_bGndServicesVisible=false;

extern LRESULT CALLBACK fnKeyboardProc(int nCode,WPARAM wParam,LPARAM lParam)
{
	if (nCode < 0)  
		return CallNextHookEx(g_hKeyHook, nCode, wParam, lParam); 

	BYTE ls=HIBYTE(GetKeyState(VK_LSHIFT));
	BYTE rs=HIBYTE(GetKeyState(VK_RSHIFT));

	if((ls||rs)&&(wParam==48&&((lParam>>30)&1))) {
		POS_TGL(POS_GROUND_STARTUP);
		return 1;
	}

	return CallNextHookEx(g_hKeyHook, nCode, wParam, lParam); 
}

SDGlobal::SDGlobal() : CSimKey(ImportTable.pPanels)
{
	m_PrevoiusFlight	= "";
	m_UIGeneratedFlight	= "";
	m_FlightInfo		= auto_ptr<FLIGHT_FILE_INFO>(new FLIGHT_FILE_INFO);

	PrepareSimSaveFiles();

	m_TimerSave			= auto_ptr<SDTimerSave>(new SDTimerSave());
	m_Container			= auto_ptr<SDLogicContainer>(new SDLogicContainer());
	StartKey();

	m_Htail				= CHtail::Instance();
	m_Flaps				= CFlaps::Instance();
	m_RTU				= CRTU::Instance();
	m_AirStarter		= CAirStarter::Instance();
	m_Gear				= CGear::Instance();

	g_hKeyHook = SetWindowsHookEx( WH_KEYBOARD, fnKeyboardProc, (HINSTANCE) NULL, GetCurrentThreadId()); 

	m_EnginePressed=false;

	for(int i=0;i<MAX_AZS_IN_VC;i++) {
		m_AzsVC[i]=NULL;
	}

	m_AzsVC[  0]=new CNamedVar("p0_001azs",ImportTable.pPanels,false);
	m_AzsVC[  1]=new CNamedVar("p0_002cov",ImportTable.pPanels,false);
	m_AzsVC[  2]=new CNamedVar("p0_002azs",ImportTable.pPanels,false);
	m_AzsVC[  3]=new CNamedVar("p0_003azs",ImportTable.pPanels,false);
	m_AzsVC[  4]=new CNamedVar("p0_004cov",ImportTable.pPanels,false);
	m_AzsVC[  5]=new CNamedVar("p0_004azs",ImportTable.pPanels,false);
	m_AzsVC[  6]=new CNamedVar("p0_005azs",ImportTable.pPanels,false);
	m_AzsVC[  7]=new CNamedVar("p0_006azs",ImportTable.pPanels,false);
	m_AzsVC[  8]=new CNamedVar("p0_007azs",ImportTable.pPanels,false);
	m_AzsVC[  9]=new CNamedVar("p0_008azs",ImportTable.pPanels,false);
	m_AzsVC[ 10]=new CNamedVar("p0_009azs",ImportTable.pPanels,false);
	m_AzsVC[ 11]=new CNamedVar("p0_010azs",ImportTable.pPanels,false);
	m_AzsVC[ 12]=new CNamedVar("p1_011azs",ImportTable.pPanels,false);
	m_AzsVC[ 13]=new CNamedVar("p1_012azs",ImportTable.pPanels,false);
	m_AzsVC[ 14]=new CNamedVar("p1_013azs",ImportTable.pPanels,false);
	m_AzsVC[ 15]=new CNamedVar("p1_014cov",ImportTable.pPanels,false);
	m_AzsVC[ 16]=new CNamedVar("p1_014azs",ImportTable.pPanels,false);
	m_AzsVC[ 17]=new CNamedVar("p1_015azs",ImportTable.pPanels,false);
	m_AzsVC[ 18]=new CNamedVar("p1_016azs",ImportTable.pPanels,false);
	m_AzsVC[ 19]=new CNamedVar("p1_017cov",ImportTable.pPanels,false);
	m_AzsVC[ 20]=new CNamedVar("p1_017azs",ImportTable.pPanels,false);
	m_AzsVC[ 21]=new CNamedVar("p1_018cov",ImportTable.pPanels,false);
	m_AzsVC[ 22]=new CNamedVar("p1_018azs",ImportTable.pPanels,false);
	m_AzsVC[ 23]=new CNamedVar("p1_019cov",ImportTable.pPanels,false);
	m_AzsVC[ 24]=new CNamedVar("p1_019azs",ImportTable.pPanels,false);
	m_AzsVC[ 25]=new CNamedVar("p1_020azs",ImportTable.pPanels,false);
	m_AzsVC[ 26]=new CNamedVar("p1_021azs",ImportTable.pPanels,false);
	m_AzsVC[ 27]=new CNamedVar("p1_022azs",ImportTable.pPanels,false);
	m_AzsVC[ 28]=new CNamedVar("p1_023azs",ImportTable.pPanels,false);
	m_AzsVC[ 29]=new CNamedVar("p1_024azs",ImportTable.pPanels,false);
	m_AzsVC[ 30]=new CNamedVar("p1_025azs",ImportTable.pPanels,false);
	m_AzsVC[ 31]=new CNamedVar("p1_026azs",ImportTable.pPanels,false);
	m_AzsVC[ 32]=new CNamedVar("p1_027azs",ImportTable.pPanels,false);
	m_AzsVC[ 33]=new CNamedVar("p1_028cov",ImportTable.pPanels,false);
	m_AzsVC[ 34]=new CNamedVar("p1_028azs",ImportTable.pPanels,false);
	m_AzsVC[ 35]=new CNamedVar("p1_029azs",ImportTable.pPanels,false);
	m_AzsVC[ 36]=new CNamedVar("p1_030azs",ImportTable.pPanels,false);
	m_AzsVC[ 37]=new CNamedVar("p1_030azs",ImportTable.pPanels,false);
	m_AzsVC[ 38]=new CNamedVar("p1_031azs",ImportTable.pPanels,false);
	m_AzsVC[ 39]=new CNamedVar("p1_032azs",ImportTable.pPanels,false);
	m_AzsVC[ 40]=new CNamedVar("p1_033azs",ImportTable.pPanels,false);
	m_AzsVC[ 41]=new CNamedVar("p4_034cov",ImportTable.pPanels,false);
	m_AzsVC[ 42]=new CNamedVar("p4_034azs",ImportTable.pPanels,false);
	m_AzsVC[ 43]=new CNamedVar("p4_035cov",ImportTable.pPanels,false);
	m_AzsVC[ 44]=new CNamedVar("p4_035azs",ImportTable.pPanels,false);
	m_AzsVC[ 45]=new CNamedVar("p4_036cov",ImportTable.pPanels,false);
	m_AzsVC[ 46]=new CNamedVar("p4_036azs",ImportTable.pPanels,false);
	m_AzsVC[ 47]=new CNamedVar("p4_037azs",ImportTable.pPanels,false);
	m_AzsVC[ 48]=new CNamedVar("p4_038azs",ImportTable.pPanels,false);
	m_AzsVC[ 49]=new CNamedVar("p4_039azs",ImportTable.pPanels,false);
	m_AzsVC[ 50]=new CNamedVar("p4_040cov",ImportTable.pPanels,false);
	m_AzsVC[ 51]=new CNamedVar("p4_040azs",ImportTable.pPanels,false);
	m_AzsVC[ 52]=new CNamedVar("p4_041azs",ImportTable.pPanels,false);
	m_AzsVC[ 53]=new CNamedVar("p4_042cov",ImportTable.pPanels,false);
	m_AzsVC[ 54]=new CNamedVar("p4_042azs",ImportTable.pPanels,false);
	m_AzsVC[ 55]=new CNamedVar("p4_043cov",ImportTable.pPanels,false);
	m_AzsVC[ 56]=new CNamedVar("p4_043azs",ImportTable.pPanels,false);
	m_AzsVC[ 57]=new CNamedVar("p4_044cov",ImportTable.pPanels,false);
	m_AzsVC[ 58]=new CNamedVar("p4_044azs",ImportTable.pPanels,false);
	m_AzsVC[ 59]=new CNamedVar("p4_045cov",ImportTable.pPanels,false);
	m_AzsVC[ 60]=new CNamedVar("p4_045azs",ImportTable.pPanels,false);
	m_AzsVC[ 61]=new CNamedVar("p4_046azs",ImportTable.pPanels,false);
	m_AzsVC[ 62]=new CNamedVar("p4_047cov",ImportTable.pPanels,false);
	m_AzsVC[ 63]=new CNamedVar("p4_047azs",ImportTable.pPanels,false);
	m_AzsVC[ 64]=new CNamedVar("p4_048cov",ImportTable.pPanels,false);
	m_AzsVC[ 65]=new CNamedVar("p4_048azs",ImportTable.pPanels,false);
	m_AzsVC[ 66]=new CNamedVar("p4_049cov",ImportTable.pPanels,false);
	m_AzsVC[ 67]=new CNamedVar("p4_049azs",ImportTable.pPanels,false);
	m_AzsVC[ 68]=new CNamedVar("p4_050azs",ImportTable.pPanels,false);
	m_AzsVC[ 69]=new CNamedVar("p4_051azs",ImportTable.pPanels,false);
	m_AzsVC[ 70]=new CNamedVar("p5_052azs",ImportTable.pPanels,false);
	m_AzsVC[ 71]=new CNamedVar("p5_053azs",ImportTable.pPanels,false);
	m_AzsVC[ 72]=new CNamedVar("p5_054azs",ImportTable.pPanels,false);
	m_AzsVC[ 73]=new CNamedVar("p5_055azs",ImportTable.pPanels,false);
	m_AzsVC[ 74]=new CNamedVar("p5_056azs",ImportTable.pPanels,false);
	m_AzsVC[ 75]=new CNamedVar("p5_057cov",ImportTable.pPanels,false);
	m_AzsVC[ 76]=new CNamedVar("p5_057azs",ImportTable.pPanels,false);
	m_AzsVC[ 77]=new CNamedVar("p5_058cov",ImportTable.pPanels,false);
	m_AzsVC[ 78]=new CNamedVar("p5_058azs",ImportTable.pPanels,false);
	m_AzsVC[ 79]=new CNamedVar("p5_059cov",ImportTable.pPanels,false);
	m_AzsVC[ 80]=new CNamedVar("p5_059azs",ImportTable.pPanels,false);
	m_AzsVC[ 81]=new CNamedVar("p5_060azs",ImportTable.pPanels,false);
	m_AzsVC[ 82]=new CNamedVar("p5_061azs",ImportTable.pPanels,false);
	m_AzsVC[ 83]=new CNamedVar("p5_062azs",ImportTable.pPanels,false);
	m_AzsVC[ 84]=new CNamedVar("p5_063azs",ImportTable.pPanels,false);
	m_AzsVC[ 85]=new CNamedVar("p5_064azs",ImportTable.pPanels,false);
	m_AzsVC[ 86]=new CNamedVar("p6_065azs",ImportTable.pPanels,false);
	m_AzsVC[ 87]=new CNamedVar("p6_066azs",ImportTable.pPanels,false);
	m_AzsVC[ 88]=new CNamedVar("p6_067azs",ImportTable.pPanels,false);
	m_AzsVC[ 89]=new CNamedVar("p6_068azs",ImportTable.pPanels,false);
	m_AzsVC[ 90]=new CNamedVar("p6_069azs",ImportTable.pPanels,false);
	m_AzsVC[ 91]=new CNamedVar("p6_070azs",ImportTable.pPanels,false);
	m_AzsVC[ 92]=new CNamedVar("p7_071azs",ImportTable.pPanels,false);
	m_AzsVC[ 93]=new CNamedVar("p7_072azs",ImportTable.pPanels,false);
	m_AzsVC[ 94]=new CNamedVar("p7_073azs",ImportTable.pPanels,false);
	m_AzsVC[ 95]=new CNamedVar("p7_074azs",ImportTable.pPanels,false);
	m_AzsVC[ 96]=new CNamedVar("p7_075azs",ImportTable.pPanels,false);
	m_AzsVC[ 97]=new CNamedVar("p7_076azs",ImportTable.pPanels,false);
	m_AzsVC[ 98]=new CNamedVar("p7_077azs",ImportTable.pPanels,false);
	m_AzsVC[ 99]=new CNamedVar("p8_078azs",ImportTable.pPanels,false);
	m_AzsVC[100]=new CNamedVar("p8_079azs",ImportTable.pPanels,false);
	m_AzsVC[101]=new CNamedVar("p8_080azs",ImportTable.pPanels,false);
	m_AzsVC[102]=new CNamedVar("p8_081azs",ImportTable.pPanels,false);
	m_AzsVC[103]=new CNamedVar("p8_082azs",ImportTable.pPanels,false);
	m_AzsVC[104]=new CNamedVar("p8_083azs",ImportTable.pPanels,false);
	m_AzsVC[105]=new CNamedVar("p8_084azs",ImportTable.pPanels,false);
	m_AzsVC[106]=new CNamedVar("p8_085azs",ImportTable.pPanels,false);
	m_AzsVC[107]=new CNamedVar("p8_086azs",ImportTable.pPanels,false);
	m_AzsVC[108]=new CNamedVar("p8_087azs",ImportTable.pPanels,false);
	m_AzsVC[109]=new CNamedVar("p8_088azs",ImportTable.pPanels,false);
	m_AzsVC[110]=new CNamedVar("p8_089azs",ImportTable.pPanels,false);
	m_AzsVC[111]=new CNamedVar("p8_090azs",ImportTable.pPanels,false);
	m_AzsVC[112]=new CNamedVar("p8_091azs",ImportTable.pPanels,false);
	m_AzsVC[113]=new CNamedVar("p8_092azs",ImportTable.pPanels,false);
	m_AzsVC[114]=new CNamedVar("p8_093azs",ImportTable.pPanels,false);
	m_AzsVC[115]=new CNamedVar("p8_094azs",ImportTable.pPanels,false);
	m_AzsVC[116]=new CNamedVar("p8_095azs",ImportTable.pPanels,false);
	m_AzsVC[117]=new CNamedVar("p8_096azs",ImportTable.pPanels,false);
	m_AzsVC[118]=new CNamedVar("p8_097azs",ImportTable.pPanels,false);
	m_AzsVC[119]=new CNamedVar("p8_098azs",ImportTable.pPanels,false);
	m_AzsVC[120]=new CNamedVar("p8_099azs",ImportTable.pPanels,false);
	m_AzsVC[121]=new CNamedVar("p8_100azs",ImportTable.pPanels,false);
	m_AzsVC[122]=new CNamedVar("p8_101azs",ImportTable.pPanels,false);
	m_AzsVC[123]=new CNamedVar("p8_102azs",ImportTable.pPanels,false);
	m_AzsVC[124]=new CNamedVar("p8_103azs",ImportTable.pPanels,false);
	m_AzsVC[125]=new CNamedVar("p8_104azs",ImportTable.pPanels,false);
	m_AzsVC[126]=new CNamedVar("p8_105azs",ImportTable.pPanels,false);
	m_AzsVC[127]=new CNamedVar("p8_106azs",ImportTable.pPanels,false);
	m_AzsVC[128]=new CNamedVar("p8_107azs",ImportTable.pPanels,false);
	m_AzsVC[129]=new CNamedVar("p8_108azs",ImportTable.pPanels,false);
	m_AzsVC[130]=new CNamedVar("p8_109azs",ImportTable.pPanels,false);
	m_AzsVC[131]=new CNamedVar("p8_110azs",ImportTable.pPanels,false);
	m_AzsVC[132]=new CNamedVar("p8_111azs",ImportTable.pPanels,false);
	m_AzsVC[133]=new CNamedVar("p8_112azs",ImportTable.pPanels,false);
	m_AzsVC[134]=new CNamedVar("p8_113azs",ImportTable.pPanels,false);
	m_AzsVC[135]=new CNamedVar("p9_114azs",ImportTable.pPanels,false);
	m_AzsVC[136]=new CNamedVar("p9_115azs",ImportTable.pPanels,false);
	m_AzsVC[137]=new CNamedVar("p9_116azs",ImportTable.pPanels,false);
	m_AzsVC[138]=new CNamedVar("p9_117azs",ImportTable.pPanels,false);
	m_AzsVC[139]=new CNamedVar("p9_118azs",ImportTable.pPanels,false);
	m_AzsVC[140]=new CNamedVar("p9_119azs",ImportTable.pPanels,false);
	m_AzsVC[141]=new CNamedVar("p9_120azs",ImportTable.pPanels,false);
	m_AzsVC[142]=new CNamedVar("p9_121azs",ImportTable.pPanels,false);
	m_AzsVC[143]=new CNamedVar("p9_122azs",ImportTable.pPanels,false);
	m_AzsVC[144]=new CNamedVar("p9_123azs",ImportTable.pPanels,false);
	m_AzsVC[145]=new CNamedVar("p9_124azs",ImportTable.pPanels,false);
	m_AzsVC[146]=new CNamedVar("p9_125azs",ImportTable.pPanels,false);
	m_AzsVC[147]=new CNamedVar("p9_126azs",ImportTable.pPanels,false);
	m_AzsVC[148]=new CNamedVar("p9_127azs",ImportTable.pPanels,false);
	m_AzsVC[149]=new CNamedVar("p9_128azs",ImportTable.pPanels,false);
	m_AzsVC[150]=new CNamedVar("p9_129azs",ImportTable.pPanels,false);
	m_AzsVC[151]=new CNamedVar("p9_130azs",ImportTable.pPanels,false);
	m_AzsVC[152]=new CNamedVar("p9_131azs",ImportTable.pPanels,false);
	m_AzsVC[153]=new CNamedVar("p9_132azs",ImportTable.pPanels,false);
	m_AzsVC[154]=new CNamedVar("p9_133azs",ImportTable.pPanels,false);
	m_AzsVC[155]=new CNamedVar("p9_134azs",ImportTable.pPanels,false);
	m_AzsVC[156]=new CNamedVar("p9_135azs",ImportTable.pPanels,false);
	m_AzsVC[157]=new CNamedVar("p9_136azs",ImportTable.pPanels,false);
	m_AzsVC[158]=new CNamedVar("p9_137azs",ImportTable.pPanels,false);
	m_AzsVC[159]=new CNamedVar("p9_138azs",ImportTable.pPanels,false);
	m_AzsVC[160]=new CNamedVar("p9_139azs",ImportTable.pPanels,false);
	m_AzsVC[161]=new CNamedVar("p9_140azs",ImportTable.pPanels,false);
	m_AzsVC[162]=new CNamedVar("p9_141azs",ImportTable.pPanels,false);
	m_AzsVC[163]=new CNamedVar("p9_142azs",ImportTable.pPanels,false);
	m_AzsVC[164]=new CNamedVar("p9_143azs",ImportTable.pPanels,false);
	m_AzsVC[165]=new CNamedVar("p9_144azs",ImportTable.pPanels,false);
	m_AzsVC[166]=new CNamedVar("p9_145azs",ImportTable.pPanels,false);
	m_AzsVC[167]=new CNamedVar("p9_146azs",ImportTable.pPanels,false);
	m_AzsVC[168]=new CNamedVar("p9_147azs",ImportTable.pPanels,false);
	m_AzsVC[169]=new CNamedVar("p9_148azs",ImportTable.pPanels,false);
	m_AzsVC[170]=new CNamedVar("p9_149azs",ImportTable.pPanels,false);
	m_AzsVC[171]=new CNamedVar("p4_052azs",ImportTable.pPanels,false);
	m_AzsVC[172]=new CNamedVar("p4_053azs",ImportTable.pPanels,false);

	m_GltVC[  0]=new CNamedVar("sd6015_glt_sgu_left",ImportTable.pPanels,false);
	m_GltVC[  1]=new CNamedVar("sd6015_glt_sgu_right",ImportTable.pPanels,false);
	m_GltVC[  2]=new CNamedVar("sd6015_glt_xpdr_mode",ImportTable.pPanels,false);
	m_GltVC[  3]=new CNamedVar("sd6015_glt__temp",ImportTable.pPanels,false);
	m_GltVC[  4]=new CNamedVar("sd6015_glt_v27",ImportTable.pPanels,false);
	m_GltVC[  5]=new CNamedVar("sd6015_glt_v36",ImportTable.pPanels,false);
	m_GltVC[  6]=new CNamedVar("sd6015_glt_v115",ImportTable.pPanels,false);
	
	m_AlreadyLoaded=false;
}

SDGlobal::~SDGlobal()
{
	m_AlreadyLoaded=false;
	for(int i=0;i<MAX_AZS_IN_VC;i++) {
		if(m_AzsVC[i]) {
			delete m_AzsVC[i];
			m_AzsVC[i]=NULL;
		}
	}
	for(int i=0;i<MAX_GLT_IN_VC;i++) {
		if(m_GltVC[i]) {
			delete m_GltVC[i];
			m_GltVC[i]=NULL;
		}
	}
	UnhookWindowsHookEx(g_hKeyHook);
	//ImportTable.pFlight->flight_get(m_FlightInfo.get());
	GetFlightFileInfo(m_FlightInfo.get());
	char m_sFileName[MAX_PATH];
	strcpy(m_sFileName,m_FlightInfo->FlightFile.file_name);
	m_sFileName[strrchr(m_sFileName,'\\')-m_sFileName+1]='\0';
	strcat(m_sFileName,m_PrevoiusFlight.c_str());
	Save(m_sFileName);
	StopKey();
	CHtail::Release();
	CFlaps::Release();
	CRTU::Release();
	CAirStarter::Release();
	CGear::Release();
}

void SDGlobal::Init()
{
	m_XmlPanelLang=auto_ptr<CNamedVar>(new CNamedVar("sd6015_panel_lang",ImportTable.pPanels,false));
	POS_SET(POS_TIME_AFTER_LOAD,360);
	POS_SET(POS_PANEL_LANG,g_pSDAPI->m_Config.m_CfgCommon.m_iLanguage);
	AZS_SET(AZS_LLIGHTSMOTOR,1);
	SDLogicBase::InitAll();
	Load();
}

void SDGlobal::Update()
{
	m_XmlPanelLang->set_number(POS_GET(POS_PANEL_LANG)+1);

	m_ElapsedSec[0]=g_pSDAPI->GetN(ELAPSED_SECONDS);
	if(m_ElapsedSec[0]==m_ElapsedSec[1]||GetMainIsLoading()) {
		m_Paused=true;
	} else {
		m_Paused=false;
	}
	m_ElapsedSec[1]=m_ElapsedSec[0];
	
	POS_SET(POS_SIM_PAUSED,m_Paused);
	POS_SET(POS_TIME_OF_DAY,GetTimeOfDay());
	POS_SET(POS_ACTIVE_VIEW_MODE,ImportTable.pGlobal->pactive_viewblock->view_mode);

	if(m_TimerSave->m_SaveThread.m_bFileChanged) {
		char *a=strrchr(m_TimerSave->m_SaveThread.m_sFileName,'\\')+1;
		if(_stricoll(a,m_UIGeneratedFlight.c_str())) {
			Save(m_TimerSave->m_SaveThread.m_sFileName);
		}
		m_TimerSave->m_SaveThread.m_bFileChanged=false;
	}

	leaveon(m_Paused);

	POS_INC(POS_TIME_AFTER_LOAD);

	m_Htail->Update();
	m_Flaps->Update();
	m_RTU->Update();
	m_AirStarter->Update();
	m_Gear->Update();

	SDLogicBase::UpdateAll();

	UpdateVCAZS();
}

#define SAVE_SECTION_DBL(T,M,S) /**/ \
	for(i=0;i<M;i++) { \
	char buf[6]; \
	for(j=0;j<VAL_MAX;j++) { \
	sprintf(buf,T"%d%d",i,j); \
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf,g_SDAPIEntrys.S[i].m_Val[j]); \
	} \
	}

#define SAVE_SECTION_INT(T,M,S) /**/ \
	for(i=0;i<M;i++) { \
	char buf[6]; \
	for(j=0;j<VAL_MAX;j++) { \
	sprintf(buf,T"%d%d",i,j); \
	ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf,g_SDAPIEntrys.S[i].m_Val[j]); \
	} \
	}

#define LOAD_SECTION_DBL(T,M,S) /**/ \
	for(i=0;i<M;i++) { \
	double tmp; \
	char buf[6]; \
	for(j=0;j<VAL_MAX;j++) { \
	sprintf(buf,T"%d%d",i,j); \
	tmp=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf,1234.56); \
	if(tmp!=1234.56) \
	g_SDAPIEntrys.S[i].m_Val[j]=tmp; \
	} \
	}

#define LOAD_SECTION_INT(T,M,S) /**/ \
	for(i=0;i<M;i++) { \
	int tmp; \
	char buf[6]; \
	for(j=0;j<VAL_MAX;j++) { \
	sprintf(buf,T"%d%d",i,j); \
	tmp=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf,123456); \
	if(tmp!=123456) \
	g_SDAPIEntrys.S[i].m_Val[j]=tmp; \
	} \
	}

void SDGlobal::Load()
{
	char m_sFileName[MAX_PATH];
	//ImportTable.pFlight->flight_get(m_FlightInfo.get());
	GetFlightFileInfo(m_FlightInfo.get());
	sprintf(m_sFileName,"%s.%s",m_FlightInfo->FlightFile.file_name,SIM_FLT_EXTENSION.c_str());
	char *a=strrchr(m_sFileName,'\\')+1;
	if(_stricoll(a,m_UIGeneratedFlight.c_str())) {
		CIniFile *ini=new CIniFile(m_sFileName);

		int i,j;

		LOAD_SECTION_INT("0",AZS_MAX,SDAZS);
		LOAD_SECTION_INT("1",BTN_MAX,SDBTN);
		LOAD_SECTION_INT("2",KRM_MAX,SDKRM);
		LOAD_SECTION_INT("3",GLT_MAX,SDGLT);
		LOAD_SECTION_DBL("4",POS_MAX,SDPOS);
		LOAD_SECTION_DBL("5",NDL_MAX,SDNDL);
		LOAD_SECTION_INT("6",HND_MAX,SDHND);
		LOAD_SECTION_INT("7",TBL_MAX,SDTBL);
		LOAD_SECTION_INT("8",TBG_MAX,SDTBG);
		LOAD_SECTION_INT("9",LMP_MAX,SDLMP);
		LOAD_SECTION_INT("a",PWR_MAX,SDPWR);
		LOAD_SECTION_INT("b",SND_MAX,SDSND);
		LOAD_SECTION_INT("k",KLN_MAX,SDKLN);

		SDLogicBase::LoadAll(ini);

		m_AirStarter->Load(ini);
		m_Flaps->Load(ini);
		m_Htail->Load(ini);
		LoadPax(ini);

		double masstmp=0;

		/*
		masstmp=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"LMQL",-123456789);
		if(masstmp!=-123456789)
		SetLeftTankQuantityLbs(masstmp);

		masstmp=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"RMQL",-123456789);
		if(masstmp!=-123456789)
		SetRightTankQuantityLbs(masstmp);

		masstmp=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"DMSE",-123456789);
		if(masstmp!=-123456789)
		SetPayloadWeightPound(SEAT_EQUIP,masstmp);

		masstmp=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"DMSC",-123456789);
		if(masstmp!=-123456789)
		SetPayloadWeightPound(SEAT_CARGO,masstmp);
		*/

		//g_iCurrentPanel[0]=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),"CPV1",0);
		//g_iCurrentPanel[1]=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),"CPV2",0);

		delete ini;

		POS_SET(POS_TIME_AFTER_LOAD,0);
	}
}

void SDGlobal::Save(const char *name)
{
	CIniFile *ini=new CIniFile(name);

	int i,j;

	SAVE_SECTION_INT("0",AZS_MAX,SDAZS);
	SAVE_SECTION_INT("1",BTN_MAX,SDBTN);
	SAVE_SECTION_INT("2",KRM_MAX,SDKRM);
	SAVE_SECTION_INT("3",GLT_MAX,SDGLT);
	SAVE_SECTION_DBL("4",POS_MAX,SDPOS);
	SAVE_SECTION_DBL("5",NDL_MAX,SDNDL);
	SAVE_SECTION_INT("6",HND_MAX,SDHND);
	SAVE_SECTION_INT("7",TBL_MAX,SDTBL);
	SAVE_SECTION_INT("8",TBG_MAX,SDTBG);
	SAVE_SECTION_INT("9",LMP_MAX,SDLMP);
	SAVE_SECTION_INT("a",PWR_MAX,SDPWR);
	SAVE_SECTION_INT("b",SND_MAX,SDSND);
	SAVE_SECTION_INT("k",KLN_MAX,SDKLN);

	SDLogicBase::SaveAll(ini);

	m_AirStarter->Save(ini);
	m_Flaps->Save(ini);
	m_Htail->Save(ini);
	SavePax(ini);
	

	/*
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"LMQL",GetLeftTankQuantityLbs());
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"RMQL",GetRightTankQuantityLbs());
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"DMSE",GetPayloadWeightPound(SEAT_EQUIP));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"DMSC",GetPayloadWeightPound(SEAT_CARGO));
	*/

	//ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),"CPV1",g_iCurrentPanel[0]);
	//ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),"CPV2",g_iCurrentPanel[1]);

	delete ini;
}

bool SDGlobal::fnKeyHandler(ID32 event,UINT32 evdata)
{
	int tmp=0;
	int tmp2=0;
	switch(event) {
		case KEY_AUTOPILOT_ON:
		case KEY_AUTOPILOT_OFF:
		case KEY_AP_MASTER:
			AZS_TGL(AZS_AP_PWR);
			break;
		case KEY_AP_ALT_HOLD:
		case KEY_AP_ALT_HOLD_ON:
		case KEY_AP_ALT_HOLD_OFF:
			AZS_SET(AZS_AP_ALT_HOLD,1);
			break;
		case KEY_AP_WING_LEVELER_OFF:
		case KEY_AP_WING_LEVELER_ON:
		case KEY_AP_WING_LEVELER:
			AZS_TGL(AZS_AP_PITCH_HOLD);
			break;
		case KEY_TOGGLE_FLIGHT_DIRECTOR:
			AZS_SET(AZS_AP_CMD,1);
		break;
		case KEY_GEAR_TOGGLE:
			if(g_pSDAPI->GetN(GEAR_POS_NOSE)<10&&g_pSDAPI->GetN(GEAR_POS_LEFT)<10&&g_pSDAPI->GetN(GEAR_POS_RIGHT)<10&&!POS_GET(POS_MAIN_HYDRO_WORK))
				ImportTable.pPanels->trigger_key_event(KEY_GEAR_UP,0);
			if(g_pSDAPI->GetN(GEAR_POS_NOSE)>16300&&g_pSDAPI->GetN(GEAR_POS_LEFT)>16300&&g_pSDAPI->GetN(GEAR_POS_RIGHT)>16300&&!POS_GET(POS_MAIN_HYDRO_WORK))
				ImportTable.pPanels->trigger_key_event(KEY_GEAR_DOWN,0);

			KEY_SET(KEY_GEAR,1);
		break;   
		case KEY_SITUATION_RESET:
			SDLogicBase::InitAll();
			Load();
			break;
		case KEY_ELEV_TRIM_UP:
			if(POS_GET(POS_AP_PITCH_MODE)&&!POS_GET(POS_AP_HAND_MODE))return true;
			m_Htail->Dec(0);
			break;
		case KEY_ELEV_TRIM_DN:
			if(POS_GET(POS_AP_PITCH_MODE)&&!POS_GET(POS_AP_HAND_MODE))return true;
			m_Htail->Inc(0);
			break;
		case KEY_BRAKES:
			POS_SET(POS_BRAKE_TRIGGER,1);
			break;
		case KEY_FLAPS_DOWN:
		case KEY_FLAPS_UP:
			//if(g_pSDAPI->m_Config.m_CfgFlaps.m_bRealControl) {
			if(!POS_GET(POS_MAIN_HYDRO_WORK)) {
				m_Flaps->ReturnFlapTab(0);
				return true;
			} 
			m_Flaps->CalculateFlapTab();
			//}
			break;
		case KEY_FLAPS_INCR:
			//if(g_pSDAPI->m_Config.m_CfgFlaps.m_bRealControl) {
			if(!POS_GET(POS_MAIN_HYDRO_WORK)) {
				m_Flaps->ReturnFlapTab(1);
				return true;
			} 
			m_Flaps->CalculateFlapTab();
			/*
			} else {
			if(NDL_GET(NDL_MAIN_HYD)<g_pSDAPI->m_Config.m_CfgFlaps.m_dHydLimit) {
			m_Flaps->ReturnFlapTab(1);
			return true;
			}
			if(!m_Flaps->GetLeftPos()) {
			for(int i=0;i<20;i++)
			m_Flaps->CalculateFlapTab();
			} else {
			for(int i=0;i<15;i++)
			m_Flaps->CalculateFlapTab();
			}
			}
			*/
			break;
		case KEY_FLAPS_DECR:
			//if(g_pSDAPI->m_Config.m_CfgFlaps.m_bRealControl) {
			if(!POS_GET(POS_MAIN_HYDRO_WORK)) {
				m_Flaps->ReturnFlapTab(2);
				return true;
			} 
			m_Flaps->CalculateFlapTab();
			/*
			} else {
			if(NDL_GET(NDL_MAIN_HYD)<g_pSDAPI->m_Config.m_CfgFlaps.m_dHydLimit) {
			m_Flaps->ReturnFlapTab(2);
			return true;
			}
			if(m_Flaps->GetLeftPos()==35) {
			for(int i=0;i<15;i++)
			m_Flaps->CalculateFlapTab();
			} else {
			for(int i=0;i<20;i++)
			m_Flaps->CalculateFlapTab();
			}
			}
			*/
			break;
		case KEY_ENGINE_AUTO_START:
			for(int i=AZS_COM1_BUS;i<AZS_EXT_PWR;i++)
				AZS_SET(i,1);

			AZS_SET(AZS_POWERSOURCE,POWER_SOURCE_BAT);
			AZS_SET(AZS_INV1_36V,1);
			AZS_SET(AZS_INV2_36V,1);
			AZS_SET(AZS_INV1_115V,1);
			AZS_SET(AZS_INV2_115V,1);
			AZS_SET(AZS_GENERATOR1,1);
			AZS_SET(AZS_GENERATOR2,1);
			AZS_SET(AZS_GENERATOR3,1);
			AZS_SET(AZS_EMERG_ENG1_CUT,0);
			AZS_SET(AZS_EMERG_ENG2_CUT,0);
			AZS_SET(AZS_EMERG_ENG3_CUT,0);
			AZS_SET(AZS_TOPL_LEV,1);
			AZS_SET(AZS_TOPL_PRAV,1);
			AZS_SET(AZS_PKAI251,3);
			AZS_SET(AZS_PKAI252,3);
			AZS_SET(AZS_PKAI253,3);
			AZS_SET(AZS_MANOM,1);
			AZS_SET(AZS_RADALT,1);
			GLT_SET(GLT_ARK1,1);
			GLT_SET(GLT_ARK2,1);
			AZS_SET(AZS_KMP1PWR,1);
			AZS_SET(AZS_KMP2PWR,1);
			AZS_SET(AZS_SO72PWR,1);
			break;
		case KEY_THROTTLE_DECR:
		case KEY_THROTTLE_DECR_SMALL:
		case KEY_DECREASE_THROTTLE:
			POS_SET(POS_RUD1_STOP,0);
			POS_SET(POS_RUD2_STOP,0);
			POS_SET(POS_RUD3_STOP,0);
			if(g_pSDAPI->GetN(GENERAL_ENGINE1_THROTTLE_LEVER_POS)==0&&g_pSDAPI->GetN(GENERAL_ENGINE2_THROTTLE_LEVER_POS)==0&&g_pSDAPI->GetN(GENERAL_ENGINE3_THROTTLE_LEVER_POS)==0)
				m_RTU->Process();
			break;
		case KEY_ENGINE:
			m_EnginePressed=true;
		break;
		case KEY_SELECT_1:
			if(m_EnginePressed) {
				ImportTable.pGlobal->engine_control_select=1;
				m_EnginePressed=false;
			}
		break;
		case KEY_SELECT_2:
			if(m_EnginePressed) {
				ImportTable.pGlobal->engine_control_select=2;
				m_EnginePressed=false;
			}
		break;
		case KEY_SELECT_3:
			if(m_EnginePressed) {
				ImportTable.pGlobal->engine_control_select=4;
				m_EnginePressed=false;
			}
		break;
		case KEY_SELECT_4:
			if(m_EnginePressed) {
				ImportTable.pGlobal->engine_control_select=7;
				m_EnginePressed=false;
			}
		break;
		default:
			POS_SET(POS_BRAKE_TRIGGER,0);
	}

	return true;
}

void SDGlobal::PrepareSimSaveFiles()
{
	std::string tmp;
	tmp=g_pSDAPI->m_SimPath;
	tmp+=SIM_MODULES_DIR+"\\"+SIM_FSUI_MODULE;
	GetStringFromResource(tmp,UI_GENERATEDFLIGHT_STRING_RES,m_UIGeneratedFlight,SIM_FLT_GENERATED);
	m_UIGeneratedFlight+="."+SIM_FLT_EXTENSION;
	tmp=g_pSDAPI->m_SimPath;
	tmp+=SIM_LANG_MODULE;
	GetStringFromResource(tmp,PREVIOUSFLIGHT_STRING_RES,m_PrevoiusFlight,SIM_FLT_PREVIOUS);
	m_PrevoiusFlight+="."+SIM_FLT_EXTENSION;
}

void SDGlobal::UpdateVCAZS()
{
	static int m_Tick=0;

	if(GetActiveViewMode()!=VIEW_MODE_VIRTUAL_COCKPIT)
		return;

	m_Tick=++m_Tick%2;

	switch(m_Tick) {
		case 0:
			m_AzsVC[  0]->set_number(AZS_GET(AZS_FIRE_SOUND_ALARM		));
			m_AzsVC[  1]->set_number(AZS_GET(AZS_GEAR_WARNING_COV		));
			m_AzsVC[  2]->set_number(AZS_GET(AZS_GEAR_WARNING			));
			m_AzsVC[  3]->set_number(AZS_GET(AZS_AUTO_SKID				));
			m_AzsVC[  4]->set_number(AZS_GET(AZS_ADI_BACKUP_POWER_COV	));
			m_AzsVC[  5]->set_number(AZS_GET(AZS_ADI_BACKUP_POWER		));
			m_AzsVC[  6]->set_number(AZS_GET(AZS_APU_FUEL_VALVE			));
			m_AzsVC[  7]->set_number(AZS_GET(AZS_WARNING_LIGHTS			));
			m_AzsVC[  8]->set_number(AZS_GET(AZS_EMERG_EXIT_LIGHTS		));
			m_AzsVC[  9]->set_number(AZS_GET(AZS_WHITE_DECK_FLOOD_LIGHT	));
			m_AzsVC[ 10]->set_number(AZS_GET(AZS_RED_PANEL_LIGHT		));
			m_AzsVC[ 11]->set_number(!AZS_GET(AZS_FUEL_IND)?1:AZS_GET(AZS_FUEL_IND)==1?0:2);
			m_AzsVC[ 12]->set_number(AZS_GET(AZS_POWERSOURCE)==POWER_SOURCE_BAT?0:AZS_GET(AZS_POWERSOURCE)==POWER_SOURCE_OFF?1:2);
			m_AzsVC[ 13]->set_number(AZS_GET(AZS_INV1_36V				));
			m_AzsVC[ 14]->set_number(AZS_GET(AZS_INV2_36V				));
			m_AzsVC[ 15]->set_number(AZS_GET(AZS_115V_BCKUP_COV			));
			m_AzsVC[ 16]->set_number(AZS_GET(AZS_115V_BCKUP				));
			m_AzsVC[ 17]->set_number(AZS_GET(AZS_INV1_115V				));
			m_AzsVC[ 18]->set_number(AZS_GET(AZS_INV2_115V				));
			m_AzsVC[ 19]->set_number(AZS_GET(AZS_AIR_PRESS_DROP_EMERG_COV));
			m_AzsVC[ 20]->set_number(AZS_GET(AZS_AIR_PRESS_DROP_EMERG	));
			m_AzsVC[ 21]->set_number(AZS_GET(AZS_AIR_PRESS_CONTROL_BCKUP_COV));
			m_AzsVC[ 22]->set_number(AZS_GET(AZS_AIR_PRESS_CONTROL_BCKUP));
			m_AzsVC[ 23]->set_number(AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG_COV));
			m_AzsVC[ 24]->set_number(AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG)==0?1:AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG)==1?0:2);
			m_AzsVC[ 25]->set_number(AZS_GET(AZS_AIR_PRESS_SYS_SOUND_ALARM));
			m_AzsVC[ 26]->set_number(AZS_GET(AZS_AIR_PRESS_SYS_MASTER	)==0?1:AZS_GET(AZS_AIR_PRESS_SYS_MASTER)==1?0:2);
			m_AzsVC[ 27]->set_number(AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN)==0?0:AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN)==1?2:AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN)==2?3:1);
			m_AzsVC[ 28]->set_number(AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT)==0?0:AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT)==1?2:AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT)==2?3:1);
			m_AzsVC[ 29]->set_number(AZS_GET(AZS_BLEEDAIR_EXTRACT_VALVE	)==0?0:AZS_GET(AZS_BLEEDAIR_EXTRACT_VALVE	)==1?2:AZS_GET(AZS_BLEEDAIR_EXTRACT_VALVE	)==2?3:1);
			m_AzsVC[ 30]->set_number(AZS_GET(AZS_BLEEDAIR_EXTRACT_RATIO	));
			m_AzsVC[ 31]->set_number(AZS_GET(AZS_DECK_COND				));
			m_AzsVC[ 32]->set_number(AZS_GET(AZS_STAIRS_PWR				));
			m_AzsVC[ 33]->set_number(AZS_GET(AZS_STAIRS_COV				));
			m_AzsVC[ 34]->set_number(AZS_GET(AZS_STAIRS					));
			m_AzsVC[ 35]->set_number(AZS_GET(AZS_NO_SMOKING_SIGNS		));
			m_AzsVC[ 36]->set_number(AZS_GET(AZS_REDLIGHT1				));
			m_AzsVC[ 37]->set_number(AZS_GET(AZS_REDLIGHT2				));
			m_AzsVC[ 38]->set_number(AZS_GET(AZS_GENERATOR1				));
			m_AzsVC[ 39]->set_number(AZS_GET(AZS_GENERATOR2				));
			m_AzsVC[ 40]->set_number(AZS_GET(AZS_GENERATOR3				));
			m_AzsVC[ 41]->set_number(AZS_GET(AZS_EMERG_ENG1_CUT_COV		));
			m_AzsVC[ 42]->set_number(AZS_GET(AZS_EMERG_ENG1_CUT			));
			m_AzsVC[ 43]->set_number(AZS_GET(AZS_EMERG_ENG2_CUT_COV		));
			m_AzsVC[ 44]->set_number(AZS_GET(AZS_EMERG_ENG2_CUT			));
			m_AzsVC[ 45]->set_number(AZS_GET(AZS_EMERG_ENG3_CUT_COV		));
			m_AzsVC[ 46]->set_number(AZS_GET(AZS_EMERG_ENG3_CUT			));
			m_AzsVC[ 47]->set_number(AZS_GET(AZS_ACT					));
			m_AzsVC[ 48]->set_number(AZS_GET(AZS_TOPL_LEV				));
			m_AzsVC[ 49]->set_number(AZS_GET(AZS_TOPL_PRAV				));
			m_AzsVC[ 50]->set_number(AZS_GET(AZS_TOPL_PRAV_AVAR_COV		));
			m_AzsVC[ 51]->set_number(AZS_GET(AZS_TOPL_PRAV_AVAR			));
			m_AzsVC[ 52]->set_number(AZS_GET(AZS_MANOM					));
			m_AzsVC[ 53]->set_number(AZS_GET(AZS_STAB_MAIN_CONTR_COV	));
			m_AzsVC[ 54]->set_number(AZS_GET(AZS_STAB_MAIN_CONTR		));
			m_AzsVC[ 55]->set_number(AZS_GET(AZS_EMERG_GEAR_COV			));
			m_AzsVC[ 56]->set_number(AZS_GET(AZS_EMERG_GEAR				));
			m_AzsVC[ 57]->set_number(AZS_GET(AZS_FLAPS_COV				));
			m_AzsVC[ 58]->set_number(AZS_GET(AZS_FLAPS					));
			m_AzsVC[ 59]->set_number(AZS_GET(AZS_EMERG_FLAPS_COV		));
			m_AzsVC[ 60]->set_number(AZS_GET(AZS_EMERG_FLAPS			));
			m_AzsVC[ 61]->set_number(AZS_GET(AZS_FUEL_PUMP_LO			));
			m_AzsVC[ 62]->set_number(AZS_GET(AZS_XFEED_TANKS_COV		));
			m_AzsVC[ 63]->set_number(AZS_GET(AZS_XFEED_TANKS			));
			m_AzsVC[ 64]->set_number(AZS_GET(AZS_XFEED_PUMPS_COV		));
			m_AzsVC[ 65]->set_number(AZS_GET(AZS_XFEED_PUMPS			));
			m_AzsVC[ 66]->set_number(AZS_GET(AZS_HYD_PUMP_COV			));
			m_AzsVC[ 67]->set_number(AZS_GET(AZS_HYD_PUMP				));
			m_AzsVC[ 68]->set_number(AZS_GET(AZS_TRIM_ELERON)==0?1:AZS_GET(AZS_TRIM_ELERON)==1?0:2);
			m_AzsVC[ 69]->set_number(AZS_GET(AZS_TRIM_RUDDER)==0?1:AZS_GET(AZS_TRIM_RUDDER)==1?0:2);
			m_AzsVC[ 70]->set_number(AZS_GET(AZS_LLIGHTLEFT	)==0?1:AZS_GET(AZS_LLIGHTLEFT	)==1?2:0);
			m_AzsVC[ 71]->set_number(AZS_GET(AZS_LLIGHTRIGHT)==0?1:AZS_GET(AZS_LLIGHTRIGHT  )==1?2:0);
			m_AzsVC[ 72]->set_number(AZS_GET(AZS_LLIGHTSMOTOR			));
			m_AzsVC[ 73]->set_number(AZS_GET(AZS_LIGHTSNAV	));
			m_AzsVC[ 74]->set_number(AZS_GET(AZS_LIGHTSBCN	));
			m_AzsVC[ 75]->set_number(AZS_GET(AZS_PKAI251_COV			));
			break;
		case 1:
			m_AzsVC[ 76]->set_number(AZS_GET(AZS_PKAI251)==2||AZS_GET(AZS_PKAI251)==3?1:0);
			m_AzsVC[ 77]->set_number(AZS_GET(AZS_PKAI252_COV			));
			m_AzsVC[ 78]->set_number(AZS_GET(AZS_PKAI252)==2||AZS_GET(AZS_PKAI252)==3?1:0);
			m_AzsVC[ 79]->set_number(AZS_GET(AZS_PKAI253_COV			));
			m_AzsVC[ 80]->set_number(AZS_GET(AZS_PKAI253)==2||AZS_GET(AZS_PKAI253)==3?1:0);
			m_AzsVC[ 81]->set_number(AZS_GET(AZS_WINDOWHEATL			));
			m_AzsVC[ 82]->set_number(AZS_GET(AZS_WINDOWHEATR			));
			m_AzsVC[ 83]->set_number(AZS_GET(AZS_WINDOWHEATPWR			));
			m_AzsVC[ 84]->set_number(AZS_GET(AZS_ARK1LEFTRIGHT			));
			m_AzsVC[ 85]->set_number(AZS_GET(AZS_ARK2LEFTRIGHT			));
			m_AzsVC[ 86]->set_number(AZS_GET(AZS_RADALT					));
			m_AzsVC[ 87]->set_number(AZS_GET(AZS_STARTAI9PWR			));
			m_AzsVC[ 88]->set_number(AZS_GET(AZS_STARTAI9MODE)==0?0:AZS_GET(AZS_STARTAI9MODE)==1?2:AZS_GET(AZS_STARTAI9MODE)==2?3:1);
			m_AzsVC[ 89]->set_number(AZS_GET(AZS_STARTAI25PWR			));
			m_AzsVC[153]->set_number(AZS_GET(AZS_TURBO_COOLER_UNIT_BUS	));
			m_AzsVC[ 90]->set_number(AZS_GET(AZS_STARTAI25MODE			));
			m_AzsVC[ 91]->set_number(AZS_GET(AZS_STARTAI25SEL			));
			m_AzsVC[ 92]->set_number(AZS_GET(AZS_ENG1HEAT				));
			m_AzsVC[ 93]->set_number(AZS_GET(AZS_ENG2HEAT				));
			m_AzsVC[ 94]->set_number(AZS_GET(AZS_ENG3HEAT				));
			m_AzsVC[ 95]->set_number(AZS_GET(AZS_PITOTHEAT1				));
			m_AzsVC[ 96]->set_number(AZS_GET(AZS_PITOTHEAT2				));
			m_AzsVC[ 97]->set_number(AZS_GET(AZS_ICEWARN1				));
			m_AzsVC[ 98]->set_number(AZS_GET(AZS_ICEWARN2				));
			m_AzsVC[154]->set_number(AZS_GET(AZS_NOSE_LEG_STEER_BUS		));
			m_AzsVC[ 99]->set_number(AZS_GET(AZS_COM1_BUS				));
			m_AzsVC[100]->set_number(AZS_GET(AZS_CAPT_VSI_BUS			));
			m_AzsVC[101]->set_number(AZS_GET(AZS_CAPT_ADI_MAIN_BUS		));
			m_AzsVC[102]->set_number(AZS_GET(AZS_FUEL_IND_BUS			));
			m_AzsVC[103]->set_number(AZS_GET(AZS_LAMP_TEST_BUS			));
			m_AzsVC[104]->set_number(AZS_GET(AZS_ENG1_IND_BUS			));
			m_AzsVC[105]->set_number(AZS_GET(AZS_ENG2_IND_BUS			));
			m_AzsVC[106]->set_number(AZS_GET(AZS_ENG3_IND_BUS			));
			m_AzsVC[107]->set_number(AZS_GET(AZS_CAPT_ADI_BACKUP_BUS	));
			m_AzsVC[155]->set_number(AZS_GET(AZS_FUEL_X_FEED9_BUS		));
			m_AzsVC[108]->set_number(AZS_GET(AZS_ADF1_BUS				));
			m_AzsVC[109]->set_number(AZS_GET(AZS_XPDR_BUS				));
			m_AzsVC[110]->set_number(AZS_GET(AZS_ELEC_36V_BACKUP_BUS	));
			m_AzsVC[111]->set_number(AZS_GET(AZS_INV1_115V_BUS			));
			m_AzsVC[112]->set_number(AZS_GET(AZS_INV2_115V_BUS			));
			m_AzsVC[113]->set_number(AZS_GET(AZS_AUTO_BACKUP_115V_BUS	));
			m_AzsVC[114]->set_number(AZS_GET(AZS_ENG1_FIRE_ALARM_BUS	));
			m_AzsVC[115]->set_number(AZS_GET(AZS_ENG2_FIRE_ALARM_BUS	));
			m_AzsVC[116]->set_number(AZS_GET(AZS_ENG3_FIRE_ALARM_BUS	));
			m_AzsVC[156]->set_number(AZS_GET(AZS_ENG1_DE_ICE_BUS		));
			m_AzsVC[117]->set_number(AZS_GET(AZS_STALL_WARN_BUS			));
			m_AzsVC[118]->set_number(AZS_GET(AZS_FUEL_PUMP_AUTO_BACKUP_BUS));
			m_AzsVC[119]->set_number(AZS_GET(AZS_FUEL_X_FEED8_BUS		));
			m_AzsVC[120]->set_number(AZS_GET(AZS_ENG1_FUEL_VALVE_BUS	));
			m_AzsVC[121]->set_number(AZS_GET(AZS_ENG2_FUEL_VALVE_BUS	));
			m_AzsVC[122]->set_number(AZS_GET(AZS_ENG3_FUEL_VALVE_BUS	));
			m_AzsVC[123]->set_number(AZS_GET(AZS_FIRE_EXT_1_BUS			));
			m_AzsVC[124]->set_number(AZS_GET(AZS_FIRE_EXT_2_BUS			));
			m_AzsVC[125]->set_number(AZS_GET(AZS_FIRE_EXT_3_BUS			));
			m_AzsVC[157]->set_number(AZS_GET(AZS_ENG2_DE_ICE_BUS		));
			m_AzsVC[126]->set_number(AZS_GET(AZS_AIR_STARTER_IND_BUS	));
			m_AzsVC[127]->set_number(AZS_GET(AZS_GEAR_UP_HORN_BUS		));
			m_AzsVC[128]->set_number(AZS_GET(AZS_APU_IGNIT_BUS			));
			m_AzsVC[129]->set_number(AZS_GET(AZS_ENG1_IGNIT_BUS			));
			m_AzsVC[130]->set_number(AZS_GET(AZS_ENG2_IGNIT_BUS			));
			m_AzsVC[131]->set_number(AZS_GET(AZS_ENG3_IGNIT_BUS			));
			m_AzsVC[132]->set_number(AZS_GET(AZS_FIRE_EXT1_VALVE_BUS	));
			m_AzsVC[133]->set_number(AZS_GET(AZS_FIRE_EXT2_VALVE_BUS	));
			m_AzsVC[134]->set_number(AZS_GET(AZS_FIRE_EXT3_VALVE_BUS	));
			m_AzsVC[158]->set_number(AZS_GET(AZS_ENG3_DE_ICE_BUS		));
			m_AzsVC[135]->set_number(AZS_GET(AZS_COM2_BUS				));
			m_AzsVC[136]->set_number(AZS_GET(AZS_GROUND_PWR_BUS			));
			m_AzsVC[137]->set_number(AZS_GET(AZS_COPLT_ADI_BUS			));
			m_AzsVC[138]->set_number(AZS_GET(AZS_GMK_COURSE_SYS_BUS		));
			m_AzsVC[139]->set_number(AZS_GET(AZS_HYDRO_SYS_BUS			));
			m_AzsVC[140]->set_number(AZS_GET(AZS_AUTOPILOT_BUS			));
			m_AzsVC[141]->set_number(AZS_GET(AZS_GEAR_IND_BUS			));
			m_AzsVC[142]->set_number(AZS_GET(AZS_SIGNAL_ROCKETS_BUS		));
			m_AzsVC[143]->set_number(AZS_GET(AZS_ILS_SYS_BUS			));
			m_AzsVC[159]->set_number(AZS_GET(AZS_BRAKE_SYS_BUS			));
			m_AzsVC[144]->set_number(AZS_GET(AZS_ADF2_BUS				));
			m_AzsVC[145]->set_number(AZS_GET(AZS_INV_115V_EMERG_SWITCH_BUS));
			m_AzsVC[146]->set_number(AZS_GET(AZS_STAB_EMERG_BUS			));
			m_AzsVC[147]->set_number(AZS_GET(AZS_FLAPS_EMERG_BUS		));
			m_AzsVC[148]->set_number(AZS_GET(AZS_GEAR_EMERG_BUS			));
			m_AzsVC[149]->set_number(AZS_GET(AZS_ANTI_ICE_SYS_BUS		));
			m_AzsVC[150]->set_number(AZS_GET(AZS_EXTREME_BANK_IND_BUS	));
			m_AzsVC[151]->set_number(AZS_GET(AZS_AIR_COND_SYS_BUS		));
			m_AzsVC[152]->set_number(AZS_GET(AZS_STAIRS_SERVO_BUS		));
			m_AzsVC[160]->set_number(AZS_GET(AZS_DECK_HEATER_BUS		));
			m_AzsVC[161]->set_number(AZS_GET(AZS_CABIN_HEATER_BUS		));
			m_AzsVC[162]->set_number(AZS_GET(AZS_TAXI_LIGHT_L_BUS		));
			m_AzsVC[163]->set_number(AZS_GET(AZS_LANDING_LIGHT_L_BUS	));
			m_AzsVC[164]->set_number(AZS_GET(AZS_TAXI_LIGHT_R_BUS		));
			m_AzsVC[165]->set_number(AZS_GET(AZS_LANDING_LIGHT_R_BUS	));
			m_AzsVC[166]->set_number(AZS_GET(AZS_ALER_TRIM_BUS			));
			m_AzsVC[167]->set_number(AZS_GET(AZS_RUDDER_TRIM_BUS		));
			m_AzsVC[168]->set_number(AZS_GET(AZS_STAB_MAIN_BUS			));
			m_AzsVC[169]->set_number(AZS_GET(AZS_FLAPS_MAIN_BUS			));
			m_AzsVC[170]->set_number(AZS_GET(AZS_GEAR_MAIN_BUS			));
			m_AzsVC[171]->set_number(AZS_GET(AZS_AP_PWR					));
			m_AzsVC[172]->set_number(AZS_GET(AZS_AP_PITCH_HOLD			));
			break;
	}

	m_GltVC[0]->set_number(GLT_GET(GLT_SGU6));
	m_GltVC[1]->set_number(GLT_GET(GLT_SGU7));
	m_GltVC[2]->set_number(GLT_GET(GLT_XPDRMODE));
	m_GltVC[3]->set_number(GLT_GET(GLT_TEMP_DUCT));
	m_GltVC[4]->set_number(GLT_GET(GLT_VOLT27));
	m_GltVC[5]->set_number(GLT_GET(GLT_VOLT36));
	m_GltVC[6]->set_number(GLT_GET(GLT_VOLT115));

}
double SDGlobal::GetTimeOfDay() 
{
	double i=g_pSDAPI->GetN(CLOCK_HOUR);
	if(i>=6&&i<7)
		return TIME_OF_DAY_DAWN;
	if(i>=7&&i<20)
		return TIME_OF_DAY_DAY;
	if(i>=20&&i<21)
		return TIME_OF_DAY_DUSK;
	return TIME_OF_DAY_NIGHT;	
}

void SDGlobal::LoadPax(CIniFile *ini)
{
	SetPayloadWeightKg(SEAT_PILOT     ,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX01",GetPayloadWeightKg(SEAT_PILOT)     ));
	SetPayloadWeightKg(SEAT_COPILOT   ,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX02",GetPayloadWeightKg(SEAT_COPILOT)   ));
	SetPayloadWeightKg(SEAT_ENGINEER  ,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX03",GetPayloadWeightKg(SEAT_ENGINEER)  ));
	SetPayloadWeightKg(SEAT_ROW1_SEAT1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX04",GetPayloadWeightKg(SEAT_ROW1_SEAT1)));
	SetPayloadWeightKg(SEAT_ROW1_SEAT2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX05",GetPayloadWeightKg(SEAT_ROW1_SEAT2)));
	SetPayloadWeightKg(SEAT_ROW1_SEAT3,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX06",GetPayloadWeightKg(SEAT_ROW1_SEAT3)));
	SetPayloadWeightKg(SEAT_ROW2_SEAT1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX07",GetPayloadWeightKg(SEAT_ROW2_SEAT1)));
	SetPayloadWeightKg(SEAT_ROW2_SEAT2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX08",GetPayloadWeightKg(SEAT_ROW2_SEAT2)));
	SetPayloadWeightKg(SEAT_ROW2_SEAT3,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX09",GetPayloadWeightKg(SEAT_ROW2_SEAT3)));
	SetPayloadWeightKg(SEAT_ROW3_SEAT1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX10",GetPayloadWeightKg(SEAT_ROW3_SEAT1)));
	SetPayloadWeightKg(SEAT_ROW3_SEAT2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX11",GetPayloadWeightKg(SEAT_ROW3_SEAT2)));
	SetPayloadWeightKg(SEAT_ROW3_SEAT3,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX12",GetPayloadWeightKg(SEAT_ROW3_SEAT3)));
	SetPayloadWeightKg(SEAT_ROW4_SEAT1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX13",GetPayloadWeightKg(SEAT_ROW4_SEAT1)));
	SetPayloadWeightKg(SEAT_ROW4_SEAT2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX14",GetPayloadWeightKg(SEAT_ROW4_SEAT2)));
	SetPayloadWeightKg(SEAT_ROW4_SEAT3,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX15",GetPayloadWeightKg(SEAT_ROW4_SEAT3)));
	SetPayloadWeightKg(SEAT_ROW5_SEAT1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX16",GetPayloadWeightKg(SEAT_ROW5_SEAT1)));
	SetPayloadWeightKg(SEAT_ROW5_SEAT2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX17",GetPayloadWeightKg(SEAT_ROW5_SEAT2)));
	SetPayloadWeightKg(SEAT_ROW5_SEAT3,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX18",GetPayloadWeightKg(SEAT_ROW5_SEAT3)));
	SetPayloadWeightKg(SEAT_ROW6_SEAT1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX19",GetPayloadWeightKg(SEAT_ROW6_SEAT1)));
	SetPayloadWeightKg(SEAT_ROW6_SEAT2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX20",GetPayloadWeightKg(SEAT_ROW6_SEAT2)));
	SetPayloadWeightKg(SEAT_ROW6_SEAT3,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX21",GetPayloadWeightKg(SEAT_ROW6_SEAT3)));
	SetPayloadWeightKg(SEAT_ROW7_SEAT1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX22",GetPayloadWeightKg(SEAT_ROW7_SEAT1)));
	SetPayloadWeightKg(SEAT_ROW7_SEAT2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX23",GetPayloadWeightKg(SEAT_ROW7_SEAT2)));
	SetPayloadWeightKg(SEAT_ROW7_SEAT3,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX24",GetPayloadWeightKg(SEAT_ROW7_SEAT3)));
	SetPayloadWeightKg(SEAT_ROW8_SEAT1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX25",GetPayloadWeightKg(SEAT_ROW8_SEAT1)));
	SetPayloadWeightKg(SEAT_ROW8_SEAT2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX26",GetPayloadWeightKg(SEAT_ROW8_SEAT2)));
	SetPayloadWeightKg(SEAT_ROW8_SEAT3,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX27",GetPayloadWeightKg(SEAT_ROW8_SEAT3)));
	SetPayloadWeightKg(SEAT_ROW9_SEAT1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX28",GetPayloadWeightKg(SEAT_ROW9_SEAT1)));
	SetPayloadWeightKg(SEAT_ROW9_SEAT2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX29",GetPayloadWeightKg(SEAT_ROW9_SEAT2)));
	SetPayloadWeightKg(SEAT_ROW9_SEAT3,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX30",GetPayloadWeightKg(SEAT_ROW9_SEAT3)));
	SetLeftTankQuantityKg(			   ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYLT"   ,GetLeftTankQuantityKg()			));
	SetRightTankQuantityKg(			   ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYRT"   ,GetRightTankQuantityKg()			));
	SetPayloadWeightKg(SEAT_EQUIP	  ,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYE"    ,GetPayloadWeightKg(SEAT_EQUIP)	    ));
	SetPayloadWeightKg(SEAT_CARGO	  ,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"PAYC"    ,GetPayloadWeightKg(SEAT_CARGO)	    ));
}																																

void SDGlobal::SavePax(CIniFile *ini)
{
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX01",GetPayloadWeightKg(SEAT_PILOT)     );
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX02",GetPayloadWeightKg(SEAT_COPILOT)   );
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX03",GetPayloadWeightKg(SEAT_ENGINEER)  );
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX04",GetPayloadWeightKg(SEAT_ROW1_SEAT1));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX05",GetPayloadWeightKg(SEAT_ROW1_SEAT2));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX06",GetPayloadWeightKg(SEAT_ROW1_SEAT3));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX07",GetPayloadWeightKg(SEAT_ROW2_SEAT1));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX08",GetPayloadWeightKg(SEAT_ROW2_SEAT2));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX09",GetPayloadWeightKg(SEAT_ROW2_SEAT3));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX10",GetPayloadWeightKg(SEAT_ROW3_SEAT1));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX11",GetPayloadWeightKg(SEAT_ROW3_SEAT2));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX12",GetPayloadWeightKg(SEAT_ROW3_SEAT3));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX13",GetPayloadWeightKg(SEAT_ROW4_SEAT1));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX14",GetPayloadWeightKg(SEAT_ROW4_SEAT2));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX15",GetPayloadWeightKg(SEAT_ROW4_SEAT3));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX16",GetPayloadWeightKg(SEAT_ROW5_SEAT1));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX17",GetPayloadWeightKg(SEAT_ROW5_SEAT2));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX18",GetPayloadWeightKg(SEAT_ROW5_SEAT3));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX19",GetPayloadWeightKg(SEAT_ROW6_SEAT1));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX20",GetPayloadWeightKg(SEAT_ROW6_SEAT2));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX21",GetPayloadWeightKg(SEAT_ROW6_SEAT3));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX22",GetPayloadWeightKg(SEAT_ROW7_SEAT1));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX23",GetPayloadWeightKg(SEAT_ROW7_SEAT2));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX24",GetPayloadWeightKg(SEAT_ROW7_SEAT3));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX25",GetPayloadWeightKg(SEAT_ROW8_SEAT1));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX26",GetPayloadWeightKg(SEAT_ROW8_SEAT2));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX27",GetPayloadWeightKg(SEAT_ROW8_SEAT3));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX28",GetPayloadWeightKg(SEAT_ROW9_SEAT1));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX29",GetPayloadWeightKg(SEAT_ROW9_SEAT2));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYPAX30",GetPayloadWeightKg(SEAT_ROW9_SEAT3));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYLT"   ,GetLeftTankQuantityKg());
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYRT"   ,GetRightTankQuantityKg());
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYE"    ,GetPayloadWeightKg(SEAT_EQUIP));
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"PAYC"    ,GetPayloadWeightKg(SEAT_CARGO));
}
