/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 3 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Common/CommonSys.h"
#include "../Common/CommonAircraft.h"
#include "../Common/CommonLogic.h"
#include "../Common/Macros.h"
#include "../../res/Version.h"
#include "../../res/2d/pa_res.h"
#include "../api/ServerDef.h"
#include "../api/APIEntry.h"
#include "../api/Api.h"
#include "../Lib/Undoc.h"
#include "../Lib/EString.h"

