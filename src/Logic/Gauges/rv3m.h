/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/rv3m.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"


class SDRV3MGauge : public SDLogicBase
{
private:
	bool m_SignalStarted;
	int m_Timer;
	bool m_bAltReached;
	bool m_NotVisible;

	auto_ptr<CNamedVar> VCAnimNdl;
	auto_ptr<CNamedVar> VCAnimDH;

	double GetXmlAltRus(double ndl);
	double GetXmlAltEng(double ndl);

public:
	SDRV3MGauge();
	virtual ~SDRV3MGauge();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

};
