/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/vs.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "vs.h"

SDVSGauge::SDVSGauge() 
{
}

SDVSGauge::~SDVSGauge()
{
}

void SDVSGauge::Init()
{
	XmlAnimVS30=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_var30",ImportTable.pPanels));
	XmlAnimVS10=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_var10",ImportTable.pPanels));
}

void SDVSGauge::Update()
{
	NDL_SET(NDL_VS,g_pSDAPI->GetN(VERTICAL_SPEED)/256);
	NDL_SET(NDL_VS_EN,METER_TO_FEET(NDL_GET(NDL_VS)));

	XmlAnimVS30->set_degree(dgrd(NDL_GET(NDL_VS)*3*2));
	XmlAnimVS10->set_degree(dgrd(NDL_GET(NDL_VS_SRD)*9));
}

void SDVSGauge::Load(CIniFile *ini)
{
}

void SDVSGauge::Save(CIniFile *ini)
{
}
