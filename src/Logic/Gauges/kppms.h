/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/kppms.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

class SDKPPMSGauge : public SDLogicBase
{
private:
	int m_Nav;
	auto_ptr<CNamedVar> xml11;
	auto_ptr<CNamedVar> xml21;
	auto_ptr<CNamedVar> xml2;
	auto_ptr<CNamedVar> VCAnimHdg1;
	auto_ptr<CNamedVar> VCAnimHdg2;
	auto_ptr<CNamedVar> VCAnimScl1;
	auto_ptr<CNamedVar> VCAnimScl2;
	auto_ptr<CNamedVar> VCAnimGlide1;
	auto_ptr<CNamedVar> VCAnimGlide2;
	auto_ptr<CNamedVar> VCAnimCourse1;
	auto_ptr<CNamedVar> VCAnimCourse2;

protected:
	int blenker_kurs;
	int blenker_gliss;
	double ils_kurs;
	double ils_gliss;
	double scale;
	bool have_course;
	bool have_glide;

	bool m_LeftPlayed;
	bool m_RightPlayed;
	bool m_UpPlayed;
	bool m_DownPlayed;
	bool m_Ils0Played;

public:
	SDKPPMSGauge();
	virtual ~SDKPPMSGauge();

	void SetNav(int _nav){m_Nav=_nav;};

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

};
