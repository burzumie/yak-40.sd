/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/da30.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "da30.h"

SDDA30Gauge::SDDA30Gauge() 
{
}

SDDA30Gauge::~SDDA30Gauge()
{
}

void SDDA30Gauge::Init()
{
	xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_509",ImportTable.pPanels));
	xml2=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_510",ImportTable.pPanels));
	xml3=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_530",ImportTable.pPanels));
	VCAnimVS=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_da30_vs",ImportTable.pPanels));
	VCAnimTurn=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_da30_turn",ImportTable.pPanels));
	VCAnimBall=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_da30_ball",ImportTable.pPanels));
}

void SDDA30Gauge::Update()
{
	NDL_SET(NDL_DA30_BALL,g_pSDAPI->GetN(TURN_COORDINATOR_BALL_POS)*-1);
	if(PWR_GET(PWR_BUS36)&&AZS_GET(AZS_CAPT_VSI_BUS)) {
		NDL_SET(NDL_DA30_TURN,g_pSDAPI->GetXML(XML_DELTA_HEADING_RATE,"rpm"));
	} else {
		NDL_SET(NDL_DA30_TURN,0);
	}
	xml1->set_number(NDL_GET(NDL_DA30_TURN));
	xml2->set_number(NDL_GET(NDL_DA30_BALL));
	xml3->set_number(NDL_GET(NDL_VS));
	VCAnimTurn->set_degree(dgrd(NDL_GET(NDL_DA30_TURN)*36));
	VCAnimVS->set_degree(dgrd(SetVS()));
	VCAnimBall->set_degree(dgrd(NDL_GET(NDL_DA30_BALL)*0.0826));

}

double SDDA30Gauge::SetVS()
{
	double var=NDL_GET(NDL_VS);

	if(var>0) {
		if(var >= 0  && var <= 5 )		return var*8.17;
		if(var >  5  && var <= 10)		return 5*8.17+(var-5)*8.06;
		if(var >  10 && var <= 20)		return 5*8.17+(10-5)*8.06+(var-10)*5.75;
		if(var >  20 && var <= 30)		return 5*8.17+(10-5)*8.06+(20-10)*5.75+(var-20)*4.07;
	} else if(var<0) {
		var=fabs(var);

		if(var >= 0  && var <= 5 )		return (var*8.16)*-1;
		if(var >  5  && var <= 10)		return (5*8.16+(var-5)*8.04)*-1;
		if(var >  10 && var <= 20)		return (5*8.16+(10-5)*8.04+(var-10)*5.87)*-1;
		if(var >  20 && var <= 30)		return (5*8.16+(10-5)*8.04+(20-10)*5.87+(var-20)*4.03)*-1;
	}

	return 0;
}

void SDDA30Gauge::Load(CIniFile *ini)
{
}

void SDDA30Gauge::Save(CIniFile *ini)
{
}
