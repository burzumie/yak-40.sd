/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/iku.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "iku.h"

SDIKUGauge::SDIKUGauge() 
{
	adf1out=(double)(rand()%360);
	adf2out=(double)(rand()%360);
	nav1out=(double)(rand()%360);
	nav2out=(double)(rand()%360);
}

SDIKUGauge::~SDIKUGauge()
{
}

void SDIKUGauge::Init()
{
	xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_520",ImportTable.pPanels));
	xml2=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_521",ImportTable.pPanels));
	VCAnimRadcomp1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_radcomp1",ImportTable.pPanels));
	VCAnimRadcomp2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_radcomp2",ImportTable.pPanels));
	VCAnimIkuWhite=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_iku_white",ImportTable.pPanels));
	VCAnimIkuYellow=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_iku_yellow",ImportTable.pPanels));
	VCAnimIkuScale=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_iku_scale",ImportTable.pPanels));
}

void SDIKUGauge::ProcessNAV() 
{
	double vor_noise[2];

	SINT32 tick = (SINT32)g_pSDAPI->GetN(TICK18);
	vor_noise[0] = 0.5*((sin((double)tick/5)+0.05)*0.5+(sin((double)tick/23)-0.05)*0.8+sin((double)tick/111)*0.3+sin((double)tick/166)*0.3);
	vor_noise[1] = 0.5*((sin((double)tick/6)+0.05)*0.5+(sin((double)tick/15)-0.05)*0.8+sin((double)tick/132)*0.3+sin((double)tick/178)*0.3);

	if(AZS_GET(AZS_KMP1VOR)&&AZS_GET(AZS_KMP_RCVR)/*GLT_GET(GLT_KMPSYS)!=0&&GLT_GET(GLT_KMPSYS)!=2*/) {
		nav1out=g_pSDAPI->GetN(VOR1_BEARING_DEGREES);
	} else {
		nav1out+=vor_noise[0];
	}
	if(AZS_GET(AZS_KMP2VOR)&&AZS_GET(AZS_KMP_RCVR)/*GLT_GET(GLT_KMPSYS)!=0&&GLT_GET(GLT_KMPSYS)!=2*/) {
		nav2out=g_pSDAPI->GetN(VOR2_BEARING_DEGREES);
	} else {
		nav2out+=vor_noise[1];
	}
}

void SDIKUGauge::ProcessADF() 
{
	double adf_noise[2];

	SINT32 tick = (SINT32)g_pSDAPI->GetN(TICK18);
	adf_noise[0] = 0.5*((sin((double)tick/5)+0.05)*0.5+(sin((double)tick/23)-0.05)*0.8+sin((double)tick/111)*0.3+sin((double)tick/166)*0.3);
	adf_noise[1] = 0.5*((sin((double)tick/6)+0.05)*0.5+(sin((double)tick/15)-0.05)*0.8+sin((double)tick/132)*0.3+sin((double)tick/178)*0.3);

	if(GLT_GET(GLT_ARK1)>0&&AZS_GET(AZS_ADF1_BUS)&&PWR_GET(PWR_BUS27)&&PWR_GET(PWR_BUS36)) {
		int ark1;
		int ark1l=((int)GLT_GET(GLT_ARK1LEFT100)*100+(int)GLT_GET(GLT_ARK1LEFT10)*10+(int)GLT_GET(GLT_ARK1LEFT1))+100;
		int ark1r=((int)GLT_GET(GLT_ARK1RIGHT100)*100+(int)GLT_GET(GLT_ARK1RIGHT10)*10+(int)GLT_GET(GLT_ARK1RIGHT1))+100;
		if(!AZS_GET(AZS_ARK1LEFTRIGHT)) 
			ark1=ark1l;
		else 
			ark1=ark1r;
		ImportTable.pPanels->trigger_key_event(KEY_ADF_COMPLETE_SET, Dec2Bcd(ark1*10000));

		if(g_pSDAPI->GetXML(XML_ADF_SIGNAL,"number",1)>0) {
			adf1out=g_pSDAPI->GetXML(XML_ADF_RADIAL,"degrees",1);
		} else {
			adf1out+=adf_noise[0];
		}
	} 
	if(GLT_GET(GLT_ARK2)>0&&AZS_GET(AZS_ADF2_BUS)&&PWR_GET(PWR_BUS27)&&PWR_GET(PWR_BUS36)) {
		int ark2;
		int ark2l=((int)GLT_GET(GLT_ARK2LEFT100)*100+(int)GLT_GET(GLT_ARK2LEFT10)*10+(int)GLT_GET(GLT_ARK2LEFT1))+100;
		int ark2r=((int)GLT_GET(GLT_ARK2RIGHT100)*100+(int)GLT_GET(GLT_ARK2RIGHT10)*10+(int)GLT_GET(GLT_ARK2RIGHT1))+100;
		if(!AZS_GET(AZS_ARK2LEFTRIGHT)) 
			ark2=ark2l;
		else 
			ark2=ark2r;
		ImportTable.pPanels->trigger_key_event(KEY_ADF2_COMPLETE_SET, Dec2Bcd(ark2*10000));
		if(g_pSDAPI->GetXML(XML_ADF_SIGNAL,"number",2)>0) {
			adf2out=g_pSDAPI->GetXML(XML_ADF_RADIAL,"degrees",2);
		} else {
			adf2out+=adf_noise[1];
		}
	}
}

void SDIKUGauge::Update()
{
	FLAGS c;
	ProcessADF();
	ProcessNAV();
	NDL_SET(NDL_ARK1,adf1out);
	NDL_SET(NDL_ARK2,adf2out);
	switch((int)GLT_GET(GLT_IKU_LEFT)) {
		case 1:
			c=g_pSDAPI->GetF(VOR1_CODE);
			if(!(c&VOR_CODE_NAV_UNAVAILABLE)) {
				yellow=nav1out;
			}
			break;
		case 0:
			yellow=adf1out;
			break;
		case 3:
			yellow=adf1out;
			break;
		case 2:
			c=g_pSDAPI->GetF(VOR2_CODE);
			if(!(c&VOR_CODE_NAV_UNAVAILABLE)) {
				yellow=nav2out;
			}
			break;
	}
	switch((int)GLT_GET(GLT_IKU_RIGHT)) {
		case 1:
			c=g_pSDAPI->GetF(VOR1_CODE);
			if(!(c&VOR_CODE_NAV_UNAVAILABLE)) {
				white=nav1out;
			}
			break;
		case 0:
			white=adf1out;
			break;
		case 3:
			white=adf2out;
			break;
		case 2:
			c=g_pSDAPI->GetF(VOR2_CODE);
			if(!(c&VOR_CODE_NAV_UNAVAILABLE)) {
				white=nav2out;
			}
			break;
	}
	NDL_SET(NDL_IKU_WHITE,white);
	NDL_SET(NDL_IKU_YELLOW,yellow);

	xml1->set_number(NormaliseDegreeW<double>(NDL_GET(NDL_IKU_YELLOW)));
	xml2->set_number(NormaliseDegreeW<double>(NDL_GET(NDL_IKU_WHITE)));
	VCAnimRadcomp1->set_degree(dgrd(adf1out));
	VCAnimRadcomp2->set_degree(dgrd(adf2out));
	VCAnimIkuWhite->set_degree(dgrd(NDL_GET(NDL_IKU_WHITE)));
	VCAnimIkuYellow->set_degree(dgrd(NDL_GET(NDL_IKU_YELLOW)));
	VCAnimIkuScale->set_degree(dgrd(NDL_GET(NDL_GMK_PRICOURSE)/2*-1));
}

void SDIKUGauge::Load(CIniFile *ini)
{
}

void SDIKUGauge::Save(CIniFile *ini)
{
}
