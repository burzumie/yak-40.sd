/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/kus.cpp $

  Last modification:
    $Date: 21.02.06 0:14 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "kus.h"

SDKUSGauge::SDKUSGauge() 
{
}

SDKUSGauge::~SDKUSGauge()
{
}

void SDKUSGauge::Init()
{
	VCAnimIas=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kus_ias",ImportTable.pPanels));
	VCAnimTas=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kus_tas",ImportTable.pPanels));
}

void SDKUSGauge::Update()
{
	double a=g_pSDAPI->GetXML(XML_AIRSPEED_TRUE,"knots");
	double b=g_pSDAPI->GetXML(XML_AIRSPEED_INDICATED,"knots");
	NDL_SET(NDL_KUS_TAS,a*1.852);
	NDL_SET(NDL_KUS_IAS,b*1.852);
	NDL_SET(NDL_KUS_TAS_EN,a);
	NDL_SET(NDL_KUS_IAS_EN,b);

	if(!POS_GET(POS_PANEL_LANG)) {
		VCAnimIas->set_degree(dgrd(GetXmlIasRus(NDL_GET(NDL_KUS_IAS))));
		VCAnimTas->set_degree(dgrd(GetXmlTasRus(NDL_GET(NDL_KUS_TAS))));
	} else {
		VCAnimIas->set_degree(dgrd(GetXmlIasEng(NDL_GET(NDL_KUS_IAS_EN))));
		VCAnimTas->set_degree(dgrd(GetXmlTasEng(NDL_GET(NDL_KUS_TAS_EN))));
	}

}

double SDKUSGauge::GetXmlIasRus(double ndl)
{
	double var=ndl;

	if(var<=100)				return var*0.27;
	if(var>100  && var<=200)	return 100*0.27+( var-100)*0.55;
	if(var>200  && var<=400)	return 100*0.27+( 200-100)*0.55+( var-200)*0.535;
	if(var>400)              	return 100*0.27+( 200-100)*0.55+( 400-200)*0.535+( var-400)*0.4;

	return 0;
}

double SDKUSGauge::GetXmlTasRus(double ndl)
{
	double var=ndl;

	if(var<=400)				return var*0.055;
	if(var>400  && var<=500)	return 400*0.055+( var-400)*0.45;
	if(var>500  && var<=600)	return 400*0.055+( 500-400)*0.45+( var-500)*0.44;
	if(var>600)              	return 400*0.055+( 500-400)*0.45+( 600-500)*0.44+( var-600)*0.46;

	return 0;
}

double SDKUSGauge::GetXmlIasEng(double ndl)
{
	double var=ndl;

	if(var<=100)				return var*0.55;
	if(var>100  && var<=200)	return 100*0.55+( var-100)*1.35;
	if(var>200)              	return 100*0.55+( 200-100)*1.35+( var-200)*0.786;

	return 0;
}

double SDKUSGauge::GetXmlTasEng(double ndl)
{
	double var=ndl;

	if(var<=200)				return var*0.33;
	if(var>200  && var<=300)	return 200*0.33+( var-200)*0.44;
	if(var>300)             	return 200*0.33+( 300-200)*0.44+( var-300)*0.45;

	return 0;
}

void SDKUSGauge::Load(CIniFile *ini)
{
}

void SDKUSGauge::Save(CIniFile *ini)
{
}
