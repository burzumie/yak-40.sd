/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/gforce.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "gforce.h"

SDGforceGauge::SDGforceGauge() 
{
}

SDGforceGauge::~SDGforceGauge()
{
}

void SDGforceGauge::Init()
{
	xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_513",ImportTable.pPanels));
	xml2=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_511",ImportTable.pPanels));
	xml3=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_512",ImportTable.pPanels));

	xmlAnimMin=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_gforce_min",ImportTable.pPanels));
	xmlAnimMax=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_gforce_max",ImportTable.pPanels));
	xmlAnimCur=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_gforce_cur",ImportTable.pPanels));

	m_Min=0;
	m_Max=0;
	m_Cur=0;
	m_SetByReset=false;
	m_Powered=false;
}

void SDGforceGauge::Update()
{
	if(BTN_GET(BTN_GFORCE_RESET))
		Reset();
	if(PWR_GET(PWR_BUS27)&&PWR_GET(PWR_BUS115)&&AZS_GET(AZS_GROUND_PWR_BUS)) {
		m_Min=g_pSDAPI->GetN(G_FORCE_MINIMUM)/624;
		m_Max=g_pSDAPI->GetN(G_FORCE_MAXIMUM)/624;
		m_Cur=g_pSDAPI->GetN(G_FORCE)/624;
		m_SetByReset=false;
		m_Powered=true;
	} else {
		if(!m_SetByReset)m_Cur=0;
		m_Powered=false;
	}
	NDL_SET(NDL_GFORCE_CUR,m_Cur);
	NDL_SET(NDL_GFORCE_MIN,m_Min);
	NDL_SET(NDL_GFORCE_MAX,m_Max);

	xml1->set_number(NDL_GET(NDL_GFORCE_CUR));
	xml2->set_number(NDL_GET(NDL_GFORCE_MIN));
	xml3->set_number(NDL_GET(NDL_GFORCE_MAX));

	double ndlcur=NDL_GET(NDL_GFORCE_CUR);
	double ndlmin=NDL_GET(NDL_GFORCE_MIN);
	double ndlmax=NDL_GET(NDL_GFORCE_MAX);

	if(ndlcur>=0) {
		xmlAnimCur->set_degree(dgrd(ndlcur*50));
	} else {
		if(ndlcur<0&&ndlcur>=-2)
			xmlAnimCur->set_degree(dgrd(ndlcur*25));
		else if(ndlcur<-2)
			xmlAnimCur->set_degree(dgrd((2*25+(ndlcur-2)*50)*-1));
	}

	if(ndlmin>=0) {
		xmlAnimMin->set_degree(dgrd(ndlmin*50));
	} else {
		if(ndlmin<0&&ndlmin>=-2)
			xmlAnimMin->set_degree(dgrd(ndlmin*25));
		else if(ndlmin<-2)
			xmlAnimMin->set_degree(dgrd((2*25+(ndlmin-2)*50)*-1));
	}

	if(ndlmax>=0) {
		xmlAnimMax->set_degree(dgrd(ndlmax*50));
	} else {
		if(ndlmax<0&&ndlmax>=-2)
			xmlAnimMax->set_degree(dgrd(ndlmax*25));
		else if(ndlmax<-2)
			xmlAnimMax->set_degree(dgrd((2*25+(ndlmax-2)*50)*-1));
	}

}

void SDGforceGauge::Load(CIniFile *ini)
{
}

void SDGforceGauge::Save(CIniFile *ini)
{
}

void SDGforceGauge::Reset() 
{
	if(!m_Powered)
		return;
	m_Min=1;
	m_Max=1;
	m_Cur=1;
	m_SetByReset=true;
	ImportTable.pPanels->trigger_key_event(KEY_RESET_G_FORCE_INDICATOR,0);
}
