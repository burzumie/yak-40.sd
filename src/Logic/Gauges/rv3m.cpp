/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/rv3m.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "rv3m.h"

SDRV3MGauge::SDRV3MGauge() 
{
	m_Timer=0;
	m_SignalStarted=false;
	m_bAltReached=false;
}

SDRV3MGauge::~SDRV3MGauge()
{
}

void SDRV3MGauge::Init()
{
	VCAnimNdl=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_radalt_alt",ImportTable.pPanels));
	VCAnimDH=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_radalt_dh",ImportTable.pPanels));
	m_NotVisible=false;
}

void SDRV3MGauge::Update()
{
	if(PWR_GET(PWR_BUS115)&&PWR_GET(PWR_BUS27)&&AZS_GET(AZS_RADALT)&&g_pSDAPI->GetN(RADIO_HEIGHT)<=g_pSDAPI->m_Config.m_CfgRV3M.m_dMax) {
		if(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)) {
			POS_SET(POS_RV3M_LAMP,1);
		} else {
			NDL_SET(NDL_RV3M_ALT,g_pSDAPI->GetN(RADIO_HEIGHT)-g_pSDAPI->m_Config.m_CfgRV3M.m_dCorrectAlt);
			if(NDL_GET(NDL_RV3M_ALT)>=NDL_GET(NDL_RV3M_DH)-g_pSDAPI->m_Config.m_CfgRV3M.m_dCheckAlt&&NDL_GET(NDL_RV3M_ALT)<=NDL_GET(NDL_RV3M_DH)+g_pSDAPI->m_Config.m_CfgRV3M.m_dCheckAlt&&!m_SignalStarted) {
				m_Timer=0;
				m_SignalStarted=true;
				if(NDL_GET(NDL_VS)<-2)
					SND_SET(SND_DECISION_HEIGHT,1);
			} else {
				POS_SET(POS_RV3M_LAMP,0);
			}
			if(m_SignalStarted) {
				POS_SET(POS_RV3M_LAMP,1);
				m_Timer++;
				if(m_Timer>g_pSDAPI->m_Config.m_CfgRV3M.m_dSecToLight*18) {
					POS_SET(POS_RV3M_LAMP,0);
					m_SignalStarted=false;
				}
			}
		}
		m_bAltReached=false;
		m_NotVisible=false;
	} else {
		m_NotVisible=true;
		m_bAltReached=true;
		POS_SET(POS_RV3M_LAMP,0);
		NDL_SET(NDL_RV3M_ALT,g_pSDAPI->m_Config.m_CfgRV3M.m_dMin);
	}

	int krm=KRM_CHG(KRM_RV3M);
	if(krm<0) {
		ImportTable.pPanels->send_key_event(KEY_DECISION_HEIGHT_DEC,0);
	} else if(krm>0) {
		ImportTable.pPanels->send_key_event(KEY_DECISION_HEIGHT_INC,0);
	}

	NDL_SET(NDL_RV3M_DH, g_pSDAPI->GetN(DECISION_HEIGHT));

	if(PWR_GET(PWR_BUS115)&&AZS_GET(AZS_RADALT))
		TBL_SET(TBL_RAD_ALTIMETER_FAIL,0);
	else
		TBL_SET(TBL_RAD_ALTIMETER_FAIL,1);

	NDL_SET(NDL_RV3M_ALT_EN,METER_TO_FEET(NDL_GET(NDL_RV3M_ALT)));
	NDL_SET(NDL_RV3M_DH_EN, METER_TO_FEET(NDL_GET(NDL_RV3M_DH)));

	const FLOAT64 deltarus = 5; // ��� ������ ����� ��� ������� ����� �������� �������
	const FLOAT64 deltaeng = 20; // ��� ������ ����� ��� ������� ����� �������� �������
	static FLOAT64 rv3_needle_pos_rus = 0; // ���������� ������� ������ ��������� �������
	static FLOAT64 rv3_needle_pos_eng = 0; // ���������� ������� ������ ��������� �������

	FLOAT64 rv3_level_rus=NDL_GET(NDL_RV3M_ALT);
	FLOAT64 rv3_level_eng=NDL_GET(NDL_RV3M_ALT_EN);
	
	if(m_NotVisible&&rv3_level_rus<g_pSDAPI->GetN(RADIO_HEIGHT)) {
		if( rv3_needle_pos_rus < rv3_level_rus ) rv3_needle_pos_rus += deltarus; // ������� ����� ���� �� ���������� �������� �������
		if( rv3_needle_pos_rus > rv3_level_rus && rv3_needle_pos_rus!=-5) rv3_needle_pos_rus -= deltarus; // ������� ���� ���� �� ���������� �������� �������
		if(rv3_needle_pos_rus<-5)
			rv3_needle_pos_rus=-5;
	} else if(!m_NotVisible&&rv3_needle_pos_rus<rv3_level_rus) {
		/*if( rv3_needle_pos_rus < rv3_level_rus )*/ rv3_needle_pos_rus = rv3_level_rus; // ������� ����� ���� �� ���������� �������� �������
		if(rv3_needle_pos_rus<-5)
			rv3_needle_pos_rus=-5;
	} else {
		rv3_needle_pos_rus = rv3_level_rus;
	}

	if(m_NotVisible&&rv3_level_eng<METER_TO_FEET(g_pSDAPI->GetN(RADIO_HEIGHT))) {
		if( rv3_needle_pos_eng < rv3_level_eng ) rv3_needle_pos_eng += deltaeng; // ������� ����� ���� �� ���������� �������� �������
		if( rv3_needle_pos_eng > rv3_level_eng && rv3_needle_pos_eng!=-22) rv3_needle_pos_eng -= deltaeng; // ������� ���� ���� �� ���������� �������� �������
		if(rv3_needle_pos_eng<-22)
			rv3_needle_pos_eng=-22;
	} else if(!m_NotVisible&&rv3_level_eng<rv3_level_eng) {
		if( rv3_needle_pos_eng < rv3_level_eng ) rv3_needle_pos_eng += deltaeng; // ������� ����� ���� �� ���������� �������� �������
		if(rv3_needle_pos_eng<-22)
			rv3_needle_pos_eng=-22;
	} else {
		rv3_needle_pos_eng = rv3_level_eng;
	}

	if(!POS_GET(POS_PANEL_LANG)) {
		VCAnimNdl->set_degree(dgrd(GetXmlAltRus(rv3_needle_pos_rus)));
		VCAnimDH->set_degree(dgrd(GetXmlAltRus(NDL_GET(NDL_RV3M_DH))));
	} else {
		VCAnimNdl->set_degree(dgrd(GetXmlAltEng(rv3_needle_pos_eng)));
		VCAnimDH->set_degree(dgrd(GetXmlAltEng(NDL_GET(NDL_RV3M_DH_EN))));
	}

}

double SDRV3MGauge::GetXmlAltRus(double ndl)
{
	double var=ndl;

	if(var<=30)				return var*2.1;
	if(var>30&&var<=50)		return 30*2.1+(var-30)*2.05;
	if(var>50&&var<=100)	return 30*2.1+( 50-30)*2.05+(var-50)*0.68;
	if(var>100)          	return 30*2.1+( 50-30)*2.05+(100-50)*0.68+(var-100)*0.35;

	return 0;
}

double SDRV3MGauge::GetXmlAltEng(double ndl)
{
	double var=ndl;

	if(var<=500)				return var*0.36;
	if(var>500  && var<=1000)	return 500*0.36+( var-500)*0.104;
	if(var>1000 && var<=1500)	return 500*0.36+(1000-500)*0.104+( var-1000)*0.07;
	if(var>1500 && var<=2000)	return 500*0.36+(1000-500)*0.104+(1500-1000)*0.07+( var-1500)*0.058;
	if(var>2000)             	return 500*0.36+(1000-500)*0.104+(1500-1000)*0.07+(2000-1500)*0.058+(var-2000)*0.046;

	return 0;
}

void SDRV3MGauge::Load(CIniFile *ini)
{
}

void SDRV3MGauge::Save(CIniFile *ini)
{
}
