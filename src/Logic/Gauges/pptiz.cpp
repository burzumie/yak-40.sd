/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/pptiz.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "pptiz.h"

SDPptizGauge::SDPptizGauge() 
{
}

SDPptizGauge::~SDPptizGauge()
{
}

void SDPptizGauge::Init()
{
	VCAnimNdl=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_pptiz",ImportTable.pPanels));
}

void SDPptizGauge::Update()
{
	if(!AZS_GET(AZS_FUEL_IND_BUS)||!PWR_GET(PWR_BUS27)||!PWR_GET(PWR_BUS115)) {
		NDL_SET(NDL_PPTIZ,0);
		return;
	}

	double l=g_pSDAPI->GetN(FUEL_TANK_LEFT_MAIN_LEVEL)/FUEL_LEVEL_PCT*20;
	double r=g_pSDAPI->GetN(FUEL_TANK_RIGHT_MAIN_LEVEL)/FUEL_LEVEL_PCT*20;

	if(BTN_GET(BTN_PPTIZ_TEST_0)||BTN_GET(BTN_PPTIZ_TEST_4000)) {
		if(BTN_GET(BTN_PPTIZ_TEST_0))
			NDL_SET(NDL_PPTIZ,0);

		if(BTN_GET(BTN_PPTIZ_TEST_4000))
			NDL_SET(NDL_PPTIZ,4000);

	} else {
		switch((int)AZS_GET(AZS_FUEL_IND)) {
			case 0:
				NDL_SET(NDL_PPTIZ,l+r);
			break;
			case 1:
				NDL_SET(NDL_PPTIZ,l*2);
			break;
			case 2:
				NDL_SET(NDL_PPTIZ,r*2);
			break;
			default:
				NDL_SET(NDL_PPTIZ,0);
		}
	}

	const FLOAT64 delta = 40; // ��� ������ ����� ��� ������� ����� �������� �������
	static FLOAT64 fuel_needle_pos = 0; // ���������� ������� ������ ��������� �������

	FLOAT64 fuel_level = NDL_GET(NDL_PPTIZ); // �������� ������� �������

	if( fuel_needle_pos < fuel_level ) fuel_needle_pos += delta; // ������� ����� ���� �� ���������� �������� �������
	if( fuel_needle_pos > fuel_level ) fuel_needle_pos -= delta; // ������� ���� ���� �� ���������� �������� �������

	VCAnimNdl->set_degree(dgrd(XmlGetNdl(fuel_needle_pos)));

}

double SDPptizGauge::XmlGetNdl(double ndl)
{
	double var=ndl;

	if(var<=1000)					return var*0.07;
	if(var >1000 && var <= 3000)	return 1000*0.07+(var-1000)*0.09;
	if(var >3000               )	return 1000*0.07+(3000-1000)*0.09+(var-3000)*0.07;

	return 0;

}

void SDPptizGauge::Load(CIniFile *ini)
{
}

void SDPptizGauge::Save(CIniFile *ini)
{
}
