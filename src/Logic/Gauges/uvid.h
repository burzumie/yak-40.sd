/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/uvid.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

class SDUVIDGauge : public SDLogicBase
{
public:
	double m_Alt,m_Press;
	auto_ptr<CNamedVar> VCAnimPressHG;
	auto_ptr<CNamedVar> VCAnimPressMM;
	auto_ptr<CNamedVar> VCAnimPressMB;

	auto_ptr<CNamedVar> VCAnimPressMMNum1;
	auto_ptr<CNamedVar> VCAnimPressMMNum2;
	auto_ptr<CNamedVar> VCAnimPressMMNum3;

	auto_ptr<CNamedVar> VCAnimPressMBNum1;
	auto_ptr<CNamedVar> VCAnimPressMBNum2;
	auto_ptr<CNamedVar> VCAnimPressMBNum3;
	auto_ptr<CNamedVar> VCAnimPressMBNum4;

	auto_ptr<CNamedVar> VCAnimPressHGNum1;
	auto_ptr<CNamedVar> VCAnimPressHGNum2;
	auto_ptr<CNamedVar> VCAnimPressHGNum3;
	auto_ptr<CNamedVar> VCAnimPressHGNum4;

	SDUVIDGauge(){};
	virtual ~SDUVIDGauge(){};

	virtual void Init()=0;
	virtual void Update()=0;
	virtual void Load(CIniFile *ini){};
	virtual void Save(CIniFile *ini){};

};

class SDUVIDLeftGauge : public SDUVIDGauge
{
private:
	auto_ptr<CNamedVar> xml1;
	auto_ptr<CNamedVar> xml2;
	auto_ptr<CNamedVar> xml3;
	auto_ptr<CNamedVar> xmlAlt;

	auto_ptr<CNamedVar> VCAnimNdl;

	auto_ptr<CNamedVar> VCAnimNum1;
	auto_ptr<CNamedVar> VCAnimNum2;
	auto_ptr<CNamedVar> VCAnimNum3;
	auto_ptr<CNamedVar> VCAnimNum4;

public:
	SDUVIDLeftGauge();

	virtual void Init();
	virtual void Update();
};

class SDUVIDRightGauge : public SDUVIDGauge
{
private:
	auto_ptr<CNamedVar> xml1;
	auto_ptr<CNamedVar> xml2;
	auto_ptr<CNamedVar> xml3;
	auto_ptr<CNamedVar> xmlAlt;

	auto_ptr<CNamedVar> VCAnimNdl1k;
	auto_ptr<CNamedVar> VCAnimNdl100;
	auto_ptr<CNamedVar> VCAnimScl;

public:
	SDUVIDRightGauge();

	virtual void Init();
	virtual void Update();
};
