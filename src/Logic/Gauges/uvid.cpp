/**************************************************************************
 *
 *   SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004
 *
 *   Start date: [18.01.2006 3:04:01]
 *
 *   Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]
 *
 *   All rights reserved.
 *
 **************************************************************************
 *
 * $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/uvid.cpp $
 * $Revision: 3 $
 * $Date: 21.02.06 0:14 $
 * $Author: Except $
 *
 **************************************************************************/

#include "uvid.h"

SDUVIDLeftGauge::SDUVIDLeftGauge() 
{
}

void SDUVIDLeftGauge::Init()
{
	xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_418",ImportTable.pPanels));
	xml2=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_419",ImportTable.pPanels));
	xml3=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_420",ImportTable.pPanels));
	xmlAlt=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_529",ImportTable.pPanels));

	VCAnimNdl=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_uvid",ImportTable.pPanels));

	VCAnimPressHG=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressHg",ImportTable.pPanels));
	VCAnimPressMM=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressMM",ImportTable.pPanels));
	VCAnimPressMB=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressMB",ImportTable.pPanels));

	VCAnimNum1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_1",ImportTable.pPanels));
	VCAnimNum2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_2",ImportTable.pPanels));
	VCAnimNum3=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_3",ImportTable.pPanels));
	VCAnimNum4=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_4",ImportTable.pPanels));

	VCAnimPressHGNum1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressHg1",ImportTable.pPanels));
	VCAnimPressHGNum2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressHg2",ImportTable.pPanels));
	VCAnimPressHGNum3=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressHg3",ImportTable.pPanels));
	VCAnimPressHGNum4=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressHg4",ImportTable.pPanels));

	VCAnimPressMBNum1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressMb1",ImportTable.pPanels));
	VCAnimPressMBNum2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressMb2",ImportTable.pPanels));
	VCAnimPressMBNum3=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressMb3",ImportTable.pPanels));
	VCAnimPressMBNum4=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressMb4",ImportTable.pPanels));

	VCAnimPressMMNum1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressMM1",ImportTable.pPanels));
	VCAnimPressMMNum2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressMM2",ImportTable.pPanels));
	VCAnimPressMMNum3=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid0_PressMM3",ImportTable.pPanels));

	POS_SET(POS_UVID0_HG,g_pSDAPI->m_Config.m_CfgUVID.m_iDefaultHGLeft);
}

void SDUVIDLeftGauge::Update()
{
	xml1->set_number(POS_GET(POS_UVID0_MB));
	xml2->set_number(POS_GET(POS_UVID0_HG));
	xml3->set_number(POS_GET(POS_UVID0_MM));

	double tmp, tmp2;

	int krm=KRM_CHG(KRM_UVID0);

	if(krm<0&&PWR_GET(PWR_BUS27)&&PWR_GET(PWR_BUS36)) {
		POS_DEC2(POS_UVID0_HG,0.01);
	} else if(krm>0&&PWR_GET(PWR_BUS27)&&PWR_GET(PWR_BUS36)) {
		POS_INC2(POS_UVID0_HG,0.01);
	}

	tmp2=POS_GET(POS_UVID0_HG);
	POS_SET(POS_UVID0_MM,tmp2*25.40707);
	POS_SET(POS_UVID0_MB,tmp2*33.85695);
		
	if(g_pSDAPI->GetN(DISPLAY_UNITS)==ENGLISH_UNITS||g_pSDAPI->GetN(DISPLAY_UNITS)==METRIC_UNITS_ALT_FEET)
		tmp=FEET_TO_METER(g_pSDAPI->GetN(ALT_FROM_BAROMETRIC_PRESSURE));
	else
		tmp=g_pSDAPI->GetN(ALT_FROM_BAROMETRIC_PRESSURE);

	m_Alt=((POS_GET(POS_UVID0_MM)-g_pSDAPI->GetN(KOHLSMAN_SETTING_HG)*25.40707)*12)+tmp;
	m_Press=POS_GET(POS_UVID0_MM);

	NDL_SET(NDL_UVID0_HIDE,m_Alt);
	NDL_SET(NDL_UVID0_EN_HIDE,METER_TO_FEET(m_Alt));

	ImportTable.pPanels->send_key_event(KEY_KOHLSMAN_SET,UINT32(POS_GET(POS_UVID0_MB)*16));

	if(PWR_GET(PWR_BUS27)&&PWR_GET(PWR_BUS36)) {
		tmp=m_Press;

		POS_SET(POS_UVID0_PSI_100_MM, (int)fabs(10.0-(double)(((int)tmp%1000-(int)tmp%100)/100)));
		POS_SET(POS_UVID0_PSI_10_MM , (int)fabs(10.0-(double)(((int)tmp%100-(int)tmp%10)/10)));   
		POS_SET(POS_UVID0_PSI_1_MM 	, (int)fabs(10.0-(double)((int)tmp%10)));    

		tmp=POS_GET(POS_UVID0_HG)*100;
		POS_SET(POS_UVID0_PSI_1000_HG,(int)fabs(10.0-(double)(((int)tmp%10000-(int)tmp%1000)/1000)));
		POS_SET(POS_UVID0_PSI_100_HG, (int)fabs(10.0-(double)(((int)tmp%1000-(int)tmp%100)/100)));
		POS_SET(POS_UVID0_PSI_10_HG , (int)fabs(10.0-(double)(((int)tmp%100-(int)tmp%10)/10)));   
		POS_SET(POS_UVID0_PSI_1_HG 	, (int)fabs(10.0-(double)((int)tmp%10)));    

		tmp=POS_GET(POS_UVID0_MB);
		POS_SET(POS_UVID0_PSI_1000_MB,(int)fabs(10.0-(double)(((int)tmp%10000-(int)tmp%1000)/1000)));
		POS_SET(POS_UVID0_PSI_100_MB, (int)fabs(10.0-(double)(((int)tmp%1000-(int)tmp%100)/100)));
		POS_SET(POS_UVID0_PSI_10_MB , (int)fabs(10.0-(double)(((int)tmp%100-(int)tmp%10)/10)));   
		POS_SET(POS_UVID0_PSI_1_MB 	, (int)fabs(10.0-(double)((int)tmp%10)));    

		NDL_SET(NDL_UVID0,m_Alt);
		POS_SET(POS_UVID0_ALT_10  		, m_Alt<-10?10:100-GetAlt10(m_Alt));
		POS_SET(POS_UVID0_ALT_100		, m_Alt<-10?10:100-GetAlt100(m_Alt));
		POS_SET(POS_UVID0_ALT_1000 		, m_Alt<-10?10:100-GetAlt1000(m_Alt));
		POS_SET(POS_UVID0_ALT_10000		, m_Alt<-10?10:100-GetAlt10000(m_Alt));

		double feet=METER_TO_FEET(m_Alt);
		NDL_SET(NDL_UVID0_EN,feet);
		POS_SET(POS_UVID0_ALT_10_EN		, feet<-10?10:100-GetAlt10(feet));
		POS_SET(POS_UVID0_ALT_100_EN	, feet<-10?10:100-GetAlt100(feet));
		POS_SET(POS_UVID0_ALT_1000_EN 	, feet<-10?10:100-GetAlt1000(feet));
		POS_SET(POS_UVID0_ALT_10000_EN	, feet<-10?10:100-GetAlt10000(feet));

	} else {
		NDL_SET(NDL_UVID0,0);
		NDL_SET(NDL_UVID0_EN,0);
	}

	VCAnimNum1->set_degree(dgrd(POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10000_EN)	*3.6:POS_GET(POS_UVID0_ALT_10000)	*3.6));
	VCAnimNum2->set_degree(dgrd(POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_1000_EN)	*3.6:POS_GET(POS_UVID0_ALT_1000)	*3.6));
	VCAnimNum3->set_degree(dgrd(POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_100_EN)	*3.6:POS_GET(POS_UVID0_ALT_100)		*3.6));
	VCAnimNum4->set_degree(dgrd(POS_GET(POS_PANEL_LANG)?POS_GET(POS_UVID0_ALT_10_EN)	*3.6:POS_GET(POS_UVID0_ALT_10)		*3.6));

	VCAnimNdl->set_degree(dgrd(POS_GET(POS_PANEL_LANG)? NDL_GET(NDL_UVID0_EN)*0.36:NDL_GET(NDL_UVID0)*0.36));
	xmlAlt->set_number(POS_GET(POS_PANEL_LANG)?NDL_GET(NDL_UVID0_EN):NDL_GET(NDL_UVID0));

	VCAnimPressHG->set_number(POS_GET(POS_UVID0_HG));
	VCAnimPressMB->set_number(POS_GET(POS_UVID0_MB));
	VCAnimPressMM->set_number(POS_GET(POS_UVID0_MM));

	VCAnimPressHGNum1->set_degree(dgrd(POS_GET(POS_UVID0_PSI_1000_HG)	*36));
	VCAnimPressHGNum2->set_degree(dgrd(POS_GET(POS_UVID0_PSI_100_HG )	*36));
	VCAnimPressHGNum3->set_degree(dgrd(POS_GET(POS_UVID0_PSI_10_HG  )	*36));
	VCAnimPressHGNum4->set_degree(dgrd(POS_GET(POS_UVID0_PSI_1_HG 	)	*36));

	VCAnimPressMBNum1->set_degree(dgrd(POS_GET(POS_UVID0_PSI_1000_MB)	*36));
	VCAnimPressMBNum2->set_degree(dgrd(POS_GET(POS_UVID0_PSI_100_MB )	*36));
	VCAnimPressMBNum3->set_degree(dgrd(POS_GET(POS_UVID0_PSI_10_MB  )	*36));
	VCAnimPressMBNum4->set_degree(dgrd(POS_GET(POS_UVID0_PSI_1_MB 	)	*36));

	VCAnimPressMMNum1->set_degree(dgrd(POS_GET(POS_UVID0_PSI_100_MM)	*36));
	VCAnimPressMMNum2->set_degree(dgrd(POS_GET(POS_UVID0_PSI_10_MM )	*36));
	VCAnimPressMMNum3->set_degree(dgrd(POS_GET(POS_UVID0_PSI_1_MM  )	*36));

}

//////////////////////////////////////////////////////////////////////////

SDUVIDRightGauge::SDUVIDRightGauge() 
{
}

void SDUVIDRightGauge::Init()
{
	xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_409",ImportTable.pPanels));
	xml2=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_410",ImportTable.pPanels));
	xml3=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_411",ImportTable.pPanels));
	xmlAlt=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_523",ImportTable.pPanels));
	POS_SET(POS_UVID1_HG,g_pSDAPI->m_Config.m_CfgUVID.m_iDefaultHGRight);

	VCAnimNdl1k=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_vd10_1k",ImportTable.pPanels));
	VCAnimNdl100=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_vd10_100",ImportTable.pPanels));
	VCAnimScl=auto_ptr<CNamedVar>(new CNamedVar("sd6015_scl_VD10Press",ImportTable.pPanels));

	VCAnimPressHG=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid1_PressHg",ImportTable.pPanels));
	VCAnimPressMM=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid1_PressMM",ImportTable.pPanels));
	VCAnimPressMB=auto_ptr<CNamedVar>(new CNamedVar("sd6015_uvid1_PressMB",ImportTable.pPanels));

}

void SDUVIDRightGauge::Update()
{
	xml1->set_number(POS_GET(POS_UVID1_MB));
	xml2->set_number(POS_GET(POS_UVID1_HG));
	xml3->set_number(POS_GET(POS_UVID1_MM));

	double tmp, tmp2;

	int krm=KRM_CHG(KRM_UVID1);

	if(krm<0) {
		POS_DEC2(POS_UVID1_HG,0.01);
	} else if(krm>0) {
		POS_INC2(POS_UVID1_HG,0.01);
	}

	tmp2=POS_GET(POS_UVID1_HG);
	POS_SET(POS_UVID1_MM,tmp2*25.40707);
	POS_SET(POS_UVID1_MB,tmp2*33.85695);

	if(POS_GET(POS_UVID1_MM) > 712 && POS_GET(POS_UVID1_MM) < 812)
		ImportTable.pPanels->send_key_event(KEY_KOHLSMAN_SET, UINT32(POS_GET(POS_UVID1_MM)*1.333224*16));

	if(g_pSDAPI->GetN(DISPLAY_UNITS)==ENGLISH_UNITS||g_pSDAPI->GetN(DISPLAY_UNITS)==METRIC_UNITS_ALT_FEET)
		tmp=FEET_TO_METER(g_pSDAPI->GetN(ALT_FROM_BAROMETRIC_PRESSURE));
	else
		tmp=g_pSDAPI->GetN(ALT_FROM_BAROMETRIC_PRESSURE);

	m_Alt=((POS_GET(POS_UVID1_MM)-g_pSDAPI->GetN(KOHLSMAN_SETTING_HG)*25.40707)*12)+tmp;
	m_Press=POS_GET(POS_UVID1_MM);

	int itmp=(int)m_Alt;
	int itmp2=(((int)itmp%1000000-(int)itmp%100000)/1000)+itmp/10;

	NDL_SET(NDL_UVID1,m_Alt);
	NDL_SET(NDL_UVID1_100,itmp);
	NDL_SET(NDL_UVID1_1000,itmp2);

	NDL_SET(NDL_UVID1_100_EN,METER_TO_FEET(itmp));
	NDL_SET(NDL_UVID1_1000_EN,METER_TO_FEET(itmp2));

	xmlAlt->set_number(POS_GET(POS_PANEL_LANG)?METER_TO_FEET(m_Alt):m_Alt);

	VCAnimNdl1k->set_degree(dgrd(POS_GET(POS_PANEL_LANG)?NDL_GET(NDL_UVID1_1000_EN)*0.36:NDL_GET(NDL_UVID1_1000)*0.36));
	VCAnimNdl100->set_degree(dgrd(POS_GET(POS_PANEL_LANG)?NDL_GET(NDL_UVID1_100_EN)*0.36:NDL_GET(NDL_UVID1_100)*0.36));

	VCAnimPressHG->set_number(POS_GET(POS_UVID1_HG));
	VCAnimPressMB->set_number(POS_GET(POS_UVID1_MB));
	VCAnimPressMM->set_number(POS_GET(POS_UVID1_MM));

	if(POS_GET(POS_PANEL_LANG)) {
		double ang=NormaliseDegreeW((POS_GET(POS_UVID1_HG)+2.95)*73.47);
		ang=fabs(360-ang);
		VCAnimScl->set_degree(dgrd(ang));
	} else {
		double ang=NormaliseDegreeW((POS_GET(POS_UVID1_MM)+47)*3);
		ang=fabs(360-ang);
		VCAnimScl->set_degree(dgrd(ang));
	}

}
