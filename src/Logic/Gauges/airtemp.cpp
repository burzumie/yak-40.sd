/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/airtemp.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "airtemp.h"

SDAirtempGauge::SDAirtempGauge() 
{
}

SDAirtempGauge::~SDAirtempGauge()
{
}

void SDAirtempGauge::Init()
{
	xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_508",ImportTable.pPanels));
	XmlAnimNdl=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_oat",ImportTable.pPanels));
}

void SDAirtempGauge::Update()
{
	NDL_SET(NDL_AIR_TEMP,PWR_GET(PWR_BUS27)?g_pSDAPI->GetN(TOTAL_AIR_TEMP)*256:-100);
	xml1->set_number(NDL_GET(NDL_AIR_TEMP));
	XmlAnimNdl->set_degree(dgrd(NDL_GET(NDL_AIR_TEMP)*0.8));
}

void SDAirtempGauge::Load(CIniFile *ini)
{
}

void SDAirtempGauge::Save(CIniFile *ini)
{
}
