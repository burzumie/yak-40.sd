/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "Main.h"
#include "../Lib/simtools.h"
#include "Container.h"
#include "SaveLoad.h"

#define MAX_AZS_IN_VC 173
#define MAX_GLT_IN_VC 7

class SDGlobal : public CSimKey
{
public:
	SDGlobal();
	~SDGlobal();

	void Init();
	void Update();
	void Load();
	void Save(const char *name);

private:
	auto_ptr<SDLogicContainer>	m_Container;
	auto_ptr<SDTimerSave>		m_TimerSave;
	auto_ptr<FLIGHT_FILE_INFO>	m_FlightInfo;
	double						m_ElapsedSec[2];
	bool						m_Paused;
	std::string					m_PrevoiusFlight;
	std::string					m_UIGeneratedFlight;
	CHtail						*m_Htail;
	CFlaps						*m_Flaps;
	CRTU						*m_RTU;
	CAirStarter					*m_AirStarter;
	CGear						*m_Gear;
	CNamedVar					*m_AzsVC[MAX_AZS_IN_VC];
	CNamedVar					*m_GltVC[MAX_GLT_IN_VC];
	bool						m_AlreadyLoaded;
	auto_ptr<CNamedVar>			m_XmlPanelLang;
	bool						m_EnginePressed;

	bool						fnKeyHandler(ID32 event,UINT32 evdata);
	double						GetTimeOfDay();
	void						PrepareSimSaveFiles();
	void						UpdateVCAZS();
	void						LoadPax(CIniFile *ini);
	void						SavePax(CIniFile *ini);

};

extern SDGlobal *g_pGlobal;

