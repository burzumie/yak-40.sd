/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/LogicBase.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "Main.h"

class SDLogicBase
{
private:
	static std::list<SDLogicBase *> m_ChildList;
	FAILURE_ACTION m_Failure;

public:
	SDLogicBase() {
		m_ChildList.push_back(this);
		m_Failure=FAIL_ACTION_NONE;
	}
	~SDLogicBase() {
		m_ChildList.remove(this);
	}

	virtual void Init()					= 0;
	virtual void Update()				= 0;
	virtual void Load(CIniFile *ini)	= 0;
	virtual void Save(CIniFile *ini)	= 0;
	virtual void Failure(FAILURE_ACTION act) {m_Failure=act;};

	static void InitAll() {
		std::list<SDLogicBase*>::const_iterator end(m_ChildList.end());

		for(std::list<SDLogicBase*>::const_iterator i=m_ChildList.begin();i!=end;++i) 
			(*i)->Init();
	};

	static void UpdateAll() {
		std::list<SDLogicBase*>::const_iterator end(m_ChildList.end());

		for(std::list<SDLogicBase*>::const_iterator i=m_ChildList.begin();i!=end;++i) 
			(*i)->Update();
	};

	static void LoadAll(CIniFile *ini) {
		std::list<SDLogicBase*>::const_iterator end(m_ChildList.end());

		for(std::list<SDLogicBase*>::const_iterator i=m_ChildList.begin();i!=end;++i) 
			(*i)->Load(ini);
	};

	static void SaveAll(CIniFile *ini) {
		std::list<SDLogicBase*>::const_iterator end(m_ChildList.end());

		for(std::list<SDLogicBase*>::const_iterator i=m_ChildList.begin();i!=end;++i) 
			(*i)->Save(ini);
	};
};
