/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Container.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "Systems/ElectroSystem.h"
#include "Systems/MiscSystem.h"
#include "Systems/GmkSystem.h"
#include "Systems/HydroSystem.h"
#include "Systems/Autopilot.h"
#include "Systems/APU.h"
#include "Systems/Engines.h"
#include "Systems/KursMP.h"
#include "Systems/Fuel.h"
#include "Systems/SrdSkv.h"
#include "Systems/Radio.h"
#include "Systems/POS.h"

#include "Gauges/airtemp.h"
#include "Gauges/htailgau.h"
#include "Gauges/flapsgau.h"
#include "Gauges/achs.h"
#include "Gauges/gforce.h"
#include "Gauges/pptiz.h"
#include "Gauges/rv3m.h"
#include "Gauges/kus.h"
#include "Gauges/vs.h"
#include "Gauges/da30.h"
#include "Gauges/uvid.h"
#include "Gauges/kppms.h"
#include "Gauges/iku.h"
#include "Gauges/agb.h"

class SDLogicContainer
{
public:
	auto_ptr<SDSystemElectro	>	m_Electro			;
	auto_ptr<SDSystemMisc		>	m_Misc				;
	auto_ptr<SDSystemGMK		>	m_GMK				;
	auto_ptr<SDSystemHydro		>	m_Hydro				;
	auto_ptr<SDSystemAutopilot	>	m_Autopilot			;
	auto_ptr<SDSystemAPU		>	m_APU				;
	auto_ptr<SDSystemEngines	>	m_Engines			;
	auto_ptr<SDSystemKMP    	>	m_KMP    			;
	auto_ptr<SDSystemFuel   	>	m_Fuel   			;
	auto_ptr<SDSystemSrdSkv   	>	m_SrdSkv   			;
	auto_ptr<SDSystemRadio   	>	m_Radio   			;
	auto_ptr<SDSystemPOS   		>	m_POS     			;

	auto_ptr<SDAirtempGauge		>	m_AirtempGauge		;	
	auto_ptr<SDHtailGauge		>	m_HtailGauge		;
	auto_ptr<SDFlapsGauge		>	m_FlapsGauge		;
	auto_ptr<SDAchsGauge		>	m_AchsGauge			;
	auto_ptr<SDGforceGauge		>	m_GforceGauge		;
	auto_ptr<SDPptizGauge		>	m_PptizGauge		;
	auto_ptr<SDRV3MGauge		>	m_RV3MGauge			;
	auto_ptr<SDKUSGauge			>	m_KUSGauge			;
	auto_ptr<SDVSGauge			>	m_VSGauge			;
	auto_ptr<SDDA30Gauge		>	m_DA30Gauge			;
	auto_ptr<SDUVIDLeftGauge	>	m_UVIDLeftGauge		;
	auto_ptr<SDUVIDRightGauge	>	m_UVIDRightGauge	;
	auto_ptr<SDKPPMSGauge		>	m_KPPMSLeftGauge	;
	auto_ptr<SDKPPMSGauge		>	m_KPPMSRightGauge	;
	auto_ptr<SDIKUGauge			>	m_IKUGauge			;
	auto_ptr<SDAGB1Gauge		>	m_AGB1Gauge			;
	auto_ptr<SDAGB2Gauge		>	m_AGB2Gauge			;
	auto_ptr<SDAGB3Gauge		>	m_AGB3Gauge			;

	SDLogicContainer() {
		m_Electro			= auto_ptr<SDSystemElectro	>(	new SDSystemElectro		());
		m_Misc				= auto_ptr<SDSystemMisc		>(	new SDSystemMisc		());
		m_GMK 				= auto_ptr<SDSystemGMK 		>(	new SDSystemGMK			());
		m_Hydro				= auto_ptr<SDSystemHydro	>(	new SDSystemHydro		());
		m_Autopilot			= auto_ptr<SDSystemAutopilot>(	new SDSystemAutopilot	());
		m_APU      			= auto_ptr<SDSystemAPU      >(	new SDSystemAPU      	());
		m_Engines  			= auto_ptr<SDSystemEngines  >(	new SDSystemEngines  	());
		m_KMP      			= auto_ptr<SDSystemKMP      >(	new SDSystemKMP      	());
		m_Fuel     			= auto_ptr<SDSystemFuel     >(	new SDSystemFuel     	());
		m_SrdSkv   			= auto_ptr<SDSystemSrdSkv   >(	new SDSystemSrdSkv     	());
		m_Radio   			= auto_ptr<SDSystemRadio	>(	new SDSystemRadio     	());
		m_POS     			= auto_ptr<SDSystemPOS  	>(	new SDSystemPOS       	());

		m_AirtempGauge		= auto_ptr<SDAirtempGauge	>(	new SDAirtempGauge		());
		m_HtailGauge		= auto_ptr<SDHtailGauge		>(	new SDHtailGauge		());
		m_FlapsGauge		= auto_ptr<SDFlapsGauge		>(	new SDFlapsGauge		());
		m_AchsGauge			= auto_ptr<SDAchsGauge		>(	new SDAchsGauge			());
		m_GforceGauge		= auto_ptr<SDGforceGauge	>(	new SDGforceGauge		());
		m_PptizGauge		= auto_ptr<SDPptizGauge		>(	new SDPptizGauge		());
		m_RV3MGauge			= auto_ptr<SDRV3MGauge		>(	new SDRV3MGauge			());
		m_KUSGauge			= auto_ptr<SDKUSGauge		>(	new SDKUSGauge			());
		m_VSGauge			= auto_ptr<SDVSGauge		>(	new SDVSGauge			());
		m_DA30Gauge			= auto_ptr<SDDA30Gauge		>(	new SDDA30Gauge			());
		m_UVIDLeftGauge		= auto_ptr<SDUVIDLeftGauge	>(	new SDUVIDLeftGauge		());
		m_UVIDRightGauge	= auto_ptr<SDUVIDRightGauge	>(	new SDUVIDRightGauge	());
		m_KPPMSLeftGauge	= auto_ptr<SDKPPMSGauge		>(	new SDKPPMSGauge		());
		m_KPPMSLeftGauge->SetNav(1);
		m_KPPMSRightGauge	= auto_ptr<SDKPPMSGauge		>(	new SDKPPMSGauge		());
		m_KPPMSRightGauge->SetNav(2);
		m_IKUGauge			= auto_ptr<SDIKUGauge		>(	new SDIKUGauge			());
		m_AGB1Gauge			= auto_ptr<SDAGB1Gauge		>(	new SDAGB1Gauge			());
		m_AGB2Gauge			= auto_ptr<SDAGB2Gauge		>(	new SDAGB2Gauge			());
		m_AGB3Gauge			= auto_ptr<SDAGB3Gauge		>(	new SDAGB3Gauge			());
	};

	~SDLogicContainer() {
	};

};