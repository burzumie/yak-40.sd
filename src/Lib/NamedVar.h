/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Lib/NamedVar.h $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "..\Common\CommonSys.h"
#ifdef PA_EXPORTS
#include "../Common/CommonLogic.h"
#else
#include "../Common/CommonVisual.h"
#endif

class CNamedVar
{
public:
	CNamedVar(const char *name,PANELS *p,bool unregister_after_use=false);
	CNamedVar(const std::string name,PANELS *p,bool unregister_after_use=false);
	~CNamedVar(void);

	//////////////////////////////////////////////////////////////////////////
	inline double get_number       () { return (double)  m_Panels->get_named_variable_typed_value( m_VarID, m_numberEnum        );      };
	inline bool   get_bool         () { return (unsigned)m_Panels->get_named_variable_typed_value( m_VarID, m_numberEnum        ) != 0; };
	inline double get_degree       () { return (double)  m_Panels->get_named_variable_typed_value( m_VarID, m_degreeEnum        );      };
	inline double get_radian       () { return (double)  m_Panels->get_named_variable_typed_value( m_VarID, m_radianEnum        );      };
	inline double get_feet_per_sec () { return (double)  m_Panels->get_named_variable_typed_value( m_VarID, m_feet_per_secEnum  );      };
	inline double get_km_per_hour  () { return (double)  m_Panels->get_named_variable_typed_value( m_VarID, m_km_per_hourEnum   );      };
	inline double get_km           () { return (double)  m_Panels->get_named_variable_typed_value( m_VarID, m_kmEnum            );      };
	inline double get_meter_per_sec() { return (double)  m_Panels->get_named_variable_typed_value( m_VarID, m_meter_per_secEnum );      };
	inline double get_meters       () { return (double)  m_Panels->get_named_variable_typed_value( m_VarID, m_meters_Enum       );      };
	inline double get_percent      () { return (double)  m_Panels->get_named_variable_typed_value( m_VarID, m_percent_Enum      );      };
	inline int    get_integer      () { return (int)	  m_Panels->get_named_variable_typed_value( m_VarID, m_numberEnum		 );      };

	//////////////////////////////////////////////////////////////////////////
	inline void set_number       (int      value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_numberEnum        ); };
	inline void set_number       (unsigned value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_numberEnum        ); };
	inline void set_number       (double   value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_numberEnum        ); };
	inline void set_bool         (bool     value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_numberEnum        ); };
	inline void set_degree       (double   value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_degreeEnum        ); };
	inline void set_radian       (double   value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_radianEnum        ); };
	inline void set_feet_per_sec (double   value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_feet_per_secEnum  ); };
	inline void set_km_per_hour  (double   value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_km_per_hourEnum   ); };
	inline void set_km           (double   value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_kmEnum            ); };
	inline void set_meter_per_sec(double   value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_meter_per_secEnum ); };
	inline void set_meters       (double   value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_meters_Enum       ); };
	inline void set_percent      (double   value) { m_Panels->set_named_variable_typed_value( m_VarID, (FLOAT64)value, m_percent_Enum      ); };
  
protected:
	ID   m_VarID;
	bool m_bUnregisterAfterUse;
	PANELS *m_Panels;

	// enum� ������ ���������
	static bool m_bEnumsInitialised;
	static ENUM m_boolEnum;
	static ENUM m_numberEnum;
	static ENUM m_degreeEnum;
	static ENUM m_radianEnum;
	static ENUM m_feet_per_secEnum;
	static ENUM m_km_per_hourEnum;
	static ENUM m_kmEnum;
	static ENUM m_meter_per_secEnum;
	static ENUM m_meters_Enum;
	static ENUM m_percent_Enum;

	void init_enums();

private:
	char name[BUFSIZ];

};

