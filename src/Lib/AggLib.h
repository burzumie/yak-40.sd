/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Lib/IniFile.h $

Last modification:
$Date: 18.02.06 11:43 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../Common/CommonSys.h"
#include "../Common/CommonVisual.h"

#include <agg_gamma_lut.h>
#include <agg_trans_affine.h>
#include <agg_rendering_buffer.h>
#include <agg_pixfmt_rgb_packed.h>
#include <agg_renderer_base.h>
#include <agg_renderer_primitives.h>
#include <agg_rasterizer_scanline_aa.h>
#include <agg_scanline_p.h>
#include <agg_renderer_scanline.h>
#include <agg_conv_stroke.h>
#include <agg_conv_contour.h>
#include <agg_path_storage.h>
#include <agg_ellipse.h>
#include <agg_gsv_text.h>
#include <agg_conv_curve.h>


#define MAX_BMP_CACHE_SIZE 100

namespace agg
{

	//=================================================================
	class tt_glyph
	{
		enum { buf_size = 16384-32 };

	public:
		~tt_glyph();
		tt_glyph();

		// Set the created font and the "flip_y" flag.
		//------------------------
		void font(HDC dc, HFONT f) { m_dc = dc; m_font = f; }
		void flip_y(bool flip) { m_flip_y = flip; }

		bool glyph(unsigned chr, bool hinted = true);

		// The following functions can be called after glyph()
		// and return the respective values of the 
		// GLYPHMETRICS structure.
		//-------------------------
		int      origin_x() const { return m_origin_x; }
		int      origin_y() const { return m_origin_y; }
		unsigned width()    const { return m_width;    }
		unsigned height()   const { return m_height;   }
		int      inc_x()    const { return m_inc_x;    }
		int      inc_y()    const { return m_inc_y;    }

		// Set the starting point of the Glyph
		//-------------------------
		void start_point(double x, double y) 
		{ 
			m_start_x = x; 
			m_start_y = y; 
		}

		// Vertex Source Interface
		//-------------------------
		void rewind(unsigned) 
		{ 
			m_cur_vertex = m_vertices; 
			m_cur_flag = m_flags; 
		}

		unsigned vertex(double* x, double* y)
		{
			*x = m_start_x + *m_cur_vertex++;
			*y = m_start_y + *m_cur_vertex++;
			return *m_cur_flag++;
		}

	private:
		HDC           m_dc;
		HFONT         m_font;
		char*         m_gbuf;
		int8u*        m_flags;
		double*       m_vertices;
		unsigned      m_max_vertices;
		const int8u*  m_cur_flag; 
		const double* m_cur_vertex;
		double        m_start_x;
		double        m_start_y;
		MAT2          m_mat2;

		int      m_origin_x;
		int      m_origin_y;
		unsigned m_width;
		unsigned m_height;
		int      m_inc_x;
		int      m_inc_y;

		bool     m_flip_y;
	};

	template<class Rasterizer, class Renderer, class Scanline, class CharT>
	void render_text(Rasterizer& ras, Renderer& ren, Scanline& sl, 
		agg::tt_glyph& gl, double x, double y, const CharT* str,
		bool hinted = true)
	{
		// The minimal pipeline is the curve converter. Of course, there
		// any other transformations are applicapble, conv_stroke<>, for example.
		// If there are other thransformations, it probably makes sense to 
		// turn off the hints (hinted=false), i.e., to use unhinted glyphs.
		//--------------------------
		agg::conv_curve<agg::tt_glyph> curve(gl);
		while(*str)
		{
			gl.start_point(x, y);
			gl.glyph(*str++, hinted);
			ras.add_path(curve);
			agg::render_scanlines(ras, sl, ren);
			x += gl.inc_x();
			y += gl.inc_y();
		}
	}

}
