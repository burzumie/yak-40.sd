/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Lib/PanelsTools.h $

  Last modification:
    $Date: 19.02.06 9:19 $
    $Revision: 4 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include "../api/Api.h"
#ifdef PA_EXPORTS
#include "../Common/CommonLogic.h"
#else
#include "../Common/CommonVisual.h"
#endif

class CTimer
{
private:
	friend void FSAPI fnTimerHnd(DWORD event_id,PVOID buffer_specific,PVOID user_buffer,PVOID user_param);
	CHAINID chain;
	bool started;
	FS6 *pFS6;
public:
	CTimer(FS6 *var);
	virtual ~CTimer();
	void StartTimer(CHAINID _chain=CHAIN_1HZ_SYNC);
	void StopTimer();
	bool IsStarted(){return started;};
protected:
	virtual void fnHandler()=NULL;
};

class CSimKey
{
private:
	friend bool FSAPI fnSimKeyHnd(ID32 event,UINT32 evdata,PVOID userdata);
	bool started;
	PANELS *pPanels;
public:
	CSimKey(PANELS *var);
	virtual ~CSimKey();
	void StartKey();
	void StopKey();
	bool IsStarted(){return started;};
protected:
	virtual bool fnKeyHandler(ID32 event,UINT32 evdata)=NULL;
};

class CTimer1Sec : public CTimer
{
private:
	int timer;
public:
	CTimer1Sec(FS6 *var):CTimer(var){timer=0;};
	virtual ~CTimer1Sec(){};
	void Reset(){timer=0;};
	int GetTimer(){return timer;};
	void Set(int val){timer=val;};
protected:
	void fnHandler(){timer++;};
};

class CTimerMixin;

struct STimerInfo
{
	unsigned int  uUserID;   // user-assigned timer ID
	CTimerMixin  *pInstance; // pointer to CTimerMixin instance

	STimerInfo() : uUserID(0), pInstance(NULL) {};

	STimerInfo(unsigned int uID, CTimerMixin *pInst)
	{
		uUserID   = uID;
		pInstance = pInst;
	}
};

typedef map <unsigned int, UINT_PTR> mapUserID2SysID;  // map user-assigned timer IDs to IDs from SetTimer function
typedef map <UINT_PTR, STimerInfo>   mapSysID2Info;    // map timer IDs to timer-info structures

class CTimerMixin
{
public:
	CTimerMixin ();
	~CTimerMixin ();

	bool SetTimer      (unsigned int uID, unsigned int uTime);
	void KillTimer     (unsigned int uID);
	void KillAllTimers (void);

	virtual void OnTimer (unsigned int uID) = 0;


private:
	static mapSysID2Info   m_mapSysID2Info;
	mapUserID2SysID m_mapUserID2SysID;

	static void CALLBACK TimerProc (HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime);
};
