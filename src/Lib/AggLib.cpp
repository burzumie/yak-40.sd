/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Lib/IniFile.cpp $

Last modification:
$Date: 18.02.06 11:43 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "AggLib.h"

namespace agg
{

tt_glyph::~tt_glyph()
{
	delete [] m_vertices;
	delete [] m_flags;
	delete [] m_gbuf;
}


tt_glyph::tt_glyph() :
m_dc(0),
m_font(0),
m_gbuf(new char [buf_size]),
m_flags(new int8u [256]),
m_vertices(new double[512]),
m_max_vertices(256),
m_cur_flag(m_flags),
m_cur_vertex(m_vertices),
m_start_x(0.0),
m_start_y(0.0),
m_flip_y(false)
{
	m_vertices[0] = m_vertices[1] = 0.0;
	m_flags[0] = path_cmd_stop;
	memset(&m_mat2, 0, sizeof(m_mat2));
	m_mat2.eM11.value = 1;
	m_mat2.eM22.value = 1;
}



static inline double fx_to_dbl(const FIXED& p)
{
	return double(p.value) + double(p.fract) * (1.0 / 65536.0);
}

static inline FIXED dbl_to_fx(double d)
{
	int l;
	l = long(d * 65536.0);
	return *(FIXED*)&l;
}


bool tt_glyph::glyph(unsigned chr, bool hinted)
{
	m_vertices[0] = m_vertices[1] = 0.0;
	m_flags[0] = path_cmd_stop;
	rewind(0);

	if (m_font == 0) return false;

#ifndef GGO_UNHINTED         // For compatibility with old SDKs.
#define GGO_UNHINTED 0x0100
#endif

	int unhinted = hinted ? 0 : GGO_UNHINTED;

	GLYPHMETRICS gm;
	int total_size = GetGlyphOutline(m_dc,
		chr,
		GGO_NATIVE | unhinted,
		&gm,
		buf_size,
		(void*)m_gbuf,
		&m_mat2);

	if (total_size < 0) return false;

	m_origin_x = gm.gmptGlyphOrigin.x;
	m_origin_y = gm.gmptGlyphOrigin.y;
	m_width    = gm.gmBlackBoxX;
	m_height   = gm.gmBlackBoxY;
	m_inc_x    = gm.gmCellIncX;
	m_inc_y    = gm.gmCellIncY;

	if (m_max_vertices <= total_size / sizeof(POINTFX))
	{
		delete [] m_vertices;
		delete [] m_flags;
		m_max_vertices = total_size / sizeof(POINTFX) + 256;
		m_flags = new int8u [m_max_vertices];
		m_vertices = new double [m_max_vertices * 2];
	}

	const char* cur_glyph = m_gbuf;
	const char* end_glyph = m_gbuf + total_size;

	double* vertex_ptr = m_vertices;
	int8u*  flag_ptr   = m_flags;

	while(cur_glyph < end_glyph)
	{
		const TTPOLYGONHEADER* th = (TTPOLYGONHEADER*)cur_glyph;

		const char* end_poly = cur_glyph + th->cb;
		const char* cur_poly = cur_glyph + sizeof(TTPOLYGONHEADER);

		*vertex_ptr++ = fx_to_dbl(th->pfxStart.x);
		*vertex_ptr++ = m_flip_y ? 
			-fx_to_dbl(th->pfxStart.y): 
		fx_to_dbl(th->pfxStart.y);
		*flag_ptr++   = path_cmd_move_to;

		while(cur_poly < end_poly)
		{
			const TTPOLYCURVE* pc = (const TTPOLYCURVE*)cur_poly;

			if (pc->wType == TT_PRIM_LINE)
			{
				int i;
				for (i = 0; i < pc->cpfx; i++)
				{
					*vertex_ptr++ = fx_to_dbl(pc->apfx[i].x);
					*vertex_ptr++ = m_flip_y ? 
						-fx_to_dbl(pc->apfx[i].y): 
					fx_to_dbl(pc->apfx[i].y);
					*flag_ptr++   = path_cmd_line_to;
				}
			}

			if (pc->wType == TT_PRIM_QSPLINE)
			{
				int u;
				for (u = 0; u < pc->cpfx - 1; u++)  // Walk through points in spline
				{
					POINTFX pnt_b = pc->apfx[u];    // B is always the current point
					POINTFX pnt_c = pc->apfx[u+1];

					if (u < pc->cpfx - 2)           // If not on last spline, compute C
					{
						// midpoint (x,y)
						*(int*)&pnt_c.x = (*(int*)&pnt_b.x + *(int*)&pnt_c.x) / 2;
						*(int*)&pnt_c.y = (*(int*)&pnt_b.y + *(int*)&pnt_c.y) / 2;
					}

					*vertex_ptr++ = fx_to_dbl(pnt_b.x);
					*vertex_ptr++ = m_flip_y ? 
						-fx_to_dbl(pnt_b.y): 
					fx_to_dbl(pnt_b.y);
					*flag_ptr++   = path_cmd_curve3;

					*vertex_ptr++ = fx_to_dbl(pnt_c.x);
					*vertex_ptr++ = m_flip_y ? 
						-fx_to_dbl(pnt_c.y): 
					fx_to_dbl(pnt_c.y);
					*flag_ptr++   = path_cmd_curve3;
				}
			}
			cur_poly += sizeof(WORD) * 2 + sizeof(POINTFX) * pc->cpfx;
		}
		cur_glyph += th->cb;
		*vertex_ptr++ = 0.0;
		*vertex_ptr++ = 0.0;
		*flag_ptr++   = path_cmd_end_poly | path_flags_close | path_flags_ccw;
	}

	*vertex_ptr++ = 0.0;
	*vertex_ptr++ = 0.0;
	*flag_ptr++   = path_cmd_stop;

	return true;
}

}

