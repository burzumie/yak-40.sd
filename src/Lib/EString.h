/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include <string>

static const int BufferSize = 256;

class EStringBuffer
{
private:
	char    FBuffer[BufferSize];
	friend class EString;
};

class EString: public std::string 
{
public:
	EString();
	EString(std::string str);
	EString(const char *str);
	EString(const unsigned char *str) { assign(reinterpret_cast<const char*>(str)); };
	EString(char Ch);
	explicit EString(int FromInt);
	explicit EString(size_t FromSizeT);
	explicit EString(double FromDouble);
	explicit EString(double FromDouble, int Digits);
	explicit EString(const EStringBuffer& Buffer);
	//
	// EString
	//
	EStringBuffer   GetStringBuffer() const;
	void            TrimSpaces();
	void            TrimBrackets();
	void            ToUpper();
	void            AppendInt(int I, int Format);
	void            AddTrailingChar(char Ch);
	void            ReplaceAll(char OldChar, char NewChar);
	void            Fetch(const int Len);
	EString         GetToken(const size_t Num) const;
	EString         GetFormattedString(const int ExpandZeroes, const int Num) const;
	void            pop_back();
	void            pop_front();
	// conversion
	inline int      ToInt() const;
	inline bool     ToBool() const;
	inline double	ToDouble() const;
	inline float	ToFloat() const;
	//
	inline bool     IsCorrectDouble() const;
	inline bool     IsCorrectInt() const;
	inline bool     IsCorrectBool() const;
	//
	inline bool     ContainsSubStr(const EString& Str) const;
	//
	static bool     CharIsLower(const char Ch);
	static bool     IsSeparator(const char Ch);
	static bool     IsDigit(const char Ch);
	static EString  ToUpper(const EString& Str);
	static EString  ReplaceAll(const EString& Str, char OldChar, char NewChar);
	operator const char*() const;
};

//
// Implementation
//

int EString::ToInt() const
{
	return atoi(c_str());
}

double EString::ToDouble() const
{
	return atof(c_str());
}

float EString::ToFloat() const
{
	return static_cast<float>(atof(c_str()));
}

bool EString::ToBool() const
{
	EString Str(*this);

	Str.ToUpper();

	return (Str == "TRUE")?true:false;
}

bool EString::IsCorrectDouble() const
{
	if (length()==0) return false;

	bool DecimalDot = false;

	for (size_t i=0; i != length(); ++i) {
		if ( at(i)=='-' || at(i)=='+' ) {
			if (i !=0 ) 
				return false;
			if (length() < 2 ) 
				return false;
			continue;
		}
		if (at(i)=='.') {
			if (DecimalDot) 
				return false; 
			else 
				DecimalDot = true;
			if (length() < 2 ) 
				return false;
			continue;
		}
		if (!IsDigit(at(i))) 
			return false;
	}
	return true;
}

bool EString::IsCorrectInt() const
{
	if (length()==0) 
		return false;

	for (size_t i=0; i != length(); ++i) {
		if ( at(i) == '-' || at(i)=='+' ) {
			if (i != 0 ) 
				return false;
			if (length() < 2 ) 
				return false;
			continue;
		}
		if (!IsDigit(at(i))) 
			return false;
	}
	return true;
}

bool EString::IsCorrectBool() const
{
	EString Str(*this);
	Str.ToUpper();
	return ((Str=="TRUE")||(Str=="FALSE"));
}

bool EString::ContainsSubStr(const EString& Str) const
{
	return ( find(Str) != EString::npos );
}
