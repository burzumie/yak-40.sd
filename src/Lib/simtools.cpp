/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Lib/PanelsTools.cpp $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "SimTools.h"

extern void FSAPI fnTimerHnd(DWORD event_id,PVOID buffer_specific,PVOID user_buffer,PVOID user_param)
{
	CTimer *pMyHandle=(CTimer *)user_param;
	if(!pMyHandle->started)return;
	pMyHandle->fnHandler();
}

CTimer::CTimer(FS6 *var)
{
	pFS6=var;
	started=false;
	chain=CHAINID_MAX;
}

CTimer::~CTimer()
{
	StopTimer();
}

void CTimer::StartTimer(CHAINID _chain)
{
	StopTimer();
	chain=_chain;
	if(started)return;
	pFS6->chain_insert2(chain,fnTimerHnd,0,this);
	started=true;
}

void CTimer::StopTimer()
{
	if(!started)return;
	pFS6->chain_delete2(chain,fnTimerHnd,0,this);
	started=false;
}

extern bool FSAPI fnSimKeyHnd(ID32 event,UINT32 evdata,PVOID userdata)
{
	CSimKey *pMyHandle=(CSimKey *)userdata;
	if(!pMyHandle->started)return false;
	return pMyHandle->fnKeyHandler(event,evdata);
}

CSimKey::CSimKey(PANELS *var)
{
	pPanels=var;
	started=false;
}

CSimKey::~CSimKey()
{
	StopKey();
}

void CSimKey::StartKey()
{
	StopKey();
	if(started)return;
	pPanels->register_key_event_handler((GAUGE_KEY_EVENT_HANDLER)fnSimKeyHnd,this);
	started=true;
}

void CSimKey::StopKey()
{
	if(!started)return;
	pPanels->unregister_key_event_handler((GAUGE_KEY_EVENT_HANDLER)fnSimKeyHnd,this);
	started=false;
}

//////////////////////////////////////////////////////////////////////////

map <UINT_PTR, STimerInfo> CTimerMixin::m_mapSysID2Info;
void TimerProc(HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime);

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////

CTimerMixin::CTimerMixin ()
{
}

CTimerMixin::~CTimerMixin ()
{
	KillAllTimers();
}

bool CTimerMixin::SetTimer (unsigned int uID, unsigned int uTime)
{
	// check for existing timer with such UserID
	mapUserID2SysID::const_iterator iter;
	iter=m_mapUserID2SysID.find(uID);
	// if exist - kill that timer
	if (iter != m_mapUserID2SysID.end()) KillTimer(uID);

	// set new timer and store timer-info
	UINT_PTR uSysID=::SetTimer(NULL, 0, uTime, TimerProc);
	if (uSysID != 0)
	{
		m_mapSysID2Info[uSysID]=STimerInfo(uID, this);
		m_mapUserID2SysID[uID]=uSysID;
		return true;
	} 
	else return false;
}

void CTimerMixin::KillTimer (unsigned int uID)
{
	::KillTimer(NULL, m_mapUserID2SysID[uID]);
	m_mapSysID2Info.erase(m_mapUserID2SysID[uID]);
	m_mapUserID2SysID.erase(uID);
}

void CTimerMixin::KillAllTimers (void)
{
	mapSysID2Info::const_iterator iter;
	for (iter=m_mapSysID2Info.begin(); iter!=m_mapSysID2Info.end(); iter++)
		::KillTimer(NULL, iter->first);
	m_mapSysID2Info.clear();
	m_mapUserID2SysID.clear();
}

void CTimerMixin::TimerProc (HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
{
	// find instance for timer with idEvent
	mapSysID2Info::const_iterator iter;
	iter=m_mapSysID2Info.find(idEvent);
	if (iter==m_mapSysID2Info.end()) return;
	// call OnTimer implementation with user-assigned timer ID
	else iter->second.pInstance->OnTimer(iter->second.uUserID);
}
