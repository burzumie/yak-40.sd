/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Lib/Strings.cpp $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Strings.h"

#pragma warning(disable: 4996)

HRESULT ConvertAnsiStringToWideCch(WCHAR *wstrDestination, const char *strSource, int cchDestChar)
{
	if(wstrDestination==NULL || strSource==NULL || cchDestChar < 1)
		return E_INVALIDARG;

	int nResult = MultiByteToWideChar(CP_ACP, 0, strSource, -1, wstrDestination, cchDestChar);
	wstrDestination[cchDestChar-1] = 0;

	if( nResult == 0 )
		return E_FAIL;

	return S_OK;
}

HRESULT ConvertWideStringToAnsiCch(char *strDestination, const WCHAR *wstrSource, int cchDestChar)
{
	if(strDestination==NULL || wstrSource==NULL || cchDestChar < 1)
		return E_INVALIDARG;

	int nResult = WideCharToMultiByte(CP_ACP, 0, wstrSource, -1, strDestination, cchDestChar*sizeof(CHAR), NULL, NULL);
	strDestination[cchDestChar-1] = 0;

	if( nResult == 0 )
		return E_FAIL;
	
	return S_OK;
}

void ExtractPath(char *in)
{
	in[strrchr((char *)in,'\\')-(char *)in+1]='\0';
}

// ���������� �������
const char  chWhite[]  =  " \n\t\r";
const WCHAR wchWhite[] = L" \n\t\r";

/* ���������� ��� ������ rStr1 � rStr2, ���������� true, ���� ��� �����
!������ eng! */
bool CompareNoCase(const std::string &rStr1, const std::string &rStr2)
{
	return 0 == strcmpi(rStr1.c_str(), rStr2.c_str());
}

bool CompareNoCase(const std::wstring &rStr1, const std::wstring &rStr2)
{
	return 0 == wcsicmp(rStr1.c_str(), rStr2.c_str());
}

/* ������� ���������� ������� (chWhite) � ����� ������, ������, ����� */
void TrimBoth(std::string &rString)
{
	TrimLeft(rString);
	TrimRight(rString);
}

void TrimRight(std::string &rString)
{
	rString = rString.substr(0, rString.find_last_not_of(chWhite) + 1);
}

void TrimLeft(std::string &rString)
{
	int nFirst = rString.find_first_not_of(chWhite);
	if (rString.npos == nFirst) 
		return;
	rString = rString.substr(nFirst);
}

void TrimBoth(std::wstring &rString)
{
	TrimLeft(rString);
	TrimRight(rString);
}

void TrimRight(std::wstring &rString)
{
	rString = rString.substr(0, rString.find_last_not_of(wchWhite) + 1);
}

void TrimLeft(std::wstring &rString)
{
	int nFirst = rString.find_first_not_of(wchWhite);
	if (rString.npos == nFirst) 
		return;
	rString = rString.substr(nFirst);
}

CTokenizerW::CTokenizerW(const std::wstring& cs, const std::wstring& csDelim) : m_cs(cs), m_nCurPos(0)
{
	SetDelimiters(csDelim);
}

void CTokenizerW::SetDelimiters(const std::wstring& csDelim)
{
	for(size_t i = 0; i < csDelim.length(); ++i)
		m_delim.set(static_cast<int>(csDelim[i]));
}

bool CTokenizerW::Next(std::wstring& cs)
{
	cs.erase();// .clear();

	while(m_nCurPos < m_cs.length() && m_delim[static_cast<int>(m_cs[m_nCurPos])])
		++m_nCurPos;

	if(m_nCurPos >= m_cs.length())
		return false;

	size_t nStartPos = m_nCurPos;
	while(m_nCurPos < m_cs.length() && !m_delim[static_cast<int>(m_cs[m_nCurPos])])
		++m_nCurPos;

	cs = m_cs.substr(nStartPos, m_nCurPos - nStartPos);

	return true;
}

std::wstring CTokenizerW::Tail() const
{
	size_t nCurPos = m_nCurPos;

	while(nCurPos < m_cs.length() && m_delim[static_cast<int>(m_cs[nCurPos])])
		++nCurPos;

	std::wstring csResult;

	if(nCurPos < m_cs.length())
		csResult = m_cs.substr(nCurPos);

	return csResult;
}

CTokenizerA::CTokenizerA(const std::string& cs, const std::string& csDelim) : m_cs(cs), m_nCurPos(0)
{
	SetDelimiters(csDelim);
}

void CTokenizerA::SetDelimiters(const std::string& csDelim)
{
	for(size_t i = 0; i < csDelim.length(); ++i)
		m_delim.set(static_cast<int>(csDelim[i]));
}

bool CTokenizerA::Next(std::string& cs)
{
	cs.erase(); //clear();

	while(m_nCurPos < m_cs.length() && m_delim[static_cast<int>(m_cs[m_nCurPos])])
		++m_nCurPos;

	if(m_nCurPos >= m_cs.length())
		return false;

	size_t nStartPos = m_nCurPos;
	while(m_nCurPos < m_cs.length() && !m_delim[static_cast<int>(m_cs[m_nCurPos])])
		++m_nCurPos;

	cs = m_cs.substr(nStartPos, m_nCurPos - nStartPos);

	return true;
}

std::string CTokenizerA::Tail() const
{
	size_t nCurPos = m_nCurPos;

	while(nCurPos < m_cs.length() && m_delim[static_cast<int>(m_cs[nCurPos])])
		++nCurPos;

	std::string csResult;

	if(nCurPos < m_cs.length())
		csResult = m_cs.substr(nCurPos);

	return csResult;
}

#pragma warning(disable: 4996)
//////////////////////////////////////////////////////////////////////////
// StrMap
//

static int cnt=0;

CStrMap::CStrMap(int extrabytes){
	FList[0] = '\0';// NULL;
	FCount = 0;
	FCapacity = 0;
	FExtraLen = extrabytes;
	FRecordLen = sizeof(char*) + sizeof(int) + extrabytes;
}

void CStrMap::FillFromChain(char *strchain, void *data){
	while ( *strchain ) {
		int len = (int)strlen( strchain );
		AddStr(strchain, len, data );
		strchain += len+1;
		data = (char*)data + FExtraLen;
	}
}

void CStrMap::CreateFromChain( int extrabytes, char *strchain, void *data ){
	FExtraLen = extrabytes;
	FRecordLen = sizeof(char*) + sizeof(int) + extrabytes;
	FillFromChain( strchain, data );
	ShrinkMem();
}

CStrMap::~CStrMap()
{
	Clear();
}

void CStrMap::AddString( const char *str, void *data )
{
	AddStr( str, (int)strlen(str), data );
}

void CStrMap::AddStr( const char *str, int len, void *data )
{
	if (FCount >= FCapacity ) {
		int delta = (FCapacity > 64) ? FCapacity / 4 : 16;
		SetCapacity( FCapacity + delta );
	}
	char *Rec;
	Rec = FList + FCount * FRecordLen;
	*(char**)Rec = (char *)malloc(len + FExtraLen + sizeof(int));
	strncpy(*(char**)Rec,str,len);
	*(int*)(Rec + sizeof(char*)) = len;
	if (data) {
		void *recdata = (Rec + sizeof(char*) + sizeof(int));
		memcpy( recdata, data, FExtraLen );
	}
	FCount++;
}

void CStrMap::ShrinkMem()
{
	SetCapacity( FCount );
}

void CStrMap::Trim(int NewCount)
{
	FCount = NewCount;
}

void CStrMap::TrimClear(int NewCount)
{
	if(NewCount < FCount){
		int i;
		char *Rec = FList + NewCount * FRecordLen;
		for (i = NewCount; i < FCount; i++) {
			free( *(char**)Rec );
			Rec += FRecordLen;
		}
		FCount = NewCount;
	}
}

void CStrMap::SetCapacity(int NewCapacity)
{
	FCapacity = NewCapacity;
	if (FCount >FCapacity )
		FCount = FCapacity;

}

int CStrMap::IndexOf(char *str, void **data)
{
	return IndexOf( str, (int)strlen(str), data );
}

int CStrMap::IndexOf( char *str, int len, void **data )
{
	int i;
	char *Rec = FList;
	for (i=0; i<FCount; i++) {
		int recLen = *(int*)(Rec + sizeof(char*));
		if (recLen==len && strncmp( str, *(char**)Rec, recLen )==0 ) {
			*data = (Rec + sizeof(char*) + sizeof(int));
			return i;
		}
		Rec += FRecordLen;
	}
	*data = NULL;
	return -1;
}

int CStrMap::Replace( char *str, void *data )
{
	return Replace( str, (int)strlen(str), data );
}

int CStrMap::Replace( char *str, int len, void *data )
{
	int i;
	char *Rec = FList;
	for (i=0; i<FCount; i++) {
		int recLen = *(int*)(Rec + sizeof(char*));
		if (recLen==len && strncmp( str, *(char**)Rec, recLen )==0 ) {
			void *recdata = (Rec + sizeof(char*) + sizeof(int));
			memcpy( recdata, data, FExtraLen );			
			return i;
		}
		Rec += FRecordLen;
	}
	return -1;
}

char* CStrMap::GetString( int index, int *len, void **data )
{
	char *Rec = FList + index * FRecordLen;
	*len =  *(int*)(Rec + sizeof(char*));
	if (data!=NULL && FExtraLen>0)
		*data = (Rec + sizeof(char*) + sizeof(int));
	return *(char**)Rec;
}

void CStrMap::Clear()
{
	TrimClear( 0 );
	/*
	if (FList){
		free(FList);
		FList = NULL;
	}
	*/
}

//////////////////////////////////////////////////////////////////////////
// Symtable
//
static char MathSymbols[] =
"\033\002" "<<" ">>" "**" "<>" ">=" "<=" "&&" "||" "/*" ":="
"\033\001" "(+-*/%$^~&|=><?:),;";

CSymTable::~CSymTable(){
	for(int i = 0; i < 256 ; i++){
		free(table[ i ]);
	}
}

void CSymTable::PrepareSymbols( char *symbols ){
	int i = 0, nchars = 1;
	memset( table, 0, 256 * sizeof(SymbolRec*) );
	while (*symbols) {
		if (*symbols=='\033') {
			nchars = *++symbols;
			++symbols;
		} else {
			SymbolRec **RecList = &table [*symbols];
			SymbolRec *Rec = *RecList;
			int count = 0;
			while ( Rec ) {
				++count;
				if ( Rec->More )
					++Rec;
				else
					break;
			}
			if ( Rec ) {
				*RecList = (SymbolRec*)
					realloc( *RecList, (count+1)*sizeof(SymbolRec) );
				Rec = *RecList + count;
				(Rec-1)->More = 1;
			} else {
				SymbolRec* R = (SymbolRec*) malloc( 7 );
				*RecList=R;
				Rec = *RecList;
			}
			strncpy( Rec->Sym, symbols, 4 );
			Rec->Len = (char) nchars;
			Rec->Index = (char) i;
			Rec->More = 0;
			symbols += nchars;
			++i;
		}
	}
}

int CSymTable::FindSymbol( char *str, int *nchars )
{
	SymbolRec *Rec = table[ (int)*str ];
	while ( Rec ) {
		if ( (Rec->Len == 1 && Rec->Sym[0] == str[0])
			||
			(Rec->Len == 2 && Rec->Sym[0] == str[0] && Rec->Sym[1] == str[1])
			||
			(Rec->Len == 3 && Rec->Sym[0] == str[0] && Rec->Sym[1] == str[1]
		&& Rec->Sym[2] == str[2])
			) {
				*nchars = Rec->Len;
				return Rec->Index;
		}
		Rec = ( Rec->More ) ? Rec + 1 : NULL;
	}
	return -1;
}

CMathSymTable::CMathSymTable(){
	PrepareSymbols(MathSymbols);
}

//////////////////////////////////////////////////////////////////////////
// Lexer
//

/* Uppercase translation table for the Win1251 charset */
const char Win1251UpcaseTbl[] =
"\000\001\002\003\004\005\006\007\010\011\012\013\014\015\016\017"
"\020\021\022\023\024\025\026\027\030\031\032\033\034\035\036\037"
" !\042#$%&'()*+,-./0123456789:;<=>?"
"@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
"`ABCDEFGHIJKLMNOPQRSTUVWXYZ{|}~\177"
"\200\201\202\203\204\205\206\207\210\211\212\213\214\215\216\217"
"\220\221\222\223\224\225\226\227\230\231\232\233\234\235\236\237"
"\240\241\242\243\244\245\246\247\250\251\252\253\254\255\256\257"
"\260\261\262\263\264\265\266\267\270\271\272\273\274\275\276\277"
"\300\301\302\303\304\305\306\307\310\311\312\313\314\315\316\317"
"\320\321\322\323\324\325\326\327\330\331\332\333\334\335\336\337"
"\300\301\302\303\304\305\306\307\310\311\312\313\314\315\316\317"
"\320\321\322\323\324\325\326\327\330\331\332\333\334\335\336\337";

// initializations

void InitCharTypeTable( hqCharType *CharTypeTable, int CharTypes )
{
	int ch;
#ifdef MY_DEBUG
	printf( "CharTypeTable = 0x%X; CharTypes = %d\n", (unsigned)CharTypeTable,
		CharTypes );
#endif
	memset( CharTypeTable, CH_UNKNOWN, 256 * sizeof(hqCharType) );

	CharTypeTable[0] = CH_FINAL;

	if (CharTypes & CH_SEPARAT) {
		CharTypeTable[' '] = CH_SEPARAT;
		CharTypeTable[9]  = CH_SEPARAT;
		CharTypeTable[13]  = CH_SEPARAT;
		CharTypeTable[10]  = CH_SEPARAT;
	}

	if (CharTypes & CH_QUOTE) {
		CharTypeTable['\'']  = CH_QUOTE;
	}

	if (CharTypes & CH_LETTER) {
		for (ch='A'; ch<='Z'; ch++)
			CharTypeTable[ch] = CH_LETTER;
		for (ch='a'; ch<='z'; ch++)
			CharTypeTable[ch] = CH_LETTER;
		CharTypeTable['_'] = CH_LETTER;
	}

	if (CharTypes & CH_DIGIT) {
		for (ch='0'; ch<='9'; ch++)
			CharTypeTable[ch] = CH_DIGIT;
	}
}

// CLexer implementation

#define CHARTYPEPP CharTypeTable[ (uchar) *++(SS) ]
#define CHARTYPE CharTypeTable[ (uchar) *SS ]

int CLexer::SetParseString(const char *str )
{
	PrevTokenType = TOK_NONE;
	if ( !str || !*str )
		return 0;

	if(str_)
		free(str_);
	str_ = (char *)malloc(strlen(str)+1);
	strcpy(str_, str);
	SS = str_;
	CharType = CHARTYPE;
	return 1;
}

hqTokenType CLexer::GetNextToken()
{
	hqTokenType result = TOK_ERROR;

next_token:

	while ( CharType == CH_SEPARAT )
		CharType = CHARTYPEPP;

	switch ( CharType ) {
	case CH_FINAL:
		result = TOK_FINAL;
		break;
	case CH_LETTER:
		Name = SS;
		do {
			CharType = CHARTYPEPP;
		} while (CharType <= CH_DIGIT);
		NameLen = SS - Name;
		result = TOK_NAME;
		break;
	case CH_DIGIT: {
		char *NewSS;
		if ( *SS == '0' && *(SS+1) == 'x' ) {
			IntValue = strtol( SS, &NewSS, 16 );
			if ( SS != NewSS ) {
				SS = NewSS;
				if (NoIntegers) {
					ExtValue = IntValue;
					result = TOK_FLOAT;
				} else
					result = TOK_INT;
				CharType = CHARTYPE;
			}
			break;
		}
		ExtValue = strtod( SS, &NewSS );
		if ( SS != NewSS ) {;
		SS = NewSS;
		if ( !NoIntegers
			&& ExtValue<=INT_MAX
			&& ExtValue>=INT_MAX
			&& (double)( IntValue = (uchar) ExtValue )
			== ExtValue ) {
				result = TOK_INT;
		} else
			result = TOK_FLOAT;
		CharType = CHARTYPE;
		}
		break;
				   }
	case CH_SYMBOL: {
		int nchars;
		int i = SymTable->FindSymbol( SS, &nchars );
		if (i >= 0) {
			SS += nchars;
			if (i == cssn) {
				char comend = *ComEnd;
				char comendpp = *(ComEnd+1);
				while ( *SS ) {
					if ( *SS == comend
						&& 
						( comendpp == '\0' || *(SS+1) == comendpp )
						) {
							++SS;
							if (comendpp != '\0')
								++SS;
							CharType = CHARTYPE;
							goto next_token;
					}
					++SS;
				}
				break;
			}
			CharType = CHARTYPE;
			IntValue = i;
			result = TOK_SYMBOL;
		}
		break;
					}
	case CH_QUOTE:
		Name = ++(SS);
		while ( CharTypeTable[ (uchar)*SS ] != CH_QUOTE
			&& *(SS) != '\0' )
			++SS;
		if ( CHARTYPE == CH_QUOTE ) {
			NameLen = SS - Name;
			++SS;
			CharType = CHARTYPE;
			result = TOK_STRING;
		}
		break;
	default:
		break;
	}
	return PrevTokenType = result;
}

char* CLexer::GetCurrentPos()
{
	return SS;
}
