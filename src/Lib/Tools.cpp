/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Lib/Tools.cpp $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Tools.h"

UINT32 HornerScheme(UINT32 Num,UINT32 Divider,UINT32 Factor)
{
	UINT32 Remainder=0,Quotient=0,Result=0;
	Remainder=Num%Divider;
	Quotient=Num/Divider;
	if(!(Quotient==0&&Remainder==0)) {
		Result+=HornerScheme(Quotient,Divider,Factor)*Factor+Remainder;
	}
	return Result;
}
