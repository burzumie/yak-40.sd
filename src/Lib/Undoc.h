/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Lib/ctimer.cpp $

Last modification:
$Date: 18.02.06 11:43 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "Tools.h"
#include "SimTools.h"

extern PSIM_DATA m_pSimData;

inline void UndocUpdate()
{
	m_pSimData=ImportTable.pSIM1->sim1_get_UserSimData();
}

inline void UndocReset()
{
	m_pSimData=NULL;
}

// Flaps
inline double	GetLeftFlapPosDg()							{if(!m_pSimData)return 0; return rddg(m_pSimData->pFlapSystem->pFlapSystemList->flap_pos[0]);}
inline double	GetRightFlapPosDg()							{if(!m_pSimData)return 0; return rddg(m_pSimData->pFlapSystem->pFlapSystemList->flap_pos[1]);}
inline double	GetLeftFlapPosRd()							{if(!m_pSimData)return 0; return m_pSimData->pFlapSystem->pFlapSystemList->flap_pos[0];}
inline double	GetRightFlapPosRd()							{if(!m_pSimData)return 0; return m_pSimData->pFlapSystem->pFlapSystemList->flap_pos[1];}
inline void		SetLeftFlapPosDg(double val)				{if(!m_pSimData)return;   m_pSimData->pFlapSystem->pFlapSystemList->flap_pos[0]=dgrd(val);}
inline void		SetRightFlapPosDg(double val)				{if(!m_pSimData)return;   m_pSimData->pFlapSystem->pFlapSystemList->flap_pos[1]=dgrd(val);}
inline void		SetLeftFlapPosRd(double val)				{if(!m_pSimData)return;   m_pSimData->pFlapSystem->pFlapSystemList->flap_pos[0]=val;}
inline void		SetRightFlapPosRd(double val)				{if(!m_pSimData)return;   m_pSimData->pFlapSystem->pFlapSystemList->flap_pos[1]=val;}
inline UINT		GetMaxFlapHandleIndex()						{if(!m_pSimData)return 0; return m_pSimData->pFlapSystem->MaxFlapHandleIndex;}
inline void		SetMaxFlapHandleIndex(UINT val)				{if(!m_pSimData)return;   m_pSimData->pFlapSystem->MaxFlapHandleIndex=val;}
inline UINT		GetMaxFlapIndex()							{if(!m_pSimData)return 0; return m_pSimData->pFlapSystem->pFlapSystemList->pFlapFixedData->MaxFlapIndex;}
inline void		SetMaxFlapIndex(UINT val)					{if(!m_pSimData)return;   m_pSimData->pFlapSystem->pFlapSystemList->pFlapFixedData->MaxFlapIndex=val;}
inline UINT		GetFlapHandleIndex()						{if(!m_pSimData)return 0; return m_pSimData->pFlapSystem->FlapHandleIndex;}
inline void		SetFlapHandleIndex(UINT val)				{if(!m_pSimData)return;   m_pSimData->pFlapSystem->FlapHandleIndex=val;}
inline void		IncFlapHandleIndex()						{if(!m_pSimData)return;   m_pSimData->pFlapSystem->FlapHandleIndex++;}
inline void		DecFlapHandleIndex()						{if(!m_pSimData)return;   m_pSimData->pFlapSystem->FlapHandleIndex--;}
inline double	GetHandleToPosTableIn(int idx)				{if(!m_pSimData)return 0; return m_pSimData->pFlapSystem->pFlapSystemList->pFlapFixedData->HandleToPosTable.table[idx].tin;}
inline double	GetHandleToPosTableOut(int idx)				{if(!m_pSimData)return 0; return m_pSimData->pFlapSystem->pFlapSystemList->pFlapFixedData->HandleToPosTable.table[idx].tout;}
inline void		SetHandleToPosTableIn(int idx,double val)	{if(!m_pSimData)return;   m_pSimData->pFlapSystem->pFlapSystemList->pFlapFixedData->HandleToPosTable.table[idx].tin=val;}
inline void		SetHandleToPosTableOut(int idx,double val)	{if(!m_pSimData)return;   m_pSimData->pFlapSystem->pFlapSystemList->pFlapFixedData->HandleToPosTable.table[idx].tout=val;}
inline double	GetHandleToPosTableIn2(int idx)				{if(!m_pSimData)return 0; return m_pSimData->pFlapSystem->pFlapSystemList->pNextFlap->pFlapFixedData->HandleToPosTable.table[idx].tin;}
inline double	GetHandleToPosTableOut2(int idx)			{if(!m_pSimData)return 0; return m_pSimData->pFlapSystem->pFlapSystemList->pNextFlap->pFlapFixedData->HandleToPosTable.table[idx].tout;}
inline void		SetHandleToPosTableIn2(int idx,double val)	{if(!m_pSimData)return;   m_pSimData->pFlapSystem->pFlapSystemList->pNextFlap->pFlapFixedData->HandleToPosTable.table[idx].tin=val;}
inline void		SetHandleToPosTableOut2(int idx,double val)	{if(!m_pSimData)return;   m_pSimData->pFlapSystem->pFlapSystemList->pNextFlap->pFlapFixedData->HandleToPosTable.table[idx].tout=val;}

// Gear
inline GEAR_MOVEMENT_TYPE IsLeftGearMovement()				{if(!m_pSimData)return MOVEMENT_NONE; return m_pSimData->ContactReaction.pLeftGear->pArticulateVars->Movement;}
inline GEAR_MOVEMENT_TYPE IsCenterGearMovement()			{if(!m_pSimData)return MOVEMENT_NONE; return m_pSimData->ContactReaction.pCenterGear->pArticulateVars->Movement;}
inline GEAR_MOVEMENT_TYPE IsRightGearMovement()				{if(!m_pSimData)return MOVEMENT_NONE; return m_pSimData->ContactReaction.pRightGear->pArticulateVars->Movement;}
inline double	GetNoseLegMaxSteerAngle()					{if(!m_pSimData)return 0; return rddg(m_pSimData->pStaticData->ContactFixedData.pContactPointsFixedData[0].MaxSteerAngle);}
inline void  	SetNoseLegMaxSteerAngle(double val)			{if(!m_pSimData)return;   m_pSimData->pStaticData->ContactFixedData.pContactPointsFixedData[0].MaxSteerAngle=dgrd(val);}

// Fuel
inline double	GetLeftTankQuantityLbs()					{if(!m_pSimData)return 0; return m_pSimData->LeftMain.quantity_lbs;}
inline double	GetRightTankQuantityLbs()					{if(!m_pSimData)return 0; return m_pSimData->RightMain.quantity_lbs;}
inline void  	SetLeftTankQuantityLbs(double val)			{if(!m_pSimData)return;   m_pSimData->LeftMain.quantity_lbs=val;}
inline void  	SetRightTankQuantityLbs(double val)			{if(!m_pSimData)return;   m_pSimData->RightMain.quantity_lbs=val;}
inline void  	IncLeftTankQuantityLbs(double val=1)		{if(!m_pSimData)return;   m_pSimData->LeftMain.quantity_lbs+=val;}
inline void  	IncRightTankQuantityLbs(double val=1)		{if(!m_pSimData)return;   m_pSimData->RightMain.quantity_lbs+=val;}
inline void  	DecLeftTankQuantityLbs(double val=1)		{if(!m_pSimData)return;   m_pSimData->LeftMain.quantity_lbs-=val;}
inline void  	DecRightTankQuantityLbs(double val=1)		{if(!m_pSimData)return;   m_pSimData->RightMain.quantity_lbs-=val;}
inline double	GetLeftTankQuantityKg()						{if(!m_pSimData)return 0; return LBS_TO_KG(m_pSimData->LeftMain.quantity_lbs);}
inline double	GetRightTankQuantityKg()					{if(!m_pSimData)return 0; return LBS_TO_KG(m_pSimData->RightMain.quantity_lbs);}
inline void  	SetLeftTankQuantityKg(double val)			{if(!m_pSimData)return;   m_pSimData->LeftMain.quantity_lbs=KG_TO_LBS(val);}
inline void  	SetRightTankQuantityKg(double val)			{if(!m_pSimData)return;   m_pSimData->RightMain.quantity_lbs=KG_TO_LBS(val);}
inline void  	IncLeftTankQuantityKg(double val=1)			{if(!m_pSimData)return;   m_pSimData->LeftMain.quantity_lbs+=KG_TO_LBS(val);}
inline void  	IncRightTankQuantityKg(double val=1)		{if(!m_pSimData)return;   m_pSimData->RightMain.quantity_lbs+=KG_TO_LBS(val);}
inline void  	DecLeftTankQuantityKg(double val=1)			{if(!m_pSimData)return;   m_pSimData->LeftMain.quantity_lbs-=KG_TO_LBS(val);}
inline void  	DecRightTankQuantityKg(double val=1)		{if(!m_pSimData)return;   m_pSimData->RightMain.quantity_lbs-=KG_TO_LBS(val);}
inline double	GetLeftTankPosLat()							{if(!m_pSimData)return 0; return m_pSimData->pStaticData->LeftMainTankFixedData.pos.lat;}
inline double	GetRightTankPosLat()						{if(!m_pSimData)return 0; return m_pSimData->pStaticData->RightMainTankFixedData.pos.lat;}

// Mass
inline double	GetPayloadWeightPound(int idx)				{if(!m_pSimData)return 0; return m_pSimData->DynamicMassProps.pPayload[idx].weight;}
inline double	GetPayloadWeightKg(int idx)					{if(!m_pSimData)return 0; return POUND_TO_KILOGRAM(m_pSimData->DynamicMassProps.pPayload[idx].weight);}
inline void  	SetPayloadWeightPound(int idx,double val)	{if(!m_pSimData)return;   m_pSimData->DynamicMassProps.pPayload[idx].weight=val;}
inline void  	SetPayloadWeightKg(int idx,double val)		{if(!m_pSimData)return;   m_pSimData->DynamicMassProps.pPayload[idx].weight=KILOGRAM_TO_POUND(val);}
inline UINT		GetPayloadNumberOfStations()				{if(!m_pSimData)return 0; return m_pSimData->pStaticData->pMassProps->number_of_payload_stations;}
inline void		SetPayloadNumberOfStations(UINT val)		{if(!m_pSimData)return;   m_pSimData->pStaticData->pMassProps->number_of_payload_stations=val;}
inline double	GetPayloadPosLat(int idx)					{if(!m_pSimData)return 0; return m_pSimData->DynamicMassProps.pPayload[idx].pos.lat;}
inline void  	SetPayloadPosLat(int idx,double val)		{if(!m_pSimData)return;   m_pSimData->DynamicMassProps.pPayload[idx].pos.lat=val;}
inline double	GetEmptyCgPosLat()							{if(!m_pSimData)return 0; return m_pSimData->pStaticData->pMassProps->empty_cg_position.lat;}
inline void  	SetEmptyCgPosLat(double val)				{if(!m_pSimData)return;   m_pSimData->pStaticData->pMassProps->empty_cg_position.lat=val;}
inline double	GetEmptyWeightPound()						{if(!m_pSimData)return 0; return m_pSimData->pStaticData->pMassProps->empty_weight;}
inline double	GetEmptyWeightKg()							{if(!m_pSimData)return 0; return POUND_TO_KILOGRAM(m_pSimData->pStaticData->pMassProps->empty_weight);}
inline double	GetMaxGrossWeightPound()					{if(!m_pSimData)return 0; return m_pSimData->pStaticData->pMassProps->max_gross_weight;}
inline double	GetMaxGrossWeightKg()						{if(!m_pSimData)return 0; return POUND_TO_KILOGRAM(m_pSimData->pStaticData->pMassProps->max_gross_weight);}
inline void  	SetEmptyWeightPound(double val)				{if(!m_pSimData)return;   m_pSimData->pStaticData->pMassProps->empty_weight=val;}
inline void  	SetEmptyWeightKg(double val)				{if(!m_pSimData)return;   m_pSimData->pStaticData->pMassProps->empty_weight=KILOGRAM_TO_POUND(val);}
inline void  	SetMaxGrossWeightPound(double val)			{if(!m_pSimData)return;   m_pSimData->pStaticData->pMassProps->max_gross_weight=val;}
inline void  	SetMaxGrossWeightKg(double val)				{if(!m_pSimData)return;   m_pSimData->pStaticData->pMassProps->max_gross_weight=KILOGRAM_TO_POUND(val);}

// HTail
inline double	GetHtailIncidenceDg()						{if(!m_pSimData)return 0; return rddg(m_pSimData->pStaticData->pAirplaneGeometry->htail_incidence);}
inline double	GetHtailIncidenceRd()						{if(!m_pSimData)return 0; return m_pSimData->pStaticData->pAirplaneGeometry->htail_incidence;}
inline void  	SetHtailIncidenceDg(double val)				{if(!m_pSimData)return;   m_pSimData->pStaticData->pAirplaneGeometry->htail_incidence=dgrd(val);}
inline void  	SetHtailIncidenceRd(double val)				{if(!m_pSimData)return;   m_pSimData->pStaticData->pAirplaneGeometry->htail_incidence=val;}

// Engines
inline USHORT	GetNumOfEngines()							{if(!m_pSimData)return 0; return m_pSimData->pStaticData->Number_of_engines;}
inline void		SetNumOfEngines(USHORT val)					{if(!m_pSimData)return;   m_pSimData->pStaticData->Number_of_engines=val;}
inline float	GetThrottleLowerLimitPct()					{if(!m_pSimData)return 0; return m_pSimData->pStaticData->ThrottleLowerLimitPct;}
inline void 	SetThrottleLowerLimitPct(float val)			{if(!m_pSimData)return;   m_pSimData->pStaticData->ThrottleLowerLimitPct=val;}
inline double	GetThrottleLeverPos(int idx)				{if(!m_pSimData)return 0; return m_pSimData->gen_eng_array[idx].EngCntrls.throttle_lever_pos;}
inline void  	SetThrottleLeverPos(int idx,double val)		{if(!m_pSimData)return;   m_pSimData->gen_eng_array[idx].EngCntrls.throttle_lever_pos=val;}
inline double	GetJetThrustScalar()						{if(!m_pSimData)return 0; return m_pSimData->pStaticData->jet_engine_tuning.JetThrustScalar;}
inline void  	SetJetThrustScalar(double val)				{if(!m_pSimData)return;   m_pSimData->pStaticData->jet_engine_tuning.JetThrustScalar=val;}

// Parameters
inline void		GetSimDataP(void *p,_sim_data_type typ)		{ImportTable.pSIM1->GetSimDataP(m_pSimData,&p,typ);}
inline void		SetSimDataP(void *p,_sim_data_type typ)		{ImportTable.pSIM1->SetSimDataP(m_pSimData,&p,typ);}

// Electrical
inline double	GetTotalLoadAmps()							{if(!m_pSimData)return 0; return m_pSimData->ElectricalSystem.TotalLoadAmps;}
inline void  	SetTotalLoadAmps(double val)				{if(!m_pSimData)return;   m_pSimData->ElectricalSystem.TotalLoadAmps=val;}
inline int		GetBatterySwitch()							{if(!m_pSimData)return 0; return m_pSimData->ElectricalSystem.fBatterySwitch;}
inline void  	SetBatterySwitch(int val)					{if(!m_pSimData)return;   m_pSimData->ElectricalSystem.fBatterySwitch=val;}
inline int		GetAvionicsMasterSwitch()					{if(!m_pSimData)return 0; return m_pSimData->ElectricalSystem.fAvionicsMasterSwitch;}
inline void  	SetAvionicsMasterSwitch(int val)			{if(!m_pSimData)return;   m_pSimData->ElectricalSystem.fAvionicsMasterSwitch=val;}
inline double	GetBatteryVoltage()							{if(!m_pSimData)return 0; return m_pSimData->ElectricalSystem.BatteryVoltage;}
inline void  	SetBatteryVoltage(double val)				{if(!m_pSimData)return;   m_pSimData->ElectricalSystem.BatteryVoltage=val;}
inline UINT		GetNumActiveGenBuses()						{if(!m_pSimData)return 0; return m_pSimData->ElectricalSystem.NumActiveGenBuses;}
inline void		SetNumActiveGenBuses(UINT val)				{if(!m_pSimData)return;   m_pSimData->ElectricalSystem.NumActiveGenBuses=val;}

// Gear
inline UINT		GetGear1FailFlag()							{if(!m_pSimData)return 0; return m_pSimData->gear1_fail_flag;}
inline UINT		GetGear2FailFlag()							{if(!m_pSimData)return 0; return m_pSimData->gear2_fail_flag;}
inline UINT		GetGear3FailFlag()							{if(!m_pSimData)return 0; return m_pSimData->gear3_fail_flag;}
inline void		SetGear1FailFlag(UINT val)					{if(!m_pSimData)return;   m_pSimData->gear1_fail_flag=val;}
inline void		SetGear2FailFlag(UINT val)					{if(!m_pSimData)return;   m_pSimData->gear2_fail_flag=val;}
inline void		SetGear3FailFlag(UINT val)					{if(!m_pSimData)return;   m_pSimData->gear3_fail_flag=val;}
inline float 	GetLandinGearHydraulicPressure()			{if(!m_pSimData)return 0; return m_pSimData->LandingGearSystem.HydraulicPressure;}
inline void		SetLandinGearHydraulicPressure(float val)	{if(!m_pSimData)return;   m_pSimData->LandingGearSystem.HydraulicPressure=val;}

// Brakes
inline float	GetBrakingCf()								{if(!m_pSimData)return 0; return m_pSimData->pStaticData->BrakingCf;}
inline void 	SetBrakingCf(float val)						{if(!m_pSimData)return;   m_pSimData->pStaticData->BrakingCf=val;}
inline int  	GetParkingBrake()							{if(!m_pSimData)return 0; return m_pSimData->bParkingBrakeOn;}
inline void 	SetParkingBrake(int val)					{if(!m_pSimData)return;   m_pSimData->bParkingBrakeOn=val;}
inline double	GetLeftBrakePos()							{if(!m_pSimData)return 0; return m_pSimData->Lbrake_pos;}
inline void 	SetLeftBrakePos(double val)					{if(!m_pSimData)return;   m_pSimData->Lbrake_pos=val;}
inline double	GetRightBrakePos()							{if(!m_pSimData)return 0; return m_pSimData->Rbrake_pos;}
inline void 	SetRightBrakePos(double val)				{if(!m_pSimData)return;   m_pSimData->Rbrake_pos=val;}

// Hydraulic
inline double	GetHydraulicSystemIntegrity()				{if(!m_pSimData)return 0; return m_pSimData->hydraulic_system_integrity;}
inline void		SetHydraulicSystemIntegrity(double val)		{if(!m_pSimData)return;   m_pSimData->hydraulic_system_integrity=val;}

// Aero tuning
inline double	GetYawStabilityScalar()						{if(!m_pSimData)return 0; return m_pSimData->pStaticData->aero_tuning.YawStabilityScalar;}
inline void		SetYawStabilityScalar(double val)			{if(!m_pSimData)return;   m_pSimData->pStaticData->aero_tuning.YawStabilityScalar=val;}
inline double	GetRollStabilityScalar()					{if(!m_pSimData)return 0; return m_pSimData->pStaticData->aero_tuning.RollStabilityScalar;}
inline void		SetRollStabilityScalar(double val)			{if(!m_pSimData)return;   m_pSimData->pStaticData->aero_tuning.RollStabilityScalar=val;}
inline double	GetPitchStabilityScalar()					{if(!m_pSimData)return 0; return m_pSimData->pStaticData->aero_tuning.PitchStabilityScalar;}
inline void		SetPitchStabilityScalar(double val)			{if(!m_pSimData)return;   m_pSimData->pStaticData->aero_tuning.PitchStabilityScalar=val;}
inline double	GetRudderEffScalar()						{if(!m_pSimData)return 0; return m_pSimData->pStaticData->aero_tuning.RudderEffScalar;}
inline void		SetRudderEffScalar(double val)				{if(!m_pSimData)return;   m_pSimData->pStaticData->aero_tuning.RudderEffScalar=val;}
inline double	GetAileronEffScalar()						{if(!m_pSimData)return 0; return m_pSimData->pStaticData->aero_tuning.AileronEffScalar;}
inline void		SetAileronEffScalar(double val)				{if(!m_pSimData)return;   m_pSimData->pStaticData->aero_tuning.AileronEffScalar=val;}
inline double	GetElevatorEffScalar()						{if(!m_pSimData)return 0; return m_pSimData->pStaticData->aero_tuning.ElevatorEffScalar;}
inline void		SetElevatorEffScalar(double val)			{if(!m_pSimData)return;   m_pSimData->pStaticData->aero_tuning.ElevatorEffScalar=val;}
inline double	GetParasiticDragScalar()					{if(!m_pSimData)return 0; return m_pSimData->pStaticData->aero_tuning.ParasiticDragScalar;}
inline void		SetParasiticDragScalar(double val)			{if(!m_pSimData)return;   m_pSimData->pStaticData->aero_tuning.ParasiticDragScalar=val;}
inline double	GetZeroAlphaLiftScalar()					{if(!m_pSimData)return 0; return m_pSimData->pStaticData->aero_tuning.ZeroAlphaLiftScalar;}
inline void		SetZeroAlphaLiftScalar(double val)			{if(!m_pSimData)return;   m_pSimData->pStaticData->aero_tuning.ZeroAlphaLiftScalar=val;}

// Trimmer
inline double  	GetAileronTrimPos()							{if(!m_pSimData)return 0; return m_pSimData->ailer_trim_pos;}
inline void 	SetAileronTrimPos(double val)				{if(!m_pSimData)return;   m_pSimData->ailer_trim_pos=val;}
inline double  	GetRudderTrimPos()							{if(!m_pSimData)return 0; return m_pSimData->rudder_trim_pos;}
inline void 	SetRudderTrimPos(double val)				{if(!m_pSimData)return;   m_pSimData->rudder_trim_pos=val;}

// Weather
inline float	GetTempC()									{float ret=0;POBSV current_weather = ImportTable.pWEATHER->GetGlobalObsv();if(current_weather){ret=current_weather->temperature->temp;}return ret;}
inline float	GetDewPoint()								{float ret=0;POBSV current_weather = ImportTable.pWEATHER->GetGlobalObsv();if(current_weather){ret=current_weather->temperature->dew_point;}return ret;}

// Global
inline USHORT	GetTimeOfDay()								{assert(ImportTable.pGlobal); return ImportTable.pGlobal->time_of_day;}
inline int		GetSoundMaster()							{assert(ImportTable.pGlobal); return ImportTable.pGlobal->sound_master;}
inline VIEW_MODE GetActiveViewMode()						{assert(ImportTable.pGlobal); return ImportTable.pGlobal->pactive_viewblock->view_mode;}

// Flight
inline UINT		GetFlightFileInfo(PFLIGHT_FILE_INFO var)	{return ImportTable.pFlight->flight_get(var);}

// Main
inline int		GetMainIsLoading()							{return ImportTable.pMain->main_is_loading();}

// Window
inline UINT		SetWindowTextSim(char *str, MSG_TYPE_ENUM type, unsigned int v1, int v2) {return ImportTable.pWindow->show_message(str,type,v1,v2);}

// View
inline void		ViewMoveTo(SINT16 x,SINT16 y,SINT16 z,int slot=2)		{PVIEWBLOCK pv=ImportTable.pGlobal->pactive_viewblock;pv->eyepoint_offset[slot].x=x;pv->eyepoint_offset[slot].y=y;pv->eyepoint_offset[slot].z=z;}
inline void		ViewPanReset()								{ImportTable.pWindow->view_window_pan_reset(ImportTable.pGlobal->pactive_awind);}
inline void		ViewPanDown()								{ImportTable.pWindow->view_window_pan(ImportTable.pGlobal->pactive_awind,180);}
inline void		ViewPanUp()									{ImportTable.pWindow->view_window_pan(ImportTable.pGlobal->pactive_awind,0);}
inline void		ViewPanLeft()								{ImportTable.pWindow->view_window_pan(ImportTable.pGlobal->pactive_awind,270);}
inline void		ViewPanRight()								{ImportTable.pWindow->view_window_pan(ImportTable.pGlobal->pactive_awind,90);}

// Config
inline void		ConfigGetInt(char *pszSection, char *pszKeyword, unsigned int *puValue) {ImportTable.pFS6->configuration_int_get(pszSection,pszKeyword,puValue);}

// Smoke
inline void    	CopyFromSmokeSystem_0(_smokePoint *dest)	{if(m_pSimData)memcpy((_smokePoint *)dest,(_smokePoint *)m_pSimData->pStaticData->pSmokePointList						,sizeof(_smokePoint));else dest=NULL;}
inline void    	CopyFromSmokeSystem_1(_smokePoint *dest)	{if(m_pSimData)memcpy((_smokePoint *)dest,(_smokePoint *)m_pSimData->pStaticData->pSmokePointList->pNext				,sizeof(_smokePoint));else dest=NULL;}
inline void    	CopyFromSmokeSystem_2(_smokePoint *dest)	{if(m_pSimData)memcpy((_smokePoint *)dest,(_smokePoint *)m_pSimData->pStaticData->pSmokePointList->pNext->pNext			,sizeof(_smokePoint));else dest=NULL;}
inline void    	CopyFromSmokeSystem_3(_smokePoint *dest)	{if(m_pSimData)memcpy((_smokePoint *)dest,(_smokePoint *)m_pSimData->pStaticData->pSmokePointList->pNext->pNext->pNext	,sizeof(_smokePoint));else dest=NULL;}
inline void    	CopyToSmokeSystem_0(_smokePoint *src)		{if(m_pSimData)memcpy((_smokePoint *)m_pSimData->pStaticData->pSmokePointList						,(_smokePoint *)src	,sizeof(_smokePoint));}
inline void    	CopyToSmokeSystem_1(_smokePoint *src)		{if(m_pSimData)memcpy((_smokePoint *)m_pSimData->pStaticData->pSmokePointList->pNext				,(_smokePoint *)src	,sizeof(_smokePoint));}
inline void    	CopyToSmokeSystem_2(_smokePoint *src)		{if(m_pSimData)memcpy((_smokePoint *)m_pSimData->pStaticData->pSmokePointList->pNext->pNext			,(_smokePoint *)src	,sizeof(_smokePoint));}
inline void    	CopyToSmokeSystem_3(_smokePoint *src)		{if(m_pSimData)memcpy((_smokePoint *)m_pSimData->pStaticData->pSmokePointList->pNext->pNext->pNext	,(_smokePoint *)src	,sizeof(_smokePoint));}

inline void    	CopyFromSmokeSystem_XYZ_0(XYZF64 *dest)	{if(m_pSimData)memcpy((XYZF64 *)&dest,(XYZF64 *)&m_pSimData->pStaticData->pSmokePointList->xyz						,sizeof(XYZF64));}
inline void    	CopyFromSmokeSystem_XYZ_1(XYZF64 *dest)	{if(m_pSimData)memcpy((XYZF64 *)&dest,(XYZF64 *)&m_pSimData->pStaticData->pSmokePointList->pNext->xyz				,sizeof(XYZF64));}
inline void    	CopyFromSmokeSystem_XYZ_2(XYZF64 *dest)	{if(m_pSimData)memcpy((XYZF64 *)&dest,(XYZF64 *)&m_pSimData->pStaticData->pSmokePointList->pNext->pNext->xyz		,sizeof(XYZF64));}
inline void    	CopyFromSmokeSystem_XYZ_3(XYZF64 *dest)	{if(m_pSimData)memcpy((XYZF64 *)&dest,(XYZF64 *)&m_pSimData->pStaticData->pSmokePointList->pNext->pNext->pNext->xyz	,sizeof(XYZF64));}
inline void    	CopyToSmokeSystem_XYZ_0(XYZF64 *src)	{if(m_pSimData)memcpy((XYZF64 *)&m_pSimData->pStaticData->pSmokePointList->xyz						,(XYZF64 *)&src	,sizeof(XYZF64));}
inline void    	CopyToSmokeSystem_XYZ_1(XYZF64 *src)	{if(m_pSimData)memcpy((XYZF64 *)&m_pSimData->pStaticData->pSmokePointList->pNext->xyz				,(XYZF64 *)&src	,sizeof(XYZF64));}
inline void    	CopyToSmokeSystem_XYZ_2(XYZF64 *src)	{if(m_pSimData)memcpy((XYZF64 *)&m_pSimData->pStaticData->pSmokePointList->pNext->pNext->xyz		,(XYZF64 *)&src	,sizeof(XYZF64));}
inline void    	CopyToSmokeSystem_XYZ_3(XYZF64 *src)	{if(m_pSimData)memcpy((XYZF64 *)&m_pSimData->pStaticData->pSmokePointList->pNext->pNext->pNext->xyz	,(XYZF64 *)&src	,sizeof(XYZF64));}
