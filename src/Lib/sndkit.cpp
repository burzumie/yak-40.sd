/**********************************************************
	(c) Igor Borozdin 2006

	Project beginned 2006-08-2

	$Author: Bor $ 
	$Date: 2006/08/13 14:50:05 $ 
	$Revision: 1.3 $

	$RCSfile: sndkit.cpp,v $ 

	Desc: Sound engine for MSFS
***********************************************************/

#define STRICT

#include <windows.h>
#include <iostream>
#include <mmsystem.h>
#include <assert.h>
#include "sndkit.h"
#include "../Common/CommonSys.h"

namespace sndkit
{

	//-----------------------------------------------------------------------------
	// Name: CWaveFile::CWaveFile()
	// Desc: Constructs the class.  Call Open() to open a wave file for reading.
	//       Then call Read() as needed.  Calling the destructor or Close()
	//       will close the file.
	//-----------------------------------------------------------------------------
	CWaveFile::CWaveFile()
	{
		m_pwfx    = NULL;
		m_hmmio   = NULL;
		m_pResourceBuffer = NULL;
		m_dwSize  = 0;
		m_bIsReadingFromMemory = FALSE;
		ZeroMemory(&m_Sample, sizeof(SMPL));
		m_pSampleLoops = NULL;
	}


	//-----------------------------------------------------------------------------
	// Name: CWaveFile::~CWaveFile()
	// Desc: Destructs the class
	//-----------------------------------------------------------------------------
	CWaveFile::~CWaveFile()
	{
		Close();

		if( !m_bIsReadingFromMemory )
			SAFE_DELETE_ARRAY( m_pwfx );
		
		SAFE_DELETE_ARRAY( m_pSampleLoops );
	}

	//-----------------------------------------------------------------------------
	// Name: CWaveFile::Open()
	// Desc: Opens a wave file for reading
	//-----------------------------------------------------------------------------
	HRESULT CWaveFile::Open( LPWSTR strFileName, WAVEFORMATEX* pwfx,HMODULE hModule)
	{
		HRESULT hr;

		m_bIsReadingFromMemory = FALSE;

		if( strFileName == NULL )
			return E_INVALIDARG;
		SAFE_DELETE_ARRAY( m_pwfx );

		m_hmmio = mmioOpenW( strFileName, NULL, MMIO_ALLOCBUF | MMIO_READ );

		if( NULL == m_hmmio )
		{
			HRSRC   hResInfo;
			HGLOBAL hResData;
			DWORD   dwSize;
			VOID*   pvRes;

			// Loading it as a file failed, so try it as a resource
			if( NULL == ( hResInfo = FindResourceW( hModule, strFileName, L"WAVE" ) ) )
			{
				if( NULL == ( hResInfo = FindResourceW( hModule, strFileName, L"WAV" ) ) ) {
					if( NULL == ( hResInfo = FindResourceW( hModule, strFileName, MAKEINTRESOURCEW(10) ) ) ) {
						return DXUT_ERR( L"FindResource", E_FAIL );
					}
				}
	        }

		    if( NULL == ( hResData = LoadResource( hModule, hResInfo ) ) )
			    return DXUT_ERR( L"LoadResource", E_FAIL );

			if( 0 == ( dwSize = SizeofResource( hModule, hResInfo ) ) )
				return DXUT_ERR( L"SizeofResource", E_FAIL );

			if( NULL == ( pvRes = LockResource( hResData ) ) )
				return DXUT_ERR( L"LockResource", E_FAIL );

			m_pResourceBuffer = new CHAR[ dwSize ];
			if( m_pResourceBuffer == NULL )
				return DXUT_ERR( L"new", E_OUTOFMEMORY );
			memcpy( m_pResourceBuffer, pvRes, dwSize );

			MMIOINFO mmioInfo;
			ZeroMemory( &mmioInfo, sizeof(mmioInfo) );
			mmioInfo.fccIOProc = FOURCC_MEM;
			mmioInfo.cchBuffer = dwSize;
			mmioInfo.pchBuffer = (CHAR*) m_pResourceBuffer;

			m_hmmio = mmioOpen( NULL, &mmioInfo, MMIO_ALLOCBUF | MMIO_READ );
		}

		if( FAILED( hr = ReadMMIO() ) )
		{
			// ReadMMIO will fail if its an not a wave file
			mmioClose( m_hmmio, 0 );
			return DXUT_ERR( L"ReadMMIO", hr );
		}
			
		if( FAILED( hr = ResetFile() ) )
			return DXUT_ERR( L"ResetFile", hr );
		
		// After the reset, the size of the wav file is m_ck.cksize so store it now
		m_dwSize = m_ck.cksize;

		return hr;
	}


	HRESULT CWaveFile::OpenFromMemory( BYTE* pbData, ULONG ulDataSize,
                                   WAVEFORMATEX* pwfx)
	{
		m_pwfx       = pwfx;
		m_ulDataSize = ulDataSize;
	    m_pbData     = pbData;
		m_pbDataCur  = m_pbData;
		m_bIsReadingFromMemory = TRUE;

		return S_OK;
	}


	HRESULT CWaveFile::ReadMMIO()
	{
		MMCKINFO        ckIn;           // chunk info. for general use.
		PCMWAVEFORMAT   pcmWaveFormat;  // Temp PCM structure to load in.

		m_pwfx = NULL;

		if( ( 0 != mmioDescend( m_hmmio, &m_ckRiff, NULL, 0 ) ) )
			return DXUT_ERR( L"mmioDescend", E_FAIL );

		// Check to make sure this is a valid wave file
		if( (m_ckRiff.ckid != FOURCC_RIFF) ||
			(m_ckRiff.fccType != mmioFOURCC('W', 'A', 'V', 'E') ) )
			return DXUT_ERR( L"mmioFOURCC", E_FAIL );

		// Search the input file for for the 'fmt ' chunk.
		ckIn.ckid = mmioFOURCC('f', 'm', 't', ' ');
		if( 0 != mmioDescend( m_hmmio, &ckIn, &m_ckRiff, MMIO_FINDCHUNK ) )
			return DXUT_ERR( L"mmioDescend", E_FAIL );

	    // Expect the 'fmt' chunk to be at least as large as <PCMWAVEFORMAT>;
		// if there are extra parameters at the end, we'll ignore them
		if( ckIn.cksize < (LONG) sizeof(PCMWAVEFORMAT) )
			return DXUT_ERR( L"sizeof(PCMWAVEFORMAT)", E_FAIL );

		// Read the 'fmt ' chunk into <pcmWaveFormat>.
		if( mmioRead( m_hmmio, (HPSTR) &pcmWaveFormat,
                  sizeof(pcmWaveFormat)) != sizeof(pcmWaveFormat) )
			return DXUT_ERR( L"mmioRead", E_FAIL );

		// Allocate the waveformatex, but if its not pcm format, read the next
		// word, and thats how many extra bytes to allocate.
		if( pcmWaveFormat.wf.wFormatTag == WAVE_FORMAT_PCM )
		{
			m_pwfx = (WAVEFORMATEX*)new CHAR[ sizeof(WAVEFORMATEX) ];
			if( NULL == m_pwfx )
				return DXUT_ERR( L"m_pwfx", E_FAIL );

			// Copy the bytes from the pcm structure to the waveformatex structure
			memcpy( m_pwfx, &pcmWaveFormat, sizeof(pcmWaveFormat) );
			m_pwfx->cbSize = 0;
		}
		else
		{
			// Read in length of extra bytes.
			WORD cbExtraBytes = 0L;
			if( mmioRead( m_hmmio, (CHAR*)&cbExtraBytes, sizeof(WORD)) != sizeof(WORD) )
	            return DXUT_ERR( L"mmioRead", E_FAIL );

		    m_pwfx = (WAVEFORMATEX*)new CHAR[ sizeof(WAVEFORMATEX) + cbExtraBytes ];
			if( NULL == m_pwfx )
				return DXUT_ERR( L"new", E_FAIL );

			// Copy the bytes from the pcm structure to the waveformatex structure
			memcpy( m_pwfx, &pcmWaveFormat, sizeof(pcmWaveFormat) );
			m_pwfx->cbSize = cbExtraBytes;

			// Now, read those extra bytes into the structure, if cbExtraAlloc != 0.
			if( mmioRead( m_hmmio, (CHAR*)(((BYTE*)&(m_pwfx->cbSize))+sizeof(WORD)),
				          cbExtraBytes ) != cbExtraBytes )
			{
				SAFE_DELETE( m_pwfx );
				return DXUT_ERR( L"mmioRead", E_FAIL );
			}
		}

		// Ascend the input file out of the 'fmt ' chunk.
		if( 0 != mmioAscend( m_hmmio, &ckIn, 0 ) )
		{
			SAFE_DELETE( m_pwfx );
			return DXUT_ERR( L"mmioAscend", E_FAIL );
		}


		ckIn.ckid = mmioFOURCC('s', 'm', 'p', 'l');
		if( MMSYSERR_NOERROR  == mmioDescend( m_hmmio, &ckIn, &m_ckRiff, MMIO_FINDCHUNK ) )
		{
			if( mmioRead(m_hmmio, (CHAR*)&m_Sample, sizeof(SMPL)) == sizeof(SMPL) )
			{
				m_pSampleLoops = ::new SAMPLELOOP[m_Sample.dwNumSampleLoops];
				mmioRead(m_hmmio, (CHAR*)m_pSampleLoops, sizeof(SAMPLELOOP)*m_Sample.dwNumSampleLoops) ;
			};
		};

		return S_OK;
	}


//-----------------------------------------------------------------------------
// Name: CWaveFile::GetSize()
// Desc: Retuns the size of the read access wave file
//-----------------------------------------------------------------------------
DWORD CWaveFile::GetSize()
{
    return m_dwSize;
}


//-----------------------------------------------------------------------------
// Name: CWaveFile::ResetFile()
// Desc: Resets the internal m_ck pointer so reading starts from the
//       beginning of the file again
//-----------------------------------------------------------------------------
HRESULT CWaveFile::ResetFile()
{
    if( m_bIsReadingFromMemory )
    {
        m_pbDataCur = m_pbData;
    }
    else
    {
        if( m_hmmio == NULL )
            return CO_E_NOTINITIALIZED;

		// Seek to the data
		if( -1 == mmioSeek( m_hmmio, m_ckRiff.dwDataOffset + sizeof(FOURCC),
			SEEK_SET ) )
			return DXUT_ERR( L"mmioSeek", E_FAIL );
		// Search the input file for the 'data' chunk.
		m_ck.ckid = mmioFOURCC('d', 'a', 't', 'a');
		if( 0 != mmioDescend( m_hmmio, &m_ck, &m_ckRiff, MMIO_FINDCHUNK ) )
			return DXUT_ERR( L"mmioDescend", E_FAIL );
    }

    return S_OK;
}


//-----------------------------------------------------------------------------
// Name: CWaveFile::Read()
// Desc: Reads section of data from a wave file into pBuffer and returns
//       how much read in pdwSizeRead, reading not more than dwSizeToRead.
//       This uses m_ck to determine where to start reading from.  So
//       subsequent calls will be continue where the last left off unless
//       Reset() is called.
//-----------------------------------------------------------------------------
HRESULT CWaveFile::Read( BYTE* pBuffer, DWORD dwSizeToRead, DWORD* pdwSizeRead )
{
    if( m_bIsReadingFromMemory )
    {
        if( m_pbDataCur == NULL )
            return CO_E_NOTINITIALIZED;
        if( pdwSizeRead != NULL )
            *pdwSizeRead = 0;

        if( (BYTE*)(m_pbDataCur + dwSizeToRead) >
            (BYTE*)(m_pbData + m_ulDataSize) )
        {
            dwSizeToRead = m_ulDataSize - (DWORD)(m_pbDataCur - m_pbData);
        }

        CopyMemory( pBuffer, m_pbDataCur, dwSizeToRead );

        if( pdwSizeRead != NULL )
            *pdwSizeRead = dwSizeToRead;

        return S_OK;
    }
    else
    {
        MMIOINFO mmioinfoIn; // current status of m_hmmio

        if( m_hmmio == NULL )
            return CO_E_NOTINITIALIZED;
        if( pBuffer == NULL || pdwSizeRead == NULL )
            return E_INVALIDARG;

        if( pdwSizeRead != NULL )
            *pdwSizeRead = 0;

        if( 0 != mmioGetInfo( m_hmmio, &mmioinfoIn, 0 ) )
            return DXUT_ERR( L"mmioGetInfo", E_FAIL );

        UINT cbDataIn = dwSizeToRead;
        if( cbDataIn > m_ck.cksize )
            cbDataIn = m_ck.cksize;

        m_ck.cksize -= cbDataIn;

        for( DWORD cT = 0; cT < cbDataIn; cT++ )
        {
            // Copy the bytes from the io to the buffer.
            if( mmioinfoIn.pchNext == mmioinfoIn.pchEndRead )
            {
                if( 0 != mmioAdvance( m_hmmio, &mmioinfoIn, MMIO_READ ) )
                    return DXUT_ERR( L"mmioAdvance", E_FAIL );

                if( mmioinfoIn.pchNext == mmioinfoIn.pchEndRead )
                    return DXUT_ERR( L"mmioinfoIn.pchNext", E_FAIL );
            }

            // Actual copy.
            *((BYTE*)pBuffer+cT) = *((BYTE*)mmioinfoIn.pchNext);
            mmioinfoIn.pchNext++;
        }

        if( 0 != mmioSetInfo( m_hmmio, &mmioinfoIn, 0 ) )
            return DXUT_ERR( L"mmioSetInfo", E_FAIL );

        if( pdwSizeRead != NULL )
            *pdwSizeRead = cbDataIn;

        return S_OK;
    }
}


//-----------------------------------------------------------------------------
// Name: CWaveFile::Close()
// Desc: Closes the wave file
//-----------------------------------------------------------------------------
HRESULT CWaveFile::Close()
{
	mmioClose( m_hmmio, 0 );
	m_hmmio = NULL;
	SAFE_DELETE_ARRAY( m_pResourceBuffer );

    return S_OK;
}

//-----------------------------------------------------------------------------
// Name: CSound::CSound()
// Desc: Constructs the class
//-----------------------------------------------------------------------------
CSound::CSound( LPDIRECTSOUNDBUFFER* apDSBuffer, DWORD dwDSBufferSize,
                DWORD dwNumBuffers, CWaveFile* pWaveFile, DWORD dwCreationFlags )
{
	m_lVolume = 0;
	m_hLoopEvent = NULL;
	m_pDSNotify = NULL;
	m_bMute = FALSE;
	
    if( dwNumBuffers <= 0 )
        return;

    m_apDSBuffer = new LPDIRECTSOUNDBUFFER[dwNumBuffers];

    if( NULL != m_apDSBuffer )
    {
        for( DWORD i=0; i<dwNumBuffers; i++ )
            m_apDSBuffer[i] = apDSBuffer[i];

        m_dwDSBufferSize = dwDSBufferSize;
        m_dwNumBuffers   = dwNumBuffers;
        m_pWaveFile      = pWaveFile;
        m_dwCreationFlags= dwCreationFlags;

        FillBufferWithSound( m_apDSBuffer[0], FALSE );
    }

	if( m_hLoopEvent )
	{
		m_PosNotify.dwOffset = m_dwStopPosition;
		m_PosNotify.hEventNotify = m_hLoopEvent;
	};
}


//-----------------------------------------------------------------------------
// Name: CSound::~CSound()
// Desc: Destroys the class
//-----------------------------------------------------------------------------
CSound::~CSound()
{
    for( DWORD i=0; i<m_dwNumBuffers; i++ )
    {
        SAFE_RELEASE( m_apDSBuffer[i] );
    }

    SAFE_DELETE_ARRAY( m_apDSBuffer );
    SAFE_DELETE( m_pWaveFile );
	
	if( m_hLoopEvent )
		::CloseHandle( m_hLoopEvent );

	SAFE_RELEASE( m_pDSNotify );
}


//-----------------------------------------------------------------------------
// Name: CSound::FillBufferWithSound()
// Desc: Fills a DirectSound buffer with a sound file
//-----------------------------------------------------------------------------
HRESULT CSound::FillBufferWithSound( LPDIRECTSOUNDBUFFER pDSB, BOOL bRepeatWavIfBufferLarger )
{
    HRESULT hr;
    VOID*   pDSLockedBuffer      = NULL; // Pointer to locked buffer memory
    DWORD   dwDSLockedBufferSize = 0;    // Size of the locked DirectSound buffer
    DWORD   dwWavDataRead        = 0;    // Amount of data read from the wav file

    if( pDSB == NULL )
        return CO_E_NOTINITIALIZED;

	// Create the notification events, so that we know when to fill
    // the buffer as the sound plays.
	if( m_pWaveFile->m_pSampleLoops )
	{
		if( FAILED( hr = pDSB->QueryInterface( IID_IDirectSoundNotify,
                                                (VOID**)&m_pDSNotify ) ) )
		{
			return DXUT_ERR( L"QueryInterface", hr );
		};
		m_hLoopEvent = ::CreateEvent(NULL, true, false, NULL);
		m_dwStartPosition = m_pWaveFile->m_pSampleLoops[0].dwStart*
			m_pWaveFile->m_pwfx->nChannels*m_pWaveFile->m_pwfx->wBitsPerSample/8;
		m_dwStopPosition = m_pWaveFile->m_pSampleLoops[0].dwEnd*
			m_pWaveFile->m_pwfx->nChannels*m_pWaveFile->m_pwfx->wBitsPerSample/8;
	};


    // Make sure we have focus, and we didn't just switch in from
    // an app which had a DirectSound device
    if( FAILED( hr = RestoreBuffer( pDSB, NULL ) ) )
        return DXUT_ERR( L"RestoreBuffer", hr );

    // Lock the buffer down
    if( FAILED( hr = pDSB->Lock( 0, m_dwDSBufferSize,
                                 &pDSLockedBuffer, &dwDSLockedBufferSize,
                                 NULL, NULL, 0L ) ) )
        return DXUT_ERR( L"Lock", hr );

    // Reset the wave file to the beginning
    m_pWaveFile->ResetFile();

    if( FAILED( hr = m_pWaveFile->Read( (BYTE*) pDSLockedBuffer,
                                        dwDSLockedBufferSize,
                                        &dwWavDataRead ) ) )
        return DXUT_ERR( L"Read", hr );

    if( dwWavDataRead == 0 )
    {
        // Wav is blank, so just fill with silence
        FillMemory( (BYTE*) pDSLockedBuffer,
                    dwDSLockedBufferSize,
                    (BYTE)(m_pWaveFile->m_pwfx->wBitsPerSample == 8 ? 128 : 0 ) );
    }
    else if( dwWavDataRead < dwDSLockedBufferSize )
    {
        // If the wav file was smaller than the DirectSound buffer,
        // we need to fill the remainder of the buffer with data
        if( bRepeatWavIfBufferLarger )
        {
            // Reset the file and fill the buffer with wav data
            DWORD dwReadSoFar = dwWavDataRead;    // From previous call above.
            while( dwReadSoFar < dwDSLockedBufferSize )
            {
                // This will keep reading in until the buffer is full
                // for very short files
                if( FAILED( hr = m_pWaveFile->ResetFile() ) )
                    return DXUT_ERR( L"ResetFile", hr );

                hr = m_pWaveFile->Read( (BYTE*)pDSLockedBuffer + dwReadSoFar,
                                        dwDSLockedBufferSize - dwReadSoFar,
                                        &dwWavDataRead );
                if( FAILED(hr) )
                    return DXUT_ERR( L"Read", hr );

                dwReadSoFar += dwWavDataRead;
            }
        }
        else
        {
            // Don't repeat the wav file, just fill in silence
            FillMemory( (BYTE*) pDSLockedBuffer + dwWavDataRead,
                        dwDSLockedBufferSize - dwWavDataRead,
                        (BYTE)(m_pWaveFile->m_pwfx->wBitsPerSample == 8 ? 128 : 0 ) );
        }
    }

    // Unlock the buffer, we don't need it anymore.
    pDSB->Unlock( pDSLockedBuffer, dwDSLockedBufferSize, NULL, 0 );

    return S_OK;
}


//-----------------------------------------------------------------------------
// Name: CSound::RestoreBuffer()
// Desc: Restores the lost buffer. *pbWasRestored returns TRUE if the buffer was
//       restored.  It can also NULL if the information is not needed.
//-----------------------------------------------------------------------------
HRESULT CSound::RestoreBuffer( LPDIRECTSOUNDBUFFER pDSB, BOOL* pbWasRestored )
{
    HRESULT hr;

    if( pDSB == NULL )
        return CO_E_NOTINITIALIZED;
    if( pbWasRestored )
        *pbWasRestored = FALSE;

    DWORD dwStatus;
    if( FAILED( hr = pDSB->GetStatus( &dwStatus ) ) )
        return DXUT_ERR( L"GetStatus", hr );

    if( dwStatus & DSBSTATUS_BUFFERLOST )
    {
        // Since the app could have just been activated, then
        // DirectSound may not be giving us control yet, so
        // the restoring the buffer may fail.
        // If it does, sleep until DirectSound gives us control.
        do
        {
            hr = pDSB->Restore();
            if( hr == DSERR_BUFFERLOST )
                Sleep( 10 );
        }
        while( ( hr = pDSB->Restore() ) == DSERR_BUFFERLOST );

        if( pbWasRestored != NULL )
            *pbWasRestored = TRUE;

        return S_OK;
    }
    else
    {
        return S_FALSE;
    }
}


//-----------------------------------------------------------------------------
// Name: CSound::GetFreeBuffer()
// Desc: Finding the first buffer that is not playing and return a pointer to
//       it, or if all are playing return a pointer to a randomly selected buffer.
//-----------------------------------------------------------------------------
LPDIRECTSOUNDBUFFER CSound::GetFreeBuffer()
{
    if( m_apDSBuffer == NULL )
        return FALSE;

	DWORD i;
    for( i=0; i<m_dwNumBuffers; i++ )
    {
        if( m_apDSBuffer[i] )
        {
            DWORD dwStatus = 0;
            m_apDSBuffer[i]->GetStatus( &dwStatus );
            if ( ( dwStatus & DSBSTATUS_PLAYING ) == 0 )
                break;
        }
    }

    if( i != m_dwNumBuffers )
        return m_apDSBuffer[ i ];
    else
        return m_apDSBuffer[ rand() % m_dwNumBuffers ];
}


//-----------------------------------------------------------------------------
// Name: CSound::GetBuffer()
// Desc:
//-----------------------------------------------------------------------------
LPDIRECTSOUNDBUFFER CSound::GetBuffer( DWORD dwIndex )
{
    if( m_apDSBuffer == NULL )
        return NULL;
    if( dwIndex >= m_dwNumBuffers )
        return NULL;

    return m_apDSBuffer[dwIndex];
}


//-----------------------------------------------------------------------------
// Name: CSound::Play()
// Desc: Plays the sound using voice management flags.  Pass in DSBPLAY_LOOPING
//       in the dwFlags to loop the sound
//-----------------------------------------------------------------------------
HRESULT CSound::Play( DWORD dwFlags, DWORD dwPriority, LONG lVolume, LONG lFrequency, LONG lPan )
{
    HRESULT hr;
    BOOL    bRestored;

    if( m_apDSBuffer == NULL )
        return CO_E_NOTINITIALIZED;

    LPDIRECTSOUNDBUFFER pDSB;
	if(m_hLoopEvent )
		pDSB = m_apDSBuffer[0];
	else
		pDSB = GetFreeBuffer();

    if( pDSB == NULL )
        return DXUT_ERR( L"GetFreeBuffer", E_FAIL );

    // Restore the buffer if it was lost
    if( FAILED( hr = RestoreBuffer( pDSB, &bRestored ) ) )
        return DXUT_ERR( L"RestoreBuffer", hr );

    if( bRestored )
    {
        // The buffer was restored, so we need to fill it with new data
        if( FAILED( hr = FillBufferWithSound( pDSB, FALSE ) ) )
            return DXUT_ERR( L"FillBufferWithSound", hr );
    }

    if( m_dwCreationFlags & DSBCAPS_CTRLVOLUME )
    {
		if( lVolume )
			SetVolume(lVolume);
    }

    if( lFrequency != -1 &&
        (m_dwCreationFlags & DSBCAPS_CTRLFREQUENCY) )
    {
        pDSB->SetFrequency( lFrequency );
    }

    if( m_dwCreationFlags & DSBCAPS_CTRLPAN )
    {
        pDSB->SetPan( lPan );
    }

	if( m_hLoopEvent )
	{
		if( !IsSoundPlaying() )
		{
			m_pDSNotify->SetNotificationPositions(1, &m_PosNotify);
			return pDSB->Play( 0, dwPriority, dwFlags );
		}
		else
		{
			DWORD dwCurrentPlayCursor;
			m_apDSBuffer[0]->GetCurrentPosition(&dwCurrentPlayCursor, NULL);
			if( dwCurrentPlayCursor > m_dwStopPosition )
			{
				m_apDSBuffer[0]->Stop();
				m_pDSNotify->SetNotificationPositions(1, &m_PosNotify);
				DWORD dwActualPos = m_dwDSBufferSize - dwCurrentPlayCursor;
				if( dwActualPos > m_dwStartPosition )
					dwActualPos = m_dwStartPosition;
				m_apDSBuffer[0]->SetCurrentPosition( dwActualPos  );
				return pDSB->Play( 0, dwPriority, dwFlags );
			};
			return S_OK;
		};
		return S_OK;
	}
	else
	{
		pDSB->SetCurrentPosition(0);
		return pDSB->Play( 0, dwPriority, dwFlags );
	}
}



//-----------------------------------------------------------------------------
// Name: CSound::Stop()
// Desc: Stops the sound from playing
//-----------------------------------------------------------------------------
HRESULT CSound::Stop()
{
    if( m_apDSBuffer == NULL )
        return CO_E_NOTINITIALIZED;

    HRESULT hr = 0;
	
	if( m_hLoopEvent )
	{
		DWORD dwStatus;
		m_apDSBuffer[0]->GetStatus( &dwStatus );
		if( ( dwStatus & DSBSTATUS_PLAYING ) != 0 )
		{
			m_apDSBuffer[0]->Stop();
			DWORD dwCurrentPlayCursor;
			m_apDSBuffer[0]->GetCurrentPosition(&dwCurrentPlayCursor, NULL);
			m_pDSNotify->SetNotificationPositions(0, NULL);
			if( dwCurrentPlayCursor < m_dwStartPosition )
			{
				DWORD dwActualPos = m_dwDSBufferSize - dwCurrentPlayCursor;
				// pass two samples that safely exclude notificatiob
				if( dwActualPos < m_dwStopPosition )
					dwActualPos = m_dwStopPosition + 
					2*m_pWaveFile->m_pwfx->nChannels*m_pWaveFile->m_pwfx->wBitsPerSample/8;
				m_apDSBuffer[0]->SetCurrentPosition( dwActualPos );
			}
			else
			if( dwCurrentPlayCursor < m_dwStopPosition)
			{
				m_apDSBuffer[0]->SetCurrentPosition(m_dwStopPosition);
			};
			m_apDSBuffer[0]->Play(0,0,0);
		};
	}
	else
	{
		for( DWORD i=0; i<m_dwNumBuffers; i++ )
			hr |= m_apDSBuffer[i]->Stop();
	};

    return hr;
}


//-----------------------------------------------------------------------------
// Name: CSound::Reset()
// Desc: Reset all of the sound buffers
//-----------------------------------------------------------------------------
HRESULT CSound::Reset()
{
    if( m_apDSBuffer == NULL )
        return CO_E_NOTINITIALIZED;

    HRESULT hr = 0;

    for( DWORD i=0; i<m_dwNumBuffers; i++ )
        hr |= m_apDSBuffer[i]->SetCurrentPosition( 0 );

    return hr;
}


//-----------------------------------------------------------------------------
// Name: CSound::IsSoundPlaying()
// Desc: Checks to see if a buffer is playing and returns TRUE if it is.
//-----------------------------------------------------------------------------
BOOL CSound::IsSoundPlaying()
{
    BOOL bIsPlaying = FALSE;

    if( m_apDSBuffer == NULL )
        return FALSE;

    for( DWORD i=0; i<m_dwNumBuffers; i++ )
    {
        if( m_apDSBuffer[i] )
        {
            DWORD dwStatus = 0;
            m_apDSBuffer[i]->GetStatus( &dwStatus );
            bIsPlaying |= ( ( dwStatus & DSBSTATUS_PLAYING ) != 0 );
        }
    }

    return bIsPlaying;
}

//-----------------------------------------------------------------------------
// Name: CSound::SetVolume()
// Desc: 
//-----------------------------------------------------------------------------
HRESULT CSound::SetVolume( LONG lVolume )
{
	if( m_lVolume != lVolume )
	{
		m_lVolume = lVolume;
		if( !m_bMute )
			return m_apDSBuffer[0]->SetVolume( m_lVolume );
	};
	return S_FALSE;
};

//-----------------------------------------------------------------------------
// Name: CSound::Mute()
// Desc: 
//-----------------------------------------------------------------------------
HRESULT CSound::Mute( LONG lVolume )
{
	if( !m_bMute )
	{
		m_bMute = TRUE;
		return m_apDSBuffer[0]->SetVolume( lVolume  );
	};
	return S_FALSE;
};
		
//-----------------------------------------------------------------------------
// Name: CSound::RestoreVolume()
// Desc: 
//-----------------------------------------------------------------------------
HRESULT CSound::RestoreVolume( void )
{
	if( m_bMute )
	{
		m_bMute = FALSE;
		return m_apDSBuffer[0]->SetVolume( m_lVolume );
	};
	return S_FALSE;
};


//-----------------------------------------------------------------------------
// Name: CSound::Pause()
// Desc: 
//-----------------------------------------------------------------------------
HRESULT	CSound::Pause(void)
{
	return m_apDSBuffer[0]->Stop();
};


//-----------------------------------------------------------------------------
// Name: CSound::Replay()
// Desc: 
//-----------------------------------------------------------------------------
HRESULT CSound::Replay(void)
{
	return m_apDSBuffer[0]->Play(0,0,0);
};


//-----------------------------------------------------------------------------
// Name: CSoundManager::CSoundManager()
// Desc: Constructs the class
//-----------------------------------------------------------------------------
CSoundManager::CSoundManager()
{
	m_nSoundsCount = 0;
	s_nEventsCount = 0;
	s_bIsTerminating = FALSE;
    m_pDS = NULL;

	for(DWORD i=0; i< MAX_SND_SOCKETS; i++ ) {
		m_Sounds[i]=NULL;
	}
}


//-----------------------------------------------------------------------------
// Name: CSoundManager::~CSoundManager()
// Desc: Destroys the class
//-----------------------------------------------------------------------------
CSoundManager::~CSoundManager()
{
	s_bIsTerminating = TRUE;
	::SetEvent(s_hEvents[0]);
	::TerminateThread(m_hThread, 0);

	for(DWORD i=0; i< m_nSoundsCount; i++ ) {
		SAFE_DELETE( m_Sounds[i] );
	}

	SAFE_RELEASE( m_pDS );
}


//-----------------------------------------------------------------------------
// Name: CSoundManager::Initialize()
// Desc: Initializes the IDirectSound object and also sets the primary buffer
//       format.  This function must be called before any others.
//-----------------------------------------------------------------------------
HRESULT CSoundManager::Initialize( HINSTANCE hdll,
                                   DWORD dwCoopLevel )
{
    HRESULT             hr;

	m_HWND=(HINSTANCE)hdll;
	m_HDLL=(HMODULE)hdll;
	m_WND=GetActiveWindow();

    SAFE_RELEASE( m_pDS );

    // Create IDirectSound using the primary sound device
    if( FAILED( hr = DirectSoundCreate8( NULL, &m_pDS, NULL ) ) )
        return DXUT_ERR( L"DirectSoundCreate8", hr );

    // Set DirectSound coop level
    if( FAILED( hr = m_pDS->SetCooperativeLevel( m_WND, dwCoopLevel ) ) )
        return DXUT_ERR( L"SetCooperativeLevel", hr );

	// ���������� �������
	s_hEvents[s_nEventsCount] = ::CreateEvent(NULL, true, false, NULL);
	s_LoopSounds[s_nEventsCount] = NULL;
	s_nEventsCount++;

	m_hThread = ::CreateThread(NULL, 0, SoundEventHandler, this, 0, &m_dwThreadID);

    return S_OK;
}


//-----------------------------------------------------------------------------
// Name: CSoundManager::SetPrimaryBufferFormat()
// Desc: Set primary buffer to a specified format
//       !WARNING! - Setting the primary buffer format and then using this
//                   same DirectSound object for DirectMusic messes up
//                   DirectMusic!
//       For example, to set the primary buffer format to 22kHz stereo, 16-bit
//       then:   dwPrimaryChannels = 2
//               dwPrimaryFreq     = 22050,
//               dwPrimaryBitRate  = 16
//-----------------------------------------------------------------------------
HRESULT CSoundManager::SetPrimaryBufferFormat( DWORD dwPrimaryChannels,
                                               DWORD dwPrimaryFreq,
                                               DWORD dwPrimaryBitRate )
{
    HRESULT             hr;
    LPDIRECTSOUNDBUFFER pDSBPrimary = NULL;

    if( m_pDS == NULL )
        return CO_E_NOTINITIALIZED;

    // Get the primary buffer
    DSBUFFERDESC dsbd;
    ZeroMemory( &dsbd, sizeof(DSBUFFERDESC) );
    dsbd.dwSize        = sizeof(DSBUFFERDESC);
    dsbd.dwFlags       = DSBCAPS_PRIMARYBUFFER;
    dsbd.dwBufferBytes = 0;
    dsbd.lpwfxFormat   = NULL;

    if( FAILED( hr = m_pDS->CreateSoundBuffer( &dsbd, &pDSBPrimary, NULL ) ) )
        return DXUT_ERR( L"CreateSoundBuffer", hr );

    WAVEFORMATEX wfx;
    ZeroMemory( &wfx, sizeof(WAVEFORMATEX) );
    wfx.wFormatTag      = (WORD) WAVE_FORMAT_PCM;
    wfx.nChannels       = (WORD) dwPrimaryChannels;
    wfx.nSamplesPerSec  = (DWORD) dwPrimaryFreq;
    wfx.wBitsPerSample  = (WORD) dwPrimaryBitRate;
    wfx.nBlockAlign     = (WORD) (wfx.wBitsPerSample / 8 * wfx.nChannels);
    wfx.nAvgBytesPerSec = (DWORD) (wfx.nSamplesPerSec * wfx.nBlockAlign);

    if( FAILED( hr = pDSBPrimary->SetFormat(&wfx) ) )
        return DXUT_ERR( L"SetFormat", hr );

    SAFE_RELEASE( pDSBPrimary );

    return S_OK;
}


//-----------------------------------------------------------------------------
// Name: CSoundManager::AddAndNotify()
// Desc: �������� ���� � ������ � ��������� ����������
//-----------------------------------------------------------------------------
BOOL CSoundManager::AddAndNotify(CSound* pSound)
{
	m_Sounds[m_nSoundsCount++] = pSound;
	if( pSound->m_hLoopEvent )
	{
		s_LoopSounds[s_nEventsCount] = pSound;
		s_hEvents[s_nEventsCount] = pSound->m_hLoopEvent;
		s_nEventsCount++;
		// �������� ���������� �������� ������� � ������������� ������ � ��������
		// � ����� ����������� �������
		::SetEvent(s_hEvents[0]);
	};

	return TRUE;
};


//-----------------------------------------------------------------------------
// Name: CSoundManager::Create()
// Desc:
//-----------------------------------------------------------------------------
HRESULT CSoundManager::Create( CSound** ppSound,
                               LPWSTR strWaveFileName,
                               DWORD dwCreationFlags,
                               GUID guid3DAlgorithm,
                               DWORD dwNumBuffers )
{
    DWORD   i;
    HRESULT hr;
    HRESULT hrRet = S_OK;

    LPDIRECTSOUNDBUFFER* apDSBuffer     = NULL;
    DWORD                dwDSBufferSize = NULL;
    CWaveFile*           pWaveFile      = NULL;


    if( m_pDS == NULL )
        return CO_E_NOTINITIALIZED;
    if( strWaveFileName == NULL || ppSound == NULL || dwNumBuffers < 1 )
        return E_INVALIDARG;

    apDSBuffer = new LPDIRECTSOUNDBUFFER[dwNumBuffers];
    if( apDSBuffer == NULL )
    {
        hr = E_OUTOFMEMORY;
        goto LFail;
    }

    pWaveFile = new CWaveFile();
    if( pWaveFile == NULL )
    {
        hr = E_OUTOFMEMORY;
        goto LFail;
    }

    pWaveFile->Open( strWaveFileName, NULL, (HMODULE)m_HDLL);

    if( pWaveFile->GetSize() == 0 )
    {
        // Wave is blank, so don't create it.
        hr = E_FAIL;
        goto LFail;
    }

	if( pWaveFile->m_pwfx->wFormatTag != WAVE_FORMAT_PCM )
	{
		// Wave isn't PCM, so don't create it.
        hr = E_FAIL;
        goto LFail;
	};

    // Make the DirectSound buffer the same size as the wav file
    dwDSBufferSize = pWaveFile->GetSize();

    // Create the direct sound buffer, and only request the flags needed
    // since each requires some overhead and limits if the buffer can
    // be hardware accelerated
    DSBUFFERDESC dsbd;
    ZeroMemory( &dsbd, sizeof(DSBUFFERDESC) );
    dsbd.dwSize          = sizeof(DSBUFFERDESC);
	if( pWaveFile->m_pSampleLoops )
		dsbd.dwFlags         = dwCreationFlags
								|DSBCAPS_GETCURRENTPOSITION2
								|DSBCAPS_CTRLPOSITIONNOTIFY
								|DSBCAPS_LOCSOFTWARE;
	else
		dsbd.dwFlags         = dwCreationFlags ;
    dsbd.dwBufferBytes   = dwDSBufferSize;
    dsbd.guid3DAlgorithm = guid3DAlgorithm;
    dsbd.lpwfxFormat     = pWaveFile->m_pwfx;

    // DirectSound is only guarenteed to play PCM data.  Other
    // formats may or may not work depending the sound card driver.
    hr = m_pDS->CreateSoundBuffer( &dsbd, &apDSBuffer[0], NULL );

    // Be sure to return this error code if it occurs so the
    // callers knows this happened.
    if( hr == DS_NO_VIRTUALIZATION )
        hrRet = DS_NO_VIRTUALIZATION;

    if( FAILED(hr) )
    {
        // DSERR_BUFFERTOOSMALL will be returned if the buffer is
        // less than DSBSIZE_FX_MIN and the buffer is created
        // with DSBCAPS_CTRLFX.

        // It might also fail if hardware buffer mixing was requested
        // on a device that doesn't support it.
        DXUT_ERR( L"CreateSoundBuffer", hr );

        goto LFail;
    }

    // Default to use DuplicateSoundBuffer() when created extra buffers since always
    // create a buffer that uses the same memory however DuplicateSoundBuffer() will fail if
    // DSBCAPS_CTRLFX is used, so use CreateSoundBuffer() instead in this case.
    if( (dwCreationFlags & DSBCAPS_CTRLFX) == 0 )
    {
        for( i=1; i<dwNumBuffers; i++ )
        {
            if( FAILED( hr = m_pDS->DuplicateSoundBuffer( apDSBuffer[0], &apDSBuffer[i] ) ) )
            {
                DXUT_ERR( L"DuplicateSoundBuffer", hr );
                goto LFail;
            }
        }
    }
    else
    {
        for( i=1; i<dwNumBuffers; i++ )
        {
            hr = m_pDS->CreateSoundBuffer( &dsbd, &apDSBuffer[i], NULL );
            if( FAILED(hr) )
            {
                DXUT_ERR( L"CreateSoundBuffer", hr );
                goto LFail;
            }
        }
   }


    // Create the sound
    *ppSound = new CSound( apDSBuffer, dwDSBufferSize, dwNumBuffers, pWaveFile, dwCreationFlags );

	AddAndNotify( *ppSound );

    SAFE_DELETE_ARRAY( apDSBuffer );
    return hrRet;

LFail:
    // Cleanup
    SAFE_DELETE( pWaveFile );
    SAFE_DELETE_ARRAY( apDSBuffer );
    return hr;
}


//-----------------------------------------------------------------------------
// Name: CSoundManager::CreateFromMemory()
// Desc:
//-----------------------------------------------------------------------------
HRESULT CSoundManager::CreateFromMemory( CSound** ppSound,
                                        BYTE* pbData,
                                        ULONG  ulDataSize,
                                        LPWAVEFORMATEX pwfx,
                                        DWORD dwCreationFlags,
                                        GUID guid3DAlgorithm,
                                        DWORD dwNumBuffers )
{
    HRESULT hr;
    DWORD   i;
    LPDIRECTSOUNDBUFFER* apDSBuffer     = NULL;
    DWORD                dwDSBufferSize = NULL;
    CWaveFile*           pWaveFile      = NULL;

    if( m_pDS == NULL )
        return CO_E_NOTINITIALIZED;
    if( pbData == NULL || ppSound == NULL || dwNumBuffers < 1 )
        return E_INVALIDARG;

    apDSBuffer = new LPDIRECTSOUNDBUFFER[dwNumBuffers];
    if( apDSBuffer == NULL )
    {
        hr = E_OUTOFMEMORY;
        goto LFail;
    }

    pWaveFile = new CWaveFile();
    if( pWaveFile == NULL )
    {
        hr = E_OUTOFMEMORY;
        goto LFail;
    }

    pWaveFile->OpenFromMemory( pbData,ulDataSize, pwfx );


    // Make the DirectSound buffer the same size as the wav file
    dwDSBufferSize = ulDataSize;

    // Create the direct sound buffer, and only request the flags needed
    // since each requires some overhead and limits if the buffer can
    // be hardware accelerated
    DSBUFFERDESC dsbd;
    ZeroMemory( &dsbd, sizeof(DSBUFFERDESC) );
    dsbd.dwSize          = sizeof(DSBUFFERDESC);
    dsbd.dwFlags         = dwCreationFlags;
    dsbd.dwBufferBytes   = dwDSBufferSize;
    dsbd.guid3DAlgorithm = guid3DAlgorithm;
    dsbd.lpwfxFormat     = pwfx;

    if( FAILED( hr = m_pDS->CreateSoundBuffer( &dsbd, &apDSBuffer[0], NULL ) ) )
    {
        DXUT_ERR( L"CreateSoundBuffer", hr );
        goto LFail;
    }

    // Default to use DuplicateSoundBuffer() when created extra buffers since always
    // create a buffer that uses the same memory however DuplicateSoundBuffer() will fail if
    // DSBCAPS_CTRLFX is used, so use CreateSoundBuffer() instead in this case.
    if( (dwCreationFlags & DSBCAPS_CTRLFX) == 0 )
    {
        for( i=1; i<dwNumBuffers; i++ )
        {
            if( FAILED( hr = m_pDS->DuplicateSoundBuffer( apDSBuffer[0], &apDSBuffer[i] ) ) )
            {
                DXUT_ERR( L"DuplicateSoundBuffer", hr );
                goto LFail;
            }
        }
    }
    else
    {
        for( i=1; i<dwNumBuffers; i++ )
        {
            hr = m_pDS->CreateSoundBuffer( &dsbd, &apDSBuffer[i], NULL );
            if( FAILED(hr) )
            {
                DXUT_ERR( L"CreateSoundBuffer", hr );
                goto LFail;
            }
        }
   }

    // Create the sound
    *ppSound = new CSound( apDSBuffer, dwDSBufferSize, dwNumBuffers, pWaveFile, dwCreationFlags );

	AddAndNotify( *ppSound );

    SAFE_DELETE_ARRAY( apDSBuffer );
    return S_OK;

LFail:
    // Cleanup

    SAFE_DELETE_ARRAY( apDSBuffer );
    return hr;
}

DWORD CSoundManager::SoundEventHandler()
{
	while( !s_bIsTerminating )
	{

#ifdef _DEBUG
		std::cout<<"Events Count: "<<CSoundManager::s_nEventsCount<<std::endl;
#endif
		DWORD dwRes = ::WaitForMultipleObjects(
			s_nEventsCount, s_hEvents, false, INFINITE);
		if( dwRes == WAIT_OBJECT_0 )
		{
			::ResetEvent( s_hEvents[0] );
			continue;
		};
		if( dwRes == WAIT_ABANDONED_0 )
			::ExitThread(0);
		if( dwRes >= WAIT_OBJECT_0 && dwRes < s_nEventsCount )
		{
			DWORD dwObjIndex = dwRes - WAIT_OBJECT_0;
#ifdef _DEBUG
			std::cout<<"Object signaled: "<<dwObjIndex<<std::endl;
#endif
			::ResetEvent( s_hEvents[dwObjIndex] );
			CSound *pSound = s_LoopSounds[dwObjIndex];
			assert( pSound != NULL );
			LPDIRECTSOUNDBUFFER pDSB = (pSound->m_apDSBuffer[0]);
			assert( pDSB != NULL );
			pDSB->SetCurrentPosition(pSound->m_dwStartPosition);
		};
		continue;
	};
	::ExitThread(0);
};

};