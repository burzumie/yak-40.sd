/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Common/Macros.h $

  Last modification:
    $Date: 19.02.06 5:54 $
    $Revision: 7 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

double	FSAPI icb_dummy(PELEMENT_ICON pelement);
double FSAPI icb_Ico(PELEMENT_ICON pelement); 

#define CHK_BRT() CheckBright(pelement,(int)POS_GET(POS_PANEL_STATE));

#define GAUGE_HEADER_FS700_EX(default_size_mm,gauge_name,element_list,pmouse_rect,pgauge_callback,user_data,parameters,usage,gvn) \
		GAUGE_HEADER_FS800(gvn,default_size_mm,gauge_name,element_list,pmouse_rect,pgauge_callback,user_data,usage)

/************************************************************************/
/* �������� �������                                                     */
/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//
// �����
//
#define MOUSE_MLR	MOUSE_MOVE|MOUSE_LEFTSINGLE|MOUSE_RIGHTSINGLE
#define MOUSE_LR	MOUSE_LEFTSINGLE|MOUSE_RIGHTSINGLE
#define MOUSE_DLR	MOUSE_LEFTALL|MOUSE_RIGHTALL|MOUSE_DOWN_REPEAT

#define PRS_AZS(P,V)	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) { \
	AZS_SET(P,0); \
} else { \
	AZS_SET(P,V); \
} \
	return TRUE;

#define PRS_BTN(P)	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) { \
	BTN_SET(P,0); \
} else { \
	BTN_SET(P,1); \
} \
	return TRUE;

#define PRS_BTN2(P,V)	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) { \
	BTN_SET(P,0); \
} else { \
	BTN_SET(P,V); \
} \
	return TRUE;

//////////////////////////////////////////////////////////////////////////
//
// ������� ������� ����
//
#define MOUSE_PBOX(X,Y,SX,SY,CURS,FLG,CB)	/**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_CHILD_FUNCT(0,0,SX,SY,CURS,FLG,CB) \
	MOUSE_PARENT_END

//////////////////////////////////////////////////////////////////////////
//
// ������� ������� ���� � ���������
//
#define MOUSE_TBOX(TN,X,Y,SX,SY,CURS,FLG,CB)	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",ttargs) \
	MOUSE_CHILD_FUNCT(0,0,SX,SY,CURS,FLG,(PMOUSE_FUNCTION)CB) \
	MOUSE_PARENT_END

#define MOUSE_BOX(X,Y,SX,SY,CURS,FLG,CB)	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_CHILD_FUNCT(0,0,SX,SY,CURS,FLG,(PMOUSE_FUNCTION)CB) \
	MOUSE_PARENT_END

//////////////////////////////////////////////////////////////////////////
//
// ������� ���� �������� �� ��� ����� �� ����������� � ���������
//
#define MOUSE_TSHB(TN,X,Y,SX,SY,CURS1,CURS2,FLG,CB1,CB2) /**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",ttargs) \
	MOUSE_CHILD_FUNCT(0,0,SX/2,SY,CURS1,FLG,(PMOUSE_FUNCTION)CB1) \
	MOUSE_CHILD_FUNCT(SX/2,0,SX/2,SY,CURS2,FLG,(PMOUSE_FUNCTION)CB2) \
	MOUSE_PARENT_END

//////////////////////////////////////////////////////////////////////////
//
// ������� ���� �������� �� ��� ����� �� ����������� � ���������
//
#define MOUSE_TSH3(TN,X,Y,SX,SY,CURS1,CURS2,CURS3,FLG,CB1,CB2,CB3) /**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",ttargs) \
	MOUSE_CHILD_FUNCT(0,0,SX/3,SY,CURS1,FLG,(PMOUSE_FUNCTION)CB1) \
	MOUSE_CHILD_FUNCT(SX/3,0,SX/3,SY,CURS2,FLG,(PMOUSE_FUNCTION)CB2) \
	MOUSE_CHILD_FUNCT(SX-SX/3,0,SX/3,SY,CURS3,FLG,(PMOUSE_FUNCTION)CB3) \
	MOUSE_PARENT_END

//////////////////////////////////////////////////////////////////////////
//
// ������� ���� �������� �� ��� ����� �� ��������� � ���������
//
#define MOUSE_TSVB(TN,X,Y,SX,SY,CURS1,CURS2,FLG,CB1,CB2) MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",ttargs) \
	MOUSE_CHILD_FUNCT(0,0,SX,SY/2,CURS1,FLG,(PMOUSE_FUNCTION)CB1) \
	MOUSE_CHILD_FUNCT(0,SY/2,SX,SY/2,CURS2,FLG,(PMOUSE_FUNCTION)CB2) \
	MOUSE_PARENT_END

//////////////////////////////////////////////////////////////////////////
//
// ������� ���� �������� �� ��� ����� �� ��������� � ���������
//
#define MOUSE_TSV3(TN,X,Y,SX,SY,CURS1,CURS2,CURS3,FLG,CB1,CB2,CB3) /**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",ttargs) \
	MOUSE_CHILD_FUNCT(0,0,SX,SY/3,CURS1,FLG,(PMOUSE_FUNCTION)CB1) \
	MOUSE_CHILD_FUNCT(0,SY/3,SX,SY/3,CURS2,FLG,(PMOUSE_FUNCTION)CB2) \
	MOUSE_CHILD_FUNCT(0,SY-SY/3,SX,SY/3,CURS3,FLG,(PMOUSE_FUNCTION)CB3) \
	MOUSE_PARENT_END

//////////////////////////////////////////////////////////////////////////
//
// ������� ���� �������� �� ������ ����� (�����, ����, ��� ����, ��� �����) � ���������
//
#define MOUSE_TS4B(TN,X,Y,SX,SY,CURS1,CURS2,CURS3,CURS4,FLG,CB1,CB2,CB3,CB4) /**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",ttargs) \
	MOUSE_CHILD_FUNCT(    0,     0,    SX, SY/(PIXEL)3.14 ,CURS1,FLG,(PMOUSE_FUNCTION)CB1) \
	MOUSE_CHILD_FUNCT(    0, SY-30, SX-40, SY/(PIXEL)3.6  ,CURS2,FLG,(PMOUSE_FUNCTION)CB2) \
	MOUSE_CHILD_FUNCT( SX/2, SY-30, SX-40, SY/(PIXEL)3.6  ,CURS3,FLG,(PMOUSE_FUNCTION)CB3) \
	MOUSE_CHILD_FUNCT(    0,  SY/3,    SX, SY/(PIXEL)2.4  ,CURS4,FLG,(PMOUSE_FUNCTION)CB4) \
	MOUSE_PARENT_END

//////////////////////////////////////////////////////////////////////////
//
// ������� ���� ������ � ���������
//
#define MOUSE_TTPB(TN,X,Y,SX,SY)    /**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",ttargs) \
	MOUSE_PARENT_END

/************************************************************************/
/* �������� ������. ��������� ������ �� ������ �������.                 */
/* ������� � ����� �������� ����������� ������� l_						*/
/************************************************************************/
#define MAKE_ICB(N,C)	double		FSAPI N(PELEMENT_ICON pelement){C;}
#define MAKE_NCB(N,C)	double		FSAPI N(PELEMENT_NEEDLE pelement){C;}
#define MAKE_MCB(N,C)	double		FSAPI N(PELEMENT_MOVING_IMAGE pelement){C;}
#define MAKE_SCB(N,C)	double		FSAPI N(PELEMENT_SLIDER pelement){C;}
#define MAKE_SPCB(N,C)	double		FSAPI N(PELEMENT_SPRITE pelement){C;}
#define MAKE_MSCB(N,C)	bool		FSAPI N(PPIXPOINT relative_point,FLAGS32 mouse_flags){C; return TRUE;}
#define MAKE_TCB(N,C)	PCSTRINGZ	FSAPI N(FLOAT64 number,ID id,PCSTRINGZ string,MODULE_VAR *source_var,PGAUGEHDR gauge){C}
#define MAKE_TTA(N)		MOUSE_TOOLTIP_ARG(MODULE_VAR_NONE,0,NULL,NULL,NULL,NULL,NULL,N)

#define	MY_ICON(		NAME,														\
	RES_ID,														\
	NEXT_LIST,													\
	POSITION_X, POSITION_Y,										\
	CALLBACK,													\
	NUM_ICONS)													\
	\
	ELEMENT_ICON				NAME	=											\
	{																				\
	ELEMENT_TYPE_ICON,															\
	RES_ID,																		\
	POSITION_X, POSITION_Y,														\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(NULL),													\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE,										\
		0,																			\
		0,																			\
		{MODULE_VAR_NONE},															\
		NULL,																		\
		ICON_SWITCH_TYPE_SET_CUR_ICON,												\
		0,																			\
		0,																			\
		0,																			\
		NUM_ICONS,																	\
		0,																			\
		0,																			\
		NULL,																		\
		NULL,																		\
		CALLBACK,																	\
	};																				\
	\
	PELEMENT_HEADER	l_##NAME[]={														\
	&NAME.header,																	\
	NULL																			\
};

#define	MY_NEEDLE(		NAME,														\
	RES_ID,														\
	NEXT_LIST,													\
	BKND_POSITION_X, BKND_POSITION_Y,							\
	NDL_POSITION_X, NDL_POSITION_Y,								\
	CALLBACK,													\
	NONLINEARITY_TABLE,											\
	MAX_DEG_PER_SEC )											\
	\
	ELEMENT_NEEDLE				NAME	=											\
	{																				\
	ELEMENT_TYPE_NEEDLE,														\
	RES_ID,																		\
	BKND_POSITION_X, BKND_POSITION_Y,											\
		{0,0},																		\
		NDL_POSITION_X, NDL_POSITION_Y,												\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(NULL),													\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|BIT7,								\
		0,																			\
		0,																			\
		{MODULE_VAR_NONE},															\
		NULL,																		\
		NULL,																		\
		NULL,																		\
		NULL,																		\
		0, 0,																		\
		NONLINEARITY_TABLE,															\
		sizeof( NONLINEARITY_TABLE )/sizeof( NONLINEARITY ),						\
		MAX_DEG_PER_SEC,															\
		0.0,																		\
		0.0,																		\
		0.0,																		\
		{0,0},																		\
		{0,0},{0,0},{0,0},{0,0},													\
		{0},																		\
		NULL,																		\
		0,																			\
		0,																			\
		{0,0},																		\
		CALLBACK,																	\
		{0,0},																		\
	};																				\
	\
	PELEMENT_HEADER	l_##NAME[]={														\
	&NAME.header,																	\
	NULL																			\
};

#define	MY_MOVING(		NAME,														\
	RES_ID,														\
	NEXT_LIST,													\
	POSITION_X, POSITION_Y,										\
	CALLBACK_X,													\
	MIN_X, MAX_X,												\
	CALLBACK_Y,													\
	MIN_Y, MAX_Y )												\
	\
	ELEMENT_MOVING_IMAGE		NAME	=											\
	{																				\
	ELEMENT_TYPE_MOVING_IMAGE,													\
	RES_ID,																		\
	POSITION_X, POSITION_Y,														\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(NULL),													\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE,										\
		0,																			\
		0,																			\
		{MODULE_VAR_NONE},															\
		NULL,																		\
		MIN_X,																		\
		MAX_X,																		\
		NULL,																		\
		{MODULE_VAR_NONE},															\
		NULL,																		\
		MIN_Y,																		\
		MAX_Y,																		\
		NULL,																		\
		0.0,																		\
		0.0,																		\
		0.0,																		\
		0.0,																		\
		{0,0},																		\
		{0},																		\
		{0},																		\
		NULL,																		\
		NULL,																		\
		CALLBACK_X,																	\
		CALLBACK_Y																	\
	};																				\
	\
	PELEMENT_HEADER	l_##NAME[]={														\
	&NAME.header,																	\
	NULL																			\
};

#define	MY_SLIDER(		NAME,														\
	RES_ID,														\
	NEXT_LIST,													\
	POSITION_X, POSITION_Y,										\
	CALLBACK_X, SCALE_X,										\
	CALLBACK_Y, SCALE_Y )										\
	\
	ELEMENT_SLIDER				NAME	=											\
	{																				\
	ELEMENT_TYPE_SLIDER,														\
	RES_ID,																		\
	POSITION_X, POSITION_Y,														\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(NULL),													\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE,										\
		0,																			\
		0,																			\
		\
		{MODULE_VAR_NONE},															\
		NULL,																		\
		SCALE_X,																	\
		NULL,																		\
		{MODULE_VAR_NONE},															\
		NULL,																		\
		SCALE_Y,																	\
		NULL,																		\
		{0},																		\
		NULL,																		\
		CALLBACK_X,																	\
		CALLBACK_Y																	\
	};																				\
	\
	PELEMENT_HEADER	l_##NAME[]={														\
	&NAME.header,																	\
	NULL																			\
};

#define	MY_STRING(		NAME,														\
	NEXT_LIST,													\
	POSITION_X, POSITION_Y,										\
	SIZE_X, SIZE_Y,												\
	NUM_CHARS,													\
	SOURCE_VAR_1,												\
	SOURCE_VAR_2,												\
	SOURCE_VAR_3,												\
	FORECOLOR,													\
	BACKCOLOR,													\
	HILITECOLOR,												\
	FONT_NAME,													\
	FONT_WEIGHT,												\
	FONT_CHARSET,												\
	FONT_SIZE,													\
	DRAW_TEXT_FLAGS,											\
	HILITE_LIST,												\
	CALLBACK)													\
	\
	ELEMENT_STRING				NAME	=											\
	{																				\
	ELEMENT_TYPE_STRING,														\
	-1,																			\
	POSITION_X, POSITION_Y,														\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(NULL),													\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_BRIGHT,					\
		0,																			\
		0,																			\
		\
		{{SOURCE_VAR_1}, {SOURCE_VAR_2}, {SOURCE_VAR_3}},							\
		{0, 0, 0},																	\
		{NULL, NULL, NULL},															\
		NULL,																		\
		NULL,																		\
		NULL,																		\
		SIZE_X, SIZE_Y,																\
		NULL,																		\
		NULL,																		\
		FORECOLOR,																	\
		BACKCOLOR,																	\
		HILITECOLOR,																\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		FONT_NAME,																	\
		FONT_WEIGHT,																\
		FONT_CHARSET,																\
		DRAW_TEXT_FLAGS,															\
		NUM_CHARS,																	\
		0,																			\
		0,																			\
		0,																			\
		HILITE_LIST,																\
		0,																			\
		FONT_SIZE,																	\
		CALLBACK,																	\
		FALSE,																		\
		0,																			\
	};																				\
	\
	PELEMENT_HEADER	l_##NAME[]={														\
	&NAME.header,																	\
	NULL																			\
};

#define	MY_SPRITE(		NAME,														\
	RES_ID,														\
	NEXT_LIST,													\
	BKND_POSITION_X, BKND_POSITION_Y,							\
	TEXTURE_CENTER_X, TEXTURE_CENTER_Y,							\
	TEXTURE_SCALE_X, TEXTURE_SCALE_Y,							\
	SOURCE_VAR_X, CALLBACK_X, SCALE_X,							\
	SOURCE_VAR_Y, CALLBACK_Y, SCALE_Y,							\
	SOURCE_VAR_0, CALLBACK_0, SCALE_0 )							\
	\
	ELEMENT_SPRITE				NAME	=											\
	{																				\
	ELEMENT_TYPE_SPRITE,														\
	RES_ID,																		\
		{BKND_POSITION_X, BKND_POSITION_Y},											\
		{0,0},																		\
		{TEXTURE_CENTER_X, TEXTURE_CENTER_Y},										\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(NULL),													\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE,										\
		0,																			\
		0,																			\
		\
		{SOURCE_VAR_X},																\
		NULL,																		\
		SCALE_X,																	\
		{SOURCE_VAR_Y},																\
		NULL,																		\
		SCALE_Y,																	\
		{SOURCE_VAR_0},																\
		NULL,																		\
		SCALE_0,																	\
		(FLOAT32)TEXTURE_SCALE_X, (FLOAT32)TEXTURE_SCALE_Y,							\
		{0,0},																		\
		{0,0},																		\
		{0,0},{0,0},{0,0},{0,0},													\
		{0,0},{0,0},{0,0},{0,0},													\
		{0},																		\
		NULL,																		\
		0,																			\
		{0},																		\
		NULL,																		\
		NULL,																		\
		CALLBACK_X,																	\
		CALLBACK_Y,																	\
		CALLBACK_0																	\
	};																				\
	\
	PELEMENT_HEADER	l_##NAME[]={														\
	&NAME.header,																	\
	NULL																			\
};

#define	MY_STATIC(		NAME,														\
	NAMELIST,													\
	RES_ID,														\
	NEXT_LIST)													\
	\
	ELEMENT_STATIC_IMAGE		NAME	=											\
	{																				\
	ELEMENT_TYPE_STATIC_IMAGE,													\
	RES_ID,																		\
	0, 0,																		\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(NULL),													\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE,										\
		0,																			\
		0																			\
	};																				\
	\
	PELEMENT_HEADER NAMELIST=&NAME.header;

#define	MY_STAT2(		NAME,														\
	RES_ID,														\
	NEXT_LIST,X,Y,FLAGS,GAUGEHDR_VAR_NAME)													\
	\
	ELEMENT_STATIC_IMAGE		NAME	=											\
	{																				\
	ELEMENT_TYPE_STATIC_IMAGE,													\
	RES_ID,																		\
	X, Y,																		\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(NULL),													\
		FLAGS,										\
		0,																			\
		0																			\
	};																				\
	\
	PELEMENT_HEADER	l_##NAME[]={														\
	&NAME.header,																	\
	NULL																			\
};

//////////////////////////////////////////////////////////////////////////

#define SHOW_AZS(V,M)		CHK_BRT(); return AZS_GET(V)+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_BTN(V,M)		CHK_BRT(); return BTN_GET(V)+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_GLT(V,M)		CHK_BRT(); return GLT_GET(V)+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_KRM(V,M)		CHK_BRT(); return KRM_GET(V)+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_POS(V,M)		CHK_BRT(); return POS_GET(V)+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_POSM(V,P,M)	CHK_BRT(); return POS_GET(V)-P+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_POSP(V,P,M)	CHK_BRT(); return POS_GET(V)+P+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_LMP(V,B,M)	CHK_BRT(); int i=!PWR_GET(PWR_BUS27)?0:B&&AZS_GET(AZS_LAMP_TEST_BUS)?AZS_GET(AZS_WARNING_LIGHTS)+1:!LMP_GET(V)?0:AZS_GET(AZS_WARNING_LIGHTS)+1; if(i&&POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY)LIGHT_IMAGE(pelement); LMPE_SET(V,i); return i+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_LMPEXT(V,B,M) CHK_BRT(); int i=!PWR_GET(PWR_BUS27)&&!PWR_GET(PWR_BUS27EXT)?0:B&&AZS_GET(AZS_LAMP_TEST_BUS)?AZS_GET(AZS_WARNING_LIGHTS)+1:!LMP_GET(V)?0:AZS_GET(AZS_WARNING_LIGHTS)+1; if(i&&POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY)LIGHT_IMAGE(pelement); LMPE_SET(V,i); return i+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_TBL(V,B,M)	CHK_BRT(); int i=!PWR_GET(PWR_BUS27)?0:B&&AZS_GET(AZS_LAMP_TEST_BUS)?AZS_GET(AZS_WARNING_LIGHTS)+1:!TBL_GET(V)?0:AZS_GET(AZS_WARNING_LIGHTS)+1; if(i&&POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY)LIGHT_IMAGE(pelement); TBLE_SET(V,i); return i+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_TBG(V,B,M)	CHK_BRT(); int i=!PWR_GET(PWR_BUS27)?0:B&&AZS_GET(AZS_LAMP_TEST_BUS)?AZS_GET(AZS_WARNING_LIGHTS)+1:!TBG_GET(V)?0:AZS_GET(AZS_WARNING_LIGHTS)+1; if(i&&POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY)LIGHT_IMAGE(pelement); TBGE_SET(V,i); return i+POS_GET(POS_PANEL_STATE)*M;
#define SHOW_NDL(V)		CHK_BRT(); return NDL_GET(V);
#define SHOW_NDLM(V,P)	CHK_BRT(); return NDL_GET(V)-P;
#define SHOW_NDLP(V,P)	CHK_BRT(); return NDL_GET(V)+P;
#define SHOW_HND(V,M)	CHK_BRT(); return HND_GET(V)+POS_GET(POS_PANEL_STATE)*M;

#define SHOW_AZS_COV(N) /**/ \
	if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	switch((int)AZS_GET(N)) { \
			case 0: \
			AZS_SET(N,1); \
			break; \
			case 1: \
			AZS_SET(N,0); \
			break; \
			case 2: \
			AZS_SET(N,3); \
			break; \
			case 3: \
			AZS_SET(N,2); \
			break; \
		} \
	} else if(mouse_flags&MOUSE_LEFTSINGLE) { \
	if(AZS_GET(N)==1) { \
	AZS_SET(N,2); \
	} else if(AZS_GET(N)==2) { \
	AZS_SET(N,1); \
	} \
	} \
	return TRUE;

#define SHOW_AZS_COV_PK25(N) /**/ \
	if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	switch((int)AZS_GET(N)) { \
			case 0: \
			AZS_SET(N,1); \
			break; \
			case 1: \
			AZS_SET(N,3); \
			break; \
			case 2: \
			AZS_SET(N,3); \
			break; \
			case 3: \
			AZS_SET(N,2); \
			break; \
	} \
	} else if(mouse_flags&MOUSE_LEFTSINGLE) { \
	if(AZS_GET(N)==1) { \
	AZS_SET(N,2); \
	} else if(AZS_GET(N)==2) { \
	AZS_SET(N,1); \
	} \
	} \
	return TRUE;

#define	MY_STATIC2(NAME,NAMELIST,RES_ID,NEXT_LIST,GAUGEHDR_VAR_NAME) \
	ELEMENT_STATIC_IMAGE NAME = {				\
	ELEMENT_TYPE_STATIC_IMAGE,				\
	RES_ID,									\
	0, 0,									\
		{0,0},									\
		{0,0},									\
		&GAUGEHDR_VAR_NAME,						\
		NULL,									\
		(PPELEMENT_HEADER)(NEXT_LIST),			\
		(PFAILURE_RECORD)(NULL),				\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE,	\
		0,										\
		0										\
	};											\
	PELEMENT_HEADER NAMELIST=&NAME.header;

#define	MY_STATIC2A(NAME,RES_ID,NEXT_LIST,X,Y,GAUGEHDR_VAR_NAME) \
	ELEMENT_STATIC_IMAGE NAME = {				\
	ELEMENT_TYPE_STATIC_IMAGE,				\
	RES_ID,									\
	X, Y,									\
		{0,0},									\
		{0,0},									\
		&GAUGEHDR_VAR_NAME,						\
		NULL,									\
		(PPELEMENT_HEADER)(NEXT_LIST),			\
		(PFAILURE_RECORD)(NULL),				\
		IMAGE_USE_ALPHA|IMAGE_USE_TRANSPARENCY,	\
		0,										\
		0										\
	};											\
	PELEMENT_HEADER	l_##NAME[]={				\
	&NAME.header,							\
	NULL									\
	};	

#define	MY_STATIC2F(NAME,RES_ID,NEXT_LIST,X,Y,FLAGS,GAUGEHDR_VAR_NAME) \
	ELEMENT_STATIC_IMAGE NAME = {				\
	ELEMENT_TYPE_STATIC_IMAGE,				\
	RES_ID,									\
	X, Y,									\
		{0,0},									\
		{0,0},									\
		&GAUGEHDR_VAR_NAME,						\
		NULL,									\
		(PPELEMENT_HEADER)(NEXT_LIST),			\
		(PFAILURE_RECORD)(NULL),				\
		FLAGS,	\
		0,										\
		0										\
	};											\
	PELEMENT_HEADER	l_##NAME[]={				\
	&NAME.header,							\
	NULL									\
	};	

#define	MY_STATIC2B(NAME,NAMELIST,RES_ID,NEXT_LIST,GAUGEHDR_VAR_NAME) \
	ELEMENT_STATIC_IMAGE NAME = {				\
	ELEMENT_TYPE_STATIC_IMAGE,				\
	RES_ID,									\
	0, 0,									\
		{0,0},									\
		{0,0},									\
		&GAUGEHDR_VAR_NAME,						\
		NULL,									\
		(PPELEMENT_HEADER)(NEXT_LIST),			\
		(PFAILURE_RECORD)(NULL),				\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_BRIGHT,	\
		0,										\
		0										\
	};											\
	PELEMENT_HEADER NAMELIST=&NAME.header;

#define	MY_ICON2(NAME,RES_ID,NEXT_LIST,POSITION_X,POSITION_Y,CALLBACK,NUM_ICONS,GAUGEHDR_VAR_NAME) \
	ELEMENT_ICON NAME = {						\
	ELEMENT_TYPE_ICON,						\
	RES_ID,									\
	POSITION_X, POSITION_Y,					\
		{0,0},									\
		{0,0},									\
		&GAUGEHDR_VAR_NAME,						\
		NULL,									\
		(PPELEMENT_HEADER)(NEXT_LIST),			\
		(PFAILURE_RECORD)(NULL),				\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE,	\
		0,										\
		0,										\
		{MODULE_VAR_NONE},						\
		NULL,									\
		ICON_SWITCH_TYPE_SET_CUR_ICON,			\
		0,										\
		0,										\
		0,										\
		NUM_ICONS,								\
		0,										\
		0,										\
		NULL,									\
		NULL,									\
		CALLBACK,								\
	};											\
	PELEMENT_HEADER	l_##NAME[]={				\
	&NAME.header,							\
	NULL									\
	};

#define	MY_NEEDLE2(NAME,RES_ID,NEXT_LIST,BKND_POSITION_X,BKND_POSITION_Y,NDL_POSITION_X,NDL_POSITION_Y,CALLBACK,NONLINEARITY_TABLE,MAX_DEG_PER_SEC,GAUGEHDR_VAR_NAME) \
	ELEMENT_NEEDLE NAME	= {										\
	ELEMENT_TYPE_NEEDLE,									\
	RES_ID,													\
	BKND_POSITION_X, BKND_POSITION_Y,						\
		{0,0},													\
		NDL_POSITION_X, NDL_POSITION_Y,							\
		&GAUGEHDR_VAR_NAME,										\
		NULL,													\
		(PPELEMENT_HEADER)(NEXT_LIST),							\
		(PFAILURE_RECORD)(NULL),								\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|BIT7,			\
		0,														\
		0,														\
		{MODULE_VAR_NONE},										\
		NULL,													\
		NULL,													\
		NULL,													\
		NULL,													\
		0, 0,													\
		NONLINEARITY_TABLE,										\
		sizeof( NONLINEARITY_TABLE )/sizeof( NONLINEARITY ),	\
		MAX_DEG_PER_SEC,										\
		0.0,													\
		0.0,													\
		0.0,													\
		{0,0},													\
		{0,0},{0,0},{0,0},{0,0},								\
		{0},													\
		NULL,													\
		0,														\
		0,														\
		{0,0},													\
		CALLBACK,												\
		{0,0},													\
	};															\
	PELEMENT_HEADER	l_##NAME[]={								\
	&NAME.header,											\
	NULL													\
	};

#define	MY_SLIDER2(NAME,RES_ID,NEXT_LIST,POSITION_X,POSITION_Y,CALLBACK_X,SCALE_X,CALLBACK_Y,SCALE_Y,GAUGEHDR_VAR_NAME) \
	ELEMENT_SLIDER NAME	= {						\
	ELEMENT_TYPE_SLIDER,					\
	RES_ID,									\
	POSITION_X, POSITION_Y,					\
		{0,0},									\
		{0,0},									\
		&GAUGEHDR_VAR_NAME,						\
		NULL,									\
		(PPELEMENT_HEADER)(NEXT_LIST),			\
		(PFAILURE_RECORD)(NULL),				\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE,	\
		0,										\
		0,										\
		{MODULE_VAR_NONE},						\
		NULL,									\
		SCALE_X,								\
		NULL,									\
		{MODULE_VAR_NONE},						\
		NULL,									\
		SCALE_Y,								\
		NULL,									\
		{0},									\
		NULL,									\
		CALLBACK_X,								\
		CALLBACK_Y								\
	};											\
	PELEMENT_HEADER	l_##NAME[]={				\
	&NAME.header,							\
	NULL									\
	};

#define	MY_MOVING2(NAME,RES_ID,NEXT_LIST,POSITION_X,POSITION_Y,CALLBACK_X,MIN_X,MAX_X,CALLBACK_Y,MIN_Y,MAX_Y,GAUGEHDR_VAR_NAME) \
	ELEMENT_MOVING_IMAGE NAME =	{				\
	ELEMENT_TYPE_MOVING_IMAGE,				\
	RES_ID,									\
	POSITION_X, POSITION_Y,					\
		{0,0},									\
		{0,0},									\
		&GAUGEHDR_VAR_NAME,						\
		NULL,									\
		(PPELEMENT_HEADER)(NEXT_LIST),			\
		(PFAILURE_RECORD)(NULL),				\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE,	\
		0,										\
		0,										\
		{MODULE_VAR_NONE},						\
		NULL,									\
		MIN_X,									\
		MAX_X,									\
		NULL,									\
		{MODULE_VAR_NONE},						\
		NULL,									\
		MIN_Y,									\
		MAX_Y,									\
		NULL,									\
		0.0,									\
		0.0,									\
		0.0,									\
		0.0,									\
		{0,0},									\
		{0},									\
		{0},									\
		NULL,									\
		NULL,									\
		CALLBACK_X,								\
		CALLBACK_Y								\
	};											\
	PELEMENT_HEADER	l_##NAME[]={				\
	&NAME.header,							\
	NULL									\
	};

#define	MY_SPRITE2(NAME,RES_ID,NEXT_LIST,BKND_POSITION_X,BKND_POSITION_Y,TEXTURE_CENTER_X,TEXTURE_CENTER_Y,TEXTURE_SCALE_X,TEXTURE_SCALE_Y,SOURCE_VAR_X,CALLBACK_X,SCALE_X,SOURCE_VAR_Y,CALLBACK_Y,SCALE_Y,SOURCE_VAR_0,CALLBACK_0,SCALE_0,GAUGEHDR_VAR_NAME) \
	ELEMENT_SPRITE NAME	= {									\
	ELEMENT_TYPE_SPRITE,								\
	RES_ID,												\
		{BKND_POSITION_X, BKND_POSITION_Y},					\
		{0,0},												\
		{TEXTURE_CENTER_X, TEXTURE_CENTER_Y},				\
		&GAUGEHDR_VAR_NAME,									\
		NULL,												\
		(PPELEMENT_HEADER)(NEXT_LIST),						\
		(PFAILURE_RECORD)(NULL),							\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|BIT7,		\
		0,													\
		0,													\
		{SOURCE_VAR_X},										\
		NULL,												\
		SCALE_X,											\
		{SOURCE_VAR_Y},										\
		NULL,												\
		SCALE_Y,											\
		{SOURCE_VAR_0},										\
		NULL,												\
		SCALE_0,											\
		(FLOAT32)TEXTURE_SCALE_X, (FLOAT32)TEXTURE_SCALE_Y,	\
		{0,0},												\
		{0,0},												\
		{0,0},{0,0},{0,0},{0,0},							\
		{0,0},{0,0},{0,0},{0,0},							\
		{0},												\
		NULL,												\
		0,													\
		{0},												\
		NULL,												\
		NULL,												\
		CALLBACK_X,											\
		CALLBACK_Y,											\
		CALLBACK_0											\
	};														\
	PELEMENT_HEADER	l_##NAME[] = {							\
	&NAME.header,										\
	NULL												\
	};

#define	MY_STRING2(		NAME,														\
	NEXT_LIST,													\
	POSITION_X, POSITION_Y,										\
	SIZE_X, SIZE_Y,												\
	NUM_CHARS,													\
	SOURCE_VAR_1,												\
	SOURCE_VAR_2,												\
	SOURCE_VAR_3,												\
	FORECOLOR,													\
	BACKCOLOR,													\
	HILITECOLOR,												\
	FONT_NAME,													\
	FONT_WEIGHT,												\
	FONT_CHARSET,												\
	FONT_SIZE,													\
	DRAW_TEXT_FLAGS,											\
	HILITE_LIST,												\
	CALLBACK,GAUGEHDR_VAR_NAME)													\
	\
	ELEMENT_STRING				NAME	=											\
	{																				\
	ELEMENT_TYPE_STRING,														\
	-1,																			\
	POSITION_X, POSITION_Y,														\
		{0,0},																		\
		{0,0},																		\
		&GAUGEHDR_VAR_NAME,															\
		NULL,																		\
		(PPELEMENT_HEADER)(NEXT_LIST),												\
		(PFAILURE_RECORD)(NULL),													\
		IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_BRIGHT,					\
		0,																			\
		0,																			\
		\
		{{SOURCE_VAR_1}, {SOURCE_VAR_2}, {SOURCE_VAR_3}},							\
		{0, 0, 0},																	\
		{NULL, NULL, NULL},															\
		NULL,																		\
		NULL,																		\
		NULL,																		\
		SIZE_X, SIZE_Y,																\
		NULL,																		\
		NULL,																		\
		FORECOLOR,																	\
		BACKCOLOR,																	\
		HILITECOLOR,																\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		0,																			\
		FONT_NAME,																	\
		FONT_WEIGHT,																\
		FONT_CHARSET,																\
		DRAW_TEXT_FLAGS,															\
		NUM_CHARS,																	\
		0,																			\
		0,																			\
		0,																			\
		HILITE_LIST,																\
		0,																			\
		FONT_SIZE,																	\
		CALLBACK,																	\
		FALSE,																		\
		0,																			\
	};																				\
	\
	PELEMENT_HEADER	l_##NAME[]={														\
	&NAME.header,																	\
	NULL																			\
};

#define MOUSE_TTPB2(TN,X,Y,SX,SY,N)    /**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",N##_ttargs) \
	MOUSE_PARENT_END

#define MOUSE_TBOX2(TN,X,Y,SX,SY,CURS,FLG,CB,N)	/**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",N##_ttargs) \
	MOUSE_CHILD_FUNCT(0,0,SX,SY,CURS,FLG,(PMOUSE_FUNCTION)CB) \
	MOUSE_PARENT_END

#define MOUSE_BOX2(X,Y,SX,SY,CURS,FLG,CB,N)	/**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_CHILD_FUNCT(0,0,SX,SY,CURS,FLG,(PMOUSE_FUNCTION)CB) \
	MOUSE_PARENT_END

#define MOUSE_TSHB2(TN,X,Y,SX,SY,CURS1,CURS2,FLG,CB1,CB2,N) /**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",N##_ttargs) \
	MOUSE_CHILD_FUNCT(0,0,SX/2,SY,CURS1,FLG,(PMOUSE_FUNCTION)CB1) \
	MOUSE_CHILD_FUNCT(SX/2,0,SX/2,SY,CURS2,FLG,(PMOUSE_FUNCTION)CB2) \
	MOUSE_PARENT_END

#define MOUSE_TSVB2(TN,X,Y,SX,SY,CURS1,CURS2,FLG,CB1,CB2,N) /**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",N##_ttargs) \
	MOUSE_CHILD_FUNCT(0,0,SX,SY/2,CURS1,FLG,(PMOUSE_FUNCTION)CB1) \
	MOUSE_CHILD_FUNCT(0,SY/2,SX,SY/2,CURS2,FLG,(PMOUSE_FUNCTION)CB2) \
	MOUSE_PARENT_END

#define MOUSE_TSV32(TN,X,Y,SX,SY,CURS1,CURS2,CURS3,FLG,CB1,CB2,CB3,N) /**/ \
	MOUSE_PARENT_BEGIN(X,Y,SX,SY,HELP_NONE) \
	MOUSE_TOOLTIP_TEXT_STRING("%"TN"!s!",N##_ttargs) \
	MOUSE_CHILD_FUNCT(0,0,SX,SY/3,CURS1,FLG,(PMOUSE_FUNCTION)CB1) \
	MOUSE_CHILD_FUNCT(0,SY/3,SX,SY/3,CURS2,FLG,(PMOUSE_FUNCTION)CB2) \
	MOUSE_CHILD_FUNCT(0,SY-SY/3,SX,SY/3,CURS3,FLG,(PMOUSE_FUNCTION)CB3) \
	MOUSE_PARENT_END

#define SIMPLE_GAUGE(N,I,I2) /**/ \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
		CHK_BRT();\
		return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I2,&l_##N##Ico, N);

#define SIMPLE_GAUGE_VC(N,I) /**/ \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_VC_TGL_XML(N,X,I,SX,SY,TT) /**/ \
	static auto_ptr<CNamedVar> m_Xml##N; \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(!m_Xml##N.get()) \
	m_Xml##N=auto_ptr<CNamedVar>(new CNamedVar(X,ImportTable.pPanels,true)); \
	m_Xml##N->set_bool(!m_Xml##N->get_bool()); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return TT;) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", 0, 0, I##_SX, I##_SY,CURSOR_HAND,MOUSE_LR,mcb_##N,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_VC_PRESS_BTN(N,B,I) /**/ \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	PRS_BTN(B); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return BTN_TTGET(B);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", 0, 0, I##_SX, I##_SY,CURSOR_HAND,MOUSE_DLR,mcb_##N,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_VC_PRESS_BTN_2_POS(N,B,I) /**/ \
	bool FSAPI mcb1_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	PRS_BTN2(B,1); \
	return TRUE; \
	} \
	bool FSAPI mcb2_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	PRS_BTN2(B,2); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return BTN_TTGET(B);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TSHB2("1", 0, 0, I##_SX, I##_SY,CURSOR_UPARROW,CURSOR_DOWNARROW,MOUSE_DLR,mcb1_##N,mcb2_##N,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_LANG(N,IR,XIR,YIR,IE,XIE,YIE) /**/ \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(IR##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE,IE,NULL			,XIE,YIE,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoR,IR,&l_##N##IcoE	,XIR,YIR,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,IR,&l_##N##IcoR, N);

#define SIMPLE_GAUGE_VC_LANG1(N,I,I2,X1,Y1,X2,Y2) /**/ \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE1,I2,NULL,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_VC_LANG1_MOUSE(N,I,I2,X1,Y1,X2,Y2,A,X3,Y3,SX3,SY3) /**/ \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(A); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(A);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", X3, Y3, SX3, SY3,CURSOR_HAND,MOUSE_LR,mcb_##N,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE1,I2,NULL,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_LANG2(N,I,I2,I3,X1,Y1,X2,Y2,X3,Y3) /**/ \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE2,I3,NULL,X3,Y3,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE1,I2,&l_##N##IcoE2,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_LANG3(N,I,I2,I3,I4,X1,Y1,X2,Y2,X3,Y3,X4,Y4) /**/ \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE3,I4,NULL,X4,Y4,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE2,I3,&l_##N##IcoE3,X3,Y3,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE1,I2,&l_##N##IcoE2,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_LANG4(N,I,I2,I3,I4,I5,X1,Y1,X2,Y2,X3,Y3,X4,Y4,X5,Y5) /**/ \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE4,I5,NULL,X5,Y5,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE3,I4,&l_##N##IcoE4,X4,Y4,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE2,I3,&l_##N##IcoE3,X3,Y3,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE1,I2,&l_##N##IcoE2,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_LANG1_TRIM(N,I,I2,X1,Y1,X2,Y2,KL,KR,A,XM,YM,SXM,SYM,BUS) /**/ \
	bool FSAPI mcb_##N##_left(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(PWR_GET(PWR_BUS27)&&AZS_GET(BUS)) \
	ImportTable.pPanels->trigger_key_event(KL,0); \
	PRS_AZS(A,1); \
	return TRUE; \
	}; \
	bool FSAPI mcb_##N##_right(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(PWR_GET(PWR_BUS27)&&AZS_GET(BUS)) \
	ImportTable.pPanels->trigger_key_event(KR,0);  \
	PRS_AZS(A,2); \
	return TRUE; \
	}; \
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(A);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TSHB2("1", XM, YM, SXM, SYM,CURSOR_HAND,CURSOR_HAND,MOUSE_DLR,mcb_##N##_left,mcb_##N##_right,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE1,I2,NULL,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_AZS(N,I,A) /**/ \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(A); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(A);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", 0, 0, I##_SX, I##_SY,CURSOR_HAND,MOUSE_LR,mcb_##N,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_CNT(N,IR,XIR,YIR,IE,XIE,YIE,TT,C,FLG) /**/ \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	C; \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return TT;) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", 0, 0, IR##_SX, IR##_SY,CURSOR_HAND,FLG,mcb_##N,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(IR##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE,IE,NULL			,XIE,YIE,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoR,IR,&l_##N##IcoE	,XIR,YIR,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,IR,&l_##N##IcoR, N);

#define SIMPLE_GAUGE_V�_TT(N,I,TT) /**/ \
	static MAKE_TCB(N##_tcb_01,return TT;) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",0, 0, I##_SX, I##_SY,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define TABLO_VC(N,I,T) /**/ \
	static MAKE_TCB(N##_tcb_01,return TBL_TTGET(T);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1", 0, 0, I##_SX, I##_SY,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	SHOW_TBL(T,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3); \
	int tbl=TBLE_GET(T);\
	int brt=AZS_GET(AZS_WARNING_LIGHTS);\
	int lng=int(POS_GET(POS_PANEL_LANG));\
	int sta=int(POS_GET(POS_PANEL_STATE));\
	if(tbl) {\
	if(brt) {\
	return lng?sta*3+2:sta*3+(PANEL_LIGHT_MAX*3+2);\
	} else {\
	return lng?sta*3+1:sta*3+(PANEL_LIGHT_MAX*3+1);\
	}\
	}\
	return lng?sta*3:sta*3+(PANEL_LIGHT_MAX*3);\
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,6*PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_TT8(N,I,TT) /**/ \
	static MAKE_TCB(N##_tcb_01,return TT;) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",0, 0, I##_SX, I##_SY,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return !POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE)*2:POS_GET(POS_PANEL_STATE)*2+1; \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX*2, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_TGL_AZS(N,A,I,SX,SY) /**/ \
	bool FSAPI mcb1_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(A);\
	return TRUE;\
	};\
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(A);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,SX,SY,CURSOR_HAND,MOUSE_LR,mcb1_##N,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_V�_TGL_AZS3V(N,A,I,SX,SY,U,C,D) /**/ \
	bool FSAPI mcb1_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(A,C);\
	return TRUE;\
	};\
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(A);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,SX,SY,CURSOR_HAND,MOUSE_LR,mcb1_##N,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_VC_BTN(N,I,TT,C1) /**/ \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	C1; \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return TT;) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", 0, 0, I##_SX, I##_SY,CURSOR_HAND,MOUSE_DLR,mcb_##N,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define SIMPLE_GAUGE_VC_KNB(N,I,TT,C1,C2) /**/ \
	bool FSAPI mcb_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	C1; \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	C2; \
	} \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return TT;) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", 0, 0, I##_SX, I##_SY,CURSOR_HAND,MOUSE_DLR,mcb_##N,N) \
	MOUSE_END \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);
