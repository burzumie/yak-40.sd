/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Common/CommonFS.h $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include <fs_sdk_common.h>
#include <fs6/fs6.h>
#include <global/global.h>
#include <util/util.h>
#include <controls/controls.h>
#include <panels/panels.h>
#include <flight/flight.h>
#include <g2d/g2d.h>
#include <main/main.h>
#include <multiplayer/multiplayer.h>
#include <sound/sound.h>
#include <weather/weather.h>
#include <window/window.h>
#include <sim1/sim1.h>
#include <acontain/acontain.h>
#include <fs_sdk.h>

#define SDAPISIGN	"FB56C3A2"

#define GAUGE(x) extern GAUGEHDR x;
