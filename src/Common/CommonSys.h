/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Common/CommonSys.h $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#define WIN32_LEAN_AND_MEAN
//#include <windows.h>
#include <cstdio>
#include <cmath>
#include <time.h>
#include <assert.h>
#include <conio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <map>
#include <vector>
#include <bitset>
#include <algorithm>

#define DIRECTINPUT_VERSION 0x0900
#include <dinput.h>

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#define repeat			do 
#define until(exp)		while(!(exp))

#define breakon(exp)	if((exp))break
#define ignoreon(exp)	if((exp))continue
#define leaveon(exp)	if((exp))return

#ifdef _DEBUG
extern _CrtMemState memoryState;
#endif

using namespace std;

extern HINSTANCE g_hInstance;

#ifndef SAFE_NEW
#define SAFE_NEW(p,C)		 { if(!p) { (p)=new C; } }
#endif    
#ifndef SAFE_DELETE
#define SAFE_DELETE(p)       { if(p) { delete (p);     (p)=NULL; } }
#endif    
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }
#endif    
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if(p) { (p)->Release(); (p)=NULL; } }
#endif
