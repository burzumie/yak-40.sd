/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Common/Macros.h $

  Last modification:
    $Date: 19.02.06 5:54 $
    $Revision: 7 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include "../api/Api.h"

/************************************************************************/
/* DLLMAIN DEBUG                                                        */
/************************************************************************/
#define DLLMAIND(FML)	double FSAPI icb_dummy(PELEMENT_ICON pelement) \
{ \
	CHK_BRT(); \
	return 0; \
} \
_CrtMemState memoryState; \
HINSTANCE g_hInstance=NULL; \
sImportTab ImportTable; \
BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) \
{ \
	_CrtMemCheckpoint(&memoryState); \
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF); \
	g_hInstance=(HINSTANCE)hModule; \
	switch(ul_reason_for_call)	{ \
		case DLL_PROCESS_ATTACH: \
		AllocConsole(); \
		DisableThreadLibraryCalls((HMODULE)hModule); \
		break; \
		case DLL_THREAD_ATTACH: \
		break; \
		case DLL_THREAD_DETACH: \
		break; \
		case DLL_PROCESS_DETACH: \
		FreeConsole(); \
		break; \
	} \
	return TRUE; \
} \
static void UpdatePanel() \
{ \
} \
static void FSAPI fnInitPanel(void) \
{ \
	\
	ImportTable.pPanels->initialize_var_by_name(&MVSDAPIEntrys	,SDAPISIGN); \
	g_pSDAPIEntrys	=(SDAPIEntrys *)MVSDAPIEntrys.var_ptr; \
} \
static void FSAPI fnDeinitPanel(void) \
{ \
	CHECK_MEM_LEAK(FML); \
}
/************************************************************************/
/* DLLMAIN                                                              */
/************************************************************************/

#define DLLMAIN()	double FSAPI icb_dummy(PELEMENT_ICON pelement) \
{ \
	CHK_BRT(); \
	return 0; \
} \
HINSTANCE g_hInstance=NULL; \
sImportTab ImportTable; \
BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) \
{ \
	g_hInstance=(HINSTANCE)hModule; \
	switch(ul_reason_for_call)	{ \
		case DLL_PROCESS_ATTACH: \
		DisableThreadLibraryCalls((HMODULE)hModule); \
		break; \
		case DLL_THREAD_ATTACH: \
		break; \
		case DLL_THREAD_DETACH: \
		break; \
		case DLL_PROCESS_DETACH: \
		break; \
	} \
	return TRUE; \
} \
static void UpdatePanel() \
{ \
} \
static void FSAPI fnInitPanel(void) \
{ \
	ImportTable.pPanels->initialize_var_by_name(&MVSDAPIEntrys	,SDAPISIGN); \
	g_pSDAPIEntrys	=(SDAPIEntrys *)MVSDAPIEntrys.var_ptr; \
} \
static void FSAPI fnDeinitPanel(void) \
{ \
}

/************************************************************************/
/* �������� ������ ������ � ������ ������ � ���� � ����� ����� �        */
/************************************************************************/
#define CHECK_MEM_LEAK(F) /**/ \
	HANDLE hfile = CreateFileA (F, \
	GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, \
	FILE_ATTRIBUTE_NORMAL, NULL); \
	_CrtSetReportMode (_CRT_WARN, _CRTDBG_MODE_FILE); \
	_CrtSetReportFile (_CRT_WARN, hfile); \
	_CrtSetReportMode (_CRT_ERROR, _CRTDBG_MODE_FILE); \
	_CrtSetReportFile (_CRT_ERROR, hfile); \
	_CrtMemDumpAllObjectsSince(&memoryState); \
	CloseHandle (hfile);
