/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Common/CommonFS.h $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include <fs_sdk_common.h>
#include <fs6/fs6.h>
#include <global/global.h>
#include <util/util.h>
#include <controls/controls.h>
#include <panels/panels.h>
#include <flight/flight.h>
#include <g2d/g2d.h>
#include <main/main.h>
#include <multiplayer/multiplayer.h>
#include <sound/sound.h>
#include <weather/weather.h>
#include <window/window.h>
#include <sim1/sim1.h>
#include <acontain/acontain.h>
#include <fs_sdk.h>

#define SDAPISIGN	"FB56C3A2"

#define GAUGE(x) extern GAUGEHDR x;

#define		CL_TRANSP								RGB(   0,   0,   0 )
#define		CL_ORANGE								RGB( 250, 193,  46 )
#define		CL_GRAY  								RGB( 229, 229, 229 )
#define 	CL_WHITE 								RGB( 255, 255, 255 )
#define 	CL_RED									RGB( 255,   0,   0 )
#define		CL_YELLOW								RGB( 255, 255,   0 )
#define		CL_BLACK								RGB(   8,   8,  18 )
#define		CL_CRT									RGB(   0,   0,  98 )
#define		CL_LCD									RGB(   8,  70,   8 )
#define		CL_AQUA									0xFFFF00
#define		CL_GREEN								RGB(   0, 255,   0 )
#define		CL_LIME									0x00FF00
#define		CL_NAVY									0x800000
#define		CL_BLUE									0xFF0000
#define		CL_FUCHSIA								0xFF00FF
#define		CL_PURPLE								0x800080
#define		CL_BLACK_CRT_BACKGROUND					RGB(   8,   8,   8 )
#define		CL_LCD_BACKGROUND						CL_LIME
#define		CL_BLU_BACKGROUND						RGB(   0,  74,  82 )
#define		CL_AMBRA								RGB( 243, 174,   7 ) 
#define		CL_SILVER								RGB( 189, 189, 189 ) 

#define		FONT_COURIER							"Courier New"
#define		FONT_QUARTZ								"Quartz"
#define		FONT_ARIAL								"Arial"
#define		FONT_HELVETICA							"Helvetica"
#define		FONT_TNR								"Times New Roman"
#define		FONT_VERDANA							"Verdana"
#define		FONT_GLASS								"Glass Gauge"

