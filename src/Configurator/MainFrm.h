//---------------------------------------------------------------------------

#ifndef MainFrmH
#define MainFrmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Dialogs.hpp>
#include <IniFiles.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
#define FS_REG_BASE     "SOFTWARE\\Microsoft\\Microsoft Games\\Flight Simulator\\9.0"

class TMainForm : public TForm
{
__published:	// IDE-managed Components
    TMainMenu *MainMenu;
    TMenuItem *Exit1;
    TStatusBar *StatusBar;
    TPanel *Panel1;
    TOpenDialog *OpenConfigDialog;
    TPanel *Panel2;
    TPanel *Panel3;
    TLabel *Label1;
    TComboBox *cbLanguage;
    TPanel *Panel4;
    TLabel *Label2;
    TCheckBox *cbxTooltipsEnable;
    TEdit *edTooltipsFile;
    TLabel *Label3;
    TSpeedButton *SpeedButton1;
    void __fastcall mnuExitClick(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall FormActivate(TObject *Sender);
private:	// User declarations
  	TCHAR sSimPath[MAX_PATH];
  	String sConfigPath;
  	String sSoundPath;
    bool ConfigFound;
    TIniFile *iniConfig;

    /// Config

    int cfgLanguage;
    bool cfgSoundEnable;
    String cfgSoundSoundPack;

    bool cfgTooltipsEnable;
    String cfgTooltipsFile;

    bool cfgNavOBSZero;
    String cfgNavOBS1Default;
    String cfgNavOBS2Default;
    String cfgNavNAV1Default;
    String cfgNavNAV2Default;
    String cfgNavXPDRDefault;
    String cfgNavADF1LDefault;
    String cfgNavADF1RDefault;
    String cfgNavADF2LDefault;
    String cfgNavADF2RDefault;

    String cfgNavCOM1Default;
    String cfgNavCOM2Default;

    double cfgHtailStep;

    double cfgAPBankLimit;
    double cfgAPAileronLimitPCT;
    double cfgAPBankCoeffP;
    double cfgAPBankCoeffI;
    double cfgAPBankCoeffD;
    double cfgAPElevatorLimitPCT;
    double cfgAPPitchCoeffP;
    double cfgAPPitchCoeffI;
    double cfgAPPitchCoeffD;
    double cfgAPAltholdCoeffP;
    double cfgAPAltholdCoeffI;
    double cfgAPAltholdCoeffD;

    double cfgFlapsStep;

    String cfgUVIDHGLDefault;
    String cfgUVIDHGRDefault;

    /// End

    void __fastcall ReadConfigIni();

public:		// User declarations
     __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
