#include <SecuredSections.h>
#include <stdio.h>
#include <Windows.h>

typedef bool (__stdcall *UninstallKeyFn)(void);

int main(int argc,char *argv[])
{
	NANOBEGIN
	int returnvalue=0;  
	HINSTANCE libInst=LoadLibrary("ArmAccess.DLL");  
	if (!libInst) return 0; /* Couldn't load library */  
	/* Substitute the typedefs above for functions other than InstallKey */  
	UninstallKeyFn UninstallKey=(UninstallKeyFn)GetProcAddress(libInst, "UninstallKey");  
/*
	if (UninstallKey==0) returnvalue=0; / * Couldn't find function * /  
	else returnvalue=UninstallKey();  
*/
	FreeLibrary(libInst); /* Not needed for the virtual ArmAccess.DLL, but it won't hurt anything. */  
	NANOEND
	return returnvalue;  
}

