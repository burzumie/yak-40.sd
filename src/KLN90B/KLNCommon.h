#pragma once

#include "../Common/CommonSys.h"
#include "../Common/CommonLogic.h"
#include "Regexp.h"

#define CALC_K(l) l.k = l.x2==l.x1 ? 7000.0f*1.852f*10e3 : (l.y2-l.y1)/(l.x2-l.x1)
#define CALC_B(l) l.b = (l.y1-l.k*l.x1)

#define TO_RAD(x)   (x)*(3.1415926535897932384626433832795f/180.0f)
#define FROM_RAD(x) (x)*(180.0f/3.1415926535897932384626433832795f)

//////////////////////////////////////////////////////////////////////////
//
// NAVDB
//
//////////////////////////////////////////////////////////////////////////

#pragma pack(1)
#pragma warning(disable:4200)

typedef struct _sub_common
{
	WORD id;
	DWORD size_of_record;
}sub_common;

typedef struct _marker
{
	sub_common header;
	BYTE heading;
	BYTE type;
	DWORD longitude;
	DWORD latitude;
	DWORD altitude;
	DWORD icao_id;
	WORD  reg_id;
	WORD  unknown1;
}marker;

typedef struct _icao_list
{
	BYTE  reg_i;
	BYTE  coun_i;
	WORD  state_i;
	WORD  city_i;
	WORD  apt_i;
	DWORD icao_id;
	DWORD unknown1;
	DWORD unknown2;
}icao_list;

typedef struct _airport
{
	sub_common header;
	BYTE  rnw_count;
	BYTE  com_count;
	BYTE  start_count;
	BYTE  approach_count;
	BYTE  apr_count_del_flag;
	BYTE  heli_count;
	DWORD longitude;
	DWORD latitude;
	DWORD altitude;
	DWORD tower_long;
	DWORD tower_lat;
	DWORD tower_alt;
	float mag_var;
	DWORD icao_id;
	DWORD unk1;
	DWORD unk2;
}airport;

typedef struct _name_sub
{
	sub_common header;
	char  str[];
}name_sub;

typedef struct _rnw_sub
{
	sub_common header;
	WORD  surface_type;
	BYTE  rnw_number;
	BYTE  rnw_designat;
	BYTE  sec_rnw_number;
	BYTE  sec_rnw_designat;
	DWORD icao_id_pri_ils;
	DWORD icao_id_sec_ils;
	DWORD longitude;
	DWORD latitude;
	DWORD elevation;
	float lenght;
	float width;
	float heading;
	float pattern_alitude;
	WORD  marking_flags;
	BYTE  light_flags;
	BYTE  pattern_flags;
}rnw_sub;
typedef struct _sub_com
{
	sub_common header;
	WORD type;
	DWORD freq;
	char  name[];
}sub_com;
typedef struct _sub_approach
{
	sub_common header;
	BYTE       suffix;
	BYTE       rnw_number;
	BYTE       type_rnwdes_gps_flag;
	BYTE       transit_count;
	BYTE       approach_leg_count;
	BYTE       missed_approach_count;
	DWORD      fix_type_ident;
	DWORD      fix_regid_icaoid_apt;
	float      altitude;
	float      heading;
	float      missed_alitude;
}sub_approach;

typedef struct _way_point
{
	sub_common header;
	BYTE       type;
	BYTE       routes_count;
	DWORD      logitude;
	DWORD      latitude;
	float      magvar;
	DWORD      icao_id;
	DWORD      reg_icaoid;
	//**************************** Optional Part
	BYTE       route_type;
	CHAR       name[8];
	DWORD      next_type_id;
	DWORD      next_reg_aptid;
	float      next_alt_min;
	DWORD      prev_type_id;
	DWORD		 prev_reg_aptid;
	float		 prev_alt_min;
}way_point;

typedef struct _bgl_header
{
	WORD bgl_id;
	WORD version;
	DWORD size_of_header;
	DWORD unknown1[3];
	DWORD section_count;
}bgl_header;

typedef struct _object_pointer
{
	DWORD section_type;
	DWORD unknown;
	DWORD sub_secs_count;
	DWORD file_offset;
	DWORD size_of_header;
}object_pointer;
typedef struct _section_header
{
	DWORD id;
	DWORD number_of_records;
	DWORD file_offset;
	DWORD size_of_subsections;
}section_header;

typedef struct _ils_vor
{
	sub_common header;
	BYTE type;
	BYTE flags;
	DWORD longitude;
	DWORD latitude;
	DWORD elevation;
	DWORD freq;
	float range;
	float mag_var;
	DWORD icao_id;
	DWORD reg_apt_icaoid;
}ils_vor;
typedef struct _ils_heading
{
	sub_common header;
	WORD unknown;
	float heading;
	float width;
}ils_heading;
typedef struct _ils_glide
{
	sub_common header;
	WORD unknown;
	DWORD longitude;
	DWORD latitude;
	DWORD elevation;
	float range;
	float pitch;
}ils_glide;

typedef struct _ilsvor_dme
{
	sub_common header;
	WORD unknown;
	DWORD longitude;
	DWORD latitude;
	DWORD elevation;
	float range;
}ilsvor_dme;

typedef struct _ndb
{
	sub_common header;
	WORD type;
	DWORD freq;
	DWORD longitude;
	DWORD latitude;
	DWORD elevation;
	float range;
	float mag_var;
	DWORD icao_id;
	DWORD reg_apt_icaoid;
}ndb;
typedef struct _names_list
{
	sub_common header;
	WORD region_names;
	WORD country_names;
	WORD state_names;
	WORD city_names;
	WORD airport_names;
	WORD icao_ids;
	DWORD region_offset;
	DWORD country_offset;
	DWORD state_offset;
	DWORD city_offset;
	DWORD airport_offset;
	DWORD icao_offset;
}names_list;

typedef struct __names_list_t
{
	char **apt_names;
	int names_count;
	char **city_names;
	int cities_count;
	char **country_names;
	int countries_count;
	char **region_names;
	int regions_count;
	icao_list *icao_list;
	int icao_ids;
}_names_list_t;

//**************** For old 2000 Bgl Style *********************************
//typedef SHORT VAR16;
//typedef INT VAR32;
typedef int SINT32;
//typedef LONG ANGL32;
typedef __int64 SINT64;
typedef GUID GUID128;

typedef struct __FS_HEADER{ // FLIGHT SIMULATOR 7.00 BGL file header
	VAR16 FL_world_set; // 00 world set number (unused)
	SINT32 FL_north_bounds; // 02 north bounds (NSEW bounds)
	SINT32 FL_south_bounds; // 06 south bound
	ANGL32 FL_east_bounds; // 10 east bound
	ANGL32 FL_west_bounds; // 14 west bound
	UINT32 FL_vor_ptr; // 18 VOR data
	VAR16 FL_vor_lo; // 22 lowest vor freq (channel 0-199)
	VAR16 FL_vor_hi; // 24 highest vor freq (108.00-117.95
	UINT32 FL_free[6]; // 26 (MUST BE 0)
	UINT32 FL_msa_data; // 50 Minimum Safe Altitude Data
	UINT32 FL_terrain_data; // 54 new terrain data
	UINT32 FL_object_ptr; // 58 OBJECT data
	UINT32 FL_library_ptr; // 62 LIBRARY data
	UINT32 FL_facilities_ptr; // 66 FACILITIES data
	UINT32 FL_free_70; // 70 (MUST BE 0)
	UINT32 FL_free_74; // 74 (MUST BE 0)
	UINT32 FL_adf_data; // 78 ADF data
	UINT32 FL_dynamic_data; // 82 DYNAMIC data
	SINT64 FL_library_min; // 86 Library id min
	SINT64 FL_library_max; // 94 Library id max
	UINT32 FL_misc_data; //102 MISC data (ground alt db)
	UINT32 FL_title; //106 TITLE AND DESCRIPTION
	UINT32 FL_magvar; //110 MAGVAR data
	UINT32 FL_exceptions; //114 EXCEPTION LIST data
	UINT32 FL_magic; //118 magic number (0x87654321)
	VAR32 FL_spare2; //122
	VAR16 FL_spare3; //126
	GUID128 FL_guid; //128 GUID
	UINT32 FL_product_id; //144 PRODUCT ID
	UINT32 FL_build_num; //148 PRODUCT BUILD NUMBER
	UINT32 FL_ptr_count; //152 data pointers count
	UINT32 FL_facility2_name_list; //156 facility2 name list
	UINT32 FL_facility2_band_list; //160 facility2 latband list
	UINT32 FL_facility2_data; //164 facility2 object list
} FILE_HEADER2;
//*********************************************************************************
typedef struct _band_set
{
	BYTE op_code;
	DWORD offset;
}band_set;
typedef struct _rel_band_set
{
	BYTE op_code;
	WORD lat_min;
	WORD lat_max;
	DWORD offset;
}rel_band_set;

typedef struct _old_ilsvor
{
	BYTE   type;     // Type ( ILS, VOR )
	BYTE   range;    // range in 2048M units
	WORD   magvar;   // magnetic variation in PseudoDegrees
	BYTE   code;     // code
	WORD   latlo;    // latitude of station in 2 Meter Units (low 16-bits)
	BYTE   lathi;    // hi 8-bits
	WORD   lonlo;    // longitude of station in 24-bit PseudoDegrees
	BYTE   lonhi;    // hi 8-bits
	WORD   alt;      // altitude of station in Meters
	WORD   heading;  // localizer direction (ANGL16) (0 for VORs)
	BYTE   ident[5]; 
	BYTE   name[24];
}old_ilsvor;

typedef struct _old_ILS_GS
{
	WORD latlo;    // latitude of station in 2 Meter Units (low 16-bits)
	BYTE lathi;    
	WORD lonlo;    // longitude of station in 24-bit PseudoDegrees
	BYTE lonhi;
	WORD alt;      // altitude of station in Meters
	WORD slope;    // Glideslope 2 * 65536 * sin (angle)
}old_ILS_GS;

typedef struct _old_ILS_GS2
{
	old_ILS_GS main_ils;
	WORD width_scaler;
}old_ILS_GS2;

typedef struct _old_dme
{
	BYTE  code;             //DME entity opcode
	DWORD FEOP_DME_MAGIC1;  //DME magic number 1
	WORD  FEOP_DME_MAGIC2;  //DME magic number 2
	WORD  FEOP_DME_MAGIC3;  // DME magic number 3
	WORD  latlo;            //latitude of station in 2 Meter Units (low 16-bits)
	BYTE  lathi;            
	WORD  lonlo;            //longitude of station in 24-bit PseudoDegrees
	BYTE  lonhi;
	WORD  alt;              //altitude of station in Meters
}old_dme;

//======== For run-time NavDB usage =============================
enum {NVDB_NDB,NVDB_VOR,NVDB_APT,NVDB_WPT,NVDB_SUP};

typedef struct _nvdb_entry
{
	int TYPE;
	void *element;
}nvdb_entry;
typedef struct _nvdb_ndb
{
	char   ICAO_ID[6];
	double Lon;
	double Lat;
	char   REGION_ID[6];
	char   APT_ID[6];
	unsigned long FREQ;
	char   NAME[23];
}nvdb_ndb;

typedef struct _nvdb_vor
{
	char          ICAO_ID[6];
	double        Lon;
	double        Lat;
	unsigned long FREQ;
	int           TYPE; 
	char          REG_ID[6];
	char          APT_ID[6];
	char          NAME[23];
	int           DME;
	double        mag_var;
}nvdb_vor;

typedef struct _K_line_t
{
	double x1;
	double x2;
	double y1;
	double y2;
	double k;
	double b;
	int    n1;
	int    n2;
	char   n1_icao[6];
	char   n2_icao[6];
	int    is_arrow;
	int    is_dto_line;
	int    numbers_only;
	char   pDes[4];
	char   sDes[4];
}K_line_t;


typedef struct _rnw_t
{
	double bLat;
	double bLon;
	double eLat;
	double eLon;
	float  len;
	BYTE   pNum;
	BYTE   pDes;
	BYTE   sNum;
	BYTE   sDes;
	float  HDG;
	BYTE   light_flag;
	BYTE   pattern_flag;
	K_line_t apt3_line;
}rnw_t;

typedef struct _freq_t
{
	DWORD freq;
	int type;
}freq_t;

typedef struct _nvdb_apt
{
	char   ICAO_ID[6];
	double Lon;
	double Lat;
	double Alt;
	char   NAME[0x48];
	char   CITY[0x48];
	char   COUNTRY[0x48];
	char   REG_ID[0x48];
	int    rnws_count;
	rnw_t  *rws;
	int    freqs_count;
	freq_t  *freqs;
	double apt3_resolution;
	vector<string> *sids;
	vector<string> *stars;
}nvdb_apt;

typedef struct _nvdb_wpt
{
	char   ICAO_ID[6];
	double Lon;
	double Lat;
	char   REG_ID[6]; 
	char   APT_ID[6];
	int    TYPE;
	int    ROUTES; 
	int    SUB_TYPE;
}nvdb_wpt;

#define USR_MAX_POINTS 250
#define MAIN_POINTS_COUNT (long)(nav_db.apts_count+nav_db.ndbs_count+nav_db.vors_count+nav_db.wpts_count+nav_db.sups_count)

typedef struct _nvdb_usr
{
	char   ICAO_ID[6];
	double Lon;
	double Lat;
	char   REG_ID[6]; 
	char   APT_ID[6];
	int    usr_type;
	int    padding[2]; 
}nvdb_usr;

typedef struct _earth_quad
{
	vector<int> navaids;
}earth_quad_t;

typedef struct _nav_db_t
{
	nvdb_ndb   *ndbs;
	LONG        ndbs_count;
	nvdb_vor   *vors;
	LONG        vors_count;
	nvdb_apt   *apts;
	LONG        apts_count;
	nvdb_wpt   *wpts;
	LONG        wpts_count;
	nvdb_wpt   *sups;
	LONG        sups_count;
	nvdb_usr   *usrs;
	LONG        usrs_count;
	earth_quad_t navaids[360][181];
}nav_db_t;

typedef struct _naviad_info
{
	int global_index;
	int type;
	int local_index;
}naviad_info;


#pragma pack()

const float lat_meters_degree = 40007000.0f/360.0f;
const float lon_meters_degree = 40075000.0f/360.0f;


void convert_id(unsigned long id_code,char *buffer);
void parse_wpt_group(int count,int offset,int len);
void parse_wpts(long offset,long sec_count);
void parse_apt_group(int count,int offset,int len);
void parse_airports(long offset,long apt_count);

void parse_vorils(long offset,long sec_count);
void parse_vorils_group(int count,int offset,int len);

void parse_ndb_group(int count,int offset,int len);
void parse_ndbs(long offset,long sec_count);

void parse_ilsvor(int type,long offset);
void parse_channel(float chanel_number,long chan_off,long main_offset);
void read_magdec(char *FS_MagDec_File);
float get_magdec(float Lon,float Lat);
void mav_db_main(char *FS_MainDir);

nvdb_wpt *get_sup_by_id(long id);
long get_sups_count(void);
long get_sup_actual_index(long j);
void insert_to_navaids(double Lon,double Lat,naviad_info *nv,char *icao_id);
void drop_from_navaids(double Lon,double Lat,naviad_info *nv,char *icao_id);
void init_usr_points(void);
int sup_create_new_point(char *icao_code);
BOOL sup_is_creating(void);

//////////////////////////////////////////////////////////////////////////
//
// Common
//
//////////////////////////////////////////////////////////////////////////

#define LONG3(d) (long)((d)*100.0f)
#define LONG2(d) (long)((d)*10.0f)
#define LONG1(d) (long)((d)*1.0f)

typedef struct _character
{
	COLORREF text_color;
	COLORREF text_background;
	CHAR     ch;
#define ATTRIBUTE_FLASH        0x80000000
#define ATTRIBUTE_INVERSE      0x40000000
#define ATTRIBUTE_ISSPACE      0x20000000
#define ATTRIBUTE_SMALL        0x10000000
#define ATTRIBUTE_BSMALL       0x04000000
#define ATTRIBUTE_TRANSPARENT  0x02000000
#define ATTRIBUTE_REDRAW       0x00000001
#define ATTRIBUTE_NORMAL       0x00000000
	ULONG    attribute;
}symbol;

enum {INPUT_ONOFF, INPUT_BTNMORE=2, INPUT_BTNLESS=4, INPUT_ROUTERPLUS=8, INPUT_ROUTERMINUS=16,
INPUT_RINNERPLUS=32, INPUT_RINNERMINUS=64,INPUT_ENTER=128,
INPUT_LINNERPLUS=256, INPUT_LINNERMINUS=512,INPUT_DTO=1024,INPUT_RCURSOR=2048,
INPUT_LOUTERMINUS=4096,INPUT_LOUTERPLUS=8192,INPUT_PULLSCAN=16384,INPUT_CLR=32768,
INPUT_LCURSOR=65536,INPUT_MSG=INPUT_LCURSOR<<1};

enum {REQUEST_LOADFPL0=1};

enum {STATE_OFF,STATE_INSELFTEST,STATE_INSELFTEST_STAGE1,STATE_DBEXPIRE,STATE_MAIN_LOOP};
enum {FORMAT_BLACK,FORMAT_TWOPARTS,FORMAT_ONEPART};

enum {SYSTEM_TIME};
enum {CHAR_UNKNOWN=-1,CHAR_ALPHA_NUM=0,DTO_CHAR=1,FROM_CHAR=2,TO_CHAR=3,NDB_CHAR=4,VOR_CHAR=5,APT_CHAR=6,MORE_CHAR=7,BLANK_CHAR = 0x20};
int char_type(char ch);

void main_loop(int ACTION);
void print_str(int row,int col,ULONG attribute,char *text,...);
char K_get_char(int row,int col);
void K_change_attribute(int row,int col,int len,ULONG attribute);
ULONG K_get_attribute(int row,int col);
void K_set_char(int row,int col,char ch);
void clear_screen(void);

enum {SCREEN_SAVE,SCREEN_RESTORE};
void sr_screen(int ACTION);
void sr_screen_to_buf(int ACTION,void *__temp_buffer);

enum {DB_OK,DB_NOTEXIST,DB_EXPIRE,DB_ERRCRITICAL};
int check_db(char *fs_base_dir);
int Create_NAVDB(char *FS_MainDir);
void Load_NAVDB(char *FS_MainDir);
int do_dbexpire(int ACTION);
void do_poweron_page(int ACTION);
char *get_date(char *f_str,int TYPE);
void init_console(void);
void update_screen(void);
char K_next_char(char ch);
char K_prev_char(char ch);
char *K_get_string(int start_row,int start_col,int end_col,char *_string);
char *K_trim_string(char *_string);

void read_magdec(char *FS_MagDec_File);
double get_magdec(double Lon,double Lat);
void K_DEBUG(char *format,...);
void K_DEBUG2(char *format,...);
void init_debug(void);
void deinit_debug(void);
void K_print_debug_buffer(void);
//*****************************************************************************
typedef struct _K_deg
{
	DWORD degrees;
	DWORD mins;
	DWORD dec_mins;
}K_deg;
void K_GetDegMin(FLOAT64 whole_degree,K_deg *deg_ptr);
double DM2DEG(double deg,double min,double dec_min);

char *K_FormName(char *src,char *dst,int count);
BOOL is_bgl_compressed(char *file_name);
double get_US(void);
LONG get_TK(void);
FLOAT64 get_TK2(void);
LONG get_GS(double divisor);
double get_HDG(void);
double get_IPU(double w);
double get_W(void);
double get_MAGVAR(void);
double get_TCRS(void);
void get_PPOS(double *latitude,double *longitude);
double get_S(double src_lat,double src_long,double dst_lat,double dst_long);
double get_ZPU(double src_lat,double src_long,double dst_lat,double dst_long,double mag_var);

int inc_nav_page(int curr_page);
int dec_nav_page(int curr_page);
void do_nav_page(int ACTION,int page_type);
int do_ndb_page(int ACTION);
int do_vor_page(int ACTION);
int do_apt_page(int ACTION);
int do_wpt_page(int ACTION);
int do_sup_page(int ACTION);

enum {NAV_PAGE,NDB_PAGE,VOR_PAGE,APT_PAGE,WPT_PAGE,SUP_PAGE,DT_PAGE,ACT_PAGE,CALC_PAGE,DTO_PAGE=1000,MSG_PAGE=1001};
enum {FPL_PAGE=1,SET_PAGE=2,OTH_PAGE=3,MOD_PAGE=4};

enum {PAGE_LEFT,PAGE_RIGHT};
enum {ACTION_INIT=-10,ACTION_TIMER=-9,MULTI_LINE_SELECTED=-8,ACTION_FREE_RESOURCES=-7,
ACTION_FPLDTO_DONE=-6,PAGE_FORCE_CHANGE=-5,ACTION_SHOW=-4,ACTION_HIDE=-3,ACTION_SUP_PT_CREATED=-2};

enum {STRING_T,INT_T,DOUBLE_T};
void K_GetParam(char *main_str,char *sub_str,int TYPE,void *target_buffer,char beg='[',char end=']');
unsigned long icao2id(char *buffer);
void UnLoad_NAVDB(void);

void mem_init(void);
void mem_deinit(void);
void *K_malloc(DWORD bytes);
void K_free(void *ptr);

BOOL is_ndb_id(long ind,long *local_index);
BOOL is_vor_id(long ind,long *local_index);
BOOL is_apt_id(long ind,long *local_index);
BOOL is_wpt_id(long ind,long *local_index);
BOOL is_sup_id(long ind,long *local_index);


BOOL K_is_scanpull(void);
void build_nearest_list(void);

int ndb_compare(nvdb_ndb *ndb1,nvdb_ndb *ndb2);
int vor_compare(nvdb_vor *vor1,nvdb_vor *vor2);
int apt_compare(nvdb_apt *apt1,nvdb_apt *apt2);

int do_dto_page(int ACTION,DWORD extra);

int get_lp_type(void);
int get_rp_type(void);
void switch_lp(int page_type,int extra);
void switch_rp(int page_type,int extra);

nvdb_ndb *get_current_ndb(void);
nvdb_vor *get_current_vor(void);
nvdb_apt *get_current_apt(void);
nvdb_wpt *get_current_wpt(void);
nvdb_wpt *get_current_sup(void);
enum {NAVAID_MIN=0,NAVAID_NDB=0,NAVAID_VOR=1,NAVAID_APT=2,NAVAID_WPT=3,NAVAID_SUP=4,NAVAID_MAX=4,NAVAID_NULL=5,NAVAID_SID_MAIN=6,NAVAID_STAR_MAIN=7,NAVAID_SS_POINT};
//===================== NAVPAGES ===========================
void set_navpage_number(int page_pos,int page_number);
int get_navpage_number(int page_pos);
//===================== For Navigation =====================
#pragma pack(1)
typedef struct _navaid_common_header
{
	char   ICAO_ID[6];
	double Lon;
	double Lat;
}nv_hdr_t;

typedef struct _curr_point
{
	int type;
	void *buffer_spec;
	FLOAT64 abs_dis;
	FLOAT64 dis_from_beg;
	FLOAT64 dis_from_curr;
	FLOAT64 dtk;
	char alt_name[20];
}nv_point_t;

enum {DTO_LEG,FPL_LEG,FPLDTO_LEG,UNKNOWN_LEG};
void set_route_leg(nv_point_t *src_point,nv_point_t *dst_point,int leg_type);

void fpl_set_navigation(void);
void fpl_continue_navigation(void);
int fpl_get_number_upt(void *buffer_spec);
int fpl_get_nulls_before(int fpl_number,int stop_point);

typedef struct _rt_leg_t
{
	double Lon_src;
	double Lon_dst;
	double Lat_src;
	double Lat_dst;
	char   *dst_icao_id;
	char   *src_icao_id;
	double DIS;
	double ETE;
	double BRG;
	double LBU;
	double DTK;
	double OD;
	double LEN;
	BOOL FT;
	nv_point_t *srcp;
	nv_point_t *dstp;
	int leg_crs;
}rt_leg_t;
#pragma pack()
rt_leg_t *get_rt_leg(void);
void calulate_navdata(void);
//============================================================
void set_ml_mode(int page);
//============================================================
LONG round_deg(double deg);
//================= Input Handlers here ======================
BOOL fpl_handle_key(int INPUT_MASK);
BOOL nav_handle_key(int INPUT_MASK);
BOOL nav_handle2_key(int INPUT_MASK);
BOOL dto_handle_key(int INPUT_MASK);
BOOL oth_handle_key(int INPUT_MASK);

BOOL ndb_handle_key(int INPUT_MASK);
BOOL vor_handle_key(int INPUT_MASK);
BOOL apt_handle_key(int INPUT_MASK);
BOOL int_handle_key(int INPUT_MASK);
BOOL sup_handle_key(int INPUT_MASK);
BOOL dt_handle_key(int INPUT_MASK);
BOOL act_handle_key(int INPUT_MASK);
//=================== FPL ====================================
int do_fpl_pages(int ACTION);
//============================================================
void print_ndb(nvdb_ndb *__ndb,char *status);
void print_vor(nvdb_vor *__vor,char *status);
void print_apt(nvdb_apt *__apt,char *status);
void apt_print_apt(nvdb_apt *__apt,char *status,int apt_page=1,int sub_page=1);
void apt_prev_page_ext(int main_page,int sub_page,int *p_main_page,int *p_sub_page,nvdb_apt *__apt);
void apt_next_page_ext(int main_page,int sub_page,int *p_main_page,int *p_sub_page,nvdb_apt *__apt);
void print_wpt(nvdb_wpt *__wpt,char *status);
//=============================================================
void block_all(int page);
void do_block(int ACTION);
void unblock_all(void);
//=============================================================
void fpl_set_next_leg(void);
//=============================================================
inline double __add_deg(double ang,double addition)
{
	ang+=addition; if(ang>360.0f) return(ang-360.0f); if(ang<0.0f) return(360.0f+ang); return(ang);
}
inline double __add_deg180(double ang,double addition)
{
	ang+=addition; if(ang>180.0f) return(ang-360.0f); if(ang<-180.0f) return(360.0f+ang); return(ang);
}
//===============================================================
void update_sim_vars(void);
void init_sim_vars(void);
//================================================================
enum {H_U,H_D,V_L,V_R};

typedef struct _K_npoint_t
{
	double x;
	double y;
	int type;
	char ICAO_ID[6];
	int pt_type;
}K_npoint_t;

enum {NAV5_MODE_TK,NAV5_MODE_DTK,NAV5_MODE_HDG,NAV5_MODE_TN};
typedef struct _K_nav_box_t
{
	K_line_t box_lines[4];
	double start_x;
	double start_y;
	double stop_x;
	double stop_y;
	int curr_nav5_mode;
}K_nav_box_t;

typedef struct _K_nav_lines_t
{
	K_line_t rt_legs[30];
	K_line_t rnws[30];
	K_npoint_t points[30];
	int points_count;
	int legs_count;
	int rnws_count;
	BOOL is_r_menu;
	BOOL is_dto_list;
	BOOL is_msg;
}K_nav_lines_t;
typedef struct K_point_t
{
	double x;
	double y;
}K_point_t;

const int max_points = 30;
typedef struct _fpl_t
{
	nv_point_t points[max_points+1];
	int point_in_use;
}fpl_t;
int fpl_get_leg(void);
void fpl_set_curr_leg(void);
BOOL is_dto(void);
void set_kln90_dir(char *dir_name);
int get_kln90_dir(char *buffer);
void set_fs_dir(char *dir_name);
int get_fs_dir(char *buffer);
char *GetFsMainDir(void);
int nav_mode(void);
nv_hdr_t *get_dtofpl(void);
void fpl_set_fpldto_index(int index);
nv_point_t *fpl_get_dtofpl(void);
void fpl_set_fpldto(void);
void init_navigation(void);

void set_fpl0_from_af(void);

nv_hdr_t *get_navaid_buffer(long local_index,int type);
void activate_fpl0(fpl_t *__fpl,BOOL inverse);
void copy_fpl(fpl_t *dst,fpl_t *src);
void Save_FPL(fpl_t *__fpl,int f_number);
//FLOAT64 fpl_get_next_leg_dtk(FLOAT64 *,FLOAT64 *);
FLOAT64 fpl_get_next_leg_dtk(FLOAT64 *__lbu,FLOAT64 *__dis,rt_leg_t *__rt);
BOOL fpl_get_actual_track(rt_leg_t *__rt);
FLOAT64 fpl_get_active_leg_dtk(void);

const double INVALID_ANGLE = -1000000.55f;
LONG K_ftol(double arg);

fpl_t *fpl_GetCurrentFPL(int *fpl_number_ptr);
enum {POINT_NONE,DONE_POINT,FROM_POINT,TO_POINT,WILL_POINT,FPL_POINT,AFPL_POINT};
nv_point_t *get_RowPointType(int row,int *point_type);
int do_dt_pages(int ACTION);
int do_act_page(int ACTION);
void calculate_fpls(void);

nv_point_t *fpl_get_active(int *index);
nv_point_t *fpl_get_last(int *index);
nv_point_t *fpl_get_next(int *index);
void check_super(void);
BOOL get_CDI(FLOAT64 *xtk,FLOAT64 *scale);

enum {SUPER_NAV1,SUPER_NAV5,SUPER_NONE};
int get_super_type(void);
BOOL get_FT(void);
void get_LL(FLOAT64 Lat_src,FLOAT64 Lon_src,FLOAT64 S,FLOAT64 OD,FLOAT64 *dLat,FLOAT64 *dLon);
double get_LBU(double src_lat,double src_long,double dst_lat,double dst_long,double craft_lat,double craft_long,double *delta,double *SS);
double get_LBU2(double src_lat,double src_long,double dst_lat,double dst_long,double craft_lat,double craft_long,double *delta,double *SS);
int fpl_get_legs(fpl_t *__fpl);
BOOL get_leg(fpl_t *__fpl,rt_leg_t *rt_leg,int leg_number);
BOOL IsBlocked(void);
int fpl_get_active_leg(void);

enum {ADD_STATUS_MESSAGE,ENABLE_ENT,DISABLE_ENT,ENABLE_FMES,DISABLE_FMES,ENABLE_RENT,DISABLE_RENT};
enum {NO_ACTION,NO_MORE_MESSAGES};
int do_status_line(int ACTION,char *str);
BOOL K_get_rp_point(nv_point_t *pt);
enum {DTO_MODE_NORMAL,DTO_MODE_FPL};
nv_point_t *dto_get_cp(void);
void dto_change_mode(int MODE);
void nav_set_mode(int MODE);
BOOL fpl_is_in_fpl(nv_hdr_t *__nv);
BOOL fpl_get_active_LL(double *Lat,double *Lon);
BOOL fpl_get_next_LL(double *Lat,double *Lon);
int fpl_get_real_points(int fpl);
//enum {VOR_TERMINAL=1,VOR_LOW=2,VOR_HIGH=3};

typedef struct _dto_list_t
{
	nv_point_t points[max_points+1];
	int list_count;
	int list_index;
}dto_list_t;
void dto_update_list(void);
void dto_init_list(void);
void dto_list_next(void);
void dto_list_prev(void);

enum {LEG_MODE,OBS_MODE,OBS_CANCEL_MODE};
int do_mod_page(int ACTION);
double cdi_get_scale(void);
BOOL mod_handle_key(int INPUT_MASK);
int mod_get_mode(void);
int mod_get_obs(void);


int do_set_page(int ACTION);
BOOL set_handle_key(int INPUT_MASK);
#define GET_SIM_VAL(v1,v2) do { v2=v1.var_value.n; }while(0);

typedef struct _K_dite_time_t
{
	int year;
	int month;
	int day;
	int hour;
	int min;
	int sec;
}K_dite_time_t;
void get_DT(K_dite_time_t *__dt);
void get_NOW_plus_this(K_dite_time_t *add);
K_dite_time_t *sub_DT(K_dite_time_t *newer,K_dite_time_t *later);
K_dite_time_t *calc_flying_time(int time_type);

BOOL set_is0_active(void);
int set_get_rml(void);
FLOAT64 mod_get_scale(void);
int do_msg_page(int ACTION);
void msg_add_message(char *message);
typedef struct _saved_pages_t
{
	int lp;
	int rp;
}saved_pages_t;
void set_s5_message(BOOL s5_msg);
enum {FRONT_PAGE,REGULAR_PAGE};
int lp_kind(void);
void dto_set(nv_point_t *__pt);

void obs_next_val(void);
void obs_prev_val(void);
void mod_set_obs_value(FLOAT64 new_vlaue,rt_leg_t *__rt);
long *find_ids_by_icao(char *icao_id_s,long *array_size);
//void get_icao_by_id(long point_id,char *__ICAO_ID);
void get_icao_by_id(long point_id,char *__ICAO_ID,nv_point_t *point=NULL);
nv_point_t *show_active_point(long point_id);
void fpl_build_ml_list(int count,long *list);
void show_nv_point(nv_point_t *__pt,BOOL save_screen=TRUE);
void print_navaid_arrow(void *buffer_spec);
int get_arrow_attr(void);
BOOL on_the_ground(void);

#define CALC_K(l) l.k = l.x2==l.x1 ? 7000.0f*1.852f*10e3 : (l.y2-l.y1)/(l.x2-l.x1)
#define CALC_B(l) l.b = (l.y1-l.k*l.x1)

char *get_RNW_des(int des);
void calc_log_line(double sLat,double sLon,double dLat,double dLon,double cLat,double cLon,K_line_t *result_line,double north=0,double mag_var=0);
BOOL do_draw_rnws(void);
int find_of(int *array,int value,int max_values);

void K_save_dword_param(char *section,char *attribute,DWORD value);
BOOL K_load_dword_param(char *section,char *attribute,DWORD *value);
void K_save_string_param(char *section,char *attribute,const TCHAR *value);
BOOL K_load_string_param(char *section,char *attribute,TCHAR *value);

long add_usr_point(nv_point_t *new_point);
void usr_del_point(long index);
long usr_get_pt_count(void);
nvdb_usr *usr_get_point(long index,int type=0);
long usr_spec_points(int nav_type);
nvdb_usr *usr_wpt_by_id(int nav_type,int index);
nvdb_usr *usr_wpt_by_id2(int nav_type,int index);
long usr_index_by_id(int nav_type,int index);
void act_get_act_point(nv_point_t *nvpt);
char *act_nvtype2text(int nv_type);
int do_oth_page(int ACTION);
void sup_nvpt_deleted(nvdb_wpt *delete_point);

void show_input_dlg(void);
void input_config_init(void);
void input_config_clear(void);

enum {MSG_KMSG=1};
int get_msg_stat(void);
double get_GPS_altitude(void);
void insert_into_fpl(fpl_t *__fpl,nv_point_t *nv_point,int index);

//////////////////////////////////////////////////////////////////////////
//
// Nearest
//
//////////////////////////////////////////////////////////////////////////

#define MAX_IN_LIST 256

typedef struct _nr_ndb
{
	unsigned long index;
	double   S;
	double   radial;
}nr_ndb;

typedef struct _nr_ndb_list
{
	nr_ndb list[MAX_IN_LIST];
	long nr_ndb_count;
}nr_ndb_list;

typedef struct _nr_vor
{
	unsigned long index;
	double   S;
	double   radial;
}nr_vor;

typedef struct _nr_vor_list
{
	nr_vor list[MAX_IN_LIST];
	long nr_vor_count;
}nr_vor_list;

typedef struct _nr_apt
{
	unsigned long index;
	double   S;
	double   radial;
}nr_apt;

typedef struct _nr_apt_list
{
	nr_apt list[MAX_IN_LIST];
	long nr_apt_count;
}nr_apt_list;

//////////////////////////////////////////////////////////////////////////
//
// SID STAR
//
//////////////////////////////////////////////////////////////////////////

#define SID_STAR_FILENAME "KLN_sid_star"
void ss_load_on_demand(nvdb_apt *__apt);
void ss_load_sid_rnws(nvdb_apt *__apt,char *sid_name);
void ss_load_star_rnws(nvdb_apt *__apt,char *star_name);
void ss_load_sid_points(nvdb_apt *__apt,char *sid_name,char *rnw_name);
void ss_load_star_points(nvdb_apt *__apt,char *star_name,char *rnw_name);
void ss_reload(nvdb_apt *__apt);
nv_hdr_t *get_sid_point(int index);
nv_hdr_t *get_star_point(int index);

//////////////////////////////////////////////////////////////////////////
//
// VNAV
//
//////////////////////////////////////////////////////////////////////////

int get_IND_altitude(void);
int get_SEL_altitude(void);
void set_SEL_altitude(int __alt);
int get_SEL_dis(void);
void set_SEL_dis(int __dis);
BOOL vnav_posable(void);
char *vnav_get_wpt(void);
void start_vnav_calc(void);
double get_vnav_angle(void);
void update_vnav(void);
double vnav_get_angle(void);
void vnav_complete_edit(void);
int vnav_get_state(void);
int vnav_get_ADV(void);
void vnav_set_cur_ang(void);
void vnav_set_ang(double __ang);
double vnav_in_time(void);
enum {VNAV_INACTIVE,VNAV_ARMED,VNAV_IN10,VNAV_WORKS};

//////////////////////////////////////////////////////////////////////////
//
// Main
//
//////////////////////////////////////////////////////////////////////////

typedef struct _K_draw_t
{
	int x;
	int y;
	HFONT h_font;
	HFONT h_font_s;
	HPEN  h_pen_b;
	HPEN  h_pen_t;
	HPEN  h_pen_t_s;
	HBRUSH h_brush_t;
	HBRUSH h_brush_b;
	HDC    hdc;
	COLORREF text_color;
	COLORREF text_background;
	int X;
	int Y;
	int X_s;
	int Y_s;
}K_draw_t;

typedef struct _K_string
{
	int row;
	HDC hdc;
	COLORREF text_color;
	COLORREF text_background;
	int XX;
	int X;
	int Y;
}K_string;
__inline FLOAT64 ANGL48_TO_FLOAT64 (const ANGL48 angl48)
{
	return (FLOAT64)(SINT64)(angl48.i64 & ~0xFFFF) * (1/4294967296.0);
}
__inline FLOAT64 ANGL48_TO_DEGREES (const ANGL48 angle)
{
	return ANGL48_TO_FLOAT64(angle) * (360.0 / (65536.0 * 65536.0));
}
__inline ANGL48 FLOAT64_TO_ANGL48 (const FLOAT64 angle)
{
	ANGL48 r;
	r.i64 = (SINT64)(angle * 4294967296.0);
	return r;
}

__inline ANGL48 DEGREES_TO_ANGL48 (const FLOAT64 angle)
{
	return FLOAT64_TO_ANGL48(angle * ((65536.0 * 65536.0) / 360.0));
}

__inline FLOAT64 LAT_METERS48_TO_RADIANS (SIF48 sif48Lat)
{
	return (FLOAT64)(SINT64)(sif48Lat.i64 & ~0xFFFF) * ((1.0 / (40007000.0/(3.14159265358*2))) * (1/4294967296.0));
}

__inline SIF48 FLOAT64_TO_SIF48 (FLOAT64 number)
{
	SIF48 r;
	r.i64 = (SINT64)(number * 4294967296.0);
	return r;
}
__inline SIF48 LAT_RADIANS_TO_METERS48 (const FLOAT64 radians)
{
	return FLOAT64_TO_SIF48(radians / ((1.0 / (40007000.0/(3.14159265358*2)))));
}
__inline FLOAT64 RADIANS_TO_DEGREES (const FLOAT64 radians)
{
	return radians*((double)180.0/ (double)3.14159265358);
}
__inline FLOAT64 DEGREES_TO_RADIANS (const FLOAT64 degrees)
{
	return degrees/((double)180.0/ (double)3.14159265358);
}
__inline FLOAT64 ALT_DEGREES_TO_MET(const FLOAT64 degrees)
{
	return degrees*(40005000.0 / 360);
}
__inline FLOAT64 ALT_MET_TO_DEGREES(const FLOAT64 met)
{
	return met/(40005000.0 / 360);
}
void do_input_event(long event);
void check_for_buttons(void);

int do_calc_page(int ACTION);
BOOL calc_handle_key(int INPUT_MASK);
