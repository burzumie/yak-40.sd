#include "KLNCommon.h"

using namespace std;

nav_db_t nav_db={0};


#define RETURN_DBCHEK(n) switch(n){ case DBPART_NOTEXIST: return(DB_NOTEXIST); case DBPART_EXPIRE: return(DB_EXPIRE);   }


char ndbs_file[MAX_PATH];
char vors_file[MAX_PATH];
char wpts_file[MAX_PATH];
char sups_file[MAX_PATH];
char apts_file[MAX_PATH];

enum {DBPART_NOTEXIST,DBPART_EXPIRE,DBPART_OK};

int check_dbpart_expiration(char *db_part_name)
{
   HANDLE file_hand = CreateFile(db_part_name,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
   if(INVALID_HANDLE_VALUE == file_hand)
	   return(DBPART_NOTEXIST);
   FILETIME ft_ct,ft_lat,ft_lwt;
   GetFileTime(file_hand,&ft_ct,&ft_lat,&ft_lwt);
   CloseHandle(file_hand);

   SYSTEMTIME stUTC, stLocal;
   FileTimeToSystemTime(&ft_lwt, &stUTC);
   SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);
   WORD file_month,file_year;
   file_month = stLocal.wMonth;
   file_year  = stLocal.wYear;
   GetSystemTime(&stUTC);
   SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);
   return(file_month == stLocal.wMonth && file_year == stLocal.wYear?DBPART_OK:DBPART_EXPIRE);
}
int check_db(char *fs_base_dir)
{
   char dir_and_name[MAX_PATH];
   WIN32_FIND_DATA FileData;
   sprintf(dir_and_name,"%s",g_pSDAPI->m_NavBasePath);
   set_kln90_dir(dir_and_name);
   HANDLE h_ourdir = FindFirstFile(dir_and_name,&FileData);
   if(h_ourdir == INVALID_HANDLE_VALUE)
   {
	   if(!CreateDirectory(dir_and_name,NULL))
		   return(DB_ERRCRITICAL);
	   FileData.dwFileAttributes |= FILE_ATTRIBUTE_DIRECTORY;
   }   
   else
       FindClose(h_ourdir);

   if( FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
   {
	   sprintf(ndbs_file,"%sNDB.DAT",g_pSDAPI->m_NavBasePath);
	   sprintf(vors_file,"%sVOR.DAT",g_pSDAPI->m_NavBasePath);
	   sprintf(wpts_file,"%sWPT.DAT",g_pSDAPI->m_NavBasePath);
	   sprintf(sups_file,"%sSUP.DAT",g_pSDAPI->m_NavBasePath);
	   sprintf(apts_file,"%sAPT.DAT",g_pSDAPI->m_NavBasePath);
       RETURN_DBCHEK(check_dbpart_expiration(ndbs_file));
	   RETURN_DBCHEK(check_dbpart_expiration(vors_file));
	   RETURN_DBCHEK(check_dbpart_expiration(apts_file));
	   RETURN_DBCHEK(check_dbpart_expiration(wpts_file));
	   RETURN_DBCHEK(check_dbpart_expiration(sups_file));
	   return(DB_OK);
   } 
   return(DB_ERRCRITICAL);
}

map<unsigned long,vector<int> >icao_index;

int Create_NAVDB(char *FS_MainDir)
{
	mav_db_main(FS_MainDir);
	return(0);
}

int navaid_type(int element_number,long *local_index)
{
	if(is_ndb_id(element_number,local_index))
		return(NAVAID_NDB);
	if(is_vor_id(element_number,local_index))
		return(NAVAID_VOR);
	if(is_apt_id(element_number,local_index))
		return(NAVAID_APT);
	if(is_wpt_id(element_number,local_index))
		return(NAVAID_WPT);
	return NAVAID_NULL;
}
inline BOOL is_ndb_equal(int ind1,int ind2)
{
	return(!ndb_compare(&nav_db.ndbs[ind1],&nav_db.ndbs[ind2]));
}
inline BOOL is_vor_equal(int ind1,int ind2)
{
   return(!vor_compare(&nav_db.vors[ind1],&nav_db.vors[ind2]));
}
inline int is_apt_equal(int ind1,int ind2)
{
	return(apt_compare(&nav_db.apts[ind1],&nav_db.apts[ind2]));
}
BOOL is_wpt_equal(int local_index,int curr_local_ind)
{
   return(FALSE);
}

inline BOOL is_in_navaid(double Lat,double Lon,naviad_info *nv_info)
{
	if( (nv_info->type==NAVAID_WPT) || (nv_info->type==NAVAID_SUP) )
		return(FALSE);
	int row = Lon<0?(DWORD)(360+Lon):(DWORD)(Lon);
	int col = (DWORD)(90+Lat);

	for(vector<int>::iterator it = nav_db.navaids[row][col].navaids.begin();
		it!=nav_db.navaids[row][col].navaids.end();
		it++
		)
	{
	  int ind = *it;
	  int curr_local_ind;

	  if(  nv_info->type 
		   == 
		   navaid_type(ind,(long *)&curr_local_ind) 
		 )
	  {
		  switch(nv_info->type)
		   {
		   case NAVAID_NDB:
			   {
			      if(is_ndb_equal(nv_info->local_index,curr_local_ind))
				     return(TRUE);
			   }
			   break;
		   case NAVAID_VOR:
			   {
			      if(is_vor_equal(nv_info->local_index,curr_local_ind))
				     return(TRUE);
			   }
			   break;
		   case NAVAID_APT:
			   {
			      int ret = is_apt_equal(nv_info->local_index,curr_local_ind);
				  if(ret!=1)
				  {
				     if(ret==2 || ret==4)
					 {
					    nvdb_apt temp_apt;
						memcpy(&temp_apt,&nav_db.apts[curr_local_ind],sizeof(nvdb_apt));
						memcpy(&nav_db.apts[curr_local_ind],&nav_db.apts[nv_info->local_index],sizeof(nvdb_apt));
						memcpy(&nav_db.apts[nv_info->local_index],&temp_apt,sizeof(nvdb_apt));
					 }
					 return(TRUE);
				  }
			   }
				  //if(is_apt_equal(nv_info->local_index,curr_local_ind))
				  //   return(TRUE);
			   break;
//		   case NAVAID_WPT:
//			   {
//			      if(is_wpt_equal(nv_info->local_index,curr_local_ind))
//				     return(TRUE);
//			   }
			   break;
		   }
	  }
	}
	return(FALSE);
}
//================= Fill elements to coord ===========================================
void insert_to_navaids(double Lon,double Lat,naviad_info *nv,char *icao_id)
{
	int row = Lon<0?(DWORD)(360+Lon):(DWORD)(Lon);
	int col = (DWORD)(90+Lat);
    if( (nv->type==-1) || (is_in_navaid(Lat,Lon,nv)==FALSE) )
	{
       nav_db.navaids[row][col].navaids.push_back(nv->global_index);
	   icao_index[icao2id(icao_id)].push_back(nv->global_index);
	}
}

void drop_from_navaids(double Lon,double Lat,naviad_info *nv,char *icao_id)
{
	int row = Lon<0?(DWORD)(360+Lon):(DWORD)(Lon);
	int col = (DWORD)(90+Lat);

	for(vector<int>::iterator it = nav_db.navaids[row][col].navaids.begin();it!=nav_db.navaids[row][col].navaids.end();it++)
	{
		if(nv->global_index == *it)
		{
			nav_db.navaids[row][col].navaids.erase(it);
			break;
		}
	}
	for(vector<int>::iterator it = icao_index[icao2id(icao_id)].begin();it!=icao_index[icao2id(icao_id)].end();it++)
	{
		if(nv->global_index == *it)
		{
			icao_index[icao2id(icao_id)].erase(it);
			break;
		}
	}
	if(icao_index[icao2id(icao_id)].empty())
	{
		map<unsigned long,vector<int> >::iterator it;
        it = icao_index.find(icao2id(icao_id));
		icao_index.erase(it);
		//icao_index.erase(icao2id(icao_id));
	}
	//nav_db.navaids[row][col].navaids.push_back(nv->global_index);
	//icao_index[icao2id(icao_id)].push_back(nv->global_index);
}

vector<int> *get_from_navaids(double Lon,double Lat)
{
	int row = Lon<0?(DWORD)(360+Lon):(DWORD)(Lon);
	int col = (DWORD)(90+Lat);
    return(&(nav_db.navaids[row][col].navaids));
}
//================= Sorting ===========================================================
int sort_ndbs(const void *arg1,const void *arg2)
{
    return(_stricmp(((nvdb_ndb *)arg1)->ICAO_ID,((nvdb_ndb *)arg2)->ICAO_ID));
}
int sort_vors(const void *arg1,const void *arg2)
{
    return(_stricmp(((nvdb_vor *)arg1)->ICAO_ID,((nvdb_vor *)arg2)->ICAO_ID));
}
int sort_apts(const void *arg1,const void *arg2)
{
	return(_stricmp(((nvdb_apt *)arg1)->ICAO_ID,((nvdb_apt *)arg2)->ICAO_ID));
}
int sort_wpts(const void *arg1,const void *arg2)
{
    return(_stricmp(((nvdb_wpt *)arg1)->ICAO_ID,((nvdb_wpt *)arg2)->ICAO_ID));
}
int sort_rnws(const void *arg1,const void *arg2)
{
	double res = ((rnw_t *)arg2)->len - ((rnw_t *)arg1)->len;
	if(res<0)
		return(-1);
	if(res>0)
		return(1);
	return(0);
}
//================= Nav DB Loading ====================================================
LONG rec_count(FILE *fp)
{
	char number[9]={'0'};
	fseek(fp,-9,SEEK_END);
	fread(number,sizeof(number)-sizeof(char),1,fp);
    number[8]='\0';
	LONG ret;
	sscanf(number,"%X",&ret);
	return(ret);
}

void Load_NDBS(FILE *ndbs_fp,long ndbs_count)
{
    char buffer[MAX_PATH];
	char name[MAX_PATH];
	nvdb_ndb *__ndbs;
	__ndbs = (nvdb_ndb *)K_malloc(sizeof(nvdb_ndb)*ndbs_count);
	nav_db.ndbs = __ndbs;
	fseek(ndbs_fp,0,SEEK_SET);
	for(int i=0;i<ndbs_count;i++)
	{
		
		fgets(buffer,sizeof(buffer)-1,ndbs_fp);
		/*
		sscanf(buffer,"Lon=[%f] Lat=[%f] ICAO_ID=[%s] REGION_ID=[%s] APT_ID=[%s] FREQ=[%lu] NAME=[%s]",
			          &__ndbs[i].Lon,&__ndbs[i].Lat,__ndbs[i].ICAO_ID,__ndbs[i].REGION_ID,
			          __ndbs[i].APT_ID,&__ndbs[i].FREQ,name);
        */
		K_GetParam(buffer,"Lon=",DOUBLE_T,&__ndbs[i].Lon);
		K_GetParam(buffer,"Lat=",DOUBLE_T,&__ndbs[i].Lat);
		K_GetParam(buffer,"ICAO_ID=",STRING_T,__ndbs[i].ICAO_ID);
   		K_GetParam(buffer,"REGION_ID=",STRING_T,__ndbs[i].REGION_ID);
		K_GetParam(buffer,"APT_ID=",STRING_T,__ndbs[i].APT_ID);
		K_GetParam(buffer,"FREQ=",INT_T,&__ndbs[i].FREQ);
        K_GetParam(buffer,"NAME=",STRING_T,name);
		name[22]='\0';
		strcpy(__ndbs[i].NAME,name);
	}
    qsort(__ndbs,ndbs_count,sizeof(nvdb_ndb),sort_ndbs);    
}
void Load_VORS(FILE *vors_fp,long vors_count)
{
    char buffer[MAX_PATH];
	char name[MAX_PATH];
	char S_DME[4];
	nvdb_vor *__vors;
	__vors = (nvdb_vor *)K_malloc(sizeof(nvdb_vor)*vors_count);
	nav_db.vors = __vors;

	fseek(vors_fp,0,SEEK_SET);
	for(int i=0;i<vors_count;i++)
	{
		fgets(buffer,sizeof(buffer)-1,vors_fp);

		/*
		sscanf(buffer,"Lon=[%f] Lat=[%f] FREQ=[%lu] TYPE=[%d] ICAO_ID=[%s] REG_ID=[%s] APT_ID=[%s] NAME=[%s] DME=[%s]",
                      &__vors[i].Lon,&__vors[i].Lat,&__vors[i].FREQ,&__vors[i].TYPE,
					  __vors[i].ICAO_ID,__vors[i].REG_ID,__vors[i].APT_ID,name,S_DME);
		*/
		K_GetParam(buffer,"Lon=",DOUBLE_T,&__vors[i].Lon);
		K_GetParam(buffer,"Lat=",DOUBLE_T,&__vors[i].Lat);
		K_GetParam(buffer,"FREQ=",INT_T,&__vors[i].FREQ);
		K_GetParam(buffer,"MV=",DOUBLE_T,&__vors[i].mag_var);
		K_GetParam(buffer,"TYPE=",INT_T,&__vors[i].TYPE);
		K_GetParam(buffer,"ICAO_ID=",STRING_T,__vors[i].ICAO_ID);
		K_GetParam(buffer,"REG_ID=",STRING_T,__vors[i].REG_ID);
		K_GetParam(buffer,"APT_ID=",STRING_T,__vors[i].APT_ID);
        K_GetParam(buffer,"NAME=",STRING_T,name);
		K_GetParam(buffer,"DME=",STRING_T,S_DME);

		name[22]='\0';
        strcpy(__vors[i].NAME,name);
		__vors[i].DME = _stricmp(S_DME,"YES")?0:1;
	}
	qsort(__vors,vors_count,sizeof(nvdb_vor),sort_vors);
}
char buffer[1024*1024];

void Load_APTS(FILE *apts_fp,long apts_count)
{ 
	nvdb_apt *__apts;
	__apts = (nvdb_apt *)K_malloc(sizeof(nvdb_apt)*apts_count);
	nav_db.apts = __apts;

	fseek(apts_fp,0,SEEK_SET);
	for(int i=0;i<apts_count;i++)
	{
		fgets(buffer,sizeof(buffer)-1,apts_fp);

		/*
		sscanf(buffer,"Lon=[%f] Lat=[%f] Alt=[%f] ICAO_ID=[%s] NAME=[%s] CITY=[%s] COUNTRY=[%s]",
                      &__apts[i].Lon,&__apts[i].Lat,&__apts[i].Alt,__apts[i].ICAO_ID,
                       __apts[i].NAME,__apts[i].CITY,__apts[i].COUNTRY);
		*/
		K_GetParam(buffer,"Lon=",DOUBLE_T,&__apts[i].Lon);
		K_GetParam(buffer,"Lat=",DOUBLE_T,&__apts[i].Lat);
        K_GetParam(buffer,"Alt=",DOUBLE_T,&__apts[i].Alt);
        K_GetParam(buffer,"ICAO_ID=",STRING_T,__apts[i].ICAO_ID);
        K_GetParam(buffer,"NAME=",STRING_T,__apts[i].NAME);
        K_GetParam(buffer,"CITY=",STRING_T,__apts[i].CITY);
        K_GetParam(buffer,"COUNTRY=",STRING_T,__apts[i].COUNTRY);
		strcpy(__apts[i].REG_ID,__apts[i].ICAO_ID);
		__apts[i].REG_ID[2]='\0';
		__apts[i].NAME[22]='\0';

		fgets(buffer,sizeof(buffer)-1,apts_fp);
		K_GetParam(buffer,"RNWS=",INT_T,&__apts[i].rnws_count);
		K_GetParam(buffer,"COMS=",INT_T,&__apts[i].freqs_count);
        
		__apts[i].rws = (rnw_t *)K_malloc(sizeof(rnw_t)*__apts[i].rnws_count);
		__apts[i].freqs = (freq_t *)K_malloc(sizeof(freq_t)*__apts[i].freqs_count);

		double max_res = 0;
		for(int r=0;r<__apts[i].rnws_count;r++)
		{
		   char par[MAX_PATH];
           char rnw_data[2*MAX_PATH];
		   double _temp_d;
		   INT   _temp_i;

		   sprintf(par,"RNW%d=",r+1);
		   K_GetParam(buffer,par,STRING_T,rnw_data);

		   K_GetParam(rnw_data,"rLat=",DOUBLE_T,&__apts[i].rws[r].bLat,'{','}');
		   K_GetParam(rnw_data,"rLon=",DOUBLE_T,&__apts[i].rws[r].bLon,'{','}');
		   K_GetParam(rnw_data,"HDG=",DOUBLE_T,&_temp_d,'{','}');
		   __apts[i].rws[r].HDG = _temp_d;
		   K_GetParam(rnw_data,"LEN=",DOUBLE_T,&_temp_d,'{','}');
		   __apts[i].rws[r].len = _temp_d;

		   K_GetParam(rnw_data,"LF=",INT_T,&_temp_i,'{','}');
		   __apts[i].rws[r].light_flag = (BYTE)(_temp_i&31);
		   K_GetParam(rnw_data,"PF=",INT_T,&_temp_i,'{','}');
		   __apts[i].rws[r].pattern_flag = (BYTE)_temp_i;

		   K_GetParam(rnw_data,"PN=",INT_T,&_temp_i,'{','}');
		   __apts[i].rws[r].pNum = _temp_i;
		   K_GetParam(rnw_data,"PD=",INT_T,&_temp_i,'{','}');
		   __apts[i].rws[r].pDes = _temp_i;
		   K_GetParam(rnw_data,"SN=",INT_T,&_temp_i,'{','}');
		   __apts[i].rws[r].sNum = _temp_i;
		   K_GetParam(rnw_data,"SD=",INT_T,&_temp_i,'{','}');
		   __apts[i].rws[r].sDes = _temp_i;
		   get_LL(__apts[i].rws[r].bLat,__apts[i].rws[r].bLon,__apts[i].rws[r].len/1000.0f/2,__apts[i].rws[r].HDG,&__apts[i].rws[r].eLat,&__apts[i].rws[r].eLon);
		   double __bLat,__bLon;
		   get_LL(__apts[i].rws[r].bLat,__apts[i].rws[r].bLon,__apts[i].rws[r].len/1000.0f/2,
			      __add_deg(__apts[i].rws[r].HDG,180.0f),&__bLat,&__bLon);
		   __apts[i].rws[r].bLat = __bLat; __apts[i].rws[r].bLon = __bLon;

		   calc_log_line(__apts[i].rws[r].bLat,__apts[i].rws[r].bLon,__apts[i].rws[r].eLat,__apts[i].rws[r].eLon,__apts[i].Lat,__apts[i].Lon,&__apts[i].rws[r].apt3_line);

		   _snprintf(__apts[i].rws[r].apt3_line.pDes,sizeof(__apts[i].rws[r].apt3_line.pDes)-1,
			   "%02d%s",(int)__apts[i].rws[r].pNum,get_RNW_des(__apts[i].rws[r].pDes));

		   _snprintf(__apts[i].rws[r].apt3_line.sDes,sizeof(__apts[i].rws[r].apt3_line.sDes)-1,
			   "%02d%s",(int)__apts[i].rws[r].sNum,get_RNW_des(__apts[i].rws[r].sDes));
		   K_line_t *ln = &__apts[i].rws[r].apt3_line;

		   if(fabs(ln->x1) > max_res) max_res=fabs(ln->x1);
		   if(fabs(ln->x2) > max_res) max_res=fabs(ln->x2);
		   if(fabs(ln->y1) > max_res) max_res=fabs(ln->y1);
		   if(fabs(ln->y2) > max_res) max_res=fabs(ln->y2);
		   //K_DEBUG("ICAO=[%s] x1=[%f] y1=[%f] x2=[%f] y2=[%f] max_res=[%f]\n",__apts[i].ICAO_ID,ln->x1,ln->y1,ln->x2,ln->y2,2.0f*max_res);  
		}	
		__apts[i].apt3_resolution = 2.0f*max_res;
		qsort(__apts[i].rws,__apts[i].rnws_count,sizeof(rnw_t),sort_rnws);
        
		for(int f=0;f<__apts[i].freqs_count;f++)
		{
		   char par[MAX_PATH];
		   char com_data[2*MAX_PATH];

		   sprintf(par,"COM%d=",f+1);
		   K_GetParam(buffer,par,STRING_T,com_data);

 		   K_GetParam(com_data,"TYPE=",INT_T,&__apts[i].freqs[f].type,'{','}');
		   K_GetParam(com_data,"FREQ=",INT_T,&__apts[i].freqs[f].freq,'{','}');		   
		}
		__apts[i].sids = NULL;
		__apts[i].stars = NULL;
	}
	qsort(__apts,apts_count,sizeof(nvdb_apt),sort_apts);
}

void Load_WPTS(FILE *wpts_fp,long wpts_count)
{
	char buffer[MAX_PATH];
	char sub_type[4];
	nvdb_wpt *__wpts;
    
	DWORD is_ext_int=0;
	K_load_dword_param("KLN90B","EXTINTDB",&is_ext_int);
	if(is_ext_int)
	{
	   char file_name[MAX_PATH];
	   char kln_dir[MAX_PATH];
	   vector<nvdb_wpt > ints;
	   nvdb_wpt temp_wpt;
	   
	   get_kln90_dir(kln_dir);
	   sprintf(file_name,"%s\\%s",kln_dir,"isec.txt");
	   FILE *int_file = fopen(file_name,"rb");
	   if(!int_file)
	   {
	      nav_db.wpts_count=0;
		  return;
	   }
	   while(1)
	   {
	      fgets(buffer,sizeof(buffer)-1,int_file);
		  if(buffer[0]==';') continue;
		  if(feof(int_file))
			  break;
          memset(&temp_wpt,0,sizeof(temp_wpt));
		  int str_row=0;
		  char temp_str[MAX_PATH];
		  int tstr_ind;
          //========================== ICAO ID ==============================
		  while(buffer[str_row]==0x20 || buffer[str_row]==0x09)
			  str_row++;
		  tstr_ind=0;
		  while( buffer[str_row]!=0x20 && buffer[str_row]!=0x09 )
			  temp_str[tstr_ind++] = buffer[str_row++];
		  temp_str[tstr_ind]='\0';
		  strcpy(temp_wpt.ICAO_ID,temp_str);
          //========================== Latitude ==============================
		  while(buffer[str_row]==0x20 || buffer[str_row]==0x09)
		  {
		     if(buffer[str_row]=='\n' || buffer[str_row]=='\r')
			    break;
		     str_row++;
		  }
		  if(buffer[str_row]=='\n' || buffer[str_row]=='\r')
		     continue;
		  tstr_ind=0;
		  while( buffer[str_row]!=0x20 && buffer[str_row]!=0x09 )
		  {
			  if(buffer[str_row]=='\n' || buffer[str_row]=='\r')
				  break;
			  temp_str[tstr_ind++] = buffer[str_row++];
		  }
		  if(buffer[str_row]=='\n' || buffer[str_row]=='\r')
			  continue;
		  temp_str[tstr_ind]='\0';
		  temp_wpt.Lat = atof(temp_str);
          //========================== Longitude ==============================
		  while(buffer[str_row]==0x20 || buffer[str_row]==0x09)
		  {
		     if(buffer[str_row]=='\n' || buffer[str_row]=='\r')
			    break;
		     str_row++;
		  }
		  if(buffer[str_row]=='\n' || buffer[str_row]=='\r')
			  continue;
		  tstr_ind=0;
		  while( buffer[str_row]!=0x20 && buffer[str_row]!=0x09 )
		  {
	         if(buffer[str_row]=='\n' || buffer[str_row]=='\r')
			    break;
		     temp_str[tstr_ind++] = buffer[str_row++];
		  }
		  if(buffer[str_row]=='\n' || buffer[str_row]=='\r')
			  continue;
		  temp_str[tstr_ind]='\0';
		  temp_wpt.Lon = atof(temp_str);
		  ints.push_back(temp_wpt);
	   }
	   nav_db.wpts_count = ints.size();
	   nav_db.wpts = (nvdb_wpt *)K_malloc(sizeof(nvdb_wpt)*nav_db.wpts_count);
	   int wpt_number=0;
	   for(vector<nvdb_wpt>::iterator it = ints.begin();it!=ints.end();it++)
	   {
          nav_db.wpts[wpt_number++] = *it;
	   }
	   ints.clear();
	}
	else
	{
	   __wpts = (nvdb_wpt *)K_malloc(sizeof(nvdb_wpt)*wpts_count);
       nav_db.wpts = __wpts;
	   fseek(wpts_fp,0,SEEK_SET);
	   for(int i=0;i<wpts_count;i++)
	   {
	      fgets(buffer,sizeof(buffer)-1,wpts_fp);
          K_GetParam(buffer,"Lon=",DOUBLE_T,&__wpts[i].Lon); 
          K_GetParam(buffer,"Lat=",DOUBLE_T,&__wpts[i].Lat);
          K_GetParam(buffer,"ICAO_ID=",STRING_T,__wpts[i].ICAO_ID); 
          K_GetParam(buffer,"REG_ID=",STRING_T,__wpts[i].REG_ID);
          K_GetParam(buffer,"APT_ID=",STRING_T,__wpts[i].APT_ID); 
          K_GetParam(buffer,"TYPE=",INT_T,&__wpts[i].TYPE);
          K_GetParam(buffer,"ROUTES=",INT_T,&__wpts[i].ROUTES); 
          K_GetParam(buffer,"SUB_TYPE=",STRING_T,sub_type);
		  if     (_stricmp(sub_type,"WPT"))
		           __wpts[i].SUB_TYPE=1;
		  else if(_stricmp(sub_type,"MRK"))
		        __wpts[i].SUB_TYPE=2;
		  else
                __wpts[i].SUB_TYPE=0;
	   }   
	}
    qsort(nav_db.wpts,wpts_count,sizeof(nvdb_wpt),sort_wpts);
}

void Load_SUPS(FILE *sups_fp,long sups_count)
{
    char buffer[MAX_PATH];
	nvdb_wpt *__wpts;
	__wpts = (nvdb_wpt *)K_malloc(sizeof(nvdb_wpt)*sups_count);
    nav_db.sups = __wpts;

	fseek(sups_fp,0,SEEK_SET);
	for(int i=0;i<sups_count;i++)
	{
		fgets(buffer,sizeof(buffer)-1,sups_fp);
        K_GetParam(buffer,"Lon=",DOUBLE_T,&__wpts[i].Lon); 
        K_GetParam(buffer,"Lat=",DOUBLE_T,&__wpts[i].Lat);
        K_GetParam(buffer,"ICAO_ID=",STRING_T,__wpts[i].ICAO_ID); 
        K_GetParam(buffer,"REG_ID=",STRING_T,__wpts[i].REG_ID);
        K_GetParam(buffer,"APT_ID=",STRING_T,__wpts[i].APT_ID); 
        K_GetParam(buffer,"TYPE=",INT_T,&__wpts[i].TYPE);
        K_GetParam(buffer,"ROUTES=",INT_T,&__wpts[i].ROUTES); 
        __wpts[i].SUB_TYPE=0;
	}
	qsort(__wpts,sups_count,sizeof(nvdb_wpt),sort_wpts);
}

void Load_USRS(char *usr_file_name,long usrs_count)
{
	char buffer[MAX_PATH];
	char kln_dir[MAX_PATH];
	char usrs_file[MAX_PATH];
	char ponit_name[MAX_PATH];
	get_kln90_dir(kln_dir);
	sprintf(usrs_file,"%s\\users.dat",kln_dir);

	nvdb_usr *__usrs;
	__usrs = (nvdb_usr *)K_malloc(sizeof(nvdb_wpt)*usrs_count);
	nav_db.usrs = __usrs;
	nav_db.usrs_count = USR_MAX_POINTS;

	for(int i=0;i<usrs_count;i++)
	{
       sprintf(ponit_name,"Point.%d",i);
	   DWORD bytes = GetPrivateProfileString("User Points",ponit_name,"NULL",buffer,sizeof(buffer)-1,usrs_file);
	   if(!bytes || !strcmp(buffer,"NULL"))
	   {
	      __usrs[i].ICAO_ID[0]='\0';
	      __usrs[i].usr_type=-1;
		  continue;
	   }
	   K_GetParam(buffer,"Lat=",DOUBLE_T,&__usrs[i].Lat);
	   K_GetParam(buffer,"Lon=",DOUBLE_T,&__usrs[i].Lon);
	   K_GetParam(buffer,"ICAO_ID=",STRING_T,&__usrs[i].ICAO_ID);
	   K_GetParam(buffer,"USRTYPE=",INT_T,&__usrs[i].usr_type);
	}
	//qsort(__wpts,sups_count,sizeof(nvdb_wpt),sort_wpts);
	init_usr_points();
}

BOOL is_ndb_id(long ind,long *local_index)
{
	if(ind < nav_db.ndbs_count)
	{
	    if(local_index) *local_index = ind;
		return(TRUE);
	}
	return(FALSE);
}
BOOL is_vor_id(long ind,long *local_index)
{
	if(ind < (nav_db.ndbs_count+nav_db.vors_count) && (ind>=nav_db.ndbs_count))
	{
	    if(local_index) 
			*local_index = ind - nav_db.ndbs_count;
		return(TRUE);
	}
	return(FALSE);
}
BOOL is_apt_id(long ind,long *local_index)
{
	if(ind < (nav_db.ndbs_count+nav_db.vors_count+nav_db.apts_count) && (ind>=nav_db.ndbs_count+nav_db.vors_count))
	{
	    if(local_index) 
			*local_index = ind - nav_db.ndbs_count - nav_db.vors_count;
		return(TRUE);
	}
	return(FALSE);
}

BOOL is_wpt_id(long ind,long *local_index)
{
	if( ind < (nav_db.ndbs_count+nav_db.vors_count+nav_db.apts_count+nav_db.wpts_count) 
		&&
	   ( ind>=nav_db.ndbs_count+nav_db.vors_count+nav_db.apts_count )	
	   )
	{
	    if(local_index) 
			*local_index = ind - nav_db.ndbs_count - nav_db.vors_count - nav_db.apts_count;
		return(TRUE);
	}
	return(FALSE);
}

BOOL is_sup_id(long ind,long *local_index)
{
	long sup_beg_ind = nav_db.ndbs_count+nav_db.vors_count+nav_db.apts_count+nav_db.wpts_count;
	if(ind >= sup_beg_ind)
	{
	    if(ind < (sup_beg_ind+nav_db.sups_count))
		{
		   if(local_index) 
		      *local_index = ind - sup_beg_ind;
		   return(TRUE);
		}
		else
		{
		   long usr_index = ind - (sup_beg_ind+nav_db.sups_count);
		   if(local_index) 
			   *local_index = -(usr_index+1);
		   return(TRUE);
		}
	}

	return(FALSE);
}
nv_hdr_t *get_navaid_buffer(long local_index,int type)
{
    if(type==NAVAID_NDB)
	{
		if(local_index < nav_db.ndbs_count && local_index>=0)
			return((nv_hdr_t *)&nav_db.ndbs[local_index]);
	}
    if(type==NAVAID_VOR)
	{
		if(local_index < nav_db.vors_count && local_index>=0)
			return((nv_hdr_t *)&nav_db.vors[local_index]);
	}
    if(type==NAVAID_APT)
	{
		if(local_index < nav_db.apts_count && local_index>=0)
			return((nv_hdr_t *)&nav_db.apts[local_index]);
	}
    if(type==NAVAID_WPT)
	{
		if(local_index < nav_db.wpts_count && local_index>=0)
			return((nv_hdr_t *)&nav_db.wpts[local_index]);
	}
    if(type==NAVAID_SUP)
	{
		if(local_index < nav_db.sups_count && local_index>=0)
			return((nv_hdr_t *)&nav_db.sups[local_index]);
	}
	return(NULL);
}
void Build_Navaids(void)
{
   int i;
   naviad_info nv;
   for(i=0;i<nav_db.ndbs_count;i++)
   {
	   nv.global_index=i;
	   nv.local_index=i;
	   nv.type = NAVAID_NDB;
	   insert_to_navaids(nav_db.ndbs[i].Lon,nav_db.ndbs[i].Lat,&nv,nav_db.ndbs[i].ICAO_ID);
   }
   for(i=0;i<nav_db.vors_count;i++)
   {
	   nv.global_index=i+nav_db.ndbs_count;
	   nv.local_index=i;
	   nv.type = NAVAID_VOR;
	   insert_to_navaids(nav_db.vors[i].Lon,nav_db.vors[i].Lat,&nv,nav_db.vors[i].ICAO_ID);
   }
   for(i=0;i<nav_db.apts_count;i++)
   {
	   nv.global_index=i+nav_db.ndbs_count+nav_db.vors_count;
	   nv.local_index=i;
	   nv.type = NAVAID_APT;
//	   if(icao2id(nav_db.apts[i].ICAO_ID) == 0x036a0740)
//		   K_DEBUG("ULLI\n");
	   insert_to_navaids(nav_db.apts[i].Lon,nav_db.apts[i].Lat,
	                     &nv,
						 nav_db.apts[i].ICAO_ID);
   }
   for(i=0;i<nav_db.wpts_count;i++)
   {
	   nv.global_index=i+nav_db.ndbs_count+nav_db.vors_count+nav_db.apts_count;
	   nv.local_index=i;
	   nv.type = NAVAID_WPT;
	   insert_to_navaids(nav_db.wpts[i].Lon,nav_db.wpts[i].Lat,
	                     &nv,
						 nav_db.wpts[i].ICAO_ID);
   }
   for(i=0;i<nav_db.sups_count;i++)
   {
	   nv.global_index=i+nav_db.ndbs_count+nav_db.vors_count+nav_db.apts_count+nav_db.wpts_count;
	   nv.local_index=i;
	   nv.type = NAVAID_SUP;
	   insert_to_navaids(nav_db.sups[i].Lon,nav_db.sups[i].Lat,
		   &nv,
		   nav_db.sups[i].ICAO_ID);
   }
   for(i=0;i<nav_db.usrs_count;i++)
   {
	   long usr_ind_beg = nav_db.ndbs_count+nav_db.vors_count+nav_db.apts_count+nav_db.wpts_count+nav_db.sups_count;
	   nv.global_index=i+usr_ind_beg;
	   nv.local_index=-1;
	   nv.type = nav_db.usrs[i].usr_type;
	   if(nav_db.usrs[i].ICAO_ID[0])
	      insert_to_navaids(nav_db.usrs[i].Lon,nav_db.usrs[i].Lat,&nv,nav_db.usrs[i].ICAO_ID);
   }
}
void Load_NAVDB(char *FS_MainDir)
{
	memset(&nav_db,0,sizeof(nav_db));	
	
	FILE *ndbs_fp = fopen(ndbs_file,"rb");
	if(ndbs_fp)
	   nav_db.ndbs_count = rec_count(ndbs_fp);

	FILE *vors_fp = fopen(vors_file,"rb");
	if(vors_fp)
	   nav_db.vors_count = rec_count(vors_fp);

    FILE *apts_fp = fopen(apts_file,"rb");
	if(apts_fp)
	   nav_db.apts_count = rec_count(apts_fp);

	FILE *wpts_fp = fopen(wpts_file,"rb");
	if(wpts_fp)
	   nav_db.wpts_count = rec_count(wpts_fp);

	FILE *sups_fp = fopen(sups_file,"rb");
	if(sups_fp)
	   nav_db.sups_count = rec_count(sups_fp);

	Load_NDBS(ndbs_fp,nav_db.ndbs_count);
	Load_VORS(vors_fp,nav_db.vors_count);
	Load_APTS(apts_fp,nav_db.apts_count);
	Load_WPTS(wpts_fp,nav_db.wpts_count);
	Load_SUPS(sups_fp,nav_db.sups_count);
    Load_USRS("users.dat",USR_MAX_POINTS);
	//qsort(nav_db.nvdb_whole,nav_db.whole_count,sizeof(nvdb_entry),sort_whole_db);

	Build_Navaids();

    if(ndbs_fp) fclose(ndbs_fp);
	if(wpts_fp) fclose(wpts_fp);
	if(sups_fp) fclose(sups_fp);
	if(apts_fp) fclose(apts_fp);
	if(vors_fp) fclose(vors_fp);

    char FS_MagVar_File[MAX_PATH];
	sprintf(FS_MagVar_File,"%sScenery\\Base\\Scenery\\magdec.bgl",FS_MainDir);
	read_magdec(FS_MagVar_File);

	do_fpl_pages(ACTION_INIT);
	do_apt_page(ACTION_INIT);
}
#define FREE_NVDB(p,c) if(p){K_free(p);p=NULL;c=0;}
void UnLoad_NAVDB(void)
{
	for(int i=0;i<nav_db.apts_count;i++)
	{
		if(nav_db.apts[i].rws) K_free(nav_db.apts[i].rws);
		if(nav_db.apts[i].freqs) K_free(nav_db.apts[i].freqs);
		if(nav_db.apts[i].sids)
		{
		   (nav_db.apts[i].sids)->clear();
		   delete nav_db.apts[i].sids;

		}
		if(nav_db.apts[i].stars)
		{
		   (nav_db.apts[i].stars)->clear();
		   delete nav_db.apts[i].stars;
		}
	}
	FREE_NVDB(nav_db.apts,nav_db.apts_count);
	FREE_NVDB(nav_db.wpts,nav_db.wpts_count);
	FREE_NVDB(nav_db.vors,nav_db.vors_count);
	FREE_NVDB(nav_db.ndbs,nav_db.ndbs_count);
	FREE_NVDB(nav_db.sups,nav_db.sups_count);
	FREE_NVDB(nav_db.usrs,nav_db.usrs_count);

	for(map<unsigned long,vector<int> >::iterator it = icao_index.begin();it!=icao_index.end();++it)
	{
		long aaa = it->first;
		vector<int> v = it->second;
		v.clear();
	}
	icao_index.clear();

    for(int i=0;i<360;i++)
       for(int j=0;j<181;j++)
           nav_db.navaids[i][j].navaids.clear();
	do_ndb_page(ACTION_FREE_RESOURCES);
	do_vor_page(ACTION_FREE_RESOURCES);
	do_apt_page(ACTION_FREE_RESOURCES);
	do_wpt_page(ACTION_FREE_RESOURCES);
	do_sup_page(ACTION_FREE_RESOURCES);
	do_fpl_pages(ACTION_FREE_RESOURCES);
	do_nav_page(ACTION_FREE_RESOURCES,0);
}
#define BGLID           0x0001
#define ENDOFDATA       "End of Data "

typedef unsigned char   _byte;
typedef unsigned short  _word;
typedef signed short    _int;

typedef _int             pseudodegree_t;
typedef	pseudodegree_t  magdectable_t[360][181];

typedef	struct _tag_magdechdr_t
{
  _int   nBglID;         // BGL file ID: 0x0001	
  _byte  unknown1[108];  // all zero
  _int   nHdrSize;       // Header size? 0x0080
  _byte  unknown2[16];   // all zero
  _word  nRowNum;        // number of rows (longitudes) in table: 360
  _word  nColNum;        // number of columns (latitudes) intable: 181 (incl Equator)
  _byte  bDay;           // date day? (01)
  _byte  bMonth;         // date month? (01)
  _byte  bYear;          // date year? (93)
  _byte  bCentury;       // date century? (19)
} magdechdr_t;

#pragma pack(1)
typedef struct _tag_magdeclbgl_t
{
  magdechdr_t   hdr;
  magdectable_t mdt;
  char          eofdata[12];
}magdecbgl_t;
#pragma pack()
magdecbgl_t mag_dec_file;

float f_magdectable[360][181];

void read_magdec(char *FS_MagDec_File)
{
   FILE *fp = fopen(FS_MagDec_File,"rb");
   fread(&mag_dec_file,sizeof(mag_dec_file),1,fp);
   for(int meredian=0;meredian<360;meredian++)
   {
	   for(int parallel=0;parallel<=180;parallel++)
	   {
		   f_magdectable[meredian][parallel]
		   = 
		   (float)mag_dec_file.mdt[meredian][parallel] / 65536.0f * 360.0f;
	   }
   }
   fclose(fp);
}

double get_magdec(double Lon,double Lat)
{
	if(Lat>74.0f || Lat<-60.0f)
		return(0.0f);
	
	int row2 = Lon < 0 ? ceil(360+Lon):ceil(Lon);
	int row1 = Lon < 0 ? floor(360+Lon):floor(Lon);
	int col2 = ceil(90+Lat);
	int col1 = floor(90+Lat);

	if(row1 < 0 ) row1 = 359; if(row1 > 359 ) row1 = 0;
	if(row2 < 0 ) row2 = 359; if(row2 > 359 ) row2 = 0;

	if(col1 < 0 ) col1 = 180; if(col1 > 180 ) col1 = 0; 
	if(col2 < 0 ) col2 = 180; if(col2 > 180 ) col2 = 0; 

	double K4 = f_magdectable[row1][col2];
	double K1 = f_magdectable[row1][col1];
	double K3 = f_magdectable[row2][col2];
	double K2 = f_magdectable[row2][col1];
	int m=(Lat-(DWORD)(Lat))*10;
	int n=(Lon-(DWORD)(Lon))*10;
    double K = m*((K3-K2)/10.0f) + K1 + n*((K3-K1)/10.0f);
	return(K);
}