/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "Main.h"
#include "KLN90B.h"

double FSAPI icb_IcoIn(PELEMENT_ICON pelement) 
{
	CHK_BRT();
	return KLN_GET(KLN_PULL_SCAN)?POS_GET(POS_PANEL_STATE):-1; 
} 

double FSAPI icb_IcoOut(PELEMENT_ICON pelement) 
{
	CHK_BRT();
	return !KLN_GET(KLN_PULL_SCAN)?POS_GET(POS_PANEL_STATE):-1; 
} 

BOOL FSAPI PowerOnOff(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI BrightPlus(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI BrightMinus(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI RKnobPlus(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI RKnobMinus(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI EnterButton(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI LKnobPlus(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI LKnobMinus(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI DButton(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI RCursor(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI LCursor(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI PullScan(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI CLR(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI K_MSG(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI LKnobOuterLeft (PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI LKnobOuterRight(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI LKnobInnerLeft (PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI LKnobInnerRight(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI RKnobOuterLeft (PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI RKnobOuterRight(PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI RKnobInnerLeft (PPIXPOINT pPoint, FLAGS32 flags);
BOOL FSAPI RKnobInnerRight(PPIXPOINT pPoint, FLAGS32 flags);

static MAKE_MSCB(mcb_kln_pwr		,KLN_TGL(KLN_PWR				); PowerOnOff(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_brtl		,KLN_DEC(KLN_BRT_LEFT			); BrightMinus(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_brtr		,KLN_INC(KLN_BRT_LEFT			); BrightPlus(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_crsrl		,KLN_TGL(KLN_LEFT_CRSR			); LCursor(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_crsrr		,KLN_TGL(KLN_RIGHT_CRSR			); RCursor(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_loutl		,KLN_DEC(KLN_LEFT_OUTER_LEFT	); LKnobOuterLeft(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_loutr		,KLN_DEC(KLN_LEFT_OUTER_RIGHT	); LKnobOuterRight(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_linnl		,KLN_DEC(KLN_LEFT_INNER_LEFT	); LKnobInnerLeft(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_linnr		,KLN_DEC(KLN_LEFT_INNER_RIGHT	); LKnobInnerRight(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_routl		,KLN_DEC(KLN_RIGHT_OUTER_LEFT	); RKnobOuterLeft(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_routr		,KLN_DEC(KLN_RIGHT_OUTER_RIGHT	); RKnobOuterRight(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_rinnl		,KLN_DEC(KLN_RIGHT_INNER_LEFT	); RKnobInnerLeft(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_rinnr		,KLN_DEC(KLN_RIGHT_INNER_RIGHT	); RKnobInnerRight(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_pulls		,KLN_TGL(KLN_PULL_SCAN 			); PullScan(relative_point,mouse_flags); )
static MAKE_MSCB(mcb_kln_msg  		,KLN_TGL(KLN_MESSAGE   			); K_MSG(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_alt  		,KLN_TGL(KLN_ALTITUDE  			))
static MAKE_MSCB(mcb_kln_dto  		,KLN_TGL(KLN_DIRECT_TO 			); DButton(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_clr  		,KLN_TGL(KLN_CLEAR   			); CLR(relative_point,mouse_flags);)
static MAKE_MSCB(mcb_kln_ent  		,KLN_TGL(KLN_ENTER   			); EnterButton(relative_point,mouse_flags);)

static MAKE_TCB(tcb_01	,KLN_TT(KLN_PWR					))
static MAKE_TCB(tcb_02	,KLN_TT(KLN_BRT_LEFT			))
static MAKE_TCB(tcb_03	,KLN_TT(KLN_BRT_RIGHT			))
static MAKE_TCB(tcb_04	,KLN_TT(KLN_LEFT_CRSR			))
static MAKE_TCB(tcb_05	,KLN_TT(KLN_RIGHT_CRSR			))
static MAKE_TCB(tcb_06	,KLN_TT(KLN_LEFT_OUTER_LEFT		))
static MAKE_TCB(tcb_07	,KLN_TT(KLN_LEFT_OUTER_RIGHT	))
static MAKE_TCB(tcb_08	,KLN_TT(KLN_LEFT_INNER_LEFT		))
static MAKE_TCB(tcb_09	,KLN_TT(KLN_LEFT_INNER_RIGHT	))
static MAKE_TCB(tcb_10	,KLN_TT(KLN_RIGHT_OUTER_LEFT	))
static MAKE_TCB(tcb_11	,KLN_TT(KLN_RIGHT_OUTER_RIGHT	))
static MAKE_TCB(tcb_12	,KLN_TT(KLN_RIGHT_INNER_LEFT	))
static MAKE_TCB(tcb_13	,KLN_TT(KLN_RIGHT_INNER_RIGHT	))
static MAKE_TCB(tcb_14	,KLN_TT(KLN_PULL_SCAN        	))
static MAKE_TCB(tcb_15	,KLN_TT(KLN_MESSAGE          	))
static MAKE_TCB(tcb_16	,KLN_TT(KLN_ALTITUDE         	))
static MAKE_TCB(tcb_17	,KLN_TT(KLN_DIRECT_TO        	))
static MAKE_TCB(tcb_18	,KLN_TT(KLN_CLEAR            	))
static MAKE_TCB(tcb_19	,KLN_TT(KLN_ENTER            	))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_kln,HELP_NONE,0,0) 
MOUSE_TBOX(  "2",  400,   0, 20, 60,CURSOR_DOWNARROW,MOUSE_DLR ,mcb_kln_brtl) 
MOUSE_TBOX(  "3",  465,   0, 20, 60,CURSOR_UPARROW	,MOUSE_DLR ,mcb_kln_brtr) 
MOUSE_TBOX(  "6",    0, 100, 24, 50,CURSOR_DOWNARROW,MOUSE_DLR ,mcb_kln_loutl) 
MOUSE_TBOX(  "7",  110, 100, 24, 50,CURSOR_UPARROW	,MOUSE_DLR ,mcb_kln_loutr) 
MOUSE_TSHB(  "8",   24, 100, 86, 50,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_kln_linnl,mcb_kln_linnr) 
MOUSE_TBOX( "10",  395, 100, 24, 65,CURSOR_DOWNARROW,MOUSE_DLR ,mcb_kln_routl) 
MOUSE_TBOX( "11",  495, 100, 24, 65,CURSOR_UPARROW	,MOUSE_DLR ,mcb_kln_routr) 
MOUSE_TBOX( "12",  420, 100, 15, 65,CURSOR_DOWNARROW,MOUSE_DLR ,mcb_kln_rinnl) 
MOUSE_TBOX( "13",  483, 100, 15, 65,CURSOR_UPARROW	,MOUSE_DLR ,mcb_kln_rinnr) 

MOUSE_TBOX(  "1",  420,   0, 45, 60,CURSOR_HAND,MOUSE_LR,mcb_kln_pwr)
MOUSE_TBOX(  "4",   39,  55, 59, 23,CURSOR_HAND,MOUSE_LR,mcb_kln_crsrl)
MOUSE_TBOX(  "5",  425,  55, 59, 23,CURSOR_HAND,MOUSE_LR,mcb_kln_crsrr)
MOUSE_TBOX( "14",  442, 110, 32, 32,CURSOR_HAND,MOUSE_LR,mcb_kln_pulls)
MOUSE_TBOX( "15",  135, 136, 22, 10,CURSOR_HAND,MOUSE_LR,mcb_kln_msg)
MOUSE_TBOX( "16",  187, 136, 22, 10,CURSOR_HAND,MOUSE_LR,mcb_kln_alt)
MOUSE_TBOX( "17",  229, 136, 22, 10,CURSOR_HAND,MOUSE_LR,mcb_kln_dto)
MOUSE_TBOX( "18",  284, 136, 22, 10,CURSOR_HAND,MOUSE_LR,mcb_kln_clr)
MOUSE_TBOX( "19",  337, 136, 22, 10,CURSOR_HAND,MOUSE_LR,mcb_kln_ent)

MOUSE_END 

extern long kln90b_win_id;

void FSAPI kln_cb( PGAUGEHDR pgauge, SINT32 service_id, UINT32 extra_data ) 
{
	switch(service_id) {
		case PANEL_SERVICE_CONNECT_TO_WINDOW:
		break;
		case PANEL_SERVICE_PRE_INSTALL: {
			kln90b_win_id = pgauge->reserved3 ? *(long *)pgauge->reserved3:-1;
		}break;
		case PANEL_SERVICE_POST_INSTALL: {
			SAFE_NEW(g_pKLN90B,SDKLN90B());
			assert(g_pKLN90B);
			g_pKLN90B->Init();
		}break;
		case PANEL_SERVICE_PRE_KILL: {
			SAFE_DELETE(g_pKLN90B);
		}break;
		case PANEL_SERVICE_PRE_UPDATE:
			g_pKLN90B->Update();
		break;
		case PANEL_SERVICE_POST_DRAW: {
			PELEMENT_STATIC_IMAGE pelement=(PELEMENT_STATIC_IMAGE)(pgauge->elements_list[0]->next_element[0]->next_element[0]);

			assert(pelement);

			g_pKLN90B->Draw(pelement);

			//SET_OFF_SCREEN(pelement);
		}break;
	}
}

#define CRT_LEFT_BORDER 137
#define CRT_TOP_BORDER  23

extern PELEMENT_HEADER kln_list; 
GAUGE_HEADER_FS700_EX(KLN90B_BACKGROUND_SX, "kln", &kln_list, rect_kln, kln_cb, 0, 0, 0, kln); 

//MY_ICON2	(klnKnobIn			,KLN90B_BTN_IN_D			,NULL               	,  340,	   80,	icb_IcoIn 	,PANEL_LIGHT_MAX	, kln) 
//MY_ICON2	(klnKnobOut			,KLN90B_BTN_OUT_D			,&l_klnKnobIn			,  340,	   80,	icb_IcoOut	,PANEL_LIGHT_MAX	, kln) 
MY_STATIC2F (klncrt				,KLN90B_BCK_DISPLAY   		,/*&l_klnKnobOut*/NULL	, CRT_LEFT_BORDER, CRT_TOP_BORDER,	IMAGE_USE_ERASE|IMAGE_USE_BRIGHT|IMAGE_CREATE_DIBSECTION, kln)
MY_ICON2	(klnIco				,KLN90B_BACKGROUND_D		,&l_klncrt				,	 0,		0,	icb_Ico		,PANEL_LIGHT_MAX	, kln) 
MY_STATIC2  (klnbg,kln_list		,KLN90B_BACKGROUND			,&l_klnIco				, kln);

