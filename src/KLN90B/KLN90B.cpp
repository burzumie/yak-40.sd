/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/Engines.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "KLN90B.h"

SDKLN90B *g_pKLN90B=NULL;

extern CRITICAL_SECTION cs_afp_fpl0;

HANDLE main_thread;
DWORD kln90b_thread_id;
CRITICAL_SECTION video_buffer_cs;
symbol video_buffer[7][23];
static symbol main_shadow_buffer[7][23];

int EXIT_MAIN_THREAD = 0;


HANDLE input_sem;
ULONG input_event[1024];
INT   events_count = 0;
CRITICAL_SECTION input_cs;
K_draw_t K_Draw;

extern INT SCREEN_FORMAT;
extern INT GLOBAL_STATE;
extern int INSELFTEST_CDI;

BOOL kln90b_hidden = FALSE;
long kln90b_win_id;

DWORD last_redraw = 0;
DWORD last_redraw_bl = 0;

void draw_ndb(HDC hdc,int x,int y)
{
	Arc(hdc,x-K_Draw.X_s/2+1,y-K_Draw.X_s/2+1,x+K_Draw.X_s/2-1,y+K_Draw.X_s/2-1,
		x-K_Draw.X_s/2+1,y-K_Draw.X_s/2+1,x-K_Draw.X_s/2+1,y-K_Draw.X_s/2+1);
}
void draw_vor(HDC hdc,int x,int y)
{
	Arc(hdc,x-K_Draw.X/2+1,y-K_Draw.X/2+1,x+K_Draw.X/2-1,y+K_Draw.X/2-1,
		x-K_Draw.X/2+1,y-K_Draw.X/2+1,x-K_Draw.X/2+1,y-K_Draw.X/2+1);
	Arc(hdc,x-1,y-1,x+1,y+1,x-1,y-1,x-1,y-1);

}
void draw_apt(HDC hdc,int x,int y)
{
	for(int i=K_Draw.X/2-1;i>1;i--)
	{
		MoveToEx(hdc,x-i,y,NULL);
		LineTo(hdc,x,y-i);
		LineTo(hdc,x+i,y);
		LineTo(hdc,x,y+i);
		LineTo(hdc,x-i,y);
	}
	//MoveToEx(hdc,x-1,y,NULL);
	//LineTo(hdc,x+1,y);
}

void print_char(HDC hdc,int X,int Y,char *str,int len,BOOL erase,BOOL inverse)
{
	HPEN pen;
	HBRUSH brush;

	if(inverse)
	{
		pen=K_Draw.h_pen_b; brush = K_Draw.h_brush_b;
	}
	else
	{
		pen=K_Draw.h_pen_t;brush=K_Draw.h_brush_t;
	}

	SelectObject(hdc,pen);
	SelectObject(hdc,brush);
	if(*str=='|')
	{
		if(erase) TextOut(hdc,X,Y," ",1);
		MoveToEx(hdc,X,Y-1,NULL);
		LineTo(hdc,X,Y+K_Draw.Y-1);
	}
	else if(*str=='{')
	{
		if(erase) TextOut(hdc,X,Y," ",1);
		MoveToEx(hdc,X+K_Draw.X,Y+K_Draw.Y/2-1,NULL);
		LineTo(hdc,X,Y+K_Draw.Y/2-1);
		LineTo(hdc,X,Y+K_Draw.Y-1);
	}
	else if(*str == '}' || *str == '>')
	{
		if(erase) TextOut(hdc,X,Y," ",1);
		if(*str == '}')
		{
			MoveToEx(hdc,X,Y-1,NULL);
			LineTo(hdc,X,Y+K_Draw.Y/2-1);
		}
		else
		{
			MoveToEx(hdc,X,Y+K_Draw.Y/2-1,NULL);
		}

		LineTo(hdc,X+K_Draw.X/2-1,Y+K_Draw.Y/2-1);
		BeginPath(hdc);
		LineTo(hdc,X+K_Draw.X/2-1,Y+K_Draw.Y/4-1-1);
		LineTo(hdc,X+K_Draw.X-1,Y+K_Draw.Y/2-1);
		LineTo(hdc,X+K_Draw.X/2-1,Y+K_Draw.Y-K_Draw.Y/4+1-1);
		LineTo(hdc,X+K_Draw.X/2-1,Y+K_Draw.Y/2-1);
		EndPath(hdc);
		SelectObject(hdc, !inverse ? K_Draw.h_brush_t : K_Draw.h_brush_b);
		FillPath(hdc);
	}
	else if(*str == '<')
	{
		if(erase) TextOut(hdc,X,Y," ",1);


		MoveToEx(hdc,X+K_Draw.X-1,Y+K_Draw.Y/2-1,NULL);
		LineTo(hdc,X+K_Draw.X/2,Y+K_Draw.Y/2-1);

		BeginPath(hdc);
		LineTo(hdc,X+K_Draw.X/2,Y+K_Draw.Y/4-1-1);
		LineTo(hdc,X,Y+K_Draw.Y/2-1);
		LineTo(hdc,X+K_Draw.X/2,Y+K_Draw.Y-K_Draw.Y/4+1-1);
		LineTo(hdc,X+K_Draw.X/2,Y+K_Draw.Y/2-1);
		EndPath(hdc);
		SelectObject(hdc,K_Draw.h_brush_t);
		FillPath(hdc);
	}
	else if(*str == DTO_CHAR)
	{
		TextOut(hdc,X,Y,"D ",2);
		MoveToEx(hdc,X,Y+K_Draw.Y/2-1,NULL);
		LineTo(hdc,X+K_Draw.X+1,Y+K_Draw.Y/2-1);
	}
	else if(*str == TO_CHAR)
	{
		TextOut(hdc,X,Y," ",1);
		MoveToEx(hdc,X,Y+K_Draw.Y/2+2,NULL);
		BeginPath(hdc);
		LineTo(hdc,X+K_Draw.X/2,Y+2);
		LineTo(hdc,X+K_Draw.X,Y+K_Draw.Y/2+2);
		LineTo(hdc,X,Y+K_Draw.Y/2+2);
		EndPath(hdc);
		SelectObject(hdc,K_Draw.h_brush_t);
		FillPath(hdc);
	}
	else if(*str == FROM_CHAR)
	{
		TextOut(hdc,X,Y," ",1);
		MoveToEx(hdc,X,Y+K_Draw.Y/2-2,NULL);
		BeginPath(hdc);
		LineTo(hdc,X+K_Draw.X/2,Y+K_Draw.Y-2);
		LineTo(hdc,X+K_Draw.X,Y+K_Draw.Y/2-2);
		LineTo(hdc,X,Y+K_Draw.Y/2-2);
		EndPath(hdc);
		SelectObject(hdc,K_Draw.h_brush_t);
		FillPath(hdc);
	}
	else if(*str == '^')
	{
		TextOut(hdc,X,Y," ",1);
		MoveToEx(hdc,X+K_Draw.X/2-1,Y+K_Draw.Y-1,NULL);
		LineTo(hdc,X+K_Draw.X/2-1,Y+K_Draw.Y/2-1);

		MoveToEx(hdc,X,Y+K_Draw.Y/2,NULL);
		BeginPath(hdc);
		LineTo(hdc,X+K_Draw.X/2-1,Y);
		LineTo(hdc,X+K_Draw.X-1,Y+K_Draw.Y/2);
		LineTo(hdc,X,Y+K_Draw.Y/2);
		EndPath(hdc);
		FillPath(hdc);
	}
	else if(*str == NDB_CHAR)
	{
		TextOut(hdc,X,Y," ",1);
		draw_ndb(hdc,X+K_Draw.X_s/2,Y+K_Draw.X_s/2);
	}  
	else if(*str == VOR_CHAR)
	{
		TextOut(hdc,X,Y," ",1);
		draw_vor(hdc,X+K_Draw.X_s/2,Y+K_Draw.X_s/2);
	}  
	else if(*str == APT_CHAR)
	{
		TextOut(hdc,X,Y," ",1);
		draw_apt(hdc,X+K_Draw.X_s/2,Y+K_Draw.X_s/2);
	}  
	else if(*str == MORE_CHAR)
	{
		TextOut(hdc,X,Y,">",1);
	}  
	else
	{
		if(!erase)
			SetBkMode(hdc,TRANSPARENT);
		TextOut(hdc,X,Y,str,len);
		if(!erase)
			SetBkMode(hdc,OPAQUE);
	}
}

void Out_String(K_string *str,int is_space,int beg=0,int len=23)
{
	for(int col=beg;col<beg+len;col++)
	{
		BOOL erase=TRUE;
		BOOL inverse=FALSE;

		if(main_shadow_buffer[str->row][col].attribute & ATTRIBUTE_TRANSPARENT)
			erase=FALSE;
		if(col>0)
			if(main_shadow_buffer[str->row][col-1].ch == DTO_CHAR)
				erase=FALSE;     
		if(main_shadow_buffer[str->row][col].attribute & ATTRIBUTE_INVERSE)
		{
			SetBkColor(str->hdc,str->text_color);
			SetTextColor(str->hdc,str->text_background);
			inverse=TRUE;
		}
		else
		{
			SetTextColor(str->hdc,str->text_color);
			SetBkColor(str->hdc,str->text_background);
		}
		if(main_shadow_buffer[str->row][col].attribute & ATTRIBUTE_FLASH)
		{
			if(is_space && (main_shadow_buffer[str->row][col].attribute&ATTRIBUTE_INVERSE))
		 {

			 SetTextColor(str->hdc,str->text_color);
			 SetBkColor(str->hdc,str->text_background);
			 if(main_shadow_buffer[str->row][col].attribute & ATTRIBUTE_SMALL)
			 {
				 print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y," ",1,erase,inverse);
				 SelectObject(str->hdc,K_Draw.h_font_s);
				 print_char(str->hdc,(str->XX)/2+col*(K_Draw.X_s),str->Y+K_Draw.Y/2-K_Draw.Y_s/2,&main_shadow_buffer[str->row][col].ch,1,erase,inverse);
				 SelectObject(str->hdc,K_Draw.h_font);
			 }
			 else if(main_shadow_buffer[str->row][col].attribute & ATTRIBUTE_BSMALL)
			 {
				 print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y," ",1,erase,inverse);
				 SelectObject(str->hdc,K_Draw.h_font_s);
				 print_char(str->hdc,(str->XX)/2+col*(str->X)+(K_Draw.X-K_Draw.X_s),str->Y+K_Draw.Y/2-K_Draw.Y_s/2,&main_shadow_buffer[str->row][col].ch,1,erase,inverse);
				 SelectObject(str->hdc,K_Draw.h_font);
			 }
			 else
				 print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y,&main_shadow_buffer[str->row][col].ch,1,erase,inverse);
		 }
			else if(is_space && !(main_shadow_buffer[str->row][col].attribute&ATTRIBUTE_INVERSE))
		 {
			 SetTextColor(str->hdc,str->text_color);
			 SetBkColor(str->hdc,str->text_background);
			 print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y," ",1,erase,inverse);
		 }
			else
		 {
			 if(main_shadow_buffer[str->row][col].attribute & ATTRIBUTE_SMALL)
			 {
				 print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y," ",1,erase,inverse);
				 SelectObject(str->hdc,K_Draw.h_font_s);
				 print_char(str->hdc,(str->XX)/2+col*(K_Draw.X_s),str->Y+K_Draw.Y/2-K_Draw.Y_s/2,&main_shadow_buffer[str->row][col].ch,1,erase,inverse);
				 SelectObject(str->hdc,K_Draw.h_font);
			 }
			 else if(main_shadow_buffer[str->row][col].attribute & ATTRIBUTE_BSMALL)
			 {
				 print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y," ",1,erase,inverse);
				 SelectObject(str->hdc,K_Draw.h_font_s);
				 print_char(str->hdc,(str->XX)/2+col*(str->X)+(K_Draw.X-K_Draw.X_s),str->Y+K_Draw.Y/2-K_Draw.Y_s/2,&main_shadow_buffer[str->row][col].ch,1,erase,inverse);
				 SelectObject(str->hdc,K_Draw.h_font);
			 }
			 else
				 print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y,&main_shadow_buffer[str->row][col].ch,1,erase,inverse);			  
		 }
		}
		else
		{
			if(main_shadow_buffer[str->row][col].attribute & ATTRIBUTE_SMALL)
		 {
			 print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y," ",1,erase,inverse);
			 SelectObject(str->hdc,K_Draw.h_font_s);
			 print_char(str->hdc,(str->XX)/2+col*(K_Draw.X_s),str->Y+K_Draw.Y/2-K_Draw.Y_s/2,&main_shadow_buffer[str->row][col].ch,1,erase,inverse);
			 SelectObject(str->hdc,K_Draw.h_font);
		 }
			else if(main_shadow_buffer[str->row][col].attribute & ATTRIBUTE_BSMALL)
			{
				print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y," ",1,erase,inverse);
				SelectObject(str->hdc,K_Draw.h_font_s);
				print_char(str->hdc,(str->XX)/2+col*(str->X)+(K_Draw.X-K_Draw.X_s),str->Y+K_Draw.Y/2-K_Draw.Y_s/2,&main_shadow_buffer[str->row][col].ch,1,erase,inverse);
				SelectObject(str->hdc,K_Draw.h_font);
			}
			else
				print_char(str->hdc,(str->XX)/2+col*(str->X),str->Y,&main_shadow_buffer[str->row][col].ch,1,erase,inverse);
		}
	}   
}

//=================================================================================================
extern FLOAT64 nav_5_resolution;
extern K_nav_lines_t K_nav_lines;
extern K_nav_box_t K_nav_box;

void draw_rnws(HDC hdc,int start_x,int len_x,int start_y,int len_y,rnw_t *rnws,int count,double resolution)
{
	double all_miles,dots_in_mile_x,dots_in_mile_y;
	SelectObject(hdc,K_Draw.h_pen_t);  

	all_miles      =  resolution;
	dots_in_mile_x =  len_x / resolution;
	dots_in_mile_y =  len_y / resolution;

	for(int i=0;i<count;i++)
	{
		long x1 = rnws[i].apt3_line.x1*dots_in_mile_x;
		long x2 = rnws[i].apt3_line.x2*dots_in_mile_x;
		long y1 = rnws[i].apt3_line.y1*dots_in_mile_y;
		long y2 = rnws[i].apt3_line.y2*dots_in_mile_y;

		x1 = start_x + len_x/2 + x1;
		x2 = start_x + len_x/2 + x2;
		y1 = start_y + len_y/2 - y1;
		y2 = start_y + len_y/2 - y2;

		if(x1 < start_x) x1 = start_x;
		if(x2 < start_x) x2 = start_x;
		if(x1 > start_x+len_x) x1=start_x+len_x;
		if(x2 > start_x+len_x) x2=start_x+len_x;

		if(y1 < start_y) y1 = start_y;
		if(y2 < start_y) y2 = start_y;
		if(y1 > start_y+len_y) y1=start_y+len_y;
		if(y2 > start_y+len_y) y2=start_y+len_y;
		MoveToEx(hdc,x1,y1,NULL);
		LineTo  (hdc,x2,y2);
	}

	for(int i=count-1;i>=0;i--)
	{
		long x1 = rnws[i].apt3_line.x1*dots_in_mile_x;
		long x2 = rnws[i].apt3_line.x2*dots_in_mile_x;
		long y1 = rnws[i].apt3_line.y1*dots_in_mile_y;
		long y2 = rnws[i].apt3_line.y2*dots_in_mile_y;

		x1 = start_x + len_x/2 + x1;
		x2 = start_x + len_x/2 + x2;
		y1 = start_y + len_y/2 - y1;
		y2 = start_y + len_y/2 - y2;

		if(x1 < start_x) x1 = start_x;
		if(x2 < start_x) x2 = start_x;
		if(x1 > start_x+len_x) x1=start_x+len_x;
		if(x2 > start_x+len_x) x2=start_x+len_x;

		if(y1 < start_y) y1 = start_y;
		if(y2 < start_y) y2 = start_y;
		if(y1 > start_y+len_y) y1=start_y+len_y;
		if(y2 > start_y+len_y) y2=start_y+len_y;

		SelectObject(hdc,(HFONT)K_Draw.h_font_s);
		TextOut(hdc,x1-K_Draw.X_s/2,y1-K_Draw.Y_s/2,rnws[i].apt3_line.pDes,strlen(rnws->apt3_line.pDes));
		TextOut(hdc,x2-K_Draw.X_s/2,y2-K_Draw.Y_s/2,rnws[i].apt3_line.sDes,strlen(rnws->apt3_line.sDes));
	}  
}

void draw_arrows(HDC hdc,int x1,int y1,int x2,int y2)
{
	double len = sqrt((double)((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)));

	double p3_x = ((x1-x2) / len);
	double p3_y = ((y1-y2) / len);

	double x1_1 = (p3_x-p3_y)*0.7071;
	double y1_1 = (p3_x+p3_y)*0.7071;

	double x1_2 = (p3_y+p3_x)*0.7071;
	double y1_2 = (p3_y-p3_x)*0.7071;

	double posx = /*p3_x*len*(1/8.0)*/p3_x*K_Draw.X_s+x2;
	double posy = /*p3_y*len*(1/8.0)*/p3_y*K_Draw.Y_s*0.5+y2;

	double x11 = /*x1_1*(0.125f)*len*/x1_1*K_Draw.X_s+posx;
	double y11 = /*y1_1*(0.125f)*len*/y1_1*K_Draw.Y_s*1.3+posy;

	double x12 = /*x1_2*(0.125f)*len*/x1_2*K_Draw.X_s+posx;
	double y12 = /*y1_2*(0.125f)*len*/y1_2*K_Draw.Y_s*1.3+posy;
	POINT old_pl;

	MoveToEx(hdc,posx,posy,&old_pl);
	LineTo(hdc,x11,y11);

	MoveToEx(hdc,posx,posy,NULL);
	LineTo(hdc,x12,y12);

	MoveToEx(hdc,old_pl.x,old_pl.y,NULL);
}

void print_str_lr(HDC hdc,int x,int y,char *str,int left_right,int beg_x)
{
	if(!left_right)
	{
		int pixel_len = strlen(str)*K_Draw.X_s;
		int s_x = x-K_Draw.X_s-pixel_len;
		if(s_x>=beg_x)
		{
			TextOut(hdc,s_x,y-K_Draw.Y_s/2,str,strlen(str));
			return;
		}
	}
	TextOut(hdc,x+K_Draw.X_s,y-K_Draw.Y_s/2,str,strlen(str));
}

void print_str_c(HDC hdc,int x,int y,char *str,int beg_x,int sm)
{
	int pixel_len = strlen(str)*K_Draw.X_s;
	int s_x = x-pixel_len/2;
	if(s_x>=beg_x)
	{
		if(sm)
			TextOut(hdc,s_x,y+K_Draw.Y_s/2+1,str,strlen(str));
		else
			TextOut(hdc,s_x,y+K_Draw.Y/2+1,str,strlen(str)); 
		return;
	}
	if(sm)
		TextOut(hdc,x,y+K_Draw.Y_s/2+1,str,strlen(str));
	else
		TextOut(hdc,x,y+K_Draw.Y/2+1,str,strlen(str));
}

void draw_nav5_page(HDC hdc,int start_x,int len_x,int start_y,int len_y,BOOL s5)
{
	int sam_nach_y,sam_nach_x,len_x_aircr,len_y_aircr;
	double all_miles,dots_in_mile_x,dots_in_mile_y;
	SelectObject(hdc,K_Draw.h_pen_t);  
	if(K_nav_box.curr_nav5_mode != NAV5_MODE_TN)
	{
		sam_nach_y =  len_y * 0.654f;
		sam_nach_x =  len_x * 0.446f;
		len_x_aircr = len_x * 0.09f;
		len_y_aircr = len_y * 0.103f;
		all_miles = 1.3f*nav_5_resolution;
		dots_in_mile_x = len_x / nav_5_resolution;
		dots_in_mile_y = len_y / all_miles;
	}
	else
	{
		len_x_aircr = len_x * 0.09f;
		len_y_aircr = len_y * 0.103f;

		sam_nach_y     =  len_y * 0.5f - len_y_aircr/2.0f;
		sam_nach_x     =  len_x * 0.5f - len_x_aircr/2.0f;
		all_miles      =  nav_5_resolution;
		dots_in_mile_x =  len_x / nav_5_resolution;
		dots_in_mile_y =  len_y / nav_5_resolution;
	}
	//========================================================================================================
	//if(s5)
	//   SelectObject(hdc,(HPEN)K_Draw.h_pen_t_s);
	//else
	SelectObject(hdc,(HPEN)K_Draw.h_pen_t);

	int last_x=-100000,last_y=-100000;
	int left_right=1;

	SetTextColor(hdc,K_Draw.text_color);
	SetBkColor(hdc,K_Draw.text_background);

	for(int i=0;i<K_nav_lines.legs_count;i++)
	{
		if(K_nav_lines.rt_legs[i].numbers_only)
			continue;
		long x1 = K_nav_lines.rt_legs[i].x1*dots_in_mile_x;
		long y1 = K_nav_lines.rt_legs[i].y1*dots_in_mile_y;
		x1 = start_x + len_x/2 + x1;
		y1 = (start_y+sam_nach_y+len_y_aircr/2)-y1;

		if(x1 < start_x) x1 = start_x;
		if(x1 > start_x+len_x) x1=start_x+len_x;

		if(y1 < start_y) y1 = start_y;
		if(y1 > start_y+len_y) y1=start_y+len_y;
		MoveToEx(hdc,x1,y1,NULL);
		break;
	}
	for(int i=0;i<K_nav_lines.legs_count;i++)
	{
		long x2 = K_nav_lines.rt_legs[i].x2*dots_in_mile_x;
		long y2 = K_nav_lines.rt_legs[i].y2*dots_in_mile_y;
		x2 = start_x + len_x/2 + x2;
		y2 = (start_y+sam_nach_y+len_y_aircr/2)-y2;


		if(x2 < start_x) x2 = start_x;
		if(x2 > start_x+len_x) x2=start_x+len_x;


		if(y2 < start_y) y2 = start_y;
		if(y2 > start_y+len_y) y2=start_y+len_y;

		if(K_nav_lines.rt_legs[i].is_dto_line)
		{
			long x1 = K_nav_lines.rt_legs[i].x1*dots_in_mile_x;
			long y1 = K_nav_lines.rt_legs[i].y1*dots_in_mile_y;
			x1 = start_x + len_x/2 + x1;
			y1 = (start_y+sam_nach_y+len_y_aircr/2)-y1;

			if(x1 < start_x) x1 = start_x;
			if(x1 > start_x+len_x) x1=start_x+len_x;

			if(y1 < start_y) y1 = start_y;
			if(y1 > start_y+len_y) y1=start_y+len_y;

			if(K_nav_lines.rt_legs[i].is_dto_line!=3)
		 {
			 MoveToEx(hdc,x1,y1,NULL);
			 LineTo  (hdc,x2,y2);
		 }
			if(K_nav_lines.rt_legs[i].is_arrow)
		 {
			 draw_arrows(hdc,x1,y1,x2,y2);
		 }
		}
		else
		{
			if(!K_nav_lines.rt_legs[i].numbers_only)
			{
				long x1 = K_nav_lines.rt_legs[i].x1*dots_in_mile_x;
				long y1 = K_nav_lines.rt_legs[i].y1*dots_in_mile_y;
				x1 = start_x + len_x/2 + x1;
				y1 = (start_y+sam_nach_y+len_y_aircr/2)-y1;

				if(x1 < start_x) x1 = start_x;
				if(x1 > start_x+len_x) x1=start_x+len_x;

				if(y1 < start_y) y1 = start_y;
				if(y1 > start_y+len_y) y1=start_y+len_y;
				MoveToEx  (hdc,x1,y1,NULL);
				LineTo  (hdc,x2,y2);
				if(K_nav_lines.rt_legs[i].is_arrow)
				{
					long x1 = K_nav_lines.rt_legs[i].x1*dots_in_mile_x;
					long y1 = K_nav_lines.rt_legs[i].y1*dots_in_mile_y;
					x1 = start_x + len_x/2 + x1;
					y1 = (start_y+sam_nach_y+len_y_aircr/2)-y1;

					if(x1 < start_x) x1 = start_x;
					if(x1 > start_x+len_x) x1=start_x+len_x;

					if(y1 < start_y) y1 = start_y;
					if(y1 > start_y+len_y) y1=start_y+len_y;
					draw_arrows(hdc,x1,y1,x2,y2);
				}
			}
		}
	}
	// ============================================== For Text =================================
	if(K_nav_lines.legs_count)
	{
		long x1 = K_nav_lines.rt_legs[0].x1*dots_in_mile_x;
		long y1 = K_nav_lines.rt_legs[0].y1*dots_in_mile_y;
		x1 = start_x + len_x/2 + x1;
		y1 = (start_y+sam_nach_y+len_y_aircr/2)-y1;

		if(x1 < start_x) x1 = start_x;
		if(x1 > start_x+len_x) x1=start_x+len_x;

		if(y1 < start_y) y1 = start_y;
		if(y1 > start_y+len_y) y1=start_y+len_y;

		char _str[3];
		if(!s5)
		{
			if(K_nav_lines.rt_legs[0].n1)
			{
				_snprintf(_str,3,"%d",K_nav_lines.rt_legs[0].n1);
				_str[2]='\0';
				SelectObject(hdc,(HFONT)K_Draw.h_font_s);
				TextOut(hdc,x1-K_Draw.X_s/2,y1-K_Draw.Y_s/2,_str,strlen(_str)); 
			}
		}
		else
		{
			if(K_nav_lines.rt_legs[0].n1)
			{
				RECT rect; rect.left=x1-K_Draw.X_s/2+1; rect.top=y1-K_Draw.X_s/2+1;
				rect.right = x1+K_Draw.X_s/2-1; rect.bottom = y1+K_Draw.X_s/2-1;
				::FillRect(hdc,&rect,K_Draw.h_brush_t);
				SelectObject(hdc,(HFONT)K_Draw.h_font_s);
				print_str_lr(hdc,x1,y1,K_nav_lines.rt_legs[0].n1_icao,left_right,start_x);
				left_right = 1-left_right;		 
			}
		}	  
	}

	for(int i=0;i<K_nav_lines.legs_count;i++)
	{
		long x2 = K_nav_lines.rt_legs[i].x2*dots_in_mile_x;
		long y2 = K_nav_lines.rt_legs[i].y2*dots_in_mile_y;
		x2 = start_x + len_x/2 + x2;
		y2 = (start_y+sam_nach_y+len_y_aircr/2)-y2;


		if(x2 < start_x) x2 = start_x;
		if(x2 > start_x+len_x) x2=start_x+len_x;


		if(y2 < start_y) y2 = start_y;
		if(y2 > start_y+len_y) y2=start_y+len_y;

		if(K_nav_lines.rt_legs[i].is_dto_line)
		{
			if(K_nav_lines.rt_legs[i].is_arrow)
			{
				if(s5)
				{
					if(K_nav_lines.rt_legs[i].is_dto_line==1)
					{
						RECT rect; rect.left=x2-K_Draw.X_s/2+1; rect.top=y2-K_Draw.X_s/2+1;
						rect.right = x2+K_Draw.X_s/2-1; rect.bottom = y2+K_Draw.X_s/2-1;
						::FillRect(hdc,&rect,K_Draw.h_brush_t);
						SelectObject(hdc,(HFONT)K_Draw.h_font_s);
						print_str_lr(hdc,x2,y2,K_nav_lines.rt_legs[i].n2_icao,left_right,start_x);
						left_right = 1-left_right;		 
					}
				}
				else
				{
					if(K_nav_lines.rt_legs[i].is_dto_line==1)
					{
						SelectObject(hdc,(HFONT)K_Draw.h_font_s);
						TextOut(hdc,x2-K_Draw.X_s/2,y2-K_Draw.Y_s/2,"*",1);
					}
				}
			}
		}
		else
		{
			char _str[3];
			if(!s5)
			{
				if(K_nav_lines.rt_legs[i].n2)
				{
					_snprintf(_str,3,"%d",K_nav_lines.rt_legs[i].n2);
					_str[2]='\0';
					SelectObject(hdc,(HFONT)K_Draw.h_font_s);
					TextOut(hdc,x2-K_Draw.X_s/2,y2-K_Draw.Y_s/2,_str,strlen(_str)); 
				}
			}
			else
			{
				if(K_nav_lines.rt_legs[i].n2)
				{
					RECT rect; rect.left=x2-K_Draw.X_s/2+1; rect.top=y2-K_Draw.X_s/2+1;
					rect.right = x2+K_Draw.X_s/2-1; rect.bottom = y2+K_Draw.X_s/2-1;
					::FillRect(hdc,&rect,K_Draw.h_brush_t);
					SelectObject(hdc,(HFONT)K_Draw.h_font_s);
					print_str_lr(hdc,x2,y2,K_nav_lines.rt_legs[i].n2_icao,left_right,start_x);
					left_right = 1-left_right;		
				}
			}
		}
	} 
	//=============================== Rnws ======================================================
	for(int i=0;i<K_nav_lines.rnws_count;i++)
	{
		long x1 = K_nav_lines.rnws[i].x1*dots_in_mile_x;
		long x2 = K_nav_lines.rnws[i].x2*dots_in_mile_x;
		long y1 = K_nav_lines.rnws[i].y1*dots_in_mile_y;
		long y2 = K_nav_lines.rnws[i].y2*dots_in_mile_y;
		x1 = start_x + len_x/2 + x1;
		x2 = start_x + len_x/2 + x2;
		y1 = (start_y+sam_nach_y+len_y_aircr/2)-y1;
		y2 = (start_y+sam_nach_y+len_y_aircr/2)-y2;

		if(x1 < start_x) x1 = start_x;
		if(x2 < start_x) x2 = start_x;
		if(x1 > start_x+len_x) x1=start_x+len_x;
		if(x2 > start_x+len_x) x2=start_x+len_x;

		if(y1 < start_y) y1 = start_y;
		if(y2 < start_y) y2 = start_y;
		if(y1 > start_y+len_y) y1=start_y+len_y;
		if(y2 > start_y+len_y) y2=start_y+len_y;

		MoveToEx(hdc,x1,y1,NULL);
		LineTo  (hdc,x2,y2);

		if(K_nav_lines.rnws[i].numbers_only)
		{
			if(!K_nav_lines.rnws[i].n1)
		 {
			 SelectObject(hdc,(HFONT)K_Draw.h_font_s);
			 TextOut(hdc,x1-K_Draw.X_s/2,y1-K_Draw.Y_s/2,K_nav_lines.rnws[i].pDes,strlen(K_nav_lines.rnws[i].pDes));
		 }
			if(!K_nav_lines.rnws[i].n2)
		 {
			 SelectObject(hdc,(HFONT)K_Draw.h_font_s);
			 TextOut(hdc,x2-K_Draw.X_s/2,y2-K_Draw.Y_s/2,K_nav_lines.rnws[i].sDes,strlen(K_nav_lines.rnws[i].sDes));
		 }
		}
	}
	//===========================================================================================
	SetBkMode(hdc,TRANSPARENT); 
	for(int i=0;i<K_nav_lines.points_count;i++)
	{
		long x = K_nav_lines.points[i].x*dots_in_mile_x;
		long y = K_nav_lines.points[i].y*dots_in_mile_y;

		x = start_x + len_x/2 + x;
		y = (start_y+sam_nach_y+len_y_aircr/2)-y;

		if(x < start_x) x = start_x;
		if(x > start_x+len_x) x=start_x+len_x;

		if(y < start_y) y = start_y;
		if(y > start_y+len_y) y=start_y+len_y;

		switch(K_nav_lines.points[i].pt_type)
		{
		case NAVAID_NDB:
			{
				SelectObject(hdc,(HFONT)K_Draw.h_font_s);
				SelectObject(hdc,(HPEN)K_Draw.h_pen_t);
				draw_ndb(hdc,x,y);
				print_str_c(hdc,x,y,K_nav_lines.points[i].ICAO_ID,start_x,1);
				break;
			}
		case NAVAID_VOR:
			{
				SelectObject(hdc,(HFONT)K_Draw.h_font_s);
				draw_vor(hdc,x,y);
				print_str_c(hdc,x,y,K_nav_lines.points[i].ICAO_ID,start_x,0); 
				break;
			}
		case NAVAID_APT:
			{
				SelectObject(hdc,(HFONT)K_Draw.h_font_s);
				if(nav_5_resolution>2.0f)
					draw_apt(hdc,x,y);
				print_str_c(hdc,x,y,K_nav_lines.points[i].ICAO_ID,start_x,0);   
				break;
			}
		}
	}
	SetBkMode(hdc,OPAQUE); 
	//*******************************************************************************************
	SelectObject(hdc,(HPEN)K_Draw.h_pen_t);
	if( K_nav_box.curr_nav5_mode == NAV5_MODE_TK || 
		K_nav_box.curr_nav5_mode == NAV5_MODE_HDG
		)
	{
		//============================ Fusel ===================================================
		RECT rect;

		rect.left = start_x+len_x/2-2;//start_x+len_x/2-len_x_aircr/2-2;
		rect.top = start_y+sam_nach_y-1;
		rect.bottom = start_y+sam_nach_y+len_y_aircr+2;
		rect.right = start_x + len_x/2+1;//start_x+len_x/2+len_x_aircr/2+2;
		::FillRect(hdc,&rect,K_Draw.h_brush_b);

		MoveToEx(hdc,start_x + len_x/2,start_y+sam_nach_y,NULL);
		LineTo  (hdc,start_x + len_x/2,start_y+sam_nach_y+len_y_aircr);
		MoveToEx(hdc,start_x+len_x/2-1,start_y+sam_nach_y,NULL);
		LineTo  (hdc,start_x+len_x/2-1,start_y+sam_nach_y+len_y_aircr);
		//============================ Wing ====================================================
		MoveToEx(hdc,start_x+len_x/2-len_x_aircr/2,start_y+sam_nach_y+len_y_aircr/2,NULL);
		LineTo  (hdc,start_x+len_x/2+len_x_aircr/2,start_y+sam_nach_y+len_y_aircr/2);
		MoveToEx(hdc,start_x+len_x/2-len_x_aircr/2,start_y+sam_nach_y+len_y_aircr/2-1,NULL);
		LineTo  (hdc,start_x+len_x/2+len_x_aircr/2,start_y+sam_nach_y+len_y_aircr/2-1);
		if((start_y+sam_nach_y+len_y_aircr/2-2) < (start_y+sam_nach_y))
		{
			MoveToEx(hdc,start_x+len_x/2-len_x_aircr/2,start_y+sam_nach_y+len_y_aircr/2-2,NULL);
			LineTo  (hdc,start_x+len_x/2+len_x_aircr/2,start_y+sam_nach_y+len_y_aircr/2-2);
		}
		//============================ STAB ======================================================
		MoveToEx(hdc,start_x+len_x/2-2,start_y+sam_nach_y+len_y_aircr-1,NULL);
		LineTo(hdc,start_x+len_x/2+2,start_y+sam_nach_y+len_y_aircr-1);
		MoveToEx(hdc,start_x+len_x/2-2,start_y+sam_nach_y+len_y_aircr,NULL);
		LineTo(hdc,start_x+len_x/2+2,start_y+sam_nach_y+len_y_aircr);
		//*******************************************************************************************
	}
	else
	{
		int st_x = start_x + len_x/2;
		int st_y = start_y+sam_nach_y+len_y_aircr/2;

		for(int i=len_x_aircr/2;i>=0;i--)
		{
			MoveToEx(hdc,st_x-i,st_y,NULL);
			LineTo(hdc,st_x,st_y-i);
			LineTo(hdc,st_x+i,st_y);
			LineTo(hdc,st_x,st_y+i);
			LineTo(hdc,st_x-i,st_y);
		}
		MoveToEx(hdc,st_x-1,st_y,NULL);
		LineTo(hdc,st_x+1,st_y);
	}
}

void draw_cdi(HDC hdc,int start_x,int start_y,BOOL is_big)
{

	FLOAT64 xtk,scale;
	BOOL is_cdi = get_CDI(&xtk,&scale);
	xtk=-xtk;

	if(is_big)
	{
		SelectObject(hdc,K_Draw.h_pen_t_s);
		SelectObject(hdc,K_Draw.h_brush_t);

		for(int col=1;col<23;col+=2)
		{
			if(col==11) continue;

			MoveToEx(hdc,start_x+col*K_Draw.X,start_y+K_Draw.Y,NULL);
			LineTo(hdc,start_x+(col+1)*K_Draw.X-1,start_y+K_Draw.Y);
			LineTo(hdc,start_x+col*K_Draw.X,start_y+K_Draw.Y);

			MoveToEx(hdc,start_x+col*K_Draw.X+K_Draw.X/2,start_y+K_Draw.Y-K_Draw.X/2,NULL);
			LineTo(hdc,start_x+col*K_Draw.X+K_Draw.X/2,start_y+K_Draw.Y-K_Draw.X/2+K_Draw.X-1);
			LineTo(hdc,start_x+col*K_Draw.X+K_Draw.X/2,start_y+K_Draw.Y-K_Draw.X/2);

			int x_c = start_x+col*K_Draw.X+K_Draw.X/2;
			int y_c = start_y+K_Draw.Y;

			BeginPath(hdc);
			Ellipse(hdc,x_c-K_Draw.X/2+1,y_c-K_Draw.X/2+1,x_c+K_Draw.X/2-1,y_c+K_Draw.X/2-1);
			EndPath(hdc);
			FillPath(hdc);
		} 
		int x_2 = K_Draw.X/2;
		int y_2 = K_Draw.Y/2;
		int x_c = start_x + 11*K_Draw.X + x_2;
		int y_c = start_y + K_Draw.Y;
		SelectObject(hdc,K_Draw.h_pen_t);
		SelectObject(hdc,K_Draw.h_brush_t);
		if(is_cdi)
		{
			if(get_FT())
			{
				MoveToEx(hdc,x_c-K_Draw.X,y_c+y_2/2,NULL);
				BeginPath(hdc);
				LineTo(hdc,x_c,start_y+y_2/2);
				LineTo(hdc,x_c+K_Draw.X,y_c+y_2/2);
				LineTo(hdc,x_c-K_Draw.X,y_c+y_2/2);
				EndPath(hdc);
			}
			else
			{
				MoveToEx(hdc,x_c-K_Draw.X,y_c-y_2/2,NULL);
				BeginPath(hdc);
				LineTo(hdc,x_c,(start_y+2*K_Draw.Y)-y_2/2);
				LineTo(hdc,x_c+K_Draw.X,y_c-y_2/2);
				LineTo(hdc,x_c-K_Draw.X,y_c-y_2/2);
				EndPath(hdc);
			}
			FillPath(hdc);
			int x_dest = xtk*((5.0/scale)*2.0f*(FLOAT64)K_Draw.X);
			x_dest+=start_x+K_Draw.X*11+x_2;
			if(x_dest<start_x) x_dest=start_x+x_2/2;
			if(x_dest>23*K_Draw.X) x_dest = 23*K_Draw.X-x_2/2;
			RECT rect;
			rect.left = x_dest-x_2/2;
			rect.right = x_dest+x_2/2;
			rect.top = start_y;
			rect.bottom = start_y + 2*K_Draw.Y;
			::FillRect(hdc,&rect,K_Draw.h_brush_t);
		}
		else
		{
			SetTextColor(hdc,K_Draw.text_background);
			SetBkColor(hdc,K_Draw.text_color);
			TextOut(hdc,x_2+K_Draw.X*6,y_c-y_2,"F  L   A  G",strlen("F  L   A  G"));
		}
	}   
	else
	{
		if(is_cdi)
		{
			int x_2 = K_Draw.X/2;
			int x_dest = xtk*((5.0/scale)*1.0f*(FLOAT64)K_Draw.X);
			x_dest+=start_x+K_Draw.X*5+x_2;
			if(x_dest<start_x) x_dest=start_x+x_2/2;
			if(x_dest>(start_x+11*K_Draw.X)) x_dest = (start_x+11*K_Draw.X)-x_2/2;
			RECT rect;
			rect.left = x_dest-x_2/2+1;
			rect.right = x_dest+x_2/2-1;
			rect.top = start_y;
			rect.bottom = start_y + K_Draw.Y;
			::FillRect(hdc,&rect,K_Draw.h_brush_t); 
		}
	}
}

void build_draw(HDC hdc,PIXPOINT *dim)
{
	if(dim->x != K_Draw.x || dim->y != K_Draw.y)
	{
		if(K_Draw.h_font)
			DeleteObject(K_Draw.h_font);
		if(K_Draw.h_font_s)
			DeleteObject(K_Draw.h_font_s);

		K_Draw.X = (long)(double)((double)dim->x / (double)23.0f);
		K_Draw.Y = (long)(double)((double)(dim->y-3) / (double)8);

		K_Draw.X_s = K_Draw.X*0.67+1;
		K_Draw.Y_s = K_Draw.Y*0.72+1;

		LOGFONT lf;
		lf.lfHeight = -K_Draw.Y; 
		lf.lfWidth = -K_Draw.X; 
		lf.lfEscapement = 0; 
		lf.lfOrientation = 0; 
		lf.lfWeight = 500;//FW_REGULAR; 
		lf.lfItalic = 0; 
		lf.lfUnderline = 0; 
		lf.lfStrikeOut = 0; 
		lf.lfCharSet = RUSSIAN_CHARSET; 
		lf.lfOutPrecision=0;//OUT_TT_ONLY_PRECIS;//0; 
		lf.lfClipPrecision=0; 
		lf.lfQuality=0; 
		lf.lfPitchAndFamily=FIXED_PITCH; 
		strcpy(lf.lfFaceName,/*"Lucida Console"*/"Glass Gauge"); 

		//K_Draw.h_font = CreateFontIndirect(&lf);
		K_Draw.h_font = CreateFont(-K_Draw.Y,-K_Draw.X,0,0,500,0,0,0,RUSSIAN_CHARSET,0,0,0,FIXED_PITCH,/*"Lucida Console"*/"Glass Gauge");
		K_Draw.h_font_s = CreateFont(-K_Draw.Y_s,-K_Draw.X_s,0,0,500,0,0,0,RUSSIAN_CHARSET,0,0,0,FIXED_PITCH,/*"Lucida Console"*/"Glass Gauge");
		K_Draw.x = dim->x;
		K_Draw.y = dim->y;
		//K_DEBUG("K_Draw.h_font=[0x%08X] K_Draw.h_font_s=[0x%08X]\n",K_Draw.h_font,K_Draw.h_font_s);
	}
	if(K_Draw.text_color!=main_shadow_buffer[0][0].text_color
		||
		K_Draw.text_background != main_shadow_buffer[0][0].text_background
		)
	{
		K_Draw.text_color        = main_shadow_buffer[0][0].text_color;
		K_Draw.text_background   = main_shadow_buffer[0][0].text_background;
		if(K_Draw.h_pen_b)
			DeleteObject(K_Draw.h_pen_b);
		if(K_Draw.h_pen_t)
			DeleteObject(K_Draw.h_pen_t);
		if(K_Draw.h_pen_t_s)
			DeleteObject(K_Draw.h_pen_t_s);
		if(K_Draw.h_brush_t)
			DeleteObject(K_Draw.h_brush_t);
		if(K_Draw.h_brush_b)
			DeleteObject(K_Draw.h_brush_b);

		K_Draw.h_pen_b   = CreatePen(PS_SOLID,0,K_Draw.text_background);
		K_Draw.h_pen_t   = CreatePen(PS_SOLID,0,K_Draw.text_color);
		K_Draw.h_pen_t_s = CreatePen(PS_SOLID,2,K_Draw.text_color);
		K_Draw.h_brush_t = CreateSolidBrush(K_Draw.text_color);
		K_Draw.h_brush_b = CreateSolidBrush(K_Draw.text_background);
		//K_DEBUG("K_Draw.h_pen_b=[0x%08X] K_Draw.h_pen_t=[0x%08X]\n",K_Draw.h_pen_b,K_Draw.h_pen_t);
	}

}

FLOAT64 ExtGPSLbu;
FLOAT64 ExtGPSLbuDiff;
FLOAT64 ExtGpsPuDelta;
FLOAT64 ExtDefApHDG;
BOOL ExtGpsGot=0;
BOOL UseDefAP;

void do_input_event(long event)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=event;
	LeaveCriticalSection(&input_cs);
	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);
}

BOOL FSAPI PowerOnOff(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_ONOFF;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}
BOOL FSAPI BrightPlus(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_BTNMORE;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}
BOOL FSAPI BrightMinus(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_BTNLESS;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

#define WHEEL_DOWN 8192
#define WHEEL_UP   16384


BOOL FSAPI RKnobPlus(PPIXPOINT pPoint, FLAGS32 flags)
{
	//if(!(flags & 0x80000000 || flags & 0x20000000))  return(TRUE);

	EnterCriticalSection(&input_cs);
	if     (flags & 0x80000000)
		input_event[events_count++]=INPUT_ROUTERPLUS; 
	else if(flags & 0x20000000)
		input_event[events_count++]=INPUT_RINNERPLUS;
	else if(flags & WHEEL_DOWN)
		input_event[events_count++]=INPUT_RINNERMINUS;
	else if(flags & WHEEL_UP)
		input_event[events_count++]=INPUT_RINNERPLUS;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}
BOOL FSAPI RKnobMinus(PPIXPOINT pPoint, FLAGS32 flags)
{
	//if(!(flags & 0x80000000 || flags & 0x20000000))  return(TRUE);
	EnterCriticalSection(&input_cs);
	if     (flags & 0x80000000)
		input_event[events_count++]=INPUT_ROUTERMINUS; 
	else if(flags & 0x20000000)
		input_event[events_count++]=INPUT_RINNERMINUS;
	else if(flags & WHEEL_DOWN)
		input_event[events_count++]=INPUT_RINNERMINUS;
	else if(flags & WHEEL_UP)
		input_event[events_count++]=INPUT_RINNERPLUS;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI EnterButton(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_ENTER;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}
BOOL FSAPI LKnobPlus(PPIXPOINT pPoint, FLAGS32 flags)
{
	//if(!(flags & 0x80000000 || flags & 0x20000000))  return(TRUE);
	EnterCriticalSection(&input_cs);
	if     (flags & 0x80000000)
		input_event[events_count++]=INPUT_LOUTERPLUS; 
	else if(flags & 0x20000000)
		input_event[events_count++]=INPUT_LINNERPLUS;
	else if(flags & WHEEL_DOWN)
		input_event[events_count++]=INPUT_LINNERMINUS;
	else if(flags & WHEEL_UP)
		input_event[events_count++]=INPUT_LINNERPLUS;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}
BOOL FSAPI LKnobMinus(PPIXPOINT pPoint, FLAGS32 flags)
{
	//if(!(flags & 0x80000000 || flags & 0x20000000))  return(TRUE);
	EnterCriticalSection(&input_cs);
	if     (flags & 0x80000000)
		input_event[events_count++]=INPUT_LOUTERMINUS; 
	else if(flags & 0x20000000)
		input_event[events_count++]=INPUT_LINNERMINUS;
	else if(flags & WHEEL_DOWN)
		input_event[events_count++]=INPUT_LINNERMINUS;
	else if(flags & WHEEL_UP)
		input_event[events_count++]=INPUT_LINNERPLUS;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI LKnobOuterLeft (PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_LOUTERMINUS; 
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI LKnobOuterRight(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_LOUTERPLUS; 
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI LKnobInnerLeft (PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_LINNERMINUS; 
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI LKnobInnerRight(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_LINNERPLUS; 
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI RKnobOuterLeft (PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_ROUTERMINUS; 
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI RKnobOuterRight(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_ROUTERPLUS; 
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI RKnobInnerLeft (PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_RINNERMINUS; 
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI RKnobInnerRight(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_RINNERPLUS; 
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}


BOOL FSAPI DButton(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_DTO;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI RCursor(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_RCURSOR;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}
BOOL FSAPI LCursor(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_LCURSOR;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI PullScan(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_PULLSCAN;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);

	return(TRUE);
}

BOOL FSAPI CLR(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_CLR;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);
	return(TRUE);
}

BOOL FSAPI K_MSG(PPIXPOINT pPoint, FLAGS32 flags)
{
	EnterCriticalSection(&input_cs);
	input_event[events_count++]=INPUT_MSG;
	LeaveCriticalSection(&input_cs);

	LONG prev_val;
	ReleaseSemaphore(input_sem,1,&prev_val);
	return(TRUE);
}

extern "C" DWORD WINAPI klb90b_main(PVOID argument);

SDKLN90B::SDKLN90B() 
{
	init_sim_vars();
	memset(&K_Draw,0,sizeof(K_Draw));
	mem_init();
	for(int y=0;y<7;y++)
		for(int x=0;x<23;x++)
		{
			video_buffer[y][x].ch = ' ';
			video_buffer[y][x].text_background = RGB(11,11,11);
			video_buffer[y][x].text_color      = RGB(0,128,0);
			video_buffer[y][x].attribute       = 0;
		}
		UseDefAP=FALSE;
		InitializeCriticalSection(&video_buffer_cs);
		init_debug();
		input_sem = CreateSemaphore(NULL,0,1024,NULL);
		InitializeCriticalSection(&input_cs);
		InitializeCriticalSection(&cs_afp_fpl0);
		main_thread = CreateThread(NULL,0,klb90b_main,0,0,&kln90b_thread_id);
}

SDKLN90B::~SDKLN90B()
{
	UseDefAP=FALSE;
	EXIT_MAIN_THREAD = 1;
	WaitForSingleObject(main_thread,INFINITE);
	DeleteCriticalSection(&video_buffer_cs);
	DeleteCriticalSection(&input_cs);
	DeleteCriticalSection(&cs_afp_fpl0);
	deinit_debug();
	CloseHandle(input_sem);
	mem_deinit();
}

void SDKLN90B::Init()
{
}

void SDKLN90B::Update()
{
	update_sim_vars();
	if(UseDefAP && ExtGpsGot)
	{
		ImportTable.pPanels->trigger_key_event(KEY_HEADING_BUG_SET,K_ftol(ExtDefApHDG));
	}
}

void SDKLN90B::Load(CIniFile *ini)
{
}

void SDKLN90B::Save(CIniFile *ini)
{
}

void SDKLN90B::Draw(PELEMENT_STATIC_IMAGE pelement)
{
	static int is_space = 1;
	static int _IsSpace = 0;

	K_print_debug_buffer();
	if( GetTickCount() - last_redraw_bl >= 500 )
	{
		is_space = 1-is_space;
		_IsSpace = is_space;
		last_redraw_bl = GetTickCount();
		last_redraw = GetTickCount();
	}
	else if( GetTickCount() - last_redraw >= 200 )
	{
		last_redraw = GetTickCount();
	}
	else
		return;


	EnterCriticalSection(&video_buffer_cs);
	memcpy(main_shadow_buffer,video_buffer,sizeof(main_shadow_buffer));
	LeaveCriticalSection(&video_buffer_cs);

	if (pelement)
	{
		HDC hdc = pelement->hdc;			
		// 7 Rows 23 Columns
		PIXPOINT dim = pelement->image_data.final->dim;
		int X = (long)(double)((double)dim.x / (double)23.0f);
		int Y = (long)(double)((double)(dim.y-3) / (double)7);
		build_draw(hdc,&dim);
		PIXPOINT pos = pelement->position;

		if (hdc)
		{
			RECT client_rect;
			client_rect.left = 0;
			client_rect.right = dim.x;
			client_rect.top = 0;
			client_rect.bottom = dim.y;

			//******************** Set for Font Format ************************************
			if(SCREEN_FORMAT == FORMAT_BLACK)
			{			   
				if(GLOBAL_STATE == STATE_OFF)
				{
					::FillRect(hdc, &client_rect, (HBRUSH) GetStockObject (BLACK_BRUSH));
					SET_OFF_SCREEN (pelement);
					return;
				}
				::FillRect(hdc, &client_rect,K_Draw.h_brush_b);
			}
			else
				::FillRect(hdc, &client_rect,K_Draw.h_brush_b);
			if(!K_Draw.h_font || !K_Draw.h_font_s || !K_Draw.h_pen_b || !K_Draw.h_pen_t ) 
			{
				//K_DEBUG("GDI Bug !!!\n");
				return;
			}
			SelectObject(hdc,(HFONT)K_Draw.h_font);
			TEXTMETRIC text_met;
			GetTextMetrics(hdc,&text_met);
			int YY = dim.y - (text_met.tmHeight * 7);
			int XX = dim.x - (text_met.tmMaxCharWidth * 23);
			//X = text_met.tmMaxCharWidth;
			int row;

			K_string str;
			str.hdc = hdc;
			str.text_background = K_Draw.text_background;
			str.text_color      = K_Draw.text_color;
			str.X  = X;
			str.XX = XX;
			int super_page = get_super_type(); //get_lp_type() == NAV_PAGE && get_rp_type() == NAV_PAGE
			//&& get_navpage_number(PAGE_LEFT)==5 && 
			//get_navpage_number(PAGE_RIGHT)==5;

			if(super_page!=SUPER_NAV5)
			{
				for(row=0;row<7;row++)
				{
					str.row  = row;
					str.Y    = row == 6 && (super_page != SUPER_NAV5) ? YY/2 + Y*row + 3 : YY/2+Y*row;
					Out_String(&str,is_space);		   
				}
			}

			if(super_page != SUPER_NAV5)
			{
				SelectObject(hdc,K_Draw.h_pen_b);
				// Horisontal Lines
				MoveToEx(hdc,XX/2,YY/2 + Y*6,NULL);
				LineTo(hdc,dim.x-XX/2,YY/2 + Y*6);
				MoveToEx(hdc,XX/2,YY/2 + Y*6 + 1,NULL);
				LineTo(hdc,dim.x-XX/2,YY/2 + Y*6 + 1);
				MoveToEx(hdc,XX/2,YY/2 + Y*6 + 2,NULL);
				LineTo(hdc,dim.x-XX/2,YY/2 + Y*6 + 2);
				SelectObject(hdc,K_Draw.h_pen_t);
			}
			SelectObject(hdc,K_Draw.h_pen_t);
			if(super_page == SUPER_NAV5)
			{

				RECT _rect; _rect.left=XX/2+X*8;_rect.right=_rect.left+X*15;
				_rect.top=YY/2;_rect.bottom=_rect.top+Y*7+3;
				::FillRect(hdc,&_rect,K_Draw.h_brush_b);
				draw_nav5_page(hdc,XX/2+X*8,X*15,YY/2,Y*7+3,TRUE);

				_rect.left=XX/2;_rect.right=_rect.left+X*8;
				_rect.top=YY/2;_rect.bottom=_rect.top+Y*7+3;
				::FillRect(hdc,&_rect,K_Draw.h_brush_b);

				MoveToEx(hdc, XX/2+X*7+X/2,YY/2,NULL);
				LineTo(hdc, XX/2+X*7+X/2,dim.y-YY/2);

				SelectObject(hdc,(HFONT)K_Draw.h_font);
				for(row=0;row<7;row++)
				{
					str.row  = row;
					str.Y    = YY/2+Y*row;
					Out_String(&str,is_space,0,7);		   
				}

				SelectObject(hdc,(HFONT)K_Draw.h_font);
				str.row  = 6; str.Y = YY/2+Y*6;
				char dis_str[5];
				K_get_string(6,8,8+3,dis_str);
				K_trim_string(dis_str);
				int attr = K_get_attribute(6,8);
				int str_dis_len=0;
				if(attr & ATTRIBUTE_INVERSE) str_dis_len=4;
				else str_dis_len = strlen(dis_str);

				Out_String(&str,is_space,8,str_dis_len);		   			   

				if(K_nav_lines.is_r_menu)
				{
					RECT _rect; _rect.left=XX/2+X*14+X/2; _rect.right=dim.x - XX/2;
					_rect.top=YY/2;_rect.bottom=YY/2+Y*4+1;
					::FillRect(hdc,&_rect,K_Draw.h_brush_b);
					SelectObject(hdc,(HFONT)K_Draw.h_font);
					for(row=0;row<4;row++)
					{
						str.row  = row;
						str.Y    = YY/2+Y*row;
						Out_String(&str,is_space,15,8);		   
					}
					SelectObject(hdc,K_Draw.h_pen_t);
					MoveToEx(hdc, XX/2+X*14+X/2,YY/2,NULL);
					LineTo(hdc, XX/2+X*14+X/2,YY/2+Y*4+1);
					LineTo(hdc, dim.x - XX/2,YY/2+Y*4+1);      
				}
				if(K_nav_lines.is_dto_list)
				{
					str.row  = 6;
					str.Y    = YY/2+6*Y;
					Out_String(&str,is_space,18,5);		      
				}
				if(K_nav_lines.is_msg)
				{
					str.row  = 5;
					str.Y    = YY/2+5*Y;
					Out_String(&str,is_space,8,3);		      		      
				}

			}
			else if(get_lp_type() == NAV_PAGE && get_navpage_number(PAGE_LEFT)==5)
			{
				draw_nav5_page(hdc,XX/2,X*11,YY/2,Y*6,FALSE);       
			}
			else if(get_rp_type() == NAV_PAGE && get_navpage_number(PAGE_RIGHT)==5)
			{
				if(IsBlocked() == FALSE && lp_kind()!=FRONT_PAGE)
					draw_nav5_page(hdc,XX/2+X*12,X*11,YY/2,Y*6,FALSE);
			}
			if(super_page == SUPER_NAV1)
			{
				draw_cdi(hdc,XX/2,YY/2+Y*1,TRUE);
			}
			else if(get_rp_type() == NAV_PAGE && get_navpage_number(PAGE_RIGHT)==1)
			{
				if(IsBlocked() == FALSE && lp_kind()!=FRONT_PAGE)
					draw_cdi(hdc,XX/2+X*12,YY/2+Y*1,FALSE);
			}
			else if(get_lp_type() == NAV_PAGE && get_navpage_number(PAGE_LEFT)==1)
			{					   
				draw_cdi(hdc,XX/2,YY/2+Y*1,FALSE);
			}
			else if(get_rp_type() == APT_PAGE && do_draw_rnws())
			{
				if(IsBlocked() == FALSE && lp_kind()!=FRONT_PAGE)
				{
					nvdb_apt *__apt = get_current_apt();
					if(__apt)
					{
						if(__apt->rws)
							draw_rnws(hdc,XX/2+X*12,X*11,YY/2,Y*6,__apt->rws,
							__apt->rnws_count,__apt->apt3_resolution);
					}
				}
			}
			else if(GLOBAL_STATE == STATE_INSELFTEST_STAGE1 && INSELFTEST_CDI)
			{
				draw_cdi(hdc,XX/2,YY/2+Y*1,FALSE);
			}
			if(
				SCREEN_FORMAT == FORMAT_TWOPARTS
				||
				SCREEN_FORMAT == FORMAT_ONEPART
				)
			{

				POINT old_pos;               
				SelectObject(hdc,K_Draw.h_pen_b);
				MoveToEx(hdc,XX/2,YY/2 + Y*6,&old_pos);
				LineTo(hdc,dim.x-XX/2,YY/2 + Y*6);
				MoveToEx(hdc,XX/2,YY/2 + Y*6 + 2,&old_pos);
				LineTo(hdc,dim.x-XX/2,YY/2 + Y*6 + 2);


				SelectObject(hdc,K_Draw.h_pen_t);
				// Horisontal Line
				MoveToEx(hdc,XX/2,YY/2 + Y*6 + 1,&old_pos);
				LineTo(hdc,dim.x-XX/2,YY/2 + Y*6 + 1);
				// Vertical Line Left
				MoveToEx(hdc, XX/2+X*5+X/2 ,YY/2 + Y*6 + 1,&old_pos);
				LineTo(hdc,XX/2+X*5+X/2,dim.y-YY/2);
				// Vertical Line Right
				MoveToEx(hdc, XX/2+X*17+X/2 ,YY/2 + Y*6 + 1,&old_pos);
				LineTo(hdc,XX/2+X*17+X/2,dim.y-YY/2);
				if(SCREEN_FORMAT == FORMAT_TWOPARTS)
				{
					// Vertical Line Center of Screen 
					MoveToEx(hdc, XX/2+X*11+X/2,YY/2,&old_pos);
					LineTo(hdc, XX/2+X*11+X/2,YY/2 + Y*6 + 1);
				}
			}
			//**********************************************************************************
			SET_OFF_SCREEN (pelement);
		}
	}
}

