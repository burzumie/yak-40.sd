#include "KLNCommon.h"


nv_hdr_t sid_points[40];
int      sid_pts_count;
nv_hdr_t star_points[40];
int      star_pts_count;


using namespace std;
BOOL apt_rnw_nvhdr(nv_hdr_t *rnw_coords,char *rnw_str,nvdb_apt *__apt)
{
	char temp_str[MAX_PATH];
	for(int cur_rnw=0;cur_rnw<__apt->rnws_count;cur_rnw++)
	{
	   //_snprintf(,"%02d%s",(int)apt->rws[i].pNum,get_RNW_des(apt->rws[i].pDes));
	   sprintf(temp_str,"%d%s",(int)__apt->rws[cur_rnw].pNum,get_RNW_des(__apt->rws[cur_rnw].pDes));
	   if(!strcmp(temp_str,rnw_str))
	   {
	      sprintf(rnw_coords->ICAO_ID,"RW%02d%s",(int)__apt->rws[cur_rnw].pNum,get_RNW_des(__apt->rws[cur_rnw].pDes));
		  rnw_coords->Lat = __apt->rws[cur_rnw].bLat;
		  rnw_coords->Lon = __apt->rws[cur_rnw].bLon;
		  return(TRUE);
	   }
       sprintf(temp_str,"%d%s",(int)__apt->rws[cur_rnw].sNum,get_RNW_des(__apt->rws[cur_rnw].sDes));
	   if(!strcmp(temp_str,rnw_str))
	   {
	      sprintf(rnw_coords->ICAO_ID,"RW%02d%s",(int)__apt->rws[cur_rnw].sNum,get_RNW_des(__apt->rws[cur_rnw].sDes));
		  rnw_coords->Lat = __apt->rws[cur_rnw].eLat;
		  rnw_coords->Lon = __apt->rws[cur_rnw].eLon;
		  return(TRUE);
	   }
	   //_snprintf(,"%02d%s",(int)apt->rws[i].sNum,get_RNW_des(apt->rws[i].sDes));
	}
	return(FALSE);
}

char *get_ss_file_name(char *icao_id)
{
	static char sid_star_file[MAX_PATH];
	char KLN_dir[MAX_PATH];
	get_kln90_dir(KLN_dir);
	sprintf(sid_star_file,"%s\\SID_STAR\\%s_%c%c.dat",KLN_dir,SID_STAR_FILENAME,icao_id[0],icao_id[1]);
	return(sid_star_file);
}
void ss_load_on_demand(nvdb_apt *__apt)
{
   if(__apt->sids && __apt->stars) 
      return;
   __apt->sids = new vector<string>;
   __apt->stars = new vector<string>;
   char ss_header[MAX_PATH];
   char ss_value[MAX_PATH];
   sprintf(ss_header,"%s_SIDS",__apt->ICAO_ID);
   GetPrivateProfileString(ss_header,"Count","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
   if(strcmp(ss_value,"NULL"))
   {
      int sids_count = atol(ss_value);
      for(int sid=0;sid<sids_count;sid++)
      {
	     sprintf(ss_header,"%s_SID_%03d",__apt->ICAO_ID,sid);
	     GetPrivateProfileString(ss_header,"Id","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
	     if(!strcmp(ss_value,"NULL")) continue;
	     vector<string>::iterator it1 = find((__apt->sids)->begin(),(__apt->sids)->end(),ss_value);
	     if(it1 == (__apt->sids)->end())
		 {
		    (__apt->sids)->push_back(ss_value);		   
		 }
	  }
	  sort((__apt->sids)->begin(),(__apt->sids)->end());
   } 
   //============= STARS ========================================================
   sprintf(ss_header,"%s_STARS",__apt->ICAO_ID);
   GetPrivateProfileString(ss_header,"Count","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
   if(strcmp(ss_value,"NULL"))
   {
      int stars_count = atol(ss_value);
      for(int star=0;star<stars_count;star++)
      {
	     sprintf(ss_header,"%s_STAR_%03d",__apt->ICAO_ID,star);
	     GetPrivateProfileString(ss_header,"Id","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
	     if(!strcmp(ss_value,"NULL")) continue;
	     vector<string>::iterator it1 = find((__apt->stars)->begin(),(__apt->stars)->end(),ss_value);
	     if(it1 == (__apt->stars)->end())	
		    (__apt->stars)->push_back(ss_value);		   
      }
      sort((__apt->stars)->begin(),(__apt->stars)->end());
  }
}

void ss_load_sid_rnws(nvdb_apt *__apt,char *sid_name)
{
	char ss_header[MAX_PATH];
	char ss_value[MAX_PATH];
	sprintf(ss_header,"%s_SIDS",__apt->ICAO_ID);
	GetPrivateProfileString(ss_header,"Count","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
	__apt->sids->clear();
	if(strcmp(ss_value,"NULL"))
	{
		int sids_count = atol(ss_value);
		for(int sid=0;sid<sids_count;sid++)
		{
			sprintf(ss_header,"%s_SID_%03d",__apt->ICAO_ID,sid);
			GetPrivateProfileString(ss_header,"Id","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
			if(!strcmp(ss_value,"NULL")) continue;
			if(!strcmp(ss_value,sid_name))
			{
			   GetPrivateProfileString(ss_header,"RNW","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
               char rnw_str[MAX_PATH];
			   sprintf(rnw_str,"RW%s",ss_value);
			   (__apt->sids)->push_back(rnw_str);
			}
		}
		sort((__apt->sids)->begin(),(__apt->sids)->end());
	} 
}

void ss_load_star_rnws(nvdb_apt *__apt,char *star_name)
{
	char ss_header[MAX_PATH];
	char ss_value[MAX_PATH];
	sprintf(ss_header,"%s_STARS",__apt->ICAO_ID);
	GetPrivateProfileString(ss_header,"Count","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
	__apt->stars->clear();
	if(strcmp(ss_value,"NULL"))
	{
		int stars_count = atol(ss_value);
		for(int star=0;star<stars_count;star++)
		{
			sprintf(ss_header,"%s_STAR_%03d",__apt->ICAO_ID,star);
			GetPrivateProfileString(ss_header,"Id","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
			if(!strcmp(ss_value,"NULL")) continue;
			if(!strcmp(ss_value,star_name))
			{
				GetPrivateProfileString(ss_header,"RNW","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
				char rnw_str[MAX_PATH];
				sprintf(rnw_str,"RW%s",ss_value);
				(__apt->stars)->push_back(rnw_str);
			}
		}
		sort((__apt->stars)->begin(),(__apt->stars)->end());
	} 
}

void ss_load_sid_points(nvdb_apt *__apt,char *sid_name,char *rnw_name)
{
	char ss_header[MAX_PATH];
	char ss_value[MAX_PATH];
	sprintf(ss_header,"%s_SIDS",__apt->ICAO_ID);
	GetPrivateProfileString(ss_header,"Count","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
	__apt->sids->clear();
	if(strcmp(ss_value,"NULL"))
	{
		int sids_count = atol(ss_value);
		for(int sid=0;sid<sids_count;sid++)
		{
			sprintf(ss_header,"%s_SID_%03d",__apt->ICAO_ID,sid);
			GetPrivateProfileString(ss_header,"Id","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
			if(!strcmp(ss_value,"NULL")) continue;
			if(!strcmp(ss_value,sid_name))
			{
				GetPrivateProfileString(ss_header,"RNW","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
				if(!strcmp(ss_value,rnw_name))
				{
				   for(int i=0;i<100;i++)
				   {
				      char pt_value[MAX_PATH];
				      sprintf(pt_value,"Point%02d",i);
					  GetPrivateProfileString(ss_header,pt_value,"NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
					  if(!strcmp(ss_value,"NULL")) break;
					  K_GetParam(ss_value,"Name=",STRING_T,pt_value);
					  __apt->sids->push_back(pt_value);
					  K_GetParam(ss_value,"Lat=",DOUBLE_T,&sid_points[i].Lat);
					  K_GetParam(ss_value,"Lon=",DOUBLE_T,&sid_points[i].Lon);
					  strcpy(sid_points[i].ICAO_ID,pt_value);
					  sid_pts_count=i+1;
				   }
                   break;
				}
			}
		}
	} 
}

void ss_load_star_points(nvdb_apt *__apt,char *star_name,char *rnw_name)
{
	char ss_header[MAX_PATH];
	char ss_value[MAX_PATH];
	sprintf(ss_header,"%s_STARS",__apt->ICAO_ID);
	GetPrivateProfileString(ss_header,"Count","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
	__apt->stars->clear();
	if(strcmp(ss_value,"NULL"))
	{
		int stars_count = atol(ss_value);
		for(int star=0;star<stars_count;star++)
		{
			sprintf(ss_header,"%s_STAR_%03d",__apt->ICAO_ID,star);
			GetPrivateProfileString(ss_header,"Id","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
			if(!strcmp(ss_value,"NULL")) continue;
			if(!strcmp(ss_value,star_name))
			{
				GetPrivateProfileString(ss_header,"RNW","NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
				if(!strcmp(ss_value,rnw_name))
				{				
                    nv_hdr_t rnw_pt;
					BOOL rnwstr_found = apt_rnw_nvhdr(&rnw_pt,ss_value,__apt);
					for(int i=0;i<100;i++)
					{
						char pt_value[MAX_PATH];
						sprintf(pt_value,"Point%02d",i);
						GetPrivateProfileString(ss_header,pt_value,"NULL",ss_value,sizeof(ss_value),get_ss_file_name(__apt->ICAO_ID));
						if(!strcmp(ss_value,"NULL")) break;
						K_GetParam(ss_value,"Name=",STRING_T,pt_value);
						__apt->stars->push_back(pt_value);
						K_GetParam(ss_value,"Lat=",DOUBLE_T,&star_points[i].Lat);
						K_GetParam(ss_value,"Lon=",DOUBLE_T,&star_points[i].Lon);
						strcpy(star_points[i].ICAO_ID,pt_value);
						star_pts_count=i+1;
					}
					if(rnwstr_found)
					{
					   memcpy(&star_points[star_pts_count],&rnw_pt,sizeof(nv_hdr_t));
					   star_pts_count++;
					}
					break;
				}
			}
		}
	} 
}

void ss_reload(nvdb_apt *__apt)
{
   __apt->sids->clear();
   __apt->stars->clear();
   delete __apt->sids;
   delete __apt->stars;
   __apt->sids=NULL;
   __apt->stars=NULL;
   ss_load_on_demand(__apt);
}
nv_hdr_t *get_sid_point(int index)
{
   return(&sid_points[index]);
}
nv_hdr_t *get_star_point(int index)
{
	return(&star_points[index]);
}