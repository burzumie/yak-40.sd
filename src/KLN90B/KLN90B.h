/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/Engines.h $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "Main.h"
#include "KLNCommon.h"

class SDKLN90B 
{
private:

public:
	SDKLN90B();
	virtual ~SDKLN90B();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
	virtual void Draw(PELEMENT_STATIC_IMAGE pelement);
};

extern SDKLN90B *g_pKLN90B;
