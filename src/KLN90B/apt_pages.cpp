#include "KLNCommon.h"

using namespace std;

extern nav_db_t nav_db;


nvdb_apt *current_apt = NULL;

enum {COMPLETE_LIST,NR_LIST};
static DWORD current_list = COMPLETE_LIST;
static DWORD  apt_list_index=0;
static DWORD  in_nr_list_pos;

extern map<unsigned long,vector<int> >icao_index;
extern nav_db_t nav_db;

extern nr_apt_list nr_apt_l;
extern fpl_t fpls[];
//=========================================================================================================
static int APT_INPUT_MASK = INPUT_RCURSOR|INPUT_PULLSCAN|INPUT_RINNERMINUS|INPUT_RINNERPLUS;

BOOL apt_handle_key(int INPUT_MASK)
{
    return(INPUT_MASK&APT_INPUT_MASK);
}
//==================================================================================
void print_nr_apt(nvdb_apt *__apt,int nr_index,BOOL cursored,char *status)
{
  	if(!__apt)
	{
	   print_str(0,12,ATTRIBUTE_NORMAL," %-4s  NR %d"," ",nr_index+1);
	   K_change_attribute(0,19,4,ATTRIBUTE_NORMAL|ATTRIBUTE_FLASH);
	   print_str(1,12,ATTRIBUTE_NORMAL,"%-11s"," ");
	   print_str(2,12,ATTRIBUTE_NORMAL,"%-11s"," ");
       print_str(3,12,ATTRIBUTE_NORMAL,"%-11s"," ");
       print_str(4,12,ATTRIBUTE_NORMAL,"%-5s---%cFR"," ",0xB0);                
	   print_str(5,12,ATTRIBUTE_NORMAL,"%-4s---.-NM"," ");
	}
	else
	{
	   print_str(0,12,ATTRIBUTE_NORMAL," %-4s  NR %d",__apt->ICAO_ID,nr_index+1);
	   K_change_attribute(0,19,4,ATTRIBUTE_NORMAL|ATTRIBUTE_FLASH);
	   //**********************************************************************************
	   char __name[12];
	   strncpy(__name,__apt->NAME,11);
	   __name[11]='\0';
	   print_str(1,12,ATTRIBUTE_NORMAL,"%-11s",__name);
	   print_str(2,12,ATTRIBUTE_NORMAL,"%-11s",strlen(__apt->NAME)>11?__apt->NAME+11:" ");
	   //**********************************************************************************
	   print_str(3,12,ATTRIBUTE_NORMAL,"%6d' HRD",(LONG)(__apt->rws[0].len*3.281f));
	   print_str(4,12,ATTRIBUTE_NORMAL,"%-5s%03d%c%s"," ",(DWORD)nr_apt_l.list[nr_index].radial,0xB0,
		                          fabs(__add_deg180(get_HDG(),-nr_apt_l.list[nr_index].radial))<=90.0f?"TO":"FR");
	   print_str(5,12,ATTRIBUTE_NORMAL,"%-4s%03d.%dNM"," ",(DWORD)(nr_apt_l.list[nr_index].S/1.852f),
		                            ((DWORD)(nr_apt_l.list[nr_index].S/1.852f*10.0f))%10);
	   print_navaid_arrow(__apt);
	}
    print_str(6,18,ATTRIBUTE_NORMAL,"%s",status);
}
//=========================================================================================================
int apt_compare(nvdb_apt *apt1,nvdb_apt *apt2)
{
   if( !strcmp(apt1->ICAO_ID,apt2->ICAO_ID) )
   {
      if(apt1->freqs_count == apt2->freqs_count && apt1->rnws_count == apt2->rnws_count)
	     return(0);
	  if(apt1->rnws_count > apt2->rnws_count)
	     return(2);
	  else if(apt1->rnws_count < apt2->rnws_count)
	     return(3);
	  else if(apt1->freqs_count > apt2->freqs_count)
	     return(4);
	  else if(apt1->freqs_count < apt2->freqs_count)
	     return(5);
	}
	return(1);
}
int find_in_nr_list(nvdb_apt *c_apt)
{
    
	for(int __i=0;__i<nr_apt_l.nr_apt_count && __i<9;__i++)
	{
		 if(!apt_compare(&nav_db.apts[nr_apt_l.list[__i].index],c_apt))  
			 return(__i);
	}
	return(-1);
}

long get_apt_index(long *apt_id,int elements_count,nvdb_apt *__apt)
{
	for(int i=0;i<elements_count;i++)
	{
		if(!apt_compare(&nav_db.apts[apt_id[i]],__apt))
		   return(i);
	}
	return(-1);
}

int apt_dup_sort(const void *a1,const void *a2)
{
    FLOAT64 Lon,Lat;
    get_PPOS(&Lat,&Lon);
	return int(
	       get_S(Lat,Lon,nav_db.apts[*(long *)a1].Lat,nav_db.apts[*(long *)a1].Lon)
	       -
	       get_S(Lat,Lon,nav_db.apts[*(long *)a2].Lat,nav_db.apts[*(long *)a2].Lon)
	      );
}


long *find_apts_by_icao(char *icao_id_s,long *array_size)
{
    static DWORD apt_indexes[1024];
	int i;
	DWORD icao_id = icao2id(icao_id_s);
	i=0;
	if(icao_index.find(icao_id) != icao_index.end())
	{
		i=0;
		for(vector<int>::iterator it = icao_index[icao_id].begin();it!=icao_index[icao_id].end();it++)
		{
		  int ind = *it;
		  long local_index;
		  if(is_apt_id(ind,&local_index))
		  {
              apt_indexes[i++] = local_index;
		  }
		}
	}
	if(i>0)
	{
	   if(array_size) *array_size=i;
	   return((long *)&apt_indexes);
	}
	
	char reg_exp[15];
	sprintf(reg_exp,"^%s",icao_id_s);
	Regexp re(reg_exp);
    re.CompiledOK();

	for(int j=0;j<nav_db.apts_count;j++)
	{
		if(re.Match(nav_db.apts[j].ICAO_ID))
		{
           if(array_size) 
		      *array_size=1;
		   apt_indexes[0]=j;
		   return((long *)&apt_indexes);
		}
	}
	return(NULL);
}

static int apt_page_num;
static int apt_min_page = 1;
static int apt_max_page = 7;
static int apt_sub_page;
static int apt_last_sub_page;
static BOOL apt7_SID_PAGE=TRUE;
static BOOL apt7_SS_ONLOAD=FALSE;
static int ss_start_offset;
static int ss_row;
enum {SS_SEL_SS,SS_SEL_RNW,SS_SHOW_SS};
static int ss_sel_state = SS_SEL_SS;
static char ss_sel_name[MAX_PATH];

void apt_next_page(void)
{  
	if(apt_page_num == 3 && current_apt->rnws_count && apt_sub_page<apt_last_sub_page)
	{
		apt_sub_page++;
		return;
	}
	if(apt_page_num == 4 && current_apt->freqs_count && apt_sub_page<apt_last_sub_page)
	{
		apt_sub_page++;
		return;
	}
	if(apt_page_num == 7 && apt7_SID_PAGE)
	{
       apt7_SID_PAGE=FALSE;
	   return;
	}
    apt_page_num++; 
	if(apt_page_num>apt_max_page) apt_page_num=apt_min_page;

	if(apt_page_num == 3)
	{
		apt_last_sub_page = current_apt->rnws_count % 2 ? current_apt->rnws_count/2 + 2 :  current_apt->rnws_count/2 + 1;
		if(current_apt->rnws_count)
			apt_sub_page=1;
		else
			apt_sub_page=0;
	}
	if(apt_page_num == 4)
	{
		apt_last_sub_page = current_apt->freqs_count % 5 ? current_apt->freqs_count/5 + 1 :  current_apt->freqs_count/5;
		if(current_apt->freqs_count)
			apt_sub_page=1;
		else
			apt_sub_page=0;
	}
	if(apt_page_num == 7)
	   apt7_SID_PAGE=TRUE;
}

void check_multi_page(void)
{
   if(!current_apt) 
   {
      apt_page_num=1;
	  return;
   }
   if(apt_page_num==3)
   {
      if(current_apt->rnws_count>0)
	  {
	     apt_last_sub_page = current_apt->rnws_count % 2 ? current_apt->rnws_count/2 + 2 :  current_apt->rnws_count/2 + 1;
		 apt_sub_page=2;
	  }
	  else
		  apt_sub_page=0;
   }
   if(apt_page_num==4)
   {
       if(current_apt->freqs_count>0)
	   {
	      apt_sub_page=1;
	      apt_last_sub_page = apt_last_sub_page = current_apt->freqs_count % 5 ? current_apt->freqs_count/5 + 1 :  current_apt->freqs_count/5;
	   }
	   else
	      apt_sub_page=0;
   }

}
void apt_prev_page(void)
{
	if((apt_page_num==3||apt_page_num==4) && apt_sub_page>1)
	{
		apt_sub_page--;   
		return;
	}
	if(apt_page_num==7 && !apt7_SID_PAGE)
	{
	   apt7_SID_PAGE=TRUE;
	   return;
	}
    apt_page_num--;

	if(apt_page_num<apt_min_page) apt_page_num=apt_max_page;

	if(apt_page_num == 3)
	{
		if(current_apt->rnws_count)
		{
			apt_last_sub_page = current_apt->rnws_count % 2 ? current_apt->rnws_count/2 + 2 :  current_apt->rnws_count/2 + 1;
			apt_sub_page = apt_last_sub_page;
		}
		else
			apt_sub_page=0;
	}
	if(apt_page_num == 4)
	{
		if(current_apt->freqs_count)
		{
			apt_last_sub_page = apt_last_sub_page = current_apt->freqs_count % 5 ? current_apt->freqs_count/5 + 1 :  current_apt->freqs_count/5;
			apt_sub_page = apt_last_sub_page;
		}
		else
			apt_sub_page=0;
	}
}

char *comm2text(int type)
{
   switch(type)
   {
   case 0x0001:
	   return("ATIS");
   case 0x0002:
	   return("MCOM");
   case 0x0003:
	   return("UNIC");
   case 0x0004:
	   return("CTAF");
   case 0x0005:
	   return("GRND");
   case 0x0006:
	   return("TWR ");
   case 0x0007:
	   return("CLR ");
   case 0x0008:
	   return("APR ");
   case 0x0009:
	   return("DEP ");
   case 0x000A:
	   return("CTR ");
   case 0x000B:
	   return("FSS "); // ?????
   case 0x000C:
	   return("ASOS");
   case 0x000D:
	   return("AWOS");
   }
   return("");
}

void print_apt(nvdb_apt *__apt,char *status)
{
        if(!__apt)
		{
		   apt_page_num=1;
		   print_str(0,12,ATTRIBUTE_NORMAL," %-10s"," ");
		   print_str(1,12,ATTRIBUTE_NORMAL,"%-11s"," ");
		   print_str(2,12,ATTRIBUTE_NORMAL,"CREATE NEW ");
		   print_str(3,12,ATTRIBUTE_NORMAL,"WPT AT:    ");
           print_str(4,12,ATTRIBUTE_NORMAL,"USER POS?  ");
           print_str(5,12,ATTRIBUTE_NORMAL,"PRES POS?  ");
           print_str(6,18,ATTRIBUTE_NORMAL,"%s",status);
		   update_screen();
		   return;
		}
		if(apt_page_num==3 && apt_sub_page==1)
		{
		    for(int i=0;i<6;i++)
				print_str(i,12,ATTRIBUTE_NORMAL,"%-11s"," ");
		}
		else
		   print_str(0,12,ATTRIBUTE_NORMAL," %-10s",__apt->ICAO_ID);
		//====================================================================
		if(apt_page_num==1)
		{
		   char __name[12];
		   strncpy(__name,__apt->NAME,11);
		   __name[11]='\0';
		   print_str(1,12,ATTRIBUTE_NORMAL,"%-11s",	__name);
		   print_str(2,12,ATTRIBUTE_NORMAL,"%-11s",strlen(__apt->NAME)>11?__apt->NAME+11:" ");
		}
		else if(apt_page_num==2)
		{
			char __city[12];
			strncpy(__city,__apt->CITY,11);
			__city[11]='\0';
			print_str(1,12,ATTRIBUTE_NORMAL,"%-11s",__city);
            print_str(2,12,ATTRIBUTE_NORMAL,"%-11s",strlen(__apt->CITY)>11?__apt->CITY+11:" ");
		}
		else if(apt_page_num==3)
		{
		   if(apt_sub_page>1)
		   {
			  char rt_pat1[4]={0};
			  char rt_pat2[4]={0};

			  int start_rnw_index = 2*(apt_sub_page-2);
		      int count_rnw_print = start_rnw_index+2 <= __apt->rnws_count ? 2 : 1;
		      for(int i=0;i<count_rnw_print;i++)
		      {
		         print_str(2+2*i,12,ATTRIBUTE_NORMAL,"%-3s/%-3s %-3s",
			               __apt->rws[start_rnw_index+i].apt3_line.pDes,
						   __apt->rws[start_rnw_index+i].apt3_line.sDes,
						   __apt->rws[start_rnw_index+i].light_flag?"L":" ");
			     print_str(2+2*i+1,12,ATTRIBUTE_NORMAL,"%6d' HRD",
				           (LONG)(__apt->rws[start_rnw_index+i].len/0.305f));
                 
				 if(__apt->rws[start_rnw_index+i].pattern_flag&4)
					 _snprintf(!i ? rt_pat1 : rt_pat2 ,sizeof(rt_pat1),"%s",__apt->rws[start_rnw_index+i].apt3_line.pDes);
				 else if(__apt->rws[start_rnw_index+i].pattern_flag&32)
					 _snprintf(!i ? rt_pat1 : rt_pat2 ,sizeof(rt_pat1),"%s",__apt->rws[start_rnw_index+i].apt3_line.sDes);
		      }
		      if(count_rnw_print==1)
		      {
			     print_str(4,12,ATTRIBUTE_NORMAL,"%-11s"," ");
			     print_str(5,12,ATTRIBUTE_NORMAL,"%-11s"," ");	      
		      }	
			  if( strlen(rt_pat1) || strlen(rt_pat2) )
			     print_str(1,12,ATTRIBUTE_NORMAL," RT %-3s %-3s",rt_pat1,rt_pat2);
			  else
			     print_str(1,12,ATTRIBUTE_NORMAL,"%-11s"," "); 
		   }
		   else if(!apt_sub_page)
		   {
			   print_str(1,12,ATTRIBUTE_NORMAL,"%-11s"," ");
			   print_str(2,12,ATTRIBUTE_NORMAL,"RUNWAY DATA");
			   print_str(3,12,ATTRIBUTE_NORMAL,"    NOT    ");
			   print_str(4,12,ATTRIBUTE_NORMAL," AVAILABLE ");
			   print_str(5,12,ATTRIBUTE_NORMAL,"%-11s"," ");
		   }
		}
		//====================================================================
		else if(apt_page_num==4)
		{
			if(apt_sub_page>0)
			{
			   int start_freq_index = 5*(apt_sub_page-1);
			   int count_freq_print = start_freq_index+5 <= __apt->freqs_count ? 5 : __apt->freqs_count%5;
			   int i=0;
			   for(i=0;i<count_freq_print;i++)
			   {
                  print_str(i+1,12,ATTRIBUTE_NORMAL,"%-4s %3d.%02d",comm2text(__apt->freqs[start_freq_index+i].type),__apt->freqs[start_freq_index+i].freq/1000000,(__apt->freqs[start_freq_index+i].freq%1000000)/10000);
			   }
			   for(;i<5;i++)
			      print_str(i+1,12,ATTRIBUTE_NORMAL,"%-11s"," ");
			}
			else if(!apt_sub_page)
			{
				print_str(1,12,ATTRIBUTE_NORMAL,"%-11s"," ");
				print_str(2,12,ATTRIBUTE_NORMAL," COMM FREQ ");
				print_str(3,12,ATTRIBUTE_NORMAL," DATA  NOT ");
				print_str(4,12,ATTRIBUTE_NORMAL," AVAILABLE ");
				print_str(5,12,ATTRIBUTE_NORMAL,"%-11s"," ");
			}
		}
		else if(apt_page_num==5)
		{
			print_str(1,12,ATTRIBUTE_NORMAL,"REMARKS:   ");
            print_str(2,12,ATTRIBUTE_NORMAL,"%-11s"," "); 
			print_str(3,12,ATTRIBUTE_NORMAL,"%-11s"," "); 
			print_str(4,12,ATTRIBUTE_NORMAL,"%-11s"," "); 
			print_str(5,12,ATTRIBUTE_NORMAL,"%-11s"," "); 
		}
		else if(apt_page_num==6)
		{
			print_str(1,12,ATTRIBUTE_NORMAL,"%-11s"," ");
			print_str(2,12,ATTRIBUTE_NORMAL,"NO FUEL    "); 
			print_str(3,12,ATTRIBUTE_NORMAL,"%-11s"," "); 
			print_str(4,12,ATTRIBUTE_NORMAL,"NO OXYGEN  "); 
			print_str(5,12,ATTRIBUTE_NORMAL,"NO FEE INFO"); 
		}
		else if(apt_page_num==7)
		{
			int stars_cnt = __apt->stars->size();
			int sids_cnt = __apt->sids->size();
			int s_row;
            char ss_name[MAX_PATH];
		    if(!stars_cnt && !sids_cnt)
			{
				print_str(5,12,ATTRIBUTE_NORMAL,"%-11s"," ");
				print_str(1,12,ATTRIBUTE_NORMAL,"NO SID/STAR");
				print_str(2,12,ATTRIBUTE_NORMAL,"FOR THIS   "); 
				print_str(3,12,ATTRIBUTE_NORMAL,"AIRPORT    "); 
				print_str(4,12,ATTRIBUTE_NORMAL,"IN DATABASE"); 
				print_str(5,12,ATTRIBUTE_NORMAL,"%-11s"," ");
			}
			else
			{
			   char header[MAX_PATH];
				if(apt7_SID_PAGE && !sids_cnt) apt7_SID_PAGE=FALSE;
			   if(!apt7_SID_PAGE && !stars_cnt) apt7_SID_PAGE=TRUE;
			   if(!apt7_SID_PAGE)
			   {
			      if(ss_sel_state == SS_SEL_RNW || ss_sel_state == SS_SHOW_SS)
				  {
				     sprintf(header,"%s",ss_sel_name);
					 header[6]='\0';
					 strcat(header,"-STAR");
					 print_str(0,12,ATTRIBUTE_NORMAL,"%-11s",header);
					 if(ss_sel_state == SS_SEL_RNW)
					    print_str(1,12,ATTRIBUTE_NORMAL,"RUNWAY     ");
				  }
				  else
			         print_str(1,12,ATTRIBUTE_NORMAL,"SELECT STAR");
				  s_row=0;
			      for(int star=ss_start_offset;star<ss_start_offset+3;star++,s_row++)
			      {
					if(star<stars_cnt)
					{
					   sprintf(ss_name,"%s",(__apt->stars->at(star)).c_str());
                       ss_name[6]='\0';
                       if(ss_sel_state == SS_SHOW_SS)
					      print_str(2+s_row-1,12,ATTRIBUTE_NORMAL,"%2d %-8s",star+1,ss_name);
					   else
					      print_str(2+s_row,12,ATTRIBUTE_NORMAL,"%2d %-8s",star+1,ss_name);
					}
					else
					{
						if(ss_sel_state == SS_SHOW_SS)
					       print_str(2+s_row-1,12,ATTRIBUTE_NORMAL,"%-11s"," ");
						else
						   print_str(2+s_row,12,ATTRIBUTE_NORMAL,"%-11s"," ");
					}
			      }
				  if(stars_cnt>3)
				  {
				     sprintf(ss_name,"%s",(__apt->stars->at(stars_cnt-1)).c_str());
					 ss_name[6]='\0';
					 if(ss_sel_state == SS_SHOW_SS)
				        print_str(4,12,ATTRIBUTE_NORMAL,"%2d %-8s",stars_cnt,ss_name);
					 else
			            print_str(5,12,ATTRIBUTE_NORMAL,"%2d %-8s",stars_cnt,ss_name);
				  }
			      else
				  {
					  if(ss_sel_state == SS_SHOW_SS)	         
					     print_str(4,12,ATTRIBUTE_NORMAL,"%-11s"," ");	
					  else
					     print_str(5,12,ATTRIBUTE_NORMAL,"%-11s"," ");	
				  }
				  if(ss_sel_state == SS_SHOW_SS)
				  {
				     int load_in_fpl_row;
					 if(stars_cnt>3)
					    load_in_fpl_row=5;
					 else
				        load_in_fpl_row=1+stars_cnt;
					 print_str(load_in_fpl_row++,12,ATTRIBUTE_NORMAL,"LOAD IN FPL");
					 if(apt7_SS_ONLOAD)
                        K_change_attribute(load_in_fpl_row-1,12,11,ATTRIBUTE_INVERSE|ATTRIBUTE_FLASH);
					 for(;load_in_fpl_row<6;load_in_fpl_row++)
					    print_str(load_in_fpl_row,12,ATTRIBUTE_NORMAL,"%-11s"," ");					    
				  }
			   }
			   else
			   {
				   if(ss_sel_state == SS_SEL_RNW || ss_sel_state == SS_SHOW_SS)
				   {
					   sprintf(header,"%s",ss_sel_name);
					   header[6]='\0';
					   strcat(header,"-SID");
					   print_str(0,12,ATTRIBUTE_NORMAL,"%-11s",header);
					   if(ss_sel_state == SS_SEL_RNW)
					      print_str(1,12,ATTRIBUTE_NORMAL,"RUNWAY     ");
				   }
				   else
			         print_str(1,12,ATTRIBUTE_NORMAL,"SELECT SID ");
				  s_row=0;
			      for(int sid=ss_start_offset;sid<ss_start_offset+3;sid++,s_row++)
			      {
					  if(sid<sids_cnt)
					  {
					     sprintf(ss_name,"%s",(__apt->sids->at(sid)).c_str());
					     ss_name[6]='\0';
						 if(ss_sel_state == SS_SHOW_SS)
					        print_str(2+s_row-1,12,ATTRIBUTE_NORMAL,"%2d %-8s",sid+1,ss_name);
						 else
					        print_str(2+s_row,12,ATTRIBUTE_NORMAL,"%2d %-8s",sid+1,ss_name);
					  }
					  else
					  {
					     if(ss_sel_state == SS_SHOW_SS)
					        print_str(2+s_row-1,12,ATTRIBUTE_NORMAL,"%-11s"," ");
						 else
					        print_str(2+s_row,12,ATTRIBUTE_NORMAL,"%-11s"," ");
					  }
			      }
			      if(sids_cnt>3)
				  {
				     sprintf(ss_name,"%s",(__apt->sids->at(sids_cnt-1)).c_str());
					 ss_name[6]='\0';
					 if(ss_sel_state == SS_SHOW_SS)
				        print_str(4,12,ATTRIBUTE_NORMAL,"%2d %-8s",sids_cnt,ss_name);
					 else
					    print_str(5,12,ATTRIBUTE_NORMAL,"%2d %-8s",sids_cnt,ss_name);
				  }
			      else
				  {
			         if(ss_sel_state == SS_SHOW_SS)
				        print_str(4,12,ATTRIBUTE_NORMAL,"%-11s"," ");	
					 else
				        print_str(5,12,ATTRIBUTE_NORMAL,"%-11s"," ");	
				  }
				  if(ss_sel_state == SS_SHOW_SS)
				  {
					  int load_in_fpl_row;
					  if(sids_cnt>3)
						  load_in_fpl_row=5;
					  else
						  load_in_fpl_row=1+sids_cnt;
					  print_str(load_in_fpl_row++,12,ATTRIBUTE_NORMAL,"LOAD IN FPL");
					  if(apt7_SS_ONLOAD)
						  K_change_attribute(load_in_fpl_row-1,12,11,ATTRIBUTE_INVERSE|ATTRIBUTE_FLASH);
					  for(;load_in_fpl_row<6;load_in_fpl_row++)
						  print_str(load_in_fpl_row,12,ATTRIBUTE_NORMAL,"%-11s"," ");					    
				  }

			   }
			   if(*status != 'A' && ss_row>0)   
			   {
				  if(!apt7_SS_ONLOAD)
				  {
				     if(ss_sel_state == SS_SHOW_SS )
				        K_change_attribute(ss_row-1,12,11,ATTRIBUTE_INVERSE|ATTRIBUTE_FLASH);
				     else
				        K_change_attribute(ss_row,12,11,ATTRIBUTE_INVERSE|ATTRIBUTE_FLASH);
				  }
			   }
			}
		}
		//====================================================================
		if(apt_page_num==1)
		{
		   print_str(3,12,ATTRIBUTE_NORMAL,"%-11s"," ");
		   K_deg deg;
		   K_GetDegMin(__apt->Lat,&deg);
		   print_str(4,12,ATTRIBUTE_NORMAL,"%c %.2d%c%.2d.%.2d'",__apt->Lat<0?'S':'N',(int)fabs(__apt->Lat),0xB0,deg.mins,deg.dec_mins);
		   K_GetDegMin(__apt->Lon,&deg);
		   print_str(5,12,ATTRIBUTE_NORMAL,"%c%.3d%c%.2d.%.2d'",__apt->Lon<0?'W':'E',(int)fabs(__apt->Lon),0xB0,deg.mins,deg.dec_mins);
        }
		else if(apt_page_num==2)
		{
		   print_str(3,12,ATTRIBUTE_NORMAL,"ELV%6dFT",(LONG)(__apt->Alt*3.281));
		   K_change_attribute(3,21,2,ATTRIBUTE_NORMAL|ATTRIBUTE_BSMALL);
		   print_str(4,12,ATTRIBUTE_NORMAL,"%-11s"," "); print_str(5,12,ATTRIBUTE_NORMAL,"%-11s"," ");
		}

		if(*status == 'A')
		{
		   print_str(6,18,ATTRIBUTE_NORMAL,"APT %d",apt_page_num);
		   if(apt_page_num==3)
		   {
			   if(__apt->rnws_count>0)
				   print_str(6,21,ATTRIBUTE_NORMAL,"+");
		   }
		   if(apt_page_num==4)
		   {
			   if(apt_last_sub_page>1)
			      print_str(6,21,ATTRIBUTE_NORMAL,"+");
		   }
		   if(apt_page_num==7)
		   {
	          if(__apt->stars->size()>0 && __apt->sids->size()>0)
			     print_str(6,21,ATTRIBUTE_NORMAL,"+");
			  ss_start_offset=0;
		   }
		}
		else
		{
		   print_str(6,18,ATTRIBUTE_NORMAL,"%s",status);
		   K_change_attribute(6,18,4,ATTRIBUTE_INVERSE);
		}
		if(!(apt_page_num==3 && apt_sub_page==1))
		   print_navaid_arrow(__apt);
		update_screen();
		return;
}



int do_apt_page(int ACTION)
{
    static int cursored = 0;
	static int current_col = 0;
	static int current_row = 0;
    static long written = 0;  
    static long *apt_id = NULL;
	static int dup_ident = 0;
	static DWORD dup_ident_start = 0;
	static int dup_index = 0;

	int icao_ed_col = 13;
	int icao_ed_row = 0;
	int name_ed_col = 12;
	int name_ed_row = 1;

	if(ACTION == ACTION_INIT)
	{
		build_nearest_list();
		apt_page_num=4;
		if(nr_apt_l.nr_apt_count>0)
		{
			current_apt = &nav_db.apts[nr_apt_l.list[0].index];
			ss_load_on_demand(current_apt);
		}
		else
		{
			if(nav_db.apts_count)
			{
				current_apt = &nav_db.apts[0];
			    ss_load_on_demand(current_apt);
			}
			else
			{
				current_apt=NULL;
				do_status_line(ADD_STATUS_MESSAGE,"NO APT WPTS");
			}

		}
		check_multi_page();
		APT_INPUT_MASK = INPUT_RCURSOR|INPUT_PULLSCAN|INPUT_RINNERPLUS|INPUT_RINNERMINUS;
		ss_sel_state = SS_SEL_SS;
		apt7_SS_ONLOAD=FALSE;
		return(0);
	}
	if(ACTION == ACTION_SHOW)
	{
		if(!nav_db.apts_count)
			do_status_line(ADD_STATUS_MESSAGE,"NO APT WPTS");
	}
	if(!nav_db.apts_count)
	{
		current_apt=NULL;
		current_list=COMPLETE_LIST;
		print_ndb(NULL,"APT 1");
		return(0);
    }
   if(ACTION == PAGE_FORCE_CHANGE)
	{
       if(cursored && current_list == COMPLETE_LIST)
       do_apt_page(INPUT_RCURSOR);
	   return(0);
	}
    if(ACTION == ACTION_FREE_RESOURCES)
	{
        current_apt=NULL;
		apt_id=NULL;
        cursored=current_col=current_row=written=dup_ident=
		dup_ident_start=dup_index=0;
		return(0);
	}
	if(ACTION == ACTION_TIMER && !cursored)
	{
		if(!current_apt)
		{
			apt_list_index = 0;
			current_apt = &nav_db.apts[apt_list_index];
			ss_load_on_demand(current_apt);
			check_multi_page();
     	}
		if(current_list == COMPLETE_LIST)
	       print_apt(current_apt,"APT 1");
		else if(current_list == NR_LIST)
		{
		   int ind = find_in_nr_list(current_apt);
		   if(ind>=0)
		   {
		      print_nr_apt(current_apt,ind,cursored,"APT 1");
              in_nr_list_pos=ind;
		   }
		   else
		   {
			   print_apt(current_apt,"APT 1");
			   current_list = COMPLETE_LIST;
		   }
		}
		update_screen();
		return(1);
	}
	if(ACTION == ACTION_TIMER && cursored)
	{
       if(current_list == NR_LIST)
	   {
		   if(nr_apt_l.nr_apt_count > in_nr_list_pos)
		   {
		      apt_list_index = nr_apt_l.list[in_nr_list_pos].index;
		      current_apt = &nav_db.apts[apt_list_index];
			  ss_load_on_demand(current_apt);
			  check_multi_page();
		   }
		   else
		   {
		      current_apt    = NULL;
		      apt_list_index = -1;
		   }
		   print_nr_apt(current_apt,in_nr_list_pos,cursored,"APT 1");
		   K_change_attribute(0,19,4,ATTRIBUTE_NORMAL|ATTRIBUTE_INVERSE);
		   update_screen();
	   }
	   else
	   {
	      if(apt_page_num==7)
		  {
		     if(current_row>0)
			 {
		        print_apt(current_apt,"CRSR ");
				update_screen();
			 }
			 else
			   ss_row=current_row;
		  }
	   }
	}
	//==========================================================================
	if(ACTION_TIMER && dup_ident_start)
	{
	    if(GetTickCount()-dup_ident_start >= 5000)
		{
		    //print_str(6,6,ATTRIBUTE_NORMAL,"ENR-LEG    ");
			dup_ident_start = 0;
     	}
	}
	//==========================================================================
	if(ACTION == INPUT_RCURSOR )
	{
		if(1-cursored)
		{		   
		   if( current_list == COMPLETE_LIST && !K_is_scanpull() )
		   {
		      APT_INPUT_MASK = INPUT_RCURSOR|INPUT_PULLSCAN|INPUT_ROUTERPLUS|INPUT_ROUTERMINUS|INPUT_RINNERPLUS|INPUT_RINNERMINUS;
			  current_col=icao_ed_col;
		      current_row=icao_ed_row;
			  ss_row=current_row;
			  check_multi_page();
			  print_apt(current_apt,"CRSR ");
		      K_change_attribute(current_row,current_col,1,ATTRIBUTE_INVERSE);
		      K_change_attribute(6,18,4,ATTRIBUTE_INVERSE);
		      //=========================================================================
		      apt_id = find_apts_by_icao(current_apt->ICAO_ID,&written);
		      if(apt_id)
			     qsort(apt_id,written,sizeof(*apt_id),apt_dup_sort);
              check_multi_page();
		      print_apt(current_apt,"CRSR ");
		      K_change_attribute(current_row,current_col,1,ATTRIBUTE_INVERSE);
		      K_change_attribute(6,18,4,ATTRIBUTE_INVERSE);
		      if(written>1)
		      {
                 //print_str(6,6,ATTRIBUTE_INVERSE," DUP IDENT ");
				 do_status_line(ADD_STATUS_MESSAGE," DUP IDENT ");
                 dup_index = get_apt_index(apt_id,written,current_apt);
			     apt_list_index = apt_id[dup_index];
			     dup_ident = 1;
	             dup_ident_start = GetTickCount();
			     if(K_is_scanpull())
                    K_change_attribute(icao_ed_row,icao_ed_col,4,ATTRIBUTE_INVERSE);
              }
		      //else 
              //   print_str(6,14,ATTRIBUTE_NORMAL,"   ");

              update_screen();
			  cursored=1;
		   }
		   else if( current_list == NR_LIST )
		   {
			   APT_INPUT_MASK = INPUT_RCURSOR|INPUT_PULLSCAN|INPUT_RINNERPLUS|INPUT_RINNERMINUS;
			   cursored=1;
		   }
		}
		else 
		{
		   if( current_list == COMPLETE_LIST )
		   {
		      if(K_is_scanpull())
			     APT_INPUT_MASK = INPUT_RCURSOR|INPUT_PULLSCAN|INPUT_RINNERPLUS|INPUT_RINNERMINUS;
			  else
			     APT_INPUT_MASK = INPUT_RCURSOR|INPUT_PULLSCAN|INPUT_RINNERMINUS|INPUT_RINNERPLUS;
		      dup_ident_start=0;
			  apt_id=NULL;
		      written=0;
		      dup_ident=0;
			  cursored=0;
			  ss_sel_state = SS_SEL_SS;
			  apt7_SS_ONLOAD=FALSE;
			  ss_reload(current_apt);
		   }
		   else if( current_list == NR_LIST )
		   {
			   APT_INPUT_MASK = INPUT_RCURSOR|INPUT_PULLSCAN|INPUT_RINNERPLUS|INPUT_RINNERMINUS;
			   cursored=0;
		   }
		}
	}
	//=========================================================================
	if(ACTION == INPUT_ROUTERPLUS)
	{
		if(K_is_scanpull()) return(1);
		if(apt_page_num==7 && apt7_SS_ONLOAD) return(1);
		if(current_row==5) 
		{
		  if(apt_page_num==7 && ss_sel_state==SS_SHOW_SS && !apt7_SS_ONLOAD)
		  {
		     apt7_SS_ONLOAD=TRUE;
			 current_row=2;
			 ss_start_offset=0;
			 ss_row=current_row;
		  }
			return(0);		
		}
		if(current_row>0 && apt_page_num==7)
		{
		   int ind_in_ss = ss_start_offset+(current_row-2);
           if(apt7_SID_PAGE)
		   {
		      //SID handling
			   int sids_count = current_apt->sids->size();
			   if(current_row<4 && ind_in_ss<(sids_count-1))
			      current_row++;
			   else
			   {
			      if((ind_in_ss==sids_count-2) && (current_row==4))
				     current_row=5;
				  else
				  {
				     if(ind_in_ss<(sids_count-1))
				        ss_start_offset++;
					 else if(apt_page_num==7 && ss_sel_state==SS_SHOW_SS && !apt7_SS_ONLOAD)
					 {
						 apt7_SS_ONLOAD=TRUE;
						 current_row=2;
						 ss_start_offset=0;
						 ss_row=current_row;
					 }
				  }
			   }
		   }
		   else
		   {
		      //STAR handling
			   int stars_count = current_apt->stars->size();
			   if(current_row<4 && ind_in_ss<(stars_count-1))
				   current_row++;
			   else
			   {
				   if((ind_in_ss==stars_count-2) && (current_row==4))
					   current_row=5;
				   else
				   {
					   if(ind_in_ss<(stars_count-1))
						   ss_start_offset++;
					   else if(apt_page_num==7 && ss_sel_state==SS_SHOW_SS && !apt7_SS_ONLOAD)
					   {
						   apt7_SS_ONLOAD=TRUE;
						   current_row=2;
						   ss_start_offset=0;
						   ss_row=current_row;
					   }

				   }
			   }
		   }
		   ss_row=current_row;
		   return(0);
		}
		if(K_get_char(current_row,current_col) != BLANK_CHAR && current_col<icao_ed_col+3)
		{
            K_change_attribute(current_row,current_col,1,ATTRIBUTE_NORMAL);
			current_col++;
			K_change_attribute(current_row,current_col,1,ATTRIBUTE_INVERSE);
		}
		else
		{
		   if( apt_page_num==7 && (current_apt->sids->size()>0 || current_apt->stars->size()>0))
		   {
		      current_row=2;
			  ss_start_offset=0;
			  ss_row=current_row;
			  APT_INPUT_MASK|=INPUT_ENTER;
			  ss_sel_state = SS_SEL_SS;
			  apt7_SS_ONLOAD=FALSE;
		   }
		}
	}
	if(ACTION == INPUT_ENTER)
	{
	   int ss_index = ss_start_offset+(current_row-2);
	   
	   if(ss_sel_state == SS_SEL_SS)
	   {
	      if(apt7_SID_PAGE)
	      {
	         sprintf(ss_sel_name,"%s",(current_apt->sids->at(ss_index)).c_str());
		     ss_load_sid_rnws(current_apt,ss_sel_name);
	      }
	      else
	      {
	         sprintf(ss_sel_name,"%s",(current_apt->stars->at(ss_index)).c_str());
	         ss_load_star_rnws(current_apt,ss_sel_name);
	      }
		  current_row=2;
		  ss_start_offset=0;
		  ss_row=current_row;
		  ss_sel_state = SS_SEL_RNW;
	   }
	   else if(ss_sel_state == SS_SEL_RNW)
	   {
		   char rnw_name[MAX_PATH];
		   if(apt7_SID_PAGE)
		   {
			   sprintf(rnw_name,"%s",(current_apt->sids->at(ss_index)).c_str());
			   ss_load_sid_points(current_apt,ss_sel_name,rnw_name+2);
		   }
		   else
		   {
			   sprintf(rnw_name,"%s",(current_apt->stars->at(ss_index)).c_str());
			   ss_load_star_points(current_apt,ss_sel_name,rnw_name+2);
		   }
		   current_row=2;
		   ss_start_offset=0;
		   ss_row=current_row;
		   ss_sel_state = SS_SHOW_SS;
	   }
	   else if(apt7_SS_ONLOAD)
	   {
	      if(apt7_SID_PAGE)
		  {
		     insert_into_fpl(&fpls[0],NULL,1);
		     fpls[0].points[1].type = NAVAID_SID_MAIN;
			 ss_sel_name[6]='\0';
			 strcat(ss_sel_name,"-SID");
			 strcpy(fpls[0].points[1].alt_name,ss_sel_name);
			 for(int i=0;i<current_apt->sids->size();i++)
			 {
			    nv_point_t nv_pt;
				nv_pt.buffer_spec = get_sid_point(i);
				nv_pt.type = NAVAID_SS_POINT;
				insert_into_fpl(&fpls[0],&nv_pt,2+i);                
			 }
		  }
		  else
		  {
			  insert_into_fpl(&fpls[0],NULL,fpls[0].point_in_use-1);
			  fpls[0].points[fpls[0].point_in_use-2].type = NAVAID_STAR_MAIN;
			  ss_sel_name[5]='\0';
			  strcat(ss_sel_name,"-STAR");
			  strcpy(fpls[0].points[fpls[0].point_in_use-2].alt_name,ss_sel_name);
			  for(int i=0;i<=current_apt->stars->size();i++)
			  {
				nv_point_t nv_pt;
				nv_pt.buffer_spec = get_star_point(i);
				nv_pt.type = NAVAID_SS_POINT;
				insert_into_fpl(&fpls[0],&nv_pt,fpls[0].point_in_use-1);
			  }
		  }
		  do_apt_page(INPUT_RCURSOR);
	   }
	}
	//==========================================================================
	if(ACTION == INPUT_ROUTERMINUS)
	{
		if(K_is_scanpull()) return(1);

		if(current_col > icao_ed_col)
		{
            K_change_attribute(current_row,current_col,1,ATTRIBUTE_NORMAL);
			current_col--;
			K_change_attribute(current_row,current_col,1,ATTRIBUTE_INVERSE);
		}
		if(apt_page_num==7 && current_row>=2)
		{
		   if(current_row>2)
		      current_row--;
		   else if(ss_start_offset>0)
		      ss_start_offset--;
		   ss_row=current_row;
		}
	}
	//==========================================================================
	if(ACTION == INPUT_RINNERPLUS || ACTION == INPUT_RINNERMINUS)
	{
        if(K_is_scanpull() && dup_ident)
		{
			if(ACTION == INPUT_RINNERPLUS) dup_index = dup_index < (written-1) ? ++dup_index : 0;
			if(ACTION == INPUT_RINNERMINUS) dup_index = dup_index > 0 ? --dup_index : (written-1);
			apt_list_index = apt_id[dup_index];
		    current_apt = &nav_db.apts[apt_list_index];
			ss_load_on_demand(current_apt);
			check_multi_page();
			print_apt(current_apt,"CRSR ");
			K_change_attribute(6,18,4,ATTRIBUTE_INVERSE);
			K_change_attribute(icao_ed_row,icao_ed_col,4,ATTRIBUTE_INVERSE);
		}
		else if(K_is_scanpull() && !cursored)
		{
			if(ACTION == INPUT_RINNERPLUS) 
			{
				if( 
					apt_list_index < (nav_db.apts_count-1)
					&& 
					(current_list==COMPLETE_LIST)
				  )
				{
				   current_apt=&nav_db.apts[++apt_list_index];
				   ss_load_on_demand(current_apt);
				   check_multi_page();
				}
				else if(current_list==NR_LIST)
				{
				    if(in_nr_list_pos < 9)
					{
				        in_nr_list_pos++;
						if(in_nr_list_pos < nr_apt_l.nr_apt_count)
						{
						   apt_list_index=nr_apt_l.list[in_nr_list_pos].index;
						   current_apt=&nav_db.apts[apt_list_index];
						   ss_load_on_demand(current_apt);
						   check_multi_page();
						}
						else
						{
						   apt_list_index=0;
						   current_apt=&nav_db.apts[apt_list_index];
						   check_multi_page();
						   current_list=COMPLETE_LIST;
						}
					}
				}
			}
			//======================================================================================================
			if(ACTION == INPUT_RINNERMINUS)
			{
			    if(!apt_list_index && (current_list==COMPLETE_LIST))
				{
	                if(nr_apt_l.nr_apt_count>0)
					{
					    current_list=NR_LIST;
						in_nr_list_pos=0;
						apt_list_index=nr_apt_l.list[0].index;
						current_apt=&nav_db.apts[apt_list_index];
						check_multi_page();
					}
				}
				else if(current_list==NR_LIST)
				{
				    if(in_nr_list_pos>0)
					{
				        in_nr_list_pos--;
						if(in_nr_list_pos < nr_apt_l.nr_apt_count)
						{
						   apt_list_index=nr_apt_l.list[in_nr_list_pos].index;
						   current_apt=&nav_db.apts[apt_list_index];
						   check_multi_page();
						}
						else
						{
						   current_list=COMPLETE_LIST;
						   check_multi_page();
						}
					}
				}
				else
				{
				    apt_list_index = apt_list_index > 0? --apt_list_index : 0;
					current_apt = &nav_db.apts[apt_list_index];
					ss_load_on_demand(current_apt);
					check_multi_page();
				}
			}
        }
		else if(K_is_scanpull() && cursored)
		{
		    if(ACTION == INPUT_RINNERMINUS && current_list==NR_LIST)
			{
				in_nr_list_pos = in_nr_list_pos ? --in_nr_list_pos : 0;
			}
		    if(ACTION == INPUT_RINNERPLUS && current_list==NR_LIST)
			{
				in_nr_list_pos = in_nr_list_pos<8 ? ++in_nr_list_pos : 8;
			}
		}
		else if(!K_is_scanpull() && cursored)
		{
		   char curr_char = K_get_char(current_row,current_col);
		   curr_char = ACTION == INPUT_RINNERPLUS ? K_next_char(curr_char) : K_prev_char(curr_char);
		   K_set_char(current_row,current_col,curr_char);

		   char curr_id[6];
		   K_get_string(icao_ed_row,icao_ed_col,current_col,curr_id);
		   apt_id = find_apts_by_icao(curr_id,&written);
		   if(apt_id)
		   {
		      qsort(apt_id,written,sizeof(*apt_id),apt_dup_sort);
			  apt_list_index = apt_id[0];
			  current_apt = &nav_db.apts[apt_list_index];
			  ss_load_on_demand(current_apt);
			  check_multi_page();
			  print_apt(current_apt,"CRSR ");
			  K_change_attribute(current_row,current_col,1,ATTRIBUTE_INVERSE);
		      K_change_attribute(6,18,4,ATTRIBUTE_INVERSE);
			  if(written>1)
			  {
                 //print_str(6,6,ATTRIBUTE_INVERSE," DUP IDENT ");
				 do_status_line(ADD_STATUS_MESSAGE," DUP IDENT ");
				 dup_index = 0;
				 dup_ident = 1;
	             dup_ident_start = GetTickCount();
			  }
			  else if(written>0)
			  {
				 dup_ident_start=0;
				 dup_ident = 0;
				 //print_str(6,6,ATTRIBUTE_NORMAL,"ENR-LEG    ");
			  }
			  else
			  {
                 //print_str(6,14,ATTRIBUTE_NORMAL,"   ");
				 dup_ident_start=0;
				 dup_ident = 0;
				 //print_str(6,6,ATTRIBUTE_NORMAL,"ENR-LEG ");
			  }
		      update_screen();
		    }
		    else
		    {
			   print_apt(NULL,"CRSR ");
			   print_str(0,12,ATTRIBUTE_NORMAL," %-10s",curr_id);
			   K_change_attribute(current_row,current_col,1,ATTRIBUTE_INVERSE);
		       K_change_attribute(6,18,4,ATTRIBUTE_INVERSE);
			   //print_str(6,14,ATTRIBUTE_NORMAL,"   ");
			   dup_ident_start=0;
			   //print_str(6,6,ATTRIBUTE_NORMAL,"ENR-LEG ");
		       update_screen();
		    }
		}
		else
		{
		   if(ACTION == INPUT_RINNERPLUS)
		   {
		      apt_next_page();
		   }
		   if(ACTION == INPUT_RINNERMINUS)
		   {
		      apt_prev_page();
		   }
		}
	}
	//==========================================================================
	if(ACTION == INPUT_PULLSCAN)
	{
	    if(current_list == COMPLETE_LIST)
		{
		   if(dup_ident && K_is_scanpull())
		   {
		      K_change_attribute(icao_ed_row,icao_ed_col,4,ATTRIBUTE_INVERSE);
			  //print_str(6,14,ATTRIBUTE_NORMAL,"   ");
			  dup_ident_start=0;
			  //print_str(6,6,ATTRIBUTE_NORMAL,"ENR-LEG ");
			  update_screen();
		   }
		   if(dup_ident && !K_is_scanpull())
		   {
		      K_change_attribute(icao_ed_row,icao_ed_col,4,ATTRIBUTE_NORMAL);
		      K_change_attribute(current_row,current_col,1,ATTRIBUTE_INVERSE);
		      dup_ident_start=0;
		      //print_str(6,6,ATTRIBUTE_NORMAL,"ENR-LEG    ");
		      update_screen();
		   }
		   if(!dup_ident && K_is_scanpull() && cursored)
			   //main_loop(INPUT_RCURSOR);
			   do_apt_page(INPUT_RCURSOR);
		   if(!cursored && K_is_scanpull())
               APT_INPUT_MASK = INPUT_RCURSOR|INPUT_PULLSCAN|INPUT_RINNERPLUS|INPUT_RINNERMINUS;
		   if(!cursored && !K_is_scanpull())
			   APT_INPUT_MASK = INPUT_RCURSOR|INPUT_PULLSCAN|INPUT_RINNERPLUS|INPUT_RINNERMINUS;

		}
        if(!K_is_scanpull() && current_list == NR_LIST)
		{
		   current_list = COMPLETE_LIST;
		   if(cursored)
		      //main_loop(INPUT_RCURSOR);
			  do_apt_page(INPUT_RCURSOR);
		}
	}
	//==========================================================================
	return(1);
}

nvdb_apt *get_current_apt(void)
{
   return(current_apt);
}

BOOL do_draw_rnws(void)
{
   if(apt_page_num==3 && apt_sub_page==1 && current_list==COMPLETE_LIST)
	   return(TRUE);
   else
	   return(FALSE);
}
void apt_print_apt(nvdb_apt *__apt,char *status,int apt_page,int sub_page)
{
   int c_apt_page=apt_page_num;
   int c_apt_sub_page=apt_sub_page;

   apt_page_num=apt_page;
   apt_sub_page=sub_page;
   print_apt(__apt,status);
   apt_page_num=c_apt_page;
   apt_sub_page=c_apt_sub_page;
}



void apt_next_page_ext(int main_page,int sub_page,int *p_main_page,int *p_sub_page,nvdb_apt *__apt)
{  
	static int max_main_page=4;
	static int min_main_page=1;
	
	int __last_page;
	if(main_page==3)
       __last_page=__apt->rnws_count % 2 ? __apt->rnws_count/2 + 2 :  __apt->rnws_count/2 + 1;
	if(main_page==4)
       __last_page=__apt->freqs_count % 5 ? __apt->freqs_count/5 + 1 :  __apt->freqs_count/5;

	if(main_page == 3 && __apt->rnws_count && sub_page<__last_page)
	{		
		if((sub_page+1)==1) *p_sub_page=2; else *p_sub_page=(sub_page+1);
		return;
	}
	if(main_page == 4 && __apt->freqs_count && sub_page<__last_page)
	{
		*p_sub_page=sub_page+1;
		return;
	}
	*p_main_page = main_page+1;

	if(*p_main_page>max_main_page) *p_main_page=min_main_page;

	if(*p_main_page == 3)
	{
		__last_page = __apt->rnws_count % 2 ? __apt->rnws_count/2 + 2 :  __apt->rnws_count/2 + 1;
		if(__apt->rnws_count)
			*p_sub_page=2;
		else
			*p_sub_page=0;
	}
	if(*p_main_page == 4)
	{
		__last_page = __apt->freqs_count % 5 ? __apt->freqs_count/5 + 1 :  __apt->freqs_count/5;
		if(__apt->freqs_count)
			*p_sub_page=1;
		else
			*p_sub_page=0;
	}
}

void apt_prev_page_ext(int main_page,int sub_page,int *p_main_page,int *p_sub_page,nvdb_apt *__apt)
{  
	static int max_main_page=4;
	static int min_main_page=1;
    
	int __last_page;
	if(main_page==3)
		__last_page=__apt->rnws_count % 2 ? __apt->rnws_count/2 + 2 :  __apt->rnws_count/2 + 1;
	if(main_page==4)
		__last_page=__apt->freqs_count % 5 ? __apt->freqs_count/5 + 1 :  __apt->freqs_count/5;

	if((main_page==3||main_page==4) && sub_page>1)
	{
		*p_sub_page = sub_page-1;
		if(main_page==3 && *p_sub_page==1)
			*p_main_page=2;
		return;
	}
	*p_main_page = main_page-1;

	if(*p_main_page<min_main_page) *p_main_page=max_main_page;

	if(*p_main_page == 3)
	{
		if(__apt->rnws_count)
		{
			__last_page = __apt->rnws_count % 2 ? __apt->rnws_count/2 + 2 :  __apt->rnws_count/2 + 1;
			*p_sub_page = __last_page;
		}
		else
			*p_sub_page=0;
	}
	if(*p_main_page == 4)
	{
		if(__apt->freqs_count)
		{
			__last_page = __apt->freqs_count % 5 ? __apt->freqs_count/5 + 1 :  __apt->freqs_count/5;
			*p_sub_page = __last_page;
		}
		else
			*p_sub_page=0;
	}
}
