#include "KLNCommon.h"

//#define KLN_DEBUG

void K_DEBUG(char *format,...)
{
#ifdef KLN_DEBUG
	va_list arg_list;
	va_start(arg_list,format);
	char _text[MAX_PATH];
	vsprintf(_text,format,arg_list);
	FILE *db_fp=fopen("c:\\debug.dat","a+");
	fprintf(db_fp,"%s",_text);
	fclose(db_fp);
	va_end(arg_list);
#endif
}
static char debug_buffer[MAX_PATH];
static CRITICAL_SECTION debug_cs;
static DWORD buffer_refresh;

void K_DEBUG2(char *format,...)
{
#ifdef KLN_DEBUG
	va_list arg_list;
	va_start(arg_list,format);
	EnterCriticalSection(&debug_cs);
	   vsprintf(debug_buffer,format,arg_list);
       buffer_refresh=GetTickCount();
	LeaveCriticalSection(&debug_cs);
	va_end(arg_list);
#endif
}
void K_print_debug_buffer(void)
{
	if(GetTickCount()-buffer_refresh > 10000 )
		return;
	EnterCriticalSection(&debug_cs);
	ImportTable.pWindow->clear_messages(MSG_INSTRUCTOR);
	ImportTable.pWindow->show_message(debug_buffer,MSG_INSTRUCTOR,100,1);
    LeaveCriticalSection(&debug_cs);
}
void init_debug(void)
{
   InitializeCriticalSection(&debug_cs);
   sprintf(debug_buffer,"%s","");
   buffer_refresh=0;
}
void deinit_debug(void)
{
   DeleteCriticalSection(&debug_cs);
}