/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 3 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../../Common/CommonSys.h"
#include "../../Common/CommonVisual.h"
#include "../../Common/Macros.h"
#include "../../Common/MacrosVisual.h"
#include "../../../res/Version.h"
#include "../../Common/CommonAircraft.h"
#include "../../api/ClientDef.h"
#include "../../api/APIEntry.h"
#include "../../Lib/PanelsTools.h"

#ifdef LITE_VERSION
#if		defined(HAVE_MIX_PANEL)
#include "../../../res/3d/vc4_res_m.h"
#elif	defined(HAVE_PLF_PANEL)
#include "../../../res/3d/vc4_res_p.h"
#elif	defined(HAVE_RED_PANEL)
#include "../../../res/3d/vc4_res_r.h"
#endif
#else
#include "../../../res/3d/vc4_res.h"
#endif

