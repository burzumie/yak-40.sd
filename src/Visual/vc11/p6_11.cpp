/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "Main.h"

double FSAPI icb_IcoLit(PELEMENT_ICON pelement);
double FSAPI icb_IcoIntl(PELEMENT_ICON pelement);
double FSAPI icb_IcoIntlLit(PELEMENT_ICON pelement);

static MAKE_ICB(icb_kmplherz		,SHOW_BTN(BTN_KMP1ID  		,2))
static MAKE_ICB(icb_obsleft 		,SHOW_HND(HND_OBS6			,20))
static MAKE_ICB(icb_kmp1vorl  		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP1VOR		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmp1pwrl  		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP1PWR		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst1l  		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST1		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst2l 		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST2		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst3l  		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST3		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmp1vor  		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP1VOR		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmp1pwr  		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP1PWR		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst1  		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST1		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst2  		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST2		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst3  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST3		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl1scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ1	,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl2scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ2	,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl3scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ3	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl4scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ4	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl5scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ5	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl1scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ1	,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl2scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ2	,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl3scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ3	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl4scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ4	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpl5scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ5	,10)} else {HIDE_IMAGE(pelement); return -1;})

static BOOL FSAPI mcb_kmp1_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	ImportTable.pPanels->panel_window_toggle(IDENT_P24);
	return true;
}

static MAKE_MSCB(mcb_azs_kmp1vor		    ,AZS_TGL(AZS_KMP1VOR    				))
static MAKE_MSCB(mcb_azs_kmp1pwr		    ,AZS_TGL(AZS_KMP1PWR    				))

static MAKE_MSCB(mcb_btn_kmp1test1		    ,PRS_BTN(BTN_KMP1TEST1  				))
static MAKE_MSCB(mcb_btn_kmp1test2		    ,PRS_BTN(BTN_KMP1TEST2  				))
static MAKE_MSCB(mcb_btn_kmp1test3		    ,PRS_BTN(BTN_KMP1TEST3  				))

// ���������
static BOOL FSAPI mcb_glt_kmpfreq_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV6_FREQ1);
	int f2=(int)POS_GET(POS_NAV6_FREQ2);
	int f3=(int)POS_GET(POS_NAV6_FREQ3);
	int f4=(int)POS_GET(POS_NAV6_FREQ4);
	int f5=(int)POS_GET(POS_NAV6_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(f5==5) {
			f5=0;
		} else {
			f4--;
			if(f4<0)
				f4=9;
			f5=5;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);

		return TRUE;
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {

		HND_DEC(HND_KMP1FREQ);

		if(f1==1&&f2==0&&f3==8) {
			f1=1;
			f2=1;
			f3=7;
		} else {
			// NAV3
			f3--;
			if(f3<0) {
				f3+=9;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);
	}
	return TRUE;
}

static BOOL FSAPI mcb_glt_kmpfreq_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV6_FREQ1);
	int f2=(int)POS_GET(POS_NAV6_FREQ2);
	int f3=(int)POS_GET(POS_NAV6_FREQ3);
	int f4=(int)POS_GET(POS_NAV6_FREQ4);
	int f5=(int)POS_GET(POS_NAV6_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(f5==5) {
			f4++;
			if(f4>9)
				f4=0;
			f5=0;
		} else {
			f5=5;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);

		return TRUE;
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {

		HND_INC(HND_KMP1FREQ);

		if(f1==1&&f2==1&&f3==7) {
			f1=1;
			f2=0;
			f3=8;
		} else {
			// NAV3
			f3++;
			if(f3>9) {
				f3=0;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);
	}
	return TRUE;
}

static BOOL FSAPI mcb_btn_kmp1id(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	BTN_TGL(BTN_KMP1ID);
	return TRUE;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_KMP1VOR				))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_KMP1PWR				))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_KMP1TEST1	        ))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_KMP1TEST2	        ))
static MAKE_TCB(tcb_05	,BTN_TT(BTN_KMP1TEST3	        ))
static MAKE_TCB(tcb_06	,BTN_TT(BTN_KMP1ID				))
static MAKE_TCB(tcb_08	,HND_TT(HND_KMP1FREQ	        ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MAKE_TTA(tcb_02) 
MAKE_TTA(tcb_03)
MAKE_TTA(tcb_04)
MAKE_TTA(tcb_05)
MAKE_TTA(tcb_06)
MAKE_TTA(tcb_08)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p6_11,HELP_NONE,0,0)
MOUSE_PBOX(30,30,VC11_BCK_MPNAV1_D_SX,VC11_BCK_MPNAV1_D_SY,CURSOR_NONE,MOUSE_LR,mcb_kmp1_big)

MOUSE_TBOX(  "1",  94,   28, 33, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp1vor)
MOUSE_TBOX(  "2", 363,   28, 33, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp1pwr)
MOUSE_TBOX(  "3", 152,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1test1)
MOUSE_TBOX(  "4", 223,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1test2)
MOUSE_TBOX(  "5", 297,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1test3)
MOUSE_TBOX(  "6", 349,   87, 90, 90,CURSOR_HAND,MOUSE_LR ,mcb_btn_kmp1id)
MOUSE_TSHB(  "7",  20,   84,150, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreq_l,mcb_glt_kmpfreq_r)

MOUSE_END       

extern PELEMENT_HEADER p6_11_list; 
GAUGE_HEADER_FS700_EX(VC11_BCK_MPNAV1_D_SX, "p6_11", &p6_11_list, rect_p6_11, 0, 0, 0, 0, p6_11); 

MY_ICON2	(p6_11azs_kmp1vor	, VC11_AZS_MPFREQDME_D_00		,NULL             		,  94,   28,icb_kmp1vor    	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11azs_kmp1vorl	, VC11_AZS_MPFREQDMELIT_D_00	,&l_p6_11azs_kmp1vor	,  94,   28,icb_kmp1vorl    , 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11azs_kmp1pwr	, VC11_AZS_MPFREQPWR_D_00		,&l_p6_11azs_kmp1vorl	, 363,   28,icb_kmp1pwr    	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11azs_kmp1pwrl	, VC11_AZS_MPFREQPWRLIT_D_00	,&l_p6_11azs_kmp1pwr	, 363,   28,icb_kmp1pwrl    , 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl1		, VC11_SCL_MPFREQ1_D_00			,&l_p6_11azs_kmp1pwrl	, 181,   16,icb_kmpl1scl  	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl1l	, VC11_SCL_MPFREQ1LIT_D_00		,&l_p6_11scl_kmpl1		, 181,   16,icb_kmpl1scll 	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl2		, VC11_SCL_MPFREQ2_D_00			,&l_p6_11scl_kmpl1l		, 204,   16,icb_kmpl2scl  	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl2l	, VC11_SCL_MPFREQ2LIT_D_00		,&l_p6_11scl_kmpl2		, 204,   16,icb_kmpl2scll 	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl3		, VC11_SCL_MPFREQ3_D_00			,&l_p6_11scl_kmpl2l		, 227,   16,icb_kmpl3scl  	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl3l	, VC11_SCL_MPFREQ3LIT_D_00		,&l_p6_11scl_kmpl3		, 227,   16,icb_kmpl3scll 	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl4		, VC11_SCL_MPFREQ4_D_00			,&l_p6_11scl_kmpl3l		, 250,   16,icb_kmpl4scl  	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl4l	, VC11_SCL_MPFREQ4LIT_D_00		,&l_p6_11scl_kmpl4		, 250,   16,icb_kmpl4scll 	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl5		, VC11_SCL_MPFREQ5_D_00			,&l_p6_11scl_kmpl4l		, 286,   16,icb_kmpl5scl  	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl5l	, VC11_SCL_MPFREQ5LIT_D_00		,&l_p6_11scl_kmpl5		, 286,   16,icb_kmpl5scll 	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11IcoIntlLit	, VC11_BCK_MPNAV1INTLLIT_D		,&l_p6_11scl_kmpl5l		,   0,    0,icb_IcoIntlLit	,    PANEL_LIGHT_MAX,p6_11)
MY_ICON2	(p6_11IcoIntl		, VC11_BCK_MPNAV1INTL_D			,&l_p6_11IcoIntlLit		,   0,    0,icb_IcoIntl		,    PANEL_LIGHT_MAX,p6_11)
MY_ICON2	(p6_11IcoLit		, VC11_BCK_MPNAV1LIT_D			,&l_p6_11IcoIntl		,   0,    0,icb_IcoLit		,    PANEL_LIGHT_MAX,p6_11)
MY_ICON2	(p6_11Ico			, VC11_BCK_MPNAV1_D				,&l_p6_11IcoLit			,   0,    0,icb_Ico			,    PANEL_LIGHT_MAX,p6_11)
MY_STATIC2	(p6_11bg,p6_11_list	, VC11_BCK_MPNAV1_D				,&l_p6_11Ico			,p6_11)
