/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "Main.h"

double FSAPI icb_IcoLit(PELEMENT_ICON pelement);
double FSAPI icb_IcoIntl(PELEMENT_ICON pelement);
double FSAPI icb_IcoIntlLit(PELEMENT_ICON pelement);

static MAKE_ICB(icb_kmprherz		,SHOW_BTN(BTN_KMP2ID  		,2))
static MAKE_ICB(icb_kmp2vorl  		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP2VOR		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmp2pwrl  		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP2PWR		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst1l  		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST1		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst2l 		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST2		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst3l  		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST3		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmp2vor  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP2VOR		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmp2pwr  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP2PWR		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst1  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST1		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst2  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST2		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmptst3  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP2TEST3		,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr1scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ1	,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr2scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ2	,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr3scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ3	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr4scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ4	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr5scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ5	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr1scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ1	,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr2scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ2	,2)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr3scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ3	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr4scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ4	,10)} else {HIDE_IMAGE(pelement); return -1;})
static MAKE_ICB(icb_kmpr5scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV7_FREQ5	,10)} else {HIDE_IMAGE(pelement); return -1;})

static MAKE_MSCB(mcb_azs_kmp2vor		,AZS_TGL(AZS_KMP2VOR    	))
static MAKE_MSCB(mcb_azs_kmp2pwr		,AZS_TGL(AZS_KMP2PWR    	))
static MAKE_MSCB(mcb_btn_kmp2id         ,BTN_TGL(BTN_KMP2ID			))
static MAKE_MSCB(mcb_btn_kmp2test1		,PRS_BTN(BTN_KMP2TEST1  	))
static MAKE_MSCB(mcb_btn_kmp2test2		,PRS_BTN(BTN_KMP2TEST2  	))
static MAKE_MSCB(mcb_btn_kmp2test3		,PRS_BTN(BTN_KMP2TEST3  	))

static BOOL FSAPI mcb_glt_kmpfreq_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV7_FREQ1);
	int f2=(int)POS_GET(POS_NAV7_FREQ2);
	int f3=(int)POS_GET(POS_NAV7_FREQ3);
	int f4=(int)POS_GET(POS_NAV7_FREQ4);
	int f5=(int)POS_GET(POS_NAV7_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(f5==5) {
			f5=0;
		} else {
			f4--;
			if(f4<0)
				f4=9;
			f5=5;
		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);

		return TRUE;
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {

		HND_DEC(HND_KMP2FREQ);

		if(f1==1&&f2==0&&f3==8) {
			f1=1;
			f2=1;
			f3=7;
		} else {
			// NAV3
			f3--;
			if(f3<0) {
				f3=9;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;

		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);

	}
	return TRUE;
}

static BOOL FSAPI mcb_glt_kmpfreq_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV7_FREQ1);
	int f2=(int)POS_GET(POS_NAV7_FREQ2);
	int f3=(int)POS_GET(POS_NAV7_FREQ3);
	int f4=(int)POS_GET(POS_NAV7_FREQ4);
	int f5=(int)POS_GET(POS_NAV7_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(f5==5) {
			f4++;
			if(f4>9)
				f4=0;
			f5=0;
		} else {
			f5=5;
		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);

		return TRUE;
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {

		HND_INC(HND_KMP2FREQ);

		if(f1==1&&f2==1&&f3==7) {
			f1=1;
			f2=0;
			f3=8;
		} else {
			// NAV3
			f3++;
			if(f3>9) {
				f3=0;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}

		POS_SET(POS_NAV7_FREQ1,f1);
		POS_SET(POS_NAV7_FREQ2,f2);
		POS_SET(POS_NAV7_FREQ3,f3);
		POS_SET(POS_NAV7_FREQ4,f4);
		POS_SET(POS_NAV7_FREQ5,f5);
	}
	return TRUE;
}

static BOOL FSAPI mcb_kmp2_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	ImportTable.pPanels->panel_window_toggle(IDENT_P24);
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_KMP2VOR				))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_KMP2PWR				))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_KMP2TEST1	        ))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_KMP2TEST2	        ))
static MAKE_TCB(tcb_05	,BTN_TT(BTN_KMP2TEST3	        ))
static MAKE_TCB(tcb_06	,BTN_TT(BTN_KMP2ID				))
static MAKE_TCB(tcb_08	,HND_TT(HND_KMP2FREQ	        ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MAKE_TTA(tcb_02) 
MAKE_TTA(tcb_03)
MAKE_TTA(tcb_04)
MAKE_TTA(tcb_05)
MAKE_TTA(tcb_06)
MAKE_TTA(tcb_08)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p7_12,HELP_NONE,0,0)
MOUSE_PBOX(30,30,VC11_BCK_MPNAV2_D_SX,VC11_BCK_MPNAV2_D_SY,CURSOR_NONE,MOUSE_LR,mcb_kmp2_big)

MOUSE_TBOX(  "1",  94,   28, 33, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp2vor)
MOUSE_TBOX(  "2", 363,   28, 33, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp2pwr)
MOUSE_TBOX(  "3", 152,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2test1)
MOUSE_TBOX(  "4", 223,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2test2)
MOUSE_TBOX(  "5", 297,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2test3)
MOUSE_TBOX(  "6", 349,   87, 90, 90,CURSOR_HAND,MOUSE_LR ,mcb_btn_kmp2id)
MOUSE_TSHB(  "7",  20,   84,150, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreq_l,mcb_glt_kmpfreq_r)

MOUSE_END       

extern PELEMENT_HEADER p7_12_list; 
GAUGE_HEADER_FS700_EX(VC11_BCK_MPNAV2_D_SX, "p7_12", &p7_12_list, rect_p7_12, 0, 0, 0, 0, p7_12); 

MY_ICON2	(p7_12azs_kmp1vor	, VC11_AZS_MPFREQDME_D_00		,NULL             		,  94,   28,icb_kmp2vor    	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12azs_kmp1vorl	, VC11_AZS_MPFREQDMELIT_D_00	,&l_p7_12azs_kmp1vor	,  94,   28,icb_kmp2vorl    , 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12azs_kmp1pwr	, VC11_AZS_MPFREQPWR_D_00		,&l_p7_12azs_kmp1vorl	, 363,   28,icb_kmp2pwr    	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12azs_kmp1pwrl	, VC11_AZS_MPFREQPWRLIT_D_00	,&l_p7_12azs_kmp1pwr	, 363,   28,icb_kmp2pwrl    , 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl1		, VC11_SCL_MPFREQ1_D_00			,&l_p7_12azs_kmp1pwrl	, 181,   16,icb_kmpr1scl  	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl1l	, VC11_SCL_MPFREQ1LIT_D_00		,&l_p7_12scl_kmpl1		, 181,   16,icb_kmpr1scll 	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl2		, VC11_SCL_MPFREQ2_D_00			,&l_p7_12scl_kmpl1l		, 204,   16,icb_kmpr2scl  	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl2l	, VC11_SCL_MPFREQ2LIT_D_00		,&l_p7_12scl_kmpl2		, 204,   16,icb_kmpr2scll 	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl3		, VC11_SCL_MPFREQ3_D_00			,&l_p7_12scl_kmpl2l		, 227,   16,icb_kmpr3scl  	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl3l	, VC11_SCL_MPFREQ3LIT_D_00		,&l_p7_12scl_kmpl3		, 227,   16,icb_kmpr3scll 	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl4		, VC11_SCL_MPFREQ4_D_00			,&l_p7_12scl_kmpl3l		, 250,   16,icb_kmpr4scl  	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl4l	, VC11_SCL_MPFREQ4LIT_D_00		,&l_p7_12scl_kmpl4		, 250,   16,icb_kmpr4scll 	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl5		, VC11_SCL_MPFREQ5_D_00			,&l_p7_12scl_kmpl4l		, 286,   16,icb_kmpr5scl  	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl5l	, VC11_SCL_MPFREQ5LIT_D_00		,&l_p7_12scl_kmpl5		, 286,   16,icb_kmpr5scll 	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12IcoIntlLit	, VC11_BCK_MPNAV2INTLLIT_D		,&l_p7_12scl_kmpl5l		,   0,    0,icb_IcoIntlLit	,    PANEL_LIGHT_MAX,p7_12)
MY_ICON2	(p7_12IcoIntl		, VC11_BCK_MPNAV2INTL_D			,&l_p7_12IcoIntlLit		,   0,    0,icb_IcoIntl		,    PANEL_LIGHT_MAX,p7_12)
MY_ICON2	(p7_12IcoLit		, VC11_BCK_MPNAV2LIT_D			,&l_p7_12IcoIntl		,   0,    0,icb_IcoLit		,    PANEL_LIGHT_MAX,p7_12)
MY_ICON2	(p7_12Ico			, VC11_BCK_MPNAV2_D				,&l_p7_12IcoLit			,   0,    0,icb_Ico			,    PANEL_LIGHT_MAX,p7_12)
MY_STATIC2	(p7_12bg,p7_12_list	, VC11_BCK_MPNAV2_D				,&l_p7_12Ico			,p7_12)
