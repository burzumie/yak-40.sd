/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

BOOL FSAPI mcb_dme2_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_ICB(icb_dmeleft1m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME71M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft2m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME72M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft3m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME73M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft4m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME74M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft5m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME75M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft1		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME71 )>-1?POS_GET(POS_DME71)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft2		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME72 )>-1?POS_GET(POS_DME72)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft3		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME73 )>-1?POS_GET(POS_DME73)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft4		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME74 )>-1?POS_GET(POS_DME74)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft5		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME75 )>-1?POS_GET(POS_DME75)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dme1			,SHOW_GLT(GLT_DME7	,2))

double FSAPI icb_IcoI(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):-1;
}

//////////////////////////////////////////////////////////////////////////

static BOOL FSAPI mcb_glt_dme(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	GLT_TGL(GLT_DME7);
	return TRUE;
}

static MAKE_TCB(tcb_01	,GLT_TT(GLT_DME7			    ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p7_13,HELP_NONE,0,0)
MOUSE_PBOX(30,30,P20_BCK_DME2_D_SX,P20_BCK_DME2_D_SY,CURSOR_HAND,MOUSE_LR,mcb_dme2_big)
MOUSE_TBOX( "1", 81,  128, 55, 55,CURSOR_HAND,MOUSE_LR,mcb_glt_dme)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p7_13_list; 
GAUGE_HEADER_FS700_EX(P20_BCK_DME2_D_SX, "p7_13", &p7_13_list, rect_p7_13, 0, 0, 0, 0, p7_13); 

MY_ICON2	(p12_scl_dmeleft1m	, P20_SCL_1DMELEFT1_D__ ,NULL              		,  20,   46,icb_dmeleft1m  ,    PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_scl_dmeleft1	, P20_SCL_DMELEFT1_D_00	,&l_p12_scl_dmeleft1m	,  20,   46,icb_dmeleft1   , 10*PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_scl_dmeleft2m	, P20_SCL_1DMELEFT2_D__ ,&l_p12_scl_dmeleft1	,  61,   46,icb_dmeleft2m  ,    PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_scl_dmeleft2	, P20_SCL_DMELEFT2_D_00	,&l_p12_scl_dmeleft2m	,  61,   46,icb_dmeleft2   , 10*PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_scl_dmeleft3m	, P20_SCL_1DMELEFT3_D__ ,&l_p12_scl_dmeleft2	,  87,   46,icb_dmeleft3m  ,    PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_scl_dmeleft3	, P20_SCL_DMELEFT3_D_00	,&l_p12_scl_dmeleft3m	,  87,   46,icb_dmeleft3   , 10*PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_scl_dmeleft4m	, P20_SCL_1DMELEFT4_D__ ,&l_p12_scl_dmeleft3	, 115,   46,icb_dmeleft4m  ,    PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_scl_dmeleft4	, P20_SCL_DMELEFT4_D_00	,&l_p12_scl_dmeleft4m	, 115,   46,icb_dmeleft4   , 10*PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_scl_dmeleft5m	, P20_SCL_1DMELEFT5_D__ ,&l_p12_scl_dmeleft4	, 147,   46,icb_dmeleft5m  ,    PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_scl_dmeleft5	, P20_SCL_DMELEFT5_D_00	,&l_p12_scl_dmeleft5m	, 147,   46,icb_dmeleft5   , 10*PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_knb_dme1		, P20_KNB_DME1_D_00		,&l_p12_scl_dmeleft5	,  81,  128,icb_dme1       , 2 *PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_4IcoI			, P20_BCK_DME2INTL_D	,&l_p12_knb_dme1		,   0,    0,icb_IcoI       ,    PANEL_LIGHT_MAX ,p7_13)
MY_ICON2	(p12_4Ico			, P20_BCK_DME2_D		,&l_p12_4IcoI			,   0,    0,icb_Ico        ,    PANEL_LIGHT_MAX ,p7_13)
MY_STATIC2	(p12_4bg,p7_13_list	, P20_BCK_DME2_D		,&l_p12_4Ico			,p7_13)

