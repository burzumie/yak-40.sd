/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

static double FSAPI icb_obsleft(PELEMENT_ICON pelement)
{
	CHK_BRT();
	double val=HND_GET(HND_OBS7);
	if(val>9)
		val=0;
	return val+POS_GET(POS_PANEL_STATE)*10; 
}

double FSAPI icb_obsr1scll(PELEMENT_ICON pelement);
double FSAPI icb_obsr2scll(PELEMENT_ICON pelement);
double FSAPI icb_obsr3scll(PELEMENT_ICON pelement);
double FSAPI icb_obsr1scl(PELEMENT_ICON pelement);
double FSAPI icb_obsr2scl(PELEMENT_ICON pelement);
double FSAPI icb_obsr3scl(PELEMENT_ICON pelement);

double FSAPI icb_Ico(PELEMENT_ICON pelement);

double FSAPI icb_IcoLit(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return PWR_GET(PWR_KURSMP2)&&!POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):-1;
}

double FSAPI icb_IcoIntl(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return !PWR_GET(PWR_KURSMP2)&&POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):-1;
}

double FSAPI icb_IcoIntlLit(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return POS_GET(POS_PANEL_LANG)&&PWR_GET(PWR_KURSMP2)?POS_GET(POS_PANEL_STATE):-1;
}

BOOL FSAPI mcb_obs2_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_obs_l(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_obs_r(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01	,HND_TT(HND_OBS7			    ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p7_11,HELP_NONE,0,0)
MOUSE_PBOX(30,30,P20_BCK_MPOBS2_D_SX,P20_BCK_MPOBS2_D_SY,CURSOR_HAND,MOUSE_LR,mcb_obs2_big)
MOUSE_TSHB(  "1", 252,  75, 90, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_obs_l,mcb_glt_obs_r)
MOUSE_END       

extern PELEMENT_HEADER p7_11_list; 
GAUGE_HEADER_FS700_EX(P20_BCK_MPOBS2_D_SX, "p7_11", &p7_11_list, rect_p7_11, 0, 0, 0, 0, p7_11); 

MY_ICON2	(p7_11Knb          	, P20_KNB_OBS_D_00			,NULL            	,      257,       89,icb_obsleft 		,10* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl3Lit      	, P20_SCL_OBS3LIT_D_00		,&l_p7_11Knb        ,      211,       35,icb_obsr3scll		,10* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl2Lit      	, P20_SCL_OBS2LIT_D_00		,&l_p7_11Scl3Lit    ,      171,       35,icb_obsr2scll		,10* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl1Lit      	, P20_SCL_OBS1LIT_D_00		,&l_p7_11Scl2Lit    ,      133,       35,icb_obsr1scll		, 4* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl3      	, P20_SCL_OBS3_D_00			,&l_p7_11Scl1Lit    ,      211,       35,icb_obsr3scl		,10* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl2      	, P20_SCL_OBS2_D_00			,&l_p7_11Scl3      	,      171,       35,icb_obsr2scl		,10* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl1      	, P20_SCL_OBS1_D_00			,&l_p7_11Scl2      	,      133,       35,icb_obsr1scl		, 4* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11IcoIntlLit	, P20_BCK_MPOBS2INTLLIT_D	,&l_p7_11Scl1      	,        0,        0,icb_IcoIntlLit		,    PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11IcoIntl		, P20_BCK_MPOBS2INTL_D		,&l_p7_11IcoIntlLit	,        0,        0,icb_IcoIntl		,    PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11IcoLit		, P20_BCK_MPOBS2LIT_D		,&l_p7_11IcoIntl	,        0,        0,icb_IcoLit			,    PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Ico			, P20_BCK_MPOBS2_D			,&l_p7_11IcoLit		,        0,        0,icb_Ico			,    PANEL_LIGHT_MAX,p7_11)
MY_STATIC2	(p7_11bg,p7_11_list	, P20_BCK_MPOBS2_D			,&l_p7_11Ico		,p7_11)
