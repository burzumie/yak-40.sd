/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

#define OFFX	1053
#define OFFY	0

static MAKE_ICB(icb_presschrate		,SHOW_HND(HND_PRESS_CHANGE_RATE,46))
static MAKE_ICB(icb_pressdiff		,SHOW_HND(HND_PRESS_DIFF	,20))
static MAKE_ICB(icb_pressbegin		,SHOW_HND(HND_PRESS_BEGIN	,20))
static MAKE_ICB(icb_2077pwr			,SHOW_AZS(AZS_2077PWR   	,2))
static MAKE_ICB(icb_presschng		,SHOW_POS(POS_PRESSRATE		,46))

static NONLINEARITY tbl_difpress[]={
	{{1159-OFFX,344-OFFY}, 0.0,0},
	{{1154-OFFX,310-OFFY}, 0.1,0},
	{{1173-OFFX,282-OFFY}, 0.2,0},
	{{1209-OFFX,271-OFFY}, 0.3,0},
	{{1244-OFFX,285-OFFY}, 0.4,0},
	{{1263-OFFX,315-OFFY}, 0.5,0},
	{{1256-OFFX,348-OFFY}, 0.6,0}
};

static NONLINEARITY tbl_pressbeg[]={
	{{1387-OFFX,352-OFFY}, 430,0},
	{{1386-OFFX,340-OFFY}, 450,0},
	{{1388-OFFX,316-OFFY}, 500,0},
	{{1399-OFFX,295-OFFY}, 550,0},
	{{1424-OFFX,284-OFFY}, 600,0},
	{{1451-OFFX,286-OFFY}, 650,0},
	{{1475-OFFX,299-OFFY}, 700,0},
	{{1489-OFFX,320-OFFY}, 750,0},
	{{1489-OFFX,345-OFFY}, 800,0}
};

#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_ndldiffpressD	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_2077DIFFPRESS   );}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_ndldiffpressM	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_2077DIFFPRESS   );}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_ndldiffpressP	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_2077DIFFPRESS   );}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_ndldiffpressR	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_2077DIFFPRESS   );}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_ndlpressbegD	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_2077PRESSBEGIN  );}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_ndlpressbegM	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_2077PRESSBEGIN  );}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_ndlpressbegP	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_2077PRESSBEGIN  );}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_ndlpressbegR	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_2077PRESSBEGIN  );}else HIDE_IMAGE(pelement);return -1;)
#endif

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_azs_2077pwr    , AZS_TGL(AZS_2077PWR   		))
static MAKE_MSCB(mcb_glt_prchrat_l  , HND_DEC(HND_PRESS_CHANGE_RATE); POS_DEC(POS_PRESSRATE))
static MAKE_MSCB(mcb_glt_prchrat_r  , HND_INC(HND_PRESS_CHANGE_RATE); POS_INC(POS_PRESSRATE))
static MAKE_MSCB(mcb_glt_presdif_l  , HND_DEC(HND_PRESS_DIFF); NDL_DEC2(NDL_2077DIFFPRESS,0.01))
static MAKE_MSCB(mcb_glt_presdif_r  , HND_INC(HND_PRESS_DIFF); NDL_INC2(NDL_2077DIFFPRESS,0.01))
static MAKE_MSCB(mcb_glt_presbeg_l  , HND_DEC(HND_PRESS_BEGIN); NDL_DEC2(NDL_2077PRESSBEGIN,10))
static MAKE_MSCB(mcb_glt_presbeg_r  , HND_INC(HND_PRESS_BEGIN); NDL_INC2(NDL_2077PRESSBEGIN,10))

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01	,AZS_TT(AZS_2077PWR   			))
static MAKE_TCB(tcb_02	,HND_TT(HND_PRESS_CHANGE_RATE	))
static MAKE_TCB(tcb_03	,HND_TT(HND_PRESS_DIFF			))
static MAKE_TCB(tcb_04	,HND_TT(HND_PRESS_BEGIN			))
static MAKE_TCB(tcb_05	,NDL_TT(NDL_2077DIFFPRESS		))
static MAKE_TCB(tcb_06	,NDL_TT(NDL_2077PRESSBEGIN		))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p7_4,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P7_BACKGROUND4_D_SX,P7_BACKGROUND4_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_TTPB(  "5",1120-OFFX,  238-OFFY, 180,164)    // NDL_ARK1SIGNAL				    	   
MOUSE_TTPB(  "6",1348-OFFX,  242-OFFY, 180,164)    // NDL_ARK2SIGNAL                    	   
MOUSE_TBOX(  "1",1255-OFFX,  189-OFFY, 96, 91,CURSOR_HAND,MOUSE_LR,mcb_azs_2077pwr)
MOUSE_TSHB(  "2",1534-OFFX,  375-OFFY, 66, 68,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_prchrat_l,mcb_glt_prchrat_r)
MOUSE_TSHB(  "3",1065-OFFX,  357-OFFY, 62, 63,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_presdif_l,mcb_glt_presdif_r)
MOUSE_TSHB(  "4",1296-OFFX,  368-OFFY, 64, 65,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_presbeg_l,mcb_glt_presbeg_r)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p7_4_list; 
GAUGE_HEADER_FS700_EX(P7_BACKGROUND4_D_SX, "p7_4", &p7_4_list, rect_p7_4, 0, 0, 0, 0, p7_4); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p7_cov_20771		, P7_COV_UNIT2077B_D     		,NULL                   ,1199-OFFX,  310-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_cov_20772		, P7_COV_UNIT2077C_D     		,&l_p7_cov_20771		,1432-OFFX,  319-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_NEEDLE2	(p7_ndl_diffpD		, P7_NDL_UNIT2077DIFPRESS_D     ,&l_p7_cov_20772		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressD  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_diffpM		, P7_NDL_UNIT2077DIFPRESS_M     ,&l_p7_ndl_diffpD		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressM  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_diffpP		, P7_NDL_UNIT2077DIFPRESS_P     ,&l_p7_ndl_diffpM		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressP  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_diffpR		, P7_NDL_UNIT2077DIFPRESS_R     ,&l_p7_ndl_diffpP		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressR  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbD		, P7_NDL_UNIT2077PRESSBEGIN_D   ,&l_p7_ndl_diffpR		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegD   , tbl_pressbeg, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbM		, P7_NDL_UNIT2077PRESSBEGIN_M   ,&l_p7_ndl_presbD		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegM   , tbl_pressbeg, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbP		, P7_NDL_UNIT2077PRESSBEGIN_P   ,&l_p7_ndl_presbM		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegP   , tbl_pressbeg, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbR		, P7_NDL_UNIT2077PRESSBEGIN_R   ,&l_p7_ndl_presbP		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegR   , tbl_pressbeg, 18  ,p7_4)
MY_ICON2	(p7_swt_2077pwr		, P7_SWT_2077PWR_D_00			,&l_p7_ndl_presbR		,1255-OFFX,  189-OFFY,icb_2077pwr    			, 2 *PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_cov_2077		, P7_COV_UNIT2077A_D     		,&l_p7_swt_2077pwr		,1346-OFFX,  241-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_presschrate	, P7_KNB_PRESSCHANGERATE_D_00	,&l_p7_cov_2077			,1534-OFFX,  375-OFFY,icb_presschrate			, 46*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_pressdiff	, P7_KNB_DIFFPRESS_D_00			,&l_p7_knb_presschrate	,1065-OFFX,  357-OFFY,icb_pressdiff  			, 20*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_pressbegi	, P7_KNB_BEGINPRESS_D_00		,&l_p7_knb_pressdiff	,1296-OFFX,  368-OFFY,icb_pressbegin 			, 20*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_scl_prchg		, P7_SCL_PRESSCHANGERATE_D_00	,&l_p7_knb_pressbegi	,1410-OFFX,  367-OFFY,icb_presschng 			, 46*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_4Ico			, P7_BACKGROUND4_D				,&l_p7_scl_prchg		,        0,         0,icb_Ico					,    PANEL_LIGHT_MAX,p7_4)
MY_STATIC2	(p7_4bg,p7_4_list	, P7_BACKGROUND4_D				,&l_p7_4Ico				,p7_4)
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p7_cov_20771		, P7_COV_UNIT2077B_D     		,NULL                   ,1199-OFFX,  310-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_cov_20772		, P7_COV_UNIT2077C_D     		,&l_p7_cov_20771		,1432-OFFX,  319-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_NEEDLE2	(p7_ndl_diffpD		, P7_NDL_UNIT2077DIFPRESS_D     ,&l_p7_cov_20772		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressD  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_diffpP		, P7_NDL_UNIT2077DIFPRESS_P     ,&l_p7_ndl_diffpD		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressP  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbD		, P7_NDL_UNIT2077PRESSBEGIN_D   ,&l_p7_ndl_diffpP		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegD   , tbl_pressbeg, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbP		, P7_NDL_UNIT2077PRESSBEGIN_P   ,&l_p7_ndl_presbD		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegP   , tbl_pressbeg, 18  ,p7_4)
MY_ICON2	(p7_swt_2077pwr		, P7_SWT_2077PWR_D_00			,&l_p7_ndl_presbP		,1255-OFFX,  189-OFFY,icb_2077pwr    			, 2 *PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_cov_2077		, P7_COV_UNIT2077A_D     		,&l_p7_swt_2077pwr		,1346-OFFX,  241-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_presschrate	, P7_KNB_PRESSCHANGERATE_D_00	,&l_p7_cov_2077		,1534-OFFX,  375-OFFY,icb_presschrate			, 46*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_pressdiff	, P7_KNB_DIFFPRESS_D_00			,&l_p7_knb_presschrate	,1065-OFFX,  357-OFFY,icb_pressdiff  			, 20*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_pressbegi	, P7_KNB_BEGINPRESS_D_00		,&l_p7_knb_pressdiff	,1296-OFFX,  368-OFFY,icb_pressbegin 			, 20*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_scl_prchg		, P7_SCL_PRESSCHANGERATE_D_00	,&l_p7_knb_pressbegi	,1410-OFFX,  367-OFFY,icb_presschng 			, 46*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_4Ico			, P7_BACKGROUND4_D				,&l_p7_scl_prchg		,        0,         0,icb_Ico					,    PANEL_LIGHT_MAX,p7_4)
MY_STATIC2	(p7_4bg,p7_4_list	, P7_BACKGROUND4_D				,&l_p7_4Ico			,p7_4)
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p7_cov_20771		, P7_COV_UNIT2077B_D     		,NULL                   ,1199-OFFX,  310-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_cov_20772		, P7_COV_UNIT2077C_D     		,&l_p7_cov_20771		,1432-OFFX,  319-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_NEEDLE2	(p7_ndl_diffpD		, P7_NDL_UNIT2077DIFPRESS_D     ,&l_p7_cov_20772		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressD  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_diffpR		, P7_NDL_UNIT2077DIFPRESS_R     ,&l_p7_ndl_diffpD		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressR  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbD		, P7_NDL_UNIT2077PRESSBEGIN_D   ,&l_p7_ndl_diffpR		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegD   , tbl_pressbeg, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbR		, P7_NDL_UNIT2077PRESSBEGIN_R   ,&l_p7_ndl_presbD		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegR   , tbl_pressbeg, 18  ,p7_4)
MY_ICON2	(p7_swt_2077pwr		, P7_SWT_2077PWR_D_00			,&l_p7_ndl_presbR		,1255-OFFX,  189-OFFY,icb_2077pwr    			, 2 *PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_cov_2077		, P7_COV_UNIT2077A_D     		,&l_p7_swt_2077pwr		,1346-OFFX,  241-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_presschrate	, P7_KNB_PRESSCHANGERATE_D_00	,&l_p7_cov_2077		,1534-OFFX,  375-OFFY,icb_presschrate			, 46*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_pressdiff	, P7_KNB_DIFFPRESS_D_00			,&l_p7_knb_presschrate	,1065-OFFX,  357-OFFY,icb_pressdiff  			, 20*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_pressbegi	, P7_KNB_BEGINPRESS_D_00		,&l_p7_knb_pressdiff	,1296-OFFX,  368-OFFY,icb_pressbegin 			, 20*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_scl_prchg		, P7_SCL_PRESSCHANGERATE_D_00	,&l_p7_knb_pressbegi	,1410-OFFX,  367-OFFY,icb_presschng 			, 46*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_4Ico			, P7_BACKGROUND4_D				,&l_p7_scl_prchg		,        0,         0,icb_Ico					,    PANEL_LIGHT_MAX,p7_4)
MY_STATIC2	(p7_4bg,p7_4_list	, P7_BACKGROUND4_D				,&l_p7_4Ico			,p7_4)
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p7_cov_20771		, P7_COV_UNIT2077B_D     		,NULL                   ,1199-OFFX,  310-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_cov_20772		, P7_COV_UNIT2077C_D     		,&l_p7_cov_20771		,1432-OFFX,  319-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_NEEDLE2	(p7_ndl_diffpD		, P7_NDL_UNIT2077DIFPRESS_D     ,&l_p7_cov_20772		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressD  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_diffpM		, P7_NDL_UNIT2077DIFPRESS_M     ,&l_p7_ndl_diffpD		,1209-OFFX,  318-OFFY, 5, 9, icb_ndldiffpressM  , tbl_difpress, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbD		, P7_NDL_UNIT2077PRESSBEGIN_D   ,&l_p7_ndl_diffpM		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegD   , tbl_pressbeg, 18  ,p7_4)
MY_NEEDLE2	(p7_ndl_presbM		, P7_NDL_UNIT2077PRESSBEGIN_M   ,&l_p7_ndl_presbD		,1444-OFFX,  329-OFFY, 4, 9, icb_ndlpressbegM   , tbl_pressbeg, 18  ,p7_4)
MY_ICON2	(p7_swt_2077pwr		, P7_SWT_2077PWR_D_00			,&l_p7_ndl_presbM		,1255-OFFX,  189-OFFY,icb_2077pwr    			, 2 *PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_cov_2077		, P7_COV_UNIT2077A_D     		,&l_p7_swt_2077pwr		,1346-OFFX,  241-OFFY,icb_Ico       			,    PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_presschrate	, P7_KNB_PRESSCHANGERATE_D_00	,&l_p7_cov_2077		,1534-OFFX,  375-OFFY,icb_presschrate			, 46*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_pressdiff	, P7_KNB_DIFFPRESS_D_00			,&l_p7_knb_presschrate	,1065-OFFX,  357-OFFY,icb_pressdiff  			, 20*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_knb_pressbegi	, P7_KNB_BEGINPRESS_D_00		,&l_p7_knb_pressdiff	,1296-OFFX,  368-OFFY,icb_pressbegin 			, 20*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_scl_prchg		, P7_SCL_PRESSCHANGERATE_D_00	,&l_p7_knb_pressbegi	,1410-OFFX,  367-OFFY,icb_presschng 			, 46*PANEL_LIGHT_MAX,p7_4)
MY_ICON2	(p7_4Ico			, P7_BACKGROUND4_D				,&l_p7_scl_prchg		,        0,         0,icb_Ico					,    PANEL_LIGHT_MAX,p7_4)
MY_STATIC2	(p7_4bg,p7_4_list	, P7_BACKGROUND4_D				,&l_p7_4Ico			,p7_4)
#endif

#endif