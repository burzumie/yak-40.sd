/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P4.h"

#define OFFX	1123
#define OFFY	0

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_crs_03,SHOW_POSM(POS_CRS4_03,1,0))

//////////////////////////////////////////////////////////////////////////

// �������
static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS4_01,0);
		POS_SET(POS_CRS4_02,0);
		POS_SET(POS_CRS4_03,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p1_03			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS4_03,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P4);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P1);
	}
	return true;
}

static MAKE_TCB(tcb_01	,POS_TT(POS_CRS4_03	                ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p4_3,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P4_BACKGROUND3_D_SX,P4_BACKGROUND3_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_TBOX(  "1",  1506-OFFX,    9-OFFY,MISC_CRS_P4_03_SX,MISC_CRS_P4_03_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p1_03)

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p4_3_list; 
GAUGE_HEADER_FS700_EX(P4_BACKGROUND3_D_SX, "p4_3", &p4_3_list, rect_p4_3, 0, 0, 0, 0, p4_3); 

MY_ICON2	(p4_crs_03			,MISC_CRS_P4_03				,NULL					,1506-OFFX,9-OFFY,icb_crs_03		,1					,p4_3)	
MY_ICON2	(p4_3Ico			,P4_BACKGROUND3_D			,&l_p4_crs_03			,	   0,	  0,icb_Ico			,PANEL_LIGHT_MAX	,p4_3) 
MY_STATIC2	(p4_3bg,p4_3_list	,P4_BACKGROUND3_D			,&l_p4_3Ico				, p4_3);

#endif

