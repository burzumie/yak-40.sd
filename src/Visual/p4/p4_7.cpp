/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P4.h"

#define OFFX	38
#define OFFY	0

//////////////////////////////////////////////////////////////////////////

FLOAT64 FSAPI icb_appwr(PELEMENT_ICON pelement);
FLOAT64 FSAPI icb_aptang(PELEMENT_ICON pelement);
FLOAT64 FSAPI icb_aplmpbtn_cmd(PELEMENT_ICON pelement);
FLOAT64 FSAPI icb_aplmpbtn_althld(PELEMENT_ICON pelement);
FLOAT64 FSAPI icb_trimrudder(PELEMENT_ICON pelement);
FLOAT64 FSAPI icb_aprdy(PELEMENT_ICON pelement)	;
FLOAT64 FSAPI icb_trimrudder_lamp(PELEMENT_ICON pelement);
FLOAT64 FSAPI icb_ap1(PELEMENT_ICON pelement);
FLOAT64 FSAPI icb_ap2(PELEMENT_ICON pelement);
FLOAT64 FSAPI icb_ap3(PELEMENT_ICON pelement);

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_trim_rudder_center		(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_trimeleva_l			(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_trimeleva_r			(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_appwr  				(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_appitchhld			(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_apcmd					(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_apalthld				(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_aphandle_l			(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_aphandle_r			(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_aphandle_d			(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_aphandle_u			(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_p4_aphandle_c 			(PPIXPOINT relative_point,FLAGS32 mouse_flags);

BOOL FSAPI mcb_ap_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01  ,AZS_TT(AZS_AP_PWR				    ))	
static MAKE_TCB(tcb_02  ,AZS_TT(AZS_AP_PITCH_HOLD		    ))	
static MAKE_TCB(tcb_03  ,AZS_TT(AZS_AP_CMD				    ))	
static MAKE_TCB(tcb_04  ,AZS_TT(AZS_AP_ALT_HOLD			    ))	
static MAKE_TCB(tcb_05  ,AZS_TT(AZS_TRIM_RUDDER			    ))	
static MAKE_TCB(tcb_06  ,LMP_TT(LMP_TRIM_RUDDER		        ))
static MAKE_TCB(tcb_07  ,LMP_TT(LMP_AP_RDY				    ))
static MAKE_TCB(tcb_08  ,HND_TT(HND_AP_HOR_CORRECTED        ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p4_7,HELP_NONE,0,0)
MOUSE_BOX(30,30, P14_BACKGROUND_D_SX, P14_BACKGROUND_D_SY,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_ap_big)    // AP BIG

MOUSE_TBOX( "6",  811-514-OFFX, 798-781-OFFY,  65,  59,CURSOR_HAND,MOUSE_LR,mcb_trim_rudder_center)    // LMP_TRIM_RUDDER		        
MOUSE_TTPB( "7",  598-514-OFFX, 846-781-OFFY,  67,  62)    // LMP_AP_RDY				        

MOUSE_TSHB( "5", 709-514-OFFX,  781-781-OFFY, 106,  98,CURSOR_LEFTARROW,CURSOR_RIGHTARROW,MOUSE_DLR,mcb_p4_trimeleva_l,mcb_p4_trimeleva_r)
MOUSE_TBOX( "1", 571-514-OFFX,  908-781-OFFY,  73, 109,CURSOR_HAND,MOUSE_LR,mcb_p4_appwr)
MOUSE_TBOX( "2", 806-514-OFFX,  908-781-OFFY,  83, 109,CURSOR_HAND,MOUSE_LR,mcb_p4_appitchhld)
MOUSE_TBOX( "3", 690-514-OFFX,  846-781-OFFY,  67,  62,CURSOR_HAND,MOUSE_DLR,mcb_p4_apcmd)
MOUSE_TBOX( "4", 772-514-OFFX,  857-781-OFFY,  81,  51,CURSOR_HAND,MOUSE_DLR,mcb_p4_apalthld)

MOUSE_PARENT_BEGIN( 656-514-OFFX, 912-781-OFFY, 140, 140,HELP_NONE)
MOUSE_TOOLTIP_TEXT_STRING("%8!s!",ttargs)
MOUSE_CHILD_FUNCT( 46,  0, 46, 46,CURSOR_UPARROW,   MOUSE_LEFTALL|MOUSE_RIGHTALL|MOUSE_DOWN_REPEAT,	mcb_p4_aphandle_u				)
MOUSE_CHILD_FUNCT( 46, 94, 46, 46,CURSOR_DOWNARROW, MOUSE_LEFTALL|MOUSE_RIGHTALL|MOUSE_DOWN_REPEAT,	mcb_p4_aphandle_d				)
MOUSE_CHILD_FUNCT(  0, 46, 46, 46,CURSOR_LEFTARROW, MOUSE_LEFTALL|MOUSE_RIGHTALL|MOUSE_DOWN_REPEAT,	mcb_p4_aphandle_l				)
MOUSE_CHILD_FUNCT( 94, 46, 46, 46,CURSOR_RIGHTARROW,MOUSE_LEFTALL|MOUSE_RIGHTALL|MOUSE_DOWN_REPEAT,	mcb_p4_aphandle_r				)
MOUSE_CHILD_FUNCT( 46, 46, 46, 46,CURSOR_HAND,      MOUSE_LEFTSINGLE|MOUSE_RIGHTSINGLE,				mcb_p4_aphandle_c				)
MOUSE_PARENT_END																						

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p4_7_list; 
GAUGE_HEADER_FS700_EX(P14_BACKGROUND_D_SX, "p4_7", &p4_7_list, rect_p4_7, 0, 0, 0, 0, p4_7); 

#ifndef ONLY3D

MY_ICON2	(ap_p4_knb_ap_hnd_0		,P4_KNB_AP0_D_00		,NULL						, 142-OFFX,  131-OFFY,icb_ap1				, 25 *PANEL_LIGHT_MAX	, p4_7)	
MY_ICON2	(ap_p4_knb_ap_hnd_1		,P4_KNB_AP1_D_00		,&l_ap_p4_knb_ap_hnd_0		, 142-OFFX,  131-OFFY,icb_ap2				, 25 *PANEL_LIGHT_MAX	, p4_7)	
MY_ICON2	(ap_p4_knb_ap_hnd_2		,P4_KNB_AP2_D_00		,&l_ap_p4_knb_ap_hnd_1		, 142-OFFX,  131-OFFY,icb_ap3				, 25 *PANEL_LIGHT_MAX	, p4_7)	
MY_ICON2	(ap_azs_trimrudder		,P4_SWT_TRIMRUDDER_D_00	,&l_ap_p4_knb_ap_hnd_2		, 195-OFFX,	   0-OFFY,icb_trimrudder		, 3	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_lamp_trimrudder		,P4_LMP_TRIMRUDDER_D_00	,&l_ap_azs_trimrudder		, 297-OFFX,	  17-OFFY,icb_trimrudder_lamp	, 3	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_lampbtn_ap_althld	,P4_BTN_AP3_D_00		,&l_ap_lamp_trimrudder		, 258-OFFX,	  76-OFFY,icb_aplmpbtn_althld	, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_lampbtn_ap_cmd		,P4_BTN_AP2_D_00		,&l_ap_lampbtn_ap_althld	, 176-OFFX,	  65-OFFY,icb_aplmpbtn_cmd		, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_lamp_ap_rdy			,P4_LMP_AP1_D_00		,&l_ap_lampbtn_ap_cmd		,  84-OFFX,	  65-OFFY,icb_aprdy				, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_switch_ap_tang		,P4_AZS_APPITCHHLD_D_00	,&l_ap_lamp_ap_rdy			, 292-OFFX,  127-OFFY,icb_aptang			, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_switch_ap_pwr		,P4_AZS_APPOWER_D_00	,&l_ap_switch_ap_tang		,  57-OFFX,  127-OFFY,icb_appwr				, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(p4_7Ico				,P14_BACKGROUND_D		,&l_ap_switch_ap_pwr		,		 0,			0,icb_Ico				, PANEL_LIGHT_MAX		, p4_7) 
MY_STATIC2	(p4_7bg,p4_7_list		,P14_BACKGROUND_D		,&l_p4_7Ico					, p4_7);

#else

MY_ICON2	(ap_p4_knb_ap_hnd_0		,AP_KNB_AP0_D_00		,NULL						, 142-OFFX,  131-OFFY,icb_ap1				, 25 *PANEL_LIGHT_MAX	, p4_7)	
MY_ICON2	(ap_p4_knb_ap_hnd_1		,AP_KNB_AP1_D_00		,&l_ap_p4_knb_ap_hnd_0		, 142-OFFX,  131-OFFY,icb_ap2				, 25 *PANEL_LIGHT_MAX	, p4_7)	
MY_ICON2	(ap_p4_knb_ap_hnd_2		,AP_KNB_AP2_D_00		,&l_ap_p4_knb_ap_hnd_1		, 142-OFFX,  131-OFFY,icb_ap3				, 25 *PANEL_LIGHT_MAX	, p4_7)	
MY_ICON2	(ap_azs_trimrudder		,AP_SWT_TRIMRUDDER_D_00	,&l_ap_p4_knb_ap_hnd_2		, 195-OFFX,	   0-OFFY,icb_trimrudder		, 3	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_lamp_trimrudder		,AP_LMP_TRIMRUDDER_D_00	,&l_ap_azs_trimrudder		, 297-OFFX,	  17-OFFY,icb_trimrudder_lamp	, 3	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_lampbtn_ap_althld	,AP_BTN_AP3_D_00		,&l_ap_lamp_trimrudder		, 258-OFFX,	  76-OFFY,icb_aplmpbtn_althld	, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_lampbtn_ap_cmd		,AP_BTN_AP2_D_00		,&l_ap_lampbtn_ap_althld	, 176-OFFX,	  65-OFFY,icb_aplmpbtn_cmd		, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_lamp_ap_rdy			,AP_LMP_AP1_D_00		,&l_ap_lampbtn_ap_cmd		,  84-OFFX,	  65-OFFY,icb_aprdy				, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_switch_ap_tang		,AP_AZS_APPITCHHLD_D_00	,&l_ap_lamp_ap_rdy			, 292-OFFX,  127-OFFY,icb_aptang			, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(ap_switch_ap_pwr		,AP_AZS_APPOWER_D_00	,&l_ap_switch_ap_tang		,  57-OFFX,  127-OFFY,icb_appwr				, 2	 *PANEL_LIGHT_MAX	, p4_7)
MY_ICON2	(p4_7Ico				,P14_BACKGROUND_D		,&l_ap_switch_ap_pwr		,		 0,			0,icb_Ico				, PANEL_LIGHT_MAX		, p4_7) 
MY_STATIC2	(p4_7bg,p4_7_list		,P14_BACKGROUND_D		,&l_p4_7Ico					, p4_7);

#endif