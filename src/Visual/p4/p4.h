/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Visual/p0/P0.h $

  Last modification:
    $Date: 19.02.06 10:23 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include "../../Common/CommonSys.h"
#include "../../Common/CommonVisual.h"
#include "../../Common/Macros.h"
#include "../../Common/MacrosVisual.h"
#include "../../../res/Version.h"
#include "../../Common/CommonAircraft.h"
#include "../../api/ClientDef.h"
#include "../../api/APIEntry.h"
#include "../../Lib/PanelsTools.h"

#ifdef LITE_VERSION
#if		defined(HAVE_MIX_PANEL)
#include "../../../res/2d/p4_res_m.h"
#elif	defined(HAVE_PLF_PANEL)
#ifdef ONLY3D
#include "../../../res/2d/p4_res_p3d.h"
#else
#include "../../../res/2d/p4_res_p.h"
#endif
#elif	defined(HAVE_RED_PANEL)
#include "../../../res/2d/p4_res_r.h"
#endif
#else
#ifdef ONLY3D
#include "../../../res/2d/p4_res3d.h"
#else
#include "../../../res/2d/p4_res.h"
#endif
#endif

GAUGE(p4_1)
GAUGE(p4_2)
GAUGE(p4_3)
GAUGE(p4_4)
GAUGE(p4_5)
GAUGE(p4_6)
GAUGE(p4_7)

