/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P4.h"

#define OFFX	486
#define OFFY	0

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_crs_02,SHOW_POSM(POS_CRS4_02,1,0))

static MAKE_ICB(icb_reverse					,SHOW_AZS(AZS_REVERSE,4))
static MAKE_ICB(icb_emerg_eng1_cut			,SHOW_AZS(AZS_EMERG_ENG1_CUT,4))
static MAKE_ICB(icb_emerg_eng2_cut			,SHOW_AZS(AZS_EMERG_ENG2_CUT,4))
static MAKE_ICB(icb_emerg_eng3_cut			,SHOW_AZS(AZS_EMERG_ENG3_CUT,4))
static MAKE_ICB(icb_act						,SHOW_AZS(AZS_ACT,2))      
static MAKE_ICB(icb_topl_lev				,SHOW_AZS(AZS_TOPL_LEV,2)) 
static MAKE_ICB(icb_topl_prav				,SHOW_AZS(AZS_TOPL_PRAV,2))
static MAKE_ICB(icb_topl_oslab				,SHOW_AZS(AZS_FUEL_PUMP_LO,3))
static MAKE_ICB(icb_topl_prav_avar			,SHOW_AZS(AZS_TOPL_PRAV_AVAR,4))
static MAKE_ICB(icb_topl_obyed				,SHOW_AZS(AZS_XFEED_TANKS,4))    
static MAKE_ICB(icb_topl_koltsev			,SHOW_AZS(AZS_XFEED_PUMPS,4))  
static MAKE_ICB(icb_nasosn_avar			    ,SHOW_AZS(AZS_HYD_PUMP,3))  
static MAKE_ICB(icb_manom					,SHOW_AZS(AZS_MANOM,2))
static MAKE_ICB(icb_otklstabosn				,SHOW_AZS(AZS_STAB_MAIN_CONTR,3))
static MAKE_ICB(icb_gear					,SHOW_AZS(AZS_GEAR,5))
static MAKE_ICB(icb_emerg_gear				,SHOW_AZS(AZS_EMERG_GEAR,4))      
static MAKE_ICB(icb_trimeleron				,SHOW_AZS(AZS_TRIM_ELERON,3))      

static MAKE_ICB(icb_switch_kursmp3			,SHOW_AZS(AZS_KMP_RSBN_DME,2))
static MAKE_ICB(icb_switch_kursmp2			,SHOW_AZS(AZS_KMP_DAYNIGHT,2))
static MAKE_ICB(icb_switch_kursmp1			,SHOW_AZS(AZS_KMP_RCVR,2))
static MAKE_ICB(icb_knob_kursmp			    ,SHOW_GLT(GLT_KMPSYS,3))
static MAKE_ICB(icb_lamp_kursmp_k1			,SHOW_LMP(LMP_K1,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_kursmp_g1			,SHOW_LMP(LMP_G1,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_kursmp_k2			,SHOW_LMP(LMP_K2,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_kursmp_g2			,SHOW_LMP(LMP_G2,BTN_GET(BTN_LIGHTS_TEST4),3))

static MAKE_ICB(icb_act_test				,SHOW_BTN(BTN_ACT_TEST,2))
static MAKE_ICB(icb_engstart1				,SHOW_BTN(BTN_ENGSTART1,2))
static MAKE_ICB(icb_engstart2				,SHOW_BTN(BTN_ENGSTART2,2))
static MAKE_ICB(icb_engstart3				,SHOW_BTN(BTN_ENGSTART3,2))
static MAKE_ICB(icb_contrlamp				,SHOW_BTN(BTN_LIGHTS_TEST4,2))
static MAKE_ICB(icb_shassiavarubor			,SHOW_BTN(BTN_SHASSIAVARUBOR,3))

static MAKE_ICB(icb_lamp_obyed				,SHOW_LMP(LMP_OBYED,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_koltsev			,SHOW_LMP(LMP_KOLTSEV,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_topl_lev			,SHOW_LMP(LMP_TOPL_LEV,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_topl_prav			,SHOW_LMP(LMP_TOPL_PRAV,BTN_GET(BTN_LIGHTS_TEST4),3))

/*
static MAKE_ICB(icb_lamp_reverseon			,SHOW_LMP(LMP_REVERSEON,!BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_reverseoff			,SHOW_LMP(LMP_REVERSEOFF,!BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_contr_reverseon	,SHOW_LMP(LMP_REVERSEON,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_contr_reverseoff	,SHOW_LMP(LMP_REVERSEOFF,BTN_GET(BTN_LIGHTS_TEST4),3))
*/

static MAKE_ICB(icb_lamp_act_test_fail	 	,SHOW_LMP(LMP_ACT_TEST_FAIL,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_act_test_ok  	 	,SHOW_LMP(LMP_ACT_TEST_OK,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_lamp_zar_avartorm	    ,SHOW_LMP(LMP_ZAR_AVAR_TORM,BTN_GET(BTN_LIGHTS_TEST4),3))
static MAKE_ICB(icb_trimeleron_lamp			,SHOW_LMP(LMP_TRIM_ELERON,BTN_GET(BTN_LIGHTS_TEST4),3))

static MAKE_ICB(icb_engstart1_cov			,SHOW_POS(POS_ENGSTART1,2))
static MAKE_ICB(icb_engstart2_cov			,SHOW_POS(POS_ENGSTART2,2))
static MAKE_ICB(icb_engstart3_cov			,SHOW_POS(POS_ENGSTART3,2))
static MAKE_ICB(icb_stopor					,SHOW_POS(POS_STOPOR,10))
static MAKE_ICB(icb_rud1  					,SHOW_POS(POS_RUD1,20))
static MAKE_ICB(icb_rud2  					,SHOW_POS(POS_RUD2,20))
static MAKE_ICB(icb_rud3  					,SHOW_POS(POS_RUD3,20))

static double FSAPI icb_lamp_reverseon(PELEMENT_ICON pelement)
{
	CHK_BRT();
	if(BTN_GET(BTN_LIGHTS_TEST4)) {
		HIDE_IMAGE(pelement); 
		return -1;
	} else {
		if(PWR_GET(PWR_BUS27)) {
			SHOW_IMAGE(pelement); 
			if(AZS_GET(AZS_REVERSE)==2)return AZS_GET(AZS_WARNING_LIGHTS)+1+POS_GET(POS_PANEL_STATE)*3;
			else return 0;
		}
	}
	return 0;
}

static double FSAPI icb_lamp_reverseoff(PELEMENT_ICON pelement)
{
	CHK_BRT();
	if(BTN_GET(BTN_LIGHTS_TEST4)) {
		HIDE_IMAGE(pelement); 
		return -1;
	} else {
		if(PWR_GET(PWR_BUS27)) {
			SHOW_IMAGE(pelement); 
			if(AZS_GET(AZS_REVERSE)==3)return AZS_GET(AZS_WARNING_LIGHTS)+1+POS_GET(POS_PANEL_STATE)*3;
			else return 0;
		}
	}
	return 0;
}

static double FSAPI icb_lamp_contr_reverseon(PELEMENT_ICON pelement)
{
	CHK_BRT();
	if(!BTN_GET(BTN_LIGHTS_TEST4)) {
		HIDE_IMAGE(pelement); 
		return -1;
	} else {
		if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_LAMP_TEST_BUS)) {
			SHOW_IMAGE(pelement); 
			return AZS_GET(AZS_WARNING_LIGHTS)+POS_GET(POS_PANEL_STATE)*2;
		}
	}    
	return 0;
}

static double FSAPI icb_lamp_contr_reverseoff(PELEMENT_ICON pelement)
{
	CHK_BRT();
	if(!BTN_GET(BTN_LIGHTS_TEST4)) {
		HIDE_IMAGE(pelement); 
		return -1;
	} else {
		if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_LAMP_TEST_BUS)) {
			SHOW_IMAGE(pelement); 
			return AZS_GET(AZS_WARNING_LIGHTS)+POS_GET(POS_PANEL_STATE)*2;
		}
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////

// �������
static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS4_01,0);
		POS_SET(POS_CRS4_02,0);
		POS_SET(POS_CRS4_03,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p1_02			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS4_02,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P4);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P5);
	}
	return true;
}

// ��������
static MAKE_MSCB(mcb_p4_emerg_eng1_cut	,SHOW_AZS_COV(AZS_EMERG_ENG1_CUT	))
static MAKE_MSCB(mcb_p4_emerg_eng2_cut	,SHOW_AZS_COV(AZS_EMERG_ENG2_CUT	))
static MAKE_MSCB(mcb_p4_emerg_eng3_cut	,SHOW_AZS_COV(AZS_EMERG_ENG3_CUT	))
static MAKE_MSCB(mcb_p4_topl_prav_avar	,SHOW_AZS_COV(AZS_TOPL_PRAV_AVAR	))
static MAKE_MSCB(mcb_p4_topl_obyed		,SHOW_AZS_COV(AZS_XFEED_TANKS		))	
static MAKE_MSCB(mcb_p4_topl_koltsev	,SHOW_AZS_COV(AZS_XFEED_PUMPS		))	
static MAKE_MSCB(mcb_p4_emerg_gear 		,SHOW_AZS_COV(AZS_EMERG_GEAR		))	
static MAKE_MSCB(mcb_kursmp_ind_lights  ,AZS_TGL(AZS_KMP_DAYNIGHT			))
static MAKE_MSCB(mcb_kursmp_marker_rcvr ,AZS_TGL(AZS_KMP_RCVR				))
static MAKE_MSCB(mcb_kursmp_dist_mode   ,AZS_TGL(AZS_KMP_RSBN_DME			))
static MAKE_MSCB(mcb_p4_act				,AZS_TGL(AZS_ACT					))
static MAKE_MSCB(mcb_p4_topl_lev		,AZS_TGL(AZS_TOPL_LEV				))
static MAKE_MSCB(mcb_p4_topl_prav		,AZS_TGL(AZS_TOPL_PRAV				))
static MAKE_MSCB(mcb_p4_topl_oslab_up	,AZS_SET(AZS_FUEL_PUMP_LO			,2))
static MAKE_MSCB(mcb_p4_topl_oslab_cn	,AZS_SET(AZS_FUEL_PUMP_LO			,0))
static MAKE_MSCB(mcb_p4_topl_oslab_dn	,AZS_SET(AZS_FUEL_PUMP_LO			,1))
static MAKE_MSCB(mcb_p4_manom			,AZS_TGL(AZS_MANOM					))
static BOOL FSAPI mcb_p4_stab_main_contr(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(AZS_GET(AZS_STAB_MAIN_CONTR)==0) {
			AZS_SET(AZS_STAB_MAIN_CONTR,1);
		} else  {
			AZS_SET(AZS_STAB_MAIN_CONTR,0);
		}
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		if(AZS_GET(AZS_STAB_MAIN_CONTR)==1&&AZS_GET(AZS_STAB_MAIN_CONTR)!=0)
			AZS_SET(AZS_STAB_MAIN_CONTR,2);
		else if(AZS_GET(AZS_STAB_MAIN_CONTR)!=0)
			AZS_SET(AZS_STAB_MAIN_CONTR,1);
	}
	return TRUE;
}

static BOOL FSAPI mcb_p4_gear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(POS_GET(POS_MAIN_HYDRO_WORK))
		ImportTable.pPanels->send_key_event(KEY_GEAR_TOGGLE,0);
	return TRUE;
}

static BOOL FSAPI mcb_p4_reverse_u		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int i=(int)AZS_GET(AZS_REVERSE);
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(i==0)
			AZS_SET(AZS_REVERSE,1);
		else if(i==1)
			AZS_SET(AZS_REVERSE,0);
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		if(i!=0)
			AZS_SET(AZS_REVERSE,2);
	}
	return TRUE;
}

static BOOL FSAPI mcb_p4_reverse_d		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int i=(int)AZS_GET(AZS_REVERSE);
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(i==0)
			AZS_SET(AZS_REVERSE,1);
		else if(i==1)
			AZS_SET(AZS_REVERSE,0);
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		if(i!=0)
			AZS_SET(AZS_REVERSE,3);
	}
	return TRUE;
}

static BOOL FSAPI mcb_p4_reverse_c		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int i=(int)AZS_GET(AZS_REVERSE);
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(i==0)
			AZS_SET(AZS_REVERSE,1);
		else if(i==1)
			AZS_SET(AZS_REVERSE,0);
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		if(i!=0)
			AZS_SET(AZS_REVERSE,1);
	}
	return TRUE;
}

static BOOL FSAPI mcb_p4_hydpump		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(AZS_GET(AZS_HYD_PUMP)==0) {
			AZS_SET(AZS_HYD_PUMP,1);
		} else {
			AZS_SET(AZS_HYD_PUMP,0);
		}
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		if(AZS_GET(AZS_HYD_PUMP)==1&&AZS_GET(AZS_HYD_PUMP)!=0)
			AZS_SET(AZS_HYD_PUMP,2);
		else if(AZS_GET(AZS_HYD_PUMP)!=0)
			AZS_SET(AZS_HYD_PUMP,1);
	}
	return TRUE;
}

static BOOL FSAPI mcb_trim_aileron_center	(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_ALER_TRIM_BUS))
		KEY_SET(KEY_AILERON_CENTER_TRIM,TRIMMER_CENTER);
	return TRUE;
}

static BOOL FSAPI mcb_p4_trimailer_l	(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_ALER_TRIM_BUS))
		ImportTable.pPanels->trigger_key_event(KEY_AILERON_TRIM_LEFT,0);
	PRS_AZS(AZS_TRIM_ELERON,1);
	return TRUE;
}

static BOOL FSAPI mcb_p4_trimailer_r	(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_ALER_TRIM_BUS))
		ImportTable.pPanels->trigger_key_event(KEY_AILERON_TRIM_RIGHT,0);
	PRS_AZS(AZS_TRIM_ELERON,2);
	return TRUE;
}

// ������
static MAKE_MSCB(mcb_p4_acttest			,PRS_BTN(BTN_ACT_TEST				))
static MAKE_MSCB(mcb_p4_contrlamp		,PRS_BTN(BTN_LIGHTS_TEST4			))
static BOOL FSAPI mcb_p4_engstart1		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int p=POS_ENGSTART1;
	int b=BTN_ENGSTART1;
	int i=(int)POS_GET(p);
	if(mouse_flags&MOUSE_LEFTRELEASE) { 
		if(i!=0) {
			BTN_SET(b,0);
		}
	} else {
		if(mouse_flags&MOUSE_LEFTSINGLE) { 
			if(i!=0) {
				BTN_SET(b,1);
			}
		}
	}
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_TGL(p);
		if(POS_GET(p)==0)
			BTN_SET(b,0);
	} 
	return TRUE;
}

static BOOL FSAPI mcb_p4_engstart2		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int p=POS_ENGSTART2;
	int b=BTN_ENGSTART2;
	int i=(int)POS_GET(p);
	if(mouse_flags&MOUSE_LEFTRELEASE) { 
		if(i!=0) {
			BTN_SET(b,0);
		}
	} else {
		if(mouse_flags&MOUSE_LEFTSINGLE) { 
			if(i!=0) {
				BTN_SET(b,1);
			}
		}
	}
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_TGL(p);
		if(POS_GET(p)==0)
			BTN_SET(b,0);
	} 
	return TRUE;
}

static BOOL FSAPI mcb_p4_engstart3		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int p=POS_ENGSTART3;
	int b=BTN_ENGSTART3;
	int i=(int)POS_GET(p);
	if(mouse_flags&MOUSE_LEFTRELEASE) { 
		if(i!=0) {
			BTN_SET(b,0);
		}
	} else {
		if(mouse_flags&MOUSE_LEFTSINGLE) { 
			if(i!=0) {
				BTN_SET(b,1);
			}
		}
	}
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_TGL(p);
		if(POS_GET(p)==0)
			BTN_SET(b,0);
	} 
	return TRUE;
}

static BOOL FSAPI mcb_p4_gearextemerg	(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int i=(int)BTN_GET(BTN_SHASSIAVARUBOR);
	if(mouse_flags&MOUSE_LEFTRELEASE) { 
		if(i!=0) {
			BTN_SET(BTN_SHASSIAVARUBOR,1);
		}
	} else { 
		if(mouse_flags&MOUSE_LEFTSINGLE) { 
			if(i!=0) {
				BTN_SET(BTN_SHASSIAVARUBOR,2);
			}
		}
	}
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		BTN_TGL(BTN_SHASSIAVARUBOR);
	} 
	return TRUE;
}

// ���������
static MAKE_MSCB(mcb_kursmp_ils_mode_left ,GLT_DEC(GLT_KMPSYS				);)
static MAKE_MSCB(mcb_kursmp_ils_mode_right,GLT_INC(GLT_KMPSYS				);)

// ������
static BOOL FSAPI mcb_p4_stopor			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_TGL(POS_STOPOR_TRIGGER);
	return TRUE;
}

static BOOL FSAPI mcb_p4_rud1fg(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD1_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE1_FULL,0);
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_FULL,0);
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud2fg(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD2_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE2_FULL,0);
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_FULL,0);
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud3fg(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE3_FULL,0);
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_FULL,0);
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud1nom(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD1_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE1_SET,UINT32(16383*0.83));
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_SET,UINT32(16383*0.83));
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud2nom(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD2_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE2_SET,UINT32(16383*0.83));
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_SET,UINT32(16383*0.83));
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud3nom(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE3_SET,UINT32(16383*0.83));
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_SET,UINT32(16383*0.83));
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud185nom(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD1_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE1_SET,UINT32(16383*0.67));
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_SET,UINT32(16383*0.67));
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud285nom(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD2_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE2_SET,UINT32(16383*0.67));
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_SET,UINT32(16383*0.67));
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud385nom(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE3_SET,UINT32(16383*0.67));
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_SET,UINT32(16383*0.67));
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud1mg(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD1_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE1_CUT,0);
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_CUT,0);
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud2mg(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD2_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE2_CUT,0);
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_CUT,0);
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud3mg(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE3_CUT,0);
	} else {
		POS_SET(POS_RUD1_STOP,0);
		POS_SET(POS_RUD2_STOP,0);
		POS_SET(POS_RUD3_STOP,0);
		ImportTable.pPanels->trigger_key_event(KEY_THROTTLE_CUT,0);
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud1off(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD1_STOP,1);
	} else {
		POS_SET(POS_RUD1_STOP,1);
		POS_SET(POS_RUD2_STOP,1);
		POS_SET(POS_RUD3_STOP,1);
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud2off(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD2_STOP,1);
	} else {
		POS_SET(POS_RUD1_STOP,1);
		POS_SET(POS_RUD2_STOP,1);
		POS_SET(POS_RUD3_STOP,1);
	}
	return true;
}

static BOOL FSAPI mcb_p4_rud3off(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		POS_SET(POS_RUD3_STOP,1);
	} else {
		POS_SET(POS_RUD1_STOP,1);
		POS_SET(POS_RUD2_STOP,1);
		POS_SET(POS_RUD3_STOP,1);
	}
	return true;
}

static MAKE_TCB(tcb_01,POS_TT(POS_CRS4_02	                ))
static MAKE_TCB(tcb_02,AZS_TT(AZS_REVERSE				    ))
static MAKE_TCB(tcb_03,AZS_TT(AZS_ACT					    ))
static MAKE_TCB(tcb_04,AZS_TT(AZS_TOPL_LEV				))	
static MAKE_TCB(tcb_05,AZS_TT(AZS_TOPL_PRAV			    ))	
static MAKE_TCB(tcb_06,AZS_TT(AZS_FUEL_PUMP_LO			))	
static MAKE_TCB(tcb_07,AZS_TT(AZS_TOPL_PRAV_AVAR		    ))	
static MAKE_TCB(tcb_08,AZS_TT(AZS_XFEED_TANKS			    ))	
static MAKE_TCB(tcb_09,AZS_TT(AZS_XFEED_PUMPS			    ))	
static MAKE_TCB(tcb_10,AZS_TT(AZS_HYD_PUMP				))	
static MAKE_TCB(tcb_11,AZS_TT(AZS_MANOM				    ))	
static MAKE_TCB(tcb_12,AZS_TT(AZS_STAB_MAIN_CONTR		    ))	
static MAKE_TCB(tcb_13,AZS_TT(AZS_EMERG_GEAR			    ))	
static MAKE_TCB(tcb_14,AZS_TT(AZS_GEAR					))	
static MAKE_TCB(tcb_15,AZS_TT(AZS_EMERG_ENG1_CUT		    ))	
static MAKE_TCB(tcb_16,AZS_TT(AZS_EMERG_ENG2_CUT		    ))	
static MAKE_TCB(tcb_17,AZS_TT(AZS_EMERG_ENG3_CUT		    ))	
static MAKE_TCB(tcb_18,AZS_TT(AZS_TRIM_ELERON			    ))	
static MAKE_TCB(tcb_19,AZS_TT(AZS_KMP_RCVR				))	
static MAKE_TCB(tcb_20,AZS_TT(AZS_KMP_DAYNIGHT			))	
static MAKE_TCB(tcb_21,AZS_TT(AZS_KMP_RSBN_DME			))	
static MAKE_TCB(tcb_22,BTN_TT(BTN_LIGHTS_TEST4			))	
static MAKE_TCB(tcb_23,BTN_TT(BTN_ACT_TEST				))	
static MAKE_TCB(tcb_24,BTN_TT(BTN_ENGSTART1			    ))	
static MAKE_TCB(tcb_25,BTN_TT(BTN_ENGSTART2			    ))	
static MAKE_TCB(tcb_26,BTN_TT(BTN_ENGSTART3			    ))	
static MAKE_TCB(tcb_27,BTN_TT(BTN_SHASSIAVARUBOR		    ))	
static MAKE_TCB(tcb_28,GLT_TT(GLT_KMPSYS      			))	
static MAKE_TCB(tcb_29,LMP_TT(LMP_REVERSEON			    ))
static MAKE_TCB(tcb_30,LMP_TT(LMP_REVERSEOFF			    ))
static MAKE_TCB(tcb_31,LMP_TT(LMP_CONTR_REVERSEON	        ))
static MAKE_TCB(tcb_32,LMP_TT(LMP_CONTR_REVERSEOFF	    ))
static MAKE_TCB(tcb_33,LMP_TT(LMP_TOPL_LEV			    ))
static MAKE_TCB(tcb_34,LMP_TT(LMP_TOPL_PRAV			    ))
static MAKE_TCB(tcb_35,LMP_TT(LMP_OBYED				    ))
static MAKE_TCB(tcb_36,LMP_TT(LMP_KOLTSEV			        ))
static MAKE_TCB(tcb_37,LMP_TT(LMP_ACT_TEST_FAIL		    ))
static MAKE_TCB(tcb_38,LMP_TT(LMP_ZAR_AVAR_TORM		    ))
static MAKE_TCB(tcb_39,LMP_TT(LMP_TRIM_ELERON		        ))
static MAKE_TCB(tcb_40,LMP_TT(LMP_ACT_TEST_OK		        ))
static MAKE_TCB(tcb_41,LMP_TT(LMP_K1         		        ))
static MAKE_TCB(tcb_42,LMP_TT(LMP_G1         		        ))
static MAKE_TCB(tcb_43,LMP_TT(LMP_K2         		        ))
static MAKE_TCB(tcb_44,LMP_TT(LMP_G2         		        ))
static MAKE_TCB(tcb_45,POS_TT(POS_STOPOR       		    ))
static MAKE_TCB(tcb_46,POS_TT(POS_RUD1			        ))
static MAKE_TCB(tcb_47,POS_TT(POS_RUD2			        ))
static MAKE_TCB(tcb_48,POS_TT(POS_RUD3			        ))
				
static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MAKE_TTA(tcb_20	)
MAKE_TTA(tcb_21	)
MAKE_TTA(tcb_22	) 
MAKE_TTA(tcb_23	)
MAKE_TTA(tcb_24	)
MAKE_TTA(tcb_25	)
MAKE_TTA(tcb_26	)
MAKE_TTA(tcb_27	) 
MAKE_TTA(tcb_28	) 
MAKE_TTA(tcb_29	)
MAKE_TTA(tcb_30	)
MAKE_TTA(tcb_31	)
MAKE_TTA(tcb_32	) 
MAKE_TTA(tcb_33	)
MAKE_TTA(tcb_34	)
MAKE_TTA(tcb_35	)
MAKE_TTA(tcb_36	)
MAKE_TTA(tcb_37	) 
MAKE_TTA(tcb_38	)
MAKE_TTA(tcb_39	)
MAKE_TTA(tcb_40	)
MAKE_TTA(tcb_41	)
MAKE_TTA(tcb_42	)
MAKE_TTA(tcb_43	)
MAKE_TTA(tcb_44	)
MAKE_TTA(tcb_45	)
MAKE_TTA(tcb_46	)
MAKE_TTA(tcb_47	) 
MAKE_TTA(tcb_48	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p4_2,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P4_BACKGROUND2_D_SX,P4_BACKGROUND2_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_TBOX(  "1", 750-OFFX,    9-OFFY,MISC_CRS_P4_02_SX,MISC_CRS_P4_02_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p1_02)

// �����
MOUSE_TTPB( "29",  486-OFFX,   8-OFFY,  78, 116)    // LMP_REVERSEON			        
MOUSE_TTPB( "30",  486-OFFX, 124-OFFY,  78,  95)    // LMP_REVERSEOFF			    
MOUSE_TTPB( "33",  564-OFFX, 171-OFFY,  51,  48)    // LMP_TOPL_LEV			        
MOUSE_TTPB( "34",  615-OFFX, 171-OFFY,  53,  48)    // LMP_TOPL_PRAV			        
MOUSE_TTPB( "35",  731-OFFX, 164-OFFY,  59,  59)    // LMP_OBYED				        
MOUSE_TTPB( "36",  790-OFFX, 164-OFFY,  59,  59)    // LMP_KOLTSEV			        
MOUSE_TTPB( "37",  544-OFFX, 333-OFFY,  54,  51)    // LMP_ACT_TEST_FAIL		        
MOUSE_TTPB( "38",  982-OFFX, 187-OFFY,  79,  67)    // LMP_ZAR_AVAR_TORM		        
MOUSE_TBOX( "39",  560-OFFX, 550-OFFY,  93,  69,CURSOR_HAND,MOUSE_LR,mcb_trim_aileron_center)    // LMP_TRIM_ELERON		        
MOUSE_TTPB( "40",  598-OFFX, 333-OFFY,  55,  51)    // LMP_ACT_TEST_OK		        
MOUSE_TTPB( "41",  813-OFFX,  42-OFFY,  40,  47)    // LMP_K1         		    
MOUSE_TTPB( "24",  853-OFFX,  42-OFFY,  40,  47)    // LMP_G1         		        
MOUSE_TTPB( "43",  961-OFFX,  42-OFFY,  40,  47)    // LMP_K2         		        
MOUSE_TTPB( "44", 1001-OFFX,  42-OFFY,  40,  47)    // LMP_G2         		        
								
// ��������			  
MOUSE_TSV3(  "2", 564-OFFX,   78-OFFY,  66,  86,CURSOR_HAND,CURSOR_HAND,CURSOR_HAND,MOUSE_LR,mcb_p4_reverse_u,mcb_p4_reverse_c,mcb_p4_reverse_d)
MOUSE_TBOX(  "3", 505-OFFX,  219-OFFY,  56, 114,CURSOR_HAND,MOUSE_LR,mcb_p4_act)
MOUSE_TBOX(  "4", 561-OFFX,  219-OFFY,  47, 114,CURSOR_HAND,MOUSE_LR,mcb_p4_topl_lev)
MOUSE_TBOX(  "5", 608-OFFX,  219-OFFY,  44, 114,CURSOR_HAND,MOUSE_LR,mcb_p4_topl_prav)
MOUSE_TSV3(  "6", 652-OFFX,  219-OFFY,  44, 114,CURSOR_UPARROW,CURSOR_HAND,CURSOR_DOWNARROW,MOUSE_LR,mcb_p4_topl_oslab_up,mcb_p4_topl_oslab_cn,mcb_p4_topl_oslab_dn)
MOUSE_TBOX(  "7", 696-OFFX,  220-OFFY,  48, 100,CURSOR_HAND,MOUSE_LR,mcb_p4_topl_prav_avar)
MOUSE_TBOX(  "8", 744-OFFX,  220-OFFY,  46, 100,CURSOR_HAND,MOUSE_LR,mcb_p4_topl_obyed)
MOUSE_TBOX(  "9", 790-OFFX,  220-OFFY,  53, 100,CURSOR_HAND,MOUSE_LR,mcb_p4_topl_koltsev)
MOUSE_TBOX( "10", 886-OFFX,  200-OFFY,  50, 100,CURSOR_HAND,MOUSE_LR,mcb_p4_hydpump)
MOUSE_TBOX( "11", 992-OFFX,  263-OFFY,  97, 105,CURSOR_HAND,MOUSE_LR,mcb_p4_manom)
MOUSE_TBOX( "12", 514-OFFX,  460-OFFY,  66,  85,CURSOR_HAND,MOUSE_LR,mcb_p4_stab_main_contr)
MOUSE_TBOX( "13", 934-OFFX,  368-OFFY, 150, 168,CURSOR_HAND,MOUSE_LR,mcb_p4_emerg_gear)
MOUSE_TBOX( "14", 949-OFFX,  573-OFFY, 140, 133,CURSOR_HAND,MOUSE_LR,mcb_p4_gear)
MOUSE_TBOX( "15", 637-OFFX,    8-OFFY,  51, 156,CURSOR_HAND,MOUSE_LR,mcb_p4_emerg_eng1_cut)
MOUSE_TBOX( "16", 688-OFFX,    8-OFFY,  43, 156,CURSOR_HAND,MOUSE_LR,mcb_p4_emerg_eng2_cut)
MOUSE_TBOX( "17", 731-OFFX,    8-OFFY,  47, 156,CURSOR_HAND,MOUSE_LR,mcb_p4_emerg_eng3_cut)
MOUSE_TSHB( "18", 514-OFFX,  619-OFFY, 139, 138,CURSOR_LEFTARROW,CURSOR_RIGHTARROW,MOUSE_DLR,mcb_p4_trimailer_l,mcb_p4_trimailer_r)
MOUSE_TBOX( "19", 818-OFFX,  104-OFFY,  52,  48,CURSOR_HAND,MOUSE_LR,mcb_kursmp_marker_rcvr)
MOUSE_TBOX( "20", 973-OFFX,  104-OFFY,  29,  48,CURSOR_HAND,MOUSE_LR,mcb_kursmp_ind_lights)
//MOUSE_TBOX( "21",1002-OFFX,  104-OFFY,  29,  48,CURSOR_HAND,MOUSE_LR,mcb_kursmp_dist_mode)
								
// ������
MOUSE_TBOX( "22",1079-OFFX,  419-OFFY,  35,  35,CURSOR_HAND,MOUSE_DLR,mcb_p4_contrlamp)
MOUSE_TBOX( "23", 514-OFFX,  333-OFFY,  30,  51,CURSOR_HAND,MOUSE_DLR,mcb_p4_acttest)
MOUSE_TBOX( "24", 704-OFFX,  343-OFFY,  38,  35,CURSOR_HAND,MOUSE_DLR,mcb_p4_engstart1)
MOUSE_TBOX( "25", 775-OFFX,  343-OFFY,  38,  35,CURSOR_HAND,MOUSE_DLR,mcb_p4_engstart2)
MOUSE_TBOX( "26", 847-OFFX,  343-OFFY,  38,  35,CURSOR_HAND,MOUSE_DLR,mcb_p4_engstart3)
MOUSE_TBOX( "27", 653-OFFX,  688-OFFY, 217,  96,CURSOR_HAND,MOUSE_DLR,mcb_p4_gearextemerg)
					 
// ���������		 
//MOUSE_TSHB( "28", 880-OFFX,   89-OFFY,  92,  63,CURSOR_LEFTARROW,CURSOR_RIGHTARROW,MOUSE_LR,mcb_kursmp_ils_mode_left,mcb_kursmp_ils_mode_right)
								
// ������                                	        
MOUSE_TBOX( "45", 672-OFFX,  410-OFFY,  10, 135,CURSOR_HAND,MOUSE_LR,mcb_p4_stopor)
								
MOUSE_TBOX( "46", 680-OFFX,  566-OFFY,  64,  30,CURSOR_HAND,MOUSE_LR,mcb_p4_rud1mg)
MOUSE_TBOX( "47", 747-OFFX,  566-OFFY,  64,  30,CURSOR_HAND,MOUSE_LR,mcb_p4_rud2mg)
MOUSE_TBOX( "48", 818-OFFX,  566-OFFY,  64,  30,CURSOR_HAND,MOUSE_LR,mcb_p4_rud3mg)

MOUSE_TBOX( "46", 680-OFFX,  640-OFFY,  64,  60,CURSOR_HAND,MOUSE_LR,mcb_p4_rud1off)
MOUSE_TBOX( "47", 747-OFFX,  640-OFFY,  64,  60,CURSOR_HAND,MOUSE_LR,mcb_p4_rud2off)
MOUSE_TBOX( "48", 818-OFFX,  640-OFFY,  64,  60,CURSOR_HAND,MOUSE_LR,mcb_p4_rud3off)

MOUSE_TBOX( "46", 685-OFFX,  500-OFFY,  59,  30,CURSOR_HAND,MOUSE_LR,mcb_p4_rud185nom)
MOUSE_TBOX( "47", 747-OFFX,  500-OFFY,  64,  30,CURSOR_HAND,MOUSE_LR,mcb_p4_rud285nom)
MOUSE_TBOX( "48", 818-OFFX,  500-OFFY,  64,  30,CURSOR_HAND,MOUSE_LR,mcb_p4_rud385nom)

MOUSE_TBOX( "46", 685-OFFX,  470-OFFY,  59,  30,CURSOR_HAND,MOUSE_LR,mcb_p4_rud1nom)
MOUSE_TBOX( "47", 747-OFFX,  470-OFFY,  64,  30,CURSOR_HAND,MOUSE_LR,mcb_p4_rud2nom)
MOUSE_TBOX( "48", 818-OFFX,  470-OFFY,  64,  30,CURSOR_HAND,MOUSE_LR,mcb_p4_rud3nom)

MOUSE_TBOX( "46", 685-OFFX,  400-OFFY,  59,  60,CURSOR_HAND,MOUSE_LR,mcb_p4_rud1fg)
MOUSE_TBOX( "47", 747-OFFX,  400-OFFY,  64,  60,CURSOR_HAND,MOUSE_LR,mcb_p4_rud2fg)
MOUSE_TBOX( "48", 818-OFFX,  400-OFFY,  64,  60,CURSOR_HAND,MOUSE_LR,mcb_p4_rud3fg)

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p4_2_list; 
GAUGE_HEADER_FS700_EX(P4_BACKGROUND2_D_SX, "p4_2", &p4_2_list, rect_p4_2, 0, 0, 0, 0, p4_2); 

MY_ICON2	(p4_crs_02				,MISC_CRS_P4_02					,NULL						, 750-OFFX,    9-OFFY,icb_crs_02				, 1						, p4_2) 
MY_ICON2	(p4_rud1				,P4_SLD_RUD1_D_00				,&l_p4_crs_02				, 679-OFFX,  215-OFFY,icb_rud1					, 20*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(p4_rud2				,P4_SLD_RUD2_D_00				,&l_p4_rud1					, 765-OFFX,  215-OFFY,icb_rud2					, 20*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(p4_rud3				,P4_SLD_RUD3_D_00				,&l_p4_rud2					, 836-OFFX,  215-OFFY,icb_rud3					, 20*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(p4_stopor				,P4_SLD_RUDSTOPOR_D_00			,&l_p4_rud3					, 628-OFFX,  344-OFFY,icb_stopor				, 10*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_trimeleron		,P4_LMP_TRIMELERON_D_00			,&l_p4_stopor				, 560-OFFX,	 550-OFFY,icb_trimeleron_lamp		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_trimeleron			,P4_SWT_TRIMELERON_D_00			,&l_lamp_trimeleron			, 514-OFFX,	 619-OFFY,icb_trimeleron			, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(btn_contrlamp			,P4_BTN_LTSTEST_D_00			,&l_azs_trimeleron			,1079-OFFX,  419-OFFY,icb_contrlamp				, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(btn_shassiavarubor		,P4_BTN_GEARRETRACTEMERG_D_00	,&l_btn_contrlamp			, 653-OFFX,  688-OFFY,icb_shassiavarubor		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(switch_gear			,P4_SWT_GEARMAIN_D_00			,&l_btn_shassiavarubor		, 949-OFFX,  573-OFFY,icb_gear					, 5	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_emerg_gear			,P4_SWT_GEAREMERGE_D_00			,&l_switch_gear				, 934-OFFX,  368-OFFY,icb_emerg_gear			, 4	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_otkstabosn			,P4_SWT_HTAILMAINOFF_D_00		,&l_azs_emerg_gear			, 486-OFFX,	 384-OFFY,icb_otklstabosn			, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(cov_engstart1			,P4_COV_ENG1START_D_00			,&l_azs_otkstabosn			, 692-OFFX,  284-OFFY,icb_engstart1_cov			, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(cov_engstart2			,P4_COV_ENG2START_D_00			,&l_cov_engstart1			, 765-OFFX,  284-OFFY,icb_engstart2_cov			, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(cov_engstart3			,P4_COV_ENG3START_D_00			,&l_cov_engstart2			, 836-OFFX,  284-OFFY,icb_engstart3_cov			, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(btn_engstart1			,P4_BTN_ENG1START_D_00			,&l_cov_engstart3			, 704-OFFX,  343-OFFY,icb_engstart1				, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(btn_engstart2			,P4_BTN_ENG2START_D_00			,&l_btn_engstart1			, 775-OFFX,  343-OFFY,icb_engstart2				, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(btn_engstart3			,P4_BTN_ENG3START_D_00			,&l_btn_engstart2			, 847-OFFX,  343-OFFY,icb_engstart3				, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_act_test_fail		,P4_LMP_ACTTESTFAIL_D_00		,&l_btn_engstart3			, 544-OFFX,  333-OFFY,icb_lamp_act_test_fail	, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_act_test_ok		,P4_LMP_ACTTESTOK_D_00			,&l_lamp_act_test_fail		, 598-OFFX,  333-OFFY,icb_lamp_act_test_ok  	, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(btn_act_test			,P4_BTN_ACTTEST_D_00			,&l_lamp_act_test_ok		, 514-OFFX,  333-OFFY,icb_act_test				, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_manom				,P4_AZS_MANOM_D_00				,&l_btn_act_test			, 992-OFFX,	 263-OFFY,icb_manom					, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_zar_avartorm		,P4_LMP_CHARGEEMERGEBRAKE_D_00	,&l_azs_manom				, 982-OFFX,  187-OFFY,icb_lamp_zar_avartorm		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_nasosn_avar		,P4_AZS_HYDPUMP_D_00			,&l_lamp_zar_avartorm		, 886-OFFX,  130-OFFY,icb_nasosn_avar			, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_topl_koltsev		,P4_SWT_XFEEDPUMPS_D_00			,&l_azs_nasosn_avar			, 790-OFFX,  164-OFFY,icb_topl_koltsev			, 4	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_topl_obyed			,P4_SWT_XFEED_D_00				,&l_azs_topl_koltsev		, 744-OFFX,  164-OFFY,icb_topl_obyed			, 4	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_topl_prav_avar		,P4_SWT_FUELPUMPEMERG_D_00		,&l_azs_topl_obyed			, 696-OFFX,  164-OFFY,icb_topl_prav_avar		, 4	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_koltsev			,P4_LMP_XFEEDPUMPS_D_00			,&l_azs_topl_prav_avar		, 790-OFFX,  164-OFFY,icb_lamp_koltsev			, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_obyed				,P4_LMP_XFEED_D_00				,&l_lamp_koltsev			, 731-OFFX,  164-OFFY,icb_lamp_obyed			, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_topl_oslab			,P4_SWT_FUELPUMPLO_D_00			,&l_lamp_obyed				, 653-OFFX,  219-OFFY,icb_topl_oslab			, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_topl_prav			,P4_LMP_PUMPRIGHT_D_00			,&l_azs_topl_oslab			, 615-OFFX,  171-OFFY,icb_lamp_topl_prav		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_topl_lev			,P4_LMP_PUMPLEFT_D_00			,&l_lamp_topl_prav			, 564-OFFX,  171-OFFY,icb_lamp_topl_lev			, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_topl_prav			,P4_AZS_PUMPRIGHT_D_00			,&l_lamp_topl_lev			, 608-OFFX,  219-OFFY,icb_topl_prav				, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_topl_lev			,P4_AZS_PUMPLEFT_D_00			,&l_azs_topl_prav			, 560-OFFX,  219-OFFY,icb_topl_lev				, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_act				,P4_AZS_ACT_D_00				,&l_azs_topl_lev			, 486-OFFX,  219-OFFY,icb_act					, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(switch_kursmp3			,P4_AZS_KURSMP3_D_00			,&l_azs_act					,1002-OFFX,	 104-OFFY,icb_switch_kursmp3		, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(switch_kursmp2			,P4_AZS_KURSMP2_D_00			,&l_switch_kursmp3			, 973-OFFX,  104-OFFY,icb_switch_kursmp2		, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(switch_kursmp1			,P4_AZS_KURSMP1_D_00			,&l_switch_kursmp2			, 818-OFFX,  104-OFFY,icb_switch_kursmp1		, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(knob_kursmp			,P4_KNB_KURSMP_D_00				,&l_switch_kursmp1			, 880-OFFX,   89-OFFY,icb_knob_kursmp			, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_kursmp_k1			,P4_LMP_KURSMPK1_D_00			,&l_knob_kursmp				, 813-OFFX,   42-OFFY,icb_lamp_kursmp_k1		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_kursmp_g1			,P4_LMP_KURSMPG1_D_00			,&l_lamp_kursmp_k1			, 853-OFFX,   42-OFFY,icb_lamp_kursmp_g1		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_kursmp_k2			,P4_LMP_KURSMPK2_D_00			,&l_lamp_kursmp_g1			, 961-OFFX,   42-OFFY,icb_lamp_kursmp_k2		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_kursmp_g2			,P4_LMP_KURSMPG2_D_00			,&l_lamp_kursmp_k2			,1001-OFFX,   42-OFFY,icb_lamp_kursmp_g2		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_emerg_eng1_cut		,P4_SWT_ENG1CUT_D_00			,&l_lamp_kursmp_g2			, 637-OFFX,    8-OFFY,icb_emerg_eng1_cut		, 4	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_emerg_eng2_cut		,P4_SWT_ENG2CUT_D_00			,&l_azs_emerg_eng1_cut		, 688-OFFX,    8-OFFY,icb_emerg_eng2_cut		, 4	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(azs_emerg_eng3_cut		,P4_SWT_ENG3CUT_D_00			,&l_azs_emerg_eng2_cut		, 731-OFFX,    8-OFFY,icb_emerg_eng3_cut		, 4	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_contr_reverseoff	,P4_LMP_TESTREVERSEOFF_D_00		,&l_azs_emerg_eng3_cut		, 519-OFFX,  144-OFFY,icb_lamp_contr_reverseoff	, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_contr_reverseon	,P4_LMP_TESTREVERSEON_D_00		,&l_lamp_contr_reverseoff	, 527-OFFX,   65-OFFY,icb_lamp_contr_reverseon	, 2	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_reverseoff		,P4_LMP_REVERSEOFF_D_00			,&l_lamp_contr_reverseon	, 486-OFFX,  124-OFFY,icb_lamp_reverseoff		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(lamp_reverseon			,P4_LMP_REVERSEON_D_00			,&l_lamp_reverseoff			, 486-OFFX,    8-OFFY,icb_lamp_reverseon		, 3	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(p4_reverse				,P4_SWT_REVERSE_D_00			,&l_lamp_reverseon			, 486-OFFX,    8-OFFY,icb_reverse				, 4	*PANEL_LIGHT_MAX	, p4_2)
MY_ICON2	(p4_2Ico				,P4_BACKGROUND2_D				,&l_p4_reverse				,		 0,			0,icb_Ico					, PANEL_LIGHT_MAX		, p4_2) 
MY_STATIC2	(p4_2bg,p4_2_list		,P4_BACKGROUND2_D				,&l_p4_2Ico					, p4_2);

#endif
