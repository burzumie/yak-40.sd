/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P4.h"

#define OFFX	0
#define OFFY	0

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_crs_01,SHOW_POSM(POS_CRS4_01,1,0))

//////////////////////////////////////////////////////////////////////////

// �������
static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS4_01,0);
		POS_SET(POS_CRS4_02,0);
		POS_SET(POS_CRS4_03,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p1_01			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS4_01,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P4);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P0);
	}
	return true;
}

static MAKE_TCB(tcb_01	,POS_TT(POS_CRS4_01	                ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p4_1,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P4_BACKGROUND1_D_SX,P4_BACKGROUND1_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_TBOX(  "1",  10-OFFX,    9-OFFY,MISC_CRS_P4_01_SX,MISC_CRS_P4_01_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p1_01)

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p4_1_list; 
GAUGE_HEADER_FS700_EX(P4_BACKGROUND1_D_SX, "p4_1", &p4_1_list, rect_p4_1, 0, 0, 0, 0, p4_1); 

MY_ICON2	(p4_crs_01			,MISC_CRS_P4_01				,NULL					,10-OFFX,9-OFFY,icb_crs_01		,1					,p4_1)	
MY_ICON2	(p4_1Ico			,P4_BACKGROUND1_D			,&l_p4_crs_01			,	   0,	  0,icb_Ico			,PANEL_LIGHT_MAX	,p4_1) 
MY_STATIC2	(p4_1bg,p4_1_list	,P4_BACKGROUND1_D			,&l_p4_1Ico				, p4_1);


#endif