/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	938
#define OFFY	0

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_windowheatl		,SHOW_AZS(AZS_WINDOWHEATL  ,2));
static MAKE_ICB(icb_windowheatr		,SHOW_AZS(AZS_WINDOWHEATR  ,2));
static MAKE_ICB(icb_windowheatpwr	,SHOW_AZS(AZS_WINDOWHEATPWR,2));

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_windowheatl	    ,AZS_TGL(AZS_WINDOWHEATL   ))
static MAKE_MSCB(mcb_windowheatr	    ,AZS_TGL(AZS_WINDOWHEATR   ))
static MAKE_MSCB(mcb_windowheatpwr	    ,AZS_TGL(AZS_WINDOWHEATPWR ))

static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_WINDOWHEATL             ))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_WINDOWHEATR             ))
static MAKE_TCB(tcb_03	,AZS_TT(AZS_WINDOWHEATPWR           ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p5_3,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P5_BACKGROUND3_D_SX,P5_BACKGROUND3_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_TBOX( "1", 1206-OFFX,   68-OFFY, 64,193,CURSOR_HAND,MOUSE_LR,mcb_windowheatl)
MOUSE_TBOX( "2", 1270-OFFX,   68-OFFY, 83,193,CURSOR_HAND,MOUSE_LR,mcb_windowheatr)
MOUSE_TBOX( "3", 1353-OFFX,   68-OFFY, 75,193,CURSOR_HAND,MOUSE_LR,mcb_windowheatpwr)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p5_3_list; 
GAUGE_HEADER_FS700_EX(P5_BACKGROUND3_D_SX, "p5_3", &p5_3_list, rect_p5_3, 0, 0, 0, 0, p5_3); 

MY_ICON2	(p5_azs_windowheatl		,P5_AZS_WINDOWHEATL_D_00	,NULL					,1206-OFFX,   68-OFFY,icb_windowheatl	,2*PANEL_LIGHT_MAX	, p5_3)	
MY_ICON2	(p5_azs_windowheatr		,P5_AZS_WINDOWHEATR_D_00	,&l_p5_azs_windowheatl	,1270-OFFX,   68-OFFY,icb_windowheatr	,2*PANEL_LIGHT_MAX	, p5_3)	
MY_ICON2	(p5_azs_windowheatpwr	,P5_AZS_WINDOWHEATPWR_D_00	,&l_p5_azs_windowheatr	,1353-OFFX,   68-OFFY,icb_windowheatpwr ,2*PANEL_LIGHT_MAX	, p5_3)	
MY_ICON2	(p5_3Ico				,P5_BACKGROUND3_D			,&l_p5_azs_windowheatpwr,		 0,			0,icb_Ico			,PANEL_LIGHT_MAX	, p5_3) 
MY_STATIC2	(p5_3bg,p5_3_list		,P5_BACKGROUND3_D			,&l_p5_3Ico				, p5_3);


#endif
