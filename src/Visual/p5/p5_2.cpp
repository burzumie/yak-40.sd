/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	690
#define OFFY	0

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_pkai251			,SHOW_AZS(AZS_PKAI251      ,4));
static MAKE_ICB(icb_pkai252			,SHOW_AZS(AZS_PKAI252      ,4));
static MAKE_ICB(icb_pkai253			,SHOW_AZS(AZS_PKAI253      ,4));
static MAKE_ICB(icb_lmpeng1pk   	,SHOW_LMP(LMP_ENG1PK				,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_lmpeng2pk   	,SHOW_LMP(LMP_ENG2PK				,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_lmpeng3pk   	,SHOW_LMP(LMP_ENG3PK				,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_pkai251		    ,SHOW_AZS_COV_PK25(AZS_PKAI251  ))
static MAKE_MSCB(mcb_pkai252		    ,SHOW_AZS_COV_PK25(AZS_PKAI252  ))
static MAKE_MSCB(mcb_pkai253		    ,SHOW_AZS_COV_PK25(AZS_PKAI253  ))

static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_PKAI251                 ))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_PKAI252                 ))
static MAKE_TCB(tcb_03	,AZS_TT(AZS_PKAI253                 ))
static MAKE_TCB(tcb_04	,LMP_TT(LMP_ENG1PK			        ))
static MAKE_TCB(tcb_05	,LMP_TT(LMP_ENG2PK			        ))
static MAKE_TCB(tcb_06	,LMP_TT(LMP_ENG3PK			        ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p5_2,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P5_BACKGROUND2_D_SX,P5_BACKGROUND2_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_TTPB( "4",  690-OFFX,   18-OFFY, 80,95)    // LMP_ENG1PK			       
MOUSE_TTPB( "5",  777-OFFX,   18-OFFY, 80,95)    // LMP_ENG2PK			       
MOUSE_TTPB( "6",  855-OFFX,   18-OFFY, 80,95)    // LMP_ENG3PK			       
MOUSE_TBOX( "1",  690-OFFX,  116-OFFY, 87,160,CURSOR_HAND,MOUSE_LR,mcb_pkai251)
MOUSE_TBOX( "2",  777-OFFX,  116-OFFY, 78,160,CURSOR_HAND,MOUSE_LR,mcb_pkai252)
MOUSE_TBOX( "3",  855-OFFX,  116-OFFY, 78,160,CURSOR_HAND,MOUSE_LR,mcb_pkai253)
MOUSE_END       

extern PELEMENT_HEADER p5_2_list; 
GAUGE_HEADER_FS700_EX(P5_BACKGROUND2_D_SX, "p5_2", &p5_2_list, rect_p5_2, 0, 0, 0, 0, p5_2); 

MY_ICON2	(p5_lmp_eng1pk		,P5_LMP_ENG1PK_D_00		,NULL				, 690-OFFX,   18-OFFY,icb_lmpeng1pk   	,3*PANEL_LIGHT_MAX	, p5_2)
MY_ICON2	(p5_lmp_eng2pk		,P5_LMP_ENG2PK_D_00		,&l_p5_lmp_eng1pk	, 777-OFFX,   18-OFFY,icb_lmpeng2pk   	,3*PANEL_LIGHT_MAX	, p5_2)
MY_ICON2	(p5_lmp_eng3pk		,P5_LMP_ENG3PK_D_00		,&l_p5_lmp_eng2pk	, 855-OFFX,   18-OFFY,icb_lmpeng3pk   	,3*PANEL_LIGHT_MAX	, p5_2)
MY_ICON2	(p5_swt_pkai251		,P5_SWT_PKAI251_D_00	,&l_p5_lmp_eng3pk	, 690-OFFX,  116-OFFY,icb_pkai251		,4*PANEL_LIGHT_MAX	, p5_2)
MY_ICON2	(p5_swt_pkai252		,P5_SWT_PKAI252_D_00	,&l_p5_swt_pkai251	, 777-OFFX,  116-OFFY,icb_pkai252		,4*PANEL_LIGHT_MAX	, p5_2)
MY_ICON2	(p5_swt_pkai253		,P5_SWT_PKAI253_D_00	,&l_p5_swt_pkai252	, 855-OFFX,  116-OFFY,icb_pkai253		,4*PANEL_LIGHT_MAX	, p5_2)
MY_ICON2	(p5_2Ico			,P5_BACKGROUND2_D		,&l_p5_swt_pkai253	,		 0,			0,icb_Ico			,PANEL_LIGHT_MAX	, p5_2) 
MY_STATIC2	(p5_2bg,p5_2_list	,P5_BACKGROUND2_D		,&l_p5_2Ico			,	 p5_2);

#endif
