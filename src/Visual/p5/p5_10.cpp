/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	1163
#define OFFY	989

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_fireext1ok		,SHOW_LMP(LMP_FIRE_EXTING1_RDY		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireext2ok		,SHOW_LMP(LMP_FIRE_EXTING2_RDY		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireext3ok		,SHOW_LMP(LMP_FIRE_EXTING3_RDY		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_fireext4ok		,SHOW_LMP(LMP_FIRE_EXTING4_RDY		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3));
static MAKE_ICB(icb_crs_03			,SHOW_POSM(POS_CRS5_03,1,0))

static MAKE_TCB(tcb_01	,LMP_TT(LMP_FIRE_EXTING1_RDY))
static MAKE_TCB(tcb_02	,LMP_TT(LMP_FIRE_EXTING2_RDY))
static MAKE_TCB(tcb_03	,LMP_TT(LMP_FIRE_EXTING3_RDY))
static MAKE_TCB(tcb_04	,LMP_TT(LMP_FIRE_EXTING4_RDY))
static MAKE_TCB(tcb_05	,POS_TT(POS_CRS5_03	                ))

// �������
static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p0_03			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_03,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P5);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P1);
	}
	return true;
}

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p5_10,HELP_NONE,0,0)

// �������� ��������
MOUSE_PBOX(0,0,P5_BACKGROUND9_D_SX,P5_BACKGROUND9_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

// �������  
MOUSE_TBOX(  "5",1506-OFFX, 1106-OFFY, 81,80,CURSOR_HAND,MOUSE_MLR,mcb_crs_p0_03)

MOUSE_TTPB( "1", 1211-OFFX,1044-OFFY, 70,90)    // LMP_FIRE_EXTING1_RDY	       
MOUSE_TTPB( "2", 1292-OFFX,1044-OFFY, 70,90)    // LMP_FIRE_EXTING2_RDY         
MOUSE_TTPB( "3", 1363-OFFX,1044-OFFY, 70,90)    // LMP_FIRE_EXTING3_RDY         
MOUSE_TTPB( "4", 1211-OFFX,1135-OFFY, 70,90)    // LMP_FIRE_EXTING4_RDY         
MOUSE_END       

extern PELEMENT_HEADER p5_10_list; 
GAUGE_HEADER_FS700_EX(P5_BACKGROUND10_D_SX, "p5_10", &p5_10_list, rect_p5_10, 0, 0, 0, 0, p5_10); 

MY_ICON2	(p5_crs_03			,MISC_CRS_P5_03			,NULL					,1506-OFFX, 1106-OFFY,icb_crs_03,1, p5_10)	
MY_ICON2	(p5_lmp_fireext1ok	,P5_LMP_FIREEXT1OK_D_00	,&l_p5_crs_03			,1211-OFFX, 1044-OFFY,icb_fireext1ok	, 3*PANEL_LIGHT_MAX	, p5_10)
MY_ICON2	(p5_lmp_fireext2ok	,P5_LMP_FIREEXT2OK_D_00	,&l_p5_lmp_fireext1ok	,1292-OFFX, 1044-OFFY,icb_fireext2ok	, 3*PANEL_LIGHT_MAX	, p5_10)
MY_ICON2	(p5_lmp_fireext3ok	,P5_LMP_FIREEXT3OK_D_00	,&l_p5_lmp_fireext2ok	,1363-OFFX, 1044-OFFY,icb_fireext3ok	, 3*PANEL_LIGHT_MAX	, p5_10)
MY_ICON2	(p5_lmp_fireext4ok	,P5_LMP_FIREEXT4OK_D_00	,&l_p5_lmp_fireext3ok	,1211-OFFX, 1135-OFFY,icb_fireext4ok	, 3*PANEL_LIGHT_MAX	, p5_10)
MY_ICON2	(p5_10Ico			,P5_BACKGROUND10_D		,&l_p5_lmp_fireext4ok	,		 0,			0,icb_Ico			,PANEL_LIGHT_MAX	, p5_10) 
MY_STATIC2	(p5_10bg,p5_10_list	,P5_BACKGROUND10_D		,&l_p5_10Ico			,	 p5_10);

#endif
