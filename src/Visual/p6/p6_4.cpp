/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P6.h"

#define OFFX	0
#define OFFY	521

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_ICB(icb_dmeleft1m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME61M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft2m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME62M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft3m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME63M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft4m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME64M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft5m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME65M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft1		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME61 )>-1?POS_GET(POS_DME61)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft2		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME62 )>-1?POS_GET(POS_DME62)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft3		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME63 )>-1?POS_GET(POS_DME63)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft4		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME64 )>-1?POS_GET(POS_DME64)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft5		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME65 )>-1?POS_GET(POS_DME65)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dme1			,SHOW_GLT(GLT_DME6	,2))

//////////////////////////////////////////////////////////////////////////

static BOOL FSAPI mcb_glt_dme(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	GLT_TGL(GLT_DME6);
	return TRUE;
}

BOOL FSAPI mcb_dme1_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	ImportTable.pPanels->panel_window_toggle(IDENT_P25);
	return true;
}

static MAKE_TCB(tcb_01	,GLT_TT(GLT_DME6			    ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p6_4,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P6_BACKGROUND4_D_SX,P6_BACKGROUND4_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_PBOX(0,0,P6_BACKGROUND4_D_SX,P6_BACKGROUND4_D_SY,CURSOR_HAND,MOUSE_LR,mcb_dme1_big)
MOUSE_TBOX( "1", 145-OFFX,  734-OFFY, 55, 55,CURSOR_HAND,MOUSE_LR,mcb_glt_dme)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p6_4_list; 
GAUGE_HEADER_FS700_EX(P6_BACKGROUND4_D_SX, "p6_4", &p6_4_list, rect_p6_4, 0, 0, 0, 0, p6_4); 

MY_ICON2	(p6_scl_dmeleft1m	, P6_SCL_1DMELEFT1_D__ 	,NULL              	,  90-OFFX,  660-OFFY,icb_dmeleft1m  ,    PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_scl_dmeleft1	, P6_SCL_DMELEFT1_D_00	,&l_p6_scl_dmeleft1m,  90-OFFX,  660-OFFY,icb_dmeleft1   , 10*PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_scl_dmeleft2m	, P6_SCL_1DMELEFT2_D__ 	,&l_p6_scl_dmeleft1	, 133-OFFX,  660-OFFY,icb_dmeleft2m  ,    PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_scl_dmeleft2	, P6_SCL_DMELEFT2_D_00	,&l_p6_scl_dmeleft2m, 133-OFFX,  660-OFFY,icb_dmeleft2   , 10*PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_scl_dmeleft3m	, P6_SCL_1DMELEFT3_D__ 	,&l_p6_scl_dmeleft2	, 160-OFFX,  660-OFFY,icb_dmeleft3m  ,    PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_scl_dmeleft3	, P6_SCL_DMELEFT3_D_00	,&l_p6_scl_dmeleft3m, 160-OFFX,  660-OFFY,icb_dmeleft3   , 10*PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_scl_dmeleft4m	, P6_SCL_1DMELEFT4_D__ 	,&l_p6_scl_dmeleft3	, 192-OFFX,  660-OFFY,icb_dmeleft4m  ,    PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_scl_dmeleft4	, P6_SCL_DMELEFT4_D_00	,&l_p6_scl_dmeleft4m, 192-OFFX,  660-OFFY,icb_dmeleft4   , 10*PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_scl_dmeleft5m	, P6_SCL_1DMELEFT5_D__ 	,&l_p6_scl_dmeleft4	, 219-OFFX,  660-OFFY,icb_dmeleft5m  ,    PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_scl_dmeleft5	, P6_SCL_DMELEFT5_D_00	,&l_p6_scl_dmeleft5m, 219-OFFX,  660-OFFY,icb_dmeleft5   , 10*PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_knb_dme1		, P6_KNB_DME1_D_00		,&l_p6_scl_dmeleft5	, 145-OFFX,  734-OFFY,icb_dme1       , 2 *PANEL_LIGHT_MAX ,p6_4)
MY_ICON2	(p6_4Ico			, P6_BACKGROUND4_D		,&l_p6_knb_dme1		,        0,         0,icb_Ico        ,    PANEL_LIGHT_MAX ,p6_4)
MY_STATIC2	(p6_4bg,p6_4_list	, P6_BACKGROUND4_D		,&l_p6_4Ico			,p6_4)

#endif