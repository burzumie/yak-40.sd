/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

static MAKE_NCB(icb_secD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_secM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_secP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_secR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_blenker			,SHOW_POS(POS_ACHS_BLENKER		,3))
static MAKE_ICB(icb_achs_right		,CHK_BRT(); return POS_GET(POS_PANEL_STATE);)
static MAKE_ICB(icb_achs_left		,CHK_BRT(); return POS_GET(POS_PANEL_STATE);)

BOOL FSAPI mcb_clock_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static BOOL FSAPI mcb_clock_left(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) {
		BTN_SET(BTN_CLOCK_HRONO,0);
		POS_INC(POS_ACHS_BLENKER);
	} else {
		BTN_SET(BTN_CLOCK_HRONO,1);
	}
	return TRUE;
}

static BOOL FSAPI mcb_clock_right(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) {
		BTN_SET(BTN_CLOCK_START,0);
		POS_INC(POS_ACHS_TIMER);
	} else {
		BTN_SET(BTN_CLOCK_START,1);
	}
	return TRUE;
}

static MAKE_TCB(p0_13_tcb_01	,return NDL_TTGET(NDL_MAIN_MIN);) 
static MAKE_TCB(p0_13_tcb_02	,BTN_TT(BTN_CLOCK_HRONO					))
static MAKE_TCB(p0_13_tcb_03	,BTN_TT(BTN_CLOCK_START					))

static MOUSE_TOOLTIP_ARGS(p0_13_ttargs) 
MAKE_TTA(p0_13_tcb_01) 
MAKE_TTA(p0_13_tcb_02) 
MAKE_TTA(p0_13_tcb_03) 
MOUSE_TOOLTIP_ARGS_END 

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_13,HELP_NONE,0,0) 
MOUSE_TBOX2( "1",  30,   30,P19_BACKGROUND_D_SX,P19_BACKGROUND_D_SY,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_clock_big,p0_13)    
MOUSE_TBOX2( "2",  26,  213, 34, 34,CURSOR_HAND,MOUSE_DLR,mcb_clock_left,p0_13)
MOUSE_TBOX2( "3", 303,  213, 34, 34,CURSOR_HAND,MOUSE_DLR,mcb_clock_right,p0_13)
MOUSE_END 

extern PELEMENT_HEADER p0_13_list; 
GAUGE_HEADER_FS700_EX(P19_BACKGROUND_D_SX, "p0_13", &p0_13_list, rect_p0_13, 0, 0, 0, 0, p0_13); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p13achs_right				,P19_BTN_CLCKR_D		,NULL           				, 303, 213,			icb_Ico       	,PANEL_LIGHT_MAX, p0_13)			
MY_ICON2	(p13achs_left				,P19_BTN_CLCKL_D		,&l_p13achs_right				,  26, 213,			icb_Ico      	,PANEL_LIGHT_MAX, p0_13)			
MY_NEEDLE2	(ndl_p0_13_achs_secD		,P19_NDL_CLOCKSEC_D		,&l_p13achs_left				, 181, 163,11,13,	icb_secD		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_secM		,P19_NDL_CLOCKSEC_M		,&l_ndl_p0_13_achs_secD			, 181, 163,11,13,	icb_secM		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_secP		,P19_NDL_CLOCKSEC_P		,&l_ndl_p0_13_achs_secM			, 181, 163,11,13,	icb_secP		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_secR		,P19_NDL_CLOCKSEC_R		,&l_ndl_p0_13_achs_secP			, 181, 163,11,13,	icb_secR		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_minD		,P19_NDL_CLOCKMIN_D		,&l_ndl_p0_13_achs_secR			, 181, 163, 5,12,	icb_minD		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_minM		,P19_NDL_CLOCKMIN_M		,&l_ndl_p0_13_achs_minD			, 181, 163, 5,12,	icb_minM		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_minP		,P19_NDL_CLOCKMIN_P		,&l_ndl_p0_13_achs_minM			, 181, 163, 5,12,	icb_minP		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_minR		,P19_NDL_CLOCKMIN_R		,&l_ndl_p0_13_achs_minP			, 181, 163, 5,12,	icb_minR		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_hoursD		,P19_NDL_CLOCKHRS_D		,&l_ndl_p0_13_achs_minR			, 181, 163, 3,12,	icb_hrsD		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_hoursM		,P19_NDL_CLOCKHRS_M		,&l_ndl_p0_13_achs_hoursD		, 181, 163, 3,12,	icb_hrsM		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_hoursP		,P19_NDL_CLOCKHRS_P		,&l_ndl_p0_13_achs_hoursM		, 181, 163, 3,12,	icb_hrsP		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_hoursR		,P19_NDL_CLOCKHRS_R		,&l_ndl_p0_13_achs_hoursP		, 181, 163, 3,12,	icb_hrsR		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_secD	,P19_NDL_CLOCKDWNSEC_D	,&l_ndl_p0_13_achs_hoursR		, 181, 220,15, 6,	icb_mini_secD	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_secM	,P19_NDL_CLOCKDWNSEC_M	,&l_ndl_p0_13_achs_mini_secD	, 181, 220,15, 6,	icb_mini_secM	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_secP	,P19_NDL_CLOCKDWNSEC_P	,&l_ndl_p0_13_achs_mini_secM	, 181, 220,15, 6,	icb_mini_secP	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_secR	,P19_NDL_CLOCKDWNSEC_R	,&l_ndl_p0_13_achs_mini_secP	, 181, 220,15, 6,	icb_mini_secR	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_minD	,P19_NDL_CLOCKUPMIN_D	,&l_ndl_p0_13_achs_mini_secR	, 181, 106, 6, 6,	icb_mini_minD	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_minM	,P19_NDL_CLOCKUPMIN_M	,&l_ndl_p0_13_achs_mini_minD	, 181, 106, 6, 6,	icb_mini_minM	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_minP	,P19_NDL_CLOCKUPMIN_P	,&l_ndl_p0_13_achs_mini_minM	, 181, 106, 6, 6,	icb_mini_minP	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_minR	,P19_NDL_CLOCKUPMIN_R	,&l_ndl_p0_13_achs_mini_minP	, 181, 106, 6, 6,	icb_mini_minR	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_hoursD	,P19_NDL_CLOCKUPHRS_D	,&l_ndl_p0_13_achs_mini_minR	, 181, 106, 6, 6,	icb_mini_hrsD	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_hoursM	,P19_NDL_CLOCKUPHRS_M	,&l_ndl_p0_13_achs_mini_hoursD	, 181, 106, 6, 6,	icb_mini_hrsM	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_hoursP	,P19_NDL_CLOCKUPHRS_P	,&l_ndl_p0_13_achs_mini_hoursM	, 181, 106, 6, 6,	icb_mini_hrsP	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_hoursR	,P19_NDL_CLOCKUPHRS_R	,&l_ndl_p0_13_achs_mini_hoursP	, 181, 106, 6, 6,	icb_mini_hrsR	,0,18, p0_13)	
MY_ICON2	(blenker_p13_achs			,P19_BLK_ACHSMODE_D_00	,&l_ndl_p0_13_achs_mini_hoursR	, 168, 111,			icb_blenker		,3*PANEL_LIGHT_MAX, p0_13)     
MY_ICON2	(p0_13Ico					,P19_BACKGROUND_D		,&l_blenker_p13_achs			,   0,	 0,			icb_Ico			,PANEL_LIGHT_MAX	, p0_13) 
MY_STATIC2	(p0_13bg,p0_13_list			,P19_BACKGROUND_D		,&l_p0_13Ico					, p0_13);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p13achs_right				,P19_BTN_CLCKR_D		,NULL           				, 303, 213,			icb_Ico       	,PANEL_LIGHT_MAX, p0_13)			
MY_ICON2	(p13achs_left				,P19_BTN_CLCKL_D		,&l_p13achs_right				,  26, 213,			icb_Ico      	,PANEL_LIGHT_MAX, p0_13)			
MY_NEEDLE2	(ndl_p0_13_achs_secD		,P19_NDL_CLOCKSEC_D		,&l_p13achs_left				, 181, 163,11,13,	icb_secD		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_secP		,P19_NDL_CLOCKSEC_P		,&l_ndl_p0_13_achs_secD			, 181, 163,11,13,	icb_secP		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_minD		,P19_NDL_CLOCKMIN_D		,&l_ndl_p0_13_achs_secP			, 181, 163, 5,12,	icb_minD		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_minP		,P19_NDL_CLOCKMIN_P		,&l_ndl_p0_13_achs_minD			, 181, 163, 5,12,	icb_minP		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_hoursD		,P19_NDL_CLOCKHRS_D		,&l_ndl_p0_13_achs_minP			, 181, 163, 3,12,	icb_hrsD		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_hoursP		,P19_NDL_CLOCKHRS_P		,&l_ndl_p0_13_achs_hoursD		, 181, 163, 3,12,	icb_hrsP		,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_secD	,P19_NDL_CLOCKDWNSEC_D	,&l_ndl_p0_13_achs_hoursP		, 181, 220,15, 6,	icb_mini_secD	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_secP	,P19_NDL_CLOCKDWNSEC_P	,&l_ndl_p0_13_achs_mini_secD	, 181, 220,15, 6,	icb_mini_secP	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_minD	,P19_NDL_CLOCKUPMIN_D	,&l_ndl_p0_13_achs_mini_secP	, 181, 106, 6, 6,	icb_mini_minD	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_minP	,P19_NDL_CLOCKUPMIN_P	,&l_ndl_p0_13_achs_mini_minD	, 181, 106, 6, 6,	icb_mini_minP	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_hoursD	,P19_NDL_CLOCKUPHRS_D	,&l_ndl_p0_13_achs_mini_minP	, 181, 106, 6, 6,	icb_mini_hrsD	,0,18, p0_13)	
MY_NEEDLE2	(ndl_p0_13_achs_mini_hoursP	,P19_NDL_CLOCKUPHRS_P	,&l_ndl_p0_13_achs_mini_hoursD	, 181, 106, 6, 6,	icb_mini_hrsP	,0,18, p0_13)	
MY_ICON2	(blenker_p13_achs			,P19_BLK_ACHSMODE_D_00	,&l_ndl_p0_13_achs_mini_hoursP	, 168, 111,			icb_blenker		,3*PANEL_LIGHT_MAX, p0_13)     
MY_ICON2	(p0_13Ico					,P19_BACKGROUND_D		,&l_blenker_p13_achs			,   0,	 0,			icb_Ico			,PANEL_LIGHT_MAX	, p0_13) 
MY_STATIC2	(p0_13bg,p0_13_list			,P19_BACKGROUND_D		,&l_p0_13Ico					, p0_13);
#endif

#endif
