/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"

#define CPri57(N,I,I2,I3,X1,Y1,X2,Y2,X3,Y3) /**/ \
	bool FSAPI mcb1u_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_POWERSOURCE,POWER_SOURCE_BAT); \
	return TRUE; \
	} \
	bool FSAPI mcb1c_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_POWERSOURCE,POWER_SOURCE_OFF); \
	return TRUE; \
	} \
	bool FSAPI mcb1d_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_POWERSOURCE,POWER_SOURCE_RAP); \
	return TRUE; \
	} \
	bool FSAPI mcb2_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_INV1_36V); \
	return TRUE; \
	} \
	bool FSAPI mcb3_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_INV2_36V); \
	return TRUE; \
	} \
	bool FSAPI mcb4_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_115V_BCKUP); \
	return TRUE; \
	} \
	bool FSAPI mcb5_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_INV1_115V); \
	return TRUE; \
	} \
	bool FSAPI mcb6_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_INV2_115V); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(AZS_POWERSOURCE);) \
	static MAKE_TCB(N##_tcb_02,return AZS_TTGET(AZS_INV1_36V);) \
	static MAKE_TCB(N##_tcb_03,return AZS_TTGET(AZS_INV2_36V);) \
	static MAKE_TCB(N##_tcb_04,return AZS_TTGET(AZS_115V_BCKUP);) \
	static MAKE_TCB(N##_tcb_05,return AZS_TTGET(AZS_INV1_115V);) \
	static MAKE_TCB(N##_tcb_06,return AZS_TTGET(AZS_INV2_115V);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MAKE_TTA(N##_tcb_04) \
	MAKE_TTA(N##_tcb_05) \
	MAKE_TTA(N##_tcb_06) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,84,75,CURSOR_HAND,MOUSE_LR,mcb1u_##N,N) \
	MOUSE_TBOX2("1",  0, 75,84,85,CURSOR_HAND,MOUSE_LR,mcb1c_##N,N) \
	MOUSE_TBOX2("1",  0,160,84,17,CURSOR_HAND,MOUSE_LR,mcb1d_##N,N) \
	MOUSE_TBOX2("2", 85,0,70,178,CURSOR_HAND,MOUSE_LR,mcb2_##N,N) \
	MOUSE_TBOX2("3",156,0,64,178,CURSOR_HAND,MOUSE_LR,mcb3_##N,N) \
	MOUSE_TBOX2("4",221,0,69,178,CURSOR_HAND,MOUSE_LR,mcb4_##N,N) \
	MOUSE_TBOX2("5",291,0,74,178,CURSOR_HAND,MOUSE_LR,mcb5_##N,N) \
	MOUSE_TBOX2("6",366,0,70,178,CURSOR_HAND,MOUSE_LR,mcb6_##N,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE2,I3,NULL,X3,Y3,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE1,I2,&l_##N##IcoE2,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define CPri07(N,I,I2,I3,I4,X1,Y1,X2,Y2,X3,Y3,X4,Y4) /**/ \
	bool FSAPI mcb7_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_FIRE_SOUND_ALARM); \
	return TRUE; \
	} \
	bool FSAPI mcb6_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_GEAR_WARNING); \
	return TRUE; \
	} \
	bool FSAPI mcb5_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_AUTO_SKID); \
	return TRUE; \
	} \
	bool FSAPI mcb4_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_ADI_BACKUP_POWER); \
	return TRUE; \
	} \
	bool FSAPI mcb3_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_APU_FUEL_VALVE); \
	return TRUE; \
	} \
	bool FSAPI mcb2_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_WARNING_LIGHTS); \
	return TRUE; \
	} \
	bool FSAPI mcb1_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_EMERG_EXIT_LIGHTS); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(AZS_FIRE_SOUND_ALARM);) \
	static MAKE_TCB(N##_tcb_02,return AZS_TTGET(AZS_GEAR_WARNING);) \
	static MAKE_TCB(N##_tcb_03,return AZS_TTGET(AZS_APU_FUEL_VALVE);) \
	static MAKE_TCB(N##_tcb_04,return AZS_TTGET(AZS_ADI_BACKUP_POWER);) \
	static MAKE_TCB(N##_tcb_05,return AZS_TTGET(AZS_AUTO_SKID);) \
	static MAKE_TCB(N##_tcb_06,return AZS_TTGET(AZS_WARNING_LIGHTS);) \
	static MAKE_TCB(N##_tcb_07,return AZS_TTGET(AZS_EMERG_EXIT_LIGHTS);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MAKE_TTA(N##_tcb_04) \
	MAKE_TTA(N##_tcb_05) \
	MAKE_TTA(N##_tcb_06) \
	MAKE_TTA(N##_tcb_07) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,223, 92,CURSOR_HAND,MOUSE_LR,mcb1_##N,N) \
	MOUSE_TBOX2("2",  0, 93,223, 65,CURSOR_HAND,MOUSE_LR,mcb2_##N,N) \
	MOUSE_TBOX2("3",  0,159,223, 65,CURSOR_HAND,MOUSE_LR,mcb3_##N,N) \
	MOUSE_TBOX2("4",  0,225,223, 65,CURSOR_HAND,MOUSE_LR,mcb4_##N,N) \
	MOUSE_TBOX2("5",  0,291,223, 65,CURSOR_HAND,MOUSE_LR,mcb5_##N,N) \
	MOUSE_TBOX2("6",  0,357,223,105,CURSOR_HAND,MOUSE_LR,mcb6_##N,N) \
	MOUSE_TBOX2("7",  0,463,223, 68,CURSOR_HAND,MOUSE_LR,mcb7_##N,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE3,I4,NULL,X4,Y4,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE2,I3,&l_##N##IcoE3,X3,Y3,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE1,I2,&l_##N##IcoE2,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define CPri49(N,I,I2,X1,Y1,X2,Y2) /**/ \
	bool FSAPI mcb3_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_GENERATOR1); \
	return TRUE; \
	} \
	bool FSAPI mcb2_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_GENERATOR2); \
	return TRUE; \
	} \
	bool FSAPI mcb1_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_GENERATOR3); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(AZS_GENERATOR1);) \
	static MAKE_TCB(N##_tcb_02,return AZS_TTGET(AZS_GENERATOR2);) \
	static MAKE_TCB(N##_tcb_03,return AZS_TTGET(AZS_GENERATOR3);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,173, 72,CURSOR_HAND,MOUSE_LR,mcb1_##N,N) \
	MOUSE_TBOX2("2",  0, 73,173, 72,CURSOR_HAND,MOUSE_LR,mcb2_##N,N) \
	MOUSE_TBOX2("3",  0,146,173, 72,CURSOR_HAND,MOUSE_LR,mcb3_##N,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE1,I2,NULL,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define CPri64(N,IR,XIR,YIR,IE,XIE,YIE) /**/ \
	bool FSAPI mcb1_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,1); \
	return TRUE; \
	} \
	bool FSAPI mcb2_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,2); \
	return TRUE; \
	} \
	bool FSAPI mcb3_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,1); \
	return TRUE; \
	} \
	bool FSAPI mcb4_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,2); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN);) \
	static MAKE_TCB(N##_tcb_02,return AZS_TTGET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0, 70, 30,CURSOR_HAND,MOUSE_LR,mcb1_##N,N) \
	MOUSE_TBOX2("1", 70,  0, 70, 30,CURSOR_HAND,MOUSE_LR,mcb2_##N,N) \
	MOUSE_TBOX2("2",140,  0,140, 30,CURSOR_HAND,MOUSE_LR,mcb3_##N,N) \
	MOUSE_TBOX2("2",210,  0,140, 30,CURSOR_HAND,MOUSE_LR,mcb4_##N,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(IR##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE,IE,NULL			,XIE,YIE,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoR,IR,&l_##N##IcoE	,XIR,YIR,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,IR,&l_##N##IcoR, N);

#define CPri63(N,I,I2,I3,X1,Y1,X2,Y2,X3,Y3) /**/ \
	bool FSAPI mcb1u_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_BLEEDAIR_EXTRACT_VALVE,3); \
	return TRUE; \
	} \
	bool FSAPI mcb1c_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_BLEEDAIR_EXTRACT_VALVE,0); \
	return TRUE; \
	} \
	bool FSAPI mcb1dl_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_BLEEDAIR_EXTRACT_VALVE,1); \
	return TRUE; \
	} \
	bool FSAPI mcb1dr_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_BLEEDAIR_EXTRACT_VALVE,2); \
	return TRUE; \
	} \
	bool FSAPI mcb2_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_BLEEDAIR_EXTRACT_RATIO); \
	return TRUE; \
	} \
	bool FSAPI mcb3_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_DECK_COND); \
	return TRUE; \
	} \
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(AZS_BLEEDAIR_EXTRACT_VALVE);) \
	static MAKE_TCB(N##_tcb_02,return AZS_TTGET(AZS_BLEEDAIR_EXTRACT_RATIO);) \
	static MAKE_TCB(N##_tcb_03,return AZS_TTGET(AZS_DECK_COND);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,142, 65,CURSOR_HAND,MOUSE_LR,mcb1u_##N,N) \
	MOUSE_TBOX2("1",  0, 65,142, 49,CURSOR_HAND,MOUSE_LR,mcb1c_##N,N) \
	MOUSE_TBOX2("1",  0,115, 70, 35,CURSOR_HAND,MOUSE_LR,mcb1dl_##N,N) \
	MOUSE_TBOX2("1", 70,115, 70, 35,CURSOR_HAND,MOUSE_LR,mcb1dr_##N,N) \
	MOUSE_TBOX2("2",142,  0, 70,160,CURSOR_HAND,MOUSE_LR,mcb2_##N,N) \
	MOUSE_TBOX2("3",214,  0, 87,186,CURSOR_HAND,MOUSE_LR,mcb3_##N,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE2,I3,NULL,X3,Y3,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##IcoE1,I2,&l_##N##IcoE2,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define CPri62(N,I,I2,X1,Y1,X2,Y2) /**/ \
	bool FSAPI mcb1_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_AIR_PRESS_DROP_EMERG);\
	return TRUE;\
	};\
	bool FSAPI mcb2_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_AIR_PRESS_CONTROL_BCKUP);\
	return TRUE;\
	};\
	bool FSAPI mcb3u_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_PRESS_SYS_MASTER_EMERG,1);\
	return TRUE;\
	};\
	bool FSAPI mcb3c_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_PRESS_SYS_MASTER_EMERG,0);\
	return TRUE;\
	};\
	bool FSAPI mcb3d_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_PRESS_SYS_MASTER_EMERG,2);\
	return TRUE;\
	};\
	bool FSAPI mcb4_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_AIR_PRESS_SYS_SOUND_ALARM);\
	return TRUE;\
	};\
	bool FSAPI mcb5u_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_PRESS_SYS_MASTER,1);\
	return TRUE;\
	};\
	bool FSAPI mcb5c_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_PRESS_SYS_MASTER,0);\
	return TRUE;\
	};\
	bool FSAPI mcb5d_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_PRESS_SYS_MASTER,2);\
	return TRUE;\
	};\
	bool FSAPI mcb6u_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,3);\
	return TRUE;\
	};\
	bool FSAPI mcb6c_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,0);\
	return TRUE;\
	};\
	bool FSAPI mcb6dl_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,1);\
	return TRUE;\
	};\
	bool FSAPI mcb6dr_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,2);\
	return TRUE;\
	};\
	bool FSAPI mcb7u_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,3);\
	return TRUE;\
	};\
	bool FSAPI mcb7c_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,0);\
	return TRUE;\
	};\
	bool FSAPI mcb7dl_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,1);\
	return TRUE;\
	};\
	bool FSAPI mcb7dr_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,2);\
	return TRUE;\
	};\
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(AZS_AIR_PRESS_DROP_EMERG);) \
	static MAKE_TCB(N##_tcb_02,return AZS_TTGET(AZS_AIR_PRESS_CONTROL_BCKUP);) \
	static MAKE_TCB(N##_tcb_03,return AZS_TTGET(AZS_AIR_PRESS_SYS_MASTER_EMERG);) \
	static MAKE_TCB(N##_tcb_04,return AZS_TTGET(AZS_AIR_PRESS_SYS_SOUND_ALARM);) \
	static MAKE_TCB(N##_tcb_05,return AZS_TTGET(AZS_AIR_PRESS_SYS_MASTER);) \
	static MAKE_TCB(N##_tcb_06,return AZS_TTGET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN);) \
	static MAKE_TCB(N##_tcb_07,return AZS_TTGET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MAKE_TTA(N##_tcb_04) \
	MAKE_TTA(N##_tcb_05) \
	MAKE_TTA(N##_tcb_06) \
	MAKE_TTA(N##_tcb_07) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0, 79,186,CURSOR_HAND,MOUSE_LR,mcb1_##N,N) \
	MOUSE_TBOX2("2", 80,  0, 79,186,CURSOR_HAND,MOUSE_LR,mcb2_##N,N) \
	MOUSE_TBOX2("3",160,  0, 74, 70,CURSOR_HAND,MOUSE_LR,mcb3u_##N,N) \
	MOUSE_TBOX2("3",160, 70, 74, 70,CURSOR_HAND,MOUSE_LR,mcb3c_##N,N) \
	MOUSE_TBOX2("3",160,140, 74, 46,CURSOR_HAND,MOUSE_LR,mcb3d_##N,N) \
	MOUSE_TBOX2("4",235,  0, 74,186,CURSOR_HAND,MOUSE_LR,mcb4_##N,N) \
	MOUSE_TBOX2("5",310,  0, 79, 70,CURSOR_HAND,MOUSE_LR,mcb5u_##N,N) \
	MOUSE_TBOX2("5",310, 70, 79, 70,CURSOR_HAND,MOUSE_LR,mcb5c_##N,N) \
	MOUSE_TBOX2("5",310,140, 79, 46,CURSOR_HAND,MOUSE_LR,mcb5d_##N,N) \
	MOUSE_TBOX2("6",390,  0,135, 70,CURSOR_HAND,MOUSE_LR,mcb6u_##N,N) \
	MOUSE_TBOX2("6",390, 70,135, 90,CURSOR_HAND,MOUSE_LR,mcb6c_##N,N) \
	MOUSE_TBOX2("6",390,160, 66, 26,CURSOR_HAND,MOUSE_LR,mcb6dl_##N,N) \
	MOUSE_TBOX2("6",456,160, 66, 26,CURSOR_HAND,MOUSE_LR,mcb6dr_##N,N) \
	MOUSE_TBOX2("7",525,  0,155, 70,CURSOR_HAND,MOUSE_LR,mcb7u_##N,N) \
	MOUSE_TBOX2("7",525, 70,155, 90,CURSOR_HAND,MOUSE_LR,mcb7c_##N,N) \
	MOUSE_TBOX2("7",525,160, 76, 26,CURSOR_HAND,MOUSE_LR,mcb7dl_##N,N) \
	MOUSE_TBOX2("7",600,160, 76, 26,CURSOR_HAND,MOUSE_LR,mcb7dr_##N,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE1,I2,NULL,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define CPri67(N,I,I2,X1,Y1,X2,Y2) /**/ \
	bool FSAPI mcb4_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_STAIRS_PWR);\
	return TRUE;\
	};\
	bool FSAPI mcb3_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_STAIRS);\
	return TRUE;\
	};\
	bool FSAPI mcb2_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_NO_SMOKING_SIGNS);\
	return TRUE;\
	};\
	bool FSAPI mcb1_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	AZS_TGL(AZS_REDLIGHT1);\
	return TRUE;\
	};\
	static MAKE_TCB(N##_tcb_01,return AZS_TTGET(AZS_STAIRS_PWR);) \
	static MAKE_TCB(N##_tcb_02,return AZS_TTGET(AZS_STAIRS);) \
	static MAKE_TCB(N##_tcb_03,return AZS_TTGET(AZS_NO_SMOKING_SIGNS);) \
	static MAKE_TCB(N##_tcb_04,return AZS_TTGET(AZS_REDLIGHT1);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MAKE_TTA(N##_tcb_04) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("4",  0,  0,192,140,CURSOR_HAND,MOUSE_LR,mcb1_##N,N) \
	MOUSE_TBOX2("3",  0,140,192,105,CURSOR_HAND,MOUSE_LR,mcb2_##N,N) \
	MOUSE_TBOX2("2",  0,245,192,100,CURSOR_HAND,MOUSE_LR,mcb3_##N,N) \
	MOUSE_TBOX2("1",  0,345,192, 95,CURSOR_HAND,MOUSE_LR,mcb4_##N,N) \
	MOUSE_END \
	static double FSAPI icbr_##N(PELEMENT_ICON pelement) { \
	return POS_GET(POS_PANEL_STATE); \
	} \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
	} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2(N##IcoE1,I2,NULL,X2,Y2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2(N##Ico,I,&l_##N##IcoE1,X1,Y1,icbr_##N,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

SIMPLE_GAUGE_VC		(Pri_lft,VC6_PRI_LFT_D			);
SIMPLE_GAUGE_VC		(Pri_rgt,VC6_PRI_RGT_D			);
SIMPLE_GAUGE_VC		(Pri01	,VC6_PRI01_D			);
SIMPLE_GAUGE_VC		(Pri02	,VC6_PRI02_D			);
SIMPLE_GAUGE_VC		(Pri08	,VC6_PRI08_D			);
SIMPLE_GAUGE_VC		(Pri10	,VC6_PRI10_D			);
SIMPLE_GAUGE_VC		(Pri12	,VC6_PRI12_D			);
SIMPLE_GAUGE_VC		(Pri15	,VC6_PRI15_D			);
SIMPLE_GAUGE_VC		(Pri30	,VC6_PRI30_D			);
SIMPLE_GAUGE_VC		(Pri32	,VC6_PRI32_D			);
SIMPLE_GAUGE_VC		(Pri36	,VC6_PRI36_D			);
SIMPLE_GAUGE_VC		(Pri43	,VC6_PRI43_D			);
SIMPLE_GAUGE_VC		(Pri47	,VC6_PRI47_D			);
SIMPLE_GAUGE_V�_LANG(Pri03	,VC6_PRI03_D,0,0,VC6_PRI03INTL_D,0,0);
SIMPLE_GAUGE_V�_LANG(Pri09	,VC6_PRI09_D,0,0,VC6_PRI09INTL_D,0,0);
SIMPLE_GAUGE_V�_LANG(Pri21	,VC6_PRI21_D,0,0,VC6_PRI21INTL_D,0,0);
SIMPLE_GAUGE_V�_LANG(Pri56	,VC6_PRI56_D,0,0,VC6_PRI56INTL_D,0,0);
SIMPLE_GAUGE_V�_LANG(Pri59	,VC6_PRI59_D,0,0,VC6_PRI59INTL_D,0,0);
SIMPLE_GAUGE_V�_LANG(Pri60	,VC6_PRI60_D,0,0,VC6_PRI60INTL_D,0,0);
SIMPLE_GAUGE_VC_LANG1(Pri65	,VC6_PRI65_D,VC6_COV_PRI65INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC_LANG1(Pri55	,VC6_PRI55_D,VC6_COV_PRI55INTL_D,0,0,0,0);
SIMPLE_GAUGE_V�_CNT	(Pri58	,VC6_PRI58_D,0,0,VC6_PRI58INTL_D,0,0,AZS_TTGET(AZS_POWERSOURCE),AZS_SET(AZS_POWERSOURCE,POWER_SOURCE_RAP),MOUSE_LR);
SIMPLE_GAUGE_V�_CNT	(Pri11	,VC6_PRI11_D,0,0,VC6_PRI11INTL_D,0,0,BTN_TTGET(BTN_LIGHTS_TEST0),PRS_BTN(BTN_LIGHTS_TEST0),MOUSE_DLR);
SIMPLE_GAUGE_V�_CNT	(Pri14	,VC6_PRI14_D,0,0,VC6_PRI14INTL_D,0,0,POS_TTGET(POS_PANEL_LANG),POS_TGL(POS_PANEL_LANG),MOUSE_LR);
SIMPLE_GAUGE_V�_CNT	(Pri20	,VC6_PRI20_D,0,0,VC6_PRI20INTL_D,0,0,BTN_TTGET(BTN_DIS_ALARM_HYD_PUMP_1_FAIL),PRS_BTN(BTN_DIS_ALARM_HYD_PUMP_1_FAIL),MOUSE_DLR);
SIMPLE_GAUGE_V�_CNT	(Pri22	,VC6_PRI22_D,0,0,VC6_PRI22INTL_D,0,0,BTN_TTGET(BTN_DIS_ALARM_HYD_PUMP_2_FAIL),PRS_BTN(BTN_DIS_ALARM_HYD_PUMP_2_FAIL),MOUSE_DLR);
SIMPLE_GAUGE_V�_CNT	(Pri61	,VC6_PRI61_D,0,0,VC6_PRI61INTL_D,0,0,BTN_TTGET(BTN_LIGHTS_TEST1),PRS_BTN(BTN_LIGHTS_TEST1),MOUSE_DLR);
SIMPLE_GAUGE_VC_PRESS_BTN(Pri40	,BTN_DIS_FIRE_ALARM,VC6_PRI40_D);
SIMPLE_GAUGE_V�_AZS	(Pri04	,VC6_PRI04_D,AZS_WHITE_DECK_FLOOD_LIGHT);						
SIMPLE_GAUGE_V�_AZS	(Pri05	,VC6_PRI05_D,AZS_RED_PANEL_LIGHT);						
SIMPLE_GAUGE_VC     (Pri06	,VC6_PRI06_D);
SIMPLE_GAUGE_VC     (Pri13	,VC6_PRI13_D);
SIMPLE_GAUGE_VC     (Pri37	,VC6_PRI37_D);
SIMPLE_GAUGE_VC     (Pri38	,VC6_PRI38_D);
SIMPLE_GAUGE_VC     (Pri39	,VC6_PRI39_D);
SIMPLE_GAUGE_VC     (Pri48	,VC6_PRI48_D);
SIMPLE_GAUGE_V�_TT	(Pri33	,VC6_PRI33_D,NDL_TTGET(NDL_HTAIL_DEG));
SIMPLE_GAUGE_V�_TT	(Pri34	,VC6_PRI34_D,NDL_TTGET(NDL_FLAPS));
CPri57			(Pri57	,VC6_PRI57_D,VC6_COV_PRI57INTL1_D,VC6_COV_PRI57INTL2_D,0,0,6,0,0,85);
CPri07			(Pri07	,VC6_PRI07_D,VC6_PRI07A_INTL_D,VC6_PRI07B_INTL_D,VC6_PRI07C_INTL_D,0,0,0,0,197,210,192,82);
CPri49			(Pri49	,VC6_PRI49_D,VC6_PRI49INTL_D,0,0,0,0);
CPri64			(Pri64	,VC6_PRI64_D,0,0,VC6_PRI64INTL_D,0,0);
CPri63			(Pri63	,VC6_PRI63_D,VC6_COV_PRI63INTL1_D,VC6_COV_PRI63INTL2_D,0,0,49,0,10,118);
CPri62			(Pri62	,VC6_PRI62_D,VC6_COV_PRI62INTL_D,0,0,0,0);
CPri67			(Pri67	,VC6_PRI67_D,VC6_COV_PRI67INTL_D,0,0,0,0);

#define VC6_MOV_ALTIMPRESS100_M	   VC6_MOV_ALTIMPRESS100_R   
#define VC6_MOV_ALTIMPRESS10_M 	   VC6_MOV_ALTIMPRESS10_R    
#define VC6_MOV_ALTIMPRESS1_M	   VC6_MOV_ALTIMPRESS1_R     

static double FSAPI icb_Ico(PELEMENT_ICON pelement) 
{ 
	return POS_GET(POS_PANEL_STATE); 
} 

bool ChkColor(PELEMENT_MOVING_IMAGE pelement,int st) 
{ 
	if(POS_GET(POS_PANEL_STATE)==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

bool ChkColor(PELEMENT_NEEDLE pelement,int st) 
{ 
	if(POS_GET(POS_PANEL_STATE)==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

bool ChkColor(PELEMENT_SPRITE pelement,int st) 
{ 
	if(POS_GET(POS_PANEL_STATE)==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

double ChkLangRus(PELEMENT_NEEDLE pelement, int st, int nd1) 
{ 
	if(POS_GET(POS_PANEL_STATE)==st) { 
		if(!POS_GET(POS_PANEL_LANG)) {
			SHOW_IMAGE(pelement); 
			return NDL_GET(nd1);
		}
	}
	HIDE_IMAGE(pelement);
	return 0;
}; 

double ChkLangEng(PELEMENT_NEEDLE pelement, int st, int nd1) 
{ 
	if(POS_GET(POS_PANEL_STATE)==st) { 
		if(POS_GET(POS_PANEL_LANG)) {
			SHOW_IMAGE(pelement); 
			return NDL_GET(nd1);
		}
	}
	HIDE_IMAGE(pelement);
	return 0;
}; 

#define CPri16(N) /**/ \
	static double FSAPI icbscl_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_KUS_TAS);) \
	static MAKE_TCB(N##_tcb_02,return NDL_TTGET(NDL_UVID0);) \
	BOOL FSAPI mcb_kus0_big(PPIXPOINT relative_point,FLAGS32 mouse_flags) \
{ \
	ImportTable.pPanels->panel_window_toggle(IDENT_P110); \
	return true; \
} \
	BOOL FSAPI mcb_uvid0_big(PPIXPOINT relative_point,FLAGS32 mouse_flags) \
{ \
	ImportTable.pPanels->panel_window_toggle(IDENT_P10); \
	return true; \
} \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1", 21, 34,VC6_SCL_KUS1INTL_D_SX,VC6_SCL_KUS1INTL_D_SY,CURSOR_HAND,MOUSE_LR,mcb_kus0_big,N) \
	MOUSE_TBOX2("2", 21,313,VC6_SCL_UVIDINTL_D_SX,VC6_SCL_UVIDINTL_D_SY,CURSOR_HAND,MOUSE_LR,mcb_uvid0_big,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI16_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoSclUvidEng	,VC6_SCL_UVIDINTL_D		,NULL						, 21,313,icbscl_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoSclKusEng	,VC6_SCL_KUS1INTL_D		,&l_##N##IcoSclUvidEng		, 21, 34,icbscl_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Ico				,VC6_PRI16_D			,&l_##N##IcoSclKusEng		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI16_D,&l_##N##Ico, N);

CPri16(Pri16);

#define CPri17(N) /**/ \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_AGB0_MAIN_PLANE);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",  0,  0,VC6_PRI17_D_SX,VC6_PRI17_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI17_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Ico				,VC6_PRI17_D			,NULL,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI17_D,&l_##N##Ico, N);

CPri17(Pri17);

#define CPri29(N) /**/ \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_AGB_AUXL_PLANE);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",  0,  0,VC6_PRI29_D_SX,VC6_PRI29_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI29_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Ico				,VC6_PRI29_D			,NULL,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI29_D,&l_##N##Ico, N);

CPri29(Pri29);

#define CPri18(N) /**/ \
	static double FSAPI icbblkc_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_KPPMS0_BLK_COURSE)) \
	return POS_GET(POS_PANEL_STATE); \
		else \
		return -1;\
} \
	static double FSAPI icbblkg_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_KPPMS0_BLK_GLIDE)) \
	return POS_GET(POS_PANEL_STATE); \
		else \
		return -1;\
} \
	static double FSAPI icbcov_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} \
	HIDE_IMAGE(pelement);\
	return -1;\
} \
	BOOL FSAPI mcb_kppms0_big(PPIXPOINT relative_point,FLAGS32 mouse_flags) \
{ \
	ImportTable.pPanels->panel_window_toggle(IDENT_P12); \
	return true; \
} \
	bool FSAPI mcb_kppms0(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	NDL_DEC(NDL_KPPMS0_SCALE); \
	KRM_DEC(KRM_KPPMS0); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	KRM_INC(KRM_KPPMS0); \
	NDL_INC(NDL_KPPMS0_SCALE); \
	} \
	return TRUE; \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_KPPMS0_SCALE);) \
	static MAKE_TCB(N##_tcb_02,return NDL_TTGET(NDL_MAIN_MIN	);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,VC6_PRI18_D_SX-40,VC6_PRI18_D_SY-40,CURSOR_HAND,MOUSE_LR,mcb_kppms0_big,N) \
	MOUSE_TBOX2("2", 247,264,VC6_PRI18_D_SX,VC6_PRI18_D_SY,CURSOR_HAND,MOUSE_DLR,mcb_kppms0,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI18_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##BlkG  			,VC6_BLK_KPPMS1GLIDE_D		,NULL				,113,130,icbblkg_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##BlkC  			,VC6_BLK_KPPMS1COURSE_D		,&l_##N##BlkG  		,171,171,icbblkc_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoCov			,VC6_COV_KPPMS1INTL_D		,&l_##N##BlkC  		, 78, 94,icbcov_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Ico				,VC6_PRI18_D				,&l_##N##IcoCov		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI18_D,&l_##N##Ico, N);

CPri18(Pri18);

#define CPri19(N) /**/ \
	BOOL FSAPI mcb_##N##_reset(PPIXPOINT relative_point,FLAGS32 mouse_flags) \
	{ \
		PRS_BTN(BTN_GFORCE_RESET);\
		return true; \
	} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_GFORCE_CUR);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,VC6_PRI19_D_SX,VC6_PRI19_D_SY,CURSOR_HAND,MOUSE_LR,mcb_##N##_reset,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI19_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Ico				,VC6_PRI19_D			,NULL,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI19_D,&l_##N##Ico, N);

CPri19(Pri19);

#define CPri23(N) /**/ \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_MAIN_HYD);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",  0,  0,VC6_PRI23_D_SX,VC6_PRI23_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI23_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Ico				,VC6_PRI23_D			,NULL,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI23_D,&l_##N##Ico, N);

CPri23(Pri23);

#define CPri24(N) /**/ \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_MAIN_BRAKE_HYD_LEFT);) \
	static MAKE_TCB(N##_tcb_02,return NDL_TTGET(NDL_MAIN_BRAKE_HYD_RIGHT);) \
	static MAKE_TCB(N##_tcb_03,return NDL_TTGET(NDL_AIR_TEMP);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",  0,  0,VC6_PRI24_D_SX/3,VC6_PRI24_D_SY,N) \
	MOUSE_TTPB2("2",  VC6_PRI24_D_SX/3,  0,VC6_PRI24_D_SX/3,VC6_PRI24_D_SY,N) \
	MOUSE_TTPB2("3",  VC6_PRI24_D_SX/3*2,  0,VC6_PRI24_D_SX/3,VC6_PRI24_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI24_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Ico						,VC6_PRI24_D				,NULL,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI24_D,&l_##N##Ico, N);

CPri24(Pri24);

#define CPri25(N) /**/ \
	BOOL FSAPI mcb_da30_big(PPIXPOINT relative_point,FLAGS32 mouse_flags) \
	{ \
		ImportTable.pPanels->panel_window_toggle(IDENT_P18); \
		return true; \
	} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_DA30_TURN);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,VC6_PRI25_D_SX,VC6_PRI25_D_SY,CURSOR_HAND,MOUSE_LR,mcb_da30_big,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI25_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Ico				,VC6_PRI25_D			,NULL,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI25_D,&l_##N##Ico, N);

CPri25(Pri25);

#define CPri26(N) /**/ \
	static double FSAPI icbL_##N(PELEMENT_ICON pelement) { \
	int glt=(int)GLT_GET(GLT_IKU_LEFT);\
	int lng=(int)POS_GET(POS_PANEL_LANG);\
	int sta=(int)POS_GET(POS_PANEL_STATE)*6;\
	switch(glt) {\
			case 0:\
			return lng?0+sta:2+sta;\
			break;\
			case 1:\
			return 4+sta;\
			break;\
			case 2:\
			return 5+sta;\
			break;\
			case 3:\
			return lng?1+sta:3+sta;\
			break;\
}\
	return -1;\
} \
	static double FSAPI icbR_##N(PELEMENT_ICON pelement) { \
	int glt=(int)GLT_GET(GLT_IKU_RIGHT);\
	int lng=(int)POS_GET(POS_PANEL_LANG);\
	int sta=(int)POS_GET(POS_PANEL_STATE)*6;\
	switch(glt) {\
			case 0:\
			return lng?0+sta:2+sta;\
			break;\
			case 1:\
			return 4+sta;\
			break;\
			case 2:\
			return 5+sta;\
			break;\
			case 3:\
			return lng?1+sta:3+sta;\
			break;\
}\
	return -1;\
} \
	BOOL FSAPI mcb_iku0_big(PPIXPOINT relative_point,FLAGS32 mouse_flags) \
{ \
	ImportTable.pPanels->panel_window_toggle(IDENT_P11); \
	return true; \
} \
	bool FSAPI mcb_iku0(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(GLT_IKU_LEFT); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(GLT_IKU_LEFT); \
	} \
	return TRUE; \
} \
	bool FSAPI mcb_iku1(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	if(mouse_flags&MOUSE_LEFTSINGLE) { \
	GLT_DEC(GLT_IKU_RIGHT); \
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) { \
	GLT_INC(GLT_IKU_RIGHT); \
	} \
	return TRUE; \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_IKU_WHITE);) \
	static MAKE_TCB(N##_tcb_02,return GLT_TTGET(GLT_IKU_LEFT);) \
	static MAKE_TCB(N##_tcb_03,return GLT_TTGET(GLT_IKU_RIGHT);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",  0,  0,VC6_PRI26_D_SX,260,CURSOR_HAND,MOUSE_LR,mcb_iku0_big,N) \
	MOUSE_TBOX2("2", 0,261,100,VC6_PRI26_D_SY-260,CURSOR_HAND,MOUSE_LR,mcb_iku0,N) \
	MOUSE_TBOX2("3", VC6_PRI26_D_SX-100,261,100,VC6_PRI26_D_SY-260,CURSOR_HAND,MOUSE_LR,mcb_iku1,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI26_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoSelR		,VC6_SCL_IKUSELECTORR_D_ADF1,NULL				,131,287,icbR_##N,6*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoSelL		,VC6_SCL_IKUSELECTORL_D_ADF1,&l_##N##IcoSelR	, 68,287,icbL_##N,6*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Ico			,VC6_PRI26_D				,&l_##N##IcoSelL	,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI26_D,&l_##N##Ico, N);

CPri26(Pri26);

#define CPri27(N) /**/ \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} else {\
	HIDE_IMAGE(pelement);\
	return -1;\
	} \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_RV3M_ALT);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",  0,  0,VC6_PRI27_D_SX,VC6_PRI27_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI27_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoE		,VC6_SCL_RV3MINTL_D		,NULL,  0,  0,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoR		,VC6_PRI27_D			,&l_##N##IcoE		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI27_D,&l_##N##IcoR, N);

CPri27(Pri27);

#define CPri28(N) /**/ \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} else {\
	HIDE_IMAGE(pelement);\
	return -1;\
	} \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_PPTIZ);) \
	static MAKE_TCB(N##_tcb_02,return BTN_TTGET(BTN_PPTIZ_TEST_0);) \
	static MAKE_TCB(N##_tcb_03,return BTN_TTGET(BTN_PPTIZ_TEST_4000);) \
	static MAKE_TCB(N##_tcb_04,return AZS_TTGET(AZS_FUEL_IND);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MAKE_TTA(N##_tcb_04) \
	MOUSE_TOOLTIP_ARGS_END \
	bool FSAPI mcb0_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	PRS_BTN(BTN_PPTIZ_TEST_0);\
	return TRUE;\
};\
	bool FSAPI mcb4000_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	PRS_BTN(BTN_PPTIZ_TEST_4000);\
	return TRUE;\
};\
	bool FSAPI mcbfl_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	PRS_AZS(AZS_FUEL_IND					,1);\
	return TRUE;\
};\
	bool FSAPI mcbfr_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	PRS_AZS(AZS_FUEL_IND					,2);\
	return TRUE;\
};\
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1", 10, 20,240,210,N) \
	MOUSE_TBOX2("2", 60,245, 40, 40,CURSOR_HAND,MOUSE_DLR,mcb0_##N,N) \
	MOUSE_TBOX2("3",165,245, 40, 40,CURSOR_HAND,MOUSE_DLR,mcb4000_##N,N) \
	MOUSE_TBOX2("4", 45,290, 95, 80,CURSOR_HAND,MOUSE_DLR,mcbfl_##N,N) \
	MOUSE_TBOX2("4",140,290, 95, 80,CURSOR_HAND,MOUSE_DLR,mcbfr_##N,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI28_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoE		,VC6_PRI28INTL_D		,NULL,  0,  0,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoR		,VC6_PRI28_D			,&l_##N##IcoE		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI28_D,&l_##N##IcoR, N);

CPri28(Pri28);

#define CPri35(N) /**/ \
	double Tablo(int tbl) {\
	int t=TBGE_GET(tbl);\
	int brt=AZS_GET(AZS_WARNING_LIGHTS);\
	int sta=int(POS_GET(POS_PANEL_STATE));\
	if(t) {\
	return brt?sta*3+2:sta*3+1;\
	}\
	return sta*3;\
}\
	double TabloI(int tbl) {\
	int t=TBGE_GET(tbl);\
	int brt=AZS_GET(AZS_WARNING_LIGHTS);\
	int lng=int(POS_GET(POS_PANEL_LANG));\
	int sta=int(POS_GET(POS_PANEL_STATE));\
	if(t) {\
	if(brt) {\
	return lng?sta*3+2:sta*3+(PANEL_LIGHT_MAX*3+2);\
	} else {\
	return lng?sta*3+1:sta*3+(PANEL_LIGHT_MAX*3+1);\
	}\
	}\
	return lng?sta*3:sta*3+(PANEL_LIGHT_MAX*3);\
}\
	static double FSAPI icbGw2_##N(PELEMENT_ICON pelement) {SHOW_TBG(TBG_WARN_FLAP ,BTN_GET(BTN_GEAR_LIGHTS_TEST),3); return TabloI(TBG_WARN_FLAP ); }\
	static double FSAPI icbGw1_##N(PELEMENT_ICON pelement) {SHOW_TBG(TBG_WARN_GEAR ,BTN_GET(BTN_GEAR_LIGHTS_TEST),3); return TabloI(TBG_WARN_GEAR ); }\
	static double FSAPI icbGu3_##N(PELEMENT_ICON pelement) {SHOW_TBG(TBG_GEAR3_UP  ,BTN_GET(BTN_GEAR_LIGHTS_TEST),3); return Tablo (TBG_GEAR3_UP  ); }\
	static double FSAPI icbGu2_##N(PELEMENT_ICON pelement) {SHOW_TBG(TBG_GEAR2_UP  ,BTN_GET(BTN_GEAR_LIGHTS_TEST),3); return Tablo (TBG_GEAR2_UP  ); }\
	static double FSAPI icbGu1_##N(PELEMENT_ICON pelement) {SHOW_TBG(TBG_GEAR1_UP  ,BTN_GET(BTN_GEAR_LIGHTS_TEST),3); return Tablo (TBG_GEAR1_UP  ); }\
	static double FSAPI icbGd3_##N(PELEMENT_ICON pelement) {SHOW_TBG(TBG_GEAR3_DOWN,BTN_GET(BTN_GEAR_LIGHTS_TEST),3); return Tablo (TBG_GEAR3_DOWN); }\
	static double FSAPI icbGd2_##N(PELEMENT_ICON pelement) {SHOW_TBG(TBG_GEAR2_DOWN,BTN_GET(BTN_GEAR_LIGHTS_TEST),3); return Tablo (TBG_GEAR2_DOWN); }\
	static double FSAPI icbGd1_##N(PELEMENT_ICON pelement) {SHOW_TBG(TBG_GEAR1_DOWN,BTN_GET(BTN_GEAR_LIGHTS_TEST),3); return Tablo (TBG_GEAR1_DOWN); }\
	static MAKE_TCB(N##_tcb_01,return BTN_TTGET(BTN_GEAR_LIGHTS_TEST);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	bool FSAPI mcb0_##N(PPIXPOINT relative_point,FLAGS32 mouse_flags) { \
	PRS_BTN(BTN_GEAR_LIGHTS_TEST);\
	return TRUE;\
};\
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TBOX2("1",120, 95, 80, 55,CURSOR_HAND,MOUSE_DLR,mcb0_##N,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI35_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Gw2		,VC6_TBL_GEARWARN2INTL_D_00	,NULL		,215, 16,icbGw2_##N,6*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Gw1		,VC6_TBL_GEARWARN1INTL_D_00	,&l_##N##Gw2, 17, 16,icbGw1_##N,6*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Gu3		,VC6_TBL_GEARUP3_D_00		,&l_##N##Gw1,183, 35,icbGu3_##N,3*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Gu2		,VC6_TBL_GEARUP2_D_00		,&l_##N##Gu3,144,  8,icbGu2_##N,3*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Gu1		,VC6_TBL_GEARUP1_D_00		,&l_##N##Gu2, 99, 35,icbGu1_##N,3*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Gd3		,VC6_TBL_GEARDWN3_D_00		,&l_##N##Gu1,186, 76,icbGd3_##N,3*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Gd2		,VC6_TBL_GEARDWN2_D_00		,&l_##N##Gd3,144, 53,icbGd2_##N,3*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Gd1		,VC6_TBL_GEARDWN1_D_00		,&l_##N##Gd2, 99, 76,icbGd1_##N,3*PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##Ico		,VC6_PRI35_D				,&l_##N##Gd1,  0,  0,icb_Ico,4, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI35_D,&l_##N##Ico, N);

CPri35(Pri35);

#define CPri41(N) /**/ \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_ENG1_N1);) \
	static MAKE_TCB(N##_tcb_02,return NDL_TTGET(NDL_ENG2_N1);) \
	static MAKE_TCB(N##_tcb_03,return NDL_TTGET(NDL_ENG3_N1);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",0,0,VC6_PRI41_D_SX/3,VC6_PRI41_D_SY,N) \
	MOUSE_TTPB2("2",VC6_PRI41_D_SX/3,0,VC6_PRI41_D_SX/3,VC6_PRI41_D_SY,N) \
	MOUSE_TTPB2("3",VC6_PRI41_D_SX/3*2,0,VC6_PRI41_D_SX/3,VC6_PRI41_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI41_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Ico			,VC6_PRI41_D		,NULL,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI41_D,&l_##N##Ico, N);

CPri41(Pri41);

#define CPri42(N) /**/ \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_ENG1_FUEL_PSI);) \
	static MAKE_TCB(N##_tcb_02,return NDL_TTGET(NDL_ENG2_FUEL_PSI);) \
	static MAKE_TCB(N##_tcb_03,return NDL_TTGET(NDL_ENG3_FUEL_PSI);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",0,0,VC6_PRI42_D_SX/3,VC6_PRI42_D_SY,N) \
	MOUSE_TTPB2("2",VC6_PRI42_D_SX/3,0,VC6_PRI42_D_SX/3,VC6_PRI42_D_SY,N) \
	MOUSE_TTPB2("3",VC6_PRI42_D_SX/3*2,0,VC6_PRI42_D_SX/3,VC6_PRI42_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI41_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Ico		,VC6_PRI42_D			,NULL,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI42_D,&l_##N##Ico, N);

CPri42(Pri42);

#define CPri44(N) /**/ \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_ARK1);) \
	static MAKE_TCB(N##_tcb_02,return NDL_TTGET(NDL_UVPD_ALT);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1",0,0,VC6_PRI44_D_SX,VC6_PRI44_D_SY/2,N) \
	MOUSE_TTPB2("2",0,VC6_PRI44_D_SY/2,VC6_PRI44_D_SX,VC6_PRI44_D_SY/2,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI44_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##Ico		,VC6_PRI44_D			,NULL,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI44_D,&l_##N##Ico, N);

CPri44(Pri44);

#define CPri45(N) /**/ \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} else {\
	HIDE_IMAGE(pelement);\
	return -1;\
	} \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_VS);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1", 0, 0,VC6_PRI45_D_SX,VC6_PRI45_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI45_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoE		,VC6_SCL_VAR10INTL_D	,NULL,  0,  0,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoR		,VC6_PRI45_D			,&l_##N##IcoE		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI45_D,&l_##N##IcoR, N);

CPri45(Pri45);

#define CPri51(N) /**/ \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} else {\
	HIDE_IMAGE(pelement);\
	return -1;\
	} \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_VS);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1", 0, 0,VC6_PRI51_D_SX,VC6_PRI51_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI51_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoE		,VC6_PRI51INTL_D		,NULL,  0,  0,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoR		,VC6_PRI51_D			,&l_##N##IcoE		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI51_D,&l_##N##IcoR, N);

CPri51(Pri51);

#define CPri50(N) /**/ \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} else {\
	HIDE_IMAGE(pelement);\
	return -1;\
	} \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_AMPSGEN1);) \
	static MAKE_TCB(N##_tcb_02,return NDL_TTGET(NDL_AMPSGEN1);) \
	static MAKE_TCB(N##_tcb_03,return NDL_TTGET(NDL_AMPSGEN1);) \
	static MAKE_TCB(N##_tcb_04,return NDL_TTGET(NDL_AMPS27);) \
	static MAKE_TCB(N##_tcb_05,return NDL_TTGET(NDL_VOLTS27);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MAKE_TTA(N##_tcb_04) \
	MAKE_TTA(N##_tcb_05) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1", 0, 0,VC6_PRI50_D_SX/5,VC6_PRI50_D_SY,N) \
	MOUSE_TTPB2("2", VC6_PRI50_D_SX/5, 0,VC6_PRI50_D_SX/5,VC6_PRI50_D_SY,N) \
	MOUSE_TTPB2("3", VC6_PRI50_D_SX/5*2, 0,VC6_PRI50_D_SX/5,VC6_PRI50_D_SY,N) \
	MOUSE_TTPB2("4", VC6_PRI50_D_SX/5*3, 0,VC6_PRI50_D_SX/5,VC6_PRI50_D_SY,N) \
	MOUSE_TTPB2("5", VC6_PRI50_D_SX/5*4, 0,VC6_PRI50_D_SX/5,VC6_PRI50_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI50_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoE		,VC6_COV_PRI50INTL_D	,NULL,  8,149,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoR		,VC6_PRI50_D			,&l_##N##IcoE		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI50_D,&l_##N##IcoR, N);

CPri50(Pri50);

#define CPri52(N) /**/ \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} else {\
	HIDE_IMAGE(pelement);\
	return -1;\
	} \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_URVK);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1", 0, 0,VC6_PRI52_D_SX,VC6_PRI52_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI52_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoE		,VC6_PRI52INTL_D		,NULL,  0,  0,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoR		,VC6_PRI52_D			,&l_##N##IcoE		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI52_D,&l_##N##IcoR, N);

CPri52(Pri52);

#define CPri53(N) /**/ \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} else {\
	HIDE_IMAGE(pelement);\
	return -1;\
	} \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_VOLTS36);) \
	static MAKE_TCB(N##_tcb_02,return NDL_TTGET(NDL_VOLTS115);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1", 0, 0,VC6_PRI53_D_SX/2,VC6_PRI53_D_SY,N) \
	MOUSE_TTPB2("2", VC6_PRI53_D_SX/2, 0,VC6_PRI53_D_SX/2,VC6_PRI53_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI53_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoE		,VC6_COV_PRI53INTL_D	,NULL, 35,  2,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoR		,VC6_PRI53_D			,&l_##N##IcoE		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI53_D,&l_##N##IcoR, N);

CPri53(Pri53);

#define CPri54(N) /**/ \
	static double FSAPI icbe_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} else {\
	HIDE_IMAGE(pelement);\
	return -1;\
	} \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_TEMP_SALON);) \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2("1", 0, 0,VC6_PRI54_D_SX,VC6_PRI54_D_SY,N) \
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI54_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoE		,VC6_COV_PRI54INTL_D	,NULL,  6,  0,icbe_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoR		,VC6_PRI54_D			,&l_##N##IcoE		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI54_D,&l_##N##IcoR, N);

CPri54(Pri54);

static double FSAPI icbIco1R_Pri66(PELEMENT_ICON pelement) 
{ 
	if(!POS_GET(POS_PANEL_LANG)) {
		SHOW_IMAGE(pelement);
		return LMP_GET(LMP_GMK_OSN)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIco1E_Pri66(PELEMENT_ICON pelement) 
{ 
	if(POS_GET(POS_PANEL_LANG)) {
		SHOW_IMAGE(pelement);
		return LMP_GET(LMP_GMK_OSN)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIco2R_Pri66(PELEMENT_ICON pelement) 
{
	if(!POS_GET(POS_PANEL_LANG)) {
		SHOW_IMAGE(pelement);
		return LMP_GET(LMP_GMK_ZAP)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIco2E_Pri66(PELEMENT_ICON pelement) 
{ 
	if(POS_GET(POS_PANEL_LANG)) {
		SHOW_IMAGE(pelement);
		return LMP_GET(LMP_GMK_ZAP)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIcoR_Pri66(PELEMENT_ICON pelement) 
{
	if(!POS_GET(POS_PANEL_LANG)) {
		SHOW_IMAGE(pelement);
		return PWR_GET(PWR_GMK)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIcoE_Pri66(PELEMENT_ICON pelement) 
{
	if(POS_GET(POS_PANEL_LANG)) {
		SHOW_IMAGE(pelement);
		return PWR_GET(PWR_GMK)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

double ScaleOff(PELEMENT_NEEDLE pelement,int clr) 
{
	if(POS_GET(POS_PANEL_STATE)==clr&&!PWR_GET(PWR_GMK)) {
		SHOW_IMAGE(pelement);
		return NDL_GET(NDL_GMK_SCALE)*4;
	}
	HIDE_IMAGE(pelement);
	return 0;
};

double ScaleOn(PELEMENT_NEEDLE pelement,int clr) 
{
	if(POS_GET(POS_PANEL_STATE)==clr&&PWR_GET(PWR_GMK)) {
		SHOW_IMAGE(pelement);
		return NDL_GET(NDL_GMK_SCALE)*4;
	}
	HIDE_IMAGE(pelement);
	return 0;
};

static double FSAPI icbNdl2R_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOn(pelement,PANEL_LIGHT_RED); };
static double FSAPI icbNdl2P_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOn(pelement,PANEL_LIGHT_PLF); };
static double FSAPI icbNdl2M_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOn(pelement,PANEL_LIGHT_MIX); };
static double FSAPI icbNdl2D_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOn(pelement,PANEL_LIGHT_DAY); };
static double FSAPI icbNdl1R_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOff(pelement,PANEL_LIGHT_RED); };
static double FSAPI icbNdl1P_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOff(pelement,PANEL_LIGHT_PLF); };
static double FSAPI icbNdl1M_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOff(pelement,PANEL_LIGHT_MIX); };
static double FSAPI icbNdl1D_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOff(pelement,PANEL_LIGHT_DAY); };

static MAKE_TCB(Pri66_tcb_01,return AZS_TTGET(AZS_GMK_HEMISPHERE);) 
static MAKE_TCB(Pri66_tcb_02,return AZS_TTGET(AZS_GMK_PRI_AUX);) 
static MAKE_TCB(Pri66_tcb_03,return AZS_TTGET(AZS_GMK_MODE);) 
static MAKE_TCB(Pri66_tcb_04,return AZS_TTGET(AZS_GMK_TEST);) 
static MAKE_TCB(Pri66_tcb_05,return AZS_TTGET(AZS_GMK_COURSE);) 
static MAKE_TCB(Pri66_tcb_06,HND_TT(HND_GMK			))
static MAKE_TCB(Pri66_tcb_07,LMP_TT(LMP_GMK_ZAP		))
static MAKE_TCB(Pri66_tcb_08,LMP_TT(LMP_GMK_OSN		))
static MAKE_TCB(Pri66_tcb_09,POS_TT(POS_MAX    		))

static MOUSE_TOOLTIP_ARGS(Pri66_ttargs) 
MAKE_TTA(Pri66_tcb_01) 
MAKE_TTA(Pri66_tcb_02) 
MAKE_TTA(Pri66_tcb_03) 
MAKE_TTA(Pri66_tcb_04) 
MAKE_TTA(Pri66_tcb_05) 
MAKE_TTA(Pri66_tcb_06) 
MAKE_TTA(Pri66_tcb_07) 
MAKE_TTA(Pri66_tcb_08) 
MAKE_TTA(Pri66_tcb_09) 
MOUSE_TOOLTIP_ARGS_END

bool FSAPI mcbsevuzh_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{
	AZS_TGL(AZS_GMK_HEMISPHERE);
	return TRUE;
};

bool FSAPI mcbpriaux_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	AZS_TGL(AZS_GMK_PRI_AUX);
	return TRUE;
};

bool FSAPI mcbmkgpk_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	AZS_TGL(AZS_GMK_MODE);
	return TRUE;
};

bool FSAPI mcbtstl_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	PRS_AZS(AZS_GMK_TEST,1);
	return TRUE;
};

bool FSAPI mcbtstr_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	PRS_AZS(AZS_GMK_TEST,2);
	return TRUE;
};

bool FSAPI mcbzkl_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{
	PRS_AZS(AZS_GMK_COURSE,1);
	return TRUE;
};

bool FSAPI mcbzkr_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	PRS_AZS(AZS_GMK_COURSE,2);
	return TRUE;
};

static BOOL FSAPI mcb_gmk_rightPri66(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		NDL_INC(NDL_GMK_SCALE);
		HND_INC(HND_GMK);
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		NDL_INC2(NDL_GMK_SCALE,10);
		HND_INC2(HND_GMK,10);
		if(HND_GET(HND_GMK)>89)HND_DEC2(HND_GMK,89);
	}
	return TRUE;
}

static BOOL FSAPI mcb_gmk_leftPri66(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		NDL_DEC(NDL_GMK_SCALE);
		HND_DEC(HND_GMK);
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		NDL_DEC2(NDL_GMK_SCALE,10);
		HND_DEC2(HND_GMK,10);
		if(HND_GET(HND_GMK)<0)HND_INC2(HND_GMK,89);
	}
	return TRUE;
}

static BOOL FSAPI mcb_gmk_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	ImportTable.pPanels->panel_window_toggle(IDENT_P15);
	return true;
}

MOUSE_BEGIN(rect_Pri66,HELP_NONE,0,0) 
MOUSE_BOX2(   0,  0, VC6_PRI66_D_SX, VC6_PRI66_D_SY,CURSOR_HAND,MOUSE_LR,mcb_gmk_big,Pri66)    // GMK BIG
MOUSE_TBOX2("1", 50, 15,75,65,CURSOR_HAND,MOUSE_LR,mcbsevuzh_Pri66,Pri66) 
MOUSE_TBOX2("2",175, 15,75,65,CURSOR_HAND,MOUSE_LR,mcbpriaux_Pri66,Pri66) 
MOUSE_TBOX2("3",300, 15,75,65,CURSOR_HAND,MOUSE_LR,mcbmkgpk_Pri66,Pri66) 
MOUSE_TBOX2("4", 30,170,55,80,CURSOR_HAND,MOUSE_DLR,mcbtstl_Pri66,Pri66) 
MOUSE_TBOX2("4", 85,170,55,80,CURSOR_HAND,MOUSE_DLR,mcbtstr_Pri66,Pri66) 
MOUSE_TBOX2("5",280,170,55,80,CURSOR_HAND,MOUSE_DLR,mcbzkl_Pri66,Pri66) 
MOUSE_TBOX2("5",335,170,55,80,CURSOR_HAND,MOUSE_DLR,mcbzkr_Pri66,Pri66) 
MOUSE_TSHB2( "6",  140,  130,145, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_gmk_leftPri66,mcb_gmk_rightPri66,Pri66) 
MOUSE_TTPB2( "7",  121,   5, 61,52,Pri66)
MOUSE_TTPB2( "8",  243,   5, 61,52,Pri66)
MOUSE_END 

extern PELEMENT_HEADER Pri66_list; 
GAUGE_HEADER_FS700_EX(VC6_PRI66_D_SX, "Pri66", &Pri66_list, rect_Pri66, 0, 0, 0, 0, Pri66); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(Pri66Ico5E		,VC6_TBL_GMK5INTL_D_00	,NULL				,131,188,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico5R		,VC6_TBL_GMK5_D_00		,&l_Pri66Ico5E		,131,188,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico4E		,VC6_TBL_GMK4INTL_D_00	,&l_Pri66Ico5R		,205, 57,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico4R		,VC6_TBL_GMK4_D_00		,&l_Pri66Ico4E		,205, 57,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico3E		,VC6_TBL_GMK3INTL_D_00	,&l_Pri66Ico4R		, 19, 57,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico3R		,VC6_TBL_GMK3_D_00		,&l_Pri66Ico3E		, 19, 57,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico2E		,VC6_TBL_GMK2INTL_D_00	,&l_Pri66Ico3R		,243,  5,icbIco2E_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico2R		,VC6_TBL_GMK2_D_00		,&l_Pri66Ico2E		,243,  5,icbIco2R_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico1E		,VC6_TBL_GMK1INTL_D_00	,&l_Pri66Ico2R		,121,  5,icbIco1E_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico1R		,VC6_TBL_GMK1_D_00		,&l_Pri66Ico1E		,121,  5,icbIco1R_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_NEEDLE2	(Pri66Ndl2R		,VC6_SCL_GMK_R_01		,&l_Pri66Ico1R		,212,165, 70, 70,icbNdl2R_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl2P		,VC6_SCL_GMK_P_01		,&l_Pri66Ndl2R		,212,165, 70, 70,icbNdl2P_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl2M		,VC6_SCL_GMK_M_01		,&l_Pri66Ndl2P		,212,165, 70, 70,icbNdl2M_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl2D		,VC6_SCL_GMK_D_01		,&l_Pri66Ndl2M		,212,165, 70, 70,icbNdl2D_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1R		,VC6_SCL_GMK_R_00		,&l_Pri66Ndl2D		,212,165, 70, 70,icbNdl1R_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1P		,VC6_SCL_GMK_P_00		,&l_Pri66Ndl1R		,212,165, 70, 70,icbNdl1P_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1M		,VC6_SCL_GMK_M_00		,&l_Pri66Ndl1P		,212,165, 70, 70,icbNdl1M_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1D		,VC6_SCL_GMK_D_00		,&l_Pri66Ndl1M		,212,165, 70, 70,icbNdl1D_Pri66 ,0,0,Pri66);
MY_ICON2	(Pri66IcoR		,VC6_PRI66_D			,&l_Pri66Ndl1D		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, Pri66); 
MY_STATIC2(Pri66bg,Pri66_list,VC6_PRI66_D,&l_Pri66IcoR, Pri66);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(Pri66Ico5E		,VC6_TBL_GMK5INTL_D_00	,NULL				,131,188,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico5R		,VC6_TBL_GMK5_D_00		,&l_Pri66Ico5E		,131,188,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico4E		,VC6_TBL_GMK4INTL_D_00	,&l_Pri66Ico5R		,205, 57,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico4R		,VC6_TBL_GMK4_D_00		,&l_Pri66Ico4E		,205, 57,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico3E		,VC6_TBL_GMK3INTL_D_00	,&l_Pri66Ico4R		, 19, 57,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico3R		,VC6_TBL_GMK3_D_00		,&l_Pri66Ico3E		, 19, 57,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico2E		,VC6_TBL_GMK2INTL_D_00	,&l_Pri66Ico3R		,243,  5,icbIco2E_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico2R		,VC6_TBL_GMK2_D_00		,&l_Pri66Ico2E		,243,  5,icbIco2R_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico1E		,VC6_TBL_GMK1INTL_D_00	,&l_Pri66Ico2R		,121,  5,icbIco1E_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico1R		,VC6_TBL_GMK1_D_00		,&l_Pri66Ico1E		,121,  5,icbIco1R_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_NEEDLE2	(Pri66Ndl2P		,VC6_SCL_GMK_P_01		,&l_Pri66Ico1R		,212,165, 70, 70,icbNdl2P_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl2D		,VC6_SCL_GMK_D_01		,&l_Pri66Ndl2P		,212,165, 70, 70,icbNdl2D_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1P		,VC6_SCL_GMK_P_00		,&l_Pri66Ndl2D		,212,165, 70, 70,icbNdl1P_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1D		,VC6_SCL_GMK_D_00		,&l_Pri66Ndl1P		,212,165, 70, 70,icbNdl1D_Pri66 ,0,0,Pri66);
MY_ICON2	(Pri66IcoR		,VC6_PRI66_D			,&l_Pri66Ndl1D		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, Pri66); 
MY_STATIC2(Pri66bg,Pri66_list,VC6_PRI66_D,&l_Pri66IcoR, Pri66);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(Pri66Ico5E		,VC6_TBL_GMK5INTL_D_00	,NULL				,131,188,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico5R		,VC6_TBL_GMK5_D_00		,&l_Pri66Ico5E		,131,188,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico4E		,VC6_TBL_GMK4INTL_D_00	,&l_Pri66Ico5R		,205, 57,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico4R		,VC6_TBL_GMK4_D_00		,&l_Pri66Ico4E		,205, 57,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico3E		,VC6_TBL_GMK3INTL_D_00	,&l_Pri66Ico4R		, 19, 57,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico3R		,VC6_TBL_GMK3_D_00		,&l_Pri66Ico3E		, 19, 57,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico2E		,VC6_TBL_GMK2INTL_D_00	,&l_Pri66Ico3R		,243,  5,icbIco2E_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico2R		,VC6_TBL_GMK2_D_00		,&l_Pri66Ico2E		,243,  5,icbIco2R_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico1E		,VC6_TBL_GMK1INTL_D_00	,&l_Pri66Ico2R		,121,  5,icbIco1E_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico1R		,VC6_TBL_GMK1_D_00		,&l_Pri66Ico1E		,121,  5,icbIco1R_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_NEEDLE2	(Pri66Ndl2R		,VC6_SCL_GMK_R_01		,&l_Pri66Ico1R		,212,165, 70, 70,icbNdl2R_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl2D		,VC6_SCL_GMK_D_01		,&l_Pri66Ndl2R		,212,165, 70, 70,icbNdl2D_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1R		,VC6_SCL_GMK_R_00		,&l_Pri66Ndl2D		,212,165, 70, 70,icbNdl1R_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1D		,VC6_SCL_GMK_D_00		,&l_Pri66Ndl1R		,212,165, 70, 70,icbNdl1D_Pri66 ,0,0,Pri66);
MY_ICON2	(Pri66IcoR		,VC6_PRI66_D			,&l_Pri66Ndl1D		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, Pri66); 
MY_STATIC2(Pri66bg,Pri66_list,VC6_PRI66_D,&l_Pri66IcoR, Pri66);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(Pri66Ico5E		,VC6_TBL_GMK5INTL_D_00	,NULL				,131,188,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico5R		,VC6_TBL_GMK5_D_00		,&l_Pri66Ico5E		,131,188,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico4E		,VC6_TBL_GMK4INTL_D_00	,&l_Pri66Ico5R		,205, 57,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico4R		,VC6_TBL_GMK4_D_00		,&l_Pri66Ico4E		,205, 57,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico3E		,VC6_TBL_GMK3INTL_D_00	,&l_Pri66Ico4R		, 19, 57,icbIcoE_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico3R		,VC6_TBL_GMK3_D_00		,&l_Pri66Ico3E		, 19, 57,icbIcoR_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico2E		,VC6_TBL_GMK2INTL_D_00	,&l_Pri66Ico3R		,243,  5,icbIco2E_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico2R		,VC6_TBL_GMK2_D_00		,&l_Pri66Ico2E		,243,  5,icbIco2R_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico1E		,VC6_TBL_GMK1INTL_D_00	,&l_Pri66Ico2R		,121,  5,icbIco1E_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_ICON2	(Pri66Ico1R		,VC6_TBL_GMK1_D_00		,&l_Pri66Ico1E		,121,  5,icbIco1R_Pri66,2*PANEL_LIGHT_MAX, Pri66);
MY_NEEDLE2	(Pri66Ndl2M		,VC6_SCL_GMK_M_01		,&l_Pri66Ico1R		,212,165, 70, 70,icbNdl2M_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl2D		,VC6_SCL_GMK_D_01		,&l_Pri66Ndl2M		,212,165, 70, 70,icbNdl2D_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1M		,VC6_SCL_GMK_M_00		,&l_Pri66Ndl2D		,212,165, 70, 70,icbNdl1M_Pri66 ,0,0,Pri66);
MY_NEEDLE2	(Pri66Ndl1D		,VC6_SCL_GMK_D_00		,&l_Pri66Ndl1M		,212,165, 70, 70,icbNdl1D_Pri66 ,0,0,Pri66);
MY_ICON2	(Pri66IcoR		,VC6_PRI66_D			,&l_Pri66Ndl1D		,  0,  0,icb_Ico,PANEL_LIGHT_MAX, Pri66); 
MY_STATIC2(Pri66bg,Pri66_list,VC6_PRI66_D,&l_Pri66IcoR, Pri66);
#endif

#define CPri46(N) /**/ \
	static double FSAPI icbblkc_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_KPPMS0_BLK_COURSE)) \
	return POS_GET(POS_PANEL_STATE); \
		else \
		return -1;\
} \
	static double FSAPI icbblkg_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_KPPMS0_BLK_GLIDE)) \
	return POS_GET(POS_PANEL_STATE); \
		else \
		return -1;\
} \
	static double FSAPI icbcov_##N(PELEMENT_ICON pelement) { \
	if(POS_GET(POS_PANEL_LANG)) {\
	SHOW_IMAGE(pelement);\
	return POS_GET(POS_PANEL_STATE); \
	} \
	HIDE_IMAGE(pelement);\
	return -1;\
} \
	static double FSAPI icbscl_##N(PELEMENT_ICON pelement) { \
	if(!POS_GET(POS_PANEL_LANG)) { \
	HIDE_IMAGE(pelement); \
	return -1; \
	} \
	SHOW_IMAGE(pelement); \
	return POS_GET(POS_PANEL_STATE); \
} \
	static MAKE_TCB(N##_tcb_01,return NDL_TTGET(NDL_KUS_TAS);) \
	static MAKE_TCB(N##_tcb_02,return NDL_TTGET(NDL_UVID1_1000);) \
	static MAKE_TCB(N##_tcb_03,return NDL_TTGET(NDL_AGB1_MAIN_PLANE);) \
	static MAKE_TCB(N##_tcb_04,return NDL_TTGET(NDL_KPPMS1_SCALE);) \
	BOOL FSAPI mcb_kppms1_big(PPIXPOINT relative_point,FLAGS32 mouse_flags) \
{ \
	ImportTable.pPanels->panel_window_toggle(IDENT_P122); \
	return true; \
} \
	static MOUSE_TOOLTIP_ARGS(N##_ttargs) \
	MAKE_TTA(N##_tcb_01) \
	MAKE_TTA(N##_tcb_02) \
	MAKE_TTA(N##_tcb_03) \
	MAKE_TTA(N##_tcb_04) \
	MOUSE_TOOLTIP_ARGS_END \
	MOUSE_BEGIN(rect_##N,HELP_NONE,0,0) \
	MOUSE_TTPB2( "1",   14, 104, VC6_SCL_KUS2_INTL_D_SX,VC6_SCL_KUS2_INTL_D_SY,N)\
	MOUSE_TTPB2( "2",   21, 398, VC6_SCL_VD10INTL_D_SX,VC6_SCL_VD10INTL_D_SY,N)\
	MOUSE_TTPB2( "3",  353,  64, VC6_PRI17_D_SX,VC6_PRI17_D_SY,N)\
	MOUSE_TBOX2( "4",  355, 415, 270,300,CURSOR_HAND,MOUSE_LR,mcb_kppms1_big,N)\
	MOUSE_END \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(VC6_PRI46_D_SX, #N, &N##_list, rect_##N, 0, 0, 0, 0, N); \
	MY_ICON2	(N##IcoVD10			,VC6_SCL_VD10INTL_D			,NULL           		, 21,398,icbscl_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##BlkG  			,VC6_BLK_KPPMS2GLIDE_D		,&l_##N##IcoVD10		,504,585,icbblkg_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##BlkC  			,VC6_BLK_KPPMS2COURSE_D		,&l_##N##BlkG  			,446,544,icbblkc_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoCov2			,VC6_COV_KPPMS2INTL_D		,&l_##N##BlkC  			,411,508,icbcov_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoSclKusEng	,VC6_SCL_KUS2_INTL_D		,&l_##N##IcoCov2		, 14,104,icbscl_##N,PANEL_LIGHT_MAX, N); \
	MY_ICON2	(N##IcoR			,VC6_PRI46_D				,&l_##N##IcoSclKusEng	,  0,  0,icb_Ico,PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,VC6_PRI46_D,&l_##N##IcoR, N);

CPri46(Pri46);

static BOOL FSAPI mcb_navpanel_toggle(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	ImportTable.pPanels->panel_window_toggle(IDENT_P23);
	return true;
}

MOUSE_BEGIN(rect_Pri31,HELP_NONE,0,0) 
MOUSE_BOX2(   0,  0, VC6_PRI31_D_SX, VC6_PRI31_D_SY,CURSOR_HAND,MOUSE_LR,mcb_navpanel_toggle,Pri31)
MOUSE_END 

extern PELEMENT_HEADER Pri31_list; 
GAUGE_HEADER_FS700_EX(VC6_PRI31_D_SX, "Pri31", &Pri31_list, rect_Pri31, 0, 0, 0, 0, Pri31); 

MY_ICON2	(Pri31IcoR			,VC6_PRI31_D,NULL        ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, Pri31); 
MY_STATIC2  (Pri31bg,Pri31_list	,VC6_PRI31_D,&l_Pri31IcoR, Pri31);

#ifdef _DEBUG
DLLMAIND("F:\\MemLeaks\\Yak40\\VC6.log")
#else
DLLMAIN()
#endif

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&Pri_lft				),
		(&Pri_rgt				),
		(&Pri01					),
		(&Pri02					),
		(&Pri08					),        
		(&Pri10					),        
		(&Pri12					),        
		(&Pri15					),        
		(&Pri30					),        
		(&Pri31					),        
		(&Pri32					),        
		(&Pri36					),        
		(&Pri43					),        
		(&Pri47					),        
		(&Pri03					),        
		(&Pri09					),        
		(&Pri21					),        
		(&Pri56					),        
		(&Pri59					),        
		(&Pri60					),        
		(&Pri65					),            
		(&Pri55					),
		(&Pri58					),
		(&Pri11					),
		(&Pri14					),
		(&Pri20					),
		(&Pri22					),
		(&Pri61					),
		(&Pri40					),
		(&Pri04					),
		(&Pri05					),
		(&Pri06					),
		(&Pri13					),
		(&Pri37					),
		(&Pri38					),
		(&Pri39					),
		(&Pri48					),
		(&Pri33					),
		(&Pri34					),
		(&Pri57					),
		(&Pri07					),
		(&Pri49					),
		(&Pri64					),
		(&Pri63					),
		(&Pri62					),
		(&Pri67					),
		(&Pri16					),
		(&Pri17					),
		(&Pri29					),
		(&Pri18					),
		(&Pri19					),
		(&Pri23					),
		(&Pri24					),
		(&Pri25					),
		(&Pri26					),
		(&Pri27					),
		(&Pri28					),
		(&Pri35					),
		(&Pri41					),
		(&Pri42					),
		(&Pri44					),
		(&Pri45					),
		(&Pri51					),
		(&Pri50					),
		(&Pri52					),
		(&Pri53					),
		(&Pri54					),
		(&Pri66					),
		(&Pri46					),
		0					
	}											
};												
							
