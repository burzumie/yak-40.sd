/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P1.h"

#define OFFX	453
#define OFFY	468

//////////////////////////////////////////////////////////////////////////

// ��� ��������

static MAKE_NCB(icb_flagmD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB1_MAIN_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flagmM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB1_MAIN_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flagmP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB1_MAIN_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_flagmR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB1_MAIN_FLAG	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planemD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB1_MAIN_PLANE);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planemM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB1_MAIN_PLANE);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planemP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB1_MAIN_PLANE);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_planemR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);SHOW_NDL(NDL_AGB1_MAIN_PLANE);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballmD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB1_MAIN_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballmM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB1_MAIN_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballmP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB1_MAIN_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ballmR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_AGB1_MAIN_BALL)*0.0826+90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchmD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB1_MAIN_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchmM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB1_MAIN_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchmP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB1_MAIN_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_MCB(icb_pitchmR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);CHK_BRT(); return POS_GET(POS_AGB1_MAIN_PITCH)*4.1;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_agbmain  		,SHOW_BTN(BTN_AGB_MAIN_ARRET1	,2))
static MAKE_ICB(icb_knb_agb_main    ,SHOW_KRM(KRM_AGB_MAIN1         ,20))

// �����
static MAKE_ICB(icb_kppms           ,SHOW_KRM(KRM_KPPMS1			,20))
static MAKE_NCB(icb_scaleD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_SCALE		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_scaleM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_SCALE		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_scaleP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_SCALE		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_scaleR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_SCALE		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_glissD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_ILS_GLIDE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_glissM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_ILS_GLIDE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_glissP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_ILS_GLIDE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_glissR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_ILS_GLIDE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_kursD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_ILS_COURSE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_kursM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_ILS_COURSE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_kursP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_ILS_COURSE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_SCB(icb_ils_kursR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KPPMS1_ILS_COURSE	);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hdgD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS1_SCALE)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hdgM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS1_SCALE)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hdgP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS1_SCALE)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hdgR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS1_SCALE)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_blenker_kurs	,CHK_BRT(); return POS_GET(POS_KPPMS1_BLK_COURSE)?-1:0+POS_GET(POS_PANEL_STATE);)
static MAKE_ICB(icb_blenker_gliss	,CHK_BRT(); return POS_GET(POS_KPPMS1_BLK_GLIDE)?-1:0+POS_GET(POS_PANEL_STATE);)

// ���
static NONLINEARITY tbl_tas[]={
	{{570+109-OFFX,738-OFFY},   0,0},
	{{557+109-OFFX,735-OFFY}, 400,0},
	{{546+109-OFFX,728-OFFY}, 450,0},
	{{538+109-OFFX,717-OFFY}, 500,0},
	{{538+109-OFFX,691-OFFY}, 600,0},
	{{557+109-OFFX,673-OFFY}, 700,0},
	{{583+109-OFFX,673-OFFY}, 800,0},
	{{601+109-OFFX,692-OFFY}, 900,0}
};

static NONLINEARITY tbl_ias[]={
	{{570+109-OFFX,647-OFFY},   0,0},
	{{596+109-OFFX,653-OFFY}, 100,0},
	{{617+109-OFFX,670-OFFY}, 150,0},
	{{627+109-OFFX,695-OFFY}, 200,0},
	{{611+109-OFFX,745-OFFY}, 300,0},
	{{561+109-OFFX,761-OFFY}, 400,0},
	{{526+109-OFFX,742-OFFY}, 500,0},
	{{512+109-OFFX,705-OFFY}, 600,0},
	{{526+109-OFFX,669-OFFY}, 700,0}
};

static MAKE_NCB(icb_tasD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_TAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_tasM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_TAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_tasP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_TAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_tasR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_TAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_iasD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_IAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_iasM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_IAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_iasP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_IAS);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_iasR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_KUS_IAS);}else HIDE_IMAGE(pelement);return -1;)

// �����������
static MAKE_NCB(icb_ark1D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_ARK1,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ark1M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_ARK1,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ark1P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_ARK1,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ark1R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_ARK1,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ark2D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_ARK2,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ark2M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_ARK2,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ark2P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_ARK2,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ark2R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_ARK2,90);}else HIDE_IMAGE(pelement);return -1;)

// Temp duct 
static NONLINEARITY tbl_duct[]={
	{{ 648-OFFX,1005-OFFY}, -50,0},
	{{ 660-OFFX, 987-OFFY},   0,0},
	{{ 681-OFFX, 978-OFFY},  50,0},
	{{ 703-OFFX, 986-OFFY}, 100,0},
	{{ 715-OFFX,1005-OFFY}, 150,0}
};		  

static MAKE_NCB(icb_ductD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_DUCT);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ductM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_DUCT);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ductP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_DUCT);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ductR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_DUCT);}else HIDE_IMAGE(pelement);return -1;)

// ����������� � ������ 
static NONLINEARITY tbl_salon[]={
	{{1016-OFFX,1021-OFFY},  -60,0},
	{{1016-OFFX, 986-OFFY},  -30,0},
	{{1047-OFFX, 967-OFFY},    0,0},
	{{1078-OFFX, 985-OFFY},   30,0},
	{{1079-OFFX,1021-OFFY},   60,0}
};		  

static MAKE_NCB(icb_salonD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_TEMP_SALON);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_salonM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_TEMP_SALON);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_salonP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_TEMP_SALON);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_salonR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_TEMP_SALON);}else HIDE_IMAGE(pelement);return -1;)

// ���� 
static NONLINEARITY tbl_urvk[]={
	{{1047-OFFX, 806-OFFY},   1,0},
	{{1076-OFFX, 826-OFFY},   2,0},
	{{1084-OFFX, 859-OFFY},   3,0},
	{{1073-OFFX, 887-OFFY},   4,0},
	{{1047-OFFX, 904-OFFY},   5,0},
	{{1021-OFFX, 905-OFFY},   6,0},
	{{ 997-OFFX, 891-OFFY},   7,0},
	{{ 986-OFFX, 872-OFFY},   8,0},
	{{ 983-OFFX, 851-OFFY},   9,0},
	{{ 989-OFFX, 830-OFFY},  10,0}
};

static MAKE_NCB(icb_urvkD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_URVK);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_urvkM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_URVK);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_urvkP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_URVK);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_urvkR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_URVK);}else HIDE_IMAGE(pelement);return -1;)

// ���� 
static NONLINEARITY tbl_alt[]={
	{{ 514-OFFX, 898-OFFY},   0,0},
	{{ 491-OFFX, 854-OFFY},1000,0},
	{{ 515-OFFX, 811-OFFY},2000,0},
	{{ 564-OFFX, 810-OFFY},3000,0},
	{{ 590-OFFX, 849-OFFY},4000,0},
	{{ 570-OFFX, 895-OFFY},5000,0}
};

static NONLINEARITY tbl_difpress[]={
	{{ 564-OFFX, 826-OFFY},   8,0},
	{{ 573-OFFX, 865-OFFY},   6,0},
	{{ 543-OFFX, 889-OFFY},   4,0},
	{{ 510-OFFX, 871-OFFY},   2,0},
	{{ 515-OFFX, 829-OFFY},   0,0}
};		  

static MAKE_NCB(icb_altD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_UVPD_ALT			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_altM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_UVPD_ALT			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_altP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_UVPD_ALT			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_altR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_UVPD_ALT			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_difpressD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_UVPD_DIFPRESS		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_difpressM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_UVPD_DIFPRESS		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_difpressP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_UVPD_DIFPRESS		);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_difpressR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_UVPD_DIFPRESS		);}else HIDE_IMAGE(pelement);return -1;)

// ��������� 10 
static NONLINEARITY tbl_var10[]={
	{{ 592-OFFX, 997-OFFY}, -10,0},
	{{ 542-OFFX,1048-OFFY},  -5,0},
	{{ 501-OFFX,1027-OFFY},  -2,0},
	{{ 492-OFFX, 997-OFFY},   0,0},
	{{ 501-OFFX, 967-OFFY},   2,0},
	{{ 542-OFFX, 945-OFFY},   5,0},
	{{ 592-OFFX, 997-OFFY},  10,0}
};		  

static MAKE_NCB(icb_var10D		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return LimitValue(NDL_GET(NDL_VS_SRD),-10.,10.);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_var10M		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return LimitValue(NDL_GET(NDL_VS_SRD),-10.,10.);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_var10P		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return LimitValue(NDL_GET(NDL_VS_SRD),-10.,10.);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_var10R		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return LimitValue(NDL_GET(NDL_VS_SRD),-10.,10.);}else HIDE_IMAGE(pelement);return -1;)

// ��������� 30 
static NONLINEARITY tbl_var30[]={
	{{1082-OFFX,704-OFFY}, -30,0},
	{{1070-OFFX,738-OFFY}, -20,0},
	{{1023-OFFX,755-OFFY}, -10,0},
	{{ 994-OFFX,738-OFFY},  -5,0},
	{{ 983-OFFX,705-OFFY},   0,0},
	{{ 994-OFFX,672-OFFY},   5,0},
	{{1023-OFFX,653-OFFY},  10,0},
	{{1070-OFFX,671-OFFY},  20,0},
	{{1082-OFFX,704-OFFY},  30,0}
};		  

static MAKE_NCB(icb_var30D		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VS				);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_var30M		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VS				);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_var30P		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VS				);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_var30R		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VS				);}else HIDE_IMAGE(pelement);return -1;)

// ��������� 
#define P1_COV_ALTIMSCALEMETER_D	MISC_DUMMY
#define P1_COV_ALTIMSCALEFEET_D		MISC_DUMMY
#define P1_NDL_ALTIMPRESSENG_D		MISC_DUMMY
#define P1_NDL_ALTIMPRESSRUS_D		MISC_DUMMY
#define P1_COV_ALTIMSCALEMETER_P	MISC_DUMMY
#define P1_COV_ALTIMSCALEFEET_P 	MISC_DUMMY
#define P1_NDL_ALTIMPRESSENG_P		MISC_DUMMY
#define P1_NDL_ALTIMPRESSRUS_P		MISC_DUMMY
#define P1_COV_ALTIMSCALEMETER_R	MISC_DUMMY
#define P1_COV_ALTIMSCALEFEET_R 	MISC_DUMMY
#define P1_NDL_ALTIMPRESSENG_R		MISC_DUMMY
#define P1_NDL_ALTIMPRESSRUS_R		MISC_DUMMY
#define P1_COV_ALTIMSCALEMETER_M	MISC_DUMMY
#define P1_COV_ALTIMSCALEFEET_M 	MISC_DUMMY
#define P1_NDL_ALTIMPRESSENG_M		MISC_DUMMY
#define P1_NDL_ALTIMPRESSRUS_M		MISC_DUMMY

static MAKE_NCB(icb_ndl_altim100D		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return (NDL_GET(NDL_UVID1_100)*0.36)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ndl_altim100M		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return (NDL_GET(NDL_UVID1_100)*0.36)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ndl_altim100P		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return (NDL_GET(NDL_UVID1_100)*0.36)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ndl_altim100R		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return (NDL_GET(NDL_UVID1_100)*0.36)-90;}else HIDE_IMAGE(pelement);return -1;)

static FLOAT64 FSAPI icb_ndl_altim1kD	(PELEMENT_NEEDLE pelement)	
{
	if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY) {
		SHOW_IMAGE(pelement);
		CHK_BRT(); 
		return NDL_GET(NDL_UVID1_1000)*0.36-90;
	} else HIDE_IMAGE(pelement);
	return -1;
}

static FLOAT64 FSAPI icb_ndl_altim1kM	(PELEMENT_NEEDLE pelement)	
{
	if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX) {
		SHOW_IMAGE(pelement);
		CHK_BRT(); 
		return NDL_GET(NDL_UVID1_1000)*0.36-90;
	} else HIDE_IMAGE(pelement);
	return -1;
}

static FLOAT64 FSAPI icb_ndl_altim1kP	(PELEMENT_NEEDLE pelement)	
{
	if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF) {
		SHOW_IMAGE(pelement);
		CHK_BRT(); 
		return NDL_GET(NDL_UVID1_1000)*0.36-90;
	} else HIDE_IMAGE(pelement);
	return -1;
}

static FLOAT64 FSAPI icb_ndl_altim1kR	(PELEMENT_NEEDLE pelement)	
{
	if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED) {
		SHOW_IMAGE(pelement);
		CHK_BRT(); 
		return NDL_GET(NDL_UVID1_1000)*0.36-90;
	} else HIDE_IMAGE(pelement);
	return -1;
}

static MAKE_NCB(icb_ndl_altim_D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);return fabs(360-(NormaliseDegreeW((POS_GET(POS_UVID1_MM)+47)*3)));}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ndl_altim_M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);return fabs(360-(NormaliseDegreeW((POS_GET(POS_UVID1_MM)+47)*3)));}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ndl_altim_P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);return fabs(360-(NormaliseDegreeW((POS_GET(POS_UVID1_MM)+47)*3)));}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_ndl_altim_R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);return fabs(360-(NormaliseDegreeW((POS_GET(POS_UVID1_MM)+47)*3)));}else HIDE_IMAGE(pelement);return -1;)

// ��������������
static NONLINEARITY tbl_gen1[]={
	{{ 489-OFFX,1149-OFFY}, -10,0},
	{{ 484-OFFX,1140-OFFY},   0,0},
	{{ 484-OFFX,1120-OFFY},  10,0},
	{{ 499-OFFX,1106-OFFY},  20,0},
	{{ 519-OFFX,1106-OFFY},  30,0},
	{{ 535-OFFX,1120-OFFY},  40,0},
	{{ 535-OFFX,1140-OFFY},  50,0}
};		  

static NONLINEARITY tbl_gen2[]={
	{{ 567-OFFX,1149-OFFY}, -10,0},
	{{ 561-OFFX,1140-OFFY},   0,0},
	{{ 561-OFFX,1120-OFFY},  10,0},
	{{ 576-OFFX,1106-OFFY},  20,0},
	{{ 597-OFFX,1106-OFFY},  30,0},
	{{ 612-OFFX,1120-OFFY},  40,0},
	{{ 612-OFFX,1140-OFFY},  50,0}
};		 

static NONLINEARITY tbl_gen3[]={
	{{ 647-OFFX,1149-OFFY}, -10,0},
	{{ 641-OFFX,1140-OFFY},   0,0},
	{{ 641-OFFX,1120-OFFY},  10,0},
	{{ 656-OFFX,1106-OFFY},  20,0},
	{{ 677-OFFX,1106-OFFY},  30,0},
	{{ 692-OFFX,1120-OFFY},  40,0},
	{{ 692-OFFX,1140-OFFY},  50,0}
};

static NONLINEARITY tbl_amps27[]={
	{{ 726-OFFX,1149-OFFY}, -10,0},
	{{ 719-OFFX,1140-OFFY},   0,0},
	{{ 720-OFFX,1120-OFFY},  10,0},
	{{ 735-OFFX,1106-OFFY},  20,0},
	{{ 755-OFFX,1106-OFFY},  30,0},
	{{ 770-OFFX,1120-OFFY},  40,0},
	{{ 771-OFFX,1140-OFFY},  50,0}
};

static NONLINEARITY tbl_volt27[]={
	{{ 800-OFFX,1144-OFFY},   0,0},
	{{ 806-OFFX,1109-OFFY},  10,0},
	{{ 841-OFFX,1109-OFFY},  20,0},
	{{ 848-OFFX,1144-OFFY},  30,0}
};		  
		  
static NONLINEARITY tbl_volt36[]={
	{{ 867-OFFX,1027-OFFY},   0,0},
	{{ 863-OFFX,1016-OFFY},  15,0},
	{{ 873-OFFX, 994-OFFY},  30,0},
	{{ 914-OFFX,1026-OFFY},  45,0}
};

static NONLINEARITY tbl_volt115[]={
	{{ 941-OFFX,1030-OFFY},   0,0},
	{{ 939-OFFX,1012-OFFY},  60,0},
	{{ 948-OFFX, 995-OFFY},  90,0},
	{{ 975-OFFX, 991-OFFY}, 120,0},
	{{ 989-OFFX,1027-OFFY}, 150,0}
};

static MAKE_NCB(icb_volts115D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS115			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts115M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS115			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts115P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS115			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts115R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS115			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts36D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS36			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts36M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS36			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts36P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS36			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts36R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS36			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts27D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS27			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts27M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS27			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts27P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS27			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_volts27R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_VOLTS27			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps27D		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPS27				);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps27M		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPS27				);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps27P		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPS27				);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps27R		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPS27				);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen3D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN3			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen3M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN3			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen3P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN3			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen3R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN3			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen2D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen2M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen2P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen2R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN2			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen1D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen1M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen1P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN1			);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_amps_gen1R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_AMPSGEN1			);}else HIDE_IMAGE(pelement);return -1;)

static MAKE_ICB(icb_tablo05,SHOW_TBL(TBL_FUEL_LOW_L			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo06,SHOW_TBL(TBL_FUEL_LOW_R			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo07,SHOW_TBL(TBL_ICE_ENG_L			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo08,SHOW_TBL(TBL_ICE_ENG_R			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo09,SHOW_TBL(TBL_BANK3_L_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo10,SHOW_TBL(TBL_BANK3_R_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo11,SHOW_TBL(TBL_OIL_PRESS_LOW		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo12,SHOW_TBL(TBL_ENG1_VIB_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo13,SHOW_TBL(TBL_ENG2_VIB_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo14,SHOW_TBL(TBL_ENG3_VIB_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo15,SHOW_TBL(TBL_INV_36V_FAIL		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo16,SHOW_TBL(TBL_CAB_AIR_PRESS_HIGH	,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo17,SHOW_TBL(TBL_MARKER				,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo18,SHOW_TBL(TBL_EXIT_OPEN			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo19,SHOW_TBL(TBL_ADI_FAIL			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo20,SHOW_TBL(TBL_CAB_ALT_HIGH		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo24,SHOW_TBL(TBL_GEN1_FAIL			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo25,SHOW_TBL(TBL_GEN2_FAIL			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo26,SHOW_TBL(TBL_GEN3_FAIL			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))

static MAKE_ICB(icb_gen1			,SHOW_AZS(AZS_GENERATOR1                    ,2))
static MAKE_ICB(icb_gen2			,SHOW_AZS(AZS_GENERATOR2                    ,2))
static MAKE_ICB(icb_gen3			,SHOW_AZS(AZS_GENERATOR3                    ,2))
static MAKE_ICB(icb_knob_volts115   ,SHOW_GLT(GLT_VOLT115                       ,3))
static MAKE_ICB(icb_knob_volts36    ,SHOW_GLT(GLT_VOLT36                        ,12))
static MAKE_ICB(icb_knob_volts27    ,SHOW_GLT(GLT_VOLT27                        ,7))
static MAKE_ICB(icb_tdknob          ,SHOW_GLT(GLT_TEMP_DUCT                     ,3))
static MAKE_ICB(icb_uvid            ,SHOW_KRM(KRM_UVID1                         ,20))

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_MSCB(mcb_agb_main_arret		,PRS_BTN(BTN_AGB_MAIN_ARRET1			))

static MAKE_MSCB(mcb_p1_gen1			,AZS_TGL(AZS_GENERATOR1					))
static MAKE_MSCB(mcb_p1_gen2			,AZS_TGL(AZS_GENERATOR2					))
static MAKE_MSCB(mcb_p1_gen3			,AZS_TGL(AZS_GENERATOR3					))

static MAKE_MSCB(mcb_p1_temp_duct_ind   ,GLT_SET(GLT_TEMP_DUCT					,0))
static MAKE_MSCB(mcb_p1_temp_duct_salon ,GLT_SET(GLT_TEMP_DUCT					,1))
static MAKE_MSCB(mcb_p1_temp_duct_crew  ,GLT_SET(GLT_TEMP_DUCT					,2))
static MAKE_MSCB(mcb_p1_volt27_left     ,GLT_DEC(GLT_VOLT27						))
static MAKE_MSCB(mcb_p1_volt27_right    ,GLT_INC(GLT_VOLT27						))
static MAKE_MSCB(mcb_p1_volt36_left     ,GLT_DEC(GLT_VOLT36						))
static MAKE_MSCB(mcb_p1_volt36_right    ,GLT_INC(GLT_VOLT36						))
static MAKE_MSCB(mcb_p1_volt115_up      ,GLT_DEC(GLT_VOLT115					))
static MAKE_MSCB(mcb_p1_volt115_dn      ,GLT_INC(GLT_VOLT115					))

static MAKE_MSCB(mcb_agb_main_left		,KRM_DEC(KRM_AGB_MAIN1					))
static MAKE_MSCB(mcb_agb_main_right		,KRM_INC(KRM_AGB_MAIN1					))

static BOOL FSAPI mcb_kppms_kurs_left(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE)
		NDL_DEC(NDL_KPPMS1_SCALE);
	else if(mouse_flags&MOUSE_RIGHTSINGLE)
		NDL_DEC2(NDL_KPPMS1_SCALE,10);
	KRM_DEC(KRM_KPPMS1);
	return TRUE;
}

static BOOL FSAPI mcb_kppms_kurs_right(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE)
		NDL_INC(NDL_KPPMS1_SCALE);
	else if(mouse_flags&MOUSE_RIGHTSINGLE)
		NDL_INC2(NDL_KPPMS1_SCALE,10);
	KRM_INC(KRM_KPPMS1);
	return TRUE;
}

static MAKE_MSCB(mcb_altim_press_left	,KRM_DEC(KRM_UVID1						))
static MAKE_MSCB(mcb_altim_press_right	,KRM_INC(KRM_UVID1						))

BOOL FSAPI mcb_kppms_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	ImportTable.pPanels->panel_window_toggle(IDENT_P122);
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_GENERATOR1					))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_GENERATOR2					))
static MAKE_TCB(tcb_03	,AZS_TT(AZS_GENERATOR3					))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_AGB_MAIN_ARRET1				))
static MAKE_TCB(tcb_05	,GLT_TT(GLT_TEMP_DUCT					))
static MAKE_TCB(tcb_06	,GLT_TT(GLT_VOLT27						))
static MAKE_TCB(tcb_07	,GLT_TT(GLT_VOLT36						))
static MAKE_TCB(tcb_08	,GLT_TT(GLT_VOLT115						))
static MAKE_TCB(tcb_09	,KRM_TT(KRM_AGB_MAIN1					))
static MAKE_TCB(tcb_10	,KRM_TT(KRM_KPPMS1						))
static MAKE_TCB(tcb_11	,KRM_TT(KRM_UVID1						))
static MAKE_TCB(tcb_12	,TBL_TT(TBL_FUEL_LOW_L					))
static MAKE_TCB(tcb_13	,TBL_TT(TBL_FUEL_LOW_R					))
static MAKE_TCB(tcb_14	,TBL_TT(TBL_ICE_ENG_L					))
static MAKE_TCB(tcb_15	,TBL_TT(TBL_ICE_ENG_R					))
static MAKE_TCB(tcb_16	,TBL_TT(TBL_BANK3_L_HIGH				))
static MAKE_TCB(tcb_17	,TBL_TT(TBL_BANK3_R_HIGH				))
static MAKE_TCB(tcb_18	,TBL_TT(TBL_OIL_PRESS_LOW				))
static MAKE_TCB(tcb_19	,TBL_TT(TBL_ENG1_VIB_HIGH				))
static MAKE_TCB(tcb_20	,TBL_TT(TBL_ENG2_VIB_HIGH				))
static MAKE_TCB(tcb_21	,TBL_TT(TBL_ENG3_VIB_HIGH				))
static MAKE_TCB(tcb_22	,TBL_TT(TBL_INV_36V_FAIL				))
static MAKE_TCB(tcb_23	,TBL_TT(TBL_CAB_AIR_PRESS_HIGH			))
static MAKE_TCB(tcb_24	,TBL_TT(TBL_MARKER                  	))
static MAKE_TCB(tcb_25	,TBL_TT(TBL_EXIT_OPEN					))
static MAKE_TCB(tcb_26	,TBL_TT(TBL_ADI_FAIL					))
static MAKE_TCB(tcb_27	,TBL_TT(TBL_CAB_ALT_HIGH				))
static MAKE_TCB(tcb_28	,TBL_TT(TBL_GEN1_FAIL					))
static MAKE_TCB(tcb_29	,TBL_TT(TBL_GEN2_FAIL					))
static MAKE_TCB(tcb_30	,TBL_TT(TBL_GEN3_FAIL					))
static MAKE_TCB(tcb_31  ,NDL_TT(NDL_ARK1						))
static MAKE_TCB(tcb_32  ,NDL_TT(NDL_KUS_TAS						))
static MAKE_TCB(tcb_33  ,NDL_TT(NDL_AGB1_MAIN_PLANE				))
static MAKE_TCB(tcb_34  ,NDL_TT(NDL_VS							))
static MAKE_TCB(tcb_35  ,NDL_TT(NDL_UVPD_ALT					))
static MAKE_TCB(tcb_36  ,NDL_TT(NDL_UVID1_1000					))
static MAKE_TCB(tcb_37  ,NDL_TT(NDL_KPPMS1_SCALE				))
static MAKE_TCB(tcb_38  ,NDL_TT(NDL_URVK						))
static MAKE_TCB(tcb_39  ,NDL_TT(NDL_VS							))
static MAKE_TCB(tcb_40  ,NDL_TT(NDL_DUCT						))
static MAKE_TCB(tcb_41  ,NDL_TT(NDL_VOLTS36						))
static MAKE_TCB(tcb_42  ,NDL_TT(NDL_VOLTS115					))
static MAKE_TCB(tcb_43  ,NDL_TT(NDL_TEMP_SALON					))
static MAKE_TCB(tcb_44  ,NDL_TT(NDL_AMPSGEN1					))
static MAKE_TCB(tcb_45  ,NDL_TT(NDL_AMPSGEN2					))
static MAKE_TCB(tcb_46  ,NDL_TT(NDL_AMPSGEN3					))
static MAKE_TCB(tcb_47  ,NDL_TT(NDL_AMPS27						))
static MAKE_TCB(tcb_48  ,NDL_TT(NDL_VOLTS27						))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MAKE_TTA(tcb_20	)
MAKE_TTA(tcb_21	)
MAKE_TTA(tcb_22	) 
MAKE_TTA(tcb_23	)
MAKE_TTA(tcb_24	)
MAKE_TTA(tcb_25	)
MAKE_TTA(tcb_26	)
MAKE_TTA(tcb_27	) 
MAKE_TTA(tcb_28	) 
MAKE_TTA(tcb_29	)
MAKE_TTA(tcb_30	)
MAKE_TTA(tcb_31	)
MAKE_TTA(tcb_32	) 
MAKE_TTA(tcb_33	)
MAKE_TTA(tcb_34	)
MAKE_TTA(tcb_35	)
MAKE_TTA(tcb_36	)
MAKE_TTA(tcb_37	) 
MAKE_TTA(tcb_38	)
MAKE_TTA(tcb_39	)
MAKE_TTA(tcb_40	)
MAKE_TTA(tcb_41	)
MAKE_TTA(tcb_42	)
MAKE_TTA(tcb_43	)
MAKE_TTA(tcb_44	)
MAKE_TTA(tcb_45	)
MAKE_TTA(tcb_46	)
MAKE_TTA(tcb_47	) 
MAKE_TTA(tcb_48	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p1_4,HELP_NONE,0,0)

// �������� ��������
MOUSE_PBOX(0,0,P1_BACKGROUND4_SX,P1_BACKGROUND4_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_TTPB("31", 473-OFFX, 638-OFFY, 135, 135)    // NDL_ARK1						
MOUSE_TTPB("32", 612-OFFX, 638-OFFY, 135, 135)    // NDL_KUS_TAS					
MOUSE_TTPB("33", 754-OFFX, 596-OFFY, 205, 205)    // NDL_AGB_MAIN_PLANE			
MOUSE_TTPB("34", 968-OFFX, 640-OFFY, 130, 130)    // NDL_VS						
MOUSE_TTPB("35", 473-OFFX, 793-OFFY, 135, 135)    // NDL_UVPD_ALT					
MOUSE_TTPB("36", 612-OFFX, 793-OFFY, 130, 150)    // NDL_UVID						
MOUSE_TBOX("37", 788-OFFX, 820-OFFY, 150, 150,CURSOR_HAND,MOUSE_LR,mcb_kppms_big)    // NDL_KPPMS_SCALE				
MOUSE_TTPB("38", 968-OFFX, 790-OFFY, 130, 130)    // NDL_URVK						
MOUSE_TTPB("39", 473-OFFX, 930-OFFY, 130, 130)    // NDL_VS						
MOUSE_TTPB("40", 636-OFFX, 959-OFFY,  90,  90)    // NDL_DUCT						
MOUSE_TTPB("41", 854-OFFX, 980-OFFY,  70,  70)    // NDL_VOLTS36					
MOUSE_TTPB("42", 930-OFFX, 980-OFFY,  70,  70)    // NDL_VOLTS115					
MOUSE_TTPB("43",1002-OFFX, 956-OFFY,  90,  90)    // NDL_TEMP_SALON				
MOUSE_TTPB("44", 473-OFFX,1096-OFFY,  70,  70)    // NDL_AMPSGEN1					
MOUSE_TTPB("45", 550-OFFX,1096-OFFY,  70,  70)    // NDL_AMPSGEN2					
MOUSE_TTPB("46", 630-OFFX,1096-OFFY,  70,  70)    // NDL_AMPSGEN3					
MOUSE_TTPB("47", 710-OFFX,1096-OFFY,  70,  70)    // NDL_AMPS27					
MOUSE_TTPB("48", 790-OFFX,1096-OFFY,  70,  70)    // NDL_VOLTS27					
MOUSE_TBOX( "1", 730-OFFX, 995-OFFY, 37,100,CURSOR_HAND,MOUSE_LR,mcb_p1_gen1)
MOUSE_TBOX( "2", 767-OFFX, 995-OFFY, 37,100,CURSOR_HAND,MOUSE_LR,mcb_p1_gen2)
MOUSE_TBOX( "3", 804-OFFX, 995-OFFY, 37,100,CURSOR_HAND,MOUSE_LR,mcb_p1_gen3)

// ������
MOUSE_TBOX( "4",  915-OFFX,  626-OFFY, 36, 33,CURSOR_HAND,MOUSE_DLR,mcb_agb_main_arret)

// ���������
MOUSE_TBOX( "5",  740-OFFX,  885-OFFY, 45, 15,CURSOR_HAND,MOUSE_LR,mcb_p1_temp_duct_ind)
MOUSE_TBOX( "5",  773-OFFX,  903-OFFY, 15, 35,CURSOR_HAND,MOUSE_LR,mcb_p1_temp_duct_salon)
MOUSE_TBOX( "5",  740-OFFX,  938-OFFY, 45, 15,CURSOR_HAND,MOUSE_LR,mcb_p1_temp_duct_crew)
MOUSE_TBOX( "6",  873-OFFX, 1099-OFFY, 35, 55,CURSOR_LEFTARROW,MOUSE_LR,mcb_p1_volt27_left)
MOUSE_TBOX( "6",  910-OFFX, 1099-OFFY, 35, 55,CURSOR_RIGHTARROW,MOUSE_LR,mcb_p1_volt27_right)
MOUSE_TBOX( "7",  948-OFFX, 1099-OFFY, 35, 55,CURSOR_LEFTARROW,MOUSE_LR,mcb_p1_volt36_left)
MOUSE_TBOX( "7",  985-OFFX, 1099-OFFY, 35, 55,CURSOR_RIGHTARROW,MOUSE_LR,mcb_p1_volt36_right)
MOUSE_TBOX( "8", 1036-OFFX, 1099-OFFY, 55, 25,CURSOR_UPARROW,MOUSE_LR,mcb_p1_volt115_up)
MOUSE_TBOX( "8", 1036-OFFX, 1131-OFFY, 55, 25,CURSOR_DOWNARROW,MOUSE_LR,mcb_p1_volt115_dn)

// ����������
MOUSE_TSHB( "9",  755-OFFX,  752-OFFY, 40, 30,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_agb_main_left,mcb_agb_main_right) 
MOUSE_TSHB( "10", 904-OFFX,  938-OFFY, 30, 20,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_kppms_kurs_left,mcb_kppms_kurs_right) 
MOUSE_TSHB( "11", 664-OFFX,  920-OFFY, 40, 30,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_altim_press_left,mcb_altim_press_right) 
								
// �����                                  
MOUSE_TTPB( "12",  528-OFFX, 507-OFFY, 57,51)    // TBL_FUEL_LOW_L                  		
MOUSE_TTPB( "13",  583-OFFX, 507-OFFY, 57,51)    // TBL_FUEL_LOW_R                  		
MOUSE_TTPB( "14",  629-OFFX, 507-OFFY, 57,51)    // TBL_ICE_ENG_L                  		
MOUSE_TTPB( "15",  673-OFFX, 507-OFFY, 57,51)    // TBL_ICE_ENG_R                  		
MOUSE_TTPB( "16",  719-OFFX, 507-OFFY, 57,51)    // TBL_BANK3_L_HIGH                 		
MOUSE_TTPB( "17",  764-OFFX, 507-OFFY, 57,51)    // TBL_BANK3_R_HIGH                  		
MOUSE_TTPB( "18",  483-OFFX, 551-OFFY, 57,51)    // TBL_OIL_PRESS_LOW                  		
MOUSE_TTPB( "19",  539-OFFX, 551-OFFY, 57,51)    // TBL_ENG1_VIB_HIGH                  		
MOUSE_TTPB( "20",  583-OFFX, 551-OFFY, 57,51)    // TBL_ENG2_VIB_HIGH                  		
MOUSE_TTPB( "21",  629-OFFX, 551-OFFY, 57,51)    // TBL_ENG3_VIB_HIGH                  		
MOUSE_TTPB( "22",  673-OFFX, 551-OFFY, 57,51)    // TBL_INV_36V_FAIL                  		
MOUSE_TTPB( "23",  719-OFFX, 551-OFFY, 57,51)    // TBL_CAB_AIR_PRESS_HIGH                  		
MOUSE_TTPB( "24",  764-OFFX, 551-OFFY, 57,51)    // TBL_MARKER                  		
MOUSE_TTPB( "25",  809-OFFX, 551-OFFY, 57,51)    // TBL_EXIT_OPEN                  		
MOUSE_TTPB( "26",  859-OFFX, 551-OFFY, 57,51)    // TBL_ADI_FAIL                  		
MOUSE_TTPB( "27", 1032-OFFX, 578-OFFY, 57,51)    // TBL_CAB_ALT_HIGH                  		
MOUSE_TTPB( "28",  589-OFFX,1043-OFFY, 57,51)    // TBL_GEN1_FAIL                  		
MOUSE_TTPB( "29",  646-OFFX,1046-OFFY, 57,51)    // TBL_GEN2_FAIL                  		
MOUSE_TTPB( "30",  692-OFFX,1046-OFFY, 57,51)    // TBL_GEN3_FAIL                  		
								
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p1_4_list; 
GAUGE_HEADER_FS700_EX(P1_BACKGROUND4_SX, "p1_4", &p1_4_list, rect_p1_4, 0, 0, 0, 0, p1_4); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(knob_p1_temp_duct		,P1_KNB_TEMPDUCT_D_00	    ,NULL        				, 731-OFFX, 896-OFFY,	icb_tdknob			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts115		,P1_KNB_VOLTS115_D_00		,&l_knob_p1_temp_duct		,1021-OFFX,1099-OFFY,	icb_knob_volts115	, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts36		,P1_KNB_VOLTS36_D_00		,&l_knob_p1_volts115		, 951-OFFX,1099-OFFY,	icb_knob_volts36 	,12*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts27		,P1_KNB_VOLTS27_D_00		,&l_knob_p1_volts36			, 881-OFFX,1099-OFFY,	icb_knob_volts27 	, 7*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_altim			,P1_KNB_ALTIM_D_00		    ,&l_knob_p1_volts27			, 663-OFFX, 915-OFFY,	icb_uvid			,20*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen3					,P1_AZS_GEN3_D_00			,&l_knob_p1_altim			, 800-OFFX,1033-OFFY,	icb_gen3			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen2					,P1_AZS_GEN2_D_00			,&l_gen3					, 766-OFFX,1033-OFFY,	icb_gen2			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen1					,P1_AZS_GEN1_D_00			,&l_gen2					, 737-OFFX,1033-OFFY,	icb_gen1			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo05				,P1_TBL_05_D_00				,&l_gen1					, 528-OFFX, 507-OFFY,	icb_tablo05			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo06				,P1_TBL_06_D_00				,&l_p1_tablo05				, 583-OFFX, 507-OFFY,	icb_tablo06			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo07				,P1_TBL_07_D_00				,&l_p1_tablo06				, 629-OFFX, 507-OFFY,	icb_tablo07			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo08				,P1_TBL_08_D_00				,&l_p1_tablo07				, 673-OFFX, 507-OFFY,	icb_tablo08			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo09				,P1_TBL_09_D_00				,&l_p1_tablo08				, 719-OFFX, 507-OFFY,	icb_tablo09			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo10				,P1_TBL_10_D_00				,&l_p1_tablo09				, 764-OFFX, 507-OFFY,	icb_tablo10			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo11				,P1_TBL_11_D_00				,&l_p1_tablo10				, 483-OFFX, 551-OFFY,	icb_tablo11			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo12				,P1_TBL_12_D_00				,&l_p1_tablo11				, 539-OFFX, 551-OFFY,	icb_tablo12			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo13				,P1_TBL_13_D_00				,&l_p1_tablo12				, 583-OFFX, 551-OFFY,	icb_tablo13			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo14				,P1_TBL_14_D_00				,&l_p1_tablo13				, 629-OFFX, 551-OFFY,	icb_tablo14			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo15				,P1_TBL_15_D_00				,&l_p1_tablo14				, 673-OFFX, 551-OFFY,	icb_tablo15			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo16				,P1_TBL_16_D_00				,&l_p1_tablo15				, 719-OFFX, 551-OFFY,	icb_tablo16			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo17				,P1_TBL_17_D_00				,&l_p1_tablo16				, 764-OFFX, 551-OFFY,	icb_tablo17			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo18				,P1_TBL_18_D_00				,&l_p1_tablo17				, 809-OFFX, 551-OFFY,	icb_tablo18			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo19				,P1_TBL_19_D_00				,&l_p1_tablo18				, 859-OFFX, 551-OFFY,	icb_tablo19			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo20				,P1_TBL_20_D_00				,&l_p1_tablo19				,1032-OFFX, 578-OFFY,	icb_tablo20			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo24				,P1_TBL_24_D_00				,&l_p1_tablo20				, 589-OFFX,1043-OFFY,	icb_tablo24			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo25				,P1_TBL_25_D_00				,&l_p1_tablo24				, 646-OFFX,1046-OFFY,	icb_tablo25			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo26				,P1_TBL_26_D_00				,&l_p1_tablo25				, 692-OFFX,1046-OFFY,	icb_tablo26			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(btn_p1_agb_aret	    ,P1_BTN_AGBARET_D_00		,&l_p1_tablo26		        , 915-OFFX, 626-OFFY,	icb_agbmain			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_agb_pitch      ,P1_KNB_AGBPITCH_D_00		,&l_btn_p1_agb_aret			, 757-OFFX, 752-OFFY,	icb_knb_agb_main	,20*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_agb_flag	    ,P1_COV_AGBFLAG_D			,&l_knob_p1_agb_pitch		, 772-OFFX, 612-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagD	    ,P1_NDL_AGBFLAG_D			,&l_cov_p1_agb_flag			, 801-OFFX, 668-OFFY,1,9,	icb_flagmD,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagM	    ,P1_NDL_AGBFLAG_M			,&l_ndl_p1_agb_flagD	    , 801-OFFX, 668-OFFY,1,9,	icb_flagmM,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagP	    ,P1_NDL_AGBFLAG_P			,&l_ndl_p1_agb_flagM	    , 801-OFFX, 668-OFFY,1,9,	icb_flagmP,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagR	    ,P1_NDL_AGBFLAG_R			,&l_ndl_p1_agb_flagP	    , 801-OFFX, 668-OFFY,1,9,	icb_flagmR,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeD	    ,P1_NDL_AGBPLANE_D			,&l_ndl_p1_agb_flagR	    , 854-OFFX, 706-OFFY,63,6,	icb_planemD,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeM	    ,P1_NDL_AGBPLANE_M			,&l_ndl_p1_agb_planeD	    , 854-OFFX, 706-OFFY,63,6,	icb_planemM,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeP	    ,P1_NDL_AGBPLANE_P			,&l_ndl_p1_agb_planeM	    , 854-OFFX, 706-OFFY,63,6,	icb_planemP,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeR	    ,P1_NDL_AGBPLANE_R			,&l_ndl_p1_agb_planeP	    , 854-OFFX, 706-OFFY,63,6,	icb_planemR,	0,18							, p1_4) 
MY_ICON2	(cov_p1_agb_ball	    ,P1_COV_AGBBALL_D			,&l_ndl_p1_agb_planeR	    , 846-OFFX, 766-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballD	    ,P1_NDL_AGBBALL_D			,&l_cov_p1_agb_ball			, 854-OFFX, 638-OFFY,7,10,	icb_ballmD,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballM	    ,P1_NDL_AGBBALL_M			,&l_ndl_p1_agb_ballD	    , 854-OFFX, 638-OFFY,7,10,	icb_ballmM,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballP	    ,P1_NDL_AGBBALL_P			,&l_ndl_p1_agb_ballM	    , 854-OFFX, 638-OFFY,7,10,	icb_ballmP,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballR	    ,P1_NDL_AGBBALL_R			,&l_ndl_p1_agb_ballP	    , 854-OFFX, 638-OFFY,7,10,	icb_ballmR,	0,18							, p1_4) 
MY_ICON2	(cov_p1_agb_rail	    ,P1_COV_AGBRAIL_D			,&l_ndl_p1_agb_ballR	    , 847-OFFX, 642-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_STAT2	(alpha_p1_agb1			,P1_ALPHA_AGB				,&l_cov_p1_agb_rail			, 790-OFFX, 640-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA				, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchD	    ,P1_MOV_AGBMAINPITCH_D		,&l_alpha_p1_agb1			, 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmD,	-282,282						, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchM	    ,P1_MOV_AGBMAINPITCH_M		,&l_mov_p1_agb_pitchD	    , 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmM,	-282,282						, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchP	    ,P1_MOV_AGBMAINPITCH_P		,&l_mov_p1_agb_pitchM	    , 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmP,	-282,282						, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchR	    ,P1_MOV_AGBMAINPITCH_R		,&l_mov_p1_agb_pitchP	    , 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmR,	-282,282						, p1_4) 
MY_ICON2	(knob_p1_kppms_scale	,P1_KNB_KPPMSSCALE_D_00		,&l_mov_p1_agb_pitchR	    , 901-OFFX, 932-OFFY,	icb_kppms			,20*PANEL_LIGHT_MAX				, p1_4)
MY_NEEDLE2	(ndl_p1_kppms_hdgD		,P1_NDL_KPPMSHDG_D			,&l_knob_p1_kppms_scale		, 861-OFFX, 892-OFFY,5,9,icb_hdgD,0,10									, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_hdgM		,P1_NDL_KPPMSHDG_M			,&l_ndl_p1_kppms_hdgD		, 861-OFFX, 892-OFFY,5,9,icb_hdgM,0,10									, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_hdgP		,P1_NDL_KPPMSHDG_P			,&l_ndl_p1_kppms_hdgM		, 861-OFFX, 892-OFFY,5,9,icb_hdgP,0,10									, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_hdgR		,P1_NDL_KPPMSHDG_R			,&l_ndl_p1_kppms_hdgP		, 861-OFFX, 892-OFFY,5,9,icb_hdgR,0,10									, p1_4) 
MY_STAT2	(alpha_p1_kppms			,P1_ALPHA_KPPMS				,&l_ndl_p1_kppms_hdgR		, 793-OFFX, 822-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleD	,P1_NDL_KPPMSSCALE_D		,&l_alpha_p1_kppms			, 861-OFFX, 891-OFFY,67,67,icb_scaleD,0,18								, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleM	,P1_NDL_KPPMSSCALE_M		,&l_ndl_p1_kppms_scaleD		, 861-OFFX, 891-OFFY,67,67,icb_scaleM,0,18								, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleP	,P1_NDL_KPPMSSCALE_P		,&l_ndl_p1_kppms_scaleM		, 861-OFFX, 891-OFFY,67,67,icb_scaleP,0,18								, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleR	,P1_NDL_KPPMSSCALE_R		,&l_ndl_p1_kppms_scaleP		, 861-OFFX, 891-OFFY,67,67,icb_scaleR,0,18								, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissD,P1_SLD_KPPMSILSGLIDE_D		,&l_ndl_p1_kppms_scaleR		, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissD,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissM,P1_SLD_KPPMSILSGLIDE_M		,&l_sld_p1_kppms_ils_glissD	, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissM,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissP,P1_SLD_KPPMSILSGLIDE_P		,&l_sld_p1_kppms_ils_glissM	, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissP,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissR,P1_SLD_KPPMSILSGLIDE_R		,&l_sld_p1_kppms_ils_glissP	, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissR,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursD	,P1_SLD_KPPMSILSCOURSE_D	,&l_sld_p1_kppms_ils_glissR	, 857-OFFX, 843-OFFY,	icb_ils_kursD,0.25,NULL,0						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursM	,P1_SLD_KPPMSILSCOURSE_M	,&l_sld_p1_kppms_ils_kursD	, 857-OFFX, 843-OFFY,	icb_ils_kursM,0.25,NULL,0						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursP	,P1_SLD_KPPMSILSCOURSE_P	,&l_sld_p1_kppms_ils_kursM	, 857-OFFX, 843-OFFY,	icb_ils_kursP,0.25,NULL,0						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursR	,P1_SLD_KPPMSILSCOURSE_R	,&l_sld_p1_kppms_ils_kursP	, 857-OFFX, 843-OFFY,	icb_ils_kursR,0.25,NULL,0						, p1_4) 
MY_ICON2	(blk_kppmskr	        ,P1_BLK_KPPMSCOURSE_D		,&l_sld_p1_kppms_ils_kursR	, 839-OFFX, 870-OFFY,	icb_blenker_kurs	, 1*PANEL_LIGHT_MAX				, p1_4) 									
MY_ICON2	(blk_kppmsgl	        ,P1_BLK_KPPMSGLIDESLOPE_D	,&l_blk_kppmskr				, 869-OFFX, 891-OFFY,	icb_blenker_gliss	, 1*PANEL_LIGHT_MAX				, p1_4) 								
MY_ICON2	(cov_p1_kus             ,P1_COV_KUS_D				,&l_blk_kppmsgl				, 673-OFFX, 698-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasD        ,P1_NDL_KUSIAS_D			,&l_cov_p1_kus				, 678-OFFX, 704-OFFY,41,6,icb_iasD,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasM        ,P1_NDL_KUSIAS_M			,&l_ndl_p1_kus_iasD			, 678-OFFX, 704-OFFY,41,6,icb_iasM,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasP        ,P1_NDL_KUSIAS_P			,&l_ndl_p1_kus_iasM			, 678-OFFX, 704-OFFY,41,6,icb_iasP,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasR        ,P1_NDL_KUSIAS_R			,&l_ndl_p1_kus_iasP			, 678-OFFX, 704-OFFY,41,6,icb_iasR,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasD        ,P1_NDL_KUSTAS_D			,&l_ndl_p1_kus_iasR			, 678-OFFX, 704-OFFY,10,5,icb_tasD,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasM        ,P1_NDL_KUSTAS_M			,&l_ndl_p1_kus_tasD			, 678-OFFX, 704-OFFY,10,5,icb_tasM,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasP        ,P1_NDL_KUSTAS_P			,&l_ndl_p1_kus_tasM			, 678-OFFX, 704-OFFY,10,5,icb_tasP,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasR        ,P1_NDL_KUSTAS_R			,&l_ndl_p1_kus_tasP			, 678-OFFX, 704-OFFY,10,5,icb_tasR,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1D	    ,P1_NDL_RKOMPARK1_D			,&l_ndl_p1_kus_tasR			, 541-OFFX, 705-OFFY,35, 7,	icb_ark1D	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1M	    ,P1_NDL_RKOMPARK1_M			,&l_ndl_p1_rcomp_ark1D	    , 541-OFFX, 705-OFFY,35, 7,	icb_ark1M	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1P	    ,P1_NDL_RKOMPARK1_P			,&l_ndl_p1_rcomp_ark1M	    , 541-OFFX, 705-OFFY,35, 7,	icb_ark1P	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1R	    ,P1_NDL_RKOMPARK1_R			,&l_ndl_p1_rcomp_ark1P	    , 541-OFFX, 705-OFFY,35, 7,	icb_ark1R	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2D	    ,P1_NDL_RKOMPARK2_D			,&l_ndl_p1_rcomp_ark1R	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2D	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2M	    ,P1_NDL_RKOMPARK2_M			,&l_ndl_p1_rcomp_ark2D	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2M	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2P	    ,P1_NDL_RKOMPARK2_P			,&l_ndl_p1_rcomp_ark2M	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2P	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2R	    ,P1_NDL_RKOMPARK2_R			,&l_ndl_p1_rcomp_ark2P	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2R	, 0, 18							, p1_4) 
MY_ICON2	(cov_p1_temp_duct	    ,P1_COV_TEMPDUCT_D			,&l_ndl_p1_rcomp_ark2R	    , 632-OFFX, 953-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductD      ,P1_NDL_TEMPDUCT_D			,&l_cov_p1_temp_duct	    , 682-OFFX,1011-OFFY,  4, 5,	icb_ductD		, tbl_duct, 10			, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductM      ,P1_NDL_TEMPDUCT_M			,&l_ndl_p1_temp_ductD		, 682-OFFX,1011-OFFY,  4, 5,	icb_ductM		, tbl_duct, 10			, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductP      ,P1_NDL_TEMPDUCT_P			,&l_ndl_p1_temp_ductM		, 682-OFFX,1011-OFFY,  4, 5,	icb_ductP		, tbl_duct, 10			, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductR      ,P1_NDL_TEMPDUCT_R			,&l_ndl_p1_temp_ductP		, 682-OFFX,1011-OFFY,  4, 5,	icb_ductR		, tbl_duct, 10			, p1_4) 
MY_ICON2	(cov_p1_temp_salon	    ,P1_COV_TEMPSALON_D			,&l_ndl_p1_temp_ductR		,1030-OFFX, 985-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonD	    ,P1_NDL_TEMPSALON_D			,&l_cov_p1_temp_salon	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonD	, tbl_salon, 18				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonM	    ,P1_NDL_TEMPSALON_M			,&l_ndl_p1_temp_salonD	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonM	, tbl_salon, 18				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonP	    ,P1_NDL_TEMPSALON_P			,&l_ndl_p1_temp_salonM	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonP	, tbl_salon, 18				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonR	    ,P1_NDL_TEMPSALON_R			,&l_ndl_p1_temp_salonP	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonR	, tbl_salon, 18				, p1_4) 
MY_ICON2	(cov_p1_urvk	        ,P1_COV_URVK_D				,&l_ndl_p1_temp_salonR	    ,1030-OFFX, 851-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkD	        ,P1_NDL_URVK_D				,&l_cov_p1_urvk				,1036-OFFX, 858-OFFY,29, 7,	icb_urvkD	, tbl_urvk, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkM	        ,P1_NDL_URVK_M				,&l_ndl_p1_urvkD	        ,1036-OFFX, 858-OFFY,29, 7,	icb_urvkM	, tbl_urvk, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkP	        ,P1_NDL_URVK_P				,&l_ndl_p1_urvkM	        ,1036-OFFX, 858-OFFY,29, 7,	icb_urvkP	, tbl_urvk, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkR	        ,P1_NDL_URVK_R				,&l_ndl_p1_urvkP	        ,1036-OFFX, 858-OFFY,29, 7,	icb_urvkR	, tbl_urvk, 18					, p1_4) 
MY_ICON2	(cov_p1_uvpd			,P1_COV_UVPD_D				,&l_ndl_p1_urvkR	        , 533-OFFX, 849-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altD		,P1_NDL_UVPDALT_D		    ,&l_cov_p1_uvpd				, 540-OFFX, 855-OFFY,28, 7,	icb_altD 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altM		,P1_NDL_UVPDALT_M		    ,&l_ndl_p1_uvpd_altD		, 540-OFFX, 855-OFFY,28, 7,	icb_altM 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altP		,P1_NDL_UVPDALT_P		    ,&l_ndl_p1_uvpd_altM		, 540-OFFX, 855-OFFY,28, 7,	icb_altP 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altR		,P1_NDL_UVPDALT_R		    ,&l_ndl_p1_uvpd_altP		, 540-OFFX, 855-OFFY,28, 7,	icb_altR 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressD	,P1_NDL_UVPDDIFPRESS_D		,&l_ndl_p1_uvpd_altR		, 540-OFFX, 855-OFFY,28, 7,	icb_difpressD, tbl_difpress, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressM	,P1_NDL_UVPDDIFPRESS_M		,&l_ndl_p1_uvpd_difpressD	, 540-OFFX, 855-OFFY,28, 7,	icb_difpressM, tbl_difpress, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressP	,P1_NDL_UVPDDIFPRESS_P		,&l_ndl_p1_uvpd_difpressM	, 540-OFFX, 855-OFFY,28, 7,	icb_difpressP, tbl_difpress, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressR	,P1_NDL_UVPDDIFPRESS_R		,&l_ndl_p1_uvpd_difpressP	, 540-OFFX, 855-OFFY,28, 7,	icb_difpressR, tbl_difpress, 10				, p1_4) 
MY_ICON2	(cov_p1_var10	        ,P1_COV_VAR10_D				,&l_ndl_p1_uvpd_difpressR	, 536-OFFX, 992-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_var10D	        ,P1_NDL_VAR10_D				,&l_cov_p1_var10	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10D	, tbl_var10, 10					, p1_4) 
MY_NEEDLE2	(ndl_p1_var10M	        ,P1_NDL_VAR10_M				,&l_ndl_p1_var10D	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10M	, tbl_var10, 10					, p1_4) 
MY_NEEDLE2	(ndl_p1_var10P	        ,P1_NDL_VAR10_P				,&l_ndl_p1_var10M	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10P	, tbl_var10, 10					, p1_4) 
MY_NEEDLE2	(ndl_p1_var10R	        ,P1_NDL_VAR10_R				,&l_ndl_p1_var10P	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10R	, tbl_var10, 10					, p1_4) 
MY_ICON2	(cov_p1_var30	        ,P1_COV_VAR30_D				,&l_ndl_p1_var10R	        ,1028-OFFX, 699-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_var30D	        ,P1_NDL_VAR30_D				,&l_cov_p1_var30	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30D	, tbl_var30, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_var30M	        ,P1_NDL_VAR30_M				,&l_ndl_p1_var30D	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30M	, tbl_var30, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_var30P	        ,P1_NDL_VAR30_P				,&l_ndl_p1_var30M	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30P	, tbl_var30, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_var30R	        ,P1_NDL_VAR30_R				,&l_ndl_p1_var30P	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30R	, tbl_var30, 18					, p1_4) 
MY_ICON2	(cov_p1_altim			,P1_COV_ALTIM_D				,&l_ndl_p1_var30R	        , 673-OFFX, 848-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100D    	,P1_NDL_ALTIM100_D     		,&l_cov_p1_altim			, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100D	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100M    	,P1_NDL_ALTIM100_M     		,&l_ndl_p1_altim_100D    	, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100M	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100P    	,P1_NDL_ALTIM100_P     		,&l_ndl_p1_altim_100M    	, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100P	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100R    	,P1_NDL_ALTIM100_R     		,&l_ndl_p1_altim_100P    	, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100R	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kD    	,P1_NDL_ALTIM1K_D     		,&l_ndl_p1_altim_100R    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kD		, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kM    	,P1_NDL_ALTIM1K_M     		,&l_ndl_p1_altim_1kD    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kM		, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kP    	,P1_NDL_ALTIM1K_P     		,&l_ndl_p1_altim_1kM    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kP		, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kR    	,P1_NDL_ALTIM1K_R     		,&l_ndl_p1_altim_1kP    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kR		, 0, 10				, p1_4) 
//MY_ICON2		(cov_p1_altim_scale		,P1_COV_P1_ALTIM_SCALE_D	,&l_ndl_p1_altim_1kR    	, 608-OFFX, 784-OFFY,icb_Ico  	,PANEL_LIGHT_MAX						, p1_4) 
//MY_NEEDLE2	(ndl_p1_altim_press_D	,P1_NDL_P1_ALTIM_PRESS_D 	,&l_cov_p1_altim_scale		, 680-OFFX, 853-OFFY, 60,59,	icb_ndl_altim_D,	0, 10				, p1_4) 
//MY_NEEDLE2	(ndl_p1_altim_press_M	,P1_NDL_P1_ALTIM_PRESS_M 	,&l_ndl_p1_altim_press_D	, 680-OFFX, 853-OFFY, 60,59,	icb_ndl_altim_M,	0, 10				, p1_4) 
//MY_NEEDLE2	(ndl_p1_altim_press_P	,P1_NDL_P1_ALTIM_PRESS_P 	,&l_ndl_p1_altim_press_M	, 680-OFFX, 853-OFFY, 60,59,	icb_ndl_altim_P,	0, 10				, p1_4) 
//MY_NEEDLE2	(ndl_p1_altim_press_R	,P1_NDL_P1_ALTIM_PRESS_R 	,&l_ndl_p1_altim_press_P	, 680-OFFX, 853-OFFY, 60,59,	icb_ndl_altim_R,	0, 10				, p1_4) 
//MY_ICON2		(cov_p1_altim_black		,P1_COV_P1_ALTIM_BLACK		,&l_ndl_p1_altim_press_R	, 608-OFFX, 784-OFFY,icb_dummy	,1										, p1_4) 
MY_ICON2	(cov_p1_volts115		,P1_COV_VOLTS115_D			,&l_ndl_p1_altim_1kR/*cov_p1_altim_black*/		, 950-OFFX,1002-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_volts36			,P1_COV_VOLTS36_D			,&l_cov_p1_volts115			, 874-OFFX,1002-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_volts27			,P1_COV_VOLTS27_D			,&l_cov_p1_volts36			, 809-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps27			,P1_COV_AMPS27_D			,&l_cov_p1_volts27			, 730-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen3		,P1_COV_AMPSGEN3_D			,&l_cov_p1_amps27			, 651-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen2		,P1_COV_AMPSGEN2_D			,&l_cov_p1_amps_gen3		, 571-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen1		,P1_COV_AMPSGEN1_D			,&l_cov_p1_amps_gen2		, 493-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115D		,P1_NDL_VOLTS115_D			,&l_cov_p1_amps_gen1		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115D	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115M		,P1_NDL_VOLTS115_M			,&l_ndl_p1_volts115D		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115M	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115P		,P1_NDL_VOLTS115_P			,&l_ndl_p1_volts115M		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115P	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115R		,P1_NDL_VOLTS115_R			,&l_ndl_p1_volts115P		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115R	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36D		,P1_NDL_VOLTS36_D			,&l_ndl_p1_volts115R		, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36D		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36M		,P1_NDL_VOLTS36_M			,&l_ndl_p1_volts36D			, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36M		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36P		,P1_NDL_VOLTS36_P			,&l_ndl_p1_volts36M			, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36P		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36R		,P1_NDL_VOLTS36_R			,&l_ndl_p1_volts36P			, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36R		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27D		,P1_NDL_VOLTS27_D			,&l_ndl_p1_volts36R			, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27D		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27M		,P1_NDL_VOLTS27_M			,&l_ndl_p1_volts27D			, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27M		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27P		,P1_NDL_VOLTS27_P			,&l_ndl_p1_volts27M			, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27P		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27R		,P1_NDL_VOLTS27_R			,&l_ndl_p1_volts27P			, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27R		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27D			,P1_NDL_AMPS27_D			,&l_ndl_p1_volts27R			, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27D		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27M			,P1_NDL_AMPS27_M			,&l_ndl_p1_amps27D			, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27M		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27P			,P1_NDL_AMPS27_P			,&l_ndl_p1_amps27M			, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27P		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27R			,P1_NDL_AMPS27_R			,&l_ndl_p1_amps27P			, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27R		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3D		,P1_NDL_AMPSGEN3_D			,&l_ndl_p1_amps27R			, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3D	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3M		,P1_NDL_AMPSGEN3_M			,&l_ndl_p1_amps_gen3D		, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3M	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3P		,P1_NDL_AMPSGEN3_P			,&l_ndl_p1_amps_gen3M		, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3P	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3R		,P1_NDL_AMPSGEN3_R			,&l_ndl_p1_amps_gen3P		, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3R	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2D		,P1_NDL_AMPSGEN2_D			,&l_ndl_p1_amps_gen3R		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2D	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2M		,P1_NDL_AMPSGEN2_M			,&l_ndl_p1_amps_gen2D		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2M	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2P		,P1_NDL_AMPSGEN2_P			,&l_ndl_p1_amps_gen2M		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2P	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2R		,P1_NDL_AMPSGEN2_R			,&l_ndl_p1_amps_gen2P		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2R	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1D		,P1_NDL_AMPSGEN1_D			,&l_ndl_p1_amps_gen2R		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1D	, tbl_gen1		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1M		,P1_NDL_AMPSGEN1_M			,&l_ndl_p1_amps_gen1D		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1M	, tbl_gen1		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1P		,P1_NDL_AMPSGEN1_P			,&l_ndl_p1_amps_gen1M		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1P	, tbl_gen1		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1R		,P1_NDL_AMPSGEN1_R			,&l_ndl_p1_amps_gen1P		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1R	, tbl_gen1		, 18	, p1_4) 
MY_ICON2	(p1_4Ico				,P1_BACKGROUND4_D			,&l_ndl_p1_amps_gen1R		,	     0,		   0,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_STATIC2	(p1_4bg,p1_4_list		,P1_BACKGROUND4				,&l_p1_4Ico					, p1_4);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(knob_p1_temp_duct		,P1_KNB_TEMPDUCT_D_00	    ,NULL        				, 731-OFFX, 896-OFFY,	icb_tdknob			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts115		,P1_KNB_VOLTS115_D_00		,&l_knob_p1_temp_duct		,1021-OFFX,1099-OFFY,	icb_knob_volts115	, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts36		,P1_KNB_VOLTS36_D_00		,&l_knob_p1_volts115		, 951-OFFX,1099-OFFY,	icb_knob_volts36 	,12*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts27		,P1_KNB_VOLTS27_D_00		,&l_knob_p1_volts36			, 881-OFFX,1099-OFFY,	icb_knob_volts27 	, 7*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_altim			,P1_KNB_ALTIM_D_00		    ,&l_knob_p1_volts27			, 663-OFFX, 915-OFFY,	icb_uvid			,20*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen3					,P1_AZS_GEN3_D_00			,&l_knob_p1_altim			, 800-OFFX,1033-OFFY,	icb_gen3			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen2					,P1_AZS_GEN2_D_00			,&l_gen3					, 766-OFFX,1033-OFFY,	icb_gen2			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen1					,P1_AZS_GEN1_D_00			,&l_gen2					, 737-OFFX,1033-OFFY,	icb_gen1			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo05				,P1_TBL_05_D_00				,&l_gen1					, 528-OFFX, 507-OFFY,	icb_tablo05			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo06				,P1_TBL_06_D_00				,&l_p1_tablo05				, 583-OFFX, 507-OFFY,	icb_tablo06			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo07				,P1_TBL_07_D_00				,&l_p1_tablo06				, 629-OFFX, 507-OFFY,	icb_tablo07			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo08				,P1_TBL_08_D_00				,&l_p1_tablo07				, 673-OFFX, 507-OFFY,	icb_tablo08			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo09				,P1_TBL_09_D_00				,&l_p1_tablo08				, 719-OFFX, 507-OFFY,	icb_tablo09			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo10				,P1_TBL_10_D_00				,&l_p1_tablo09				, 764-OFFX, 507-OFFY,	icb_tablo10			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo11				,P1_TBL_11_D_00				,&l_p1_tablo10				, 483-OFFX, 551-OFFY,	icb_tablo11			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo12				,P1_TBL_12_D_00				,&l_p1_tablo11				, 539-OFFX, 551-OFFY,	icb_tablo12			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo13				,P1_TBL_13_D_00				,&l_p1_tablo12				, 583-OFFX, 551-OFFY,	icb_tablo13			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo14				,P1_TBL_14_D_00				,&l_p1_tablo13				, 629-OFFX, 551-OFFY,	icb_tablo14			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo15				,P1_TBL_15_D_00				,&l_p1_tablo14				, 673-OFFX, 551-OFFY,	icb_tablo15			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo16				,P1_TBL_16_D_00				,&l_p1_tablo15				, 719-OFFX, 551-OFFY,	icb_tablo16			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo17				,P1_TBL_17_D_00				,&l_p1_tablo16				, 764-OFFX, 551-OFFY,	icb_tablo17			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo18				,P1_TBL_18_D_00				,&l_p1_tablo17				, 809-OFFX, 551-OFFY,	icb_tablo18			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo19				,P1_TBL_19_D_00				,&l_p1_tablo18				, 859-OFFX, 551-OFFY,	icb_tablo19			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo20				,P1_TBL_20_D_00				,&l_p1_tablo19				,1032-OFFX, 578-OFFY,	icb_tablo20			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo24				,P1_TBL_24_D_00				,&l_p1_tablo20				, 589-OFFX,1043-OFFY,	icb_tablo24			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo25				,P1_TBL_25_D_00				,&l_p1_tablo24				, 646-OFFX,1046-OFFY,	icb_tablo25			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo26				,P1_TBL_26_D_00				,&l_p1_tablo25				, 692-OFFX,1046-OFFY,	icb_tablo26			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(btn_p1_agb_aret	    ,P1_BTN_AGBARET_D_00		,&l_p1_tablo26				, 915-OFFX, 626-OFFY,	icb_agbmain			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_agb_pitch      ,P1_KNB_AGBPITCH_D_00		,&l_btn_p1_agb_aret			, 757-OFFX, 752-OFFY,	icb_knb_agb_main	,20*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_agb_flag	    ,P1_COV_AGBFLAG_D			,&l_knob_p1_agb_pitch		, 772-OFFX, 612-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagD	    ,P1_NDL_AGBFLAG_D			,&l_cov_p1_agb_flag			, 801-OFFX, 668-OFFY,1,9,	icb_flagmD,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagP	    ,P1_NDL_AGBFLAG_P			,&l_ndl_p1_agb_flagD	    , 801-OFFX, 668-OFFY,1,9,	icb_flagmP,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeD	    ,P1_NDL_AGBPLANE_D			,&l_ndl_p1_agb_flagP	    , 854-OFFX, 706-OFFY,63,6,	icb_planemD,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeP	    ,P1_NDL_AGBPLANE_P			,&l_ndl_p1_agb_planeD	    , 854-OFFX, 706-OFFY,63,6,	icb_planemP,	0,18							, p1_4) 
MY_ICON2	(cov_p1_agb_ball	    ,P1_COV_AGBBALL_D			,&l_ndl_p1_agb_planeP	    , 846-OFFX, 766-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballD	    ,P1_NDL_AGBBALL_D			,&l_cov_p1_agb_ball			, 854-OFFX, 638-OFFY,7,10,	icb_ballmD,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballP	    ,P1_NDL_AGBBALL_P			,&l_ndl_p1_agb_ballD	    , 854-OFFX, 638-OFFY,7,10,	icb_ballmP,	0,18							, p1_4) 
MY_ICON2	(cov_p1_agb_rail	    ,P1_COV_AGBRAIL_D			,&l_ndl_p1_agb_ballP	    , 847-OFFX, 642-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_STAT2	(alpha_p1_agb1			,P1_ALPHA_AGB				,&l_cov_p1_agb_rail			, 790-OFFX, 640-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA				, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchD	    ,P1_MOV_AGBMAINPITCH_D		,&l_alpha_p1_agb1			, 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmD,	-282,282						, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchP	    ,P1_MOV_AGBMAINPITCH_P		,&l_mov_p1_agb_pitchD	    , 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmP,	-282,282						, p1_4) 
MY_ICON2	(knob_p1_kppms_scale	,P1_KNB_KPPMSSCALE_D_00		,&l_mov_p1_agb_pitchP	    , 901-OFFX, 932-OFFY,	icb_kppms			,20*PANEL_LIGHT_MAX				, p1_4)
MY_NEEDLE2	(ndl_p1_kppms_hdgD		,P1_NDL_KPPMSHDG_D			,&l_knob_p1_kppms_scale		, 861-OFFX, 892-OFFY,5,9,icb_hdgD,0,10									, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_hdgP		,P1_NDL_KPPMSHDG_P			,&l_ndl_p1_kppms_hdgD		, 861-OFFX, 892-OFFY,5,9,icb_hdgP,0,10									, p1_4) 
MY_STAT2	(alpha_p1_kppms			,P1_ALPHA_KPPMS				,&l_ndl_p1_kppms_hdgP		, 793-OFFX, 822-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleD	,P1_NDL_KPPMSSCALE_D		,&l_alpha_p1_kppms			, 861-OFFX, 891-OFFY,67,67,icb_scaleD,0,18								, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleP	,P1_NDL_KPPMSSCALE_P		,&l_ndl_p1_kppms_scaleD		, 861-OFFX, 891-OFFY,67,67,icb_scaleP,0,18								, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissD,P1_SLD_KPPMSILSGLIDE_D		,&l_ndl_p1_kppms_scaleP		, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissD,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissP,P1_SLD_KPPMSILSGLIDE_P		,&l_sld_p1_kppms_ils_glissD	, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissP,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursD	,P1_SLD_KPPMSILSCOURSE_D	,&l_sld_p1_kppms_ils_glissP	, 857-OFFX, 843-OFFY,	icb_ils_kursD,0.25,NULL,0						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursP	,P1_SLD_KPPMSILSCOURSE_P	,&l_sld_p1_kppms_ils_kursD	, 857-OFFX, 843-OFFY,	icb_ils_kursP,0.25,NULL,0						, p1_4) 
MY_ICON2	(blk_kppmskr	        ,P1_BLK_KPPMSCOURSE_D		,&l_sld_p1_kppms_ils_kursP	, 839-OFFX, 870-OFFY,	icb_blenker_kurs	, 1*PANEL_LIGHT_MAX				, p1_4) 									
MY_ICON2	(blk_kppmsgl	        ,P1_BLK_KPPMSGLIDESLOPE_D	,&l_blk_kppmskr				, 869-OFFX, 891-OFFY,	icb_blenker_gliss	, 1*PANEL_LIGHT_MAX				, p1_4) 								
MY_ICON2	(cov_p1_kus             ,P1_COV_KUS_D				,&l_blk_kppmsgl				, 673-OFFX, 698-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasD        ,P1_NDL_KUSIAS_D			,&l_cov_p1_kus				, 678-OFFX, 704-OFFY,41,6,icb_iasD,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasP        ,P1_NDL_KUSIAS_P			,&l_ndl_p1_kus_iasD			, 678-OFFX, 704-OFFY,41,6,icb_iasP,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasD        ,P1_NDL_KUSTAS_D			,&l_ndl_p1_kus_iasP			, 678-OFFX, 704-OFFY,10,5,icb_tasD,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasP        ,P1_NDL_KUSTAS_P			,&l_ndl_p1_kus_tasD			, 678-OFFX, 704-OFFY,10,5,icb_tasP,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1D	    ,P1_NDL_RKOMPARK1_D			,&l_ndl_p1_kus_tasP			, 541-OFFX, 705-OFFY,35, 7,	icb_ark1D	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1P	    ,P1_NDL_RKOMPARK1_P			,&l_ndl_p1_rcomp_ark1D	    , 541-OFFX, 705-OFFY,35, 7,	icb_ark1P	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2D	    ,P1_NDL_RKOMPARK2_D			,&l_ndl_p1_rcomp_ark1P	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2D	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2P	    ,P1_NDL_RKOMPARK2_P			,&l_ndl_p1_rcomp_ark2D	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2P	, 0, 18							, p1_4) 
MY_ICON2	(cov_p1_temp_duct	    ,P1_COV_TEMPDUCT_D			,&l_ndl_p1_rcomp_ark2P	    , 632-OFFX, 953-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductD      ,P1_NDL_TEMPDUCT_D			,&l_cov_p1_temp_duct	    , 682-OFFX,1011-OFFY,  4, 5,	icb_ductD		, tbl_duct, 10			, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductP      ,P1_NDL_TEMPDUCT_P			,&l_ndl_p1_temp_ductD		, 682-OFFX,1011-OFFY,  4, 5,	icb_ductP		, tbl_duct, 10			, p1_4) 
MY_ICON2	(cov_p1_temp_salon	    ,P1_COV_TEMPSALON_D			,&l_ndl_p1_temp_ductP		,1030-OFFX, 985-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonD	    ,P1_NDL_TEMPSALON_D			,&l_cov_p1_temp_salon	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonD	, tbl_salon, 18				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonP	    ,P1_NDL_TEMPSALON_P			,&l_ndl_p1_temp_salonD	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonP	, tbl_salon, 18				, p1_4) 
MY_ICON2	(cov_p1_urvk	        ,P1_COV_URVK_D				,&l_ndl_p1_temp_salonP	    ,1030-OFFX, 851-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkD	        ,P1_NDL_URVK_D				,&l_cov_p1_urvk				,1036-OFFX, 858-OFFY,29, 7,	icb_urvkD	, tbl_urvk, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkP	        ,P1_NDL_URVK_P				,&l_ndl_p1_urvkD	        ,1036-OFFX, 858-OFFY,29, 7,	icb_urvkP	, tbl_urvk, 18					, p1_4) 
MY_ICON2	(cov_p1_uvpd			,P1_COV_UVPD_D				,&l_ndl_p1_urvkP	        , 533-OFFX, 849-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altD		,P1_NDL_UVPDALT_D		    ,&l_cov_p1_uvpd				, 540-OFFX, 855-OFFY,28, 7,	icb_altD 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altP		,P1_NDL_UVPDALT_P		    ,&l_ndl_p1_uvpd_altD		, 540-OFFX, 855-OFFY,28, 7,	icb_altP 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressD	,P1_NDL_UVPDDIFPRESS_D		,&l_ndl_p1_uvpd_altP		, 540-OFFX, 855-OFFY,28, 7,	icb_difpressD, tbl_difpress, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressP	,P1_NDL_UVPDDIFPRESS_P		,&l_ndl_p1_uvpd_difpressD	, 540-OFFX, 855-OFFY,28, 7,	icb_difpressP, tbl_difpress, 10				, p1_4) 
MY_ICON2	(cov_p1_var10	        ,P1_COV_VAR10_D				,&l_ndl_p1_uvpd_difpressP	, 536-OFFX, 992-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_var10D	        ,P1_NDL_VAR10_D				,&l_cov_p1_var10	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10D	, tbl_var10, 10					, p1_4) 
MY_NEEDLE2	(ndl_p1_var10P	        ,P1_NDL_VAR10_P				,&l_ndl_p1_var10D	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10P	, tbl_var10, 10					, p1_4) 
MY_ICON2	(cov_p1_var30	        ,P1_COV_VAR30_D				,&l_ndl_p1_var10P	        ,1028-OFFX, 699-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_var30D	        ,P1_NDL_VAR30_D				,&l_cov_p1_var30	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30D	, tbl_var30, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_var30P	        ,P1_NDL_VAR30_P				,&l_ndl_p1_var30D	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30P	, tbl_var30, 18					, p1_4) 
MY_ICON2	(cov_p1_altim			,P1_COV_ALTIM_D				,&l_ndl_p1_var30P	        , 673-OFFX, 848-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100D    	,P1_NDL_ALTIM100_D     		,&l_cov_p1_altim			, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100D	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100P    	,P1_NDL_ALTIM100_P     		,&l_ndl_p1_altim_100D    	, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100P	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kD    	,P1_NDL_ALTIM1K_D     		,&l_ndl_p1_altim_100P    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kD		, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kP    	,P1_NDL_ALTIM1K_P     		,&l_ndl_p1_altim_1kD    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kP		, 0, 10				, p1_4) 
/*
MY_ICON2	(cov_p1_altim_scale_met ,P1_COV_ALTIMSCALEMETER_D	,&l_ndl_p1_altim_1kP    	,   0-OFFX,   0-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_altim_scale_feet,P1_COV_ALTIMSCALEFEET_D	,&l_cov_p1_altim_scale_met	,   0-OFFX,   0-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_engD,P1_NDL_ALTIMPRESSENG_D 	,&l_cov_p1_altim_scale_feet	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_engD,	0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_engP,P1_NDL_ALTIMPRESSENG_P 	,&l_ndl_p1_altim_press_engD	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_engP,	0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_rusD,P1_NDL_ALTIMPRESSRUS_D		,&l_ndl_p1_altim_press_engP	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_rusD,	0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_rusP,P1_NDL_ALTIMPRESSRUS_P		,&l_ndl_p1_altim_press_rusD	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_rusP,	0, 10				, p1_4) 
*/
MY_ICON2	(cov_p1_volts115		,P1_COV_VOLTS115_D			,&l_ndl_p1_altim_1kP    	, 950-OFFX,1002-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_volts36			,P1_COV_VOLTS36_D			,&l_cov_p1_volts115			, 874-OFFX,1002-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_volts27			,P1_COV_VOLTS27_D			,&l_cov_p1_volts36			, 809-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps27			,P1_COV_AMPS27_D			,&l_cov_p1_volts27			, 730-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen3		,P1_COV_AMPSGEN3_D			,&l_cov_p1_amps27			, 651-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen2		,P1_COV_AMPSGEN2_D			,&l_cov_p1_amps_gen3		, 571-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen1		,P1_COV_AMPSGEN1_D			,&l_cov_p1_amps_gen2		, 493-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115D		,P1_NDL_VOLTS115_D			,&l_cov_p1_amps_gen1		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115D	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115P		,P1_NDL_VOLTS115_P			,&l_ndl_p1_volts115D		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115P	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36D		,P1_NDL_VOLTS36_D			,&l_ndl_p1_volts115P		, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36D		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36P		,P1_NDL_VOLTS36_P			,&l_ndl_p1_volts36D			, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36P		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27D		,P1_NDL_VOLTS27_D			,&l_ndl_p1_volts36P			, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27D		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27P		,P1_NDL_VOLTS27_P			,&l_ndl_p1_volts27D			, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27P		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27D			,P1_NDL_AMPS27_D			,&l_ndl_p1_volts27P			, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27D		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27P			,P1_NDL_AMPS27_P			,&l_ndl_p1_amps27D			, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27P		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3D		,P1_NDL_AMPSGEN3_D			,&l_ndl_p1_amps27P			, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3D	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3P		,P1_NDL_AMPSGEN3_P			,&l_ndl_p1_amps_gen3D		, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3P	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2D		,P1_NDL_AMPSGEN2_D			,&l_ndl_p1_amps_gen3P		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2D	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2P		,P1_NDL_AMPSGEN2_P			,&l_ndl_p1_amps_gen2D		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2P	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1D		,P1_NDL_AMPSGEN1_D			,&l_ndl_p1_amps_gen2P		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1D	, tbl_gen1		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1P		,P1_NDL_AMPSGEN1_P			,&l_ndl_p1_amps_gen1D		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1P	, tbl_gen1		, 18	, p1_4) 
MY_ICON2	(p1_4Ico				,P1_BACKGROUND4_D			,&l_ndl_p1_amps_gen1P		,	     0,		   0,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_STATIC2	(p1_4bg,p1_4_list		,P1_BACKGROUND4				,&l_p1_4Ico					, p1_4);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(knob_p1_temp_duct		,P1_KNB_TEMPDUCT_D_00	    ,NULL        				, 731-OFFX, 896-OFFY,	icb_tdknob			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts115		,P1_KNB_VOLTS115_D_00		,&l_knob_p1_temp_duct		,1021-OFFX,1099-OFFY,	icb_knob_volts115	, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts36		,P1_KNB_VOLTS36_D_00		,&l_knob_p1_volts115		, 951-OFFX,1099-OFFY,	icb_knob_volts36 	,12*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts27		,P1_KNB_VOLTS27_D_00		,&l_knob_p1_volts36		, 881-OFFX,1099-OFFY,	icb_knob_volts27 	, 7*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_altim			,P1_KNB_ALTIM_D_00		    ,&l_knob_p1_volts27		, 663-OFFX, 915-OFFY,	icb_uvid			,20*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen3					,P1_AZS_GEN3_D_00			,&l_knob_p1_altim			, 800-OFFX,1033-OFFY,	icb_gen3			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen2					,P1_AZS_GEN2_D_00			,&l_gen3					, 766-OFFX,1033-OFFY,	icb_gen2			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen1					,P1_AZS_GEN1_D_00			,&l_gen2					, 737-OFFX,1033-OFFY,	icb_gen1			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo05				,P1_TBL_05_D_00				,&l_gen1					, 528-OFFX, 507-OFFY,	icb_tablo05			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo06				,P1_TBL_06_D_00				,&l_p1_tablo05				, 583-OFFX, 507-OFFY,	icb_tablo06			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo07				,P1_TBL_07_D_00				,&l_p1_tablo06				, 629-OFFX, 507-OFFY,	icb_tablo07			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo08				,P1_TBL_08_D_00				,&l_p1_tablo07				, 673-OFFX, 507-OFFY,	icb_tablo08			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo09				,P1_TBL_09_D_00				,&l_p1_tablo08				, 719-OFFX, 507-OFFY,	icb_tablo09			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo10				,P1_TBL_10_D_00				,&l_p1_tablo09				, 764-OFFX, 507-OFFY,	icb_tablo10			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo11				,P1_TBL_11_D_00				,&l_p1_tablo10				, 483-OFFX, 551-OFFY,	icb_tablo11			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo12				,P1_TBL_12_D_00				,&l_p1_tablo11				, 539-OFFX, 551-OFFY,	icb_tablo12			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo13				,P1_TBL_13_D_00				,&l_p1_tablo12				, 583-OFFX, 551-OFFY,	icb_tablo13			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo14				,P1_TBL_14_D_00				,&l_p1_tablo13				, 629-OFFX, 551-OFFY,	icb_tablo14			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo15				,P1_TBL_15_D_00				,&l_p1_tablo14				, 673-OFFX, 551-OFFY,	icb_tablo15			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo16				,P1_TBL_16_D_00				,&l_p1_tablo15				, 719-OFFX, 551-OFFY,	icb_tablo16			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo17				,P1_TBL_17_D_00				,&l_p1_tablo16				, 764-OFFX, 551-OFFY,	icb_tablo17			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo18				,P1_TBL_18_D_00				,&l_p1_tablo17				, 809-OFFX, 551-OFFY,	icb_tablo18			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo19				,P1_TBL_19_D_00				,&l_p1_tablo18				, 859-OFFX, 551-OFFY,	icb_tablo19			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo20				,P1_TBL_20_D_00				,&l_p1_tablo19				,1032-OFFX, 578-OFFY,	icb_tablo20			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo24				,P1_TBL_24_D_00				,&l_p1_tablo20				, 589-OFFX,1043-OFFY,	icb_tablo24			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo25				,P1_TBL_25_D_00				,&l_p1_tablo24				, 646-OFFX,1046-OFFY,	icb_tablo25			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo26				,P1_TBL_26_D_00				,&l_p1_tablo25				, 692-OFFX,1046-OFFY,	icb_tablo26			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(btn_p1_agb_aret	    ,P1_BTN_AGBARET_D_00		,&l_p1_tablo26				, 915-OFFX, 626-OFFY,	icb_agbmain			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_agb_pitch      ,P1_KNB_AGBPITCH_D_00		,&l_btn_p1_agb_aret	    , 757-OFFX, 752-OFFY,	icb_knb_agb_main	,20*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_agb_flag	    ,P1_COV_AGBFLAG_D			,&l_knob_p1_agb_pitch      , 772-OFFX, 612-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagD	    ,P1_NDL_AGBFLAG_D			,&l_cov_p1_agb_flag	    , 801-OFFX, 668-OFFY,1,9,	icb_flagmD,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagR	    ,P1_NDL_AGBFLAG_R			,&l_ndl_p1_agb_flagD	    , 801-OFFX, 668-OFFY,1,9,	icb_flagmR,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeD	    ,P1_NDL_AGBPLANE_D			,&l_ndl_p1_agb_flagR	    , 854-OFFX, 706-OFFY,63,6,	icb_planemD,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeR	    ,P1_NDL_AGBPLANE_R			,&l_ndl_p1_agb_planeD	    , 854-OFFX, 706-OFFY,63,6,	icb_planemR,	0,18							, p1_4) 
MY_ICON2	(cov_p1_agb_ball	    ,P1_COV_AGBBALL_D			,&l_ndl_p1_agb_planeR	    , 846-OFFX, 766-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballD	    ,P1_NDL_AGBBALL_D			,&l_cov_p1_agb_ball	    , 854-OFFX, 638-OFFY,7,10,	icb_ballmD,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballR	    ,P1_NDL_AGBBALL_R			,&l_ndl_p1_agb_ballD	    , 854-OFFX, 638-OFFY,7,10,	icb_ballmR,	0,18							, p1_4) 
MY_ICON2	(cov_p1_agb_rail	    ,P1_COV_AGBRAIL_D			,&l_ndl_p1_agb_ballR	    , 847-OFFX, 642-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_STAT2	(alpha_p1_agb1			,P1_ALPHA_AGB				,&l_cov_p1_agb_rail	    , 790-OFFX, 640-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA				, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchD	    ,P1_MOV_AGBMAINPITCH_D		,&l_alpha_p1_agb1			, 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmD,	-282,282						, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchR	    ,P1_MOV_AGBMAINPITCH_R		,&l_mov_p1_agb_pitchD	    , 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmR,	-282,282						, p1_4) 
MY_ICON2	(knob_p1_kppms_scale	,P1_KNB_KPPMSSCALE_D_00		,&l_mov_p1_agb_pitchR	    , 901-OFFX, 932-OFFY,	icb_kppms			,20*PANEL_LIGHT_MAX				, p1_4)
MY_NEEDLE2	(ndl_p1_kppms_hdgD		,P1_NDL_KPPMSHDG_D			,&l_knob_p1_kppms_scale	, 861-OFFX, 892-OFFY,5,9,icb_hdgD,0,10									, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_hdgR		,P1_NDL_KPPMSHDG_R			,&l_ndl_p1_kppms_hdgD		, 861-OFFX, 892-OFFY,5,9,icb_hdgR,0,10									, p1_4) 
MY_STAT2	(alpha_p1_kppms			,P1_ALPHA_KPPMS				,&l_ndl_p1_kppms_hdgR		, 793-OFFX, 822-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleD	,P1_NDL_KPPMSSCALE_D		,&l_alpha_p1_kppms			, 861-OFFX, 891-OFFY,67,67,icb_scaleD,0,18								, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleR	,P1_NDL_KPPMSSCALE_R		,&l_ndl_p1_kppms_scaleD	, 861-OFFX, 891-OFFY,67,67,icb_scaleR,0,18								, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissD,P1_SLD_KPPMSILSGLIDE_D		,&l_ndl_p1_kppms_scaleR	, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissD,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissR,P1_SLD_KPPMSILSGLIDE_R		,&l_sld_p1_kppms_ils_glissD, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissR,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursD	,P1_SLD_KPPMSILSCOURSE_D	,&l_sld_p1_kppms_ils_glissR, 857-OFFX, 843-OFFY,	icb_ils_kursD,0.25,NULL,0						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursR	,P1_SLD_KPPMSILSCOURSE_R	,&l_sld_p1_kppms_ils_kursD	, 857-OFFX, 843-OFFY,	icb_ils_kursR,0.25,NULL,0						, p1_4) 
MY_ICON2	(blk_kppmskr	        ,P1_BLK_KPPMSCOURSE_D		,&l_sld_p1_kppms_ils_kursR	, 839-OFFX, 870-OFFY,	icb_blenker_kurs	, 1*PANEL_LIGHT_MAX				, p1_4) 									
MY_ICON2	(blk_kppmsgl	        ,P1_BLK_KPPMSGLIDESLOPE_D	,&l_blk_kppmskr	        , 869-OFFX, 891-OFFY,	icb_blenker_gliss	, 1*PANEL_LIGHT_MAX				, p1_4) 								
MY_ICON2	(cov_p1_kus             ,P1_COV_KUS_D				,&l_blk_kppmsgl	        , 673-OFFX, 698-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasD        ,P1_NDL_KUSIAS_D			,&l_cov_p1_kus             , 678-OFFX, 704-OFFY,41,6,icb_iasD,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasR        ,P1_NDL_KUSIAS_R			,&l_ndl_p1_kus_iasD        , 678-OFFX, 704-OFFY,41,6,icb_iasR,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasD        ,P1_NDL_KUSTAS_D			,&l_ndl_p1_kus_iasR        , 678-OFFX, 704-OFFY,10,5,icb_tasD,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasR        ,P1_NDL_KUSTAS_R			,&l_ndl_p1_kus_tasD        , 678-OFFX, 704-OFFY,10,5,icb_tasR,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1D	    ,P1_NDL_RKOMPARK1_D			,&l_ndl_p1_kus_tasR        , 541-OFFX, 705-OFFY,35, 7,	icb_ark1D	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1R	    ,P1_NDL_RKOMPARK1_R			,&l_ndl_p1_rcomp_ark1D	    , 541-OFFX, 705-OFFY,35, 7,	icb_ark1R	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2D	    ,P1_NDL_RKOMPARK2_D			,&l_ndl_p1_rcomp_ark1R	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2D	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2R	    ,P1_NDL_RKOMPARK2_R			,&l_ndl_p1_rcomp_ark2D	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2R	, 0, 18							, p1_4) 
MY_ICON2	(cov_p1_temp_duct	    ,P1_COV_TEMPDUCT_D			,&l_ndl_p1_rcomp_ark2R	    , 632-OFFX, 953-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductD      ,P1_NDL_TEMPDUCT_D			,&l_cov_p1_temp_duct	    , 682-OFFX,1011-OFFY,  4, 5,	icb_ductD		, tbl_duct, 10			, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductR      ,P1_NDL_TEMPDUCT_R			,&l_ndl_p1_temp_ductD      , 682-OFFX,1011-OFFY,  4, 5,	icb_ductR		, tbl_duct, 10			, p1_4) 
MY_ICON2	(cov_p1_temp_salon	    ,P1_COV_TEMPSALON_D			,&l_ndl_p1_temp_ductR      ,1030-OFFX, 985-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonD	    ,P1_NDL_TEMPSALON_D			,&l_cov_p1_temp_salon	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonD	, tbl_salon, 18				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonR	    ,P1_NDL_TEMPSALON_R			,&l_ndl_p1_temp_salonD	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonR	, tbl_salon, 18				, p1_4) 
MY_ICON2	(cov_p1_urvk	        ,P1_COV_URVK_D				,&l_ndl_p1_temp_salonR	    ,1030-OFFX, 851-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkD	        ,P1_NDL_URVK_D				,&l_cov_p1_urvk	        ,1036-OFFX, 858-OFFY,29, 7,	icb_urvkD	, tbl_urvk, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkR	        ,P1_NDL_URVK_R				,&l_ndl_p1_urvkD	        ,1036-OFFX, 858-OFFY,29, 7,	icb_urvkR	, tbl_urvk, 18					, p1_4) 
MY_ICON2	(cov_p1_uvpd			,P1_COV_UVPD_D				,&l_ndl_p1_urvkR	        , 533-OFFX, 849-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altD		,P1_NDL_UVPDALT_D		    ,&l_cov_p1_uvpd			, 540-OFFX, 855-OFFY,28, 7,	icb_altD 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altR		,P1_NDL_UVPDALT_R		    ,&l_ndl_p1_uvpd_altD		, 540-OFFX, 855-OFFY,28, 7,	icb_altR 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressD	,P1_NDL_UVPDDIFPRESS_D		,&l_ndl_p1_uvpd_altR		, 540-OFFX, 855-OFFY,28, 7,	icb_difpressD, tbl_difpress, 18				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressR	,P1_NDL_UVPDDIFPRESS_R		,&l_ndl_p1_uvpd_difpressD	, 540-OFFX, 855-OFFY,28, 7,	icb_difpressR, tbl_difpress, 18				, p1_4) 
MY_ICON2	(cov_p1_var10	        ,P1_COV_VAR10_D				,&l_ndl_p1_uvpd_difpressR	, 536-OFFX, 992-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_var10D	        ,P1_NDL_VAR10_D				,&l_cov_p1_var10	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10D	, tbl_var10, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_var10R	        ,P1_NDL_VAR10_R				,&l_ndl_p1_var10D	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10R	, tbl_var10, 18					, p1_4) 
MY_ICON2	(cov_p1_var30	        ,P1_COV_VAR30_D				,&l_ndl_p1_var10R	        ,1028-OFFX, 699-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_var30D	        ,P1_NDL_VAR30_D				,&l_cov_p1_var30	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30D	, tbl_var30, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_var30R	        ,P1_NDL_VAR30_R				,&l_ndl_p1_var30D	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30R	, tbl_var30, 18					, p1_4) 
MY_ICON2	(cov_p1_altim			,P1_COV_ALTIM_D				,&l_ndl_p1_var30R	        , 673-OFFX, 848-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100D    	,P1_NDL_ALTIM100_D     		,&l_cov_p1_altim			, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100D	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100R    	,P1_NDL_ALTIM100_R     		,&l_ndl_p1_altim_100D    	, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100R	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kD    	,P1_NDL_ALTIM1K_D     		,&l_ndl_p1_altim_100R    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kD		, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kR    	,P1_NDL_ALTIM1K_R     		,&l_ndl_p1_altim_1kD    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kR		, 0, 10				, p1_4) 
/*
MY_ICON2	(cov_p1_altim_scale_met ,P1_COV_ALTIMSCALEMETER_D	,&l_ndl_p1_altim_1kR    	,   0-OFFX,   0-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_altim_scale_feet,P1_COV_ALTIMSCALEFEET_D	,&l_cov_p1_altim_scale_met	,   0-OFFX,   0-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_engD,P1_NDL_ALTIMPRESSENG_D 	,&l_cov_p1_altim_scale_feet	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_engD,	0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_engR,P1_NDL_ALTIMPRESSENG_R 	,&l_ndl_p1_altim_press_engD	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_engR,	0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_rusD,P1_NDL_ALTIMPRESSRUS_D		,&l_ndl_p1_altim_press_engR	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_rusD,	0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_rusR,P1_NDL_ALTIMPRESSRUS_R		,&l_ndl_p1_altim_press_rusD	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_rusR,	0, 10				, p1_4) 
*/
MY_ICON2	(cov_p1_volts115		,P1_COV_VOLTS115_D			,&l_ndl_p1_altim_1kR    	, 950-OFFX,1002-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_volts36			,P1_COV_VOLTS36_D			,&l_cov_p1_volts115			, 874-OFFX,1002-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_volts27			,P1_COV_VOLTS27_D			,&l_cov_p1_volts36			, 809-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps27			,P1_COV_AMPS27_D			,&l_cov_p1_volts27			, 730-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen3		,P1_COV_AMPSGEN3_D			,&l_cov_p1_amps27			, 651-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen2		,P1_COV_AMPSGEN2_D			,&l_cov_p1_amps_gen3		, 571-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen1		,P1_COV_AMPSGEN1_D			,&l_cov_p1_amps_gen2		, 493-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115D		,P1_NDL_VOLTS115_D			,&l_cov_p1_amps_gen1		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115D	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115R		,P1_NDL_VOLTS115_R			,&l_ndl_p1_volts115D		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115R	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36D		,P1_NDL_VOLTS36_D			,&l_ndl_p1_volts115R		, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36D		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36R		,P1_NDL_VOLTS36_R			,&l_ndl_p1_volts36D		, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36R		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27D		,P1_NDL_VOLTS27_D			,&l_ndl_p1_volts36R		, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27D		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27R		,P1_NDL_VOLTS27_R			,&l_ndl_p1_volts27D		, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27R		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27D			,P1_NDL_AMPS27_D			,&l_ndl_p1_volts27R		, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27D		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27R			,P1_NDL_AMPS27_R			,&l_ndl_p1_amps27D			, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27R		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3D		,P1_NDL_AMPSGEN3_D			,&l_ndl_p1_amps27R			, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3D	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3R		,P1_NDL_AMPSGEN3_R			,&l_ndl_p1_amps_gen3D		, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3R	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2D		,P1_NDL_AMPSGEN2_D			,&l_ndl_p1_amps_gen3R		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2D	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2R		,P1_NDL_AMPSGEN2_R			,&l_ndl_p1_amps_gen2D		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2R	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1D		,P1_NDL_AMPSGEN1_D			,&l_ndl_p1_amps_gen2R		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1D	, tbl_gen1		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1R		,P1_NDL_AMPSGEN1_R			,&l_ndl_p1_amps_gen1D		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1R	, tbl_gen1		, 18	, p1_4) 
MY_ICON2	(p1_4Ico				,P1_BACKGROUND4_D			,&l_ndl_p1_amps_gen1R		,	     0,		   0,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_STATIC2	(p1_4bg,p1_4_list		,P1_BACKGROUND4				,&l_p1_4Ico				, p1_4);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(knob_p1_temp_duct		,P1_KNB_TEMPDUCT_D_00	    ,NULL        				, 731-OFFX, 896-OFFY,	icb_tdknob			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts115		,P1_KNB_VOLTS115_D_00		,&l_knob_p1_temp_duct		,1021-OFFX,1099-OFFY,	icb_knob_volts115	, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts36		,P1_KNB_VOLTS36_D_00		,&l_knob_p1_volts115		, 951-OFFX,1099-OFFY,	icb_knob_volts36 	,12*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_volts27		,P1_KNB_VOLTS27_D_00		,&l_knob_p1_volts36		, 881-OFFX,1099-OFFY,	icb_knob_volts27 	, 7*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_altim			,P1_KNB_ALTIM_D_00		    ,&l_knob_p1_volts27		, 663-OFFX, 915-OFFY,	icb_uvid			,20*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen3					,P1_AZS_GEN3_D_00			,&l_knob_p1_altim			, 800-OFFX,1033-OFFY,	icb_gen3			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen2					,P1_AZS_GEN2_D_00			,&l_gen3					, 766-OFFX,1033-OFFY,	icb_gen2			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(gen1					,P1_AZS_GEN1_D_00			,&l_gen2					, 737-OFFX,1033-OFFY,	icb_gen1			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo05				,P1_TBL_05_D_00				,&l_gen1					, 528-OFFX, 507-OFFY,	icb_tablo05			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo06				,P1_TBL_06_D_00				,&l_p1_tablo05				, 583-OFFX, 507-OFFY,	icb_tablo06			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo07				,P1_TBL_07_D_00				,&l_p1_tablo06				, 629-OFFX, 507-OFFY,	icb_tablo07			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo08				,P1_TBL_08_D_00				,&l_p1_tablo07				, 673-OFFX, 507-OFFY,	icb_tablo08			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo09				,P1_TBL_09_D_00				,&l_p1_tablo08				, 719-OFFX, 507-OFFY,	icb_tablo09			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo10				,P1_TBL_10_D_00				,&l_p1_tablo09				, 764-OFFX, 507-OFFY,	icb_tablo10			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo11				,P1_TBL_11_D_00				,&l_p1_tablo10				, 483-OFFX, 551-OFFY,	icb_tablo11			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo12				,P1_TBL_12_D_00				,&l_p1_tablo11				, 539-OFFX, 551-OFFY,	icb_tablo12			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo13				,P1_TBL_13_D_00				,&l_p1_tablo12				, 583-OFFX, 551-OFFY,	icb_tablo13			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo14				,P1_TBL_14_D_00				,&l_p1_tablo13				, 629-OFFX, 551-OFFY,	icb_tablo14			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo15				,P1_TBL_15_D_00				,&l_p1_tablo14				, 673-OFFX, 551-OFFY,	icb_tablo15			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo16				,P1_TBL_16_D_00				,&l_p1_tablo15				, 719-OFFX, 551-OFFY,	icb_tablo16			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo17				,P1_TBL_17_D_00				,&l_p1_tablo16				, 764-OFFX, 551-OFFY,	icb_tablo17			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo18				,P1_TBL_18_D_00				,&l_p1_tablo17				, 809-OFFX, 551-OFFY,	icb_tablo18			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo19				,P1_TBL_19_D_00				,&l_p1_tablo18				, 859-OFFX, 551-OFFY,	icb_tablo19			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo20				,P1_TBL_20_D_00				,&l_p1_tablo19				,1032-OFFX, 578-OFFY,	icb_tablo20			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo24				,P1_TBL_24_D_00				,&l_p1_tablo20				, 589-OFFX,1043-OFFY,	icb_tablo24			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo25				,P1_TBL_25_D_00				,&l_p1_tablo24				, 646-OFFX,1046-OFFY,	icb_tablo25			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(p1_tablo26				,P1_TBL_26_D_00				,&l_p1_tablo25				, 692-OFFX,1046-OFFY,	icb_tablo26			, 3*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(btn_p1_agb_aret	    ,P1_BTN_AGBARET_D_00		,&l_p1_tablo26				, 915-OFFX, 626-OFFY,	icb_agbmain			, 2*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(knob_p1_agb_pitch      ,P1_KNB_AGBPITCH_D_00		,&l_btn_p1_agb_aret	    , 757-OFFX, 752-OFFY,	icb_knb_agb_main	,20*PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_agb_flag	    ,P1_COV_AGBFLAG_D			,&l_knob_p1_agb_pitch      , 772-OFFX, 612-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagD	    ,P1_NDL_AGBFLAG_D			,&l_cov_p1_agb_flag	    , 801-OFFX, 668-OFFY,1,9,	icb_flagmD,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_flagM	    ,P1_NDL_AGBFLAG_M			,&l_ndl_p1_agb_flagD	    , 801-OFFX, 668-OFFY,1,9,	icb_flagmM,	0,6								, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeD	    ,P1_NDL_AGBPLANE_D			,&l_ndl_p1_agb_flagM	    , 854-OFFX, 706-OFFY,63,6,	icb_planemD,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_planeM	    ,P1_NDL_AGBPLANE_M			,&l_ndl_p1_agb_planeD	    , 854-OFFX, 706-OFFY,63,6,	icb_planemM,	0,18							, p1_4) 
MY_ICON2	(cov_p1_agb_ball	    ,P1_COV_AGBBALL_D			,&l_ndl_p1_agb_planeM	    , 846-OFFX, 766-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballD	    ,P1_NDL_AGBBALL_D			,&l_cov_p1_agb_ball	    , 854-OFFX, 638-OFFY,7,10,	icb_ballmD,	0,18							, p1_4) 
MY_NEEDLE2	(ndl_p1_agb_ballM	    ,P1_NDL_AGBBALL_M			,&l_ndl_p1_agb_ballD	    , 854-OFFX, 638-OFFY,7,10,	icb_ballmM,	0,18							, p1_4) 
MY_ICON2	(cov_p1_agb_rail	    ,P1_COV_AGBRAIL_D			,&l_ndl_p1_agb_ballM	    , 847-OFFX, 642-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_STAT2	(alpha_p1_agb1			,P1_ALPHA_AGB				,&l_cov_p1_agb_rail	    , 790-OFFX, 640-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA				, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchD	    ,P1_MOV_AGBMAINPITCH_D		,&l_alpha_p1_agb1			, 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmD,	-282,282						, p1_4) 
MY_MOVING2	(mov_p1_agb_pitchM	    ,P1_MOV_AGBMAINPITCH_M		,&l_mov_p1_agb_pitchD	    , 783-OFFX, 634-OFFY,0,0,0,	icb_pitchmM,	-282,282						, p1_4) 
MY_ICON2	(knob_p1_kppms_scale	,P1_KNB_KPPMSSCALE_D_00		,&l_mov_p1_agb_pitchM	    , 901-OFFX, 932-OFFY,	icb_kppms			,20*PANEL_LIGHT_MAX				, p1_4)
MY_NEEDLE2	(ndl_p1_kppms_hdgD		,P1_NDL_KPPMSHDG_D			,&l_knob_p1_kppms_scale	, 861-OFFX, 892-OFFY,5,9,icb_hdgD,0,10									, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_hdgM		,P1_NDL_KPPMSHDG_M			,&l_ndl_p1_kppms_hdgD		, 861-OFFX, 892-OFFY,5,9,icb_hdgM,0,10									, p1_4) 
MY_STAT2	(alpha_p1_kppms			,P1_ALPHA_KPPMS				,&l_ndl_p1_kppms_hdgM		, 793-OFFX, 822-OFFY,IMAGE_USE_TRANSPARENCY|IMAGE_USE_ERASE|IMAGE_USE_ALPHA,p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleD	,P1_NDL_KPPMSSCALE_D		,&l_alpha_p1_kppms			, 861-OFFX, 891-OFFY,67,67,icb_scaleD,0,18								, p1_4) 
MY_NEEDLE2	(ndl_p1_kppms_scaleM	,P1_NDL_KPPMSSCALE_M		,&l_ndl_p1_kppms_scaleD	, 861-OFFX, 891-OFFY,67,67,icb_scaleM,0,18								, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissD,P1_SLD_KPPMSILSGLIDE_D		,&l_ndl_p1_kppms_scaleM	, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissD,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_glissM,P1_SLD_KPPMSILSGLIDE_M		,&l_sld_p1_kppms_ils_glissD, 812-OFFX, 887-OFFY,	NULL,0,icb_ils_glissM,0.25						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursD	,P1_SLD_KPPMSILSCOURSE_D	,&l_sld_p1_kppms_ils_glissM, 857-OFFX, 843-OFFY,	icb_ils_kursD,0.25,NULL,0						, p1_4) 
MY_SLIDER2	(sld_p1_kppms_ils_kursM	,P1_SLD_KPPMSILSCOURSE_M	,&l_sld_p1_kppms_ils_kursD	, 857-OFFX, 843-OFFY,	icb_ils_kursM,0.25,NULL,0						, p1_4) 
MY_ICON2	(blk_kppmskr	        ,P1_BLK_KPPMSCOURSE_D		,&l_sld_p1_kppms_ils_kursM	, 839-OFFX, 870-OFFY,	icb_blenker_kurs	, 1*PANEL_LIGHT_MAX				, p1_4) 									
MY_ICON2	(blk_kppmsgl	        ,P1_BLK_KPPMSGLIDESLOPE_D	,&l_blk_kppmskr	        , 869-OFFX, 891-OFFY,	icb_blenker_gliss	, 1*PANEL_LIGHT_MAX				, p1_4) 								
MY_ICON2	(cov_p1_kus             ,P1_COV_KUS_D				,&l_blk_kppmsgl	        , 673-OFFX, 698-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasD        ,P1_NDL_KUSIAS_D			,&l_cov_p1_kus             , 678-OFFX, 704-OFFY,41,6,icb_iasD,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_iasM        ,P1_NDL_KUSIAS_M			,&l_ndl_p1_kus_iasD        , 678-OFFX, 704-OFFY,41,6,icb_iasM,tbl_ias,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasD        ,P1_NDL_KUSTAS_D			,&l_ndl_p1_kus_iasM        , 678-OFFX, 704-OFFY,10,5,icb_tasD,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_kus_tasM        ,P1_NDL_KUSTAS_M			,&l_ndl_p1_kus_tasD        , 678-OFFX, 704-OFFY,10,5,icb_tasM,tbl_tas,10							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1D	    ,P1_NDL_RKOMPARK1_D			,&l_ndl_p1_kus_tasM        , 541-OFFX, 705-OFFY,35, 7,	icb_ark1D	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark1M	    ,P1_NDL_RKOMPARK1_M			,&l_ndl_p1_rcomp_ark1D	    , 541-OFFX, 705-OFFY,35, 7,	icb_ark1M	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2D	    ,P1_NDL_RKOMPARK2_D			,&l_ndl_p1_rcomp_ark1M	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2D	, 0, 18							, p1_4) 
MY_NEEDLE2	(ndl_p1_rcomp_ark2M	    ,P1_NDL_RKOMPARK2_M			,&l_ndl_p1_rcomp_ark2D	    , 541-OFFX, 705-OFFY,36, 9,	icb_ark2M	, 0, 18							, p1_4) 
MY_ICON2	(cov_p1_temp_duct	    ,P1_COV_TEMPDUCT_D			,&l_ndl_p1_rcomp_ark2M	    , 632-OFFX, 953-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductD      ,P1_NDL_TEMPDUCT_D			,&l_cov_p1_temp_duct	    , 682-OFFX,1011-OFFY,  4, 5,	icb_ductD		, tbl_duct, 10			, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_ductM      ,P1_NDL_TEMPDUCT_M			,&l_ndl_p1_temp_ductD      , 682-OFFX,1011-OFFY,  4, 5,	icb_ductM		, tbl_duct, 10			, p1_4) 
MY_ICON2	(cov_p1_temp_salon	    ,P1_COV_TEMPSALON_D			,&l_ndl_p1_temp_ductM      ,1030-OFFX, 985-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonD	    ,P1_NDL_TEMPSALON_D			,&l_cov_p1_temp_salon	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonD	, tbl_salon, 18				, p1_4) 
MY_NEEDLE2	(ndl_p1_temp_salonM	    ,P1_NDL_TEMPSALON_M			,&l_ndl_p1_temp_salonD	    ,1047-OFFX,1002-OFFY,  3, 4,	icb_salonM	, tbl_salon, 18				, p1_4) 
MY_ICON2	(cov_p1_urvk	        ,P1_COV_URVK_D				,&l_ndl_p1_temp_salonM	    ,1030-OFFX, 851-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkD	        ,P1_NDL_URVK_D				,&l_cov_p1_urvk	        ,1036-OFFX, 858-OFFY,29, 7,	icb_urvkD	, tbl_urvk, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_urvkM	        ,P1_NDL_URVK_M				,&l_ndl_p1_urvkD	        ,1036-OFFX, 858-OFFY,29, 7,	icb_urvkM	, tbl_urvk, 18					, p1_4) 
MY_ICON2	(cov_p1_uvpd			,P1_COV_UVPD_D				,&l_ndl_p1_urvkM	        , 533-OFFX, 849-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altD		,P1_NDL_UVPDALT_D		    ,&l_cov_p1_uvpd			, 540-OFFX, 855-OFFY,28, 7,	icb_altD 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_altM		,P1_NDL_UVPDALT_M		    ,&l_ndl_p1_uvpd_altD		, 540-OFFX, 855-OFFY,28, 7,	icb_altM 	, tbl_alt, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressD	,P1_NDL_UVPDDIFPRESS_D		,&l_ndl_p1_uvpd_altM		, 540-OFFX, 855-OFFY,28, 7,	icb_difpressD, tbl_difpress, 18				, p1_4) 
MY_NEEDLE2	(ndl_p1_uvpd_difpressM	,P1_NDL_UVPDDIFPRESS_M		,&l_ndl_p1_uvpd_difpressD	, 540-OFFX, 855-OFFY,28, 7,	icb_difpressM, tbl_difpress, 18				, p1_4) 
MY_ICON2	(cov_p1_var10	        ,P1_COV_VAR10_D				,&l_ndl_p1_uvpd_difpressM	, 536-OFFX, 992-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_var10D	        ,P1_NDL_VAR10_D				,&l_cov_p1_var10	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10D	, tbl_var10, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_var10M	        ,P1_NDL_VAR10_M				,&l_ndl_p1_var10D	        , 543-OFFX, 998-OFFY,27, 8,	icb_var10M	, tbl_var10, 18					, p1_4) 
MY_ICON2	(cov_p1_var30	        ,P1_COV_VAR30_D				,&l_ndl_p1_var10M	        ,1028-OFFX, 699-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_var30D	        ,P1_NDL_VAR30_D				,&l_cov_p1_var30	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30D	, tbl_var30, 18					, p1_4) 
MY_NEEDLE2	(ndl_p1_var30M	        ,P1_NDL_VAR30_M				,&l_ndl_p1_var30D	        ,1035-OFFX, 706-OFFY,28, 5,	icb_var30M	, tbl_var30, 18					, p1_4) 
MY_ICON2	(cov_p1_altim			,P1_COV_ALTIM_D				,&l_ndl_p1_var30M	        , 673-OFFX, 848-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100D    	,P1_NDL_ALTIM100_D     		,&l_cov_p1_altim			, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100D	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_100M    	,P1_NDL_ALTIM100_M     		,&l_ndl_p1_altim_100D    	, 679-OFFX, 854-OFFY, 45, 6,	icb_ndl_altim100M	, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kD    	,P1_NDL_ALTIM1K_D     		,&l_ndl_p1_altim_100M    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kD		, 0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_1kM    	,P1_NDL_ALTIM1K_M     		,&l_ndl_p1_altim_1kD    	, 679-OFFX, 854-OFFY, 19, 8,	icb_ndl_altim1kM		, 0, 10				, p1_4) 
/*
MY_ICON2	(cov_p1_altim_scale_met ,P1_COV_ALTIMSCALEMETER_D	,&l_ndl_p1_altim_1kM    	,   0-OFFX,   0-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_altim_scale_feet,P1_COV_ALTIMSCALEFEET_D	,&l_cov_p1_altim_scale_met	,   0-OFFX,   0-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_engD,P1_NDL_ALTIMPRESSENG_D 	,&l_cov_p1_altim_scale_feet	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_engD,	0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_engM,P1_NDL_ALTIMPRESSENG_M 	,&l_ndl_p1_altim_press_engD	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_engM,	0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_rusD,P1_NDL_ALTIMPRESSRUS_D		,&l_ndl_p1_altim_press_engM	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_rusD,	0, 10				, p1_4) 
MY_NEEDLE2	(ndl_p1_altim_press_rusM,P1_NDL_ALTIMPRESSRUS_M		,&l_ndl_p1_altim_press_rusD	,   0-OFFX,   0-OFFY,  0, 0,	icb_ndl_altim_rusM,	0, 10				, p1_4) 
*/
MY_ICON2	(cov_p1_volts115		,P1_COV_VOLTS115_D			,&l_ndl_p1_altim_1kM    	, 950-OFFX,1002-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_volts36			,P1_COV_VOLTS36_D			,&l_cov_p1_volts115			, 874-OFFX,1002-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_volts27			,P1_COV_VOLTS27_D			,&l_cov_p1_volts36			, 809-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps27			,P1_COV_AMPS27_D			,&l_cov_p1_volts27			, 730-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen3		,P1_COV_AMPSGEN3_D			,&l_cov_p1_amps27			, 651-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen2		,P1_COV_AMPSGEN2_D			,&l_cov_p1_amps_gen3		, 571-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_ICON2	(cov_p1_amps_gen1		,P1_COV_AMPSGEN1_D			,&l_cov_p1_amps_gen2		, 493-OFFX,1116-OFFY,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115D		,P1_NDL_VOLTS115_D			,&l_cov_p1_amps_gen1		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115D	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts115M		,P1_NDL_VOLTS115_M			,&l_ndl_p1_volts115D		, 965-OFFX,1017-OFFY,  3, 5,	icb_volts115M	, tbl_volt115	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36D		,P1_NDL_VOLTS36_D			,&l_ndl_p1_volts115M		, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36D		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts36M		,P1_NDL_VOLTS36_M			,&l_ndl_p1_volts36D		, 890-OFFX,1017-OFFY,  3, 5,	icb_volts36M		, tbl_volt36	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27D		,P1_NDL_VOLTS27_D			,&l_ndl_p1_volts36M		, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27D		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_volts27M		,P1_NDL_VOLTS27_M			,&l_ndl_p1_volts27D		, 824-OFFX,1131-OFFY,  3, 5,	icb_volts27M		, tbl_volt27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27D			,P1_NDL_AMPS27_D			,&l_ndl_p1_volts27M		, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27D		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps27M			,P1_NDL_AMPS27_M			,&l_ndl_p1_amps27D			, 745-OFFX,1131-OFFY,  3, 4,	icb_amps27M		, tbl_amps27	, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3D		,P1_NDL_AMPSGEN3_D			,&l_ndl_p1_amps27M			, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3D	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen3M		,P1_NDL_AMPSGEN3_M			,&l_ndl_p1_amps_gen3D		, 667-OFFX,1131-OFFY,  2, 4,	icb_amps_gen3M	, tbl_gen3		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2D		,P1_NDL_AMPSGEN2_D			,&l_ndl_p1_amps_gen3M		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2D	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen2M		,P1_NDL_AMPSGEN2_M			,&l_ndl_p1_amps_gen2D		, 588-OFFX,1131-OFFY,  2, 4,	icb_amps_gen2M	, tbl_gen2		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1D		,P1_NDL_AMPSGEN1_D			,&l_ndl_p1_amps_gen2M		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1D	, tbl_gen1		, 18	, p1_4) 
MY_NEEDLE2	(ndl_p1_amps_gen1M		,P1_NDL_AMPSGEN1_M			,&l_ndl_p1_amps_gen1D		, 509-OFFX,1131-OFFY,  2, 4,	icb_amps_gen1M	, tbl_gen1		, 18	, p1_4) 
MY_ICON2	(p1_4Ico				,P1_BACKGROUND4_D			,&l_ndl_p1_amps_gen1M		,	     0,		   0,	icb_Ico			,PANEL_LIGHT_MAX				, p1_4) 
MY_STATIC2	(p1_4bg,p1_4_list		,P1_BACKGROUND4				,&l_p1_4Ico				, p1_4);
#endif
																								 
																								 
#endif																								 
																								 
																								 
																								 
																								 
																								 