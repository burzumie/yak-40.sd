/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P1.h"

double FSAPI icbPri66BGOn(PELEMENT_ICON pelement) 
{
	if(PWR_GET(PWR_GMK)) {
		CHK_BRT();
		return POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):POS_GET(POS_PANEL_STATE)+PANEL_LIGHT_MAX;
	}
	return -1; 
} 

double FSAPI icbPri66BGOff(PELEMENT_ICON pelement) 
{
	CHK_BRT();
	return POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):POS_GET(POS_PANEL_STATE)+PANEL_LIGHT_MAX;
} 


static double FSAPI icbIco1R_Pri66(PELEMENT_ICON pelement) 
{ 
	if(!POS_GET(POS_PANEL_LANG)) {
		CHK_BRT();
		SHOW_IMAGE(pelement);
		return LMP_GET(LMP_GMK_OSN)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIco1E_Pri66(PELEMENT_ICON pelement) 
{ 
	if(POS_GET(POS_PANEL_LANG)) {
		CHK_BRT();
		SHOW_IMAGE(pelement);
		return LMP_GET(LMP_GMK_OSN)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIco2R_Pri66(PELEMENT_ICON pelement) 
{
	if(!POS_GET(POS_PANEL_LANG)) {
		CHK_BRT();
		SHOW_IMAGE(pelement);
		return LMP_GET(LMP_GMK_ZAP)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIco2E_Pri66(PELEMENT_ICON pelement) 
{ 
	if(POS_GET(POS_PANEL_LANG)) {
		CHK_BRT();
		SHOW_IMAGE(pelement);
		return LMP_GET(LMP_GMK_ZAP)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIcoR_Pri66(PELEMENT_ICON pelement) 
{
	if(!POS_GET(POS_PANEL_LANG)) {
		CHK_BRT();
		SHOW_IMAGE(pelement);
		return PWR_GET(PWR_GMK)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

static double FSAPI icbIcoE_Pri66(PELEMENT_ICON pelement) 
{
	if(POS_GET(POS_PANEL_LANG)) {
		CHK_BRT();
		SHOW_IMAGE(pelement);
		return PWR_GET(PWR_GMK)+(int(POS_GET(POS_PANEL_STATE))<<1);
	}
	HIDE_IMAGE(pelement);
	return -1;
}

double ScaleOff(PELEMENT_NEEDLE pelement,int clr) 
{
	if(POS_GET(POS_PANEL_STATE)==clr&&!PWR_GET(PWR_GMK)) {
		CHK_BRT();
		SHOW_IMAGE(pelement);
		return NDL_GET(NDL_GMK_SCALE)*4;
	}
	HIDE_IMAGE(pelement);
	return 0;
};

double ScaleOn(PELEMENT_NEEDLE pelement,int clr) 
{
	if(POS_GET(POS_PANEL_STATE)==clr&&PWR_GET(PWR_GMK)) {
		CHK_BRT();
		SHOW_IMAGE(pelement);
		return NDL_GET(NDL_GMK_SCALE)*4;
	}
	HIDE_IMAGE(pelement);
	return 0;
};

static double FSAPI icbNdl2R_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOn(pelement,PANEL_LIGHT_RED); };
static double FSAPI icbNdl2P_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOn(pelement,PANEL_LIGHT_PLF); };
static double FSAPI icbNdl2M_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOn(pelement,PANEL_LIGHT_MIX); };
static double FSAPI icbNdl2D_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOn(pelement,PANEL_LIGHT_DAY); };
static double FSAPI icbNdl1R_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOff(pelement,PANEL_LIGHT_RED); };
static double FSAPI icbNdl1P_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOff(pelement,PANEL_LIGHT_PLF); };
static double FSAPI icbNdl1M_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOff(pelement,PANEL_LIGHT_MIX); };
static double FSAPI icbNdl1D_Pri66	(PELEMENT_NEEDLE pelement) { return ScaleOff(pelement,PANEL_LIGHT_DAY); };
static FLOAT64 FSAPI icb_switch_01		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_HEMISPHERE	,2)}
static FLOAT64 FSAPI icb_switch_02		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_MODE          ,2)}
static FLOAT64 FSAPI icb_switch_03		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_TEST          ,3)}
static FLOAT64 FSAPI icb_switch_04		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_COURSE        ,3)}
static FLOAT64 FSAPI icb_switch_05		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_PRI_AUX       ,2)}
static FLOAT64 FSAPI icb_gmkknob		(PELEMENT_ICON pelement)	{SHOW_HND(HND_GMK               ,90)}   


bool FSAPI mcbsevuzh_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{
	AZS_TGL(AZS_GMK_HEMISPHERE);
	return TRUE;
};

bool FSAPI mcbpriaux_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	AZS_TGL(AZS_GMK_PRI_AUX);
	return TRUE;
};

bool FSAPI mcbmkgpk_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	AZS_TGL(AZS_GMK_MODE);
	return TRUE;
};

bool FSAPI mcbtstl_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	PRS_AZS(AZS_GMK_TEST,1);
	return TRUE;
};

bool FSAPI mcbtstr_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	PRS_AZS(AZS_GMK_TEST,2);
	return TRUE;
};

bool FSAPI mcbzkl_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{
	PRS_AZS(AZS_GMK_COURSE,1);
	return TRUE;
};

bool FSAPI mcbzkr_Pri66(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{ 
	PRS_AZS(AZS_GMK_COURSE,2);
	return TRUE;
};

static BOOL FSAPI mcb_gmk_rightPri66(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		NDL_INC(NDL_GMK_SCALE);
		HND_INC(HND_GMK);
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		NDL_INC2(NDL_GMK_SCALE,10);
		HND_INC2(HND_GMK,10);
		if(HND_GET(HND_GMK)>89)HND_DEC2(HND_GMK,89);
	}
	return TRUE;
}

static BOOL FSAPI mcb_gmk_leftPri66(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		NDL_DEC(NDL_GMK_SCALE);
		HND_DEC(HND_GMK);
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		NDL_DEC2(NDL_GMK_SCALE,10);
		HND_DEC2(HND_GMK,10);
		if(HND_GET(HND_GMK)<0)HND_INC2(HND_GMK,89);
	}
	return TRUE;
}

BOOL FSAPI mcb_gmk_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	ImportTable.pPanels->panel_window_toggle(IDENT_P15);
	return true;
}

static MAKE_TCB(Pri66_tcb_01,AZS_TT(AZS_GMK_HEMISPHERE)) 
static MAKE_TCB(Pri66_tcb_02,AZS_TT(AZS_GMK_PRI_AUX	)) 
static MAKE_TCB(Pri66_tcb_03,AZS_TT(AZS_GMK_MODE	)) 
static MAKE_TCB(Pri66_tcb_04,AZS_TT(AZS_GMK_TEST	)) 
static MAKE_TCB(Pri66_tcb_05,AZS_TT(AZS_GMK_COURSE	)) 
static MAKE_TCB(Pri66_tcb_06,HND_TT(HND_GMK			))
static MAKE_TCB(Pri66_tcb_07,LMP_TT(LMP_GMK_ZAP		))
static MAKE_TCB(Pri66_tcb_08,LMP_TT(LMP_GMK_OSN		))
//static MAKE_TCB(Pri66_tcb_09,return "";) //POS_TT(POS_MAX    		))

static MOUSE_TOOLTIP_ARGS(ttargs) 
MAKE_TTA(Pri66_tcb_01) 
MAKE_TTA(Pri66_tcb_02) 
MAKE_TTA(Pri66_tcb_03) 
MAKE_TTA(Pri66_tcb_04) 
MAKE_TTA(Pri66_tcb_05) 
MAKE_TTA(Pri66_tcb_06) 
MAKE_TTA(Pri66_tcb_07) 
MAKE_TTA(Pri66_tcb_08) 
//MAKE_TTA(Pri66_tcb_09) 
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p1_6,HELP_NONE,0,0) 
MOUSE_PBOX( 30, 30,P15_BACKGROUND_D_SX-30,P15_BACKGROUND_D_SY-30,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_gmk_big)    
MOUSE_TBOX("1", 50, 15,75,65,CURSOR_HAND,MOUSE_LR,mcbsevuzh_Pri66) 
MOUSE_TBOX("2",175, 15,75,65,CURSOR_HAND,MOUSE_LR,mcbpriaux_Pri66) 
MOUSE_TBOX("3",300, 15,75,65,CURSOR_HAND,MOUSE_LR,mcbmkgpk_Pri66) 
MOUSE_TBOX("4", 30,170,55,80,CURSOR_HAND,MOUSE_DLR,mcbtstl_Pri66) 
MOUSE_TBOX("4", 85,170,55,80,CURSOR_HAND,MOUSE_DLR,mcbtstr_Pri66) 
MOUSE_TBOX("5",280,170,55,80,CURSOR_HAND,MOUSE_DLR,mcbzkl_Pri66) 
MOUSE_TBOX("5",335,170,55,80,CURSOR_HAND,MOUSE_DLR,mcbzkr_Pri66) 
MOUSE_TSHB( "6",  140,  130,145, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_gmk_leftPri66,mcb_gmk_rightPri66) 
MOUSE_TTPB( "7",  121,   5, 61,52)
MOUSE_TTPB( "8",  243,   5, 61,52)
MOUSE_END 

extern PELEMENT_HEADER p1_6_list; 
GAUGE_HEADER_FS700_EX(P15_BACKGROUND_D_SX, "p1_6", &p1_6_list, rect_p1_6, 0, 0, 0, 0, p1_6); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(swt_gmk_05			,P15_SWT_GMK3_D_00	    	,NULL					,298, 20,icb_switch_02,2*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(swt_gmk_04			,P15_SWT_GMK5_D_00	    	,&l_swt_gmk_05			,298,175,icb_switch_04,3*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(swt_gmk_03			,P15_SWT_GMK4_D_00	    	,&l_swt_gmk_04			, 49,175,icb_switch_03,3*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(swt_gmk_02			,P15_SWT_GMK2_D_00	    	,&l_swt_gmk_03			,174, 20,icb_switch_05,2*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(swt_gmk_01			,P15_SWT_GMK1_D_00	    	,&l_swt_gmk_02			, 49, 20,icb_switch_01,2*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(Pri66gmk_scale     ,P15_KNB_GMKLAT_D_00	    ,&l_swt_gmk_01			,169,126,icb_gmkknob  ,90*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(Pri66Ico2E			,P15_TBL_GMK2INTL_D_00		,&l_Pri66gmk_scale		,243,  5,icbIco2E_Pri66,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66Ico2R			,P15_TBL_GMK2_D_00			,&l_Pri66Ico2E			,243,  5,icbIco2R_Pri66,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66Ico1E			,P15_TBL_GMK1INTL_D_00		,&l_Pri66Ico2R			,121,  5,icbIco1E_Pri66,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66Ico1R			,P15_TBL_GMK1_D_00			,&l_Pri66Ico1E			,121,  5,icbIco1R_Pri66,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66CovOn  		,P15_PRI266INTL_D			,&l_Pri66Ico1R			,  0,  0,icbPri66BGOn ,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66CovOff  		,P15_PRI66INTL_D			,&l_Pri66CovOn  		,  0,  0,icbPri66BGOff,2*PANEL_LIGHT_MAX, p1_6);
MY_NEEDLE2	(Pri66Ndl2R			,P15_SCL_GMK_R_01			,&l_Pri66CovOff  		,212,165, 70, 70,icbNdl2R_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl2P			,P15_SCL_GMK_P_01			,&l_Pri66Ndl2R			,212,165, 70, 70,icbNdl2P_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl2M			,P15_SCL_GMK_M_01			,&l_Pri66Ndl2P			,212,165, 70, 70,icbNdl2M_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl2D			,P15_SCL_GMK_D_01			,&l_Pri66Ndl2M			,212,165, 70, 70,icbNdl2D_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl1R			,P15_SCL_GMK_R_00			,&l_Pri66Ndl2D			,212,165, 70, 70,icbNdl1R_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl1P			,P15_SCL_GMK_P_00			,&l_Pri66Ndl1R			,212,165, 70, 70,icbNdl1P_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl1M			,P15_SCL_GMK_M_00			,&l_Pri66Ndl1P			,212,165, 70, 70,icbNdl1M_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl1D			,P15_SCL_GMK_D_00			,&l_Pri66Ndl1M			,212,165, 70, 70,icbNdl1D_Pri66 ,0,0,p1_6);
MY_ICON2	(p1_6Ico			,P15_BACKGROUND_D			,&l_Pri66Ndl1D			,  0,  0, icb_Ico				,PANEL_LIGHT_MAX  , p1_6) 
MY_STATIC2	(p1_6bg,p1_6_list	,P15_BACKGROUND_D			,&l_p1_6Ico				, p1_6);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(swt_gmk_05			,P15_SWT_GMK3_D_00	    	,NULL					,298, 20,icb_switch_02,2*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(swt_gmk_04			,P15_SWT_GMK5_D_00	    	,&l_swt_gmk_05			,298,175,icb_switch_04,3*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(swt_gmk_03			,P15_SWT_GMK4_D_00	    	,&l_swt_gmk_04			, 49,175,icb_switch_03,3*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(swt_gmk_02			,P15_SWT_GMK2_D_00	    	,&l_swt_gmk_03			,174, 20,icb_switch_05,2*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(swt_gmk_01			,P15_SWT_GMK1_D_00	    	,&l_swt_gmk_02			, 49, 20,icb_switch_01,2*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(Pri66gmk_scale     ,P15_KNB_GMKLAT_D_00	    ,&l_swt_gmk_01			,169,126,icb_gmkknob  ,90*PANEL_LIGHT_MAX,p1_6)
MY_ICON2	(Pri66Ico2E			,P15_TBL_GMK2INTL_D_00		,&l_Pri66gmk_scale		,243,  5,icbIco2E_Pri66,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66Ico2R			,P15_TBL_GMK2_D_00			,&l_Pri66Ico2E			,243,  5,icbIco2R_Pri66,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66Ico1E			,P15_TBL_GMK1INTL_D_00		,&l_Pri66Ico2R			,121,  5,icbIco1E_Pri66,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66Ico1R			,P15_TBL_GMK1_D_00			,&l_Pri66Ico1E			,121,  5,icbIco1R_Pri66,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66CovOn  		,P15_PRI266INTL_D			,&l_Pri66Ico1R			,  0,  0,icbPri66BGOn ,2*PANEL_LIGHT_MAX, p1_6);
MY_ICON2	(Pri66CovOff  		,P15_PRI66INTL_D			,&l_Pri66CovOn  		,  0,  0,icbPri66BGOff,2*PANEL_LIGHT_MAX, p1_6);
MY_NEEDLE2	(Pri66Ndl2P			,P15_SCL_GMK_P_01			,&l_Pri66CovOff  		,212,165, 70, 70,icbNdl2P_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl2D			,P15_SCL_GMK_D_01			,&l_Pri66Ndl2P			,212,165, 70, 70,icbNdl2D_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl1P			,P15_SCL_GMK_P_00			,&l_Pri66Ndl2D			,212,165, 70, 70,icbNdl1P_Pri66 ,0,0,p1_6);
MY_NEEDLE2	(Pri66Ndl1D			,P15_SCL_GMK_D_00			,&l_Pri66Ndl1P			,212,165, 70, 70,icbNdl1D_Pri66 ,0,0,p1_6);
MY_ICON2	(p1_6Ico			,P15_BACKGROUND_D			,&l_Pri66Ndl1D			,  0,  0, icb_Ico				,PANEL_LIGHT_MAX  , p1_6) 
MY_STATIC2	(p1_6bg,p1_6_list	,P15_BACKGROUND_D			,&l_p1_6Ico				, p1_6);
#endif

