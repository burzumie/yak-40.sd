/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P1.h"

bool ChkColor(PELEMENT_NEEDLE pelement,int st)
{ 
	if(POS_GET(POS_PANEL_STATE)==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

bool ChkColor(PELEMENT_SPRITE pelement,int st) 
{ 
	if(POS_GET(POS_PANEL_STATE)==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

//////////////////////////////////////////////////////////////////////////

static MAKE_SPCB(icb_scaleD			,if(!ChkColor(pelement,PANEL_LIGHT_DAY))return 0; SHOW_NDL(NDL_KPPMS1_SCALE		))
static MAKE_SPCB(icb_scaleM			,if(!ChkColor(pelement,PANEL_LIGHT_MIX))return 0; SHOW_NDL(NDL_KPPMS1_SCALE		))
static MAKE_SPCB(icb_scaleP			,if(!ChkColor(pelement,PANEL_LIGHT_PLF))return 0; SHOW_NDL(NDL_KPPMS1_SCALE		))
static MAKE_SPCB(icb_scaleR			,if(!ChkColor(pelement,PANEL_LIGHT_RED))return 0; SHOW_NDL(NDL_KPPMS1_SCALE		))

static double FSAPI icb_knbp1_7(PELEMENT_ICON pelement) 
{ 
	CHK_BRT();
	return KRM_GET(KRM_KPPMS1)+(POS_GET(POS_PANEL_STATE)*20);
}

static double FSAPI icbndlhdgd(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY))return 0; CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS1_SCALE)-90; };
static double FSAPI icbndlhdgm(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX))return 0; CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS1_SCALE)-90; };
static double FSAPI icbndlhdgp(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF))return 0; CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS1_SCALE)-90; };
static double FSAPI icbndlhdgr(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED))return 0; CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS1_SCALE)-90; };
static double FSAPI icbndlscld(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY))return 0; CHK_BRT(); return NDL_GET(NDL_KPPMS1_SCALE); };
static double FSAPI icbndlsclm(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX))return 0; CHK_BRT(); return NDL_GET(NDL_KPPMS1_SCALE); };
static double FSAPI icbndlsclp(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF))return 0; CHK_BRT(); return NDL_GET(NDL_KPPMS1_SCALE); };
static double FSAPI icbndlsclr(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED))return 0; CHK_BRT(); return NDL_GET(NDL_KPPMS1_SCALE); };
static double FSAPI icbsldgd(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS1_ILS_GLIDE))<<1;}HIDE_IMAGE(pelement);return 0;};
static double FSAPI icbsldgm(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS1_ILS_GLIDE))<<1;}HIDE_IMAGE(pelement);return 0;};
static double FSAPI icbsldgp(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS1_ILS_GLIDE))<<1;}HIDE_IMAGE(pelement);return 0;};
static double FSAPI icbsldgr(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS1_ILS_GLIDE))<<1;}HIDE_IMAGE(pelement);return 0;};
static double FSAPI icbsldcd(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS1_ILS_COURSE))<<1;}HIDE_IMAGE(pelement);return 0;};
static double FSAPI icbsldcm(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS1_ILS_COURSE))<<1;}HIDE_IMAGE(pelement);return 0;};
static double FSAPI icbsldcp(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS1_ILS_COURSE))<<1;}HIDE_IMAGE(pelement);return 0;};
static double FSAPI icbsldcr(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS1_ILS_COURSE))<<1;}HIDE_IMAGE(pelement);return 0;};

static double FSAPI icbblkc(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	if(!POS_GET(POS_KPPMS1_BLK_COURSE)) 
		return POS_GET(POS_PANEL_STATE); 
	else 
		return -1;
}

static double FSAPI icbblkg(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	if(!POS_GET(POS_KPPMS1_BLK_GLIDE)) 
		return POS_GET(POS_PANEL_STATE); 
	else 
		return -1;
} 

static double FSAPI icbcov_p1_7(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	if(POS_GET(POS_PANEL_LANG)) {
		SHOW_IMAGE(pelement);
		return POS_GET(POS_PANEL_STATE); 
	} 
	HIDE_IMAGE(pelement);
	return -1;
} 

//////////////////////////////////////////////////////////////////////////

static BOOL FSAPI mcb_knblp1_7(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE)
		NDL_DEC(NDL_KPPMS1_SCALE);
	else if(mouse_flags&MOUSE_RIGHTSINGLE)
		NDL_DEC2(NDL_KPPMS1_SCALE,10);
	KRM_DEC(KRM_KPPMS1);
	return TRUE;
}

static BOOL FSAPI mcb_knbrp1_7(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE)
		NDL_INC(NDL_KPPMS1_SCALE);
	else if(mouse_flags&MOUSE_RIGHTSINGLE)
		NDL_INC2(NDL_KPPMS1_SCALE,10);
	KRM_INC(KRM_KPPMS1);
	return TRUE;
}

BOOL FSAPI mcb_kppms_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01,return NDL_TTGET(NDL_KPPMS1_SCALE);) 

#ifndef ONLY3D

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p1_7,HELP_NONE,0,0)
MOUSE_TBOX("1", 30, 30,P12_BACKGROUND_D_SX,P12_BACKGROUND_D_SY,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_kppms_big)    
MOUSE_TSHB("1",232,248,60,60,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_knblp1_7,mcb_knbrp1_7) 
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p1_7_list; 
GAUGE_HEADER_FS700_EX(P12_BACKGROUND_D_SX, "p1_7", &p1_7_list, rect_p1_7, 0, 0, 0, 0, p1_7); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p1_7Spot2			,P12_COV_KPPMS1SPSPOT2_D	,NULL			, 224,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7spot1			,P12_COV_KPPMS1SPSPOT1_D	,&l_p1_7Spot2	,  40,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7knb			,P12_KNB_KPPMS1SP_D_00		,&l_p1_7spot1	, 232, 248,icb_knbp1_7			,20*PANEL_LIGHT_MAX,p1_7)
MY_NEEDLE2	(p1_7NdlHdgR		,P12_NDL_KPPMS1HDG_R		,&l_p1_7knb		, 151, 169, 0, 13,icbndlhdgr	,0,0,p1_7); 
MY_NEEDLE2	(p1_7NdlHdgP		,P12_NDL_KPPMS1HDG_P		,&l_p1_7NdlHdgR	, 151, 169, 0, 13,icbndlhdgp	,0,0,p1_7); 
MY_NEEDLE2	(p1_7NdlHdgM		,P12_NDL_KPPMS1HDG_M		,&l_p1_7NdlHdgP	, 151, 169, 0, 13,icbndlhdgm	,0,0,p1_7); 
MY_NEEDLE2	(p1_7NdlHdgD		,P12_NDL_KPPMS1HDG_D		,&l_p1_7NdlHdgM	, 151, 169, 0, 13,icbndlhdgd	,0,0,p1_7); 
MY_STATIC2A	(p1_7Alpha			,P12_ALPHA_KPPMS1			,&l_p1_7NdlHdgD	,  25,  39,p1_7); 
MY_SPRITE2	(p1_7ScaleR			,P12_SCL_KPPMS1SP_R			,&l_p1_7Alpha	,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleR,-1,p1_7)	
MY_SPRITE2	(p1_7ScaleP			,P12_SCL_KPPMS1SP_P			,&l_p1_7ScaleR	,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleP,-1,p1_7)	
MY_SPRITE2	(p1_7ScaleM			,P12_SCL_KPPMS1SP_M			,&l_p1_7ScaleP	,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleM,-1,p1_7)	
MY_SPRITE2	(p1_7ScaleD			,P12_SCL_KPPMS1SP_D			,&l_p1_7ScaleM	,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleD,-1,p1_7)	
MY_SLIDER2	(p1_7SldGR			,P12_NDL_KPPMS1ILSGLIDE_R	,&l_p1_7ScaleD	,  48, 159,0,0,icbsldgr,0.25,p1_7); 
MY_SLIDER2	(p1_7SldGP			,P12_NDL_KPPMS1ILSGLIDE_P	,&l_p1_7SldGR	,  48, 159,0,0,icbsldgp,0.25,p1_7); 
MY_SLIDER2	(p1_7SldGM			,P12_NDL_KPPMS1ILSGLIDE_M	,&l_p1_7SldGP	,  48, 159,0,0,icbsldgm,0.25,p1_7); 
MY_SLIDER2	(p1_7SldGD			,P12_NDL_KPPMS1ILSGLIDE_D	,&l_p1_7SldGM	,  48, 159,0,0,icbsldgd,0.25,p1_7); 
MY_SLIDER2	(p1_7SldCR			,P12_NDL_KPPMS1ILSCOURSE_R	,&l_p1_7SldGD	, 147,  79,icbsldcr,0.25,0,0,p1_7); 
MY_SLIDER2	(p1_7SldCP			,P12_NDL_KPPMS1ILSCOURSE_P	,&l_p1_7SldCR	, 147,  79,icbsldcp,0.25,0,0,p1_7); 
MY_SLIDER2	(p1_7SldCM			,P12_NDL_KPPMS1ILSCOURSE_M	,&l_p1_7SldCP	, 147,  79,icbsldcm,0.25,0,0,p1_7); 
MY_SLIDER2	(p1_7SldCD			,P12_NDL_KPPMS1ILSCOURSE_D	,&l_p1_7SldCM	, 147,  79,icbsldcd,0.25,0,0,p1_7); 
MY_ICON2	(p1_7BlkG  			,P12_BLK_KPPMS1GLIDE_D		,&l_p1_7SldCD	, 113, 130,icbblkg				,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7BlkC  			,P12_BLK_KPPMS1COURSE_D		,&l_p1_7BlkG  	, 171, 171,icbblkc				,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7IcoCov			,P12_COV_KPPMS1INTL_D		,&l_p1_7BlkC  	,  78,  94,icbcov_p1_7			,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7Ico			,P12_BACKGROUND_D			,&l_p1_7IcoCov	,   0,   0, icb_Ico				,PANEL_LIGHT_MAX  , p1_7) 
MY_STATIC2	(p1_7bg,p1_7_list	,P12_BACKGROUND_D			,&l_p1_7Ico			, p1_7);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p1_7Spot2			,P12_COV_KPPMS1SPSPOT2_D	,NULL			, 224,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7spot1			,P12_COV_KPPMS1SPSPOT1_D	,&l_p1_7Spot2			,  40,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7knb			,P12_KNB_KPPMS1SP_D_00		,&l_p1_7spot1			, 232, 248,icb_knbp1_7			,20*PANEL_LIGHT_MAX,p1_7)
MY_NEEDLE2	(p1_7NdlHdgP		,P12_NDL_KPPMS1HDG_P		,&l_p1_7knb			, 151, 169, 0, 13,icbndlhdgp	,0,0,p1_7); 
MY_NEEDLE2	(p1_7NdlHdgD		,P12_NDL_KPPMS1HDG_D		,&l_p1_7NdlHdgP		, 151, 169, 0, 13,icbndlhdgd	,0,0,p1_7); 
MY_STATIC2A	(p1_7Alpha			,P12_ALPHA_KPPMS1			,&l_p1_7NdlHdgD		,  25,  39,p1_7); 
MY_SPRITE2	(p1_7ScaleP			,P12_SCL_KPPMS1SP_P			,&l_p1_7Alpha			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleP,-1,p1_7)	
MY_SPRITE2	(p1_7ScaleD			,P12_SCL_KPPMS1SP_D			,&l_p1_7ScaleP			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleD,-1,p1_7)	
MY_SLIDER2	(p1_7SldGP			,P12_NDL_KPPMS1ILSGLIDE_P	,&l_p1_7ScaleD			,  48, 159,0,0,icbsldgp,0.25,p1_7); 
MY_SLIDER2	(p1_7SldGD			,P12_NDL_KPPMS1ILSGLIDE_D	,&l_p1_7SldGP			,  48, 159,0,0,icbsldgd,0.25,p1_7); 
MY_SLIDER2	(p1_7SldCP			,P12_NDL_KPPMS1ILSCOURSE_P	,&l_p1_7SldGD			, 147,  79,icbsldcp,0.25,0,0,p1_7); 
MY_SLIDER2	(p1_7SldCD			,P12_NDL_KPPMS1ILSCOURSE_D	,&l_p1_7SldCP			, 147,  79,icbsldcd,0.25,0,0,p1_7); 
MY_ICON2	(p1_7BlkG  			,P12_BLK_KPPMS1GLIDE_D		,&l_p1_7SldCD			, 113, 130,icbblkg				,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7BlkC  			,P12_BLK_KPPMS1COURSE_D		,&l_p1_7BlkG  			, 171, 171,icbblkc				,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7IcoCov			,P12_COV_KPPMS1INTL_D		,&l_p1_7BlkC  			,  78,  94,icbcov_p1_7			,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7Ico			,P12_BACKGROUND_D			,&l_p1_7IcoCov			,   0,   0, icb_Ico				,PANEL_LIGHT_MAX  , p1_7) 
MY_STATIC2	(p1_7bg,p1_7_list	,P12_BACKGROUND_D			,&l_p1_7Ico				, p1_7);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p1_7Spot2			,P12_COV_KPPMS1SPSPOT2_D	,NULL			, 224,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7spot1			,P12_COV_KPPMS1SPSPOT1_D	,&l_p1_7Spot2			,  40,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7knb			,P12_KNB_KPPMS1SP_D_00		,&l_p1_7spot1			, 232, 248,icb_knbp1_7			,20*PANEL_LIGHT_MAX,p1_7)
MY_NEEDLE2	(p1_7NdlHdgR		,P12_NDL_KPPMS1HDG_R		,&l_p1_7knb			, 151, 169, 0, 13,icbndlhdgr	,0,0,p1_7); 
MY_NEEDLE2	(p1_7NdlHdgD		,P12_NDL_KPPMS1HDG_D		,&l_p1_7NdlHdgR		, 151, 169, 0, 13,icbndlhdgd	,0,0,p1_7); 
MY_STATIC2A	(p1_7Alpha			,P12_ALPHA_KPPMS1			,&l_p1_7NdlHdgD		,  25,  39,p1_7); 
MY_SPRITE2	(p1_7ScaleR			,P12_SCL_KPPMS1SP_R			,&l_p1_7Alpha			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleR,-1,p1_7)	
MY_SPRITE2	(p1_7ScaleD			,P12_SCL_KPPMS1SP_D			,&l_p1_7ScaleR			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleD,-1,p1_7)	
MY_SLIDER2	(p1_7SldGR			,P12_NDL_KPPMS1ILSGLIDE_R	,&l_p1_7ScaleD			,  48, 159,0,0,icbsldgr,0.25,p1_7); 
MY_SLIDER2	(p1_7SldGD			,P12_NDL_KPPMS1ILSGLIDE_D	,&l_p1_7SldGR			,  48, 159,0,0,icbsldgd,0.25,p1_7); 
MY_SLIDER2	(p1_7SldCR			,P12_NDL_KPPMS1ILSCOURSE_R	,&l_p1_7SldGD			, 147,  79,icbsldcr,0.25,0,0,p1_7); 
MY_SLIDER2	(p1_7SldCD			,P12_NDL_KPPMS1ILSCOURSE_D	,&l_p1_7SldCR			, 147,  79,icbsldcd,0.25,0,0,p1_7); 
MY_ICON2	(p1_7BlkG  			,P12_BLK_KPPMS1GLIDE_D		,&l_p1_7SldCD			, 113, 130,icbblkg				,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7BlkC  			,P12_BLK_KPPMS1COURSE_D		,&l_p1_7BlkG  			, 171, 171,icbblkc				,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7IcoCov			,P12_COV_KPPMS1INTL_D		,&l_p1_7BlkC  			,  78,  94,icbcov_p1_7			,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7Ico			,P12_BACKGROUND_D			,&l_p1_7IcoCov			,   0,   0, icb_Ico				,PANEL_LIGHT_MAX  , p1_7) 
MY_STATIC2	(p1_7bg,p1_7_list	,P12_BACKGROUND_D			,&l_p1_7Ico				, p1_7);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p1_7Spot2			,P12_COV_KPPMS1SPSPOT2_D	,NULL			, 224,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7spot1			,P12_COV_KPPMS1SPSPOT1_D	,&l_p1_7Spot2			,  40,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7knb			,P12_KNB_KPPMS1SP_D_00		,&l_p1_7spot1			, 232, 248,icb_knbp1_7			,20*PANEL_LIGHT_MAX,p1_7)
MY_NEEDLE2	(p1_7NdlHdgM		,P12_NDL_KPPMS1HDG_M		,&l_p1_7knb			, 151, 169, 0, 13,icbndlhdgm	,0,0,p1_7); 
MY_NEEDLE2	(p1_7NdlHdgD		,P12_NDL_KPPMS1HDG_D		,&l_p1_7NdlHdgM		, 151, 169, 0, 13,icbndlhdgd	,0,0,p1_7); 
MY_STATIC2A	(p1_7Alpha			,P12_ALPHA_KPPMS1			,&l_p1_7NdlHdgD		,  25,  39,p1_7); 
MY_SPRITE2	(p1_7ScaleM			,P12_SCL_KPPMS1SP_M			,&l_p1_7Alpha			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleM,-1,p1_7)	
MY_SPRITE2	(p1_7ScaleD			,P12_SCL_KPPMS1SP_D			,&l_p1_7ScaleM			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleD,-1,p1_7)	
MY_SLIDER2	(p1_7SldGM			,P12_NDL_KPPMS1ILSGLIDE_M	,&l_p1_7ScaleD			,  48, 159,0,0,icbsldgm,0.25,p1_7); 
MY_SLIDER2	(p1_7SldGD			,P12_NDL_KPPMS1ILSGLIDE_D	,&l_p1_7SldGM			,  48, 159,0,0,icbsldgd,0.25,p1_7); 
MY_SLIDER2	(p1_7SldCM			,P12_NDL_KPPMS1ILSCOURSE_M	,&l_p1_7SldGD			, 147,  79,icbsldcm,0.25,0,0,p1_7); 
MY_SLIDER2	(p1_7SldCD			,P12_NDL_KPPMS1ILSCOURSE_D	,&l_p1_7SldCM			, 147,  79,icbsldcd,0.25,0,0,p1_7); 
MY_ICON2	(p1_7BlkG  			,P12_BLK_KPPMS1GLIDE_D		,&l_p1_7SldCD			, 113, 130,icbblkg				,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7BlkC  			,P12_BLK_KPPMS1COURSE_D		,&l_p1_7BlkG  			, 171, 171,icbblkc				,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7IcoCov			,P12_COV_KPPMS1INTL_D		,&l_p1_7BlkC  			,  78,  94,icbcov_p1_7			,4*PANEL_LIGHT_MAX,p1_7)
MY_ICON2	(p1_7Ico			,P12_BACKGROUND_D			,&l_p1_7IcoCov			,   0,   0, icb_Ico				,PANEL_LIGHT_MAX  , p1_7) 
MY_STATIC2	(p1_7bg,p1_7_list	,P12_BACKGROUND_D			,&l_p1_7Ico				, p1_7);
#endif


#endif
