/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "PM.h"
#include "../../Lib/Undoc.h"

#define WITHCAPS 1

#ifdef WITHCAPS
#define TEXT_01	"ZOOM UP:"
#define TEXT_02 "AUTOPILOT"
#define TEXT_03 "COURSE SYSTEM (GMK)"
#define TEXT_04 "DUAL NEEDLE RMI (IKU)"
#define TEXT_05 "HSI OF CAPTAIN (KPPMS L)"
#define TEXT_06	"HSI OF COPILOT (KPPMS R)"
#define TEXT_07 "ALTIMETER (UVID)"
#define TEXT_08	"AIRSPEED IND (KUS)"
#define TEXT_09 "ATTITUDE IND (AGB)"
#define TEXT_10	"VSI (DA30)"
#define TEXT_11	"CLOCK"
#define TEXT_12	"NAV RADIOS (MP-70)"
#define TEXT_13	"DME IND (SD-75)"
#define TEXT_14 "INDICATION:"
#define TEXT_15 "INTERNATIONAL"
#define TEXT_16	"RUSSIAN"
#define TEXT_17	"LIGHTS"
#define TEXT_18	"GPS KLN-90B"
#else
#define TEXT_01	"Zoom up:"
#define TEXT_02 "Autopilot"
#define TEXT_03 "Course system (GMK)"
#define TEXT_04 "Dual needle RMI (IKU)"
#define TEXT_05 "HSI of captain (KPPMS L)"
#define TEXT_06	"HSI of copilot (KPPMS R)"
#define TEXT_07 "Altimeter (UVID)"
#define TEXT_08	"Airspeed (KUS)"
#define TEXT_09 "Attitude (AGB)"
#define TEXT_10	"VSI (DA30)"
#define TEXT_11	"Clock"
#define TEXT_12	"NAV radios (MP-70)"
#define TEXT_13	"DME (SD-75)"
#define TEXT_14 "Indication:"
#define TEXT_15 "International"
#define TEXT_16	"Russian"
#define TEXT_17	"Lights"
#define TEXT_18	"GPS"
#endif

#define SIZEX	300
#define SIZEY	20
#define NUMCHAR	25
#define OFFY	40
#define OFFX	20
#define CHARTH	FW_NORMAL
#define CHFONT	FONT_GLASS

int		g_iCurrentPanel[2]={0};
UINT	g_iPanRate=700;

FLOAT64 FSAPI scb_zoomup		(PELEMENT_STRING pelement){static bool printed=false; sprintf(pelement->string,TEXT_01); return 0;}
FLOAT64 FSAPI scb_autopilotoff	(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P14)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_02);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_autopiloton	(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P14)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_02);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_gmkoff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P15)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_03);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_gmkon			(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P15)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_03);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_ikuoff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P11)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_04);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_ikuon			(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P11)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_04);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_kppms1off		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P12)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_05);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_kppms1on		(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P12)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_05);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_kppms2off		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P122)){SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_06);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_kppms2on		(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P122)){SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_06);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_uvidoff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P10)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_07);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_uvidon		(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P10)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_07);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_kusoff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P110)){SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_08);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_kuson			(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P110)){SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_08);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_agboff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P17)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_09);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_agbon			(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P17)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_09);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_da30off		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P18)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_10);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_da30on		(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P18)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_10);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_clockoff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P19)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_11);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_clockon		(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P19)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_11);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_navoff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P24)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_12);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_navon			(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P24)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_12);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_dmeoff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P25)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_13);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_dmeon			(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P25)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_13);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_ind			(PELEMENT_STRING pelement){static bool printed=false; sprintf(pelement->string,TEXT_14); return 0;}
FLOAT64 FSAPI scb_intoff		(PELEMENT_STRING pelement){static bool printed=false; if(!POS_GET(POS_PANEL_LANG)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_15);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_inton			(PELEMENT_STRING pelement){static bool printed=false; if( POS_GET(POS_PANEL_LANG)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_15);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_rusoff		(PELEMENT_STRING pelement){static bool printed=false; if( POS_GET(POS_PANEL_LANG)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_16);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_ruson			(PELEMENT_STRING pelement){static bool printed=false; if(!POS_GET(POS_PANEL_LANG)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_16);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_lgtoff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P2)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_17);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_lgton			(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P2)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_17);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_klnoff		(PELEMENT_STRING pelement){static bool printed=false; if(!ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P3)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_18);	} else {HIDE_IMAGE(pelement);}return 0;}
FLOAT64 FSAPI scb_klnon			(PELEMENT_STRING pelement){static bool printed=false; if( ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P3)) {SHOW_IMAGE(pelement); sprintf(pelement->string,TEXT_18);	} else {HIDE_IMAGE(pelement);}return 0;}

double FSAPI icb_autopilot	(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P14)?1:0;};
double FSAPI icb_gmk		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P15)?1:0;};
double FSAPI icb_iku		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P11)?1:0;};
double FSAPI icb_kppms1		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P12)?1:0;};
double FSAPI icb_kppms2		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P122)?1:0;};
double FSAPI icb_uvid		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P10)?1:0;};
double FSAPI icb_kus		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P110)?1:0;};
double FSAPI icb_agb		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P17)?1:0;};
double FSAPI icb_da30		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P18)?1:0;};
double FSAPI icb_clock		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P19)?1:0;};
double FSAPI icb_nav		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P24)?1:0;};
double FSAPI icb_dme		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P25)?1:0;};
double FSAPI icb_int		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return POS_GET(POS_PANEL_LANG)?1:0;};
double FSAPI icb_rus		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return POS_GET(POS_PANEL_LANG)?0:1;};
double FSAPI icb_lgt		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P2)?1:0;};
double FSAPI icb_kln		(PELEMENT_ICON pelement){LIGHT_IMAGE(pelement); return ImportTable.pPanels->is_panel_window_visible_ident(IDENT_P3)?1:0;};

BOOL FSAPI mcb_nav_p0(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_COCKPIT) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P1);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P3);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P4);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P5);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P6);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P7);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P0);
	} else if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_VIRTUAL_COCKPIT) {
		g_iCurrentPanel[0]=0;
	}
	return TRUE;
}

BOOL FSAPI mcb_nav_p1(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_COCKPIT) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P0);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P2);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P4);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P5);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P6);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P7);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P1);
	} else if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_VIRTUAL_COCKPIT) {
		g_iCurrentPanel[0]=1;
	}
	return TRUE;
}

BOOL FSAPI mcb_nav_p4(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_COCKPIT) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P0);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P1);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P2);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P3);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P5);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P6);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P7);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P4);
	} else if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_VIRTUAL_COCKPIT) {
		g_iCurrentPanel[0]=4;
	}
	return TRUE;
}

BOOL FSAPI mcb_nav_p5(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_COCKPIT) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P0);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P1);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P2);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P3);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P4);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P6);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P7);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P5);
	} else if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_VIRTUAL_COCKPIT) {
		g_iCurrentPanel[0]=5;
	}
	return TRUE;
}

BOOL FSAPI mcb_nav_p6(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_COCKPIT) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P0);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P1);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P2);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P3);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P5);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P4);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P7);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P6);
	} else if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_VIRTUAL_COCKPIT) {
		g_iCurrentPanel[0]=6;
	}
	return TRUE;
}

BOOL FSAPI mcb_nav_p7(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_COCKPIT) {
		ImportTable.pPanels->panel_window_close_ident(IDENT_P0);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P1);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P2);
		//ImportTable.pPanels->panel_window_close_ident(IDENT_P3);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P5);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P6);
		ImportTable.pPanels->panel_window_close_ident(IDENT_P4);
		ImportTable.pPanels->panel_window_open_ident(IDENT_P7);
	} else if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_VIRTUAL_COCKPIT) {
		g_iCurrentPanel[0]=7;
	}
	return TRUE;
}

BOOL FSAPI mcb_nav_p8(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_COCKPIT) {
		ImportTable.pPanels->panel_window_toggle(IDENT_P8);
	} else if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_VIRTUAL_COCKPIT) {
		ImportTable.pPanels->panel_window_toggle(IDENT_P8);
		//g_iCurrentPanel[0]=8;
	}
	return TRUE;
}

BOOL FSAPI mcb_nav_p9(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_COCKPIT) {
		ImportTable.pPanels->panel_window_toggle(IDENT_P9);
	} else if(POS_GET(POS_ACTIVE_VIEW_MODE)==VIEW_MODE_VIRTUAL_COCKPIT) {
		ImportTable.pPanels->panel_window_toggle(IDENT_P9);
		//g_iCurrentPanel[0]=9;
	}
	return TRUE;
}

BOOL FSAPI mcb_big_autopilot	(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P14); return TRUE;}
BOOL FSAPI mcb_big_gmk			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P15); return TRUE;}
BOOL FSAPI mcb_big_iku			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P11); return TRUE;}
BOOL FSAPI mcb_big_kppms1		(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P12); return TRUE;}
BOOL FSAPI mcb_big_kppms2		(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P122); return TRUE;}
BOOL FSAPI mcb_big_uvid			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P10); return TRUE;}
BOOL FSAPI mcb_big_kus			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P110); return TRUE;}
BOOL FSAPI mcb_big_agb			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P17); return TRUE;}
BOOL FSAPI mcb_big_da30			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P18); return TRUE;}
BOOL FSAPI mcb_big_clock		(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P19); return TRUE;}
BOOL FSAPI mcb_big_nav			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P24); return TRUE;}
BOOL FSAPI mcb_big_dme			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P25); return TRUE;}
BOOL FSAPI mcb_big_int			(PPIXPOINT relative_point,FLAGS32 mouse_flags){POS_SET(POS_PANEL_LANG,1); return TRUE;}
BOOL FSAPI mcb_big_rus			(PPIXPOINT relative_point,FLAGS32 mouse_flags){POS_SET(POS_PANEL_LANG,0); return TRUE;}
BOOL FSAPI mcb_big_lgt			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P2); return TRUE;}
BOOL FSAPI mcb_big_kln			(PPIXPOINT relative_point,FLAGS32 mouse_flags){ImportTable.pPanels->panel_window_toggle(IDENT_P3); return TRUE;}

static MAKE_TCB(tcb_01  ,POS_TT(POS_NAV_P0))	
static MAKE_TCB(tcb_02  ,POS_TT(POS_NAV_P1))	
static MAKE_TCB(tcb_03  ,POS_TT(POS_NAV_P4))	
static MAKE_TCB(tcb_04  ,POS_TT(POS_NAV_P5))	
static MAKE_TCB(tcb_05  ,POS_TT(POS_NAV_P6))	
static MAKE_TCB(tcb_06  ,POS_TT(POS_NAV_P7))	
static MAKE_TCB(tcb_07  ,POS_TT(POS_NAV_P8))	
static MAKE_TCB(tcb_08  ,POS_TT(POS_NAV_P9))	

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	) 
MAKE_TTA(tcb_02	)
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	)
MAKE_TTA(tcb_08	)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_pm_1,HELP_NONE,0,0) 
MOUSE_TBOX( "1",  96,   63,  34,  30,CURSOR_HAND,MOUSE_LR,mcb_nav_p0)
MOUSE_TBOX( "2", 170,   63,  34,  30,CURSOR_HAND,MOUSE_LR,mcb_nav_p1)
MOUSE_TBOX( "3", 140,   63,  23, 110,CURSOR_HAND,MOUSE_LR,mcb_nav_p4)
MOUSE_TBOX( "4", 140,    2,  23,  30,CURSOR_HAND,MOUSE_LR,mcb_nav_p5)
MOUSE_TBOX( "5",  40,   96,  30,  34,CURSOR_HAND,MOUSE_LR,mcb_nav_p6)
MOUSE_TBOX( "6", 210,   96,  30,  34,CURSOR_HAND,MOUSE_LR,mcb_nav_p7)
MOUSE_TBOX( "7",   2,   90,  24,  26,CURSOR_HAND,MOUSE_LR,mcb_nav_p8)
MOUSE_TBOX( "8", 277,   90,  24,  26,CURSOR_HAND,MOUSE_LR,mcb_nav_p9)
MOUSE_PBOX(        0,  220-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_autopilot	)
MOUSE_PBOX(        0,  240-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_gmk			)
MOUSE_PBOX(        0,  260-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_iku			)
MOUSE_PBOX(        0,  280-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_kppms1		)
MOUSE_PBOX(        0,  300-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_kppms2		)
MOUSE_PBOX(        0,  320-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_uvid		)
MOUSE_PBOX(        0,  340-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_kus			)
MOUSE_PBOX(        0,  360-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_agb			)
MOUSE_PBOX(        0,  380-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_da30		)
MOUSE_PBOX(        0,  400-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_clock		)
MOUSE_PBOX(        0,  420-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_nav			)
MOUSE_PBOX(        0,  440-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_dme			)
MOUSE_PBOX(        0,  460-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_lgt			)
MOUSE_PBOX(        0,  480-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_kln			)
MOUSE_PBOX(        0,  530-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_int			)
MOUSE_PBOX(        0,  550-OFFY,  MISC_NAV_SX,  20,CURSOR_HAND,MOUSE_LR,mcb_big_rus			)
MOUSE_END 

void FSAPI pm1_cb( PGAUGEHDR pgauge, SINT32 service_id, UINT32 extra_data ) 
{
	switch(service_id) {
		case PANEL_SERVICE_POST_INITIALIZE:	
			UndocReset();
			break;
		case PANEL_SERVICE_PRE_UPDATE:
			UndocUpdate();
		break;
		case PANEL_SERVICE_PRE_DRAW:
			if(g_iCurrentPanel[0]!=g_iCurrentPanel[1]) {
				switch(g_iCurrentPanel[0]) {
		case 0:
			ViewMoveTo(0,0,0);
			ViewPanReset();
			g_iCurrentPanel[1]=g_iCurrentPanel[0];
			break;
		case 1:
			ViewMoveTo(240,0,0);
			ViewPanReset();
			g_iCurrentPanel[1]=g_iCurrentPanel[0];
			break;
		case 4:
			ViewMoveTo(120,0,0);
			ViewPanReset();
			/*
			if(g_iCurrentPanel[1]==0||g_iCurrentPanel[1]==1) {
			for(int i=0;i<int((1000-g_iPanRate)/24);i++)
			ImportTable.pPanels->send_key_event(KEY_PAN_DOWN,0); //ViewPanDown();
			} else {
			for(int i=0;i<int((1000-g_iPanRate)/20);i++)
			ImportTable.pPanels->send_key_event(KEY_PAN_DOWN,0); //ViewPanDown();
			}
			*/
			g_iCurrentPanel[1]=g_iCurrentPanel[0];
			break;
		case 5:
			ViewMoveTo(120,-43,42);
			ViewPanReset();
			/*
			if(g_iCurrentPanel[1]==0||g_iCurrentPanel[1]==1) {
			for(int i=0;i<int((1000-g_iPanRate)/24);i++)
			ImportTable.pPanels->send_key_event(KEY_PAN_UP,0); //ViewPanUp();
			} else {
			for(int i=0;i<int((1000-g_iPanRate)/20);i++)
			ImportTable.pPanels->send_key_event(KEY_PAN_UP,0); //ViewPanUp();
			}
			*/
			g_iCurrentPanel[1]=g_iCurrentPanel[0];
			break;
		case 6:
			ViewMoveTo(0,0,0);
			ViewPanReset();
			/*
			if(g_iCurrentPanel[1]==0||g_iCurrentPanel[1]==1) {
			for(int i=0;i<int((1000-g_iPanRate)/6);i++)
			ViewPanLeft();
			for(int i=0;i<int((1000-g_iPanRate)/14);i++)
			ViewPanDown();
			} else {
			for(int i=0;i<int((1000-g_iPanRate)/6+(1000-g_iPanRate)/10);i++)
			ViewPanLeft();
			for(int i=0;i<int((1000-g_iPanRate)/9);i++)
			ViewPanDown();
			}
			*/
			g_iCurrentPanel[1]=g_iCurrentPanel[0];
			break;
		case 7:
			ViewMoveTo(240,0,0);
			ViewPanReset();
			/*
			if(g_iCurrentPanel[1]==0||g_iCurrentPanel[1]==1) {
			for(int i=0;i<int((1000-g_iPanRate)/7);i++)
			ViewPanRight();
			for(int i=0;i<int((1000-g_iPanRate)/14);i++)
			ViewPanDown();
			} else {
			for(int i=0;i<int((1000-g_iPanRate)/7+(1000-g_iPanRate)/10);i++)
			ViewPanRight();
			for(int i=0;i<int((1000-g_iPanRate)/10);i++)
			ViewPanDown();
			}
			*/
			g_iCurrentPanel[1]=g_iCurrentPanel[0];
			break;
		case 8:
			ViewMoveTo(-20,20,0);
			ViewPanReset();
			/*
			if(g_iCurrentPanel[1]==0||g_iCurrentPanel[1]==1) {
			for(int i=0;i<int((1000-g_iPanRate)/4);i++)
			ViewPanLeft();
			for(int i=0;i<int((1000-g_iPanRate)/14);i++)
			ViewPanDown();
			} else {
			for(int i=0;i<int((1000-g_iPanRate)/4+(1000-g_iPanRate)/6);i++)
			ViewPanLeft();
			for(int i=0;i<int((1000-g_iPanRate)/10);i++)
			ViewPanDown();
			}
			*/
			g_iCurrentPanel[1]=g_iCurrentPanel[0];
			break;
		case 9:
			ViewMoveTo(260,20,0);
			ViewPanReset();
			/*
			if(g_iCurrentPanel[1]==0||g_iCurrentPanel[1]==1) {
			for(int i=0;i<int((1000-g_iPanRate)/4);i++)
			ViewPanRight();
			for(int i=0;i<int((1000-g_iPanRate)/14);i++)
			ViewPanDown();
			} else {
			for(int i=0;i<int((1000-g_iPanRate)/4+(1000-g_iPanRate)/6);i++)
			ViewPanRight();
			for(int i=0;i<int((1000-g_iPanRate)/10);i++)
			ViewPanDown();
			}
			*/
			g_iCurrentPanel[1]=g_iCurrentPanel[0];
			break;
				}
			}
			break;
	}
}


extern PELEMENT_HEADER pm_1_list; 
GAUGE_HEADER_FS700_EX(MISC_NAV_SX, "pm_1", &pm_1_list, rect_pm_1, pm1_cb, 0, 0, 0, pm_1); 

MY_STRING2	(str_zoomup   						,NULL				,OFFX,200-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GRAY	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_zoomup			, pm_1)
MY_STRING2	(str_autopilotoff					,&l_str_zoomup   	,OFFX,220-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_autopilotoff	, pm_1)
MY_STRING2	(str_autopiloton 					,&l_str_autopilotoff,OFFX,220-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_autopiloton	, pm_1)
MY_STRING2	(str_gmkoff      					,&l_str_autopiloton ,OFFX,240-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_gmkoff		, pm_1)
MY_STRING2	(str_gmkon       					,&l_str_gmkoff      ,OFFX,240-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_gmkon			, pm_1)  
MY_STRING2	(str_ikuoff      					,&l_str_gmkon       ,OFFX,260-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_ikuoff		, pm_1)
MY_STRING2	(str_ikuon       					,&l_str_ikuoff      ,OFFX,260-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_ikuon			, pm_1)
MY_STRING2	(str_kppms1off    					,&l_str_ikuon       ,OFFX,280-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_kppms1off		, pm_1)
MY_STRING2	(str_kppms1on     					,&l_str_kppms1off   ,OFFX,280-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_kppms1on		, pm_1)
MY_STRING2	(str_kppms2off    					,&l_str_kppms1on    ,OFFX,300-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_kppms2off		, pm_1)
MY_STRING2	(str_kppms2on     					,&l_str_kppms2off   ,OFFX,300-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_kppms2on		, pm_1)
MY_STRING2	(str_uvidoff    					,&l_str_kppms2on    ,OFFX,320-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_uvidoff		, pm_1)
MY_STRING2	(str_uvidon     					,&l_str_uvidoff    	,OFFX,320-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_uvidon		, pm_1)
MY_STRING2	(str_kusoff      					,&l_str_uvidon     	,OFFX,340-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_kusoff		, pm_1)
MY_STRING2	(str_kuson       					,&l_str_kusoff      ,OFFX,340-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_kuson			, pm_1)
MY_STRING2	(str_agboff      					,&l_str_kuson       ,OFFX,360-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_agboff		, pm_1)
MY_STRING2	(str_agbon       					,&l_str_agboff      ,OFFX,360-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_agbon			, pm_1)
MY_STRING2	(str_da30off      					,&l_str_agbon       ,OFFX,380-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_da30off		, pm_1)
MY_STRING2	(str_da30on       					,&l_str_da30off     ,OFFX,380-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_da30on		, pm_1)
MY_STRING2	(str_clockoff      					,&l_str_da30on      ,OFFX,400-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_clockoff		, pm_1)
MY_STRING2	(str_clockon						,&l_str_clockoff    ,OFFX,400-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_clockon		, pm_1)
MY_STRING2	(str_navoff      					,&l_str_clockon		,OFFX,420-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_navoff		, pm_1)
MY_STRING2	(str_navon							,&l_str_navoff      ,OFFX,420-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_navon			, pm_1)
MY_STRING2	(str_dmeoff      					,&l_str_navon		,OFFX,440-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_dmeoff		, pm_1)
MY_STRING2	(str_dmeon							,&l_str_dmeoff      ,OFFX,440-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_dmeon			, pm_1)
MY_STRING2	(str_lgtoff      					,&l_str_dmeon		,OFFX,460-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_lgtoff		, pm_1)
MY_STRING2	(str_lgton							,&l_str_lgtoff      ,OFFX,460-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_lgton			, pm_1)
MY_STRING2	(str_klnoff      					,&l_str_lgton		,OFFX,480-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_klnoff		, pm_1)
MY_STRING2	(str_klnon							,&l_str_klnoff      ,OFFX,480-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_klnon			, pm_1)
MY_STRING2	(str_ind      						,&l_str_klnon		,OFFX,510-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GRAY		,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_ind			, pm_1)
MY_STRING2	(str_intoff      					,&l_str_ind      	,OFFX,530-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_intoff		, pm_1)
MY_STRING2	(str_inton							,&l_str_intoff      ,OFFX,530-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_inton			, pm_1)
MY_STRING2	(str_rusoff      					,&l_str_inton		,OFFX,550-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_ORANGE	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_rusoff		, pm_1)
MY_STRING2	(str_ruson							,&l_str_rusoff      ,OFFX,550-OFFY,SIZEX,SIZEY,NUMCHAR,MODULE_VAR_NONE,MODULE_VAR_NONE,MODULE_VAR_NONE,CL_GREEN 	,CL_TRANSP,CL_AMBRA,CHFONT,CHARTH,DEFAULT_CHARSET,0,DT_VCENTER|DT_SINGLELINE|DT_LEFT,NULL,scb_ruson			, pm_1)
MY_ICON2	(ico_autopilot		,MISC_DOT_00	,&l_str_ruson		,0,220-OFFY+1,icb_autopilot	,2, pm_1)																																										
MY_ICON2	(ico_gmk			,MISC_DOT_00	,&l_ico_autopilot	,0,240-OFFY+1,icb_gmk		,2, pm_1)																																										
MY_ICON2	(ico_iku			,MISC_DOT_00	,&l_ico_gmk			,0,260-OFFY+1,icb_iku		,2, pm_1)
MY_ICON2	(ico_kppms1			,MISC_DOT_00	,&l_ico_iku			,0,280-OFFY+1,icb_kppms1	,2, pm_1)
MY_ICON2	(ico_kppms2			,MISC_DOT_00	,&l_ico_kppms1		,0,300-OFFY+1,icb_kppms2	,2, pm_1)
MY_ICON2	(ico_uvid			,MISC_DOT_00	,&l_ico_kppms2		,0,320-OFFY+1,icb_uvid		,2, pm_1)
MY_ICON2	(ico_kus			,MISC_DOT_00	,&l_ico_uvid		,0,340-OFFY+1,icb_kus		,2, pm_1)
MY_ICON2	(ico_agb			,MISC_DOT_00	,&l_ico_kus			,0,360-OFFY+1,icb_agb		,2, pm_1)
MY_ICON2	(ico_da30			,MISC_DOT_00	,&l_ico_agb			,0,380-OFFY+1,icb_da30		,2, pm_1)
MY_ICON2	(ico_clock			,MISC_DOT_00	,&l_ico_da30		,0,400-OFFY+1,icb_clock		,2, pm_1)
MY_ICON2	(ico_nav			,MISC_DOT_00	,&l_ico_clock		,0,420-OFFY+1,icb_nav		,2, pm_1)
MY_ICON2	(ico_dme			,MISC_DOT_00	,&l_ico_nav			,0,440-OFFY+1,icb_dme		,2, pm_1)
MY_ICON2	(ico_lgt			,MISC_DOT_00	,&l_ico_dme			,0,460-OFFY+1,icb_lgt		,2, pm_1)
MY_ICON2	(ico_kln			,MISC_DOT_00	,&l_ico_lgt			,0,480-OFFY+1,icb_kln		,2, pm_1)
MY_ICON2	(ico_int			,MISC_DOT_00	,&l_ico_kln			,0,530-OFFY+1,icb_int		,2, pm_1)
MY_ICON2	(ico_rus			,MISC_DOT_00	,&l_ico_int			,0,550-OFFY+1,icb_rus		,2, pm_1)
MY_STATIC2B	(pm_1bg,pm_1_list	,MISC_NAV		,&l_ico_rus			, pm_1)


