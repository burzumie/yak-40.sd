/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Visual/p0/P0.h $

  Last modification:
    $Date: 19.02.06 10:23 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include "../../Common/CommonSys.h"
#include "../../Common/CommonVisual.h"
#include "../../Common/Macros.h"
#include "../../Common/MacrosVisual.h"
#include "../../../res/Version.h"
#include "../../Common/CommonAircraft.h"
#include "../../api/ClientDef.h"
#include "../../api/APIEntry.h"
#include "../../Lib/PanelsTools.h"

#include "../../../res/2d/pm_res.h"

GAUGE(pm_1)

