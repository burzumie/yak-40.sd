#pragma once
#include "afxwin.h"


// CMessageDialog dialog

class CMessageDialog : public CDialog
{
	DECLARE_DYNAMIC(CMessageDialog)

public:
	CMessageDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMessageDialog();

// Dialog Data
	enum { IDD = IDD_MESSAGEFORM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	void Update();
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
//	afx_msg void OnStnClickedMessageheader();
	CStatic cstHeader;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
