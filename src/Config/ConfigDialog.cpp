// ConfigDialog.cpp : implementation file
//

#include "stdafx.h"
#include "Config.h"
#include "ConfigDialog.h"
#include ".\configdialog.h"
#include "../Common/CommonLogic.h"
#include "../Api/SimVars.h"
#include "../Api/Api.h"
#include "../Api/ClientDef.h"
#include "../Common/CommonAircraft.h"

/*
#define WS_EX_LAYERED     0x80000
#define LWA_ALPHA         0x02
BOOL (WINAPI* SetLayeredWindowAttributes)(HWND,int,int,DWORD);
*/

extern SDSimVars *g_pSimVars;

// CConfigDialog dialog

IMPLEMENT_DYNAMIC(CConfigDialog, CDialog)
CConfigDialog::CConfigDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDialog::IDD, pParent)
{
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon       = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	//��������� ���� �������
	m_hCursorDown = AfxGetApp()->LoadCursor(IDC_HAND_MOVE);
	m_hCursorUp   = AfxGetApp()->LoadCursor(IDC_HAND_POINT);
	//�������������� ������
	m_bMoveWindow = FALSE;

}

CConfigDialog::~CConfigDialog()
{
}

void CConfigDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDEREQUIP, m_SliderEquip);
	DDX_Control(pDX, IDC_SLIDERCARGO, m_SliderCargo);
	DDX_Control(pDX, IDC_SLIDERLEFTTANK, m_SliderLeftTank);
	DDX_Control(pDX, IDC_SLIDERRIGHTTANK, m_SliderRightTank);
	DDX_Control(pDX, IDC_EDITEQUIP, m_EditEquip);
	DDX_Control(pDX, IDC_EDITCARGO, m_EditCargo);
	DDX_Control(pDX, IDC_EDITLEFTTANK, m_EditLeftTank);
	DDX_Control(pDX, IDC_EDITRIGHTTANK, m_EditRightTank);
	DDX_Control(pDX, IDC_EDITTOTALW, m_EditTotalWeight);
	DDX_Control(pDX, IDC_EDITMAXW, m_EditMaxWeight);
	DDX_Control(pDX, IDC_EDITCURCG, m_EditCurrentCg);
	DDX_Control(pDX, IDC_EDITMINCG, m_EditMinimumCg);
	DDX_Control(pDX, IDC_EDITMAXCG, m_EditMaximumCg);
	DDX_Control(pDX, IDC_CHKPILOT    , m_cbPilot);
	DDX_Control(pDX, IDC_CHKCOPILOT  , m_cbCopilot);
	DDX_Control(pDX, IDC_CHKENGINEER , m_cbEngineer2);
	DDX_Control(pDX, IDC_CHKROW1SEAT1, m_cbRow1Seat1);
	DDX_Control(pDX, IDC_CHKROW1SEAT2, m_cbRow1Seat2);
	DDX_Control(pDX, IDC_CHKROW1SEAT3, m_cbRow1Seat3);
	DDX_Control(pDX, IDC_CHKROW2SEAT1, m_cbRow2Seat1);
	DDX_Control(pDX, IDC_CHKROW2SEAT2, m_cbRow2Seat2);
	DDX_Control(pDX, IDC_CHKROW2SEAT3, m_cbRow2Seat3);
	DDX_Control(pDX, IDC_CHKROW3SEAT1, m_cbRow3Seat1);
	DDX_Control(pDX, IDC_CHKROW3SEAT2, m_cbRow3Seat2);
	DDX_Control(pDX, IDC_CHKROW3SEAT3, m_cbRow3Seat3);
	DDX_Control(pDX, IDC_CHKROW4SEAT1, m_cbRow4Seat1);
	DDX_Control(pDX, IDC_CHKROW4SEAT2, m_cbRow4Seat2);
	DDX_Control(pDX, IDC_CHKROW4SEAT3, m_cbRow4Seat3);
	DDX_Control(pDX, IDC_CHKROW5SEAT1, m_cbRow5Seat1);
	DDX_Control(pDX, IDC_CHKROW5SEAT2, m_cbRow5Seat2);
	DDX_Control(pDX, IDC_CHKROW5SEAT3, m_cbRow5Seat3);
	DDX_Control(pDX, IDC_CHKROW6SEAT1, m_cbRow6Seat1);
	DDX_Control(pDX, IDC_CHKROW6SEAT2, m_cbRow6Seat2);
	DDX_Control(pDX, IDC_CHKROW6SEAT3, m_cbRow6Seat3);
	DDX_Control(pDX, IDC_CHKROW7SEAT1, m_cbRow7Seat1);
	DDX_Control(pDX, IDC_CHKROW7SEAT2, m_cbRow7Seat2);
	DDX_Control(pDX, IDC_CHKROW7SEAT3, m_cbRow7Seat3);
	DDX_Control(pDX, IDC_CHKROW8SEAT1, m_cbRow8Seat1);
	DDX_Control(pDX, IDC_CHKROW8SEAT2, m_cbRow8Seat2);
	DDX_Control(pDX, IDC_CHKROW8SEAT3, m_cbRow8Seat3);
	DDX_Control(pDX, IDC_CHKROW9SEAT1, m_cbRow9Seat1);
	DDX_Control(pDX, IDC_CHKROW9SEAT2, m_cbRow9Seat2);
	DDX_Control(pDX, IDC_CHKROW9SEAT3, m_cbRow9Seat3);
	DDX_Control(pDX, IDC_CHKRAP, m_cbExtPwr);
	DDX_Control(pDX, IDC_CHKAIR, m_cbExtAir);
}


BEGIN_MESSAGE_MAP(CConfigDialog, CDialog)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_SETCURSOR()
	ON_BN_CLICKED(IDC_CHKPILOT, OnBnClickedChkpilot)
	ON_BN_CLICKED(IDC_CHKCOPILOT, OnBnClickedChkcopilot)
	ON_BN_CLICKED(IDC_CHKENGINEER, OnBnClickedChkengineer)
	ON_BN_CLICKED(IDC_CHKROW1SEAT1, OnBnClickedChkrow1seat1)
	ON_BN_CLICKED(IDC_CHKROW1SEAT2, OnBnClickedChkrow1seat2)
	ON_BN_CLICKED(IDC_CHKROW1SEAT3, OnBnClickedChkrow1seat3)
	ON_BN_CLICKED(IDC_CHKROW2SEAT1, OnBnClickedChkrow2seat1)
	ON_BN_CLICKED(IDC_CHKROW2SEAT2, OnBnClickedChkrow2seat2)
	ON_BN_CLICKED(IDC_CHKROW2SEAT3, OnBnClickedChkrow2seat3)
	ON_BN_CLICKED(IDC_CHKROW3SEAT1, OnBnClickedChkrow3seat1)
	ON_BN_CLICKED(IDC_CHKROW3SEAT2, OnBnClickedChkrow3seat2)
	ON_BN_CLICKED(IDC_CHKROW3SEAT3, OnBnClickedChkrow3seat3)
	ON_BN_CLICKED(IDC_CHKROW4SEAT1, OnBnClickedChkrow4seat1)
	ON_BN_CLICKED(IDC_CHKROW4SEAT2, OnBnClickedChkrow4seat2)
	ON_BN_CLICKED(IDC_CHKROW4SEAT3, OnBnClickedChkrow4seat3)
	ON_BN_CLICKED(IDC_CHKROW5SEAT1, OnBnClickedChkrow5seat1)
	ON_BN_CLICKED(IDC_CHKROW5SEAT2, OnBnClickedChkrow5seat2)
	ON_BN_CLICKED(IDC_CHKROW5SEAT3, OnBnClickedChkrow5seat3)
	ON_BN_CLICKED(IDC_CHKROW6SEAT1, OnBnClickedChkrow6seat1)
	ON_BN_CLICKED(IDC_CHKROW6SEAT2, OnBnClickedChkrow6seat2)
	ON_BN_CLICKED(IDC_CHKROW6SEAT3, OnBnClickedChkrow6seat3)
	ON_BN_CLICKED(IDC_CHKROW7SEAT1, OnBnClickedChkrow7seat1)
	ON_BN_CLICKED(IDC_CHKROW7SEAT2, OnBnClickedChkrow7seat2)
	ON_BN_CLICKED(IDC_CHKROW7SEAT3, OnBnClickedChkrow7seat3)
	ON_BN_CLICKED(IDC_CHKROW8SEAT1, OnBnClickedChkrow8seat1)
	ON_BN_CLICKED(IDC_CHKROW8SEAT2, OnBnClickedChkrow8seat2)
	ON_BN_CLICKED(IDC_CHKROW8SEAT3, OnBnClickedChkrow8seat3)
	ON_BN_CLICKED(IDC_CHKROW9SEAT1, OnBnClickedChkrow9seat1)
	ON_BN_CLICKED(IDC_CHKROW9SEAT2, OnBnClickedChkrow9seat2)
	ON_BN_CLICKED(IDC_CHKROW9SEAT3, OnBnClickedChkrow9seat3)
	ON_BN_CLICKED(IDC_BTNPAXFULL, OnBnClickedBtnpaxfull)
	ON_BN_CLICKED(IDC_BTNPAXEMPTY, OnBnClickedBtnpaxempty)
	ON_BN_CLICKED(IDC_BTNPAX50PCT, OnBnClickedBtnpax50pct)
	ON_BN_CLICKED(IDC_BTNPAX25PCT, OnBnClickedBtnpax25pct)
	ON_BN_CLICKED(IDC_BTNPAX75PCT, OnBnClickedBtnpax75pct)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTNCLOSE, OnBnClickedBtnclose)
	ON_WM_KEYDOWN()
	ON_BN_CLICKED(IDC_CHKRAP, OnBnClickedChkrap)
	ON_BN_CLICKED(IDC_CHKAIR, OnBnClickedChkair)
	ON_STN_CLICKED(IDC_SALON27, OnStnClickedSalon27)
END_MESSAGE_MAP()


// CConfigDialog message handlers

#define PILOTS_WEIGHT	176.37
#define PAX_WEIGHT		165.36

BOOL CConfigDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);                                         // Set big icon
	SetIcon(m_hIcon, FALSE);                        // Set small icon

	m_hCursor = m_hCursorUp;
	::SetCursor(m_hCursor);

	m_SliderEquip.SetRange(0,200);
	m_SliderCargo.SetRange(0,275);
	m_SliderLeftTank.SetRange(0,2000);
	m_SliderRightTank.SetRange(0,2000);

	m_SliderLeftTank.SetPos(int(GetLeftTankQuantityKg()));
	m_SliderRightTank.SetPos(int(GetRightTankQuantityKg()));
	m_SliderEquip.SetPos(int(GetPayloadWeightKg(SEAT_EQUIP)));
	m_SliderCargo.SetPos(int(GetPayloadWeightKg(SEAT_CARGO)));

	m_SEPosOld=m_SliderEquip.GetPos();
	m_SCPosOld=m_SliderCargo.GetPos();
	m_TLPosOld=m_SliderLeftTank.GetPos();
	m_TRPosOld=m_SliderRightTank.GetPos();

	m_cbExtPwr.SetCheck(AZS_GET(AZS_EXT_PWR));
	m_cbExtAir.SetCheck(AZS_GET(AZS_EXT_AIR));

	SetStatePax();

	VCAnimStairs=std::auto_ptr<CNamedVar>(new CNamedVar("sd6015_stairs",ImportTable.pPanels,false));

	return TRUE;  // return TRUE unless you set the focus to a control
}

void CConfigDialog::SetStatePax()
{
	m_cbPilot.SetCheck    (GetPayloadWeightKg(SEAT_PILOT)     ?BST_CHECKED:BST_UNCHECKED);
	m_cbCopilot.SetCheck  (GetPayloadWeightKg(SEAT_COPILOT)   ?BST_CHECKED:BST_UNCHECKED);
	m_cbEngineer2.SetCheck(GetPayloadWeightKg(SEAT_ENGINEER)  ?BST_CHECKED:BST_UNCHECKED);
	m_cbRow1Seat1.SetCheck(GetPayloadWeightKg(SEAT_ROW1_SEAT1)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow1Seat2.SetCheck(GetPayloadWeightKg(SEAT_ROW1_SEAT2)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow1Seat3.SetCheck(GetPayloadWeightKg(SEAT_ROW1_SEAT3)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow2Seat1.SetCheck(GetPayloadWeightKg(SEAT_ROW2_SEAT1)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow2Seat2.SetCheck(GetPayloadWeightKg(SEAT_ROW2_SEAT2)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow2Seat3.SetCheck(GetPayloadWeightKg(SEAT_ROW2_SEAT3)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow3Seat1.SetCheck(GetPayloadWeightKg(SEAT_ROW3_SEAT1)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow3Seat2.SetCheck(GetPayloadWeightKg(SEAT_ROW3_SEAT2)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow3Seat3.SetCheck(GetPayloadWeightKg(SEAT_ROW3_SEAT3)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow4Seat1.SetCheck(GetPayloadWeightKg(SEAT_ROW4_SEAT1)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow4Seat2.SetCheck(GetPayloadWeightKg(SEAT_ROW4_SEAT2)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow4Seat3.SetCheck(GetPayloadWeightKg(SEAT_ROW4_SEAT3)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow5Seat1.SetCheck(GetPayloadWeightKg(SEAT_ROW5_SEAT1)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow5Seat2.SetCheck(GetPayloadWeightKg(SEAT_ROW5_SEAT2)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow5Seat3.SetCheck(GetPayloadWeightKg(SEAT_ROW5_SEAT3)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow6Seat1.SetCheck(GetPayloadWeightKg(SEAT_ROW6_SEAT1)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow6Seat2.SetCheck(GetPayloadWeightKg(SEAT_ROW6_SEAT2)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow6Seat3.SetCheck(GetPayloadWeightKg(SEAT_ROW6_SEAT3)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow7Seat1.SetCheck(GetPayloadWeightKg(SEAT_ROW7_SEAT1)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow7Seat2.SetCheck(GetPayloadWeightKg(SEAT_ROW7_SEAT2)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow7Seat3.SetCheck(GetPayloadWeightKg(SEAT_ROW7_SEAT3)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow8Seat1.SetCheck(GetPayloadWeightKg(SEAT_ROW8_SEAT1)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow8Seat2.SetCheck(GetPayloadWeightKg(SEAT_ROW8_SEAT2)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow8Seat3.SetCheck(GetPayloadWeightKg(SEAT_ROW8_SEAT3)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow9Seat1.SetCheck(GetPayloadWeightKg(SEAT_ROW9_SEAT1)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow9Seat2.SetCheck(GetPayloadWeightKg(SEAT_ROW9_SEAT2)?BST_CHECKED:BST_UNCHECKED);
	m_cbRow9Seat3.SetCheck(GetPayloadWeightKg(SEAT_ROW9_SEAT3)?BST_CHECKED:BST_UNCHECKED);
}

void CConfigDialog::Update()
{
	CString str;

	SetStatePax();

	if(g_pSimVars->GetN(AIRCRAFT_ON_GROUND)&&VCAnimStairs->get_bool()) {
		int se=m_SliderEquip.GetPos();
		if(se!=m_SEPosOld) {
			SetPayloadWeightKg(SEAT_EQUIP,m_SliderEquip.GetPos());
			m_SEPosOld=se;
		}
		int sc=m_SliderCargo.GetPos();
		if(sc!=m_SCPosOld) {
			SetPayloadWeightKg(SEAT_CARGO,m_SliderCargo.GetPos());
			m_SCPosOld=sc;
		}
		int tl=m_SliderLeftTank.GetPos();
		if(tl!=m_TLPosOld) {
			SetLeftTankQuantityKg(m_SliderLeftTank.GetPos());
			m_TLPosOld=tl;
		}
		int tr=m_SliderRightTank.GetPos();
		if(tr!=m_TRPosOld) {
			SetRightTankQuantityKg(m_SliderRightTank.GetPos());
			m_TRPosOld=tr;
		}
	} else {
		m_SliderLeftTank.SetPos(int(GetLeftTankQuantityKg()));
		m_SliderRightTank.SetPos(int(GetRightTankQuantityKg()));
		m_SliderEquip.SetPos(int(GetPayloadWeightKg(SEAT_EQUIP)));
		m_SliderCargo.SetPos(int(GetPayloadWeightKg(SEAT_CARGO)));
	}

	CWnd* pWnd = GetDlgItem(IDC_EDITEQUIP);
	str.Format("%.2f",GetPayloadWeightKg(SEAT_EQUIP));
	pWnd->SetWindowText(str);

	pWnd = GetDlgItem(IDC_EDITCARGO);
	str.Format("%.2f",GetPayloadWeightKg(SEAT_CARGO));
	pWnd->SetWindowText(str);

	pWnd = GetDlgItem(IDC_EDITLEFTTANK);
	str.Format("%.2f",GetLeftTankQuantityKg());
	pWnd->SetWindowText(str);

	pWnd = GetDlgItem(IDC_EDITRIGHTTANK);
	str.Format("%.2f",GetRightTankQuantityKg());
	pWnd->SetWindowText(str);

	double M=0,S=0,X=0;
	double Mtot=0,Stot=0;

	for(unsigned int i=0;i<GetPayloadNumberOfStations();i++) {
		X=GetPayloadPosLat(i);
		M=GetPayloadWeightPound(i);
		S=X*M;

		Mtot+=M;
		Stot+=S;
	}

	double Xempty=GetEmptyCgPosLat();
	double Mempty=GetEmptyWeightPound();
	double Sempty=Xempty*Mempty;

	Mtot+=Mempty;
	Stot+=Sempty;

	double fuellbs=GetLeftTankQuantityKg()+GetRightTankQuantityKg();
	double Mfuel=fuellbs*2.2046226;
	double Xfuel=GetLeftTankPosLat(); 

	double Sfuel=Mfuel*Xfuel;


	Mtot+=Mfuel;
	Stot+=Sfuel;

	pWnd = GetDlgItem(IDC_EDITTOTALW);
	double totalw=Mtot/2.2046226;
	m_fTotalWeight=totalw;
	str.Format("%.2f",totalw);
	pWnd->SetWindowText(str);

	double X_cg=Stot/Mtot;
	double CG=100*(2.425-X_cg)/9.7;
	m_fTotalCG=CG;

	pWnd = GetDlgItem(IDC_EDITCURCG);
	str.Format("%.2f",CG);
	pWnd->SetWindowText(str);

	pWnd = GetDlgItem(IDC_EDITMAXW);
	str.Format("%.2f",GetMaxGrossWeightKg());
	pWnd->SetWindowText(str);

	pWnd = GetDlgItem(IDC_EDITMINCG);
	pWnd->SetWindowText("13");

	pWnd = GetDlgItem(IDC_EDITMAXCG);
	pWnd->SetWindowText("32");

	CWnd* pWnd1 = GetDlgItem(IDC_SLIDEREQUIP);
	CWnd* pWnd2 = GetDlgItem(IDC_SLIDERCARGO);
	CWnd* pWnd3 = GetDlgItem(IDC_SLIDERLEFTTANK);
	CWnd* pWnd4 = GetDlgItem(IDC_SLIDERRIGHTTANK);
	CWnd* pWnd5 = GetDlgItem(IDC_BTNPAXFULL);
	CWnd* pWnd6 = GetDlgItem(IDC_BTNPAX75PCT);
	CWnd* pWnd7 = GetDlgItem(IDC_BTNPAX50PCT);
	CWnd* pWnd8 = GetDlgItem(IDC_BTNPAX25PCT);
	CWnd* pWnd9 = GetDlgItem(IDC_BTNPAXEMPTY);
	CWnd* pWnd10 = GetDlgItem(IDC_CHKRAP);
	CWnd* pWnd11 = GetDlgItem(IDC_CHKAIR);

	CWnd* pWndM[30];

	pWndM[ 0]=GetDlgItem(IDC_CHKPILOT    );
	pWndM[ 1]=GetDlgItem(IDC_CHKCOPILOT  );
	pWndM[ 2]=GetDlgItem(IDC_CHKENGINEER );
	pWndM[ 3]=GetDlgItem(IDC_CHKROW1SEAT1);
	pWndM[ 4]=GetDlgItem(IDC_CHKROW1SEAT2);
	pWndM[ 5]=GetDlgItem(IDC_CHKROW1SEAT3);
	pWndM[ 6]=GetDlgItem(IDC_CHKROW2SEAT1);
	pWndM[ 7]=GetDlgItem(IDC_CHKROW2SEAT2);
	pWndM[ 8]=GetDlgItem(IDC_CHKROW2SEAT3);
	pWndM[ 9]=GetDlgItem(IDC_CHKROW3SEAT1);
	pWndM[10]=GetDlgItem(IDC_CHKROW3SEAT2);
	pWndM[11]=GetDlgItem(IDC_CHKROW3SEAT3);
	pWndM[12]=GetDlgItem(IDC_CHKROW4SEAT1);
	pWndM[13]=GetDlgItem(IDC_CHKROW4SEAT2);
	pWndM[14]=GetDlgItem(IDC_CHKROW4SEAT3);
	pWndM[15]=GetDlgItem(IDC_CHKROW5SEAT1);
	pWndM[16]=GetDlgItem(IDC_CHKROW5SEAT2);
	pWndM[17]=GetDlgItem(IDC_CHKROW5SEAT3);
	pWndM[18]=GetDlgItem(IDC_CHKROW6SEAT1);
	pWndM[19]=GetDlgItem(IDC_CHKROW6SEAT2);
	pWndM[20]=GetDlgItem(IDC_CHKROW6SEAT3);
	pWndM[21]=GetDlgItem(IDC_CHKROW7SEAT1);
	pWndM[22]=GetDlgItem(IDC_CHKROW7SEAT2);
	pWndM[23]=GetDlgItem(IDC_CHKROW7SEAT3);
	pWndM[24]=GetDlgItem(IDC_CHKROW8SEAT1);
	pWndM[25]=GetDlgItem(IDC_CHKROW8SEAT2);
	pWndM[26]=GetDlgItem(IDC_CHKROW8SEAT3);
	pWndM[27]=GetDlgItem(IDC_CHKROW9SEAT1);
	pWndM[28]=GetDlgItem(IDC_CHKROW9SEAT2);
	pWndM[29]=GetDlgItem(IDC_CHKROW9SEAT3);

	if(g_pSimVars->GetN(AIRCRAFT_ON_GROUND)) {
		pWnd10->ModifyStyle(WS_DISABLED,0);
	} else {
		pWnd10->ModifyStyle(0,WS_DISABLED);
	}

	if(g_pSimVars->GetN(AIRCRAFT_ON_GROUND)&&VCAnimStairs->get_bool()) {
		pWnd1->ModifyStyle(WS_DISABLED,0);
		pWnd2->ModifyStyle(WS_DISABLED,0);
		pWnd3->ModifyStyle(WS_DISABLED,0);
		pWnd4->ModifyStyle(WS_DISABLED,0);
		pWnd5->ModifyStyle(WS_DISABLED,0);
		pWnd6->ModifyStyle(WS_DISABLED,0);
		pWnd7->ModifyStyle(WS_DISABLED,0);
		pWnd8->ModifyStyle(WS_DISABLED,0);
		pWnd9->ModifyStyle(WS_DISABLED,0);
		//pWnd10->ModifyStyle(WS_DISABLED,0);
		pWnd11->ModifyStyle(WS_DISABLED,0);
		for(int i=0;i<30;i++) {
			pWndM[i]->ModifyStyle(WS_DISABLED,0);
		}
	} else {
		pWnd1->ModifyStyle(0,WS_DISABLED);
		pWnd2->ModifyStyle(0,WS_DISABLED);
		pWnd3->ModifyStyle(0,WS_DISABLED);
		pWnd4->ModifyStyle(0,WS_DISABLED);
		pWnd5->ModifyStyle(0,WS_DISABLED);
		pWnd6->ModifyStyle(0,WS_DISABLED);
		pWnd7->ModifyStyle(0,WS_DISABLED);
		pWnd8->ModifyStyle(0,WS_DISABLED);
		pWnd9->ModifyStyle(0,WS_DISABLED);
		//pWnd10->ModifyStyle(0,WS_DISABLED);
		pWnd11->ModifyStyle(0,WS_DISABLED);
		for(int i=0;i<30;i++) {
			pWndM[i]->ModifyStyle(0,WS_DISABLED);
		}
	}

}

void CConfigDialog::OnLButtonDown(UINT nFlags, CPoint point)
{
	// �������� ������ - ����� ��������������
	m_bMoveWindow = TRUE;
	// ��� ��������� �� ���� - � ������ ����, ���������� �� ���������
	// ����� ���� �� ������� � ���� ��� ������� ��������
	SetCapture();
	// ��������� ���������� ����
	GetWindowRect(m_RectDlg);
	// ��������� ��������� ����� ������ ���� ���������
	ClientToScreen(&point);
	m_MouseInDlg = point - m_RectDlg.TopLeft();
	// ������ ������, ���� ������� ���� ������
	m_hCursor = m_hCursorDown;
	::SetCursor(m_hCursor);

	CDialog::OnLButtonDown(nFlags, point);
}

void CConfigDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	if (m_bMoveWindow) // ���� ������
	{
		// ����������� ���������� ���� � ��������
		// ������ ��� ����� ����� ��� SetWindowPos()
		ClientToScreen(&point);
		// ������� ���� � ������������ � ������ ������������ ����
		SetWindowPos(&wndTop,
			point.x - m_MouseInDlg.x, point.y - m_MouseInDlg.y,
			m_RectDlg.right - m_RectDlg.left,
			m_RectDlg.bottom - m_RectDlg.top,
			SWP_SHOWWINDOW);
		// ��������� ���������� �� ��������� ��� ����� ����� ������������
		// �������������� ��������� ���������
		// �������� �������������� ScreenToClient(&point);
		// ����� �� ��������
	}

	CDialog::OnMouseMove(nFlags, point);
}

void CConfigDialog::OnLButtonUp(UINT nFlags, CPoint point)
{
// �������������� �����������
    m_bMoveWindow = FALSE;

    // "���������" �����
    ReleaseCapture();

    // ������ ������ �� ��������
    m_hCursor = m_hCursorUp;
    
    // �������� ���������� �� ���������
    CDialog::OnLButtonUp(nFlags, point);
}

BOOL CConfigDialog::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
// �������� ������ �� ����
    ::SetCursor(m_hCursor);
    
    return TRUE;
}

static void SetWeight27(SEAT27 seat,double weight,bool toggle=true)
{
	if(toggle) {
		if(GetPayloadWeightPound(seat))
			SetPayloadWeightPound(seat,0);
		else
			SetPayloadWeightPound(seat,weight);
	} else {
		SetPayloadWeightPound(seat,weight);
	}
}

void CConfigDialog::OnBnClickedChkpilot()
{
	SetWeight27(SEAT_PILOT,PILOTS_WEIGHT);
}

void CConfigDialog::OnBnClickedChkcopilot()
{
	SetWeight27(SEAT_COPILOT,PILOTS_WEIGHT);
}

void CConfigDialog::OnBnClickedChkengineer()
{
	SetWeight27(SEAT_ENGINEER,PILOTS_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow1seat1()
{
	SetWeight27(SEAT_ROW1_SEAT1,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow1seat2()
{
	SetWeight27(SEAT_ROW1_SEAT2,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow1seat3()
{
	SetWeight27(SEAT_ROW1_SEAT3,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow2seat1()
{
	SetWeight27(SEAT_ROW2_SEAT1,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow2seat2()
{
	SetWeight27(SEAT_ROW2_SEAT2,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow2seat3()
{
	SetWeight27(SEAT_ROW2_SEAT3,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow3seat1()
{
	SetWeight27(SEAT_ROW3_SEAT1,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow3seat2()
{
	SetWeight27(SEAT_ROW3_SEAT2,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow3seat3()
{
	SetWeight27(SEAT_ROW3_SEAT3,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow4seat1()
{
	SetWeight27(SEAT_ROW4_SEAT1,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow4seat2()
{
	SetWeight27(SEAT_ROW4_SEAT2,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow4seat3()
{
	SetWeight27(SEAT_ROW4_SEAT3,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow5seat1()
{
	SetWeight27(SEAT_ROW5_SEAT1,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow5seat2()
{
	SetWeight27(SEAT_ROW5_SEAT2,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow5seat3()
{
	SetWeight27(SEAT_ROW5_SEAT3,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow6seat1()
{
	SetWeight27(SEAT_ROW6_SEAT1,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow6seat2()
{
	SetWeight27(SEAT_ROW6_SEAT2,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow6seat3()
{
	SetWeight27(SEAT_ROW6_SEAT3,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow7seat1()
{
	SetWeight27(SEAT_ROW7_SEAT1,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow7seat2()
{
	SetWeight27(SEAT_ROW7_SEAT2,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow7seat3()
{
	SetWeight27(SEAT_ROW7_SEAT3,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow8seat1()
{
	SetWeight27(SEAT_ROW8_SEAT1,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow8seat2()
{
	SetWeight27(SEAT_ROW8_SEAT2,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow8seat3()
{
	SetWeight27(SEAT_ROW8_SEAT3,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow9seat1()
{
	SetWeight27(SEAT_ROW9_SEAT1,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow9seat2()
{
	SetWeight27(SEAT_ROW9_SEAT2,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedChkrow9seat3()
{
	SetWeight27(SEAT_ROW9_SEAT3,PAX_WEIGHT);
}

void CConfigDialog::OnBnClickedBtnpaxfull()
{
	OnBnClickedBtnpaxempty();

	SetWeight27(SEAT_PILOT,PILOTS_WEIGHT,false);
	SetWeight27(SEAT_COPILOT,PILOTS_WEIGHT,false);
	SetWeight27(SEAT_ENGINEER,PILOTS_WEIGHT,false);

	for(int i=SEAT_ROW1_SEAT1;i<SEAT_ROW9_SEAT3;i++) {
		SetWeight27((SEAT27)i,PAX_WEIGHT,false);
	}
}


void CConfigDialog::OnBnClickedBtnpaxempty()
{
	for(int i=0;i<SEAT_MAX;i++) {
		SetWeight27((SEAT27)i,0,false);
	}
}

void CConfigDialog::OnBnClickedBtnpax50pct()
{
	OnBnClickedBtnpaxempty();

	SetWeight27(SEAT_PILOT,PILOTS_WEIGHT,false);
	SetWeight27(SEAT_COPILOT,PILOTS_WEIGHT,false);
	SetWeight27(SEAT_ENGINEER,PILOTS_WEIGHT,false);

	bool done=false;
	int cnt=0;
	bool set[30]={false};

	while(!done) {
		int i=rand()%26+3;
		if(!set[i]) {
			set[i]=true;
			cnt++;
		}
		if(cnt==12)
			done=true;

	}

	for(int i=3;i<30;i++) {
		if(set[i])
			SetWeight27((SEAT27)i,PAX_WEIGHT,false);
	}

}

void CConfigDialog::OnBnClickedBtnpax25pct()
{
	OnBnClickedBtnpaxempty();

	SetWeight27(SEAT_PILOT,PILOTS_WEIGHT,false);
	SetWeight27(SEAT_COPILOT,PILOTS_WEIGHT,false);
	SetWeight27(SEAT_ENGINEER,PILOTS_WEIGHT,false);

	bool done=false;
	int cnt=0;
	bool set[30]={false};

	while(!done) {
		int i=rand()%26+3;
		if(!set[i]) {
			set[i]=true;
			cnt++;
		}
		if(cnt==5)
			done=true;

	}

	for(int i=3;i<30;i++) {
		if(set[i])
			SetWeight27((SEAT27)i,PAX_WEIGHT,false);
	}
}


void CConfigDialog::OnBnClickedBtnpax75pct()
{
	OnBnClickedBtnpaxempty();

	SetWeight27(SEAT_PILOT,PILOTS_WEIGHT,false);
	SetWeight27(SEAT_COPILOT,PILOTS_WEIGHT,false);
	SetWeight27(SEAT_ENGINEER,PILOTS_WEIGHT,false);

	bool done=false;
	int cnt=0;
	bool set[30]={false};

	while(!done) {
		int i=rand()%26+3;
		if(!set[i]) {
			set[i]=true;
			cnt++;
		}
		if(cnt==19)
			done=true;

	}

	for(int i=3;i<30;i++) {
		if(set[i])
			SetWeight27((SEAT27)i,PAX_WEIGHT,false);
	}
}

HBRUSH CConfigDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetDlgCtrlID() == IDC_EDITTOTALW)
	{
		if(m_fTotalWeight>GetMaxGrossWeightKg()) {
			pDC->SetTextColor(RGB(255, 0, 0));
			pDC->SetBkMode(TRANSPARENT);
		} else {
			pDC->SetTextColor(RGB(0, 0, 0));
			pDC->SetBkMode(TRANSPARENT);
		}
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDITCURCG)
	{
		if(m_fTotalCG<13||m_fTotalCG>32) {
			pDC->SetTextColor(RGB(255, 0, 0));
			pDC->SetBkMode(TRANSPARENT);
		} else {
			pDC->SetTextColor(RGB(0, 0, 0));
			pDC->SetBkMode(TRANSPARENT);
		}
	}

	return hbr;
}

void CConfigDialog::OnBnClickedBtnclose()
{
	POS_SET(POS_GROUND_STARTUP,0);
}

void CConfigDialog::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(nChar==VK_ESCAPE)
		OnBnClickedBtnclose();

	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CConfigDialog::OnBnClickedChkrap()
{
	AZS_SET(AZS_EXT_PWR,m_cbExtPwr.GetCheck());

	if(AZS_GET(AZS_EXT_PWR))
		SetWindowTextSim("Wait. Connecting external power.",MSG_GENERIC_SCROLL,50,1);
	else
		SetWindowTextSim("Wait. Disconnecting external power.",MSG_GENERIC_SCROLL,50,1);
}

void CConfigDialog::OnBnClickedChkair()
{
	AZS_SET(AZS_EXT_AIR,m_cbExtAir.GetCheck());

	if(AZS_GET(AZS_EXT_AIR))
		SetWindowTextSim("Wait. Connecting external air.",MSG_GENERIC_SCROLL,50,1);
	else
		SetWindowTextSim("Wait. Disconnecting external air.",MSG_GENERIC_SCROLL,50,1);

}

void CConfigDialog::OnStnClickedSalon27()
{
	// TODO: Add your control notification handler code here
}
