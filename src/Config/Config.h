// Config.h : main header file for the Config DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


// CConfigApp
// See Config.cpp for the implementation of this class
//

class CConfigApp : public CWinApp
{
public:
	CConfigApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
