#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "../Lib/Undoc.h"

// CConfigDialog dialog

class CConfigDialog : public CDialog
{
	DECLARE_DYNAMIC(CConfigDialog)

protected:
	CPoint m_MouseInDlg;
	HCURSOR m_hCursor, m_hCursorDown, m_hCursorUp;
	CRect m_RectDlg;
	BOOL m_bMoveWindow;
	HICON m_hIcon;
	double m_fTotalWeight;
	double m_fTotalCG;
	int m_SEPosOld;
	int m_SCPosOld;
	int m_TLPosOld;
	int m_TRPosOld;
	std::auto_ptr<CNamedVar> VCAnimStairs;

public:
	CConfigDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CConfigDialog();

// Dialog Data
	enum { IDD = IDD_MAINFORM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	void Update(void);
	void SetStatePax(void);

	virtual BOOL OnInitDialog();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	CSliderCtrl m_SliderEquip;
	CSliderCtrl m_SliderCargo;
	CSliderCtrl m_SliderLeftTank;
	CSliderCtrl m_SliderRightTank;
	CEdit m_EditEquip;
	CEdit m_EditCargo;
	CEdit m_EditLeftTank;
	CEdit m_EditRightTank;
	CEdit m_EditTotalWeight;
	CEdit m_EditMaxWeight;
	CEdit m_EditCurrentCg;
	CEdit m_EditMinimumCg;
	CEdit m_EditMaximumCg;
	CButton m_cbPilot;
	CButton m_cbCopilot;
	CButton m_cbEngineer2;
	CButton m_cbRow1Seat1;
	CButton m_cbRow1Seat2;
	CButton m_cbRow1Seat3;
	CButton m_cbRow2Seat1;
	CButton m_cbRow2Seat2;
	CButton m_cbRow2Seat3;
	CButton m_cbRow3Seat1;
	CButton m_cbRow3Seat2;
	CButton m_cbRow3Seat3;
	CButton m_cbRow4Seat1;
	CButton m_cbRow4Seat2;
	CButton m_cbRow4Seat3;
	CButton m_cbRow5Seat1;
	CButton m_cbRow5Seat2;
	CButton m_cbRow5Seat3;
	CButton m_cbRow6Seat1;
	CButton m_cbRow6Seat2;
	CButton m_cbRow6Seat3;
	CButton m_cbRow7Seat1;
	CButton m_cbRow7Seat2;
	CButton m_cbRow7Seat3;
	CButton m_cbRow8Seat1;
	CButton m_cbRow8Seat2;
	CButton m_cbRow8Seat3;
	CButton m_cbRow9Seat1;
	CButton m_cbRow9Seat2;
	CButton m_cbRow9Seat3;
	afx_msg void OnBnClickedChkpilot();
	afx_msg void OnBnClickedChkcopilot();
	afx_msg void OnBnClickedChkengineer();
	afx_msg void OnBnClickedChkrow1seat1();
	afx_msg void OnBnClickedChkrow1seat2();
	afx_msg void OnBnClickedChkrow1seat3();
	afx_msg void OnBnClickedChkrow2seat1();
	afx_msg void OnBnClickedChkrow2seat2();
	afx_msg void OnBnClickedChkrow2seat3();
	afx_msg void OnBnClickedChkrow3seat1();
	afx_msg void OnBnClickedChkrow3seat2();
	afx_msg void OnBnClickedChkrow3seat3();
	afx_msg void OnBnClickedChkrow4seat1();
	afx_msg void OnBnClickedChkrow4seat2();
	afx_msg void OnBnClickedChkrow4seat3();
	afx_msg void OnBnClickedChkrow5seat1();
	afx_msg void OnBnClickedChkrow5seat2();
	afx_msg void OnBnClickedChkrow5seat3();
	afx_msg void OnBnClickedChkrow6seat1();
	afx_msg void OnBnClickedChkrow6seat2();
	afx_msg void OnBnClickedChkrow6seat3();
	afx_msg void OnBnClickedChkrow7seat1();
	afx_msg void OnBnClickedChkrow7seat2();
	afx_msg void OnBnClickedChkrow7seat3();
	afx_msg void OnBnClickedChkrow8seat1();
	afx_msg void OnBnClickedChkrow8seat2();
	afx_msg void OnBnClickedChkrow8seat3();
	afx_msg void OnBnClickedChkrow9seat1();
	afx_msg void OnBnClickedChkrow9seat2();
	afx_msg void OnBnClickedChkrow9seat3();
	afx_msg void OnBnClickedBtnpaxfull();
	afx_msg void OnBnClickedBtnpaxempty();
	afx_msg void OnBnClickedBtnpax50pct();
	afx_msg void OnBnClickedBtnpax25pct();
	afx_msg void OnBnClickedBtnpax75pct();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnclose();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnBnClickedChkrap();
	CButton m_cbExtPwr;
	CButton m_cbExtAir;
	afx_msg void OnBnClickedChkair();
	afx_msg void OnStnClickedSalon27();
};
