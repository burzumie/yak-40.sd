#include "stdafx.h"
#include "ConfigMain.h"
#include "ConfigDialog.h"

#ifdef _DEBUG
_CrtMemState memoryState; 
#endif

HINSTANCE g_hInstance=NULL;
sImportTab ImportTable;
SDSimVars *g_pSimVars=NULL;

CConfigDialog *pConfigDialog=NULL;
bool m_AlreadyInited=false;

void FSAPI fnInitPanel(void);

void UpdatePanel()
{
	UndocUpdate(); 

	if(g_pSimVars)
		g_pSimVars->UpdateVars();

	if(pConfigDialog) {
		if(!pConfigDialog->m_hWnd) {
			pConfigDialog->Create(IDD_MAINFORM);
		}
		if(POS_GET(POS_GROUND_STARTUP)) {	
			if(!pConfigDialog->IsWindowVisible()) {
				pConfigDialog->ShowWindow(SW_SHOW);
			}
			pConfigDialog->Update();
		} else {
			if(pConfigDialog->IsWindowVisible()) {
				pConfigDialog->ShowWindow(SW_HIDE);
			}
		}
	}
}

void FSAPI fnInitPanel(void)
{
#ifdef _DEBUG
	_CrtMemCheckpoint(&memoryState);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
#endif

}

void FSAPI fnDeinitPanel(void)
{
#ifdef _DEBUG
	CHECK_MEM_LEAK("F:\\MemLeaks\\Yak40\\CF.log");

#endif
}

#ifdef GAUGE_NAME
#undef GAUGE_NAME
#endif

#ifdef GAUGEHDR_VAR_NAME
#undef GAUGEHDR_VAR_NAME
#endif

#ifdef GAUGE_W
#undef GAUGE_W
#endif

#define GAUGE_NAME         "main"
#define GAUGEHDR_VAR_NAME  cfg_hdr
#define GAUGE_W            MISC_DUMMY_SX

extern PELEMENT_HEADER cfg_list;
GAUGE_CALLBACK cfg_cb;
GAUGE_HEADER_FS700(GAUGE_W, GAUGE_NAME, &cfg_list, 0, cfg_cb, 0, 0, 0);
MAKE_STATIC(cfg_bg,MISC_DUMMY,NULL,NULL,IMAGE_USE_TRANSPARENCY,0,0,0)
PELEMENT_HEADER	cfg_list = &cfg_bg.header;

void FSAPI cfg_cb( PGAUGEHDR pgauge, SINT32 service_id, UINT32 extra_data ) 
{
	switch(service_id) {
		case PANEL_SERVICE_CONNECT_TO_WINDOW:
			UndocUpdate(); 

			if(!g_pSimVars) {
				g_pSimVars=new SDSimVars();
				g_pSimVars->InitVars();
			}
			ImportTable.pPanels->initialize_var_by_name(&MVSDAPIEntrys,SDAPISIGN); 
			g_pSDAPIEntrys=(SDAPIEntrys *)MVSDAPIEntrys.var_ptr; 
			if(!pConfigDialog) {
				if((pConfigDialog=new CConfigDialog())) {
					if(pConfigDialog->Create(IDD_MAINFORM)) {
						pConfigDialog->Update();
					} else {
						//delete pConfigDialog;
						//pConfigDialog=NULL;
					}
				}
			}
		break;
		case PANEL_SERVICE_DISCONNECT:
			UndocReset(); 

			if(pConfigDialog) {
				delete pConfigDialog;
				pConfigDialog=NULL;
			}

			if(g_pSimVars) {
				delete g_pSimVars;
				g_pSimVars=NULL;
			}

		break;
		case PANEL_SERVICE_PRE_UPDATE:
			UpdatePanel(); 
		break;
	}
}

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&cfg_hdr),
		0
	}
};

