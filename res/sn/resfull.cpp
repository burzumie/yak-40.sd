#include <boost/algorithm/string.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <iostream>
#include <map>
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>

#define HAVE_DAY_PANEL
#define HAVE_MIX_PANEL
#define HAVE_PLF_PANEL
#define HAVE_RED_PANEL

namespace fs = boost::filesystem;
namespace po = boost::program_options;

using namespace std;
using namespace boost;

void create_rc_header(ofstream &rc, string incres="res.h")
{
	rc << "/*=========================================================================\n\n";
	rc << "  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004\n\n";
	rc << "  $Archive: $\n\n";
	rc << "  Last modification:\n";
	rc << "    $Date: $\n";
	rc << "    $Revision: $\n";
	rc << "    $Author: $\n\n";
	rc << "  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]\n\n";
	rc << "  All rights reserved.\n\n";
	rc << "=========================================================================*/\n\n";

	rc << "#include \"" << incres << "\"" << endl;
	rc << "#define APSTUDIO_READONLY_SYMBOLS\n";
	rc << "#include \"afxres.h\"\n";
	rc << "#undef APSTUDIO_READONLY_SYMBOLS\n";
	rc << "#if !defined(AFX_RESOURCE_DLL) || defined(AFX_TARG_RUS)\n";
	rc << "#ifdef _WIN32\n";
	rc << "LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US\n";
	rc << "#pragma code_page(1252)\n";
	rc << "#endif\n";
	rc << "#ifndef _MAC\n";
	rc << "VS_VERSION_INFO VERSIONINFO\n";
	rc << " FILEVERSION VERSION_MAJOR,VERSION_MINOR,0,VERSION_BUILD\n";
	rc << " PRODUCTVERSION VERSION_MAJOR,VERSION_MINOR,0,VERSION_BUILD\n";
	rc << " FILEFLAGSMASK 0x3fL\n";
	rc << " FILEFLAGS 0x0L\n";
	rc << " FILEOS 0x10004L\n";
	rc << " FILETYPE 0x1L\n";
	rc << " FILESUBTYPE 0x0L\n";
	rc << "BEGIN\n";
	rc << "    BLOCK \"StringFileInfo\"\n";
	rc << "    BEGIN\n";
	rc << "        BLOCK \"040904b0\"\n";
	rc << "        BEGIN\n";
	rc << "            VALUE \"CompanyName\", COMPANY_NAME\n";
	rc << "            VALUE \"FileDescription\", FILE_DESCRIPTION\n";
	rc << "            VALUE \"FileVersion\", VERSION_STRING\n";
	rc << "            VALUE \"LegalCopyright\", LEGAL_COPYRIGHT\n";
	rc << "            VALUE \"LegalTrademarks\", LEGAL_TRADEMARK\n";
	rc << "            VALUE \"ProductName\", PRODUCT_NAME\n";
	rc << "            VALUE \"ProductVersion\", VERSION_STRING\n";
	rc << "        END\n";
	rc << "    END\n";
	rc << "    BLOCK \"VarFileInfo\"\n";
	rc << "    BEGIN\n";
	rc << "        VALUE \"Translation\", 0x419, 1200\n";
	rc << "    END\n";
	rc << "END\n";
	rc << "#endif\n";
	rc << "#ifdef APSTUDIO_INVOKED\n";
	rc << "1 TEXTINCLUDE DISCARDABLE\n";
	rc << "BEGIN\n";
	rc << "    \"resource.h\\0\"\n";
	rc << "END\n";
	rc << "2 TEXTINCLUDE DISCARDABLE\n";
	rc << "BEGIN\n";
	rc << "    \"#include \"\"afxres.h\"\"\\r\\n\"\n";
	rc << "    \"\\0\"\n";
	rc << "END\n";
	rc << "3 TEXTINCLUDE DISCARDABLE\n";
	rc << "BEGIN\n";
	rc << "    \"\\r\\n\"\n";
	rc << "    \"\\0\"\n";
	rc << "END\n";
	rc << "#endif\n";
	rc << "#endif\n\n";

}

void create_rc_footer(ofstream &rc)
{
	rc << "\n#ifndef APSTUDIO_INVOKED\n";
	rc << "#endif\n";
}

void create_h_header(ofstream &hdr, string incsz="res_size.h")
{
	hdr << "/*=========================================================================\n\n";
	hdr << "  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004\n\n";
	hdr << "  $Archive: $\n\n";
	hdr << "  Last modification:\n";
	hdr << "    $Date: $\n";
	hdr << "    $Revision: $\n";
	hdr << "    $Author: $\n\n";
	hdr << "  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]\n\n";
	hdr << "  All rights reserved.\n\n";
	hdr << "=========================================================================*/\n\n";
	hdr << "#include \"../Version.h\"\n\n";
	hdr << "#include \"" << incsz << "\"\n\n";
}

void create_h_footer(ofstream &hdr)
{
	hdr << "\n#ifdef APSTUDIO_INVOKED\n";
	hdr << "#ifndef APSTUDIO_READONLY_SYMBOLS\n";
	hdr << "#define _APS_NEXT_RESOURCE_VALUE        101\n";
	hdr << "#define _APS_NEXT_COMMAND_VALUE         40001\n";
	hdr << "#define _APS_NEXT_CONTROL_VALUE         1000\n";
	hdr << "#define _APS_NEXT_SYMED_VALUE           101\n";
	hdr << "#endif\n";
	hdr << "#endif\n";
}

void create_hs_header(ofstream &hdr2)
{
	hdr2 << "/*=========================================================================\n\n";
	hdr2 << "  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004\n\n";
	hdr2 << "  $Archive: $\n\n";
	hdr2 << "  Last modification:\n";
	hdr2 << "    $Date: $\n";
	hdr2 << "    $Revision: $\n";
	hdr2 << "    $Author: $\n\n";
	hdr2 << "  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]\n\n";
	hdr2 << "  All rights reserved.\n\n";
	hdr2 << "=========================================================================*/\n\n";
}

string get_param(string p, string f, string ext=".bmp")
{
	string aaa=to_upper_copy(p)+"_"+to_upper_copy(replace_last_copy(f,ext,""));
	return ireplace_all_copy(aaa,"-","_");
}

void write_line_to_rc(ofstream &rc, string p, string f) 
{
	string name=f; //get_param(p,f);
	string name2=get_param(p,f);
	if((find_last(name,"_D.")||find_last(name,"_D_")||find_last(name,"_d.")||find_last(name,"_d_"))) {
		#ifdef HAVE_DAY_PANEL
		rc << get_param(p,f) << "\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
		#endif
	}
	else 
	if((find_last(name,"_M.")||find_last(name,"_M_")||find_last(name,"_m.")||find_last(name,"_m_"))) {
		#ifdef HAVE_MIX_PANEL
		rc << get_param(p,f) << "\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
		#endif
	}
	else 
	if((find_last(name,"_P.")||find_last(name,"_P_")||find_last(name,"_p.")||find_last(name,"_p_"))) {
		#ifdef HAVE_PLF_PANEL
		rc << get_param(p,f) << "\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
		#endif
	}
	else 
	if((find_last(name,"_R.")||find_last(name,"_R_")||find_last(name,"_r.")||find_last(name,"_r_"))) {
		#ifdef HAVE_RED_PANEL
		rc << get_param(p,f) << "\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
		#endif
	}
	else 
	rc << get_param(p,f) << "\t\t\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
	
}

void write_line_to_h(ofstream &hdr, string p, string f, int &counter) 
{
	string name=f; //get_param(p,f);
	string name2=get_param(p,f);
	if((find_last(name,"_D.")||find_last(name,"_D_")||find_last(name,"_d.")||find_last(name,"_d_"))) {
		#ifdef HAVE_DAY_PANEL
		hdr << "#define\t" << get_param(p,f) << "\t" << counter++ << endl;
		#endif
	}
	else 
	if((find_last(name,"_M.")||find_last(name,"_M_")||find_last(name,"_m.")||find_last(name,"_m_"))) {
		#ifdef HAVE_MIX_PANEL
		hdr << "#define\t" << get_param(p,f) << "\t" << counter++ << endl;
		#endif
	}
	else 
	if((find_last(name,"_P.")||find_last(name,"_P_")||find_last(name,"_p.")||find_last(name,"_p_"))) {
		#ifdef HAVE_PLF_PANEL
		hdr << "#define\t" << get_param(p,f) << "\t" << counter++ << endl;
		#endif
	}
	else 
	if((find_last(name,"_R.")||find_last(name,"_R_")||find_last(name,"_r.")||find_last(name,"_r_"))) {
		#ifdef HAVE_RED_PANEL
		hdr << "#define\t" << get_param(p,f) << "\t" << counter++ << endl;
		#endif
	}
	else 
	hdr << "#define\t" << get_param(p,f) << "\t\t\t" << counter++ << endl;
}

void write_line_to_rc_snd(ofstream &rc, string p, string f) 
{
	string name=f; //get_param(p,f);
	string name2=get_param(p,f,".wav");
	rc << get_param(p,f,".wav") << "\t\t\tRCDATA DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
	
}

void write_line_to_h_snd(ofstream &hdr, string p, string f, int &counter) 
{
	string name=f; //get_param(p,f);
	string name2=get_param(p,f,".wav");
	hdr << "#define\t" << get_param(p,f,".wav") << "\t\t\t" << counter++ << endl;
}

void write_line_to_hs(ofstream &hdr2, string p, string f) 
{
	string name=f; //get_param(p,f);
	string name2=get_param(p,f);
	BITMAP bm;
	HBITMAP b=(HBITMAP)LoadImage(NULL,(p+"\\\\"+f).c_str(),IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	GetObject(b,sizeof(bm),&bm);
	if((find_last(name,"_D.")||find_last(name,"_D_")||find_last(name,"_d.")||find_last(name,"_d_"))) {
		#ifdef HAVE_DAY_PANEL
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
		#endif
	}
	else 
	if((find_last(name,"_M.")||find_last(name,"_M_")||find_last(name,"_m.")||find_last(name,"_m_"))) {
		#ifdef HAVE_MIX_PANEL
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
		#endif
	}
	else 
	if((find_last(name,"_P.")||find_last(name,"_P_")||find_last(name,"_p.")||find_last(name,"_p_"))) {
		#ifdef HAVE_PLF_PANEL
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
		#endif
	}
	else 
	if((find_last(name,"_R.")||find_last(name,"_R_")||find_last(name,"_r.")||find_last(name,"_r_"))) {
		#ifdef HAVE_RED_PANEL
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
		#endif
	}
	else {
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
	}
	DeleteObject((HBITMAP)b);
}

void scandir(string initdir,int &counter,ofstream &rcout,ofstream &hout,ofstream &hsout)
{
	fs::path dir(initdir);

	fs::directory_iterator end_iter;
	for ( fs::directory_iterator dir_itr( dir ); dir_itr != end_iter; ++dir_itr ) {
		try {
			if ( !fs::is_directory( *dir_itr ) ) {
				if(dir.leaf()!="res") {
						string path1=dir.leaf();
						string file1=dir_itr->leaf();
						if(iends_with(file1,".bmp")) {
							write_line_to_rc(rcout, path1, file1);
							write_line_to_h(hout, path1, file1, counter);
							write_line_to_hs(hsout, path1, file1);
						} else 
						if(iends_with(file1,".wav")) {
							write_line_to_rc_snd(rcout, path1, file1);
							write_line_to_h_snd(hout, path1, file1, counter);
						}
				}
			}
		}
		catch ( const std::exception & ex ) {
			std::cout << dir_itr->leaf() << " " << ex.what() << std::endl;
		}
	}

}

void createdir(string name,char *dir1,char *dir2,char *dir3,char *dir4,char *dir5,char *dir6)
{
	ofstream rcout((name+"_res.rc").c_str());
	ofstream hout((name+"_res.h").c_str());
	ofstream hsout((name+"_size.h").c_str());

	create_rc_header(rcout,name+"_res.h");
	create_h_header(hout,name+"_size.h");
	create_hs_header(hsout);

	int counter=4096;

	if(dir1) {
		scandir(dir1,counter,rcout,hout,hsout);
	}
	if(dir2) {
		scandir(dir2,counter,rcout,hout,hsout);
	}
	if(dir3) {
		scandir(dir3,counter,rcout,hout,hsout);
	}
	if(dir4) {
		scandir(dir4,counter,rcout,hout,hsout);
	}
	if(dir5) {
		scandir(dir5,counter,rcout,hout,hsout);
	}
	if(dir6) {
		scandir(dir6,counter,rcout,hout,hsout);
	}

	create_rc_footer(rcout);
	create_h_footer(hout);

}

void main(int argc,char *argv[])
{
	createdir( "snd", "misc","snd",NULL,NULL,NULL,NULL);
}
