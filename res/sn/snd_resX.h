/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "snd_sizeX.h"

#define	MISC_DUMMY			4096
#define	SND_SD6015_APU_AI9			4097
#define	SND_SD6015_AZS			4098
#define	SND_SD6015_BATTERYON			4099
#define	SND_SD6015_BUTTON			4100
#define	SND_SD6015_FUELVALVE_AI25_CLOSE			4101
#define	SND_SD6015_FUELVALVE_AI25_OPEN			4102
#define	SND_SD6015_GALETA			4103
#define	SND_SD6015_GYRO			4104
#define	SND_SD6015_INNER			4105
#define	SND_SD6015_MIDDLE			4106
#define	SND_SD6015_NOSELEG			4107
#define	SND_SD6015_OUTER			4108
#define	SND_SD6015_PT500			4109
#define	SND_SD6015_RV_VPR			4110
#define	SND_SD6015_SIRENA			4111
#define	SND_SD6015_STAIRSCLOSE			4112
#define	SND_SD6015_STAIRSOPEN			4113
#define	SND_SD6015_TUMBLER			4114
#define	SND_SD6015_ZVONOK3SEC			4115

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
