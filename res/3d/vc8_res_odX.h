/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "vc8_size_odX.h"

#define	VC8_WBC01_D	1001
#define	VC8_WBC02_D	1002
#define	VC8_WBC03_D	1003
#define	VC8_WBC04_D	1004
#define	VC8_WBC05_D	1005
#define	VC8_WBC06_D	1006
#define	VC8_WBC07_D	1007
#define	VC8_WBC08_D	1008
#define	VC8_WBC09_D	1009
#define	VC8_WBC10_D	1010
#define	VC8_WBC11_D	1011
#define	VC8_WBC12_D	1012
#define	VC8_WBC13INTL_D	1013
#define	VC8_WBC13_D	1014
#define	VC8_WBC14_D	1015
#define	VC8_WBC15_D	1016
#define	VC8_WBC16INTL_D	1017
#define	VC8_WBC16_D	1018
#define	VC8_WBC17INTL_D	1019
#define	VC8_WBC17_D	1020
#define	VC8_WBC18_D	1021
#define	VC8_WBC19INTL_D	1022
#define	VC8_WBC19_D	1023
#define	VC8_WBC20INTL_D	1024
#define	VC8_WBC20_D	1025
#define	VC8_WBC21_D	1026
#define	VC8_WBC22INTL_D	1027
#define	VC8_WBC22_D	1028
#define	VC8_WBC23_D	1029
#define	VC8_WBC24_D	1030
#define	VC8_WBC25INTL_D	1031
#define	VC8_WBC25_D	1032
#define	VC8_WBC26_D	1033
#define	VC8_WBC27_D	1034
#define	VC8_WBC28INTL_D	1035
#define	VC8_WBC28_D	1036
#define	VC8_WBC29_D	1037
#define	VC8_WBC30INTL_D	1038
#define	VC8_WBC30_D	1039
#define	VC8_WBC31_D	1040
#define	VC8_WBC32INTL_D	1041
#define	VC8_WBC32_D	1042
#define	VC8_WBC33_D	1043
#define	VC8_WBC34_D	1044
#define	VC8_WBC35_D	1045
#define	VC8_WBC36_D	1046
#define	VC8_WBC37_D	1047
#define	VC8_WBC38_D	1048
#define	VC8_WBC39_D	1049
#define	VC8_WBC40_D	1050
#define	VC8_WBC41_D	1051
#define	VC8_WBC42_D	1052
#define	VC8_WBC43_D	1053
#define	VC8_WBC44_D	1054
#define	VC8_WBC45_D	1055

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
