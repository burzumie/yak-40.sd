/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../Version.h"

#include "vc8_size_p.h"

#define	VC8_WBC01_D	1001
#define	VC8_WBC01_P	1002
#define	VC8_WBC02_D	1003
#define	VC8_WBC02_P	1004
#define	VC8_WBC03_D	1005
#define	VC8_WBC03_P	1006
#define	VC8_WBC04_D	1007
#define	VC8_WBC04_P	1008
#define	VC8_WBC05_D	1009
#define	VC8_WBC05_P	1010
#define	VC8_WBC06_D	1011
#define	VC8_WBC06_P	1012
#define	VC8_WBC07_D	1013
#define	VC8_WBC07_P	1014
#define	VC8_WBC08_D	1015
#define	VC8_WBC08_P	1016
#define	VC8_WBC09_D	1017
#define	VC8_WBC09_P	1018
#define	VC8_WBC10_D	1019
#define	VC8_WBC10_P	1020
#define	VC8_WBC11_D	1021
#define	VC8_WBC11_P	1022
#define	VC8_WBC12_D	1023
#define	VC8_WBC12_P	1024
#define	VC8_WBC13INTL_D	1025
#define	VC8_WBC13INTL_P	1026
#define	VC8_WBC13_D	1027
#define	VC8_WBC13_P	1028
#define	VC8_WBC14_D	1029
#define	VC8_WBC14_P	1030
#define	VC8_WBC15_D	1031
#define	VC8_WBC15_P	1032
#define	VC8_WBC16INTL_D	1033
#define	VC8_WBC16INTL_P	1034
#define	VC8_WBC16_D	1035
#define	VC8_WBC16_P	1036
#define	VC8_WBC17INTL_D	1037
#define	VC8_WBC17INTL_P	1038
#define	VC8_WBC17_D	1039
#define	VC8_WBC17_P	1040
#define	VC8_WBC18_D	1041
#define	VC8_WBC18_P	1042
#define	VC8_WBC19INTL_D	1043
#define	VC8_WBC19INTL_P	1044
#define	VC8_WBC19_D	1045
#define	VC8_WBC19_P	1046
#define	VC8_WBC20INTL_D	1047
#define	VC8_WBC20INTL_P	1048
#define	VC8_WBC20_D	1049
#define	VC8_WBC20_P	1050
#define	VC8_WBC21_D	1051
#define	VC8_WBC21_P	1052
#define	VC8_WBC22INTL_D	1053
#define	VC8_WBC22INTL_P	1054
#define	VC8_WBC22_D	1055
#define	VC8_WBC22_P	1056
#define	VC8_WBC23_D	1057
#define	VC8_WBC23_P	1058
#define	VC8_WBC24_D	1059
#define	VC8_WBC24_P	1060
#define	VC8_WBC25INTL_D	1061
#define	VC8_WBC25INTL_P	1062
#define	VC8_WBC25_D	1063
#define	VC8_WBC25_P	1064
#define	VC8_WBC26_D	1065
#define	VC8_WBC26_P	1066
#define	VC8_WBC27_D	1067
#define	VC8_WBC27_P	1068
#define	VC8_WBC28INTL_D	1069
#define	VC8_WBC28INTL_P	1070
#define	VC8_WBC28_D	1071
#define	VC8_WBC28_P	1072
#define	VC8_WBC29_D	1073
#define	VC8_WBC29_P	1074
#define	VC8_WBC30INTL_D	1075
#define	VC8_WBC30INTL_P	1076
#define	VC8_WBC30_D	1077
#define	VC8_WBC30_P	1078
#define	VC8_WBC31_D	1079
#define	VC8_WBC31_P	1080
#define	VC8_WBC32INTL_D	1081
#define	VC8_WBC32INTL_P	1082
#define	VC8_WBC32_D	1083
#define	VC8_WBC32_P	1084
#define	VC8_WBC33_D	1085
#define	VC8_WBC33_P	1086
#define	VC8_WBC34_D	1087
#define	VC8_WBC34_P	1088
#define	VC8_WBC35_D	1089
#define	VC8_WBC35_P	1090
#define	VC8_WBC36_D	1091
#define	VC8_WBC36_P	1092
#define	VC8_WBC37_D	1093
#define	VC8_WBC37_P	1094
#define	VC8_WBC38_D	1095
#define	VC8_WBC38_P	1096
#define	VC8_WBC39_D	1097
#define	VC8_WBC39_P	1098
#define	VC8_WBC40_D	1099
#define	VC8_WBC40_P	1100
#define	VC8_WBC41_D	1101
#define	VC8_WBC41_P	1102
#define	VC8_WBC42_D	1103
#define	VC8_WBC42_P	1104
#define	VC8_WBC43_D	1105
#define	VC8_WBC43_P	1106
#define	VC8_WBC44_D	1107
#define	VC8_WBC44_P	1108
#define	VC8_WBC45_D	1109
#define	VC8_WBC45_P	1110

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
