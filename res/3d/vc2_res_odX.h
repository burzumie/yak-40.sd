/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "vc2_size_odX.h"

#define	VC2_CLMN1_D	1001
#define	VC2_CLMN2_D	1002
#define	VC2_CLMN3_D	1003
#define	VC2_CLMN4_D	1004
#define	VC2_CLMNA_BTN1_FRONT_D	1005
#define	VC2_CLMNA_BTN1_SIDE_D	1006
#define	VC2_CLMNA_BTN2_FRONT_D	1007
#define	VC2_CLMNA_BTN2_SIDE_D	1008
#define	VC2_CLMNA_HNDL1A_D	1009
#define	VC2_CLMNA_HNDL1B_D	1010
#define	VC2_CLMNA_HNDL2A_D	1011
#define	VC2_CLMNA_HNDL2B_D	1012
#define	VC2_CLMNA_LOGO_D	1013
#define	VC2_CLMNA_NBLD2_FRONT_D	1014
#define	VC2_CLMNA_NBLD2_SIDE_D	1015
#define	VC2_CLMNA_NBLD2_TOP_D	1016
#define	VC2_CLMNA_TRIM2_D	1017
#define	VC2_CLMN_NBLD1_FRONT_D	1018
#define	VC2_CLMN_NBLD1_SIDE_D	1019
#define	VC2_CLMN_NBLD1_TOP_D	1020
#define	VC2_CLMN_RUBBER_D	1021
#define	VC2_CLMN_SHAFT_BASE_D	1022
#define	VC2_CLMN_SHAFT_D	1023
#define	VC2_CLMN_TRIM1_D	1024
#define	VC2_RAD_BTN1_D	5001
#define	VC2_RAD_BTN2_D	5002
#define	VC2_RAD_BTN3_D	5003
#define	VC2_RAD_BTN4_D	5004
#define	VC2_RAD_BTNR1_D	5005
#define	VC2_RAD_BTNR2_D	5006
#define	VC2_RAD_BTNR3_D	5007
#define	VC2_RAD_BTNR4_D	5008
#define	VC2_RAD_BTNR_SIDE_D	5009
#define	VC2_RAD_BTN_SIDE_D	5010
#define	VC2_RAD_DET1_D	5011
#define	VC2_RAD_DET2_D	5012
#define	VC2_RAD_FRN_D	5013
#define	VC2_RAD_KNB1_D	5014
#define	VC2_RAD_KNB2_D	5015
#define	VC2_RAD_KNB3_D	5016
#define	VC2_RAD_KNOB_SIDE_D	5017
#define	VC2_RAD_SDE_D	5018
#define	VC2_RAD_SHD10_D	5019
#define	VC2_RAD_SHD11_D	5020
#define	VC2_RAD_SHD12_D	5021
#define	VC2_RAD_SHD13_D	5022
#define	VC2_RAD_SHD1_D	5023
#define	VC2_RAD_SHD2_D	5024
#define	VC2_RAD_SHD3_D	5025
#define	VC2_RAD_SHD4_D	5026
#define	VC2_RAD_SHD5_D	5027
#define	VC2_RAD_SHD6_D	5028
#define	VC2_RAD_SHD7_D	5029
#define	VC2_RAD_SHD8_D	5030
#define	VC2_RAD_SHD9_D	5031
#define	VC2_RAD_TOP_D	5032
#define	VC2_STS01_D	9001
#define	VC2_STS02_D	9002
#define	VC2_STS03_D	9003
#define	VC2_STS04_D	9004
#define	VC2_STS05_D	9005
#define	VC2_STS06_D	9006
#define	VC2_STS07_D	9007
#define	VC2_STS08_D	9008
#define	VC2_STS09_D	9009
#define	VC2_STS10_D	9010
#define	VC2_STS11_D	9011
#define	VC2_STS12_D	9012
#define	VC2_STS13_D	9013
#define	VC2_STS14_D	9014
#define	VC2_STS15_D	9015
#define	VC2_STS16_D	9016
#define	VC2_STSHANDLESIDE_D	9017
#define	VC2_STSHANDLETOP_D	9018
#define	VC2_STSRAILSIDE_D	9019
#define	VC2_STSRAILTOP_D	9020
#define	VC2_STSSUPPORT_D	9021
#define	VC2_SPG_SDE_D	13001
#define	VC2_SPG_TOP_D	13002
#define	VC2_PTSSPOT01_D	17001
#define	VC2_PTSSPOT02_D	17002
#define	VC2_PTSSPOT03_D	17003
#define	VC2_PTSSPOT04_D	17004
#define	VC2_PTSSPOT05_D	17005
#define	VC2_SPOTLIGHTCAP_D	21001
#define	VC2_SPOTLIGHTTOP_D	21002

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
