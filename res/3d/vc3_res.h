/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../Version.h"

#include "vc3_size.h"

#define	VC3_BCK_CNCFRONT_D	1001
#define	VC3_BCK_CNCFRONT_M	1002
#define	VC3_BCK_CNCFRONT_P	1003
#define	VC3_BCK_CNCFRONT_R	1004
#define	VC3_BCK_CNCSIDE_D	1005
#define	VC3_BCK_CNCSIDE_M	1006
#define	VC3_BCK_CNCSIDE_P	1007
#define	VC3_BCK_CNCSIDE_R	1008
#define	VC3_BCK_CNCTOP_D	1009
#define	VC3_BCK_CNCTOP_M	1010
#define	VC3_BCK_CNCTOP_P	1011
#define	VC3_BCK_CNCTOP_R	1012
#define	VC3_CNC01_D	5001
#define	VC3_CNC01_M	5002
#define	VC3_CNC01_P	5003
#define	VC3_CNC01_R	5004
#define	VC3_CNC02INTL1_D	5005
#define	VC3_CNC02INTL1_M	5006
#define	VC3_CNC02INTL1_P	5007
#define	VC3_CNC02INTL1_R	5008
#define	VC3_CNC02INTL2_D	5009
#define	VC3_CNC02INTL2_M	5010
#define	VC3_CNC02INTL2_P	5011
#define	VC3_CNC02INTL2_R	5012
#define	VC3_CNC02_D	5013
#define	VC3_CNC02_M	5014
#define	VC3_CNC02_P	5015
#define	VC3_CNC02_R	5016
#define	VC3_CNC03INTL_D	5017
#define	VC3_CNC03INTL_M	5018
#define	VC3_CNC03INTL_P	5019
#define	VC3_CNC03INTL_R	5020
#define	VC3_CNC03_D	5021
#define	VC3_CNC03_M	5022
#define	VC3_CNC03_P	5023
#define	VC3_CNC03_R	5024
#define	VC3_CNC04INTL_D	5025
#define	VC3_CNC04INTL_M	5026
#define	VC3_CNC04INTL_P	5027
#define	VC3_CNC04INTL_R	5028
#define	VC3_CNC04_D	5029
#define	VC3_CNC04_M	5030
#define	VC3_CNC04_P	5031
#define	VC3_CNC04_R	5032
#define	VC3_CNC05_D	5033
#define	VC3_CNC05_M	5034
#define	VC3_CNC05_P	5035
#define	VC3_CNC05_R	5036
#define	VC3_CNC06_D	5037
#define	VC3_CNC06_M	5038
#define	VC3_CNC06_P	5039
#define	VC3_CNC06_R	5040
#define	VC3_CNC07INTL1_D	5041
#define	VC3_CNC07INTL1_M	5042
#define	VC3_CNC07INTL1_P	5043
#define	VC3_CNC07INTL1_R	5044
#define	VC3_CNC07INTL2_D	5045
#define	VC3_CNC07INTL2_M	5046
#define	VC3_CNC07INTL2_P	5047
#define	VC3_CNC07INTL2_R	5048
#define	VC3_CNC07_D	5049
#define	VC3_CNC07_M	5050
#define	VC3_CNC07_P	5051
#define	VC3_CNC07_R	5052
#define	VC3_CNC08INTL1_D	5053
#define	VC3_CNC08INTL1_M	5054
#define	VC3_CNC08INTL1_P	5055
#define	VC3_CNC08INTL1_R	5056
#define	VC3_CNC08INTL2_D	5057
#define	VC3_CNC08INTL2_M	5058
#define	VC3_CNC08INTL2_P	5059
#define	VC3_CNC08INTL2_R	5060
#define	VC3_CNC08_D	5061
#define	VC3_CNC08_M	5062
#define	VC3_CNC08_P	5063
#define	VC3_CNC08_R	5064
#define	VC3_CNC09INTL_D	5065
#define	VC3_CNC09INTL_M	5066
#define	VC3_CNC09INTL_P	5067
#define	VC3_CNC09INTL_R	5068
#define	VC3_CNC09_D	5069
#define	VC3_CNC09_M	5070
#define	VC3_CNC09_P	5071
#define	VC3_CNC09_R	5072
#define	VC3_CNC10INTL_D	5073
#define	VC3_CNC10INTL_M	5074
#define	VC3_CNC10INTL_P	5075
#define	VC3_CNC10INTL_R	5076
#define	VC3_CNC10_D	5077
#define	VC3_CNC10_M	5078
#define	VC3_CNC10_P	5079
#define	VC3_CNC10_R	5080
#define	VC3_CNC11INTL_D	5081
#define	VC3_CNC11INTL_M	5082
#define	VC3_CNC11INTL_P	5083
#define	VC3_CNC11INTL_R	5084
#define	VC3_CNC11_D	5085
#define	VC3_CNC11_M	5086
#define	VC3_CNC11_P	5087
#define	VC3_CNC11_R	5088
#define	VC3_CNC12INTL_D	5089
#define	VC3_CNC12INTL_M	5090
#define	VC3_CNC12INTL_P	5091
#define	VC3_CNC12INTL_R	5092
#define	VC3_CNC12_D	5093
#define	VC3_CNC12_M	5094
#define	VC3_CNC12_P	5095
#define	VC3_CNC12_R	5096
#define	VC3_CNC13INTL_D	5097
#define	VC3_CNC13INTL_M	5098
#define	VC3_CNC13INTL_P	5099
#define	VC3_CNC13INTL_R	5100
#define	VC3_CNC13_D	5101
#define	VC3_CNC13_M	5102
#define	VC3_CNC13_P	5103
#define	VC3_CNC13_R	5104
#define	VC3_CNC14INTL_D	5105
#define	VC3_CNC14INTL_M	5106
#define	VC3_CNC14INTL_P	5107
#define	VC3_CNC14INTL_R	5108
#define	VC3_CNC14_D	5109
#define	VC3_CNC14_M	5110
#define	VC3_CNC14_P	5111
#define	VC3_CNC14_R	5112
#define	VC3_CNC15_D	5113
#define	VC3_CNC15_M	5114
#define	VC3_CNC15_P	5115
#define	VC3_CNC15_R	5116
#define	VC3_CNC16_D	5117
#define	VC3_CNC16_M	5118
#define	VC3_CNC16_P	5119
#define	VC3_CNC16_R	5120
#define	VC3_CNC17_D	5121
#define	VC3_CNC17_M	5122
#define	VC3_CNC17_P	5123
#define	VC3_CNC17_R	5124
#define	VC3_CNC18INTL_D	5125
#define	VC3_CNC18INTL_M	5126
#define	VC3_CNC18INTL_P	5127
#define	VC3_CNC18INTL_R	5128
#define	VC3_CNC18_D	5129
#define	VC3_CNC18_M	5130
#define	VC3_CNC18_P	5131
#define	VC3_CNC18_R	5132
#define	VC3_CNC19_D	5133
#define	VC3_CNC19_M	5134
#define	VC3_CNC19_P	5135
#define	VC3_CNC19_R	5136
#define	VC3_CNC20INTL_D	5137
#define	VC3_CNC20INTL_M	5138
#define	VC3_CNC20INTL_P	5139
#define	VC3_CNC20INTL_R	5140
#define	VC3_CNC20_D	5141
#define	VC3_CNC20_M	5142
#define	VC3_CNC20_P	5143
#define	VC3_CNC20_R	5144
#define	VC3_CNC21_D	5145
#define	VC3_CNC21_M	5146
#define	VC3_CNC21_P	5147
#define	VC3_CNC21_R	5148
#define	VC3_CNC22INTL1_D	5149
#define	VC3_CNC22INTL1_M	5150
#define	VC3_CNC22INTL1_P	5151
#define	VC3_CNC22INTL1_R	5152
#define	VC3_CNC22INTL2_D	5153
#define	VC3_CNC22INTL2_M	5154
#define	VC3_CNC22INTL2_P	5155
#define	VC3_CNC22INTL2_R	5156
#define	VC3_CNC22INTL3_D	5157
#define	VC3_CNC22INTL3_M	5158
#define	VC3_CNC22INTL3_P	5159
#define	VC3_CNC22INTL3_R	5160
#define	VC3_CNC22_D	5161
#define	VC3_CNC22_M	5162
#define	VC3_CNC22_P	5163
#define	VC3_CNC22_R	5164
#define	VC3_CNC23_D	5165
#define	VC3_CNC23_M	5166
#define	VC3_CNC23_P	5167
#define	VC3_CNC23_R	5168
#define	VC3_CNC24INTL_D	5169
#define	VC3_CNC24INTL_M	5170
#define	VC3_CNC24INTL_P	5171
#define	VC3_CNC24INTL_R	5172
#define	VC3_CNC24_D	5173
#define	VC3_CNC24_M	5174
#define	VC3_CNC24_P	5175
#define	VC3_CNC24_R	5176
#define	VC3_CNC25INTL_D	5177
#define	VC3_CNC25INTL_M	5178
#define	VC3_CNC25INTL_P	5179
#define	VC3_CNC25INTL_R	5180
#define	VC3_CNC25_D	5181
#define	VC3_CNC25_M	5182
#define	VC3_CNC25_P	5183
#define	VC3_CNC25_R	5184
#define	VC3_CNC26INTL_D	5185
#define	VC3_CNC26INTL_M	5186
#define	VC3_CNC26INTL_P	5187
#define	VC3_CNC26INTL_R	5188
#define	VC3_CNC26SIDE_D	5189
#define	VC3_CNC26SIDE_M	5190
#define	VC3_CNC26SIDE_P	5191
#define	VC3_CNC26SIDE_R	5192
#define	VC3_CNC26_D	5193
#define	VC3_CNC26_M	5194
#define	VC3_CNC26_P	5195
#define	VC3_CNC26_R	5196
#define	VC3_CNC27_D	5197
#define	VC3_CNC27_M	5198
#define	VC3_CNC27_P	5199
#define	VC3_CNC27_R	5200
#define	VC3_CNC28INTL_D	5201
#define	VC3_CNC28INTL_M	5202
#define	VC3_CNC28INTL_P	5203
#define	VC3_CNC28INTL_R	5204
#define	VC3_CNC28_D	5205
#define	VC3_CNC28_M	5206
#define	VC3_CNC28_P	5207
#define	VC3_CNC28_R	5208
#define	VC3_CNC29INTL_D	5209
#define	VC3_CNC29INTL_M	5210
#define	VC3_CNC29INTL_P	5211
#define	VC3_CNC29INTL_R	5212
#define	VC3_CNC29_D	5213
#define	VC3_CNC29_M	5214
#define	VC3_CNC29_P	5215
#define	VC3_CNC29_R	5216
#define	VC3_CNC30_D	5217
#define	VC3_CNC30_M	5218
#define	VC3_CNC30_P	5219
#define	VC3_CNC30_R	5220
#define	VC3_CNC31_D	5221
#define	VC3_CNC31_M	5222
#define	VC3_CNC31_P	5223
#define	VC3_CNC31_R	5224
#define	VC3_CNC32INTL_D	5225
#define	VC3_CNC32INTL_M	5226
#define	VC3_CNC32INTL_P	5227
#define	VC3_CNC32INTL_R	5228
#define	VC3_CNC32_D	5229
#define	VC3_CNC32_M	5230
#define	VC3_CNC32_P	5231
#define	VC3_CNC32_R	5232
#define	VC3_CNC33INTL_D	5233
#define	VC3_CNC33INTL_M	5234
#define	VC3_CNC33INTL_P	5235
#define	VC3_CNC33INTL_R	5236
#define	VC3_CNC33_D	5237
#define	VC3_CNC33_M	5238
#define	VC3_CNC33_P	5239
#define	VC3_CNC33_R	5240
#define	VC3_CNC34_D	5241
#define	VC3_CNC34_M	5242
#define	VC3_CNC34_P	5243
#define	VC3_CNC34_R	5244
#define	VC3_CNC35_D	5245
#define	VC3_CNC35_M	5246
#define	VC3_CNC35_P	5247
#define	VC3_CNC35_R	5248
#define	VC3_CNC36_D	5249
#define	VC3_CNC36_M	5250
#define	VC3_CNC36_P	5251
#define	VC3_CNC36_R	5252
#define	VC3_CNC37_D	5253
#define	VC3_CNC37_M	5254
#define	VC3_CNC37_P	5255
#define	VC3_CNC37_R	5256
#define	VC3_CNC38_D	5257
#define	VC3_CNC38_M	5258
#define	VC3_CNC38_P	5259
#define	VC3_CNC38_R	5260
#define	VC3_CNCRUD1_D	5261
#define	VC3_CNCRUD1_M	5262
#define	VC3_CNCRUD1_P	5263
#define	VC3_CNCRUD1_R	5264
#define	VC3_CNCRUD2_D	5265
#define	VC3_CNCRUD2_M	5266
#define	VC3_CNCRUD2_P	5267
#define	VC3_CNCRUD2_R	5268
#define	VC3_CNCRUD3_D	5269
#define	VC3_CNCRUD3_M	5270
#define	VC3_CNCRUD3_P	5271
#define	VC3_CNCRUD3_R	5272
#define	VC3_CNCRUDHNDLSIDE_D	5273
#define	VC3_CNCRUDHNDLSIDE_M	5274
#define	VC3_CNCRUDHNDLSIDE_P	5275
#define	VC3_CNCRUDHNDLSIDE_R	5276
#define	VC3_CNCRUDLEVER_D	5277
#define	VC3_CNCRUDLEVER_M	5278
#define	VC3_CNCRUDLEVER_P	5279
#define	VC3_CNCRUDLEVER_R	5280
#define	VC3_CNCRUDSTOPSIDE_D	5281
#define	VC3_CNCRUDSTOPSIDE_M	5282
#define	VC3_CNCRUDSTOPSIDE_P	5283
#define	VC3_CNCRUDSTOPSIDE_R	5284
#define	VC3_CNCRUDSTOPTOP_D	5285
#define	VC3_CNCRUDSTOPTOP_M	5286
#define	VC3_CNCRUDSTOPTOP_P	5287
#define	VC3_CNCRUDSTOPTOP_R	5288
#define	VC3_CNCRUDSTOP_D	5289
#define	VC3_CNCRUDSTOP_M	5290
#define	VC3_CNCRUDSTOP_P	5291
#define	VC3_CNCRUDSTOP_R	5292

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
