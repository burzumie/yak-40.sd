#include <boost/algorithm/string.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <iostream>
#include <map>
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>

#define HAVE_DAY_PANEL
#define HAVE_MIX_PANEL
#define HAVE_PLF_PANEL
#define HAVE_RED_PANEL

namespace fs = boost::filesystem;
namespace po = boost::program_options;

using namespace std;
using namespace boost;

const int counterstep=1;

void create_rc_header(ofstream &rc, string incres="res.h")
{
	rc << "/*=========================================================================\n\n";
	rc << "  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004\n\n";
	rc << "  $Archive: $\n\n";
	rc << "  Last modification:\n";
	rc << "    $Date: $\n";
	rc << "    $Revision: $\n";
	rc << "    $Author: $\n\n";
	rc << "  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]\n\n";
	rc << "  All rights reserved.\n\n";
	rc << "=========================================================================*/\n\n";

	rc << "#include \"" << incres << "\"" << endl;
	rc << "#define APSTUDIO_READONLY_SYMBOLS\n";
	rc << "#include \"afxres.h\"\n";
	rc << "#undef APSTUDIO_READONLY_SYMBOLS\n";
	rc << "#if !defined(AFX_RESOURCE_DLL) || defined(AFX_TARG_RUS)\n";
	rc << "#ifdef _WIN32\n";
	rc << "LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US\n";
	rc << "#pragma code_page(1252)\n";
	rc << "#endif\n";
	rc << "#ifndef _MAC\n";
	rc << "VS_VERSION_INFO VERSIONINFO\n";
	rc << " FILEVERSION VERSION_MAJOR,VERSION_MINOR,0,VERSION_BUILD\n";
	rc << " PRODUCTVERSION VERSION_MAJOR,VERSION_MINOR,0,VERSION_BUILD\n";
	rc << " FILEFLAGSMASK 0x3fL\n";
	rc << " FILEFLAGS 0x0L\n";
	rc << " FILEOS 0x10004L\n";
	rc << " FILETYPE 0x1L\n";
	rc << " FILESUBTYPE 0x0L\n";
	rc << "BEGIN\n";
	rc << "    BLOCK \"StringFileInfo\"\n";
	rc << "    BEGIN\n";
	rc << "        BLOCK \"040904b0\"\n";
	rc << "        BEGIN\n";
	rc << "            VALUE \"CompanyName\", COMPANY_NAME\n";
	rc << "            VALUE \"FileDescription\", FILE_DESCRIPTION\n";
	rc << "            VALUE \"FileVersion\", VERSION_STRING\n";
	rc << "            VALUE \"LegalCopyright\", LEGAL_COPYRIGHT\n";
	rc << "            VALUE \"LegalTrademarks\", LEGAL_TRADEMARK\n";
	rc << "            VALUE \"ProductName\", PRODUCT_NAME\n";
	rc << "            VALUE \"ProductVersion\", VERSION_STRING\n";
	rc << "        END\n";
	rc << "    END\n";
	rc << "    BLOCK \"VarFileInfo\"\n";
	rc << "    BEGIN\n";
	rc << "        VALUE \"Translation\", 0x419, 1200\n";
	rc << "    END\n";
	rc << "END\n";
	rc << "#endif\n";
	rc << "#ifdef APSTUDIO_INVOKED\n";
	rc << "1 TEXTINCLUDE DISCARDABLE\n";
	rc << "BEGIN\n";
	rc << "    \"resource.h\\0\"\n";
	rc << "END\n";
	rc << "2 TEXTINCLUDE DISCARDABLE\n";
	rc << "BEGIN\n";
	rc << "    \"#include \"\"afxres.h\"\"\\r\\n\"\n";
	rc << "    \"\\0\"\n";
	rc << "END\n";
	rc << "3 TEXTINCLUDE DISCARDABLE\n";
	rc << "BEGIN\n";
	rc << "    \"\\r\\n\"\n";
	rc << "    \"\\0\"\n";
	rc << "END\n";
	rc << "#endif\n";
	rc << "#endif\n\n";

}

void create_rc_footer(ofstream &rc)
{
	rc << "\n#ifndef APSTUDIO_INVOKED\n";
	rc << "#endif\n";
}

void create_h_header(ofstream &hdr, string incsz="res_size.h")
{
	hdr << "/*=========================================================================\n\n";
	hdr << "  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004\n\n";
	hdr << "  $Archive: $\n\n";
	hdr << "  Last modification:\n";
	hdr << "    $Date: $\n";
	hdr << "    $Revision: $\n";
	hdr << "    $Author: $\n\n";
	hdr << "  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]\n\n";
	hdr << "  All rights reserved.\n\n";
	hdr << "=========================================================================*/\n\n";
	hdr << "#include \"../Version.h\"\n\n";
	hdr << "#include \"" << incsz << "\"\n\n";
}

void create_h_footer(ofstream &hdr)
{
	hdr << "\n#ifdef APSTUDIO_INVOKED\n";
	hdr << "#ifndef APSTUDIO_READONLY_SYMBOLS\n";
	hdr << "#define _APS_NEXT_RESOURCE_VALUE        101\n";
	hdr << "#define _APS_NEXT_COMMAND_VALUE         40001\n";
	hdr << "#define _APS_NEXT_CONTROL_VALUE         1000\n";
	hdr << "#define _APS_NEXT_SYMED_VALUE           101\n";
	hdr << "#endif\n";
	hdr << "#endif\n";
}

void create_hs_header(ofstream &hdr2)
{
	hdr2 << "/*=========================================================================\n\n";
	hdr2 << "  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004\n\n";
	hdr2 << "  $Archive: $\n\n";
	hdr2 << "  Last modification:\n";
	hdr2 << "    $Date: $\n";
	hdr2 << "    $Revision: $\n";
	hdr2 << "    $Author: $\n\n";
	hdr2 << "  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]\n\n";
	hdr2 << "  All rights reserved.\n\n";
	hdr2 << "=========================================================================*/\n\n";
}

string get_param(string p, string f)
{
	string aaa=to_upper_copy(p)+"_"+to_upper_copy(replace_last_copy(f,".bmp",""));
	return ireplace_all_copy(aaa,"-","_");
}

void write_line_to_rc(ofstream &rc, string p, string f) 
{
	string name=f; //get_param(p,f);
	string name2=get_param(p,f);
	if((find_last(name,"_D.")||find_last(name,"_D_")||find_last(name,"_d.")||find_last(name,"_d_"))) {
		#ifdef HAVE_DAY_PANEL
		rc << get_param(p,f) << "\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
		#endif
	}
	else 
	if((find_last(name,"_M.")||find_last(name,"_M_")||find_last(name,"_m.")||find_last(name,"_m_"))) {
		#ifdef HAVE_MIX_PANEL
		rc << get_param(p,f) << "\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
		#endif
	}
	else 
	if((find_last(name,"_P.")||find_last(name,"_P_")||find_last(name,"_p.")||find_last(name,"_p_"))) {
		#ifdef HAVE_PLF_PANEL
		rc << get_param(p,f) << "\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
		#endif
	}
	else 
	if((find_last(name,"_R.")||find_last(name,"_R_")||find_last(name,"_r.")||find_last(name,"_r_"))) {
		#ifdef HAVE_RED_PANEL
		rc << get_param(p,f) << "\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
		#endif
	}
	else 
	rc << get_param(p,f) << "\tBITMAP DISCARDABLE\t" << "\"" << p+"\\\\"+f << "\"" << endl;
	
}

void write_line_to_h(ofstream &hdr, string p, string f, int &counter) 
{
	string name=f; //get_param(p,f);
	string name2=get_param(p,f);
	if((find_last(name,"_D.")||find_last(name,"_D_")||find_last(name,"_d.")||find_last(name,"_d_"))) {
		#ifdef HAVE_DAY_PANEL
		hdr << "#define\t" << get_param(p,f) << "\t" << counter << endl;
		counter+=counterstep;
		#endif
	}
	else 
	if((find_last(name,"_M.")||find_last(name,"_M_")||find_last(name,"_m.")||find_last(name,"_m_"))) {
		#ifdef HAVE_MIX_PANEL
		hdr << "#define\t" << get_param(p,f) << "\t" << counter << endl;
		counter+=counterstep;
		#endif
	}
	else 
	if((find_last(name,"_P.")||find_last(name,"_P_")||find_last(name,"_p.")||find_last(name,"_p_"))) {
		#ifdef HAVE_PLF_PANEL
		hdr << "#define\t" << get_param(p,f) << "\t" << counter << endl;
		counter+=counterstep;
		#endif
	}
	else 
	if((find_last(name,"_R.")||find_last(name,"_R_")||find_last(name,"_r.")||find_last(name,"_r_"))) {
		#ifdef HAVE_RED_PANEL
		hdr << "#define\t" << get_param(p,f) << "\t" << counter << endl;
		counter+=counterstep;
		#endif
	}
	else {
		hdr << "#define\t" << get_param(p,f) << "\t" << counter << endl;
		counter+=counterstep;
	}
}

void write_line_to_hs(ofstream &hdr2, string p, string f) 
{
	string name=f; //get_param(p,f);
	string name2=get_param(p,f);
	BITMAP bm;
	HBITMAP b=(HBITMAP)LoadImage(NULL,(p+"\\\\"+f).c_str(),IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	GetObject(b,sizeof(bm),&bm);
	if((find_last(name,"_D.")||find_last(name,"_D_")||find_last(name,"_d.")||find_last(name,"_d_"))) {
		#ifdef HAVE_DAY_PANEL
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
		#endif
	}
	else 
	if((find_last(name,"_M.")||find_last(name,"_M_")||find_last(name,"_m.")||find_last(name,"_m_"))) {
		#ifdef HAVE_MIX_PANEL
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
		#endif
	}
	else 
	if((find_last(name,"_P.")||find_last(name,"_P_")||find_last(name,"_p.")||find_last(name,"_p_"))) {
		#ifdef HAVE_PLF_PANEL
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
		#endif
	}
	else 
	if((find_last(name,"_R.")||find_last(name,"_R_")||find_last(name,"_r.")||find_last(name,"_r_"))) {
		#ifdef HAVE_RED_PANEL
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
		#endif
	}
	else {
		hdr2 << "#define\t" << get_param(p,f) << "_SX\t" << bm.bmWidth << endl;
		hdr2 << "#define\t" << get_param(p,f) << "_SY\t" << bm.bmHeight << endl;	
	}
	DeleteObject((HBITMAP)b);
}

bool scandir(string initdir,int &counter,ofstream &rcout,ofstream &hout,ofstream &hsout,string stw="")
{
	bool ret=false;

	fs::path dir(initdir);

	fs::directory_iterator end_iter;
	for ( fs::directory_iterator dir_itr( dir ); dir_itr != end_iter; ++dir_itr ) {
		try {
			if ( !fs::is_directory( *dir_itr ) ) {
				if(dir.leaf()!="res") {
						string path1=dir.leaf();
						string file1=dir_itr->leaf();
						if(stw!="") {
							if(istarts_with(file1,stw)&&iends_with(file1,".bmp")) {
								write_line_to_rc(rcout, path1, file1);
								write_line_to_h(hout, path1, file1, counter);
								write_line_to_hs(hsout, path1, file1);
								ret=true;
							}
						} else {
							if(iends_with(file1,".bmp")) {
								write_line_to_rc(rcout, path1, file1);
								write_line_to_h(hout, path1, file1, counter);
								write_line_to_hs(hsout, path1, file1);
							}
						}
				}
			}
		}
		catch ( const std::exception & ex ) {
			std::cout << dir_itr->leaf() << " " << ex.what() << std::endl;
		}
	}
	return ret;

}

int start=1001;
int step=4000;
int mult=1;

#define SCAN(dir) /**/ \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"azs"))           \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"btn"))           \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"cov"))           \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"lmp"))           \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"mov"))           \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"ndl"))           \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"sld"))           \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"swt"))           \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl_0"))         \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl_1"))         \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl_2"))         \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl_3"))         \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl_gear"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl_ark"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl_gmk"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tblt"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl0"))         \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl1"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl2"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl3"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl4"))         \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"tbl5"))         \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"alpha"))         \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"background"))    \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"blk"))           \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_agb"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_altim"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_iku"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_kppms"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_rv"))        \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_gmk_"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_gmklat"))    \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_temp"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_volt"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_ap"))        \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_kurs"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_sgu"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_pres"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_diff"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_begi"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_obs1"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_obs2"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_vhf1"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_dme"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_xpdr1"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_xpdr2"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_xpdr3"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_xpdr4"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_ark"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_1dme"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_ark"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_dme"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_gmk"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_iku"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_kppms"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_kurs"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_kus"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_mp"))        \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_obs"))       \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_pres"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_uvid"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_vhf1"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_vhf2"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_vhf2"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"glt_ark1"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"glt_ark2"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_xdpr1"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_xdpr2"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_xdpr3"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_xdpr4"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"knb_xdprmode"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_xpdr1"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_xdpr1"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_xpdr2"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_xdpr2"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_xpdr3"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_xdpr3"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_xpdr4"))     \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_xdpr4"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_vd"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_rv"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_var"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_com1"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_com2"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_adf1"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"scl_adf2"))      \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"bck"))           \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"crs"))           \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"knb_mp"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"knb_frq1"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"knb_frq2"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"knb0"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"knb1"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"knbt"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"pri"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"yoke"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"frm"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"rp"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"rof"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"npl"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"mat"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"apb"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"tex"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"txt"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"cnc"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"bbk"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"det"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"glare"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"startup"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"ovc"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"wbc"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"clmn"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"rad"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"sts"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"spg"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"pts"))         \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"galeta"))         \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"micro"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"spot"))      \
		counter=start+step*mult++;                              \
		if(scandir(dir,counter,rcout,hout,hsout,"frt"))      \
		counter=start+step*mult++;				\
		if(scandir(dir,counter,rcout,hout,hsout,"dummy"))           

void createdir(string name,char *dir1,char *dir2,char *dir3,char *dir4,char *dir5,char *dir6)
{
	ofstream rcout((name+"_res.rc").c_str());
	ofstream hout((name+"_res.h").c_str());
	ofstream hsout((name+"_size.h").c_str());

	create_rc_header(rcout,name+"_res.h");
	create_h_header(hout,name+"_size.h");
	create_hs_header(hsout);

	int counter=1000;                                               
	mult=0;

	if(dir1) {
		SCAN(dir1);
	}
	if(dir2) {
		SCAN(dir2);
	}
	if(dir3) {
		SCAN(dir3);
	}
	if(dir4) {
		SCAN(dir4);
	}
	if(dir5) {
		SCAN(dir5);
	}
	if(dir6) {
		SCAN(dir6);
	}

	create_rc_footer(rcout);
	create_h_footer(hout);

}

void main(int argc,char *argv[])
{
	createdir( "vc1", "vc1",NULL,NULL,NULL,NULL,NULL);
	createdir( "vc2", "vc2",NULL,NULL,NULL,NULL,NULL);
	createdir( "vc3", "vc3",NULL,NULL,NULL,NULL,NULL);
	createdir( "vc4", "vc4",NULL,NULL,NULL,NULL,NULL);
	createdir( "vc5", "vc5",NULL,NULL,NULL,NULL,NULL);
	createdir( "vc6", "vc6",NULL,NULL,NULL,NULL,NULL);
	createdir( "vc7", "vc7",NULL,NULL,NULL,NULL,NULL);
	createdir( "vc8", "vc8",NULL,NULL,NULL,NULL,NULL);
	createdir( "vc9", "vc9",NULL,NULL,NULL,NULL,NULL);
	createdir("vc10","vc10",NULL,NULL,NULL,NULL,NULL);
	createdir("vc11","vc11",NULL,NULL,NULL,NULL,NULL);
}
