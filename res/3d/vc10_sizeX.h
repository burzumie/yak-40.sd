/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#define	VC10_BTN01TOP_D_SX	56
#define	VC10_BTN01TOP_D_SY	34
#define	VC10_BTN01TOP_M_SX	56
#define	VC10_BTN01TOP_M_SY	34
#define	VC10_BTN01TOP_P_SX	56
#define	VC10_BTN01TOP_P_SY	34
#define	VC10_BTN01TOP_R_SX	56
#define	VC10_BTN01TOP_R_SY	34
#define	VC10_BTN01_D_SX	52
#define	VC10_BTN01_D_SY	52
#define	VC10_BTN01_M_SX	52
#define	VC10_BTN01_M_SY	52
#define	VC10_BTN01_P_SX	52
#define	VC10_BTN01_P_SY	52
#define	VC10_BTN01_R_SX	52
#define	VC10_BTN01_R_SY	52
#define	VC10_BTN_CLCKL_D_SX	34
#define	VC10_BTN_CLCKL_D_SY	34
#define	VC10_BTN_CLCKL_M_SX	34
#define	VC10_BTN_CLCKL_M_SY	34
#define	VC10_BTN_CLCKL_P_SX	34
#define	VC10_BTN_CLCKL_P_SY	34
#define	VC10_BTN_CLCKL_R_SX	34
#define	VC10_BTN_CLCKL_R_SY	34
#define	VC10_BTN_CLCKR_D_SX	34
#define	VC10_BTN_CLCKR_D_SY	34
#define	VC10_BTN_CLCKR_M_SX	34
#define	VC10_BTN_CLCKR_M_SY	34
#define	VC10_BTN_CLCKR_P_SX	34
#define	VC10_BTN_CLCKR_P_SY	34
#define	VC10_BTN_CLCKR_R_SX	34
#define	VC10_BTN_CLCKR_R_SY	34
#define	VC10_LMP_AP01_D_00_SX	127
#define	VC10_LMP_AP01_D_00_SY	77
#define	VC10_LMP_AP01_D_01_SX	127
#define	VC10_LMP_AP01_D_01_SY	77
#define	VC10_LMP_AP01_M_00_SX	127
#define	VC10_LMP_AP01_M_00_SY	77
#define	VC10_LMP_AP01_M_01_SX	127
#define	VC10_LMP_AP01_M_01_SY	77
#define	VC10_LMP_AP01_P_00_SX	127
#define	VC10_LMP_AP01_P_00_SY	77
#define	VC10_LMP_AP01_P_01_SX	127
#define	VC10_LMP_AP01_P_01_SY	77
#define	VC10_LMP_AP01_R_00_SX	127
#define	VC10_LMP_AP01_R_00_SY	77
#define	VC10_LMP_AP01_R_01_SX	127
#define	VC10_LMP_AP01_R_01_SY	77
#define	VC10_LMP_AP02_D_00_SX	122
#define	VC10_LMP_AP02_D_00_SY	77
#define	VC10_LMP_AP02_D_01_SX	122
#define	VC10_LMP_AP02_D_01_SY	77
#define	VC10_LMP_AP02_M_00_SX	122
#define	VC10_LMP_AP02_M_00_SY	77
#define	VC10_LMP_AP02_M_01_SX	122
#define	VC10_LMP_AP02_M_01_SY	77
#define	VC10_LMP_AP02_P_00_SX	122
#define	VC10_LMP_AP02_P_00_SY	77
#define	VC10_LMP_AP02_P_01_SX	122
#define	VC10_LMP_AP02_P_01_SY	77
#define	VC10_LMP_AP02_R_00_SX	122
#define	VC10_LMP_AP02_R_00_SY	77
#define	VC10_LMP_AP02_R_01_SX	122
#define	VC10_LMP_AP02_R_01_SY	77
#define	VC10_LMP_AP03_D_00_SX	122
#define	VC10_LMP_AP03_D_00_SY	77
#define	VC10_LMP_AP03_D_01_SX	122
#define	VC10_LMP_AP03_D_01_SY	77
#define	VC10_LMP_AP03_M_00_SX	122
#define	VC10_LMP_AP03_M_00_SY	77
#define	VC10_LMP_AP03_M_01_SX	122
#define	VC10_LMP_AP03_M_01_SY	77
#define	VC10_LMP_AP03_P_00_SX	122
#define	VC10_LMP_AP03_P_00_SY	77
#define	VC10_LMP_AP03_P_01_SX	122
#define	VC10_LMP_AP03_P_01_SY	77
#define	VC10_LMP_AP03_R_00_SX	122
#define	VC10_LMP_AP03_R_00_SY	77
#define	VC10_LMP_AP03_R_01_SX	122
#define	VC10_LMP_AP03_R_01_SY	77
#define	VC10_NDL_CLOCKDWNSEC_D_SX	61
#define	VC10_NDL_CLOCKDWNSEC_D_SY	12
#define	VC10_NDL_CLOCKDWNSEC_M_SX	61
#define	VC10_NDL_CLOCKDWNSEC_M_SY	12
#define	VC10_NDL_CLOCKDWNSEC_P_SX	61
#define	VC10_NDL_CLOCKDWNSEC_P_SY	12
#define	VC10_NDL_CLOCKDWNSEC_R_SX	61
#define	VC10_NDL_CLOCKDWNSEC_R_SY	12
#define	VC10_NDL_CLOCKHRS_D_SX	126
#define	VC10_NDL_CLOCKHRS_D_SY	25
#define	VC10_NDL_CLOCKHRS_M_SX	126
#define	VC10_NDL_CLOCKHRS_M_SY	25
#define	VC10_NDL_CLOCKHRS_P_SX	126
#define	VC10_NDL_CLOCKHRS_P_SY	25
#define	VC10_NDL_CLOCKHRS_R_SX	126
#define	VC10_NDL_CLOCKHRS_R_SY	25
#define	VC10_NDL_CLOCKMIN_D_SX	133
#define	VC10_NDL_CLOCKMIN_D_SY	25
#define	VC10_NDL_CLOCKMIN_M_SX	133
#define	VC10_NDL_CLOCKMIN_M_SY	25
#define	VC10_NDL_CLOCKMIN_P_SX	133
#define	VC10_NDL_CLOCKMIN_P_SY	25
#define	VC10_NDL_CLOCKMIN_R_SX	133
#define	VC10_NDL_CLOCKMIN_R_SY	25
#define	VC10_NDL_CLOCKSEC_D_SX	131
#define	VC10_NDL_CLOCKSEC_D_SY	25
#define	VC10_NDL_CLOCKSEC_M_SX	131
#define	VC10_NDL_CLOCKSEC_M_SY	25
#define	VC10_NDL_CLOCKSEC_P_SX	131
#define	VC10_NDL_CLOCKSEC_P_SY	25
#define	VC10_NDL_CLOCKSEC_R_SX	131
#define	VC10_NDL_CLOCKSEC_R_SY	25
#define	VC10_NDL_CLOCKUPHRS_D_SX	42
#define	VC10_NDL_CLOCKUPHRS_D_SY	13
#define	VC10_NDL_CLOCKUPHRS_M_SX	42
#define	VC10_NDL_CLOCKUPHRS_M_SY	13
#define	VC10_NDL_CLOCKUPHRS_P_SX	42
#define	VC10_NDL_CLOCKUPHRS_P_SY	13
#define	VC10_NDL_CLOCKUPHRS_R_SX	42
#define	VC10_NDL_CLOCKUPHRS_R_SY	13
#define	VC10_NDL_CLOCKUPMIN_D_SX	50
#define	VC10_NDL_CLOCKUPMIN_D_SY	13
#define	VC10_NDL_CLOCKUPMIN_M_SX	50
#define	VC10_NDL_CLOCKUPMIN_M_SY	13
#define	VC10_NDL_CLOCKUPMIN_P_SX	50
#define	VC10_NDL_CLOCKUPMIN_P_SY	13
#define	VC10_NDL_CLOCKUPMIN_R_SX	50
#define	VC10_NDL_CLOCKUPMIN_R_SY	13
#define	VC10_KNB_AP_D_SX	150
#define	VC10_KNB_AP_D_SY	150
#define	VC10_KNB_AP_M_SX	150
#define	VC10_KNB_AP_M_SY	150
#define	VC10_KNB_AP_P_SX	150
#define	VC10_KNB_AP_P_SY	150
#define	VC10_KNB_AP_R_SX	150
#define	VC10_KNB_AP_R_SY	150
#define	VC10_SCL_VD10PRESSINTL_SX	227
#define	VC10_SCL_VD10PRESSINTL_SY	226
#define	VC10_SCL_VD10PRESSINTL_D_SX	227
#define	VC10_SCL_VD10PRESSINTL_D_SY	226
#define	VC10_SCL_VD10PRESSINTL_M_SX	227
#define	VC10_SCL_VD10PRESSINTL_M_SY	226
#define	VC10_SCL_VD10PRESSINTL_P_SX	227
#define	VC10_SCL_VD10PRESSINTL_P_SY	226
#define	VC10_SCL_VD10PRESSINTL_R_SX	227
#define	VC10_SCL_VD10PRESSINTL_R_SY	226
#define	VC10_SCL_VD10PRESSRUS_D_SX	227
#define	VC10_SCL_VD10PRESSRUS_D_SY	226
#define	VC10_SCL_VD10PRESSRUS_M_SX	227
#define	VC10_SCL_VD10PRESSRUS_M_SY	226
#define	VC10_SCL_VD10PRESSRUS_P_SX	227
#define	VC10_SCL_VD10PRESSRUS_P_SY	226
#define	VC10_SCL_VD10PRESSRUS_R_SX	227
#define	VC10_SCL_VD10PRESSRUS_R_SY	226
#define	VC10_BCK_CLOCK_D_SX	362
#define	VC10_BCK_CLOCK_D_SY	350
#define	VC10_BCK_CLOCK_M_SX	362
#define	VC10_BCK_CLOCK_M_SY	350
#define	VC10_BCK_CLOCK_P_SX	362
#define	VC10_BCK_CLOCK_P_SY	350
#define	VC10_BCK_CLOCK_R_SX	362
#define	VC10_BCK_CLOCK_R_SY	350
#define	VC10_KNB02_D_SX	108
#define	VC10_KNB02_D_SY	108
#define	VC10_KNB02_M_SX	108
#define	VC10_KNB02_M_SY	108
#define	VC10_KNB02_P_SX	108
#define	VC10_KNB02_P_SY	108
#define	VC10_KNB02_R_SX	108
#define	VC10_KNB02_R_SY	108
#define	VC10_KNB03_D_SX	120
#define	VC10_KNB03_D_SY	120
#define	VC10_KNB03_M_SX	120
#define	VC10_KNB03_M_SY	120
#define	VC10_KNB03_P_SX	120
#define	VC10_KNB03_P_SY	120
#define	VC10_KNB03_R_SX	120
#define	VC10_KNB03_R_SY	120
#define	VC10_KNB04_D_SX	110
#define	VC10_KNB04_D_SY	110
#define	VC10_KNB04_M_SX	110
#define	VC10_KNB04_M_SY	110
#define	VC10_KNB04_P_SX	110
#define	VC10_KNB04_P_SY	110
#define	VC10_KNB04_R_SX	110
#define	VC10_KNB04_R_SY	110
#define	VC10_KNB05_D_SX	140
#define	VC10_KNB05_D_SY	140
#define	VC10_KNB05_M_SX	140
#define	VC10_KNB05_M_SY	140
#define	VC10_KNB05_P_SX	140
#define	VC10_KNB05_P_SY	140
#define	VC10_KNB05_R_SX	140
#define	VC10_KNB05_R_SY	140
#define	VC10_KNB09_D_SX	123
#define	VC10_KNB09_D_SY	123
#define	VC10_KNB09_M_SX	123
#define	VC10_KNB09_M_SY	123
#define	VC10_KNB09_P_SX	123
#define	VC10_KNB09_P_SY	123
#define	VC10_KNB09_R_SX	123
#define	VC10_KNB09_R_SY	123
#define	VC10_KNB10_D_SX	110
#define	VC10_KNB10_D_SY	110
#define	VC10_KNB10_M_SX	110
#define	VC10_KNB10_M_SY	110
#define	VC10_KNB10_P_SX	110
#define	VC10_KNB10_P_SY	110
#define	VC10_KNB10_R_SX	110
#define	VC10_KNB10_R_SY	110
#define	VC10_KNB11_D_SX	121
#define	VC10_KNB11_D_SY	121
#define	VC10_KNB11_M_SX	121
#define	VC10_KNB11_M_SY	121
#define	VC10_KNB11_P_SX	121
#define	VC10_KNB11_P_SY	121
#define	VC10_KNB11_R_SX	121
#define	VC10_KNB11_R_SY	121
#define	VC10_KNB12_D_SX	110
#define	VC10_KNB12_D_SY	110
#define	VC10_KNB12_M_SX	110
#define	VC10_KNB12_M_SY	110
#define	VC10_KNB12_P_SX	110
#define	VC10_KNB12_P_SY	110
#define	VC10_KNB12_R_SX	110
#define	VC10_KNB12_R_SY	110
#define	VC10_KNB13_D_SX	116
#define	VC10_KNB13_D_SY	115
#define	VC10_KNB13_M_SX	116
#define	VC10_KNB13_M_SY	115
#define	VC10_KNB13_P_SX	116
#define	VC10_KNB13_P_SY	115
#define	VC10_KNB13_R_SX	116
#define	VC10_KNB13_R_SY	115
#define	VC10_KNBTOPAGB1_D_SX	109
#define	VC10_KNBTOPAGB1_D_SY	75
#define	VC10_KNBTOPAGB1_M_SX	109
#define	VC10_KNBTOPAGB1_M_SY	75
#define	VC10_KNBTOPAGB1_P_SX	109
#define	VC10_KNBTOPAGB1_P_SY	75
#define	VC10_KNBTOPAGB1_R_SX	109
#define	VC10_KNBTOPAGB1_R_SY	75
#define	VC10_KNBTOPRV_D_SX	106
#define	VC10_KNBTOPRV_D_SY	76
#define	VC10_KNBTOPRV_M_SX	106
#define	VC10_KNBTOPRV_M_SY	76
#define	VC10_KNBTOPRV_P_SX	106
#define	VC10_KNBTOPRV_P_SY	76
#define	VC10_KNBTOPRV_R_SX	106
#define	VC10_KNBTOPRV_R_SY	76
#define	VC10_KNBTOPUVID_D_SX	129
#define	VC10_KNBTOPUVID_D_SY	35
#define	VC10_KNBTOPUVID_M_SX	129
#define	VC10_KNBTOPUVID_M_SY	35
#define	VC10_KNBTOPUVID_P_SX	129
#define	VC10_KNBTOPUVID_P_SY	35
#define	VC10_KNBTOPUVID_R_SX	129
#define	VC10_KNBTOPUVID_R_SY	35
#define	VC10_KNBTOPVD10_D_SX	119
#define	VC10_KNBTOPVD10_D_SY	89
#define	VC10_KNBTOPVD10_M_SX	119
#define	VC10_KNBTOPVD10_M_SY	89
#define	VC10_KNBTOPVD10_P_SX	119
#define	VC10_KNBTOPVD10_P_SY	89
#define	VC10_KNBTOPVD10_R_SX	119
#define	VC10_KNBTOPVD10_R_SY	89
#define	VC10_APB01_BCK_D_SX	450
#define	VC10_APB01_BCK_D_SY	352
#define	VC10_APB01_BCK_M_SX	450
#define	VC10_APB01_BCK_M_SY	352
#define	VC10_APB01_BCK_P_SX	450
#define	VC10_APB01_BCK_P_SY	352
#define	VC10_APB01_BCK_R_SX	450
#define	VC10_APB01_BCK_R_SY	352
#define	VC10_APB02_BCK_D_SX	172
#define	VC10_APB02_BCK_D_SY	193
#define	VC10_APB02_BCK_M_SX	172
#define	VC10_APB02_BCK_M_SY	193
#define	VC10_APB02_BCK_P_SX	172
#define	VC10_APB02_BCK_P_SY	193
#define	VC10_APB02_BCK_R_SX	172
#define	VC10_APB02_BCK_R_SY	193
#define	VC10_TEX_APSCOBA_D_SX	69
#define	VC10_TEX_APSCOBA_D_SY	126
#define	VC10_TEX_APSCOBA_M_SX	69
#define	VC10_TEX_APSCOBA_M_SY	126
#define	VC10_TEX_APSCOBA_P_SX	69
#define	VC10_TEX_APSCOBA_P_SY	126
#define	VC10_TEX_APSCOBA_R_SX	69
#define	VC10_TEX_APSCOBA_R_SY	126
#define	VC10_TEX_BRAKE_D_SX	132
#define	VC10_TEX_BRAKE_D_SY	450
#define	VC10_TEX_BRAKE_M_SX	132
#define	VC10_TEX_BRAKE_M_SY	450
#define	VC10_TEX_BRAKE_P_SX	132
#define	VC10_TEX_BRAKE_P_SY	450
#define	VC10_TEX_BRAKE_R_SX	132
#define	VC10_TEX_BRAKE_R_SY	450
#define	VC10_TEX_PEDALBASE_D_SX	400
#define	VC10_TEX_PEDALBASE_D_SY	173
#define	VC10_TEX_PEDALBASE_M_SX	400
#define	VC10_TEX_PEDALBASE_M_SY	173
#define	VC10_TEX_PEDALBASE_P_SX	400
#define	VC10_TEX_PEDALBASE_P_SY	173
#define	VC10_TEX_PEDALBASE_R_SX	400
#define	VC10_TEX_PEDALBASE_R_SY	173
#define	VC10_TEX_PEDALBTM_D_SX	270
#define	VC10_TEX_PEDALBTM_D_SY	99
#define	VC10_TEX_PEDALBTM_M_SX	270
#define	VC10_TEX_PEDALBTM_M_SY	99
#define	VC10_TEX_PEDALBTM_P_SX	270
#define	VC10_TEX_PEDALBTM_P_SY	99
#define	VC10_TEX_PEDALBTM_R_SX	270
#define	VC10_TEX_PEDALBTM_R_SY	99
#define	VC10_TEX_PEDALTOP_D_SX	120
#define	VC10_TEX_PEDALTOP_D_SY	99
#define	VC10_TEX_PEDALTOP_M_SX	120
#define	VC10_TEX_PEDALTOP_M_SY	99
#define	VC10_TEX_PEDALTOP_P_SX	120
#define	VC10_TEX_PEDALTOP_P_SY	99
#define	VC10_TEX_PEDALTOP_R_SX	120
#define	VC10_TEX_PEDALTOP_R_SY	99
#define	VC10_TEX_PEDAL_BTM_D_SX	270
#define	VC10_TEX_PEDAL_BTM_D_SY	99
#define	VC10_TEX_PEDAL_BTM_M_SX	270
#define	VC10_TEX_PEDAL_BTM_M_SY	99
#define	VC10_TEX_PEDAL_BTM_P_SX	270
#define	VC10_TEX_PEDAL_BTM_P_SY	99
#define	VC10_TEX_PEDAL_BTM_R_SX	270
#define	VC10_TEX_PEDAL_BTM_R_SY	99
#define	VC10_TXT_ALTHLD_D_SX	89
#define	VC10_TXT_ALTHLD_D_SY	23
#define	VC10_TXT_ALTHLD_M_SX	89
#define	VC10_TXT_ALTHLD_M_SY	23
#define	VC10_TXT_ALTHLD_P_SX	89
#define	VC10_TXT_ALTHLD_P_SY	23
#define	VC10_TXT_ALTHLD_R_SX	89
#define	VC10_TXT_ALTHLD_R_SY	23
#define	VC10_TXT_APCMD_D_SX	89
#define	VC10_TXT_APCMD_D_SY	23
#define	VC10_TXT_APCMD_M_SX	89
#define	VC10_TXT_APCMD_M_SY	23
#define	VC10_TXT_APCMD_P_SX	89
#define	VC10_TXT_APCMD_P_SY	23
#define	VC10_TXT_APCMD_R_SX	89
#define	VC10_TXT_APCMD_R_SY	23
#define	VC10_TXT_OFFLEFT_D_SX	89
#define	VC10_TXT_OFFLEFT_D_SY	25
#define	VC10_TXT_OFFLEFT_M_SX	89
#define	VC10_TXT_OFFLEFT_M_SY	25
#define	VC10_TXT_OFFLEFT_P_SX	89
#define	VC10_TXT_OFFLEFT_P_SY	25
#define	VC10_TXT_OFFLEFT_R_SX	89
#define	VC10_TXT_OFFLEFT_R_SY	25
#define	VC10_TXT_OFFRIGHT_D_SX	89
#define	VC10_TXT_OFFRIGHT_D_SY	25
#define	VC10_TXT_OFFRIGHT_M_SX	89
#define	VC10_TXT_OFFRIGHT_M_SY	25
#define	VC10_TXT_OFFRIGHT_P_SX	89
#define	VC10_TXT_OFFRIGHT_P_SY	25
#define	VC10_TXT_OFFRIGHT_R_SX	89
#define	VC10_TXT_OFFRIGHT_R_SY	25
#define	VC10_TXT_PITCH_D_SX	89
#define	VC10_TXT_PITCH_D_SY	25
#define	VC10_TXT_PITCH_M_SX	89
#define	VC10_TXT_PITCH_M_SY	25
#define	VC10_TXT_PITCH_P_SX	89
#define	VC10_TXT_PITCH_P_SY	25
#define	VC10_TXT_PITCH_R_SX	89
#define	VC10_TXT_PITCH_R_SY	25
#define	VC10_TXT_PWR_D_SX	89
#define	VC10_TXT_PWR_D_SY	25
#define	VC10_TXT_PWR_M_SX	89
#define	VC10_TXT_PWR_M_SY	25
#define	VC10_TXT_PWR_P_SX	89
#define	VC10_TXT_PWR_P_SY	25
#define	VC10_TXT_PWR_R_SX	89
#define	VC10_TXT_PWR_R_SY	25
#define	VC10_TXT_RDY_D_SX	84
#define	VC10_TXT_RDY_D_SY	23
#define	VC10_TXT_RDY_M_SX	84
#define	VC10_TXT_RDY_M_SY	23
#define	VC10_TXT_RDY_P_SX	84
#define	VC10_TXT_RDY_P_SY	23
#define	VC10_TXT_RDY_R_SX	84
#define	VC10_TXT_RDY_R_SY	23
#define	VC10_TXT_TITLE_D_SX	151
#define	VC10_TXT_TITLE_D_SY	22
#define	VC10_TXT_TITLE_M_SX	151
#define	VC10_TXT_TITLE_M_SY	22
#define	VC10_TXT_TITLE_P_SX	151
#define	VC10_TXT_TITLE_P_SY	22
#define	VC10_TXT_TITLE_R_SX	151
#define	VC10_TXT_TITLE_R_SY	22
