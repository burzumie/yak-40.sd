/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "vc10_size_odX.h"

#define	VC10_BTN01TOP_D	1001
#define	VC10_BTN01_D	1002
#define	VC10_BTN_CLCKL_D	1003
#define	VC10_BTN_CLCKR_D	1004
#define	VC10_LMP_AP01_D_00	5001
#define	VC10_LMP_AP01_D_01	5002
#define	VC10_LMP_AP02_D_00	5003
#define	VC10_LMP_AP02_D_01	5004
#define	VC10_LMP_AP03_D_00	5005
#define	VC10_LMP_AP03_D_01	5006
#define	VC10_NDL_CLOCKDWNSEC_D	9001
#define	VC10_NDL_CLOCKHRS_D	9002
#define	VC10_NDL_CLOCKMIN_D	9003
#define	VC10_NDL_CLOCKSEC_D	9004
#define	VC10_NDL_CLOCKUPHRS_D	9005
#define	VC10_NDL_CLOCKUPMIN_D	9006
#define	VC10_KNB_AP_D	13001
#define	VC10_SCL_VD10PRESSINTL	17001
#define	VC10_SCL_VD10PRESSINTL_D	17002
#define	VC10_SCL_VD10PRESSRUS_D	17003
#define	VC10_BCK_CLOCK_D	21001
#define	VC10_KNB02_D	25001
#define	VC10_KNB03_D	25002
#define	VC10_KNB04_D	25003
#define	VC10_KNB05_D	25004
#define	VC10_KNB09_D	25005
#define	VC10_KNB10_D	29001
#define	VC10_KNB11_D	29002
#define	VC10_KNB12_D	29003
#define	VC10_KNB13_D	29004
#define	VC10_KNBTOPAGB1_D	33001
#define	VC10_KNBTOPRV_D	33002
#define	VC10_KNBTOPUVID_D	33003
#define	VC10_KNBTOPVD10_D	33004
#define	VC10_APB01_BCK_D	37001
#define	VC10_APB02_BCK_D	37002
#define	VC10_TEX_APSCOBA_D	41001
#define	VC10_TEX_BRAKE_D	41002
#define	VC10_TEX_PEDALBASE_D	41003
#define	VC10_TEX_PEDALBTM_D	41004
#define	VC10_TEX_PEDALTOP_D	41005
#define	VC10_TEX_PEDAL_BTM_D	41006
#define	VC10_TXT_ALTHLD_D	45001
#define	VC10_TXT_APCMD_D	45002
#define	VC10_TXT_OFFLEFT_D	45003
#define	VC10_TXT_OFFRIGHT_D	45004
#define	VC10_TXT_PITCH_D	45005
#define	VC10_TXT_PWR_D	45006
#define	VC10_TXT_RDY_D	45007
#define	VC10_TXT_TITLE_D	45008

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
