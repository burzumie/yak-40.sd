/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "vc1_size_odX.h"

#define	VC1_BCK_FLOOR1_D	1001
#define	VC1_BCK_FLOOR2_D	1002
#define	VC1_BCK_FRONTWALL_D	1003
#define	VC1_BCK_LEFTPULTTOP_D	1004
#define	VC1_BCK_LEFTPULTWALLBTM_D	1005
#define	VC1_BCK_LEFTPULTWALLTOP_D	1006
#define	VC1_FRM1_1_D	5001
#define	VC1_FRM1_2_D	5002
#define	VC1_FRM1_3_D	5003
#define	VC1_FRM2_1_D	5004
#define	VC1_FRM2_2_D	5005
#define	VC1_FRM2_3_D	5006
#define	VC1_FRM2_4_D	5007
#define	VC1_FRM3_1_D	5008
#define	VC1_FRM3_2_D	5009
#define	VC1_FRM3_3_D	5010
#define	VC1_FRM3_4_D	5011
#define	VC1_FRM4_1_D	5012
#define	VC1_FRM4_2_D	5013
#define	VC1_FRM4_3_D	5014
#define	VC1_FRM4_4_D	5015
#define	VC1_FRMBTM1_1_D	5016
#define	VC1_FRMBTM1_2_D	5017
#define	VC1_FRMBTM1_3_D	5018
#define	VC1_FRMBTM1_4_D	5019
#define	VC1_FRMBTM1_D	5020
#define	VC1_FRMBTM2_1_D	5021
#define	VC1_FRMBTM2_2_D	5022
#define	VC1_FRMBTM2_3_D	5023
#define	VC1_FRMBTM2_4_D	5024
#define	VC1_FRMBTM2_5_D	5025
#define	VC1_FRMBTM2_6_D	5026
#define	VC1_FRMBTM2_7_D	5027
#define	VC1_FRMBTM2_8_D	5028
#define	VC1_FRMBTM2_D	5029
#define	VC1_FRMBTM3_D	5030
#define	VC1_FRMFRT1_D	5031
#define	VC1_FRMFRT2_D	5032
#define	VC1_FRMFRT3_D	5033
#define	VC1_FRMFRT4_D	5034
#define	VC1_FRMFRT5_D	5035
#define	VC1_FRMFRT6_D	5036
#define	VC1_RP_BLT10_D	9001
#define	VC1_RP_BLT1_D	9002
#define	VC1_RP_BLT2_D	9003
#define	VC1_RP_BLT3_D	9004
#define	VC1_RP_BLT4_D	9005
#define	VC1_RP_BLT5_D	9006
#define	VC1_RP_BLT6_D	9007
#define	VC1_RP_BLT7_D	9008
#define	VC1_RP_BLT8_D	9009
#define	VC1_RP_BLT9_D	9010
#define	VC1_RP_BTM_D	9011
#define	VC1_RP_PNL1_D	9012
#define	VC1_RP_PNL2_D	9013
#define	VC1_RP_PNL3_D	9014
#define	VC1_RP_PNL4_D	9015
#define	VC1_RP_PNL5_D	9016
#define	VC1_RP_SHD1_D	9017
#define	VC1_RP_SHD2_D	9018
#define	VC1_RP_SHD3_D	9019
#define	VC1_RP_SHD4_D	9020
#define	VC1_RP_SHD5_D	9021
#define	VC1_RP_SHD6_D	9022
#define	VC1_RP_SHD7_D	9023
#define	VC1_RP_TOP_D	9024
#define	VC1_ROFA1_D	13001
#define	VC1_ROFA2_D	13002
#define	VC1_ROFA3_D	13003
#define	VC1_ROFA4_D	13004
#define	VC1_ROFA5_D	13005
#define	VC1_ROFA6_D	13006
#define	VC1_ROFA7_D	13007
#define	VC1_ROFB1_D	13008
#define	VC1_ROFB2_D	13009
#define	VC1_ROFB3_D	13010
#define	VC1_ROFB4_D	13011
#define	VC1_ROFB5_D	13012
#define	VC1_NPLDET1_D	17001
#define	VC1_NPLDET2_D	17002
#define	VC1_NPLDET3_D	17003
#define	VC1_NPLDET4_D	17004
#define	VC1_NPLDET5_D	17005
#define	VC1_NPL_D	17006
#define	VC1_MAT_D	21001
#define	VC1_FRTDET1_D	25001
#define	VC1_FRTDET2_D	25002
#define	VC1_FRTDET3_D	25003
#define	VC1_FRTDET4_D	25004
#define	VC1_FRTDET6_D	25005
#define	VC1_FRT_HNDL_D	25006

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
