/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "vc3_size_odX.h"

#define	VC3_BCK_CNCFRONT_D	1001
#define	VC3_BCK_CNCSIDE_D	1002
#define	VC3_BCK_CNCTOP_D	1003
#define	VC3_CNC01_D	5001
#define	VC3_CNC02INTL1_D	5002
#define	VC3_CNC02INTL2_D	5003
#define	VC3_CNC02_D	5004
#define	VC3_CNC03INTL_D	5005
#define	VC3_CNC03_D	5006
#define	VC3_CNC04INTL_D	5007
#define	VC3_CNC04_D	5008
#define	VC3_CNC05_D	5009
#define	VC3_CNC06_D	5010
#define	VC3_CNC07INTL1_D	5011
#define	VC3_CNC07INTL2_D	5012
#define	VC3_CNC07_D	5013
#define	VC3_CNC08INTL1_D	5014
#define	VC3_CNC08INTL2_D	5015
#define	VC3_CNC08_D	5016
#define	VC3_CNC09INTL_D	5017
#define	VC3_CNC09_D	5018
#define	VC3_CNC10INTL_D	5019
#define	VC3_CNC10_D	5020
#define	VC3_CNC11INTL_D	5021
#define	VC3_CNC11_D	5022
#define	VC3_CNC12INTL_D	5023
#define	VC3_CNC12_D	5024
#define	VC3_CNC13INTL_D	5025
#define	VC3_CNC13_D	5026
#define	VC3_CNC14INTL_D	5027
#define	VC3_CNC14_D	5028
#define	VC3_CNC15_D	5029
#define	VC3_CNC16_D	5030
#define	VC3_CNC17_D	5031
#define	VC3_CNC18INTL_D	5032
#define	VC3_CNC18_D	5033
#define	VC3_CNC19_D	5034
#define	VC3_CNC20INTL_D	5035
#define	VC3_CNC20_D	5036
#define	VC3_CNC21_D	5037
#define	VC3_CNC22INTL1_D	5038
#define	VC3_CNC22INTL2_D	5039
#define	VC3_CNC22INTL3_D	5040
#define	VC3_CNC22_D	5041
#define	VC3_CNC23_D	5042
#define	VC3_CNC24INTL_D	5043
#define	VC3_CNC24_D	5044
#define	VC3_CNC25INTL_D	5045
#define	VC3_CNC25_D	5046
#define	VC3_CNC26INTL_D	5047
#define	VC3_CNC26SIDE_D	5048
#define	VC3_CNC26_D	5049
#define	VC3_CNC27_D	5050
#define	VC3_CNC28INTL_D	5051
#define	VC3_CNC28_D	5052
#define	VC3_CNC29INTL_D	5053
#define	VC3_CNC29_D	5054
#define	VC3_CNC30_D	5055
#define	VC3_CNC31_D	5056
#define	VC3_CNC32INTL_D	5057
#define	VC3_CNC32_D	5058
#define	VC3_CNC33INTL_D	5059
#define	VC3_CNC33_D	5060
#define	VC3_CNC34_D	5061
#define	VC3_CNC35_D	5062
#define	VC3_CNC36_D	5063
#define	VC3_CNC37_D	5064
#define	VC3_CNC38_D	5065
#define	VC3_CNCRUD1_D	5066
#define	VC3_CNCRUD2_D	5067
#define	VC3_CNCRUD3_D	5068
#define	VC3_CNCRUDHNDLSIDE_D	5069
#define	VC3_CNCRUDLEVER_D	5070
#define	VC3_CNCRUDSTOPSIDE_D	5071
#define	VC3_CNCRUDSTOPTOP_D	5072
#define	VC3_CNCRUDSTOP_D	5073

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
