/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#define	VC8_WBC01_D_SX	346
#define	VC8_WBC01_D_SY	222
#define	VC8_WBC02_D_SX	440
#define	VC8_WBC02_D_SY	356
#define	VC8_WBC03_D_SX	314
#define	VC8_WBC03_D_SY	216
#define	VC8_WBC04_D_SX	353
#define	VC8_WBC04_D_SY	222
#define	VC8_WBC05_D_SX	449
#define	VC8_WBC05_D_SY	356
#define	VC8_WBC06_D_SX	325
#define	VC8_WBC06_D_SY	216
#define	VC8_WBC07_D_SX	510
#define	VC8_WBC07_D_SY	160
#define	VC8_WBC08_D_SX	514
#define	VC8_WBC08_D_SY	90
#define	VC8_WBC09_D_SX	220
#define	VC8_WBC09_D_SY	174
#define	VC8_WBC10_D_SX	64
#define	VC8_WBC10_D_SY	180
#define	VC8_WBC11_D_SX	180
#define	VC8_WBC11_D_SY	180
#define	VC8_WBC12_D_SX	180
#define	VC8_WBC12_D_SY	180
#define	VC8_WBC13INTL_D_SX	503
#define	VC8_WBC13INTL_D_SY	41
#define	VC8_WBC13_D_SX	503
#define	VC8_WBC13_D_SY	41
#define	VC8_WBC14_D_SX	503
#define	VC8_WBC14_D_SY	27
#define	VC8_WBC15_D_SX	503
#define	VC8_WBC15_D_SY	62
#define	VC8_WBC16INTL_D_SX	503
#define	VC8_WBC16INTL_D_SY	40
#define	VC8_WBC16_D_SX	503
#define	VC8_WBC16_D_SY	40
#define	VC8_WBC17INTL_D_SX	282
#define	VC8_WBC17INTL_D_SY	43
#define	VC8_WBC17_D_SX	282
#define	VC8_WBC17_D_SY	43
#define	VC8_WBC18_D_SX	503
#define	VC8_WBC18_D_SY	74
#define	VC8_WBC19INTL_D_SX	503
#define	VC8_WBC19INTL_D_SY	51
#define	VC8_WBC19_D_SX	503
#define	VC8_WBC19_D_SY	51
#define	VC8_WBC20INTL_D_SX	275
#define	VC8_WBC20INTL_D_SY	38
#define	VC8_WBC20_D_SX	275
#define	VC8_WBC20_D_SY	38
#define	VC8_WBC21_D_SX	503
#define	VC8_WBC21_D_SY	70
#define	VC8_WBC22INTL_D_SX	503
#define	VC8_WBC22INTL_D_SY	46
#define	VC8_WBC22_D_SX	503
#define	VC8_WBC22_D_SY	46
#define	VC8_WBC23_D_SX	503
#define	VC8_WBC23_D_SY	42
#define	VC8_WBC24_D_SX	503
#define	VC8_WBC24_D_SY	42
#define	VC8_WBC25INTL_D_SX	504
#define	VC8_WBC25INTL_D_SY	46
#define	VC8_WBC25_D_SX	504
#define	VC8_WBC25_D_SY	46
#define	VC8_WBC26_D_SX	504
#define	VC8_WBC26_D_SY	26
#define	VC8_WBC27_D_SX	504
#define	VC8_WBC27_D_SY	61
#define	VC8_WBC28INTL_D_SX	504
#define	VC8_WBC28INTL_D_SY	54
#define	VC8_WBC28_D_SX	504
#define	VC8_WBC28_D_SY	54
#define	VC8_WBC29_D_SX	504
#define	VC8_WBC29_D_SY	74
#define	VC8_WBC30INTL_D_SX	504
#define	VC8_WBC30INTL_D_SY	51
#define	VC8_WBC30_D_SX	504
#define	VC8_WBC30_D_SY	51
#define	VC8_WBC31_D_SX	504
#define	VC8_WBC31_D_SY	71
#define	VC8_WBC32INTL_D_SX	504
#define	VC8_WBC32INTL_D_SY	49
#define	VC8_WBC32_D_SX	504
#define	VC8_WBC32_D_SY	49
#define	VC8_WBC33_D_SX	504
#define	VC8_WBC33_D_SY	89
#define	VC8_WBC34_D_SX	243
#define	VC8_WBC34_D_SY	170
#define	VC8_WBC35_D_SX	161
#define	VC8_WBC35_D_SY	512
#define	VC8_WBC36_D_SX	102
#define	VC8_WBC36_D_SY	33
#define	VC8_WBC37_D_SX	122
#define	VC8_WBC37_D_SY	41
#define	VC8_WBC38_D_SX	93
#define	VC8_WBC38_D_SY	35
#define	VC8_WBC39_D_SX	88
#define	VC8_WBC39_D_SY	69
#define	VC8_WBC40_D_SX	35
#define	VC8_WBC40_D_SY	509
#define	VC8_WBC41_D_SX	35
#define	VC8_WBC41_D_SY	509
#define	VC8_WBC42_D_SX	35
#define	VC8_WBC42_D_SY	509
#define	VC8_WBC43_D_SX	35
#define	VC8_WBC43_D_SY	200
#define	VC8_WBC44_D_SX	33
#define	VC8_WBC44_D_SY	459
#define	VC8_WBC45_D_SX	175
#define	VC8_WBC45_D_SY	268
