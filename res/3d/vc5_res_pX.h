/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "vc5_size_pX.h"

#define	VC5_NDL_ADF1LIT_D	1001
#define	VC5_NDL_ADF1LIT_P	1002
#define	VC5_NDL_ADF1_D	1003
#define	VC5_NDL_ADF1_P	1004
#define	VC5_NDL_ADF2LIT_D	1005
#define	VC5_NDL_ADF2LIT_P	1006
#define	VC5_NDL_ADF2_D	1007
#define	VC5_NDL_ADF2_P	1008
#define	VC5_SCL_COM1ALIT_D_00	5001
#define	VC5_SCL_COM1ALIT_D_01	5002
#define	VC5_SCL_COM1ALIT_D_02	5003
#define	VC5_SCL_COM1ALIT_D_03	5004
#define	VC5_SCL_COM1ALIT_D_04	5005
#define	VC5_SCL_COM1ALIT_D_05	5006
#define	VC5_SCL_COM1ALIT_D_06	5007
#define	VC5_SCL_COM1ALIT_D_07	5008
#define	VC5_SCL_COM1ALIT_D_08	5009
#define	VC5_SCL_COM1ALIT_D_09	5010
#define	VC5_SCL_COM1ALIT_D_10	5011
#define	VC5_SCL_COM1ALIT_D_11	5012
#define	VC5_SCL_COM1ALIT_D_12	5013
#define	VC5_SCL_COM1ALIT_D_13	5014
#define	VC5_SCL_COM1ALIT_D_14	5015
#define	VC5_SCL_COM1ALIT_D_15	5016
#define	VC5_SCL_COM1ALIT_D_16	5017
#define	VC5_SCL_COM1ALIT_D_17	5018
#define	VC5_SCL_COM1ALIT_P_00	5019
#define	VC5_SCL_COM1ALIT_P_01	5020
#define	VC5_SCL_COM1ALIT_P_02	5021
#define	VC5_SCL_COM1ALIT_P_03	5022
#define	VC5_SCL_COM1ALIT_P_04	5023
#define	VC5_SCL_COM1ALIT_P_05	5024
#define	VC5_SCL_COM1ALIT_P_06	5025
#define	VC5_SCL_COM1ALIT_P_07	5026
#define	VC5_SCL_COM1ALIT_P_08	5027
#define	VC5_SCL_COM1ALIT_P_09	5028
#define	VC5_SCL_COM1ALIT_P_10	5029
#define	VC5_SCL_COM1ALIT_P_11	5030
#define	VC5_SCL_COM1ALIT_P_12	5031
#define	VC5_SCL_COM1ALIT_P_13	5032
#define	VC5_SCL_COM1ALIT_P_14	5033
#define	VC5_SCL_COM1ALIT_P_15	5034
#define	VC5_SCL_COM1ALIT_P_16	5035
#define	VC5_SCL_COM1ALIT_P_17	5036
#define	VC5_SCL_COM1A_D_00	5037
#define	VC5_SCL_COM1A_D_01	5038
#define	VC5_SCL_COM1A_D_02	5039
#define	VC5_SCL_COM1A_D_03	5040
#define	VC5_SCL_COM1A_D_04	5041
#define	VC5_SCL_COM1A_D_05	5042
#define	VC5_SCL_COM1A_D_06	5043
#define	VC5_SCL_COM1A_D_07	5044
#define	VC5_SCL_COM1A_D_08	5045
#define	VC5_SCL_COM1A_D_09	5046
#define	VC5_SCL_COM1A_D_10	5047
#define	VC5_SCL_COM1A_D_11	5048
#define	VC5_SCL_COM1A_D_12	5049
#define	VC5_SCL_COM1A_D_13	5050
#define	VC5_SCL_COM1A_D_14	5051
#define	VC5_SCL_COM1A_D_15	5052
#define	VC5_SCL_COM1A_D_16	5053
#define	VC5_SCL_COM1A_D_17	5054
#define	VC5_SCL_COM1A_P_00	5055
#define	VC5_SCL_COM1A_P_01	5056
#define	VC5_SCL_COM1A_P_02	5057
#define	VC5_SCL_COM1A_P_03	5058
#define	VC5_SCL_COM1A_P_04	5059
#define	VC5_SCL_COM1A_P_05	5060
#define	VC5_SCL_COM1A_P_06	5061
#define	VC5_SCL_COM1A_P_07	5062
#define	VC5_SCL_COM1A_P_08	5063
#define	VC5_SCL_COM1A_P_09	5064
#define	VC5_SCL_COM1A_P_10	5065
#define	VC5_SCL_COM1A_P_11	5066
#define	VC5_SCL_COM1A_P_12	5067
#define	VC5_SCL_COM1A_P_13	5068
#define	VC5_SCL_COM1A_P_14	5069
#define	VC5_SCL_COM1A_P_15	5070
#define	VC5_SCL_COM1A_P_16	5071
#define	VC5_SCL_COM1A_P_17	5072
#define	VC5_SCL_COM1BLIT_D_00	5073
#define	VC5_SCL_COM1BLIT_D_01	5074
#define	VC5_SCL_COM1BLIT_D_02	5075
#define	VC5_SCL_COM1BLIT_D_03	5076
#define	VC5_SCL_COM1BLIT_D_04	5077
#define	VC5_SCL_COM1BLIT_D_05	5078
#define	VC5_SCL_COM1BLIT_D_06	5079
#define	VC5_SCL_COM1BLIT_D_07	5080
#define	VC5_SCL_COM1BLIT_D_08	5081
#define	VC5_SCL_COM1BLIT_D_09	5082
#define	VC5_SCL_COM1BLIT_P_00	5083
#define	VC5_SCL_COM1BLIT_P_01	5084
#define	VC5_SCL_COM1BLIT_P_02	5085
#define	VC5_SCL_COM1BLIT_P_03	5086
#define	VC5_SCL_COM1BLIT_P_04	5087
#define	VC5_SCL_COM1BLIT_P_05	5088
#define	VC5_SCL_COM1BLIT_P_06	5089
#define	VC5_SCL_COM1BLIT_P_07	5090
#define	VC5_SCL_COM1BLIT_P_08	5091
#define	VC5_SCL_COM1BLIT_P_09	5092
#define	VC5_SCL_COM1B_D_00	5093
#define	VC5_SCL_COM1B_D_01	5094
#define	VC5_SCL_COM1B_D_02	5095
#define	VC5_SCL_COM1B_D_03	5096
#define	VC5_SCL_COM1B_D_04	5097
#define	VC5_SCL_COM1B_D_05	5098
#define	VC5_SCL_COM1B_D_06	5099
#define	VC5_SCL_COM1B_D_07	5100
#define	VC5_SCL_COM1B_D_08	5101
#define	VC5_SCL_COM1B_D_09	5102
#define	VC5_SCL_COM1B_P_00	5103
#define	VC5_SCL_COM1B_P_01	5104
#define	VC5_SCL_COM1B_P_02	5105
#define	VC5_SCL_COM1B_P_03	5106
#define	VC5_SCL_COM1B_P_04	5107
#define	VC5_SCL_COM1B_P_05	5108
#define	VC5_SCL_COM1B_P_06	5109
#define	VC5_SCL_COM1B_P_07	5110
#define	VC5_SCL_COM1B_P_08	5111
#define	VC5_SCL_COM1B_P_09	5112
#define	VC5_SCL_COM1CLIT_D_00	5113
#define	VC5_SCL_COM1CLIT_D_01	5114
#define	VC5_SCL_COM1CLIT_D_02	5115
#define	VC5_SCL_COM1CLIT_D_03	5116
#define	VC5_SCL_COM1CLIT_P_00	5117
#define	VC5_SCL_COM1CLIT_P_01	5118
#define	VC5_SCL_COM1CLIT_P_02	5119
#define	VC5_SCL_COM1CLIT_P_03	5120
#define	VC5_SCL_COM1C_D_00	5121
#define	VC5_SCL_COM1C_D_01	5122
#define	VC5_SCL_COM1C_D_02	5123
#define	VC5_SCL_COM1C_D_03	5124
#define	VC5_SCL_COM1C_P_00	5125
#define	VC5_SCL_COM1C_P_01	5126
#define	VC5_SCL_COM1C_P_02	5127
#define	VC5_SCL_COM1C_P_03	5128
#define	VC5_SCL_COM2ALIT_D_00	9001
#define	VC5_SCL_COM2ALIT_D_01	9002
#define	VC5_SCL_COM2ALIT_D_02	9003
#define	VC5_SCL_COM2ALIT_D_03	9004
#define	VC5_SCL_COM2ALIT_D_04	9005
#define	VC5_SCL_COM2ALIT_D_05	9006
#define	VC5_SCL_COM2ALIT_D_06	9007
#define	VC5_SCL_COM2ALIT_D_07	9008
#define	VC5_SCL_COM2ALIT_D_08	9009
#define	VC5_SCL_COM2ALIT_D_09	9010
#define	VC5_SCL_COM2ALIT_D_10	9011
#define	VC5_SCL_COM2ALIT_D_11	9012
#define	VC5_SCL_COM2ALIT_D_12	9013
#define	VC5_SCL_COM2ALIT_D_13	9014
#define	VC5_SCL_COM2ALIT_D_14	9015
#define	VC5_SCL_COM2ALIT_D_15	9016
#define	VC5_SCL_COM2ALIT_D_16	9017
#define	VC5_SCL_COM2ALIT_D_17	9018
#define	VC5_SCL_COM2ALIT_P_00	9019
#define	VC5_SCL_COM2ALIT_P_01	9020
#define	VC5_SCL_COM2ALIT_P_02	9021
#define	VC5_SCL_COM2ALIT_P_03	9022
#define	VC5_SCL_COM2ALIT_P_04	9023
#define	VC5_SCL_COM2ALIT_P_05	9024
#define	VC5_SCL_COM2ALIT_P_06	9025
#define	VC5_SCL_COM2ALIT_P_07	9026
#define	VC5_SCL_COM2ALIT_P_08	9027
#define	VC5_SCL_COM2ALIT_P_09	9028
#define	VC5_SCL_COM2ALIT_P_10	9029
#define	VC5_SCL_COM2ALIT_P_11	9030
#define	VC5_SCL_COM2ALIT_P_12	9031
#define	VC5_SCL_COM2ALIT_P_13	9032
#define	VC5_SCL_COM2ALIT_P_14	9033
#define	VC5_SCL_COM2ALIT_P_15	9034
#define	VC5_SCL_COM2ALIT_P_16	9035
#define	VC5_SCL_COM2ALIT_P_17	9036
#define	VC5_SCL_COM2A_D_00	9037
#define	VC5_SCL_COM2A_D_01	9038
#define	VC5_SCL_COM2A_D_02	9039
#define	VC5_SCL_COM2A_D_03	9040
#define	VC5_SCL_COM2A_D_04	9041
#define	VC5_SCL_COM2A_D_05	9042
#define	VC5_SCL_COM2A_D_06	9043
#define	VC5_SCL_COM2A_D_07	9044
#define	VC5_SCL_COM2A_D_08	9045
#define	VC5_SCL_COM2A_D_09	9046
#define	VC5_SCL_COM2A_D_10	9047
#define	VC5_SCL_COM2A_D_11	9048
#define	VC5_SCL_COM2A_D_12	9049
#define	VC5_SCL_COM2A_D_13	9050
#define	VC5_SCL_COM2A_D_14	9051
#define	VC5_SCL_COM2A_D_15	9052
#define	VC5_SCL_COM2A_D_16	9053
#define	VC5_SCL_COM2A_D_17	9054
#define	VC5_SCL_COM2A_P_00	9055
#define	VC5_SCL_COM2A_P_01	9056
#define	VC5_SCL_COM2A_P_02	9057
#define	VC5_SCL_COM2A_P_03	9058
#define	VC5_SCL_COM2A_P_04	9059
#define	VC5_SCL_COM2A_P_05	9060
#define	VC5_SCL_COM2A_P_06	9061
#define	VC5_SCL_COM2A_P_07	9062
#define	VC5_SCL_COM2A_P_08	9063
#define	VC5_SCL_COM2A_P_09	9064
#define	VC5_SCL_COM2A_P_10	9065
#define	VC5_SCL_COM2A_P_11	9066
#define	VC5_SCL_COM2A_P_12	9067
#define	VC5_SCL_COM2A_P_13	9068
#define	VC5_SCL_COM2A_P_14	9069
#define	VC5_SCL_COM2A_P_15	9070
#define	VC5_SCL_COM2A_P_16	9071
#define	VC5_SCL_COM2A_P_17	9072
#define	VC5_SCL_COM2BLIT_D_00	9073
#define	VC5_SCL_COM2BLIT_D_01	9074
#define	VC5_SCL_COM2BLIT_D_02	9075
#define	VC5_SCL_COM2BLIT_D_03	9076
#define	VC5_SCL_COM2BLIT_D_04	9077
#define	VC5_SCL_COM2BLIT_D_05	9078
#define	VC5_SCL_COM2BLIT_D_06	9079
#define	VC5_SCL_COM2BLIT_D_07	9080
#define	VC5_SCL_COM2BLIT_D_08	9081
#define	VC5_SCL_COM2BLIT_D_09	9082
#define	VC5_SCL_COM2BLIT_P_00	9083
#define	VC5_SCL_COM2BLIT_P_01	9084
#define	VC5_SCL_COM2BLIT_P_02	9085
#define	VC5_SCL_COM2BLIT_P_03	9086
#define	VC5_SCL_COM2BLIT_P_04	9087
#define	VC5_SCL_COM2BLIT_P_05	9088
#define	VC5_SCL_COM2BLIT_P_06	9089
#define	VC5_SCL_COM2BLIT_P_07	9090
#define	VC5_SCL_COM2BLIT_P_08	9091
#define	VC5_SCL_COM2BLIT_P_09	9092
#define	VC5_SCL_COM2B_D_00	9093
#define	VC5_SCL_COM2B_D_01	9094
#define	VC5_SCL_COM2B_D_02	9095
#define	VC5_SCL_COM2B_D_03	9096
#define	VC5_SCL_COM2B_D_04	9097
#define	VC5_SCL_COM2B_D_05	9098
#define	VC5_SCL_COM2B_D_06	9099
#define	VC5_SCL_COM2B_D_07	9100
#define	VC5_SCL_COM2B_D_08	9101
#define	VC5_SCL_COM2B_D_09	9102
#define	VC5_SCL_COM2B_P_00	9103
#define	VC5_SCL_COM2B_P_01	9104
#define	VC5_SCL_COM2B_P_02	9105
#define	VC5_SCL_COM2B_P_03	9106
#define	VC5_SCL_COM2B_P_04	9107
#define	VC5_SCL_COM2B_P_05	9108
#define	VC5_SCL_COM2B_P_06	9109
#define	VC5_SCL_COM2B_P_07	9110
#define	VC5_SCL_COM2B_P_08	9111
#define	VC5_SCL_COM2B_P_09	9112
#define	VC5_SCL_COM2CLIT_D_00	9113
#define	VC5_SCL_COM2CLIT_D_01	9114
#define	VC5_SCL_COM2CLIT_D_02	9115
#define	VC5_SCL_COM2CLIT_D_03	9116
#define	VC5_SCL_COM2CLIT_P_00	9117
#define	VC5_SCL_COM2CLIT_P_01	9118
#define	VC5_SCL_COM2CLIT_P_02	9119
#define	VC5_SCL_COM2CLIT_P_03	9120
#define	VC5_SCL_COM2C_D_00	9121
#define	VC5_SCL_COM2C_D_01	9122
#define	VC5_SCL_COM2C_D_02	9123
#define	VC5_SCL_COM2C_D_03	9124
#define	VC5_SCL_COM2C_P_00	9125
#define	VC5_SCL_COM2C_P_01	9126
#define	VC5_SCL_COM2C_P_02	9127
#define	VC5_SCL_COM2C_P_03	9128
#define	VC5_SCL_ADF1PRI1_D_00	13001
#define	VC5_SCL_ADF1PRI1_D_01	13002
#define	VC5_SCL_ADF1PRI1_D_02	13003
#define	VC5_SCL_ADF1PRI1_D_03	13004
#define	VC5_SCL_ADF1PRI1_D_04	13005
#define	VC5_SCL_ADF1PRI1_D_05	13006
#define	VC5_SCL_ADF1PRI1_D_06	13007
#define	VC5_SCL_ADF1PRI1_D_07	13008
#define	VC5_SCL_ADF1PRI1_D_08	13009
#define	VC5_SCL_ADF1PRI1_D_09	13010
#define	VC5_SCL_ADF1PRI1_D_10	13011
#define	VC5_SCL_ADF1PRI1_D_11	13012
#define	VC5_SCL_ADF1PRI1_P_00	13013
#define	VC5_SCL_ADF1PRI1_P_01	13014
#define	VC5_SCL_ADF1PRI1_P_02	13015
#define	VC5_SCL_ADF1PRI1_P_03	13016
#define	VC5_SCL_ADF1PRI1_P_04	13017
#define	VC5_SCL_ADF1PRI1_P_05	13018
#define	VC5_SCL_ADF1PRI1_P_06	13019
#define	VC5_SCL_ADF1PRI1_P_07	13020
#define	VC5_SCL_ADF1PRI1_P_08	13021
#define	VC5_SCL_ADF1PRI1_P_09	13022
#define	VC5_SCL_ADF1PRI1_P_10	13023
#define	VC5_SCL_ADF1PRI1_P_11	13024
#define	VC5_SCL_ADF1PRI2_D_00	13025
#define	VC5_SCL_ADF1PRI2_D_01	13026
#define	VC5_SCL_ADF1PRI2_D_02	13027
#define	VC5_SCL_ADF1PRI2_D_03	13028
#define	VC5_SCL_ADF1PRI2_D_04	13029
#define	VC5_SCL_ADF1PRI2_D_05	13030
#define	VC5_SCL_ADF1PRI2_D_06	13031
#define	VC5_SCL_ADF1PRI2_D_07	13032
#define	VC5_SCL_ADF1PRI2_D_08	13033
#define	VC5_SCL_ADF1PRI2_D_09	13034
#define	VC5_SCL_ADF1PRI2_P_00	13035
#define	VC5_SCL_ADF1PRI2_P_01	13036
#define	VC5_SCL_ADF1PRI2_P_02	13037
#define	VC5_SCL_ADF1PRI2_P_03	13038
#define	VC5_SCL_ADF1PRI2_P_04	13039
#define	VC5_SCL_ADF1PRI2_P_05	13040
#define	VC5_SCL_ADF1PRI2_P_06	13041
#define	VC5_SCL_ADF1PRI2_P_07	13042
#define	VC5_SCL_ADF1PRI2_P_08	13043
#define	VC5_SCL_ADF1PRI2_P_09	13044
#define	VC5_SCL_ADF1PRI3_D_00	13045
#define	VC5_SCL_ADF1PRI3_D_01	13046
#define	VC5_SCL_ADF1PRI3_D_02	13047
#define	VC5_SCL_ADF1PRI3_D_03	13048
#define	VC5_SCL_ADF1PRI3_D_04	13049
#define	VC5_SCL_ADF1PRI3_D_05	13050
#define	VC5_SCL_ADF1PRI3_D_06	13051
#define	VC5_SCL_ADF1PRI3_D_07	13052
#define	VC5_SCL_ADF1PRI3_D_08	13053
#define	VC5_SCL_ADF1PRI3_D_09	13054
#define	VC5_SCL_ADF1PRI3_P_00	13055
#define	VC5_SCL_ADF1PRI3_P_01	13056
#define	VC5_SCL_ADF1PRI3_P_02	13057
#define	VC5_SCL_ADF1PRI3_P_03	13058
#define	VC5_SCL_ADF1PRI3_P_04	13059
#define	VC5_SCL_ADF1PRI3_P_05	13060
#define	VC5_SCL_ADF1PRI3_P_06	13061
#define	VC5_SCL_ADF1PRI3_P_07	13062
#define	VC5_SCL_ADF1PRI3_P_08	13063
#define	VC5_SCL_ADF1PRI3_P_09	13064
#define	VC5_SCL_ADF1SEC1_D_00	13065
#define	VC5_SCL_ADF1SEC1_D_01	13066
#define	VC5_SCL_ADF1SEC1_D_02	13067
#define	VC5_SCL_ADF1SEC1_D_03	13068
#define	VC5_SCL_ADF1SEC1_D_04	13069
#define	VC5_SCL_ADF1SEC1_D_05	13070
#define	VC5_SCL_ADF1SEC1_D_06	13071
#define	VC5_SCL_ADF1SEC1_D_07	13072
#define	VC5_SCL_ADF1SEC1_D_08	13073
#define	VC5_SCL_ADF1SEC1_D_09	13074
#define	VC5_SCL_ADF1SEC1_D_10	13075
#define	VC5_SCL_ADF1SEC1_D_11	13076
#define	VC5_SCL_ADF1SEC1_P_00	13077
#define	VC5_SCL_ADF1SEC1_P_01	13078
#define	VC5_SCL_ADF1SEC1_P_02	13079
#define	VC5_SCL_ADF1SEC1_P_03	13080
#define	VC5_SCL_ADF1SEC1_P_04	13081
#define	VC5_SCL_ADF1SEC1_P_05	13082
#define	VC5_SCL_ADF1SEC1_P_06	13083
#define	VC5_SCL_ADF1SEC1_P_07	13084
#define	VC5_SCL_ADF1SEC1_P_08	13085
#define	VC5_SCL_ADF1SEC1_P_09	13086
#define	VC5_SCL_ADF1SEC1_P_10	13087
#define	VC5_SCL_ADF1SEC1_P_11	13088
#define	VC5_SCL_ADF1SEC2_D_00	13089
#define	VC5_SCL_ADF1SEC2_D_01	13090
#define	VC5_SCL_ADF1SEC2_D_02	13091
#define	VC5_SCL_ADF1SEC2_D_03	13092
#define	VC5_SCL_ADF1SEC2_D_04	13093
#define	VC5_SCL_ADF1SEC2_D_05	13094
#define	VC5_SCL_ADF1SEC2_D_06	13095
#define	VC5_SCL_ADF1SEC2_D_07	13096
#define	VC5_SCL_ADF1SEC2_D_08	13097
#define	VC5_SCL_ADF1SEC2_D_09	13098
#define	VC5_SCL_ADF1SEC2_P_00	13099
#define	VC5_SCL_ADF1SEC2_P_01	13100
#define	VC5_SCL_ADF1SEC2_P_02	13101
#define	VC5_SCL_ADF1SEC2_P_03	13102
#define	VC5_SCL_ADF1SEC2_P_04	13103
#define	VC5_SCL_ADF1SEC2_P_05	13104
#define	VC5_SCL_ADF1SEC2_P_06	13105
#define	VC5_SCL_ADF1SEC2_P_07	13106
#define	VC5_SCL_ADF1SEC2_P_08	13107
#define	VC5_SCL_ADF1SEC2_P_09	13108
#define	VC5_SCL_ADF1SEC3_D_00	13109
#define	VC5_SCL_ADF1SEC3_D_01	13110
#define	VC5_SCL_ADF1SEC3_D_02	13111
#define	VC5_SCL_ADF1SEC3_D_03	13112
#define	VC5_SCL_ADF1SEC3_D_04	13113
#define	VC5_SCL_ADF1SEC3_D_05	13114
#define	VC5_SCL_ADF1SEC3_D_06	13115
#define	VC5_SCL_ADF1SEC3_D_07	13116
#define	VC5_SCL_ADF1SEC3_D_08	13117
#define	VC5_SCL_ADF1SEC3_D_09	13118
#define	VC5_SCL_ADF1SEC3_P_00	13119
#define	VC5_SCL_ADF1SEC3_P_01	13120
#define	VC5_SCL_ADF1SEC3_P_02	13121
#define	VC5_SCL_ADF1SEC3_P_03	13122
#define	VC5_SCL_ADF1SEC3_P_04	13123
#define	VC5_SCL_ADF1SEC3_P_05	13124
#define	VC5_SCL_ADF1SEC3_P_06	13125
#define	VC5_SCL_ADF1SEC3_P_07	13126
#define	VC5_SCL_ADF1SEC3_P_08	13127
#define	VC5_SCL_ADF1SEC3_P_09	13128
#define	VC5_SCL_ADF2PRI1_D_00	17001
#define	VC5_SCL_ADF2PRI1_D_01	17002
#define	VC5_SCL_ADF2PRI1_D_02	17003
#define	VC5_SCL_ADF2PRI1_D_03	17004
#define	VC5_SCL_ADF2PRI1_D_04	17005
#define	VC5_SCL_ADF2PRI1_D_05	17006
#define	VC5_SCL_ADF2PRI1_D_06	17007
#define	VC5_SCL_ADF2PRI1_D_07	17008
#define	VC5_SCL_ADF2PRI1_D_08	17009
#define	VC5_SCL_ADF2PRI1_D_09	17010
#define	VC5_SCL_ADF2PRI1_D_10	17011
#define	VC5_SCL_ADF2PRI1_D_11	17012
#define	VC5_SCL_ADF2PRI1_P_00	17013
#define	VC5_SCL_ADF2PRI1_P_01	17014
#define	VC5_SCL_ADF2PRI1_P_02	17015
#define	VC5_SCL_ADF2PRI1_P_03	17016
#define	VC5_SCL_ADF2PRI1_P_04	17017
#define	VC5_SCL_ADF2PRI1_P_05	17018
#define	VC5_SCL_ADF2PRI1_P_06	17019
#define	VC5_SCL_ADF2PRI1_P_07	17020
#define	VC5_SCL_ADF2PRI1_P_08	17021
#define	VC5_SCL_ADF2PRI1_P_09	17022
#define	VC5_SCL_ADF2PRI1_P_10	17023
#define	VC5_SCL_ADF2PRI1_P_11	17024
#define	VC5_SCL_ADF2PRI2_D_00	17025
#define	VC5_SCL_ADF2PRI2_D_01	17026
#define	VC5_SCL_ADF2PRI2_D_02	17027
#define	VC5_SCL_ADF2PRI2_D_03	17028
#define	VC5_SCL_ADF2PRI2_D_04	17029
#define	VC5_SCL_ADF2PRI2_D_05	17030
#define	VC5_SCL_ADF2PRI2_D_06	17031
#define	VC5_SCL_ADF2PRI2_D_07	17032
#define	VC5_SCL_ADF2PRI2_D_08	17033
#define	VC5_SCL_ADF2PRI2_D_09	17034
#define	VC5_SCL_ADF2PRI2_P_00	17035
#define	VC5_SCL_ADF2PRI2_P_01	17036
#define	VC5_SCL_ADF2PRI2_P_02	17037
#define	VC5_SCL_ADF2PRI2_P_03	17038
#define	VC5_SCL_ADF2PRI2_P_04	17039
#define	VC5_SCL_ADF2PRI2_P_05	17040
#define	VC5_SCL_ADF2PRI2_P_06	17041
#define	VC5_SCL_ADF2PRI2_P_07	17042
#define	VC5_SCL_ADF2PRI2_P_08	17043
#define	VC5_SCL_ADF2PRI2_P_09	17044
#define	VC5_SCL_ADF2PRI3_D_00	17045
#define	VC5_SCL_ADF2PRI3_D_01	17046
#define	VC5_SCL_ADF2PRI3_D_02	17047
#define	VC5_SCL_ADF2PRI3_D_03	17048
#define	VC5_SCL_ADF2PRI3_D_04	17049
#define	VC5_SCL_ADF2PRI3_D_05	17050
#define	VC5_SCL_ADF2PRI3_D_06	17051
#define	VC5_SCL_ADF2PRI3_D_07	17052
#define	VC5_SCL_ADF2PRI3_D_08	17053
#define	VC5_SCL_ADF2PRI3_D_09	17054
#define	VC5_SCL_ADF2PRI3_P_00	17055
#define	VC5_SCL_ADF2PRI3_P_01	17056
#define	VC5_SCL_ADF2PRI3_P_02	17057
#define	VC5_SCL_ADF2PRI3_P_03	17058
#define	VC5_SCL_ADF2PRI3_P_04	17059
#define	VC5_SCL_ADF2PRI3_P_05	17060
#define	VC5_SCL_ADF2PRI3_P_06	17061
#define	VC5_SCL_ADF2PRI3_P_07	17062
#define	VC5_SCL_ADF2PRI3_P_08	17063
#define	VC5_SCL_ADF2PRI3_P_09	17064
#define	VC5_SCL_ADF2SEC1_D_00	17065
#define	VC5_SCL_ADF2SEC1_D_01	17066
#define	VC5_SCL_ADF2SEC1_D_02	17067
#define	VC5_SCL_ADF2SEC1_D_03	17068
#define	VC5_SCL_ADF2SEC1_D_04	17069
#define	VC5_SCL_ADF2SEC1_D_05	17070
#define	VC5_SCL_ADF2SEC1_D_06	17071
#define	VC5_SCL_ADF2SEC1_D_07	17072
#define	VC5_SCL_ADF2SEC1_D_08	17073
#define	VC5_SCL_ADF2SEC1_D_09	17074
#define	VC5_SCL_ADF2SEC1_D_10	17075
#define	VC5_SCL_ADF2SEC1_D_11	17076
#define	VC5_SCL_ADF2SEC1_P_00	17077
#define	VC5_SCL_ADF2SEC1_P_01	17078
#define	VC5_SCL_ADF2SEC1_P_02	17079
#define	VC5_SCL_ADF2SEC1_P_03	17080
#define	VC5_SCL_ADF2SEC1_P_04	17081
#define	VC5_SCL_ADF2SEC1_P_05	17082
#define	VC5_SCL_ADF2SEC1_P_06	17083
#define	VC5_SCL_ADF2SEC1_P_07	17084
#define	VC5_SCL_ADF2SEC1_P_08	17085
#define	VC5_SCL_ADF2SEC1_P_09	17086
#define	VC5_SCL_ADF2SEC1_P_10	17087
#define	VC5_SCL_ADF2SEC1_P_11	17088
#define	VC5_SCL_ADF2SEC2_D_00	17089
#define	VC5_SCL_ADF2SEC2_D_01	17090
#define	VC5_SCL_ADF2SEC2_D_02	17091
#define	VC5_SCL_ADF2SEC2_D_03	17092
#define	VC5_SCL_ADF2SEC2_D_04	17093
#define	VC5_SCL_ADF2SEC2_D_05	17094
#define	VC5_SCL_ADF2SEC2_D_06	17095
#define	VC5_SCL_ADF2SEC2_D_07	17096
#define	VC5_SCL_ADF2SEC2_D_08	17097
#define	VC5_SCL_ADF2SEC2_D_09	17098
#define	VC5_SCL_ADF2SEC2_P_00	17099
#define	VC5_SCL_ADF2SEC2_P_01	17100
#define	VC5_SCL_ADF2SEC2_P_02	17101
#define	VC5_SCL_ADF2SEC2_P_03	17102
#define	VC5_SCL_ADF2SEC2_P_04	17103
#define	VC5_SCL_ADF2SEC2_P_05	17104
#define	VC5_SCL_ADF2SEC2_P_06	17105
#define	VC5_SCL_ADF2SEC2_P_07	17106
#define	VC5_SCL_ADF2SEC2_P_08	17107
#define	VC5_SCL_ADF2SEC2_P_09	17108
#define	VC5_SCL_ADF2SEC3_D_00	17109
#define	VC5_SCL_ADF2SEC3_D_01	17110
#define	VC5_SCL_ADF2SEC3_D_02	17111
#define	VC5_SCL_ADF2SEC3_D_03	17112
#define	VC5_SCL_ADF2SEC3_D_04	17113
#define	VC5_SCL_ADF2SEC3_D_05	17114
#define	VC5_SCL_ADF2SEC3_D_06	17115
#define	VC5_SCL_ADF2SEC3_D_07	17116
#define	VC5_SCL_ADF2SEC3_D_08	17117
#define	VC5_SCL_ADF2SEC3_D_09	17118
#define	VC5_SCL_ADF2SEC3_P_00	17119
#define	VC5_SCL_ADF2SEC3_P_01	17120
#define	VC5_SCL_ADF2SEC3_P_02	17121
#define	VC5_SCL_ADF2SEC3_P_03	17122
#define	VC5_SCL_ADF2SEC3_P_04	17123
#define	VC5_SCL_ADF2SEC3_P_05	17124
#define	VC5_SCL_ADF2SEC3_P_06	17125
#define	VC5_SCL_ADF2SEC3_P_07	17126
#define	VC5_SCL_ADF2SEC3_P_08	17127
#define	VC5_SCL_ADF2SEC3_P_09	17128
#define	VC5_OVC01INTL1_D	21001
#define	VC5_OVC01INTL1_P	21002
#define	VC5_OVC01INTL2_D	21003
#define	VC5_OVC01INTL2_P	21004
#define	VC5_OVC01INTL3_D	21005
#define	VC5_OVC01INTL3_P	21006
#define	VC5_OVC01INTL4_D	21007
#define	VC5_OVC01INTL4_P	21008
#define	VC5_OVC01_D	21009
#define	VC5_OVC01_P	21010
#define	VC5_OVC02INTL_D	21011
#define	VC5_OVC02INTL_P	21012
#define	VC5_OVC02_D	21013
#define	VC5_OVC02_P	21014
#define	VC5_OVC03INTL1_D	21015
#define	VC5_OVC03INTL1_P	21016
#define	VC5_OVC03INTL2_D	21017
#define	VC5_OVC03INTL2_P	21018
#define	VC5_OVC03_D	21019
#define	VC5_OVC03_P	21020
#define	VC5_OVC04INTL1_D	21021
#define	VC5_OVC04INTL1_P	21022
#define	VC5_OVC04INTL2_D	21023
#define	VC5_OVC04INTL2_P	21024
#define	VC5_OVC04INTL3_D	21025
#define	VC5_OVC04INTL3_P	21026
#define	VC5_OVC04LITINTL1_D	21027
#define	VC5_OVC04LITINTL1_P	21028
#define	VC5_OVC04LITINTL2_D	21029
#define	VC5_OVC04LITINTL2_P	21030
#define	VC5_OVC04LITINTL3_D	21031
#define	VC5_OVC04LITINTL3_P	21032
#define	VC5_OVC04LIT_D	21033
#define	VC5_OVC04LIT_P	21034
#define	VC5_OVC04_D	21035
#define	VC5_OVC04_P	21036
#define	VC5_OVC05INTL1_D	21037
#define	VC5_OVC05INTL1_P	21038
#define	VC5_OVC05INTL2_D	21039
#define	VC5_OVC05INTL2_P	21040
#define	VC5_OVC05INTL3_D	21041
#define	VC5_OVC05INTL3_P	21042
#define	VC5_OVC05LITINTL1_D	21043
#define	VC5_OVC05LITINTL1_P	21044
#define	VC5_OVC05LITINTL2_D	21045
#define	VC5_OVC05LITINTL2_P	21046
#define	VC5_OVC05LITINTL3_D	21047
#define	VC5_OVC05LITINTL3_P	21048
#define	VC5_OVC05LIT_D	21049
#define	VC5_OVC05LIT_P	21050
#define	VC5_OVC05_D	21051
#define	VC5_OVC05_P	21052
#define	VC5_OVC06INTL_D	21053
#define	VC5_OVC06INTL_P	21054
#define	VC5_OVC06_D	21055
#define	VC5_OVC06_P	21056
#define	VC5_OVC07INTL_D	21057
#define	VC5_OVC07INTL_P	21058
#define	VC5_OVC07_D	21059
#define	VC5_OVC07_P	21060
#define	VC5_OVC08INTL1_D	21061
#define	VC5_OVC08INTL1_P	21062
#define	VC5_OVC08INTL2_D	21063
#define	VC5_OVC08INTL2_P	21064
#define	VC5_OVC08INTL3_D	21065
#define	VC5_OVC08INTL3_P	21066
#define	VC5_OVC08LIT1INTL1_D	21067
#define	VC5_OVC08LIT1INTL1_P	21068
#define	VC5_OVC08LIT1_D	21069
#define	VC5_OVC08LIT1_P	21070
#define	VC5_OVC08LIT2INTL2_D	21071
#define	VC5_OVC08LIT2INTL2_P	21072
#define	VC5_OVC08LIT2_D	21073
#define	VC5_OVC08LIT2_P	21074
#define	VC5_OVC08LIT3INTL3_D	21075
#define	VC5_OVC08LIT3INTL3_P	21076
#define	VC5_OVC08LIT3_D	21077
#define	VC5_OVC08LIT3_P	21078
#define	VC5_OVC08LITSCL_D	21079
#define	VC5_OVC08LITSCL_P	21080
#define	VC5_OVC08_D	21081
#define	VC5_OVC08_P	21082
#define	VC5_OVC09INTL1_D	21083
#define	VC5_OVC09INTL1_P	21084
#define	VC5_OVC09INTL2_D	21085
#define	VC5_OVC09INTL2_P	21086
#define	VC5_OVC09INTL3_D	21087
#define	VC5_OVC09INTL3_P	21088
#define	VC5_OVC09LIT1INTL1_D	21089
#define	VC5_OVC09LIT1INTL1_P	21090
#define	VC5_OVC09LIT1_D	21091
#define	VC5_OVC09LIT1_P	21092
#define	VC5_OVC09LIT2INTL2_D	21093
#define	VC5_OVC09LIT2INTL2_P	21094
#define	VC5_OVC09LIT2_D	21095
#define	VC5_OVC09LIT2_P	21096
#define	VC5_OVC09LIT3INTL3_D	21097
#define	VC5_OVC09LIT3INTL3_P	21098
#define	VC5_OVC09LIT3_D	21099
#define	VC5_OVC09LIT3_P	21100
#define	VC5_OVC09LITSCL_D	21101
#define	VC5_OVC09LITSCL_P	21102
#define	VC5_OVC09_D	21103
#define	VC5_OVC09_P	21104
#define	VC5_OVC10A_D	21105
#define	VC5_OVC10A_P	21106
#define	VC5_OVC10BINTL_D	21107
#define	VC5_OVC10BINTL_P	21108
#define	VC5_OVC10B_D	21109
#define	VC5_OVC10B_P	21110
#define	VC5_OVC11A_D	21111
#define	VC5_OVC11A_P	21112
#define	VC5_OVC11BINTL_D	21113
#define	VC5_OVC11BINTL_P	21114
#define	VC5_OVC11B_D	21115
#define	VC5_OVC11B_P	21116
#define	VC5_OVC12A_D	21117
#define	VC5_OVC12A_P	21118
#define	VC5_OVC12BINTL_D	21119
#define	VC5_OVC12BINTL_P	21120
#define	VC5_OVC12B_D	21121
#define	VC5_OVC12B_P	21122
#define	VC5_OVC13A_D	21123
#define	VC5_OVC13A_P	21124
#define	VC5_OVC13BINTL_D	21125
#define	VC5_OVC13BINTL_P	21126
#define	VC5_OVC13B_D	21127
#define	VC5_OVC13B_P	21128
#define	VC5_OVC14INTL1_D	21129
#define	VC5_OVC14INTL1_P	21130
#define	VC5_OVC14INTL2_D	21131
#define	VC5_OVC14INTL2_P	21132
#define	VC5_OVC14INTL3_D	21133
#define	VC5_OVC14INTL3_P	21134
#define	VC5_OVC14INTL4_D	21135
#define	VC5_OVC14INTL4_P	21136
#define	VC5_OVC14_D	21137
#define	VC5_OVC14_P	21138
#define	VC5_OVC15INTL1_D	21139
#define	VC5_OVC15INTL1_P	21140
#define	VC5_OVC15INTL2_D	21141
#define	VC5_OVC15INTL2_P	21142
#define	VC5_OVC15INTL3_D	21143
#define	VC5_OVC15INTL3_P	21144
#define	VC5_OVC15INTL4_D	21145
#define	VC5_OVC15INTL4_P	21146
#define	VC5_OVC15_D	21147
#define	VC5_OVC15_P	21148
#define	VC5_OVC16INTL1_D	21149
#define	VC5_OVC16INTL1_P	21150
#define	VC5_OVC16INTL2_D	21151
#define	VC5_OVC16INTL2_P	21152
#define	VC5_OVC16_D	21153
#define	VC5_OVC16_P	21154
#define	VC5_OVC17_D	21155
#define	VC5_OVC17_P	21156
#define	VC5_OVC18_D	21157
#define	VC5_OVC18_P	21158
#define	VC5_OVC19_D	21159
#define	VC5_OVC19_P	21160
#define	VC5_OVC20_D	21161
#define	VC5_OVC20_P	21162
#define	VC5_OVCBTM_D	21163
#define	VC5_OVCBTM_P	21164
#define	VC5_OVCFRONT_D	21165
#define	VC5_OVCFRONT_P	21166
#define	VC5_OVCSIDE_D	21167
#define	VC5_OVCSIDE_P	21168

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
