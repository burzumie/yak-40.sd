/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../Version.h"

#include "p3_size3d.h"

#define	MISC_CRS_P0_01	1001
#define	MISC_CRS_P0_02	1002
#define	MISC_CRS_P0_03	1003
#define	MISC_CRS_P0_04	1004
#define	MISC_CRS_P1_01	1005
#define	MISC_CRS_P1_02	1006
#define	MISC_CRS_P1_03	1007
#define	MISC_CRS_P1_04	1008
#define	MISC_CRS_P4_01	1009
#define	MISC_CRS_P4_02	1010
#define	MISC_CRS_P4_03	1011
#define	MISC_CRS_P5_01	1012
#define	MISC_CRS_P5_02	1013
#define	MISC_CRS_P5_03	1014
#define	MISC_CRS_P6_01	1015
#define	MISC_CRS_P7_01	1016
#define	MISC_YOKE	5001
#define	MISC_DUMMY	9001

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
