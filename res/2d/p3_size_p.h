/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#define	P3_BTN_APOFF_D_00_SX	46
#define	P3_BTN_APOFF_D_00_SY	42
#define	P3_BTN_APOFF_D_01_SX	46
#define	P3_BTN_APOFF_D_01_SY	42
#define	P3_BTN_APOFF_P_00_SX	46
#define	P3_BTN_APOFF_P_00_SY	42
#define	P3_BTN_APOFF_P_01_SX	46
#define	P3_BTN_APOFF_P_01_SY	42
#define	P3_BTN_COM_D_00_SX	46
#define	P3_BTN_COM_D_00_SY	42
#define	P3_BTN_COM_D_01_SX	46
#define	P3_BTN_COM_D_01_SY	42
#define	P3_BTN_COM_P_00_SX	46
#define	P3_BTN_COM_P_00_SY	42
#define	P3_BTN_COM_P_01_SX	46
#define	P3_BTN_COM_P_01_SY	42
#define	P3_BTN_TRIMEMERG_D_00_SX	65
#define	P3_BTN_TRIMEMERG_D_00_SY	84
#define	P3_BTN_TRIMEMERG_D_01_SX	65
#define	P3_BTN_TRIMEMERG_D_01_SY	84
#define	P3_BTN_TRIMEMERG_D_02_SX	65
#define	P3_BTN_TRIMEMERG_D_02_SY	84
#define	P3_BTN_TRIMEMERG_P_00_SX	65
#define	P3_BTN_TRIMEMERG_P_00_SY	84
#define	P3_BTN_TRIMEMERG_P_01_SX	65
#define	P3_BTN_TRIMEMERG_P_01_SY	84
#define	P3_BTN_TRIMEMERG_P_02_SX	65
#define	P3_BTN_TRIMEMERG_P_02_SY	84
#define	P3_BTN_TRIMMAIN_D_00_SX	62
#define	P3_BTN_TRIMMAIN_D_00_SY	89
#define	P3_BTN_TRIMMAIN_D_01_SX	62
#define	P3_BTN_TRIMMAIN_D_01_SY	89
#define	P3_BTN_TRIMMAIN_D_02_SX	62
#define	P3_BTN_TRIMMAIN_D_02_SY	89
#define	P3_BTN_TRIMMAIN_P_00_SX	62
#define	P3_BTN_TRIMMAIN_P_00_SY	89
#define	P3_BTN_TRIMMAIN_P_01_SX	62
#define	P3_BTN_TRIMMAIN_P_01_SY	89
#define	P3_BTN_TRIMMAIN_P_02_SX	62
#define	P3_BTN_TRIMMAIN_P_02_SY	89
#define	P3_BACKGROUND_SX	1114
#define	P3_BACKGROUND_SY	412
#define	P3_BACKGROUND_D_SX	1114
#define	P3_BACKGROUND_D_SY	412
#define	P3_BACKGROUND_P_SX	1114
#define	P3_BACKGROUND_P_SY	412
#define	MISC_CRS_P0_01_SX	81
#define	MISC_CRS_P0_01_SY	80
#define	MISC_CRS_P0_02_SX	103
#define	MISC_CRS_P0_02_SY	51
#define	MISC_CRS_P0_03_SX	103
#define	MISC_CRS_P0_03_SY	51
#define	MISC_CRS_P0_04_SX	54
#define	MISC_CRS_P0_04_SY	106
#define	MISC_CRS_P1_01_SX	81
#define	MISC_CRS_P1_01_SY	80
#define	MISC_CRS_P1_02_SX	103
#define	MISC_CRS_P1_02_SY	51
#define	MISC_CRS_P1_03_SX	103
#define	MISC_CRS_P1_03_SY	51
#define	MISC_CRS_P1_04_SX	54
#define	MISC_CRS_P1_04_SY	106
#define	MISC_CRS_P4_01_SX	81
#define	MISC_CRS_P4_01_SY	80
#define	MISC_CRS_P4_02_SX	105
#define	MISC_CRS_P4_02_SY	73
#define	MISC_CRS_P4_03_SX	81
#define	MISC_CRS_P4_03_SY	80
#define	MISC_CRS_P5_01_SX	81
#define	MISC_CRS_P5_01_SY	80
#define	MISC_CRS_P5_02_SX	105
#define	MISC_CRS_P5_02_SY	73
#define	MISC_CRS_P5_03_SX	81
#define	MISC_CRS_P5_03_SY	80
#define	MISC_CRS_P6_01_SX	81
#define	MISC_CRS_P6_01_SY	80
#define	MISC_CRS_P7_01_SX	81
#define	MISC_CRS_P7_01_SY	80
#define	MISC_YOKE_SX	100
#define	MISC_YOKE_SY	48
#define	MISC_DUMMY_SX	1
#define	MISC_DUMMY_SY	1
