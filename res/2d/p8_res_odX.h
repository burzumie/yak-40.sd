/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "p8_size_odX.h"

#define	P8_AZS_811_D_00	1001
#define	P8_AZS_811_D_01	1002
#define	P8_AZS_812_D_00	1003
#define	P8_AZS_812_D_01	1004
#define	P8_AZS_813_D_00	1005
#define	P8_AZS_813_D_01	1006
#define	P8_AZS_814_D_00	1007
#define	P8_AZS_814_D_01	1008
#define	P8_AZS_815_D_00	1009
#define	P8_AZS_815_D_01	1010
#define	P8_AZS_816_D_00	1011
#define	P8_AZS_816_D_01	1012
#define	P8_AZS_817_D_00	1013
#define	P8_AZS_817_D_01	1014
#define	P8_AZS_818_D_00	1015
#define	P8_AZS_818_D_01	1016
#define	P8_AZS_819_D_00	1017
#define	P8_AZS_819_D_01	1018
#define	P8_AZS_821_D_00	1019
#define	P8_AZS_821_D_01	1020
#define	P8_AZS_822_D_00	1021
#define	P8_AZS_822_D_01	1022
#define	P8_AZS_823_D_00	1023
#define	P8_AZS_823_D_01	1024
#define	P8_AZS_824_D_00	1025
#define	P8_AZS_824_D_01	1026
#define	P8_AZS_825_D_00	1027
#define	P8_AZS_825_D_01	1028
#define	P8_AZS_826_D_00	1029
#define	P8_AZS_826_D_01	1030
#define	P8_AZS_827_D_00	1031
#define	P8_AZS_827_D_01	1032
#define	P8_AZS_828_D_00	1033
#define	P8_AZS_828_D_01	1034
#define	P8_AZS_829_D_00	1035
#define	P8_AZS_829_D_01	1036
#define	P8_AZS_831_D_00	1037
#define	P8_AZS_831_D_01	1038
#define	P8_AZS_832_D_00	1039
#define	P8_AZS_832_D_01	1040
#define	P8_AZS_833_D_00	1041
#define	P8_AZS_833_D_01	1042
#define	P8_AZS_834_D_00	1043
#define	P8_AZS_834_D_01	1044
#define	P8_AZS_835_D_00	1045
#define	P8_AZS_835_D_01	1046
#define	P8_AZS_836_D_00	1047
#define	P8_AZS_836_D_01	1048
#define	P8_AZS_837_D_00	1049
#define	P8_AZS_837_D_01	1050
#define	P8_AZS_838_D_00	1051
#define	P8_AZS_838_D_01	1052
#define	P8_AZS_839_D_00	1053
#define	P8_AZS_839_D_01	1054
#define	P8_AZS_841_D_00	1055
#define	P8_AZS_841_D_01	1056
#define	P8_AZS_842_D_00	1057
#define	P8_AZS_842_D_01	1058
#define	P8_AZS_843_D_00	1059
#define	P8_AZS_843_D_01	1060
#define	P8_AZS_844_D_00	1061
#define	P8_AZS_844_D_01	1062
#define	P8_AZS_845_D_00	1063
#define	P8_AZS_845_D_01	1064
#define	P8_AZS_846_D_00	1065
#define	P8_AZS_846_D_01	1066
#define	P8_AZS_847_D_00	1067
#define	P8_AZS_847_D_01	1068
#define	P8_AZS_848_D_00	1069
#define	P8_AZS_848_D_01	1070
#define	P8_AZS_849_D_00	1071
#define	P8_AZS_849_D_01	1072
#define	P8_BACKGROUND_D	5001
#define	MISC_CRS_P0_01	13001
#define	MISC_CRS_P0_02	13003
#define	MISC_CRS_P0_03	13005
#define	MISC_CRS_P0_04	13007
#define	MISC_CRS_P1_01	13009
#define	MISC_CRS_P1_02	13011
#define	MISC_CRS_P1_03	13013
#define	MISC_CRS_P1_04	13015
#define	MISC_CRS_P4_01	13017
#define	MISC_CRS_P4_02	13019
#define	MISC_CRS_P4_03	13021
#define	MISC_CRS_P5_01	13023
#define	MISC_CRS_P5_02	13025
#define	MISC_CRS_P5_03	13027
#define	MISC_CRS_P6_01	13029
#define	MISC_CRS_P7_01	13031
#define	MISC_YOKE	17001
#define	MISC_DUMMY	21001

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
