/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../Version.h"

#include "p5_size_p3d.h"

#define	MISC_CRS_P0_01	1001
#define	MISC_CRS_P0_02	1003
#define	MISC_CRS_P0_03	1005
#define	MISC_CRS_P0_04	1007
#define	MISC_CRS_P1_01	1009
#define	MISC_CRS_P1_02	1011
#define	MISC_CRS_P1_03	1013
#define	MISC_CRS_P1_04	1015
#define	MISC_CRS_P4_01	1017
#define	MISC_CRS_P4_02	1019
#define	MISC_CRS_P4_03	1021
#define	MISC_CRS_P5_01	1023
#define	MISC_CRS_P5_02	1025
#define	MISC_CRS_P5_03	1027
#define	MISC_CRS_P6_01	1029
#define	MISC_CRS_P7_01	1031
#define	MISC_YOKE	5001
#define	MISC_DUMMY	9001
#define	P53D_AZS_LIGHTSBCN_D_00	13001
#define	P53D_AZS_LIGHTSBCN_D_01	13002
#define	P53D_AZS_LIGHTSBCN_P_00	13003
#define	P53D_AZS_LIGHTSBCN_P_01	13004
#define	P53D_AZS_LIGHTSNAV_D_00	13005
#define	P53D_AZS_LIGHTSNAV_D_01	13006
#define	P53D_AZS_LIGHTSNAV_P_00	13007
#define	P53D_AZS_LIGHTSNAV_P_01	13008
#define	P53D_AZS_LLIGHTLEFT_D_00	13009
#define	P53D_AZS_LLIGHTLEFT_D_01	13010
#define	P53D_AZS_LLIGHTLEFT_D_02	13011
#define	P53D_AZS_LLIGHTLEFT_P_00	13012
#define	P53D_AZS_LLIGHTLEFT_P_01	13013
#define	P53D_AZS_LLIGHTLEFT_P_02	13014
#define	P53D_AZS_LLIGHTRIGHT_D_00	13015
#define	P53D_AZS_LLIGHTRIGHT_D_01	13016
#define	P53D_AZS_LLIGHTRIGHT_D_02	13017
#define	P53D_AZS_LLIGHTRIGHT_P_00	13018
#define	P53D_AZS_LLIGHTRIGHT_P_01	13019
#define	P53D_AZS_LLIGHTRIGHT_P_02	13020
#define	P53D_AZS_LLIGHTSMOTOR_D_00	13021
#define	P53D_AZS_LLIGHTSMOTOR_D_01	13022
#define	P53D_AZS_LLIGHTSMOTOR_P_00	13023
#define	P53D_AZS_LLIGHTSMOTOR_P_01	13024
#define	P53D_BACKGROUND1_D	17001
#define	P53D_BACKGROUND1_P	17002

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
