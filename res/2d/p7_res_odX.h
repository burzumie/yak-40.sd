/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "p7_size_odX.h"

#define	P7_AZS_ENG1HEAT_D_00	1001
#define	P7_AZS_ENG1HEAT_D_01	1002
#define	P7_AZS_ENG2HEAT_D_00	1003
#define	P7_AZS_ENG2HEAT_D_01	1004
#define	P7_AZS_ENG3HEAT_D_00	1005
#define	P7_AZS_ENG3HEAT_D_01	1006
#define	P7_AZS_ICEWARN1_D_00	1007
#define	P7_AZS_ICEWARN1_D_01	1008
#define	P7_AZS_ICEWARN2_D_00	1009
#define	P7_AZS_ICEWARN2_D_01	1010
#define	P7_AZS_KURSMP2PWRLIT_D_00	1011
#define	P7_AZS_KURSMP2PWRLIT_D_01	1012
#define	P7_AZS_KURSMP2PWR_D_00	1013
#define	P7_AZS_KURSMP2PWR_D_01	1014
#define	P7_AZS_KURSMP2VORLIT_D_00	1015
#define	P7_AZS_KURSMP2VORLIT_D_01	1016
#define	P7_AZS_KURSMP2VOR_D_00	1017
#define	P7_AZS_KURSMP2VOR_D_01	1018
#define	P7_AZS_PITOT1HEAT_D_00	1019
#define	P7_AZS_PITOT1HEAT_D_01	1020
#define	P7_AZS_PITOT2HEAT_D_00	1021
#define	P7_AZS_PITOT2HEAT_D_01	1022
#define	P7_BTN_KURSMP2TEST1LIT_D_00	5001
#define	P7_BTN_KURSMP2TEST1LIT_D_01	5002
#define	P7_BTN_KURSMP2TEST1_D_00	5003
#define	P7_BTN_KURSMP2TEST1_D_01	5004
#define	P7_BTN_KURSMP2TEST2LIT_D_00	5005
#define	P7_BTN_KURSMP2TEST2LIT_D_01	5006
#define	P7_BTN_KURSMP2TEST2_D_00	5007
#define	P7_BTN_KURSMP2TEST2_D_01	5008
#define	P7_BTN_KURSMP2_TEST3LIT_D_00	5009
#define	P7_BTN_KURSMP2_TEST3LIT_D_01	5010
#define	P7_BTN_KURSMP2_TEST3_D_00	5011
#define	P7_BTN_KURSMP2_TEST3_D_01	5012
#define	P7_BTN_KURSMPRIGHTID_D_00	5013
#define	P7_BTN_KURSMPRIGHTID_D_01	5014
#define	P7_BTN_LTSTEST_D_00	5015
#define	P7_BTN_LTSTEST_D_01	5016
#define	P7_COV_UNIT2077A_D	9001
#define	P7_COV_UNIT2077B_D	9002
#define	P7_COV_UNIT2077C_D	9003
#define	P7_LMP_ENG1DEICESHINE_D_01	13001
#define	P7_LMP_ENG1DEICESHINE_D_02	13002
#define	P7_LMP_ENG1DEICE_D_00	13003
#define	P7_LMP_ENG1DEICE_D_01	13004
#define	P7_LMP_ENG1DEICE_D_02	13005
#define	P7_LMP_ENG2DEICESHINE_D_01	13006
#define	P7_LMP_ENG2DEICESHINE_D_02	13007
#define	P7_LMP_ENG2DEICE_D_00	13008
#define	P7_LMP_ENG2DEICE_D_01	13009
#define	P7_LMP_ENG2DEICE_D_02	13010
#define	P7_LMP_ENG3DEICESHINE_D_01	13011
#define	P7_LMP_ENG3DEICESHINE_D_02	13012
#define	P7_LMP_ENG3DEICE_D_00	13013
#define	P7_LMP_ENG3DEICE_D_01	13014
#define	P7_LMP_ENG3DEICE_D_02	13015
#define	P7_NDL_UNIT2077DIFPRESS_D	17001
#define	P7_NDL_UNIT2077PRESSBEGIN_D	17002
#define	P7_SWT_2077PWR_D_00	21001
#define	P7_SWT_2077PWR_D_01	21002
#define	P7_BACKGROUND10_D	25001
#define	P7_BACKGROUND1_D	25002
#define	P7_BACKGROUND2_D	25003
#define	P7_BACKGROUND3_D	25004
#define	P7_BACKGROUND4_D	25005
#define	P7_BACKGROUND5_D	25006
#define	P7_BACKGROUND6_D	25007
#define	P7_BACKGROUND7_D	25008
#define	P7_BACKGROUND8_D	25009
#define	P7_BACKGROUND9_D	25010
#define	P7_BACKGROUND_LIT_D	25011
#define	P7_KNB_KURSMPRIGHTFREQ_D_00	29001
#define	P7_KNB_KURSMPRIGHTFREQ_D_01	29002
#define	P7_KNB_KURSMPRIGHTFREQ_D_02	29003
#define	P7_KNB_KURSMPRIGHTFREQ_D_03	29004
#define	P7_KNB_KURSMPRIGHTFREQ_D_04	29005
#define	P7_KNB_KURSMPRIGHTFREQ_D_05	29006
#define	P7_KNB_KURSMPRIGHTFREQ_D_06	29007
#define	P7_KNB_KURSMPRIGHTFREQ_D_07	29008
#define	P7_KNB_KURSMPRIGHTFREQ_D_08	29009
#define	P7_KNB_KURSMPRIGHTFREQ_D_09	29010
#define	P7_KNB_KURSMPRIGHTFREQ_D_10	29011
#define	P7_KNB_KURSMPRIGHTFREQ_D_11	29012
#define	P7_KNB_KURSMPRIGHTFREQ_D_12	29013
#define	P7_KNB_KURSMPRIGHTFREQ_D_13	29014
#define	P7_KNB_KURSMPRIGHTFREQ_D_14	29015
#define	P7_KNB_KURSMPRIGHTFREQ_D_15	29016
#define	P7_KNB_KURSMPRIGHTFREQ_D_16	29017
#define	P7_KNB_KURSMPRIGHTFREQ_D_17	29018
#define	P7_KNB_KURSMPRIGHTFREQ_D_18	29019
#define	P7_KNB_KURSMPRIGHTFREQ_D_19	29020
#define	P7_KNB_SGURIGHT_D_00	33001
#define	P7_KNB_SGURIGHT_D_01	33002
#define	P7_KNB_SGURIGHT_D_02	33003
#define	P7_KNB_SGURIGHT_D_03	33004
#define	P7_KNB_SGURIGHT_D_04	33005
#define	P7_KNB_PRESSCHANGERATE_D_00	37001
#define	P7_KNB_PRESSCHANGERATE_D_01	37002
#define	P7_KNB_PRESSCHANGERATE_D_02	37003
#define	P7_KNB_PRESSCHANGERATE_D_03	37004
#define	P7_KNB_PRESSCHANGERATE_D_04	37005
#define	P7_KNB_PRESSCHANGERATE_D_05	37006
#define	P7_KNB_PRESSCHANGERATE_D_06	37007
#define	P7_KNB_PRESSCHANGERATE_D_07	37008
#define	P7_KNB_PRESSCHANGERATE_D_08	37009
#define	P7_KNB_PRESSCHANGERATE_D_09	37010
#define	P7_KNB_PRESSCHANGERATE_D_10	37011
#define	P7_KNB_PRESSCHANGERATE_D_11	37012
#define	P7_KNB_PRESSCHANGERATE_D_12	37013
#define	P7_KNB_PRESSCHANGERATE_D_13	37014
#define	P7_KNB_PRESSCHANGERATE_D_14	37015
#define	P7_KNB_PRESSCHANGERATE_D_15	37016
#define	P7_KNB_PRESSCHANGERATE_D_16	37017
#define	P7_KNB_PRESSCHANGERATE_D_17	37018
#define	P7_KNB_PRESSCHANGERATE_D_18	37019
#define	P7_KNB_PRESSCHANGERATE_D_19	37020
#define	P7_KNB_PRESSCHANGERATE_D_20	37021
#define	P7_KNB_PRESSCHANGERATE_D_21	37022
#define	P7_KNB_PRESSCHANGERATE_D_22	37023
#define	P7_KNB_PRESSCHANGERATE_D_23	37024
#define	P7_KNB_PRESSCHANGERATE_D_24	37025
#define	P7_KNB_PRESSCHANGERATE_D_25	37026
#define	P7_KNB_PRESSCHANGERATE_D_26	37027
#define	P7_KNB_PRESSCHANGERATE_D_27	37028
#define	P7_KNB_PRESSCHANGERATE_D_28	37029
#define	P7_KNB_PRESSCHANGERATE_D_29	37030
#define	P7_KNB_PRESSCHANGERATE_D_30	37031
#define	P7_KNB_PRESSCHANGERATE_D_31	37032
#define	P7_KNB_PRESSCHANGERATE_D_32	37033
#define	P7_KNB_PRESSCHANGERATE_D_33	37034
#define	P7_KNB_PRESSCHANGERATE_D_34	37035
#define	P7_KNB_PRESSCHANGERATE_D_35	37036
#define	P7_KNB_PRESSCHANGERATE_D_36	37037
#define	P7_KNB_PRESSCHANGERATE_D_37	37038
#define	P7_KNB_PRESSCHANGERATE_D_38	37039
#define	P7_KNB_PRESSCHANGERATE_D_39	37040
#define	P7_KNB_PRESSCHANGERATE_D_40	37041
#define	P7_KNB_PRESSCHANGERATE_D_41	37042
#define	P7_KNB_PRESSCHANGERATE_D_42	37043
#define	P7_KNB_PRESSCHANGERATE_D_43	37044
#define	P7_KNB_PRESSCHANGERATE_D_44	37045
#define	P7_KNB_PRESSCHANGERATE_D_45	37046
#define	P7_KNB_DIFFPRESS_D_00	41001
#define	P7_KNB_DIFFPRESS_D_01	41002
#define	P7_KNB_DIFFPRESS_D_02	41003
#define	P7_KNB_DIFFPRESS_D_03	41004
#define	P7_KNB_DIFFPRESS_D_04	41005
#define	P7_KNB_DIFFPRESS_D_05	41006
#define	P7_KNB_DIFFPRESS_D_06	41007
#define	P7_KNB_DIFFPRESS_D_07	41008
#define	P7_KNB_DIFFPRESS_D_08	41009
#define	P7_KNB_DIFFPRESS_D_09	41010
#define	P7_KNB_DIFFPRESS_D_10	41011
#define	P7_KNB_DIFFPRESS_D_11	41012
#define	P7_KNB_DIFFPRESS_D_12	41013
#define	P7_KNB_DIFFPRESS_D_13	41014
#define	P7_KNB_DIFFPRESS_D_14	41015
#define	P7_KNB_DIFFPRESS_D_15	41016
#define	P7_KNB_DIFFPRESS_D_16	41017
#define	P7_KNB_DIFFPRESS_D_17	41018
#define	P7_KNB_DIFFPRESS_D_18	41019
#define	P7_KNB_DIFFPRESS_D_19	41020
#define	P7_KNB_BEGINPRESS_D_00	45001
#define	P7_KNB_BEGINPRESS_D_01	45002
#define	P7_KNB_BEGINPRESS_D_02	45003
#define	P7_KNB_BEGINPRESS_D_03	45004
#define	P7_KNB_BEGINPRESS_D_04	45005
#define	P7_KNB_BEGINPRESS_D_05	45006
#define	P7_KNB_BEGINPRESS_D_06	45007
#define	P7_KNB_BEGINPRESS_D_07	45008
#define	P7_KNB_BEGINPRESS_D_08	45009
#define	P7_KNB_BEGINPRESS_D_09	45010
#define	P7_KNB_BEGINPRESS_D_10	45011
#define	P7_KNB_BEGINPRESS_D_11	45012
#define	P7_KNB_BEGINPRESS_D_12	45013
#define	P7_KNB_BEGINPRESS_D_13	45014
#define	P7_KNB_BEGINPRESS_D_14	45015
#define	P7_KNB_BEGINPRESS_D_15	45016
#define	P7_KNB_BEGINPRESS_D_16	45017
#define	P7_KNB_BEGINPRESS_D_17	45018
#define	P7_KNB_BEGINPRESS_D_18	45019
#define	P7_KNB_BEGINPRESS_D_19	45020
#define	P7_KNB_OBSRIGHT_D_00	49001
#define	P7_KNB_OBSRIGHT_D_01	49002
#define	P7_KNB_OBSRIGHT_D_02	49003
#define	P7_KNB_OBSRIGHT_D_03	49004
#define	P7_KNB_OBSRIGHT_D_04	49005
#define	P7_KNB_OBSRIGHT_D_05	49006
#define	P7_KNB_OBSRIGHT_D_06	49007
#define	P7_KNB_OBSRIGHT_D_07	49008
#define	P7_KNB_OBSRIGHT_D_08	49009
#define	P7_KNB_OBSRIGHT_D_09	49010
#define	P7_KNB_OBSRIGHT_D_10	49011
#define	P7_KNB_OBSRIGHT_D_11	49012
#define	P7_KNB_OBSRIGHT_D_12	49013
#define	P7_KNB_OBSRIGHT_D_13	49014
#define	P7_KNB_OBSRIGHT_D_14	49015
#define	P7_KNB_OBSRIGHT_D_15	49016
#define	P7_KNB_OBSRIGHT_D_16	49017
#define	P7_KNB_OBSRIGHT_D_17	49018
#define	P7_KNB_OBSRIGHT_D_18	49019
#define	P7_KNB_OBSRIGHT_D_19	49020
#define	P7_KNB_DME2_D_00	53001
#define	P7_KNB_DME2_D_01	53002
#define	P7_SCL_1DMERIGHT1_D__	57001
#define	P7_SCL_1DMERIGHT2_D__	57002
#define	P7_SCL_1DMERIGHT3_D__	57003
#define	P7_SCL_1DMERIGHT4_D__	57004
#define	P7_SCL_1DMERIGHT5_D__	57005
#define	P7_SCL_DMERIGHT1_D_00	61001
#define	P7_SCL_DMERIGHT1_D_01	61002
#define	P7_SCL_DMERIGHT1_D_02	61003
#define	P7_SCL_DMERIGHT1_D_03	61004
#define	P7_SCL_DMERIGHT1_D_04	61005
#define	P7_SCL_DMERIGHT1_D_05	61006
#define	P7_SCL_DMERIGHT1_D_06	61007
#define	P7_SCL_DMERIGHT1_D_07	61008
#define	P7_SCL_DMERIGHT1_D_08	61009
#define	P7_SCL_DMERIGHT1_D_09	61010
#define	P7_SCL_DMERIGHT2_D_00	61011
#define	P7_SCL_DMERIGHT2_D_01	61012
#define	P7_SCL_DMERIGHT2_D_02	61013
#define	P7_SCL_DMERIGHT2_D_03	61014
#define	P7_SCL_DMERIGHT2_D_04	61015
#define	P7_SCL_DMERIGHT2_D_05	61016
#define	P7_SCL_DMERIGHT2_D_06	61017
#define	P7_SCL_DMERIGHT2_D_07	61018
#define	P7_SCL_DMERIGHT2_D_08	61019
#define	P7_SCL_DMERIGHT2_D_09	61020
#define	P7_SCL_DMERIGHT3_D_00	61021
#define	P7_SCL_DMERIGHT3_D_01	61022
#define	P7_SCL_DMERIGHT3_D_02	61023
#define	P7_SCL_DMERIGHT3_D_03	61024
#define	P7_SCL_DMERIGHT3_D_04	61025
#define	P7_SCL_DMERIGHT3_D_05	61026
#define	P7_SCL_DMERIGHT3_D_06	61027
#define	P7_SCL_DMERIGHT3_D_07	61028
#define	P7_SCL_DMERIGHT3_D_08	61029
#define	P7_SCL_DMERIGHT3_D_09	61030
#define	P7_SCL_DMERIGHT4_D_00	61031
#define	P7_SCL_DMERIGHT4_D_01	61032
#define	P7_SCL_DMERIGHT4_D_02	61033
#define	P7_SCL_DMERIGHT4_D_03	61034
#define	P7_SCL_DMERIGHT4_D_04	61035
#define	P7_SCL_DMERIGHT4_D_05	61036
#define	P7_SCL_DMERIGHT4_D_06	61037
#define	P7_SCL_DMERIGHT4_D_07	61038
#define	P7_SCL_DMERIGHT4_D_08	61039
#define	P7_SCL_DMERIGHT4_D_09	61040
#define	P7_SCL_DMERIGHT5_D_00	61041
#define	P7_SCL_DMERIGHT5_D_01	61042
#define	P7_SCL_DMERIGHT5_D_02	61043
#define	P7_SCL_DMERIGHT5_D_03	61044
#define	P7_SCL_DMERIGHT5_D_04	61045
#define	P7_SCL_DMERIGHT5_D_05	61046
#define	P7_SCL_DMERIGHT5_D_06	61047
#define	P7_SCL_DMERIGHT5_D_07	61048
#define	P7_SCL_DMERIGHT5_D_08	61049
#define	P7_SCL_DMERIGHT5_D_09	61050
#define	P7_SCL_KURSMPRIGHT1LIT_D_00	65001
#define	P7_SCL_KURSMPRIGHT1LIT_D_01	65002
#define	P7_SCL_KURSMPRIGHT1_D_00	65003
#define	P7_SCL_KURSMPRIGHT1_D_01	65004
#define	P7_SCL_KURSMPRIGHT2LIT_D_00	65005
#define	P7_SCL_KURSMPRIGHT2LIT_D_01	65006
#define	P7_SCL_KURSMPRIGHT2_D_00	65007
#define	P7_SCL_KURSMPRIGHT2_D_01	65008
#define	P7_SCL_KURSMPRIGHT3LIT_D_00	65009
#define	P7_SCL_KURSMPRIGHT3LIT_D_01	65010
#define	P7_SCL_KURSMPRIGHT3LIT_D_02	65011
#define	P7_SCL_KURSMPRIGHT3LIT_D_03	65012
#define	P7_SCL_KURSMPRIGHT3LIT_D_04	65013
#define	P7_SCL_KURSMPRIGHT3LIT_D_05	65014
#define	P7_SCL_KURSMPRIGHT3LIT_D_06	65015
#define	P7_SCL_KURSMPRIGHT3LIT_D_07	65016
#define	P7_SCL_KURSMPRIGHT3LIT_D_08	65017
#define	P7_SCL_KURSMPRIGHT3LIT_D_09	65018
#define	P7_SCL_KURSMPRIGHT3_D_00	65019
#define	P7_SCL_KURSMPRIGHT3_D_01	65020
#define	P7_SCL_KURSMPRIGHT3_D_02	65021
#define	P7_SCL_KURSMPRIGHT3_D_03	65022
#define	P7_SCL_KURSMPRIGHT3_D_04	65023
#define	P7_SCL_KURSMPRIGHT3_D_05	65024
#define	P7_SCL_KURSMPRIGHT3_D_06	65025
#define	P7_SCL_KURSMPRIGHT3_D_07	65026
#define	P7_SCL_KURSMPRIGHT3_D_08	65027
#define	P7_SCL_KURSMPRIGHT3_D_09	65028
#define	P7_SCL_KURSMPRIGHT4LIT_D_00	65029
#define	P7_SCL_KURSMPRIGHT4LIT_D_01	65030
#define	P7_SCL_KURSMPRIGHT4LIT_D_02	65031
#define	P7_SCL_KURSMPRIGHT4LIT_D_03	65032
#define	P7_SCL_KURSMPRIGHT4LIT_D_04	65033
#define	P7_SCL_KURSMPRIGHT4LIT_D_05	65034
#define	P7_SCL_KURSMPRIGHT4LIT_D_06	65035
#define	P7_SCL_KURSMPRIGHT4LIT_D_07	65036
#define	P7_SCL_KURSMPRIGHT4LIT_D_08	65037
#define	P7_SCL_KURSMPRIGHT4LIT_D_09	65038
#define	P7_SCL_KURSMPRIGHT4_D_00	65039
#define	P7_SCL_KURSMPRIGHT4_D_01	65040
#define	P7_SCL_KURSMPRIGHT4_D_02	65041
#define	P7_SCL_KURSMPRIGHT4_D_03	65042
#define	P7_SCL_KURSMPRIGHT4_D_04	65043
#define	P7_SCL_KURSMPRIGHT4_D_05	65044
#define	P7_SCL_KURSMPRIGHT4_D_06	65045
#define	P7_SCL_KURSMPRIGHT4_D_07	65046
#define	P7_SCL_KURSMPRIGHT4_D_08	65047
#define	P7_SCL_KURSMPRIGHT4_D_09	65048
#define	P7_SCL_KURSMPRIGHT5LIT_D_00	65049
#define	P7_SCL_KURSMPRIGHT5LIT_D_01	65050
#define	P7_SCL_KURSMPRIGHT5LIT_D_02	65051
#define	P7_SCL_KURSMPRIGHT5LIT_D_03	65052
#define	P7_SCL_KURSMPRIGHT5LIT_D_04	65053
#define	P7_SCL_KURSMPRIGHT5LIT_D_05	65054
#define	P7_SCL_KURSMPRIGHT5LIT_D_06	65055
#define	P7_SCL_KURSMPRIGHT5LIT_D_07	65056
#define	P7_SCL_KURSMPRIGHT5LIT_D_08	65057
#define	P7_SCL_KURSMPRIGHT5LIT_D_09	65058
#define	P7_SCL_KURSMPRIGHT5_D_00	65059
#define	P7_SCL_KURSMPRIGHT5_D_01	65060
#define	P7_SCL_KURSMPRIGHT5_D_02	65061
#define	P7_SCL_KURSMPRIGHT5_D_03	65062
#define	P7_SCL_KURSMPRIGHT5_D_04	65063
#define	P7_SCL_KURSMPRIGHT5_D_05	65064
#define	P7_SCL_KURSMPRIGHT5_D_06	65065
#define	P7_SCL_KURSMPRIGHT5_D_07	65066
#define	P7_SCL_KURSMPRIGHT5_D_08	65067
#define	P7_SCL_KURSMPRIGHT5_D_09	65068
#define	P7_SCL_OBSRIGHT1LIT_D_00	69001
#define	P7_SCL_OBSRIGHT1LIT_D_01	69002
#define	P7_SCL_OBSRIGHT1LIT_D_02	69003
#define	P7_SCL_OBSRIGHT1LIT_D_03	69004
#define	P7_SCL_OBSRIGHT1_D_00	69005
#define	P7_SCL_OBSRIGHT1_D_01	69006
#define	P7_SCL_OBSRIGHT1_D_02	69007
#define	P7_SCL_OBSRIGHT1_D_03	69008
#define	P7_SCL_OBSRIGHT2LIT_D_00	69009
#define	P7_SCL_OBSRIGHT2LIT_D_01	69010
#define	P7_SCL_OBSRIGHT2LIT_D_02	69011
#define	P7_SCL_OBSRIGHT2LIT_D_03	69012
#define	P7_SCL_OBSRIGHT2LIT_D_04	69013
#define	P7_SCL_OBSRIGHT2LIT_D_05	69014
#define	P7_SCL_OBSRIGHT2LIT_D_06	69015
#define	P7_SCL_OBSRIGHT2LIT_D_07	69016
#define	P7_SCL_OBSRIGHT2LIT_D_08	69017
#define	P7_SCL_OBSRIGHT2LIT_D_09	69018
#define	P7_SCL_OBSRIGHT2_D_00	69019
#define	P7_SCL_OBSRIGHT2_D_01	69020
#define	P7_SCL_OBSRIGHT2_D_02	69021
#define	P7_SCL_OBSRIGHT2_D_03	69022
#define	P7_SCL_OBSRIGHT2_D_04	69023
#define	P7_SCL_OBSRIGHT2_D_05	69024
#define	P7_SCL_OBSRIGHT2_D_06	69025
#define	P7_SCL_OBSRIGHT2_D_07	69026
#define	P7_SCL_OBSRIGHT2_D_08	69027
#define	P7_SCL_OBSRIGHT2_D_09	69028
#define	P7_SCL_OBSRIGHT3LIT_D_00	69029
#define	P7_SCL_OBSRIGHT3LIT_D_01	69030
#define	P7_SCL_OBSRIGHT3LIT_D_02	69031
#define	P7_SCL_OBSRIGHT3LIT_D_03	69032
#define	P7_SCL_OBSRIGHT3LIT_D_04	69033
#define	P7_SCL_OBSRIGHT3LIT_D_05	69034
#define	P7_SCL_OBSRIGHT3LIT_D_06	69035
#define	P7_SCL_OBSRIGHT3LIT_D_07	69036
#define	P7_SCL_OBSRIGHT3LIT_D_08	69037
#define	P7_SCL_OBSRIGHT3LIT_D_09	69038
#define	P7_SCL_OBSRIGHT3_D_00	69039
#define	P7_SCL_OBSRIGHT3_D_01	69040
#define	P7_SCL_OBSRIGHT3_D_02	69041
#define	P7_SCL_OBSRIGHT3_D_03	69042
#define	P7_SCL_OBSRIGHT3_D_04	69043
#define	P7_SCL_OBSRIGHT3_D_05	69044
#define	P7_SCL_OBSRIGHT3_D_06	69045
#define	P7_SCL_OBSRIGHT3_D_07	69046
#define	P7_SCL_OBSRIGHT3_D_08	69047
#define	P7_SCL_OBSRIGHT3_D_09	69048
#define	P7_SCL_PRESSCHANGERATE_D_00	73001
#define	P7_SCL_PRESSCHANGERATE_D_01	73002
#define	P7_SCL_PRESSCHANGERATE_D_02	73003
#define	P7_SCL_PRESSCHANGERATE_D_03	73004
#define	P7_SCL_PRESSCHANGERATE_D_04	73005
#define	P7_SCL_PRESSCHANGERATE_D_05	73006
#define	P7_SCL_PRESSCHANGERATE_D_06	73007
#define	P7_SCL_PRESSCHANGERATE_D_07	73008
#define	P7_SCL_PRESSCHANGERATE_D_08	73009
#define	P7_SCL_PRESSCHANGERATE_D_09	73010
#define	P7_SCL_PRESSCHANGERATE_D_10	73011
#define	P7_SCL_PRESSCHANGERATE_D_11	73012
#define	P7_SCL_PRESSCHANGERATE_D_12	73013
#define	P7_SCL_PRESSCHANGERATE_D_13	73014
#define	P7_SCL_PRESSCHANGERATE_D_14	73015
#define	P7_SCL_PRESSCHANGERATE_D_15	73016
#define	P7_SCL_PRESSCHANGERATE_D_16	73017
#define	P7_SCL_PRESSCHANGERATE_D_17	73018
#define	P7_SCL_PRESSCHANGERATE_D_18	73019
#define	P7_SCL_PRESSCHANGERATE_D_19	73020
#define	P7_SCL_PRESSCHANGERATE_D_20	73021
#define	P7_SCL_PRESSCHANGERATE_D_21	73022
#define	P7_SCL_PRESSCHANGERATE_D_22	73023
#define	P7_SCL_PRESSCHANGERATE_D_23	73024
#define	P7_SCL_PRESSCHANGERATE_D_24	73025
#define	P7_SCL_PRESSCHANGERATE_D_25	73026
#define	P7_SCL_PRESSCHANGERATE_D_26	73027
#define	P7_SCL_PRESSCHANGERATE_D_27	73028
#define	P7_SCL_PRESSCHANGERATE_D_28	73029
#define	P7_SCL_PRESSCHANGERATE_D_29	73030
#define	P7_SCL_PRESSCHANGERATE_D_30	73031
#define	P7_SCL_PRESSCHANGERATE_D_31	73032
#define	P7_SCL_PRESSCHANGERATE_D_32	73033
#define	P7_SCL_PRESSCHANGERATE_D_33	73034
#define	P7_SCL_PRESSCHANGERATE_D_34	73035
#define	P7_SCL_PRESSCHANGERATE_D_35	73036
#define	P7_SCL_PRESSCHANGERATE_D_36	73037
#define	P7_SCL_PRESSCHANGERATE_D_37	73038
#define	P7_SCL_PRESSCHANGERATE_D_38	73039
#define	P7_SCL_PRESSCHANGERATE_D_39	73040
#define	P7_SCL_PRESSCHANGERATE_D_40	73041
#define	P7_SCL_PRESSCHANGERATE_D_41	73042
#define	P7_SCL_PRESSCHANGERATE_D_42	73043
#define	P7_SCL_PRESSCHANGERATE_D_43	73044
#define	P7_SCL_PRESSCHANGERATE_D_44	73045
#define	P7_SCL_PRESSCHANGERATE_D_45	73046
#define	MISC_CRS_P0_01	81001
#define	MISC_CRS_P0_02	81003
#define	MISC_CRS_P0_03	81005
#define	MISC_CRS_P0_04	81007
#define	MISC_CRS_P1_01	81009
#define	MISC_CRS_P1_02	81011
#define	MISC_CRS_P1_03	81013
#define	MISC_CRS_P1_04	81015
#define	MISC_CRS_P4_01	81017
#define	MISC_CRS_P4_02	81019
#define	MISC_CRS_P4_03	81021
#define	MISC_CRS_P5_01	81023
#define	MISC_CRS_P5_02	81025
#define	MISC_CRS_P5_03	81027
#define	MISC_CRS_P6_01	81029
#define	MISC_CRS_P7_01	81031
#define	MISC_YOKE	85001
#define	MISC_DUMMY	89001
#define	P20_AZS_MPFREQDMELIT_D_00	93001
#define	P20_AZS_MPFREQDMELIT_D_01	93002
#define	P20_AZS_MPFREQDME_D_00	93003
#define	P20_AZS_MPFREQDME_D_01	93004
#define	P20_AZS_MPFREQPWRLIT_D_00	93005
#define	P20_AZS_MPFREQPWRLIT_D_01	93006
#define	P20_AZS_MPFREQPWR_D_00	93007
#define	P20_AZS_MPFREQPWR_D_01	93008
#define	P20_BTN_MPAUDIO_D_00	97001
#define	P20_BTN_MPAUDIO_D_01	97002
#define	P20_KNB_OBS_D_00	101001
#define	P20_KNB_OBS_D_01	101002
#define	P20_KNB_OBS_D_02	101003
#define	P20_KNB_OBS_D_03	101004
#define	P20_KNB_OBS_D_04	101005
#define	P20_KNB_OBS_D_05	101006
#define	P20_KNB_OBS_D_06	101007
#define	P20_KNB_OBS_D_07	101008
#define	P20_KNB_OBS_D_08	101009
#define	P20_KNB_OBS_D_09	101010
#define	P20_KNB_DME1_D_00	105001
#define	P20_KNB_DME1_D_01	105002
#define	P20_SCL_1DMELEFT1_D__	109001
#define	P20_SCL_1DMELEFT2_D__	109002
#define	P20_SCL_1DMELEFT3_D__	109003
#define	P20_SCL_1DMELEFT4_D__	109004
#define	P20_SCL_1DMELEFT5_D__	109005
#define	P20_SCL_DMELEFT1_D_00	113001
#define	P20_SCL_DMELEFT1_D_01	113002
#define	P20_SCL_DMELEFT1_D_02	113003
#define	P20_SCL_DMELEFT1_D_03	113004
#define	P20_SCL_DMELEFT1_D_04	113005
#define	P20_SCL_DMELEFT1_D_05	113006
#define	P20_SCL_DMELEFT1_D_06	113007
#define	P20_SCL_DMELEFT1_D_07	113008
#define	P20_SCL_DMELEFT1_D_08	113009
#define	P20_SCL_DMELEFT1_D_09	113010
#define	P20_SCL_DMELEFT2_D_00	113011
#define	P20_SCL_DMELEFT2_D_01	113012
#define	P20_SCL_DMELEFT2_D_02	113013
#define	P20_SCL_DMELEFT2_D_03	113014
#define	P20_SCL_DMELEFT2_D_04	113015
#define	P20_SCL_DMELEFT2_D_05	113016
#define	P20_SCL_DMELEFT2_D_06	113017
#define	P20_SCL_DMELEFT2_D_07	113018
#define	P20_SCL_DMELEFT2_D_08	113019
#define	P20_SCL_DMELEFT2_D_09	113020
#define	P20_SCL_DMELEFT3_D_00	113021
#define	P20_SCL_DMELEFT3_D_01	113022
#define	P20_SCL_DMELEFT3_D_02	113023
#define	P20_SCL_DMELEFT3_D_03	113024
#define	P20_SCL_DMELEFT3_D_04	113025
#define	P20_SCL_DMELEFT3_D_05	113026
#define	P20_SCL_DMELEFT3_D_06	113027
#define	P20_SCL_DMELEFT3_D_07	113028
#define	P20_SCL_DMELEFT3_D_08	113029
#define	P20_SCL_DMELEFT3_D_09	113030
#define	P20_SCL_DMELEFT4_D_00	113031
#define	P20_SCL_DMELEFT4_D_01	113032
#define	P20_SCL_DMELEFT4_D_02	113033
#define	P20_SCL_DMELEFT4_D_03	113034
#define	P20_SCL_DMELEFT4_D_04	113035
#define	P20_SCL_DMELEFT4_D_05	113036
#define	P20_SCL_DMELEFT4_D_06	113037
#define	P20_SCL_DMELEFT4_D_07	113038
#define	P20_SCL_DMELEFT4_D_08	113039
#define	P20_SCL_DMELEFT4_D_09	113040
#define	P20_SCL_DMELEFT5_D_00	113041
#define	P20_SCL_DMELEFT5_D_01	113042
#define	P20_SCL_DMELEFT5_D_02	113043
#define	P20_SCL_DMELEFT5_D_03	113044
#define	P20_SCL_DMELEFT5_D_04	113045
#define	P20_SCL_DMELEFT5_D_05	113046
#define	P20_SCL_DMELEFT5_D_06	113047
#define	P20_SCL_DMELEFT5_D_07	113048
#define	P20_SCL_DMELEFT5_D_08	113049
#define	P20_SCL_DMELEFT5_D_09	113050
#define	P20_SCL_MPFREQ1LIT_D_00	117001
#define	P20_SCL_MPFREQ1LIT_D_01	117002
#define	P20_SCL_MPFREQ1_D_00	117003
#define	P20_SCL_MPFREQ1_D_01	117004
#define	P20_SCL_MPFREQ2LIT_D_00	117005
#define	P20_SCL_MPFREQ2LIT_D_01	117006
#define	P20_SCL_MPFREQ2_D_00	117007
#define	P20_SCL_MPFREQ2_D_01	117008
#define	P20_SCL_MPFREQ3LIT_D_00	117009
#define	P20_SCL_MPFREQ3LIT_D_01	117010
#define	P20_SCL_MPFREQ3LIT_D_02	117011
#define	P20_SCL_MPFREQ3LIT_D_03	117012
#define	P20_SCL_MPFREQ3LIT_D_04	117013
#define	P20_SCL_MPFREQ3LIT_D_05	117014
#define	P20_SCL_MPFREQ3LIT_D_06	117015
#define	P20_SCL_MPFREQ3LIT_D_07	117016
#define	P20_SCL_MPFREQ3LIT_D_08	117017
#define	P20_SCL_MPFREQ3LIT_D_09	117018
#define	P20_SCL_MPFREQ3_D_00	117019
#define	P20_SCL_MPFREQ3_D_01	117020
#define	P20_SCL_MPFREQ3_D_02	117021
#define	P20_SCL_MPFREQ3_D_03	117022
#define	P20_SCL_MPFREQ3_D_04	117023
#define	P20_SCL_MPFREQ3_D_05	117024
#define	P20_SCL_MPFREQ3_D_06	117025
#define	P20_SCL_MPFREQ3_D_07	117026
#define	P20_SCL_MPFREQ3_D_08	117027
#define	P20_SCL_MPFREQ3_D_09	117028
#define	P20_SCL_MPFREQ4LIT_D_00	117029
#define	P20_SCL_MPFREQ4LIT_D_01	117030
#define	P20_SCL_MPFREQ4LIT_D_02	117031
#define	P20_SCL_MPFREQ4LIT_D_03	117032
#define	P20_SCL_MPFREQ4LIT_D_04	117033
#define	P20_SCL_MPFREQ4LIT_D_05	117034
#define	P20_SCL_MPFREQ4LIT_D_06	117035
#define	P20_SCL_MPFREQ4LIT_D_07	117036
#define	P20_SCL_MPFREQ4LIT_D_08	117037
#define	P20_SCL_MPFREQ4LIT_D_09	117038
#define	P20_SCL_MPFREQ4_D_00	117039
#define	P20_SCL_MPFREQ4_D_01	117040
#define	P20_SCL_MPFREQ4_D_02	117041
#define	P20_SCL_MPFREQ4_D_03	117042
#define	P20_SCL_MPFREQ4_D_04	117043
#define	P20_SCL_MPFREQ4_D_05	117044
#define	P20_SCL_MPFREQ4_D_06	117045
#define	P20_SCL_MPFREQ4_D_07	117046
#define	P20_SCL_MPFREQ4_D_08	117047
#define	P20_SCL_MPFREQ4_D_09	117048
#define	P20_SCL_MPFREQ5LIT_D_00	117049
#define	P20_SCL_MPFREQ5LIT_D_01	117050
#define	P20_SCL_MPFREQ5LIT_D_02	117051
#define	P20_SCL_MPFREQ5LIT_D_03	117052
#define	P20_SCL_MPFREQ5LIT_D_04	117053
#define	P20_SCL_MPFREQ5LIT_D_05	117054
#define	P20_SCL_MPFREQ5LIT_D_06	117055
#define	P20_SCL_MPFREQ5LIT_D_07	117056
#define	P20_SCL_MPFREQ5LIT_D_08	117057
#define	P20_SCL_MPFREQ5LIT_D_09	117058
#define	P20_SCL_MPFREQ5_D_00	117059
#define	P20_SCL_MPFREQ5_D_01	117060
#define	P20_SCL_MPFREQ5_D_02	117061
#define	P20_SCL_MPFREQ5_D_03	117062
#define	P20_SCL_MPFREQ5_D_04	117063
#define	P20_SCL_MPFREQ5_D_05	117064
#define	P20_SCL_MPFREQ5_D_06	117065
#define	P20_SCL_MPFREQ5_D_07	117066
#define	P20_SCL_MPFREQ5_D_08	117067
#define	P20_SCL_MPFREQ5_D_09	117068
#define	P20_SCL_OBS1LIT_D_00	121001
#define	P20_SCL_OBS1LIT_D_01	121002
#define	P20_SCL_OBS1LIT_D_02	121003
#define	P20_SCL_OBS1LIT_D_03	121004
#define	P20_SCL_OBS1_D_00	121005
#define	P20_SCL_OBS1_D_01	121006
#define	P20_SCL_OBS1_D_02	121007
#define	P20_SCL_OBS1_D_03	121008
#define	P20_SCL_OBS2LIT_D_00	121009
#define	P20_SCL_OBS2LIT_D_01	121010
#define	P20_SCL_OBS2LIT_D_02	121011
#define	P20_SCL_OBS2LIT_D_03	121012
#define	P20_SCL_OBS2LIT_D_04	121013
#define	P20_SCL_OBS2LIT_D_05	121014
#define	P20_SCL_OBS2LIT_D_06	121015
#define	P20_SCL_OBS2LIT_D_07	121016
#define	P20_SCL_OBS2LIT_D_08	121017
#define	P20_SCL_OBS2LIT_D_09	121018
#define	P20_SCL_OBS2_D_00	121019
#define	P20_SCL_OBS2_D_01	121020
#define	P20_SCL_OBS2_D_02	121021
#define	P20_SCL_OBS2_D_03	121022
#define	P20_SCL_OBS2_D_04	121023
#define	P20_SCL_OBS2_D_05	121024
#define	P20_SCL_OBS2_D_06	121025
#define	P20_SCL_OBS2_D_07	121026
#define	P20_SCL_OBS2_D_08	121027
#define	P20_SCL_OBS2_D_09	121028
#define	P20_SCL_OBS3LIT_D_00	121029
#define	P20_SCL_OBS3LIT_D_01	121030
#define	P20_SCL_OBS3LIT_D_02	121031
#define	P20_SCL_OBS3LIT_D_03	121032
#define	P20_SCL_OBS3LIT_D_04	121033
#define	P20_SCL_OBS3LIT_D_05	121034
#define	P20_SCL_OBS3LIT_D_06	121035
#define	P20_SCL_OBS3LIT_D_07	121036
#define	P20_SCL_OBS3LIT_D_08	121037
#define	P20_SCL_OBS3LIT_D_09	121038
#define	P20_SCL_OBS3_D_00	121039
#define	P20_SCL_OBS3_D_01	121040
#define	P20_SCL_OBS3_D_02	121041
#define	P20_SCL_OBS3_D_03	121042
#define	P20_SCL_OBS3_D_04	121043
#define	P20_SCL_OBS3_D_05	121044
#define	P20_SCL_OBS3_D_06	121045
#define	P20_SCL_OBS3_D_07	121046
#define	P20_SCL_OBS3_D_08	121047
#define	P20_SCL_OBS3_D_09	121048
#define	P20_BCK_DME1INTL_D	125001
#define	P20_BCK_DME1_D	125002
#define	P20_BCK_DME2INTL_D	125003
#define	P20_BCK_DME2_D	125004
#define	P20_BCK_MPNAV1INTLLIT_D	125005
#define	P20_BCK_MPNAV1INTL_D	125006
#define	P20_BCK_MPNAV1LIT_D	125007
#define	P20_BCK_MPNAV1_D	125008
#define	P20_BCK_MPNAV2INTLLIT_D	125009
#define	P20_BCK_MPNAV2INTL_D	125010
#define	P20_BCK_MPNAV2LIT_D	125011
#define	P20_BCK_MPNAV2_D	125012
#define	P20_BCK_MPOBS1INTLLIT_D	125013
#define	P20_BCK_MPOBS1INTL_D	125014
#define	P20_BCK_MPOBS1LIT_D	125015
#define	P20_BCK_MPOBS1_D	125016
#define	P20_BCK_MPOBS2INTLLIT_D	125017
#define	P20_BCK_MPOBS2INTL_D	125018
#define	P20_BCK_MPOBS2LIT_D	125019
#define	P20_BCK_MPOBS2_D	125020
#define	P20_KNB_MPFREQ_D_00	129001
#define	P20_KNB_MPFREQ_D_01	129002
#define	P20_KNB_MPFREQ_D_02	129003
#define	P20_KNB_MPFREQ_D_03	129004
#define	P20_KNB_MPFREQ_D_04	129005
#define	P20_KNB_MPFREQ_D_05	129006
#define	P20_KNB_MPFREQ_D_06	129007
#define	P20_KNB_MPFREQ_D_07	129008
#define	P20_KNB_MPFREQ_D_08	129009
#define	P20_KNB_MPFREQ_D_09	129010

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
