/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  Module:  $RCSfile: $

  Last modification:
    Date:      $Date: 18.02.06 12:05 $
    Version:   $Revision: 1 $
    Author:    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#define MISC_NAV_SX				304
#define MISC_NAV_SY				600
#define MISC_NAV_SXSY				304,600
#define MISC_DOT_00_SX			18
#define MISC_DOT_00_SY			18
#define MISC_DOT_00_SXSY			18,18
#define MISC_DOT_01_SX			18
#define MISC_DOT_01_SY			18
#define MISC_DOT_01_SXSY			18,18

