/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../Version.h"

#include "p3_size.h"

#define	P3_BTN_APOFF_D_00	1001
#define	P3_BTN_APOFF_D_01	1002
#define	P3_BTN_APOFF_M_00	1003
#define	P3_BTN_APOFF_M_01	1004
#define	P3_BTN_APOFF_P_00	1005
#define	P3_BTN_APOFF_P_01	1006
#define	P3_BTN_APOFF_R_00	1007
#define	P3_BTN_APOFF_R_01	1008
#define	P3_BTN_COM_D_00	1009
#define	P3_BTN_COM_D_01	1010
#define	P3_BTN_COM_M_00	1011
#define	P3_BTN_COM_M_01	1012
#define	P3_BTN_COM_P_00	1013
#define	P3_BTN_COM_P_01	1014
#define	P3_BTN_COM_R_00	1015
#define	P3_BTN_COM_R_01	1016
#define	P3_BTN_TRIMEMERG_D_00	1017
#define	P3_BTN_TRIMEMERG_D_01	1018
#define	P3_BTN_TRIMEMERG_D_02	1019
#define	P3_BTN_TRIMEMERG_M_00	1020
#define	P3_BTN_TRIMEMERG_M_01	1021
#define	P3_BTN_TRIMEMERG_M_02	1022
#define	P3_BTN_TRIMEMERG_P_00	1023
#define	P3_BTN_TRIMEMERG_P_01	1024
#define	P3_BTN_TRIMEMERG_P_02	1025
#define	P3_BTN_TRIMEMERG_R_00	1026
#define	P3_BTN_TRIMEMERG_R_01	1027
#define	P3_BTN_TRIMEMERG_R_02	1028
#define	P3_BTN_TRIMMAIN_D_00	1029
#define	P3_BTN_TRIMMAIN_D_01	1030
#define	P3_BTN_TRIMMAIN_D_02	1031
#define	P3_BTN_TRIMMAIN_M_00	1032
#define	P3_BTN_TRIMMAIN_M_01	1033
#define	P3_BTN_TRIMMAIN_M_02	1034
#define	P3_BTN_TRIMMAIN_P_00	1035
#define	P3_BTN_TRIMMAIN_P_01	1036
#define	P3_BTN_TRIMMAIN_P_02	1037
#define	P3_BTN_TRIMMAIN_R_00	1038
#define	P3_BTN_TRIMMAIN_R_01	1039
#define	P3_BTN_TRIMMAIN_R_02	1040
#define	P3_BACKGROUND	5001
#define	P3_BACKGROUND_D	5002
#define	P3_BACKGROUND_M	5003
#define	P3_BACKGROUND_P	5004
#define	P3_BACKGROUND_R	5005
#define	MISC_CRS_P0_01	13001
#define	MISC_CRS_P0_02	13002
#define	MISC_CRS_P0_03	13003
#define	MISC_CRS_P0_04	13004
#define	MISC_CRS_P1_01	13005
#define	MISC_CRS_P1_02	13006
#define	MISC_CRS_P1_03	13007
#define	MISC_CRS_P1_04	13008
#define	MISC_CRS_P4_01	13009
#define	MISC_CRS_P4_02	13010
#define	MISC_CRS_P4_03	13011
#define	MISC_CRS_P5_01	13012
#define	MISC_CRS_P5_02	13013
#define	MISC_CRS_P5_03	13014
#define	MISC_CRS_P6_01	13015
#define	MISC_CRS_P7_01	13016
#define	MISC_YOKE	17001
#define	MISC_DUMMY	21001

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
