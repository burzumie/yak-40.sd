/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#define	MISC_CRS_P0_01_SX	81
#define	MISC_CRS_P0_01_SY	80
#define	MISC_CRS_P0_02_SX	103
#define	MISC_CRS_P0_02_SY	51
#define	MISC_CRS_P0_03_SX	103
#define	MISC_CRS_P0_03_SY	51
#define	MISC_CRS_P0_04_SX	54
#define	MISC_CRS_P0_04_SY	106
#define	MISC_CRS_P1_01_SX	81
#define	MISC_CRS_P1_01_SY	80
#define	MISC_CRS_P1_02_SX	103
#define	MISC_CRS_P1_02_SY	51
#define	MISC_CRS_P1_03_SX	103
#define	MISC_CRS_P1_03_SY	51
#define	MISC_CRS_P1_04_SX	54
#define	MISC_CRS_P1_04_SY	106
#define	MISC_CRS_P4_01_SX	81
#define	MISC_CRS_P4_01_SY	80
#define	MISC_CRS_P4_02_SX	105
#define	MISC_CRS_P4_02_SY	73
#define	MISC_CRS_P4_03_SX	81
#define	MISC_CRS_P4_03_SY	80
#define	MISC_CRS_P5_01_SX	81
#define	MISC_CRS_P5_01_SY	80
#define	MISC_CRS_P5_02_SX	105
#define	MISC_CRS_P5_02_SY	73
#define	MISC_CRS_P5_03_SX	81
#define	MISC_CRS_P5_03_SY	80
#define	MISC_CRS_P6_01_SX	81
#define	MISC_CRS_P6_01_SY	80
#define	MISC_CRS_P7_01_SX	81
#define	MISC_CRS_P7_01_SY	80
#define	MISC_YOKE_SX	100
#define	MISC_YOKE_SY	48
#define	MISC_DUMMY_SX	1
#define	MISC_DUMMY_SY	1
#define	P15_SWT_GMK1_D_00_SX	75
#define	P15_SWT_GMK1_D_00_SY	62
#define	P15_SWT_GMK1_D_01_SX	75
#define	P15_SWT_GMK1_D_01_SY	62
#define	P15_SWT_GMK1_P_00_SX	75
#define	P15_SWT_GMK1_P_00_SY	62
#define	P15_SWT_GMK1_P_01_SX	75
#define	P15_SWT_GMK1_P_01_SY	62
#define	P15_SWT_GMK2_D_00_SX	75
#define	P15_SWT_GMK2_D_00_SY	62
#define	P15_SWT_GMK2_D_01_SX	75
#define	P15_SWT_GMK2_D_01_SY	62
#define	P15_SWT_GMK2_P_00_SX	75
#define	P15_SWT_GMK2_P_00_SY	62
#define	P15_SWT_GMK2_P_01_SX	75
#define	P15_SWT_GMK2_P_01_SY	62
#define	P15_SWT_GMK3_D_00_SX	75
#define	P15_SWT_GMK3_D_00_SY	62
#define	P15_SWT_GMK3_D_01_SX	75
#define	P15_SWT_GMK3_D_01_SY	62
#define	P15_SWT_GMK3_P_00_SX	75
#define	P15_SWT_GMK3_P_00_SY	62
#define	P15_SWT_GMK3_P_01_SX	75
#define	P15_SWT_GMK3_P_01_SY	62
#define	P15_SWT_GMK4_D_00_SX	75
#define	P15_SWT_GMK4_D_00_SY	62
#define	P15_SWT_GMK4_D_01_SX	75
#define	P15_SWT_GMK4_D_01_SY	62
#define	P15_SWT_GMK4_D_02_SX	75
#define	P15_SWT_GMK4_D_02_SY	62
#define	P15_SWT_GMK4_P_00_SX	75
#define	P15_SWT_GMK4_P_00_SY	62
#define	P15_SWT_GMK4_P_01_SX	75
#define	P15_SWT_GMK4_P_01_SY	62
#define	P15_SWT_GMK4_P_02_SX	75
#define	P15_SWT_GMK4_P_02_SY	62
#define	P15_SWT_GMK5_D_00_SX	75
#define	P15_SWT_GMK5_D_00_SY	62
#define	P15_SWT_GMK5_D_01_SX	75
#define	P15_SWT_GMK5_D_01_SY	62
#define	P15_SWT_GMK5_D_02_SX	75
#define	P15_SWT_GMK5_D_02_SY	62
#define	P15_SWT_GMK5_P_00_SX	75
#define	P15_SWT_GMK5_P_00_SY	62
#define	P15_SWT_GMK5_P_01_SX	75
#define	P15_SWT_GMK5_P_01_SY	62
#define	P15_SWT_GMK5_P_02_SX	75
#define	P15_SWT_GMK5_P_02_SY	62
#define	P15_TBL_GMK1INTL_D_00_SX	61
#define	P15_TBL_GMK1INTL_D_00_SY	52
#define	P15_TBL_GMK1INTL_D_01_SX	61
#define	P15_TBL_GMK1INTL_D_01_SY	52
#define	P15_TBL_GMK1INTL_P_00_SX	61
#define	P15_TBL_GMK1INTL_P_00_SY	52
#define	P15_TBL_GMK1INTL_P_01_SX	61
#define	P15_TBL_GMK1INTL_P_01_SY	52
#define	P15_TBL_GMK1_D_00_SX	61
#define	P15_TBL_GMK1_D_00_SY	52
#define	P15_TBL_GMK1_D_01_SX	61
#define	P15_TBL_GMK1_D_01_SY	52
#define	P15_TBL_GMK1_P_00_SX	61
#define	P15_TBL_GMK1_P_00_SY	52
#define	P15_TBL_GMK1_P_01_SX	61
#define	P15_TBL_GMK1_P_01_SY	52
#define	P15_TBL_GMK2INTL_D_00_SX	62
#define	P15_TBL_GMK2INTL_D_00_SY	52
#define	P15_TBL_GMK2INTL_D_01_SX	62
#define	P15_TBL_GMK2INTL_D_01_SY	52
#define	P15_TBL_GMK2INTL_P_00_SX	62
#define	P15_TBL_GMK2INTL_P_00_SY	52
#define	P15_TBL_GMK2INTL_P_01_SX	62
#define	P15_TBL_GMK2INTL_P_01_SY	52
#define	P15_TBL_GMK2_D_00_SX	62
#define	P15_TBL_GMK2_D_00_SY	52
#define	P15_TBL_GMK2_D_01_SX	62
#define	P15_TBL_GMK2_D_01_SY	52
#define	P15_TBL_GMK2_P_00_SX	62
#define	P15_TBL_GMK2_P_00_SY	52
#define	P15_TBL_GMK2_P_01_SX	62
#define	P15_TBL_GMK2_P_01_SY	52
#define	P15_BACKGROUND_D_SX	415
#define	P15_BACKGROUND_D_SY	256
#define	P15_BACKGROUND_P_SX	415
#define	P15_BACKGROUND_P_SY	256
#define	P15_KNB_GMKLAT_D_00_SX	83
#define	P15_KNB_GMKLAT_D_00_SY	83
#define	P15_KNB_GMKLAT_D_01_SX	83
#define	P15_KNB_GMKLAT_D_01_SY	83
#define	P15_KNB_GMKLAT_D_02_SX	83
#define	P15_KNB_GMKLAT_D_02_SY	83
#define	P15_KNB_GMKLAT_D_03_SX	83
#define	P15_KNB_GMKLAT_D_03_SY	83
#define	P15_KNB_GMKLAT_D_04_SX	83
#define	P15_KNB_GMKLAT_D_04_SY	83
#define	P15_KNB_GMKLAT_D_05_SX	83
#define	P15_KNB_GMKLAT_D_05_SY	83
#define	P15_KNB_GMKLAT_D_06_SX	83
#define	P15_KNB_GMKLAT_D_06_SY	83
#define	P15_KNB_GMKLAT_D_07_SX	83
#define	P15_KNB_GMKLAT_D_07_SY	83
#define	P15_KNB_GMKLAT_D_08_SX	83
#define	P15_KNB_GMKLAT_D_08_SY	83
#define	P15_KNB_GMKLAT_D_09_SX	83
#define	P15_KNB_GMKLAT_D_09_SY	83
#define	P15_KNB_GMKLAT_D_10_SX	83
#define	P15_KNB_GMKLAT_D_10_SY	83
#define	P15_KNB_GMKLAT_D_11_SX	83
#define	P15_KNB_GMKLAT_D_11_SY	83
#define	P15_KNB_GMKLAT_D_12_SX	83
#define	P15_KNB_GMKLAT_D_12_SY	83
#define	P15_KNB_GMKLAT_D_13_SX	83
#define	P15_KNB_GMKLAT_D_13_SY	83
#define	P15_KNB_GMKLAT_D_14_SX	83
#define	P15_KNB_GMKLAT_D_14_SY	83
#define	P15_KNB_GMKLAT_D_15_SX	83
#define	P15_KNB_GMKLAT_D_15_SY	83
#define	P15_KNB_GMKLAT_D_16_SX	83
#define	P15_KNB_GMKLAT_D_16_SY	83
#define	P15_KNB_GMKLAT_D_17_SX	83
#define	P15_KNB_GMKLAT_D_17_SY	83
#define	P15_KNB_GMKLAT_D_18_SX	83
#define	P15_KNB_GMKLAT_D_18_SY	83
#define	P15_KNB_GMKLAT_D_19_SX	83
#define	P15_KNB_GMKLAT_D_19_SY	83
#define	P15_KNB_GMKLAT_D_20_SX	83
#define	P15_KNB_GMKLAT_D_20_SY	83
#define	P15_KNB_GMKLAT_D_21_SX	83
#define	P15_KNB_GMKLAT_D_21_SY	83
#define	P15_KNB_GMKLAT_D_22_SX	83
#define	P15_KNB_GMKLAT_D_22_SY	83
#define	P15_KNB_GMKLAT_D_23_SX	83
#define	P15_KNB_GMKLAT_D_23_SY	83
#define	P15_KNB_GMKLAT_D_24_SX	83
#define	P15_KNB_GMKLAT_D_24_SY	83
#define	P15_KNB_GMKLAT_D_25_SX	83
#define	P15_KNB_GMKLAT_D_25_SY	83
#define	P15_KNB_GMKLAT_D_26_SX	83
#define	P15_KNB_GMKLAT_D_26_SY	83
#define	P15_KNB_GMKLAT_D_27_SX	83
#define	P15_KNB_GMKLAT_D_27_SY	83
#define	P15_KNB_GMKLAT_D_28_SX	83
#define	P15_KNB_GMKLAT_D_28_SY	83
#define	P15_KNB_GMKLAT_D_29_SX	83
#define	P15_KNB_GMKLAT_D_29_SY	83
#define	P15_KNB_GMKLAT_D_30_SX	83
#define	P15_KNB_GMKLAT_D_30_SY	83
#define	P15_KNB_GMKLAT_D_31_SX	83
#define	P15_KNB_GMKLAT_D_31_SY	83
#define	P15_KNB_GMKLAT_D_32_SX	83
#define	P15_KNB_GMKLAT_D_32_SY	83
#define	P15_KNB_GMKLAT_D_33_SX	83
#define	P15_KNB_GMKLAT_D_33_SY	83
#define	P15_KNB_GMKLAT_D_34_SX	83
#define	P15_KNB_GMKLAT_D_34_SY	83
#define	P15_KNB_GMKLAT_D_35_SX	83
#define	P15_KNB_GMKLAT_D_35_SY	83
#define	P15_KNB_GMKLAT_D_36_SX	83
#define	P15_KNB_GMKLAT_D_36_SY	83
#define	P15_KNB_GMKLAT_D_37_SX	83
#define	P15_KNB_GMKLAT_D_37_SY	83
#define	P15_KNB_GMKLAT_D_38_SX	83
#define	P15_KNB_GMKLAT_D_38_SY	83
#define	P15_KNB_GMKLAT_D_39_SX	83
#define	P15_KNB_GMKLAT_D_39_SY	83
#define	P15_KNB_GMKLAT_D_40_SX	83
#define	P15_KNB_GMKLAT_D_40_SY	83
#define	P15_KNB_GMKLAT_D_41_SX	83
#define	P15_KNB_GMKLAT_D_41_SY	83
#define	P15_KNB_GMKLAT_D_42_SX	83
#define	P15_KNB_GMKLAT_D_42_SY	83
#define	P15_KNB_GMKLAT_D_43_SX	83
#define	P15_KNB_GMKLAT_D_43_SY	83
#define	P15_KNB_GMKLAT_D_44_SX	83
#define	P15_KNB_GMKLAT_D_44_SY	83
#define	P15_KNB_GMKLAT_D_45_SX	83
#define	P15_KNB_GMKLAT_D_45_SY	83
#define	P15_KNB_GMKLAT_D_46_SX	83
#define	P15_KNB_GMKLAT_D_46_SY	83
#define	P15_KNB_GMKLAT_D_47_SX	83
#define	P15_KNB_GMKLAT_D_47_SY	83
#define	P15_KNB_GMKLAT_D_48_SX	83
#define	P15_KNB_GMKLAT_D_48_SY	83
#define	P15_KNB_GMKLAT_D_49_SX	83
#define	P15_KNB_GMKLAT_D_49_SY	83
#define	P15_KNB_GMKLAT_D_50_SX	83
#define	P15_KNB_GMKLAT_D_50_SY	83
#define	P15_KNB_GMKLAT_D_51_SX	83
#define	P15_KNB_GMKLAT_D_51_SY	83
#define	P15_KNB_GMKLAT_D_52_SX	83
#define	P15_KNB_GMKLAT_D_52_SY	83
#define	P15_KNB_GMKLAT_D_53_SX	83
#define	P15_KNB_GMKLAT_D_53_SY	83
#define	P15_KNB_GMKLAT_D_54_SX	83
#define	P15_KNB_GMKLAT_D_54_SY	83
#define	P15_KNB_GMKLAT_D_55_SX	83
#define	P15_KNB_GMKLAT_D_55_SY	83
#define	P15_KNB_GMKLAT_D_56_SX	83
#define	P15_KNB_GMKLAT_D_56_SY	83
#define	P15_KNB_GMKLAT_D_57_SX	83
#define	P15_KNB_GMKLAT_D_57_SY	83
#define	P15_KNB_GMKLAT_D_58_SX	83
#define	P15_KNB_GMKLAT_D_58_SY	83
#define	P15_KNB_GMKLAT_D_59_SX	83
#define	P15_KNB_GMKLAT_D_59_SY	83
#define	P15_KNB_GMKLAT_D_60_SX	83
#define	P15_KNB_GMKLAT_D_60_SY	83
#define	P15_KNB_GMKLAT_D_61_SX	83
#define	P15_KNB_GMKLAT_D_61_SY	83
#define	P15_KNB_GMKLAT_D_62_SX	83
#define	P15_KNB_GMKLAT_D_62_SY	83
#define	P15_KNB_GMKLAT_D_63_SX	83
#define	P15_KNB_GMKLAT_D_63_SY	83
#define	P15_KNB_GMKLAT_D_64_SX	83
#define	P15_KNB_GMKLAT_D_64_SY	83
#define	P15_KNB_GMKLAT_D_65_SX	83
#define	P15_KNB_GMKLAT_D_65_SY	83
#define	P15_KNB_GMKLAT_D_66_SX	83
#define	P15_KNB_GMKLAT_D_66_SY	83
#define	P15_KNB_GMKLAT_D_67_SX	83
#define	P15_KNB_GMKLAT_D_67_SY	83
#define	P15_KNB_GMKLAT_D_68_SX	83
#define	P15_KNB_GMKLAT_D_68_SY	83
#define	P15_KNB_GMKLAT_D_69_SX	83
#define	P15_KNB_GMKLAT_D_69_SY	83
#define	P15_KNB_GMKLAT_D_70_SX	83
#define	P15_KNB_GMKLAT_D_70_SY	83
#define	P15_KNB_GMKLAT_D_71_SX	83
#define	P15_KNB_GMKLAT_D_71_SY	83
#define	P15_KNB_GMKLAT_D_72_SX	83
#define	P15_KNB_GMKLAT_D_72_SY	83
#define	P15_KNB_GMKLAT_D_73_SX	83
#define	P15_KNB_GMKLAT_D_73_SY	83
#define	P15_KNB_GMKLAT_D_74_SX	83
#define	P15_KNB_GMKLAT_D_74_SY	83
#define	P15_KNB_GMKLAT_D_75_SX	83
#define	P15_KNB_GMKLAT_D_75_SY	83
#define	P15_KNB_GMKLAT_D_76_SX	83
#define	P15_KNB_GMKLAT_D_76_SY	83
#define	P15_KNB_GMKLAT_D_77_SX	83
#define	P15_KNB_GMKLAT_D_77_SY	83
#define	P15_KNB_GMKLAT_D_78_SX	83
#define	P15_KNB_GMKLAT_D_78_SY	83
#define	P15_KNB_GMKLAT_D_79_SX	83
#define	P15_KNB_GMKLAT_D_79_SY	83
#define	P15_KNB_GMKLAT_D_80_SX	83
#define	P15_KNB_GMKLAT_D_80_SY	83
#define	P15_KNB_GMKLAT_D_81_SX	83
#define	P15_KNB_GMKLAT_D_81_SY	83
#define	P15_KNB_GMKLAT_D_82_SX	83
#define	P15_KNB_GMKLAT_D_82_SY	83
#define	P15_KNB_GMKLAT_D_83_SX	83
#define	P15_KNB_GMKLAT_D_83_SY	83
#define	P15_KNB_GMKLAT_D_84_SX	83
#define	P15_KNB_GMKLAT_D_84_SY	83
#define	P15_KNB_GMKLAT_D_85_SX	83
#define	P15_KNB_GMKLAT_D_85_SY	83
#define	P15_KNB_GMKLAT_D_86_SX	83
#define	P15_KNB_GMKLAT_D_86_SY	83
#define	P15_KNB_GMKLAT_D_87_SX	83
#define	P15_KNB_GMKLAT_D_87_SY	83
#define	P15_KNB_GMKLAT_D_88_SX	83
#define	P15_KNB_GMKLAT_D_88_SY	83
#define	P15_KNB_GMKLAT_D_89_SX	83
#define	P15_KNB_GMKLAT_D_89_SY	83
#define	P15_KNB_GMKLAT_P_00_SX	83
#define	P15_KNB_GMKLAT_P_00_SY	83
#define	P15_KNB_GMKLAT_P_01_SX	83
#define	P15_KNB_GMKLAT_P_01_SY	83
#define	P15_KNB_GMKLAT_P_02_SX	83
#define	P15_KNB_GMKLAT_P_02_SY	83
#define	P15_KNB_GMKLAT_P_03_SX	83
#define	P15_KNB_GMKLAT_P_03_SY	83
#define	P15_KNB_GMKLAT_P_04_SX	83
#define	P15_KNB_GMKLAT_P_04_SY	83
#define	P15_KNB_GMKLAT_P_05_SX	83
#define	P15_KNB_GMKLAT_P_05_SY	83
#define	P15_KNB_GMKLAT_P_06_SX	83
#define	P15_KNB_GMKLAT_P_06_SY	83
#define	P15_KNB_GMKLAT_P_07_SX	83
#define	P15_KNB_GMKLAT_P_07_SY	83
#define	P15_KNB_GMKLAT_P_08_SX	83
#define	P15_KNB_GMKLAT_P_08_SY	83
#define	P15_KNB_GMKLAT_P_09_SX	83
#define	P15_KNB_GMKLAT_P_09_SY	83
#define	P15_KNB_GMKLAT_P_10_SX	83
#define	P15_KNB_GMKLAT_P_10_SY	83
#define	P15_KNB_GMKLAT_P_11_SX	83
#define	P15_KNB_GMKLAT_P_11_SY	83
#define	P15_KNB_GMKLAT_P_12_SX	83
#define	P15_KNB_GMKLAT_P_12_SY	83
#define	P15_KNB_GMKLAT_P_13_SX	83
#define	P15_KNB_GMKLAT_P_13_SY	83
#define	P15_KNB_GMKLAT_P_14_SX	83
#define	P15_KNB_GMKLAT_P_14_SY	83
#define	P15_KNB_GMKLAT_P_15_SX	83
#define	P15_KNB_GMKLAT_P_15_SY	83
#define	P15_KNB_GMKLAT_P_16_SX	83
#define	P15_KNB_GMKLAT_P_16_SY	83
#define	P15_KNB_GMKLAT_P_17_SX	83
#define	P15_KNB_GMKLAT_P_17_SY	83
#define	P15_KNB_GMKLAT_P_18_SX	83
#define	P15_KNB_GMKLAT_P_18_SY	83
#define	P15_KNB_GMKLAT_P_19_SX	83
#define	P15_KNB_GMKLAT_P_19_SY	83
#define	P15_KNB_GMKLAT_P_20_SX	83
#define	P15_KNB_GMKLAT_P_20_SY	83
#define	P15_KNB_GMKLAT_P_21_SX	83
#define	P15_KNB_GMKLAT_P_21_SY	83
#define	P15_KNB_GMKLAT_P_22_SX	83
#define	P15_KNB_GMKLAT_P_22_SY	83
#define	P15_KNB_GMKLAT_P_23_SX	83
#define	P15_KNB_GMKLAT_P_23_SY	83
#define	P15_KNB_GMKLAT_P_24_SX	83
#define	P15_KNB_GMKLAT_P_24_SY	83
#define	P15_KNB_GMKLAT_P_25_SX	83
#define	P15_KNB_GMKLAT_P_25_SY	83
#define	P15_KNB_GMKLAT_P_26_SX	83
#define	P15_KNB_GMKLAT_P_26_SY	83
#define	P15_KNB_GMKLAT_P_27_SX	83
#define	P15_KNB_GMKLAT_P_27_SY	83
#define	P15_KNB_GMKLAT_P_28_SX	83
#define	P15_KNB_GMKLAT_P_28_SY	83
#define	P15_KNB_GMKLAT_P_29_SX	83
#define	P15_KNB_GMKLAT_P_29_SY	83
#define	P15_KNB_GMKLAT_P_30_SX	83
#define	P15_KNB_GMKLAT_P_30_SY	83
#define	P15_KNB_GMKLAT_P_31_SX	83
#define	P15_KNB_GMKLAT_P_31_SY	83
#define	P15_KNB_GMKLAT_P_32_SX	83
#define	P15_KNB_GMKLAT_P_32_SY	83
#define	P15_KNB_GMKLAT_P_33_SX	83
#define	P15_KNB_GMKLAT_P_33_SY	83
#define	P15_KNB_GMKLAT_P_34_SX	83
#define	P15_KNB_GMKLAT_P_34_SY	83
#define	P15_KNB_GMKLAT_P_35_SX	83
#define	P15_KNB_GMKLAT_P_35_SY	83
#define	P15_KNB_GMKLAT_P_36_SX	83
#define	P15_KNB_GMKLAT_P_36_SY	83
#define	P15_KNB_GMKLAT_P_37_SX	83
#define	P15_KNB_GMKLAT_P_37_SY	83
#define	P15_KNB_GMKLAT_P_38_SX	83
#define	P15_KNB_GMKLAT_P_38_SY	83
#define	P15_KNB_GMKLAT_P_39_SX	83
#define	P15_KNB_GMKLAT_P_39_SY	83
#define	P15_KNB_GMKLAT_P_40_SX	83
#define	P15_KNB_GMKLAT_P_40_SY	83
#define	P15_KNB_GMKLAT_P_41_SX	83
#define	P15_KNB_GMKLAT_P_41_SY	83
#define	P15_KNB_GMKLAT_P_42_SX	83
#define	P15_KNB_GMKLAT_P_42_SY	83
#define	P15_KNB_GMKLAT_P_43_SX	83
#define	P15_KNB_GMKLAT_P_43_SY	83
#define	P15_KNB_GMKLAT_P_44_SX	83
#define	P15_KNB_GMKLAT_P_44_SY	83
#define	P15_KNB_GMKLAT_P_45_SX	83
#define	P15_KNB_GMKLAT_P_45_SY	83
#define	P15_KNB_GMKLAT_P_46_SX	83
#define	P15_KNB_GMKLAT_P_46_SY	83
#define	P15_KNB_GMKLAT_P_47_SX	83
#define	P15_KNB_GMKLAT_P_47_SY	83
#define	P15_KNB_GMKLAT_P_48_SX	83
#define	P15_KNB_GMKLAT_P_48_SY	83
#define	P15_KNB_GMKLAT_P_49_SX	83
#define	P15_KNB_GMKLAT_P_49_SY	83
#define	P15_KNB_GMKLAT_P_50_SX	83
#define	P15_KNB_GMKLAT_P_50_SY	83
#define	P15_KNB_GMKLAT_P_51_SX	83
#define	P15_KNB_GMKLAT_P_51_SY	83
#define	P15_KNB_GMKLAT_P_52_SX	83
#define	P15_KNB_GMKLAT_P_52_SY	83
#define	P15_KNB_GMKLAT_P_53_SX	83
#define	P15_KNB_GMKLAT_P_53_SY	83
#define	P15_KNB_GMKLAT_P_54_SX	83
#define	P15_KNB_GMKLAT_P_54_SY	83
#define	P15_KNB_GMKLAT_P_55_SX	83
#define	P15_KNB_GMKLAT_P_55_SY	83
#define	P15_KNB_GMKLAT_P_56_SX	83
#define	P15_KNB_GMKLAT_P_56_SY	83
#define	P15_KNB_GMKLAT_P_57_SX	83
#define	P15_KNB_GMKLAT_P_57_SY	83
#define	P15_KNB_GMKLAT_P_58_SX	83
#define	P15_KNB_GMKLAT_P_58_SY	83
#define	P15_KNB_GMKLAT_P_59_SX	83
#define	P15_KNB_GMKLAT_P_59_SY	83
#define	P15_KNB_GMKLAT_P_60_SX	83
#define	P15_KNB_GMKLAT_P_60_SY	83
#define	P15_KNB_GMKLAT_P_61_SX	83
#define	P15_KNB_GMKLAT_P_61_SY	83
#define	P15_KNB_GMKLAT_P_62_SX	83
#define	P15_KNB_GMKLAT_P_62_SY	83
#define	P15_KNB_GMKLAT_P_63_SX	83
#define	P15_KNB_GMKLAT_P_63_SY	83
#define	P15_KNB_GMKLAT_P_64_SX	83
#define	P15_KNB_GMKLAT_P_64_SY	83
#define	P15_KNB_GMKLAT_P_65_SX	83
#define	P15_KNB_GMKLAT_P_65_SY	83
#define	P15_KNB_GMKLAT_P_66_SX	83
#define	P15_KNB_GMKLAT_P_66_SY	83
#define	P15_KNB_GMKLAT_P_67_SX	83
#define	P15_KNB_GMKLAT_P_67_SY	83
#define	P15_KNB_GMKLAT_P_68_SX	83
#define	P15_KNB_GMKLAT_P_68_SY	83
#define	P15_KNB_GMKLAT_P_69_SX	83
#define	P15_KNB_GMKLAT_P_69_SY	83
#define	P15_KNB_GMKLAT_P_70_SX	83
#define	P15_KNB_GMKLAT_P_70_SY	83
#define	P15_KNB_GMKLAT_P_71_SX	83
#define	P15_KNB_GMKLAT_P_71_SY	83
#define	P15_KNB_GMKLAT_P_72_SX	83
#define	P15_KNB_GMKLAT_P_72_SY	83
#define	P15_KNB_GMKLAT_P_73_SX	83
#define	P15_KNB_GMKLAT_P_73_SY	83
#define	P15_KNB_GMKLAT_P_74_SX	83
#define	P15_KNB_GMKLAT_P_74_SY	83
#define	P15_KNB_GMKLAT_P_75_SX	83
#define	P15_KNB_GMKLAT_P_75_SY	83
#define	P15_KNB_GMKLAT_P_76_SX	83
#define	P15_KNB_GMKLAT_P_76_SY	83
#define	P15_KNB_GMKLAT_P_77_SX	83
#define	P15_KNB_GMKLAT_P_77_SY	83
#define	P15_KNB_GMKLAT_P_78_SX	83
#define	P15_KNB_GMKLAT_P_78_SY	83
#define	P15_KNB_GMKLAT_P_79_SX	83
#define	P15_KNB_GMKLAT_P_79_SY	83
#define	P15_KNB_GMKLAT_P_80_SX	83
#define	P15_KNB_GMKLAT_P_80_SY	83
#define	P15_KNB_GMKLAT_P_81_SX	83
#define	P15_KNB_GMKLAT_P_81_SY	83
#define	P15_KNB_GMKLAT_P_82_SX	83
#define	P15_KNB_GMKLAT_P_82_SY	83
#define	P15_KNB_GMKLAT_P_83_SX	83
#define	P15_KNB_GMKLAT_P_83_SY	83
#define	P15_KNB_GMKLAT_P_84_SX	83
#define	P15_KNB_GMKLAT_P_84_SY	83
#define	P15_KNB_GMKLAT_P_85_SX	83
#define	P15_KNB_GMKLAT_P_85_SY	83
#define	P15_KNB_GMKLAT_P_86_SX	83
#define	P15_KNB_GMKLAT_P_86_SY	83
#define	P15_KNB_GMKLAT_P_87_SX	83
#define	P15_KNB_GMKLAT_P_87_SY	83
#define	P15_KNB_GMKLAT_P_88_SX	83
#define	P15_KNB_GMKLAT_P_88_SY	83
#define	P15_KNB_GMKLAT_P_89_SX	83
#define	P15_KNB_GMKLAT_P_89_SY	83
#define	P15_SCL_GMK_D_00_SX	140
#define	P15_SCL_GMK_D_00_SY	140
#define	P15_SCL_GMK_D_01_SX	140
#define	P15_SCL_GMK_D_01_SY	140
#define	P15_SCL_GMK_P_00_SX	140
#define	P15_SCL_GMK_P_00_SY	140
#define	P15_SCL_GMK_P_01_SX	140
#define	P15_SCL_GMK_P_01_SY	140
#define	P15_PRI266INTL_D_SX	415
#define	P15_PRI266INTL_D_SY	256
#define	P15_PRI266INTL_P_SX	415
#define	P15_PRI266INTL_P_SY	256
#define	P15_PRI266_D_SX	415
#define	P15_PRI266_D_SY	256
#define	P15_PRI266_P_SX	415
#define	P15_PRI266_P_SY	256
#define	P15_PRI66INTL_D_SX	415
#define	P15_PRI66INTL_D_SY	256
#define	P15_PRI66INTL_P_SX	415
#define	P15_PRI66INTL_P_SY	256
#define	P15_PRI66_D_SX	415
#define	P15_PRI66_D_SY	256
#define	P15_PRI66_P_SX	415
#define	P15_PRI66_P_SY	256
