/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "p9_sizeX.h"

#define	P9_AZS_911_D_00	1001
#define	P9_AZS_911_D_01	1002
#define	P9_AZS_911_M_00	1003
#define	P9_AZS_911_M_01	1004
#define	P9_AZS_911_P_00	1005
#define	P9_AZS_911_P_01	1006
#define	P9_AZS_911_R_00	1007
#define	P9_AZS_911_R_01	1008
#define	P9_AZS_912_D_00	1009
#define	P9_AZS_912_D_01	1010
#define	P9_AZS_912_M_00	1011
#define	P9_AZS_912_M_01	1012
#define	P9_AZS_912_P_00	1013
#define	P9_AZS_912_P_01	1014
#define	P9_AZS_912_R_00	1015
#define	P9_AZS_912_R_01	1016
#define	P9_AZS_913_D_00	1017
#define	P9_AZS_913_D_01	1018
#define	P9_AZS_913_M_00	1019
#define	P9_AZS_913_M_01	1020
#define	P9_AZS_913_P_00	1021
#define	P9_AZS_913_P_01	1022
#define	P9_AZS_913_R_00	1023
#define	P9_AZS_913_R_01	1024
#define	P9_AZS_914_D_00	1025
#define	P9_AZS_914_D_01	1026
#define	P9_AZS_914_M_00	1027
#define	P9_AZS_914_M_01	1028
#define	P9_AZS_914_P_00	1029
#define	P9_AZS_914_P_01	1030
#define	P9_AZS_914_R_00	1031
#define	P9_AZS_914_R_01	1032
#define	P9_AZS_915_D_00	1033
#define	P9_AZS_915_D_01	1034
#define	P9_AZS_915_M_00	1035
#define	P9_AZS_915_M_01	1036
#define	P9_AZS_915_P_00	1037
#define	P9_AZS_915_P_01	1038
#define	P9_AZS_915_R_00	1039
#define	P9_AZS_915_R_01	1040
#define	P9_AZS_916_D_00	1041
#define	P9_AZS_916_D_01	1042
#define	P9_AZS_916_M_00	1043
#define	P9_AZS_916_M_01	1044
#define	P9_AZS_916_P_00	1045
#define	P9_AZS_916_P_01	1046
#define	P9_AZS_916_R_00	1047
#define	P9_AZS_916_R_01	1048
#define	P9_AZS_917_D_00	1049
#define	P9_AZS_917_D_01	1050
#define	P9_AZS_917_M_00	1051
#define	P9_AZS_917_M_01	1052
#define	P9_AZS_917_P_00	1053
#define	P9_AZS_917_P_01	1054
#define	P9_AZS_917_R_00	1055
#define	P9_AZS_917_R_01	1056
#define	P9_AZS_918_D_00	1057
#define	P9_AZS_918_D_01	1058
#define	P9_AZS_918_M_00	1059
#define	P9_AZS_918_M_01	1060
#define	P9_AZS_918_P_00	1061
#define	P9_AZS_918_P_01	1062
#define	P9_AZS_918_R_00	1063
#define	P9_AZS_918_R_01	1064
#define	P9_AZS_919_D_00	1065
#define	P9_AZS_919_D_01	1066
#define	P9_AZS_919_M_00	1067
#define	P9_AZS_919_M_01	1068
#define	P9_AZS_919_P_00	1069
#define	P9_AZS_919_P_01	1070
#define	P9_AZS_919_R_00	1071
#define	P9_AZS_919_R_01	1072
#define	P9_AZS_921_D_00	1073
#define	P9_AZS_921_D_01	1074
#define	P9_AZS_921_M_00	1075
#define	P9_AZS_921_M_01	1076
#define	P9_AZS_921_P_00	1077
#define	P9_AZS_921_P_01	1078
#define	P9_AZS_921_R_00	1079
#define	P9_AZS_921_R_01	1080
#define	P9_AZS_922_D_00	1081
#define	P9_AZS_922_D_01	1082
#define	P9_AZS_922_M_00	1083
#define	P9_AZS_922_M_01	1084
#define	P9_AZS_922_P_00	1085
#define	P9_AZS_922_P_01	1086
#define	P9_AZS_922_R_00	1087
#define	P9_AZS_922_R_01	1088
#define	P9_AZS_923_D_00	1089
#define	P9_AZS_923_D_01	1090
#define	P9_AZS_923_M_00	1091
#define	P9_AZS_923_M_01	1092
#define	P9_AZS_923_P_00	1093
#define	P9_AZS_923_P_01	1094
#define	P9_AZS_923_R_00	1095
#define	P9_AZS_923_R_01	1096
#define	P9_AZS_924_D_00	1097
#define	P9_AZS_924_D_01	1098
#define	P9_AZS_924_M_00	1099
#define	P9_AZS_924_M_01	1100
#define	P9_AZS_924_P_00	1101
#define	P9_AZS_924_P_01	1102
#define	P9_AZS_924_R_00	1103
#define	P9_AZS_924_R_01	1104
#define	P9_AZS_925_D_00	1105
#define	P9_AZS_925_D_01	1106
#define	P9_AZS_925_M_00	1107
#define	P9_AZS_925_M_01	1108
#define	P9_AZS_925_P_00	1109
#define	P9_AZS_925_P_01	1110
#define	P9_AZS_925_R_00	1111
#define	P9_AZS_925_R_01	1112
#define	P9_AZS_926_D_00	1113
#define	P9_AZS_926_D_01	1114
#define	P9_AZS_926_M_00	1115
#define	P9_AZS_926_M_01	1116
#define	P9_AZS_926_P_00	1117
#define	P9_AZS_926_P_01	1118
#define	P9_AZS_926_R_00	1119
#define	P9_AZS_926_R_01	1120
#define	P9_AZS_927_D_00	1121
#define	P9_AZS_927_D_01	1122
#define	P9_AZS_927_M_00	1123
#define	P9_AZS_927_M_01	1124
#define	P9_AZS_927_P_00	1125
#define	P9_AZS_927_P_01	1126
#define	P9_AZS_927_R_00	1127
#define	P9_AZS_927_R_01	1128
#define	P9_AZS_928_D_00	1129
#define	P9_AZS_928_D_01	1130
#define	P9_AZS_928_M_00	1131
#define	P9_AZS_928_M_01	1132
#define	P9_AZS_928_P_00	1133
#define	P9_AZS_928_P_01	1134
#define	P9_AZS_928_R_00	1135
#define	P9_AZS_928_R_01	1136
#define	P9_AZS_929_D_00	1137
#define	P9_AZS_929_D_01	1138
#define	P9_AZS_929_M_00	1139
#define	P9_AZS_929_M_01	1140
#define	P9_AZS_929_P_00	1141
#define	P9_AZS_929_P_01	1142
#define	P9_AZS_929_R_00	1143
#define	P9_AZS_929_R_01	1144
#define	P9_AZS_931_D_00	1145
#define	P9_AZS_931_D_01	1146
#define	P9_AZS_931_M_00	1147
#define	P9_AZS_931_M_01	1148
#define	P9_AZS_931_P_00	1149
#define	P9_AZS_931_P_01	1150
#define	P9_AZS_931_R_00	1151
#define	P9_AZS_931_R_01	1152
#define	P9_AZS_932_D_00	1153
#define	P9_AZS_932_D_01	1154
#define	P9_AZS_932_M_00	1155
#define	P9_AZS_932_M_01	1156
#define	P9_AZS_932_P_00	1157
#define	P9_AZS_932_P_01	1158
#define	P9_AZS_932_R_00	1159
#define	P9_AZS_932_R_01	1160
#define	P9_AZS_933_D_00	1161
#define	P9_AZS_933_D_01	1162
#define	P9_AZS_933_M_00	1163
#define	P9_AZS_933_M_01	1164
#define	P9_AZS_933_P_00	1165
#define	P9_AZS_933_P_01	1166
#define	P9_AZS_933_R_00	1167
#define	P9_AZS_933_R_01	1168
#define	P9_AZS_934_D_00	1169
#define	P9_AZS_934_D_01	1170
#define	P9_AZS_934_M_00	1171
#define	P9_AZS_934_M_01	1172
#define	P9_AZS_934_P_00	1173
#define	P9_AZS_934_P_01	1174
#define	P9_AZS_934_R_00	1175
#define	P9_AZS_934_R_01	1176
#define	P9_AZS_935_D_00	1177
#define	P9_AZS_935_D_01	1178
#define	P9_AZS_935_M_00	1179
#define	P9_AZS_935_M_01	1180
#define	P9_AZS_935_P_00	1181
#define	P9_AZS_935_P_01	1182
#define	P9_AZS_935_R_00	1183
#define	P9_AZS_935_R_01	1184
#define	P9_AZS_936_D_00	1185
#define	P9_AZS_936_D_01	1186
#define	P9_AZS_936_M_00	1187
#define	P9_AZS_936_M_01	1188
#define	P9_AZS_936_P_00	1189
#define	P9_AZS_936_P_01	1190
#define	P9_AZS_936_R_00	1191
#define	P9_AZS_936_R_01	1192
#define	P9_AZS_937_D_00	1193
#define	P9_AZS_937_D_01	1194
#define	P9_AZS_937_M_00	1195
#define	P9_AZS_937_M_01	1196
#define	P9_AZS_937_P_00	1197
#define	P9_AZS_937_P_01	1198
#define	P9_AZS_937_R_00	1199
#define	P9_AZS_937_R_01	1200
#define	P9_AZS_938_D_00	1201
#define	P9_AZS_938_D_01	1202
#define	P9_AZS_938_M_00	1203
#define	P9_AZS_938_M_01	1204
#define	P9_AZS_938_P_00	1205
#define	P9_AZS_938_P_01	1206
#define	P9_AZS_938_R_00	1207
#define	P9_AZS_938_R_01	1208
#define	P9_AZS_939_D_00	1209
#define	P9_AZS_939_D_01	1210
#define	P9_AZS_939_M_00	1211
#define	P9_AZS_939_M_01	1212
#define	P9_AZS_939_P_00	1213
#define	P9_AZS_939_P_01	1214
#define	P9_AZS_939_R_00	1215
#define	P9_AZS_939_R_01	1216
#define	P9_AZS_941_D_00	1217
#define	P9_AZS_941_D_01	1218
#define	P9_AZS_941_M_00	1219
#define	P9_AZS_941_M_01	1220
#define	P9_AZS_941_P_00	1221
#define	P9_AZS_941_P_01	1222
#define	P9_AZS_941_R_00	1223
#define	P9_AZS_941_R_01	1224
#define	P9_AZS_942_D_00	1225
#define	P9_AZS_942_D_01	1226
#define	P9_AZS_942_M_00	1227
#define	P9_AZS_942_M_01	1228
#define	P9_AZS_942_P_00	1229
#define	P9_AZS_942_P_01	1230
#define	P9_AZS_942_R_00	1231
#define	P9_AZS_942_R_01	1232
#define	P9_AZS_943_D_00	1233
#define	P9_AZS_943_D_01	1234
#define	P9_AZS_943_M_00	1235
#define	P9_AZS_943_M_01	1236
#define	P9_AZS_943_P_00	1237
#define	P9_AZS_943_P_01	1238
#define	P9_AZS_943_R_00	1239
#define	P9_AZS_943_R_01	1240
#define	P9_AZS_944_D_00	1241
#define	P9_AZS_944_D_01	1242
#define	P9_AZS_944_M_00	1243
#define	P9_AZS_944_M_01	1244
#define	P9_AZS_944_P_00	1245
#define	P9_AZS_944_P_01	1246
#define	P9_AZS_944_R_00	1247
#define	P9_AZS_944_R_01	1248
#define	P9_AZS_945_D_00	1249
#define	P9_AZS_945_D_01	1250
#define	P9_AZS_945_M_00	1251
#define	P9_AZS_945_M_01	1252
#define	P9_AZS_945_P_00	1253
#define	P9_AZS_945_P_01	1254
#define	P9_AZS_945_R_00	1255
#define	P9_AZS_945_R_01	1256
#define	P9_AZS_946_D_00	1257
#define	P9_AZS_946_D_01	1258
#define	P9_AZS_946_M_00	1259
#define	P9_AZS_946_M_01	1260
#define	P9_AZS_946_P_00	1261
#define	P9_AZS_946_P_01	1262
#define	P9_AZS_946_R_00	1263
#define	P9_AZS_946_R_01	1264
#define	P9_AZS_947_D_00	1265
#define	P9_AZS_947_D_01	1266
#define	P9_AZS_947_M_00	1267
#define	P9_AZS_947_M_01	1268
#define	P9_AZS_947_P_00	1269
#define	P9_AZS_947_P_01	1270
#define	P9_AZS_947_R_00	1271
#define	P9_AZS_947_R_01	1272
#define	P9_AZS_948_D_00	1273
#define	P9_AZS_948_D_01	1274
#define	P9_AZS_948_M_00	1275
#define	P9_AZS_948_M_01	1276
#define	P9_AZS_948_P_00	1277
#define	P9_AZS_948_P_01	1278
#define	P9_AZS_948_R_00	1279
#define	P9_AZS_948_R_01	1280
#define	P9_AZS_949_D_00	1281
#define	P9_AZS_949_D_01	1282
#define	P9_AZS_949_M_00	1283
#define	P9_AZS_949_M_01	1284
#define	P9_AZS_949_P_00	1285
#define	P9_AZS_949_P_01	1286
#define	P9_AZS_949_R_00	1287
#define	P9_AZS_949_R_01	1288
#define	P9_BACKGROUND_D	5001
#define	P9_BACKGROUND_M	5002
#define	P9_BACKGROUND_P	5003
#define	P9_BACKGROUND_R	5004
#define	MISC_CRS_P0_01	13001
#define	MISC_CRS_P0_02	13002
#define	MISC_CRS_P0_03	13003
#define	MISC_CRS_P0_04	13004
#define	MISC_CRS_P1_01	13005
#define	MISC_CRS_P1_02	13006
#define	MISC_CRS_P1_03	13007
#define	MISC_CRS_P1_04	13008
#define	MISC_CRS_P4_01	13009
#define	MISC_CRS_P4_02	13010
#define	MISC_CRS_P4_03	13011
#define	MISC_CRS_P5_01	13012
#define	MISC_CRS_P5_02	13013
#define	MISC_CRS_P5_03	13014
#define	MISC_CRS_P6_01	13015
#define	MISC_CRS_P7_01	13016
#define	MISC_YOKE	17001
#define	MISC_DUMMY	21001

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
