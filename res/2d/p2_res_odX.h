/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "p2_size_odX.h"

#define	P2_BTN_APOFF_D_00	1001
#define	P2_BTN_APOFF_D_01	1002
#define	P2_BTN_COM_D_00	1003
#define	P2_BTN_COM_D_01	1004
#define	P2_BTN_TRIMEMERG_D_00	1005
#define	P2_BTN_TRIMEMERG_D_01	1006
#define	P2_BTN_TRIMEMERG_D_02	1007
#define	P2_BTN_TRIMMAIN_D_00	1008
#define	P2_BTN_TRIMMAIN_D_01	1009
#define	P2_BTN_TRIMMAIN_D_02	1010
#define	P2_BACKGROUND	5001
#define	P2_BACKGROUND_D	5003
#define	MISC_CRS_P0_01	13001
#define	MISC_CRS_P0_02	13003
#define	MISC_CRS_P0_03	13005
#define	MISC_CRS_P0_04	13007
#define	MISC_CRS_P1_01	13009
#define	MISC_CRS_P1_02	13011
#define	MISC_CRS_P1_03	13013
#define	MISC_CRS_P1_04	13015
#define	MISC_CRS_P4_01	13017
#define	MISC_CRS_P4_02	13019
#define	MISC_CRS_P4_03	13021
#define	MISC_CRS_P5_01	13023
#define	MISC_CRS_P5_02	13025
#define	MISC_CRS_P5_03	13027
#define	MISC_CRS_P6_01	13029
#define	MISC_CRS_P7_01	13031
#define	MISC_YOKE	17001
#define	MISC_DUMMY	21001

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
