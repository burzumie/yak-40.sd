/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  Module:  $RCSfile: $

  Last modification:
    Date:      $Date: 18.02.06 12:05 $
    Version:   $Revision: 1 $
    Author:    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#define MISC_DUMMY_SX				1
#define MISC_DUMMY_SY				1
#define MISC_DUMMY_SXSY				1,1

