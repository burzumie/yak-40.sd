/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "p5_size_odX.h"

#define	P5_AZS_ARK1LEFTRIGHTON_D_00	1001
#define	P5_AZS_ARK1LEFTRIGHTON_D_01	1002
#define	P5_AZS_ARK1LEFTRIGHT_D_00	1003
#define	P5_AZS_ARK1LEFTRIGHT_D_01	1004
#define	P5_AZS_ARK1_CW_D_00	1005
#define	P5_AZS_ARK1_CW_D_01	1006
#define	P5_AZS_ARK2LEFTRIGHTON_D_00	1007
#define	P5_AZS_ARK2LEFTRIGHTON_D_01	1008
#define	P5_AZS_ARK2LEFTRIGHT_D_00	1009
#define	P5_AZS_ARK2LEFTRIGHT_D_01	1010
#define	P5_AZS_ARK2_CW_D_00	1011
#define	P5_AZS_ARK2_CW_D_01	1012
#define	P5_AZS_LIGHTSBCN_D_00	1013
#define	P5_AZS_LIGHTSBCN_D_01	1014
#define	P5_AZS_LIGHTSNAV_D_00	1015
#define	P5_AZS_LIGHTSNAV_D_01	1016
#define	P5_AZS_LLIGHTLEFT_D_00	1017
#define	P5_AZS_LLIGHTLEFT_D_01	1018
#define	P5_AZS_LLIGHTLEFT_D_02	1019
#define	P5_AZS_LLIGHTRIGHT_D_00	1020
#define	P5_AZS_LLIGHTRIGHT_D_01	1021
#define	P5_AZS_LLIGHTRIGHT_D_02	1022
#define	P5_AZS_LLIGHTSMOTOR_D_00	1023
#define	P5_AZS_LLIGHTSMOTOR_D_01	1024
#define	P5_AZS_VHF1ON_D_00	1025
#define	P5_AZS_VHF1ON_D_01	1026
#define	P5_AZS_VHF2ON_D_00	1027
#define	P5_AZS_VHF2ON_D_01	1028
#define	P5_AZS_WINDOWHEATL_D_00	1029
#define	P5_AZS_WINDOWHEATL_D_01	1030
#define	P5_AZS_WINDOWHEATPWR_D_00	1031
#define	P5_AZS_WINDOWHEATPWR_D_01	1032
#define	P5_AZS_WINDOWHEATR_D_00	1033
#define	P5_AZS_WINDOWHEATR_D_01	1034
#define	P5_BTN_FIREENG1_D_00	5001
#define	P5_BTN_FIREENG1_D_01	5002
#define	P5_BTN_FIREENG2_D_00	5003
#define	P5_BTN_FIREENG2_D_01	5004
#define	P5_BTN_FIREENG3_D_00	5005
#define	P5_BTN_FIREENG3_D_01	5006
#define	P5_BTN_FIREENG4_D_00	5007
#define	P5_BTN_FIREENG4_D_01	5008
#define	P5_BTN_FIREENGBAY2_D_00	5009
#define	P5_BTN_FIREENGBAY2_D_01	5010
#define	P5_BTN_FIREENGBAY3_D_00	5011
#define	P5_BTN_FIREENGBAY3_D_01	5012
#define	P5_BTN_FIREENGBAY4_D_00	5013
#define	P5_BTN_FIREENGBAY4_D_01	5014
#define	P5_BTN_FIRELTSTEST_D_00	5015
#define	P5_BTN_FIRELTSTEST_D_01	5016
#define	P5_COV_ARK1LEFT_D	9001
#define	P5_COV_ARK1RIGHT_D	9002
#define	P5_COV_ARK1SIGNAL_D	9003
#define	P5_COV_ARK1VOLUME_D	9004
#define	P5_COV_ARK2LEFT_D	9005
#define	P5_COV_ARK2RIGHT_D	9006
#define	P5_COV_ARK2SIGNAL_D	9007
#define	P5_COV_ARK2VOLUME_D	9008
#define	P5_COV_VHF1ON_D	9009
#define	P5_COV_VHF1VOLUMEOFF_D	9010
#define	P5_COV_VHF1VOLUMEON_D	9011
#define	P5_COV_VHF2ON_D	9012
#define	P5_COV_VHF2VOLUMEOFF_D	9013
#define	P5_COV_VHF2VOLUMEON_D	9014
#define	P5_LMP_ENG1PK_D_00	13001
#define	P5_LMP_ENG1PK_D_01	13002
#define	P5_LMP_ENG1PK_D_02	13003
#define	P5_LMP_ENG2PK_D_00	13004
#define	P5_LMP_ENG2PK_D_01	13005
#define	P5_LMP_ENG2PK_D_02	13006
#define	P5_LMP_ENG3PK_D_00	13007
#define	P5_LMP_ENG3PK_D_01	13008
#define	P5_LMP_ENG3PK_D_02	13009
#define	P5_LMP_FIREBAY1_D_00	13010
#define	P5_LMP_FIREBAY1_D_01	13011
#define	P5_LMP_FIREBAY1_D_02	13012
#define	P5_LMP_FIREBAY2_D_00	13013
#define	P5_LMP_FIREBAY2_D_01	13014
#define	P5_LMP_FIREBAY2_D_02	13015
#define	P5_LMP_FIREBAY3_D_00	13016
#define	P5_LMP_FIREBAY3_D_01	13017
#define	P5_LMP_FIREBAY3_D_02	13018
#define	P5_LMP_FIREENG1_D_00	13019
#define	P5_LMP_FIREENG1_D_01	13020
#define	P5_LMP_FIREENG1_D_02	13021
#define	P5_LMP_FIREENG2_D_00	13022
#define	P5_LMP_FIREENG2_D_01	13023
#define	P5_LMP_FIREENG2_D_02	13024
#define	P5_LMP_FIREENG3_D_00	13025
#define	P5_LMP_FIREENG3_D_01	13026
#define	P5_LMP_FIREENG3_D_02	13027
#define	P5_LMP_FIREEXT1OK_D_00	13028
#define	P5_LMP_FIREEXT1OK_D_01	13029
#define	P5_LMP_FIREEXT1OK_D_02	13030
#define	P5_LMP_FIREEXT2OK_D_00	13031
#define	P5_LMP_FIREEXT2OK_D_01	13032
#define	P5_LMP_FIREEXT2OK_D_02	13033
#define	P5_LMP_FIREEXT3OK_D_00	13034
#define	P5_LMP_FIREEXT3OK_D_01	13035
#define	P5_LMP_FIREEXT3OK_D_02	13036
#define	P5_LMP_FIREEXT4OK_D_00	13037
#define	P5_LMP_FIREEXT4OK_D_01	13038
#define	P5_LMP_FIREEXT4OK_D_02	13039
#define	P5_LMP_FIREEXTBAY1_D_00	13040
#define	P5_LMP_FIREEXTBAY1_D_01	13041
#define	P5_LMP_FIREEXTBAY1_D_02	13042
#define	P5_LMP_FIREEXTBAY2_D_00	13043
#define	P5_LMP_FIREEXTBAY2_D_01	13044
#define	P5_LMP_FIREEXTBAY2_D_02	13045
#define	P5_LMP_FIREEXTBAY3_D_00	13046
#define	P5_LMP_FIREEXTBAY3_D_01	13047
#define	P5_LMP_FIREEXTBAY3_D_02	13048
#define	P5_LMP_FIREEXTENG1_D_00	13049
#define	P5_LMP_FIREEXTENG1_D_01	13050
#define	P5_LMP_FIREEXTENG1_D_02	13051
#define	P5_LMP_FIREEXTENG2_D_00	13052
#define	P5_LMP_FIREEXTENG2_D_01	13053
#define	P5_LMP_FIREEXTENG2_D_02	13054
#define	P5_LMP_FIREEXTENG3_D_00	13055
#define	P5_LMP_FIREEXTENG3_D_01	13056
#define	P5_LMP_FIREEXTENG3_D_02	13057
#define	P5_NDL_ARK1SIGNAL_D	17001
#define	P5_NDL_ARK2SIGNAL_D	17002
#define	P5_SWT_ARK1DIR_D_00	21001
#define	P5_SWT_ARK1DIR_D_01	21002
#define	P5_SWT_ARK1DIR_D_02	21003
#define	P5_SWT_ARK2DIR_D_00	21004
#define	P5_SWT_ARK2DIR_D_01	21005
#define	P5_SWT_ARK2DIR_D_02	21006
#define	P5_SWT_PKAI251_D_00	21007
#define	P5_SWT_PKAI251_D_01	21008
#define	P5_SWT_PKAI251_D_02	21009
#define	P5_SWT_PKAI251_D_03	21010
#define	P5_SWT_PKAI252_D_00	21011
#define	P5_SWT_PKAI252_D_01	21012
#define	P5_SWT_PKAI252_D_02	21013
#define	P5_SWT_PKAI252_D_03	21014
#define	P5_SWT_PKAI253_D_00	21015
#define	P5_SWT_PKAI253_D_01	21016
#define	P5_SWT_PKAI253_D_02	21017
#define	P5_SWT_PKAI253_D_03	21018
#define	P5_TBL_ARK1A	25001
#define	P5_TBL_ARK1B	25003
#define	P5_TBL_ARK1C	25005
#define	P5_TBL_ARK1D	25007
#define	P5_TBL_ARK1E	25009
#define	P5_TBL_ARK1F	25011
#define	P5_TBL_ARK1G	25013
#define	P5_TBL_ARK2A	25015
#define	P5_TBL_ARK2B	25017
#define	P5_TBL_ARK2C	25019
#define	P5_TBL_ARK2D	25021
#define	P5_TBL_ARK2E	25023
#define	P5_TBL_ARK2F	25025
#define	P5_TBL_ARK2G	25027
#define	P5_BACKGROUND10_D	29001
#define	P5_BACKGROUND1_D	29002
#define	P5_BACKGROUND2_D	29003
#define	P5_BACKGROUND3_D	29004
#define	P5_BACKGROUND4_D	29005
#define	P5_BACKGROUND5_D	29006
#define	P5_BACKGROUND6_D	29007
#define	P5_BACKGROUND7_D	29008
#define	P5_BACKGROUND8_D	29009
#define	P5_BACKGROUND9_D	29010
#define	P5_KNB_VHF1LEFTOFF_D_00	33001
#define	P5_KNB_VHF1LEFTOFF_D_01	33002
#define	P5_KNB_VHF1LEFTOFF_D_02	33003
#define	P5_KNB_VHF1LEFTOFF_D_03	33004
#define	P5_KNB_VHF1LEFTOFF_D_04	33005
#define	P5_KNB_VHF1LEFTOFF_D_05	33006
#define	P5_KNB_VHF1LEFTOFF_D_06	33007
#define	P5_KNB_VHF1LEFTOFF_D_07	33008
#define	P5_KNB_VHF1LEFTOFF_D_08	33009
#define	P5_KNB_VHF1LEFTOFF_D_09	33010
#define	P5_KNB_VHF1LEFTOFF_D_10	33011
#define	P5_KNB_VHF1LEFTOFF_D_11	33012
#define	P5_KNB_VHF1LEFTOFF_D_12	33013
#define	P5_KNB_VHF1LEFTOFF_D_13	33014
#define	P5_KNB_VHF1LEFTOFF_D_14	33015
#define	P5_KNB_VHF1LEFTOFF_D_15	33016
#define	P5_KNB_VHF1LEFTOFF_D_16	33017
#define	P5_KNB_VHF1LEFTOFF_D_17	33018
#define	P5_KNB_VHF1LEFTOFF_D_18	33019
#define	P5_KNB_VHF1LEFTOFF_D_19	33020
#define	P5_KNB_VHF1LEFTON_D_00	33021
#define	P5_KNB_VHF1LEFTON_D_01	33022
#define	P5_KNB_VHF1LEFTON_D_02	33023
#define	P5_KNB_VHF1LEFTON_D_03	33024
#define	P5_KNB_VHF1LEFTON_D_04	33025
#define	P5_KNB_VHF1LEFTON_D_05	33026
#define	P5_KNB_VHF1LEFTON_D_06	33027
#define	P5_KNB_VHF1LEFTON_D_07	33028
#define	P5_KNB_VHF1LEFTON_D_08	33029
#define	P5_KNB_VHF1LEFTON_D_09	33030
#define	P5_KNB_VHF1LEFTON_D_10	33031
#define	P5_KNB_VHF1LEFTON_D_11	33032
#define	P5_KNB_VHF1LEFTON_D_12	33033
#define	P5_KNB_VHF1LEFTON_D_13	33034
#define	P5_KNB_VHF1LEFTON_D_14	33035
#define	P5_KNB_VHF1LEFTON_D_15	33036
#define	P5_KNB_VHF1LEFTON_D_16	33037
#define	P5_KNB_VHF1LEFTON_D_17	33038
#define	P5_KNB_VHF1LEFTON_D_18	33039
#define	P5_KNB_VHF1LEFTON_D_19	33040
#define	P5_KNB_VHF1RIGHTOFF_D_00	33041
#define	P5_KNB_VHF1RIGHTOFF_D_01	33042
#define	P5_KNB_VHF1RIGHTOFF_D_02	33043
#define	P5_KNB_VHF1RIGHTOFF_D_03	33044
#define	P5_KNB_VHF1RIGHTOFF_D_04	33045
#define	P5_KNB_VHF1RIGHTOFF_D_05	33046
#define	P5_KNB_VHF1RIGHTOFF_D_06	33047
#define	P5_KNB_VHF1RIGHTOFF_D_07	33048
#define	P5_KNB_VHF1RIGHTOFF_D_08	33049
#define	P5_KNB_VHF1RIGHTOFF_D_09	33050
#define	P5_KNB_VHF1RIGHTOFF_D_10	33051
#define	P5_KNB_VHF1RIGHTOFF_D_11	33052
#define	P5_KNB_VHF1RIGHTOFF_D_12	33053
#define	P5_KNB_VHF1RIGHTOFF_D_13	33054
#define	P5_KNB_VHF1RIGHTOFF_D_14	33055
#define	P5_KNB_VHF1RIGHTOFF_D_15	33056
#define	P5_KNB_VHF1RIGHTOFF_D_16	33057
#define	P5_KNB_VHF1RIGHTOFF_D_17	33058
#define	P5_KNB_VHF1RIGHTOFF_D_18	33059
#define	P5_KNB_VHF1RIGHTOFF_D_19	33060
#define	P5_KNB_VHF1RIGHTON_D_00	33061
#define	P5_KNB_VHF1RIGHTON_D_01	33062
#define	P5_KNB_VHF1RIGHTON_D_02	33063
#define	P5_KNB_VHF1RIGHTON_D_03	33064
#define	P5_KNB_VHF1RIGHTON_D_04	33065
#define	P5_KNB_VHF1RIGHTON_D_05	33066
#define	P5_KNB_VHF1RIGHTON_D_06	33067
#define	P5_KNB_VHF1RIGHTON_D_07	33068
#define	P5_KNB_VHF1RIGHTON_D_08	33069
#define	P5_KNB_VHF1RIGHTON_D_09	33070
#define	P5_KNB_VHF1RIGHTON_D_10	33071
#define	P5_KNB_VHF1RIGHTON_D_11	33072
#define	P5_KNB_VHF1RIGHTON_D_12	33073
#define	P5_KNB_VHF1RIGHTON_D_13	33074
#define	P5_KNB_VHF1RIGHTON_D_14	33075
#define	P5_KNB_VHF1RIGHTON_D_15	33076
#define	P5_KNB_VHF1RIGHTON_D_16	33077
#define	P5_KNB_VHF1RIGHTON_D_17	33078
#define	P5_KNB_VHF1RIGHTON_D_18	33079
#define	P5_KNB_VHF1RIGHTON_D_19	33080
#define	P5_KNB_ARK1LEFT1_D_00	37001
#define	P5_KNB_ARK1LEFT1_D_02	37002
#define	P5_KNB_ARK1LEFT1_D_04	37003
#define	P5_KNB_ARK1LEFT1_D_06	37004
#define	P5_KNB_ARK1LEFT1_D_08	37005
#define	P5_KNB_ARK1LEFT1_D_10	37006
#define	P5_KNB_ARK1LEFT1_D_12	37007
#define	P5_KNB_ARK1LEFT1_D_14	37008
#define	P5_KNB_ARK1LEFT1_D_16	37009
#define	P5_KNB_ARK1LEFT1_D_18	37010
#define	P5_KNB_ARK1RIGHT1_D_00	37011
#define	P5_KNB_ARK1RIGHT1_D_02	37012
#define	P5_KNB_ARK1RIGHT1_D_04	37013
#define	P5_KNB_ARK1RIGHT1_D_06	37014
#define	P5_KNB_ARK1RIGHT1_D_08	37015
#define	P5_KNB_ARK1RIGHT1_D_10	37016
#define	P5_KNB_ARK1RIGHT1_D_12	37017
#define	P5_KNB_ARK1RIGHT1_D_14	37018
#define	P5_KNB_ARK1RIGHT1_D_16	37019
#define	P5_KNB_ARK1RIGHT1_D_18	37020
#define	P5_KNB_ARK2LEFT1_D_00	37021
#define	P5_KNB_ARK2LEFT1_D_02	37022
#define	P5_KNB_ARK2LEFT1_D_04	37023
#define	P5_KNB_ARK2LEFT1_D_06	37024
#define	P5_KNB_ARK2LEFT1_D_08	37025
#define	P5_KNB_ARK2LEFT1_D_10	37026
#define	P5_KNB_ARK2LEFT1_D_12	37027
#define	P5_KNB_ARK2LEFT1_D_14	37028
#define	P5_KNB_ARK2LEFT1_D_16	37029
#define	P5_KNB_ARK2LEFT1_D_18	37030
#define	P5_KNB_ARK2RIGHT1_D_00	37031
#define	P5_KNB_ARK2RIGHT1_D_02	37032
#define	P5_KNB_ARK2RIGHT1_D_04	37033
#define	P5_KNB_ARK2RIGHT1_D_06	37034
#define	P5_KNB_ARK2RIGHT1_D_08	37035
#define	P5_KNB_ARK2RIGHT1_D_10	37036
#define	P5_KNB_ARK2RIGHT1_D_12	37037
#define	P5_KNB_ARK2RIGHT1_D_14	37038
#define	P5_KNB_ARK2RIGHT1_D_16	37039
#define	P5_KNB_ARK2RIGHT1_D_18	37040
#define	P5_SCL_ARK1SIGNAL_D	41001
#define	P5_SCL_ARK2SIGNAL_D	41002
#define	P5_SCL_VHF1LEFTOFF_D_00	45001
#define	P5_SCL_VHF1LEFTOFF_D_02	45002
#define	P5_SCL_VHF1LEFTOFF_D_04	45003
#define	P5_SCL_VHF1LEFTOFF_D_06	45004
#define	P5_SCL_VHF1LEFTOFF_D_08	45005
#define	P5_SCL_VHF1LEFTOFF_D_10	45006
#define	P5_SCL_VHF1LEFTOFF_D_12	45007
#define	P5_SCL_VHF1LEFTOFF_D_14	45008
#define	P5_SCL_VHF1LEFTOFF_D_16	45009
#define	P5_SCL_VHF1LEFTOFF_D_18	45010
#define	P5_SCL_VHF1LEFTOFF_D_19	45011
#define	P5_SCL_VHF1LEFTOFF_D_20	45012
#define	P5_SCL_VHF1LEFTOFF_D_22	45013
#define	P5_SCL_VHF1LEFTOFF_D_24	45014
#define	P5_SCL_VHF1LEFTOFF_D_26	45015
#define	P5_SCL_VHF1LEFTOFF_D_28	45016
#define	P5_SCL_VHF1LEFTOFF_D_30	45017
#define	P5_SCL_VHF1LEFTOFF_D_32	45018
#define	P5_SCL_VHF1LEFTON_D_00	45019
#define	P5_SCL_VHF1LEFTON_D_02	45020
#define	P5_SCL_VHF1LEFTON_D_04	45021
#define	P5_SCL_VHF1LEFTON_D_06	45022
#define	P5_SCL_VHF1LEFTON_D_08	45023
#define	P5_SCL_VHF1LEFTON_D_10	45024
#define	P5_SCL_VHF1LEFTON_D_12	45025
#define	P5_SCL_VHF1LEFTON_D_14	45026
#define	P5_SCL_VHF1LEFTON_D_16	45027
#define	P5_SCL_VHF1LEFTON_D_18	45028
#define	P5_SCL_VHF1LEFTON_D_19	45029
#define	P5_SCL_VHF1LEFTON_D_20	45030
#define	P5_SCL_VHF1LEFTON_D_22	45031
#define	P5_SCL_VHF1LEFTON_D_24	45032
#define	P5_SCL_VHF1LEFTON_D_26	45033
#define	P5_SCL_VHF1LEFTON_D_28	45034
#define	P5_SCL_VHF1LEFTON_D_30	45035
#define	P5_SCL_VHF1LEFTON_D_32	45036
#define	P5_SCL_VHF1MIDOFF_D_00	45037
#define	P5_SCL_VHF1MIDOFF_D_01	45038
#define	P5_SCL_VHF1MIDOFF_D_02	45039
#define	P5_SCL_VHF1MIDOFF_D_03	45040
#define	P5_SCL_VHF1MIDOFF_D_04	45041
#define	P5_SCL_VHF1MIDOFF_D_05	45042
#define	P5_SCL_VHF1MIDOFF_D_06	45043
#define	P5_SCL_VHF1MIDOFF_D_07	45044
#define	P5_SCL_VHF1MIDOFF_D_08	45045
#define	P5_SCL_VHF1MIDOFF_D_09	45046
#define	P5_SCL_VHF1MIDON_D_00	45047
#define	P5_SCL_VHF1MIDON_D_01	45048
#define	P5_SCL_VHF1MIDON_D_02	45049
#define	P5_SCL_VHF1MIDON_D_03	45050
#define	P5_SCL_VHF1MIDON_D_04	45051
#define	P5_SCL_VHF1MIDON_D_05	45052
#define	P5_SCL_VHF1MIDON_D_06	45053
#define	P5_SCL_VHF1MIDON_D_07	45054
#define	P5_SCL_VHF1MIDON_D_08	45055
#define	P5_SCL_VHF1MIDON_D_09	45056
#define	P5_SCL_VHF1RIGHTOFF_D_00	45057
#define	P5_SCL_VHF1RIGHTOFF_D_01	45058
#define	P5_SCL_VHF1RIGHTOFF_D_02	45059
#define	P5_SCL_VHF1RIGHTOFF_D_03	45060
#define	P5_SCL_VHF1RIGHTON_D_00	45061
#define	P5_SCL_VHF1RIGHTON_D_01	45062
#define	P5_SCL_VHF1RIGHTON_D_02	45063
#define	P5_SCL_VHF1RIGHTON_D_03	45064
#define	P5_KNB_VHF2LEFTOFF_D_00	49001
#define	P5_KNB_VHF2LEFTOFF_D_01	49002
#define	P5_KNB_VHF2LEFTOFF_D_02	49003
#define	P5_KNB_VHF2LEFTOFF_D_03	49004
#define	P5_KNB_VHF2LEFTOFF_D_04	49005
#define	P5_KNB_VHF2LEFTOFF_D_05	49006
#define	P5_KNB_VHF2LEFTOFF_D_06	49007
#define	P5_KNB_VHF2LEFTOFF_D_07	49008
#define	P5_KNB_VHF2LEFTOFF_D_08	49009
#define	P5_KNB_VHF2LEFTOFF_D_09	49010
#define	P5_KNB_VHF2LEFTOFF_D_10	49011
#define	P5_KNB_VHF2LEFTOFF_D_11	49012
#define	P5_KNB_VHF2LEFTOFF_D_12	49013
#define	P5_KNB_VHF2LEFTOFF_D_13	49014
#define	P5_KNB_VHF2LEFTOFF_D_14	49015
#define	P5_KNB_VHF2LEFTOFF_D_15	49016
#define	P5_KNB_VHF2LEFTOFF_D_16	49017
#define	P5_KNB_VHF2LEFTOFF_D_17	49018
#define	P5_KNB_VHF2LEFTOFF_D_18	49019
#define	P5_KNB_VHF2LEFTOFF_D_19	49020
#define	P5_KNB_VHF2LEFTON_D_00	49021
#define	P5_KNB_VHF2LEFTON_D_01	49022
#define	P5_KNB_VHF2LEFTON_D_02	49023
#define	P5_KNB_VHF2LEFTON_D_03	49024
#define	P5_KNB_VHF2LEFTON_D_04	49025
#define	P5_KNB_VHF2LEFTON_D_05	49026
#define	P5_KNB_VHF2LEFTON_D_06	49027
#define	P5_KNB_VHF2LEFTON_D_07	49028
#define	P5_KNB_VHF2LEFTON_D_08	49029
#define	P5_KNB_VHF2LEFTON_D_09	49030
#define	P5_KNB_VHF2LEFTON_D_10	49031
#define	P5_KNB_VHF2LEFTON_D_11	49032
#define	P5_KNB_VHF2LEFTON_D_12	49033
#define	P5_KNB_VHF2LEFTON_D_13	49034
#define	P5_KNB_VHF2LEFTON_D_14	49035
#define	P5_KNB_VHF2LEFTON_D_15	49036
#define	P5_KNB_VHF2LEFTON_D_16	49037
#define	P5_KNB_VHF2LEFTON_D_17	49038
#define	P5_KNB_VHF2LEFTON_D_18	49039
#define	P5_KNB_VHF2LEFTON_D_19	49040
#define	P5_KNB_VHF2RIGHTOFF_D_00	49041
#define	P5_KNB_VHF2RIGHTOFF_D_01	49042
#define	P5_KNB_VHF2RIGHTOFF_D_02	49043
#define	P5_KNB_VHF2RIGHTOFF_D_03	49044
#define	P5_KNB_VHF2RIGHTOFF_D_04	49045
#define	P5_KNB_VHF2RIGHTOFF_D_05	49046
#define	P5_KNB_VHF2RIGHTOFF_D_06	49047
#define	P5_KNB_VHF2RIGHTOFF_D_07	49048
#define	P5_KNB_VHF2RIGHTOFF_D_08	49049
#define	P5_KNB_VHF2RIGHTOFF_D_09	49050
#define	P5_KNB_VHF2RIGHTOFF_D_10	49051
#define	P5_KNB_VHF2RIGHTOFF_D_11	49052
#define	P5_KNB_VHF2RIGHTOFF_D_12	49053
#define	P5_KNB_VHF2RIGHTOFF_D_13	49054
#define	P5_KNB_VHF2RIGHTOFF_D_14	49055
#define	P5_KNB_VHF2RIGHTOFF_D_15	49056
#define	P5_KNB_VHF2RIGHTOFF_D_16	49057
#define	P5_KNB_VHF2RIGHTOFF_D_17	49058
#define	P5_KNB_VHF2RIGHTOFF_D_18	49059
#define	P5_KNB_VHF2RIGHTOFF_D_19	49060
#define	P5_KNB_VHF2RIGHTON_D_00	49061
#define	P5_KNB_VHF2RIGHTON_D_01	49062
#define	P5_KNB_VHF2RIGHTON_D_02	49063
#define	P5_KNB_VHF2RIGHTON_D_03	49064
#define	P5_KNB_VHF2RIGHTON_D_04	49065
#define	P5_KNB_VHF2RIGHTON_D_05	49066
#define	P5_KNB_VHF2RIGHTON_D_06	49067
#define	P5_KNB_VHF2RIGHTON_D_07	49068
#define	P5_KNB_VHF2RIGHTON_D_08	49069
#define	P5_KNB_VHF2RIGHTON_D_09	49070
#define	P5_KNB_VHF2RIGHTON_D_10	49071
#define	P5_KNB_VHF2RIGHTON_D_11	49072
#define	P5_KNB_VHF2RIGHTON_D_12	49073
#define	P5_KNB_VHF2RIGHTON_D_13	49074
#define	P5_KNB_VHF2RIGHTON_D_14	49075
#define	P5_KNB_VHF2RIGHTON_D_15	49076
#define	P5_KNB_VHF2RIGHTON_D_16	49077
#define	P5_KNB_VHF2RIGHTON_D_17	49078
#define	P5_KNB_VHF2RIGHTON_D_18	49079
#define	P5_KNB_VHF2RIGHTON_D_19	49080
#define	P5_SCL_VHF2LEFTOFF_D_00	53001
#define	P5_SCL_VHF2LEFTOFF_D_02	53002
#define	P5_SCL_VHF2LEFTOFF_D_04	53003
#define	P5_SCL_VHF2LEFTOFF_D_06	53004
#define	P5_SCL_VHF2LEFTOFF_D_08	53005
#define	P5_SCL_VHF2LEFTOFF_D_10	53006
#define	P5_SCL_VHF2LEFTOFF_D_12	53007
#define	P5_SCL_VHF2LEFTOFF_D_14	53008
#define	P5_SCL_VHF2LEFTOFF_D_16	53009
#define	P5_SCL_VHF2LEFTOFF_D_18	53010
#define	P5_SCL_VHF2LEFTOFF_D_19	53011
#define	P5_SCL_VHF2LEFTOFF_D_20	53012
#define	P5_SCL_VHF2LEFTOFF_D_22	53013
#define	P5_SCL_VHF2LEFTOFF_D_24	53014
#define	P5_SCL_VHF2LEFTOFF_D_26	53015
#define	P5_SCL_VHF2LEFTOFF_D_28	53016
#define	P5_SCL_VHF2LEFTOFF_D_30	53017
#define	P5_SCL_VHF2LEFTOFF_D_32	53018
#define	P5_SCL_VHF2LEFTON_D_00	53019
#define	P5_SCL_VHF2LEFTON_D_02	53020
#define	P5_SCL_VHF2LEFTON_D_04	53021
#define	P5_SCL_VHF2LEFTON_D_06	53022
#define	P5_SCL_VHF2LEFTON_D_08	53023
#define	P5_SCL_VHF2LEFTON_D_10	53024
#define	P5_SCL_VHF2LEFTON_D_12	53025
#define	P5_SCL_VHF2LEFTON_D_14	53026
#define	P5_SCL_VHF2LEFTON_D_16	53027
#define	P5_SCL_VHF2LEFTON_D_18	53028
#define	P5_SCL_VHF2LEFTON_D_19	53029
#define	P5_SCL_VHF2LEFTON_D_20	53030
#define	P5_SCL_VHF2LEFTON_D_22	53031
#define	P5_SCL_VHF2LEFTON_D_24	53032
#define	P5_SCL_VHF2LEFTON_D_26	53033
#define	P5_SCL_VHF2LEFTON_D_28	53034
#define	P5_SCL_VHF2LEFTON_D_30	53035
#define	P5_SCL_VHF2LEFTON_D_32	53036
#define	P5_SCL_VHF2MIDOFF_D_00	53037
#define	P5_SCL_VHF2MIDOFF_D_01	53038
#define	P5_SCL_VHF2MIDOFF_D_02	53039
#define	P5_SCL_VHF2MIDOFF_D_03	53040
#define	P5_SCL_VHF2MIDOFF_D_04	53041
#define	P5_SCL_VHF2MIDOFF_D_05	53042
#define	P5_SCL_VHF2MIDOFF_D_06	53043
#define	P5_SCL_VHF2MIDOFF_D_07	53044
#define	P5_SCL_VHF2MIDOFF_D_08	53045
#define	P5_SCL_VHF2MIDOFF_D_09	53046
#define	P5_SCL_VHF2MIDON_D_00	53047
#define	P5_SCL_VHF2MIDON_D_01	53048
#define	P5_SCL_VHF2MIDON_D_02	53049
#define	P5_SCL_VHF2MIDON_D_03	53050
#define	P5_SCL_VHF2MIDON_D_04	53051
#define	P5_SCL_VHF2MIDON_D_05	53052
#define	P5_SCL_VHF2MIDON_D_06	53053
#define	P5_SCL_VHF2MIDON_D_07	53054
#define	P5_SCL_VHF2MIDON_D_08	53055
#define	P5_SCL_VHF2MIDON_D_09	53056
#define	P5_SCL_VHF2RIGHTOFF_D_00	53057
#define	P5_SCL_VHF2RIGHTOFF_D_01	53058
#define	P5_SCL_VHF2RIGHTOFF_D_02	53059
#define	P5_SCL_VHF2RIGHTOFF_D_03	53060
#define	P5_SCL_VHF2RIGHTON_D_00	53061
#define	P5_SCL_VHF2RIGHTON_D_01	53062
#define	P5_SCL_VHF2RIGHTON_D_02	53063
#define	P5_SCL_VHF2RIGHTON_D_03	53064
#define	P5_GLT_ARK1LEFT100_D_00	57001
#define	P5_GLT_ARK1LEFT100_D_02	57002
#define	P5_GLT_ARK1LEFT100_D_04	57003
#define	P5_GLT_ARK1LEFT100_D_06	57004
#define	P5_GLT_ARK1LEFT100_D_08	57005
#define	P5_GLT_ARK1LEFT100_D_10	57006
#define	P5_GLT_ARK1LEFT100_D_12	57007
#define	P5_GLT_ARK1LEFT100_D_14	57008
#define	P5_GLT_ARK1LEFT100_D_16	57009
#define	P5_GLT_ARK1LEFT100_D_18	57010
#define	P5_GLT_ARK1LEFT100_D_20	57011
#define	P5_GLT_ARK1LEFT100_D_22	57012
#define	P5_GLT_ARK1LEFT10_D_00	57013
#define	P5_GLT_ARK1LEFT10_D_02	57014
#define	P5_GLT_ARK1LEFT10_D_04	57015
#define	P5_GLT_ARK1LEFT10_D_06	57016
#define	P5_GLT_ARK1LEFT10_D_08	57017
#define	P5_GLT_ARK1LEFT10_D_10	57018
#define	P5_GLT_ARK1LEFT10_D_12	57019
#define	P5_GLT_ARK1LEFT10_D_14	57020
#define	P5_GLT_ARK1LEFT10_D_16	57021
#define	P5_GLT_ARK1LEFT10_D_18	57022
#define	P5_GLT_ARK1LEFT1_D_00	57023
#define	P5_GLT_ARK1LEFT1_D_02	57024
#define	P5_GLT_ARK1LEFT1_D_04	57025
#define	P5_GLT_ARK1LEFT1_D_06	57026
#define	P5_GLT_ARK1LEFT1_D_08	57027
#define	P5_GLT_ARK1LEFT1_D_10	57028
#define	P5_GLT_ARK1LEFT1_D_12	57029
#define	P5_GLT_ARK1LEFT1_D_14	57030
#define	P5_GLT_ARK1LEFT1_D_16	57031
#define	P5_GLT_ARK1LEFT1_D_18	57032
#define	P5_GLT_ARK1LEFTHANDLE_D_00	57033
#define	P5_GLT_ARK1LEFTHANDLE_D_02	57034
#define	P5_GLT_ARK1LEFTHANDLE_D_04	57035
#define	P5_GLT_ARK1LEFTHANDLE_D_06	57036
#define	P5_GLT_ARK1LEFTHANDLE_D_08	57037
#define	P5_GLT_ARK1LEFTHANDLE_D_10	57038
#define	P5_GLT_ARK1LEFTHANDLE_D_12	57039
#define	P5_GLT_ARK1LEFTHANDLE_D_14	57040
#define	P5_GLT_ARK1LEFTHANDLE_D_16	57041
#define	P5_GLT_ARK1LEFTHANDLE_D_18	57042
#define	P5_GLT_ARK1RIGHT100_D_00	57043
#define	P5_GLT_ARK1RIGHT100_D_02	57044
#define	P5_GLT_ARK1RIGHT100_D_04	57045
#define	P5_GLT_ARK1RIGHT100_D_06	57046
#define	P5_GLT_ARK1RIGHT100_D_08	57047
#define	P5_GLT_ARK1RIGHT100_D_10	57048
#define	P5_GLT_ARK1RIGHT100_D_12	57049
#define	P5_GLT_ARK1RIGHT100_D_14	57050
#define	P5_GLT_ARK1RIGHT100_D_16	57051
#define	P5_GLT_ARK1RIGHT100_D_18	57052
#define	P5_GLT_ARK1RIGHT100_D_20	57053
#define	P5_GLT_ARK1RIGHT100_D_22	57054
#define	P5_GLT_ARK1RIGHT10_D_00	57055
#define	P5_GLT_ARK1RIGHT10_D_02	57056
#define	P5_GLT_ARK1RIGHT10_D_04	57057
#define	P5_GLT_ARK1RIGHT10_D_06	57058
#define	P5_GLT_ARK1RIGHT10_D_08	57059
#define	P5_GLT_ARK1RIGHT10_D_10	57060
#define	P5_GLT_ARK1RIGHT10_D_12	57061
#define	P5_GLT_ARK1RIGHT10_D_14	57062
#define	P5_GLT_ARK1RIGHT10_D_16	57063
#define	P5_GLT_ARK1RIGHT10_D_18	57064
#define	P5_GLT_ARK1RIGHT1_D_00	57065
#define	P5_GLT_ARK1RIGHT1_D_02	57066
#define	P5_GLT_ARK1RIGHT1_D_04	57067
#define	P5_GLT_ARK1RIGHT1_D_06	57068
#define	P5_GLT_ARK1RIGHT1_D_08	57069
#define	P5_GLT_ARK1RIGHT1_D_10	57070
#define	P5_GLT_ARK1RIGHT1_D_12	57071
#define	P5_GLT_ARK1RIGHT1_D_14	57072
#define	P5_GLT_ARK1RIGHT1_D_16	57073
#define	P5_GLT_ARK1RIGHT1_D_18	57074
#define	P5_GLT_ARK1RIGHTHANDLE_D_00	57075
#define	P5_GLT_ARK1RIGHTHANDLE_D_02	57076
#define	P5_GLT_ARK1RIGHTHANDLE_D_04	57077
#define	P5_GLT_ARK1RIGHTHANDLE_D_06	57078
#define	P5_GLT_ARK1RIGHTHANDLE_D_08	57079
#define	P5_GLT_ARK1RIGHTHANDLE_D_10	57080
#define	P5_GLT_ARK1RIGHTHANDLE_D_12	57081
#define	P5_GLT_ARK1RIGHTHANDLE_D_14	57082
#define	P5_GLT_ARK1RIGHTHANDLE_D_16	57083
#define	P5_GLT_ARK1RIGHTHANDLE_D_18	57084
#define	P5_GLT_ARK1_D_00	57085
#define	P5_GLT_ARK1_D_01	57086
#define	P5_GLT_ARK1_D_02	57087
#define	P5_GLT_ARK1_D_03	57088
#define	P5_GLT_ARK2LEFT100_D_00	61001
#define	P5_GLT_ARK2LEFT100_D_02	61002
#define	P5_GLT_ARK2LEFT100_D_04	61003
#define	P5_GLT_ARK2LEFT100_D_06	61004
#define	P5_GLT_ARK2LEFT100_D_08	61005
#define	P5_GLT_ARK2LEFT100_D_10	61006
#define	P5_GLT_ARK2LEFT100_D_12	61007
#define	P5_GLT_ARK2LEFT100_D_14	61008
#define	P5_GLT_ARK2LEFT100_D_16	61009
#define	P5_GLT_ARK2LEFT100_D_18	61010
#define	P5_GLT_ARK2LEFT100_D_20	61011
#define	P5_GLT_ARK2LEFT100_D_22	61012
#define	P5_GLT_ARK2LEFT10_D_00	61013
#define	P5_GLT_ARK2LEFT10_D_02	61014
#define	P5_GLT_ARK2LEFT10_D_04	61015
#define	P5_GLT_ARK2LEFT10_D_06	61016
#define	P5_GLT_ARK2LEFT10_D_08	61017
#define	P5_GLT_ARK2LEFT10_D_10	61018
#define	P5_GLT_ARK2LEFT10_D_12	61019
#define	P5_GLT_ARK2LEFT10_D_14	61020
#define	P5_GLT_ARK2LEFT10_D_16	61021
#define	P5_GLT_ARK2LEFT10_D_18	61022
#define	P5_GLT_ARK2LEFT1_D_00	61023
#define	P5_GLT_ARK2LEFT1_D_02	61024
#define	P5_GLT_ARK2LEFT1_D_04	61025
#define	P5_GLT_ARK2LEFT1_D_06	61026
#define	P5_GLT_ARK2LEFT1_D_08	61027
#define	P5_GLT_ARK2LEFT1_D_10	61028
#define	P5_GLT_ARK2LEFT1_D_12	61029
#define	P5_GLT_ARK2LEFT1_D_14	61030
#define	P5_GLT_ARK2LEFT1_D_16	61031
#define	P5_GLT_ARK2LEFT1_D_18	61032
#define	P5_GLT_ARK2LEFTHANDLE_D_00	61033
#define	P5_GLT_ARK2LEFTHANDLE_D_02	61034
#define	P5_GLT_ARK2LEFTHANDLE_D_04	61035
#define	P5_GLT_ARK2LEFTHANDLE_D_06	61036
#define	P5_GLT_ARK2LEFTHANDLE_D_08	61037
#define	P5_GLT_ARK2LEFTHANDLE_D_10	61038
#define	P5_GLT_ARK2LEFTHANDLE_D_12	61039
#define	P5_GLT_ARK2LEFTHANDLE_D_14	61040
#define	P5_GLT_ARK2LEFTHANDLE_D_16	61041
#define	P5_GLT_ARK2LEFTHANDLE_D_18	61042
#define	P5_GLT_ARK2RIGHT100_D_00	61043
#define	P5_GLT_ARK2RIGHT100_D_02	61044
#define	P5_GLT_ARK2RIGHT100_D_04	61045
#define	P5_GLT_ARK2RIGHT100_D_06	61046
#define	P5_GLT_ARK2RIGHT100_D_08	61047
#define	P5_GLT_ARK2RIGHT100_D_10	61048
#define	P5_GLT_ARK2RIGHT100_D_12	61049
#define	P5_GLT_ARK2RIGHT100_D_14	61050
#define	P5_GLT_ARK2RIGHT100_D_16	61051
#define	P5_GLT_ARK2RIGHT100_D_18	61052
#define	P5_GLT_ARK2RIGHT100_D_20	61053
#define	P5_GLT_ARK2RIGHT100_D_22	61054
#define	P5_GLT_ARK2RIGHT10_D_00	61055
#define	P5_GLT_ARK2RIGHT10_D_02	61056
#define	P5_GLT_ARK2RIGHT10_D_04	61057
#define	P5_GLT_ARK2RIGHT10_D_06	61058
#define	P5_GLT_ARK2RIGHT10_D_08	61059
#define	P5_GLT_ARK2RIGHT10_D_10	61060
#define	P5_GLT_ARK2RIGHT10_D_12	61061
#define	P5_GLT_ARK2RIGHT10_D_14	61062
#define	P5_GLT_ARK2RIGHT10_D_16	61063
#define	P5_GLT_ARK2RIGHT10_D_18	61064
#define	P5_GLT_ARK2RIGHT1_D_00	61065
#define	P5_GLT_ARK2RIGHT1_D_02	61066
#define	P5_GLT_ARK2RIGHT1_D_04	61067
#define	P5_GLT_ARK2RIGHT1_D_06	61068
#define	P5_GLT_ARK2RIGHT1_D_08	61069
#define	P5_GLT_ARK2RIGHT1_D_10	61070
#define	P5_GLT_ARK2RIGHT1_D_12	61071
#define	P5_GLT_ARK2RIGHT1_D_14	61072
#define	P5_GLT_ARK2RIGHT1_D_16	61073
#define	P5_GLT_ARK2RIGHT1_D_18	61074
#define	P5_GLT_ARK2RIGHTHANDLE_D_00	61075
#define	P5_GLT_ARK2RIGHTHANDLE_D_02	61076
#define	P5_GLT_ARK2RIGHTHANDLE_D_04	61077
#define	P5_GLT_ARK2RIGHTHANDLE_D_06	61078
#define	P5_GLT_ARK2RIGHTHANDLE_D_08	61079
#define	P5_GLT_ARK2RIGHTHANDLE_D_10	61080
#define	P5_GLT_ARK2RIGHTHANDLE_D_12	61081
#define	P5_GLT_ARK2RIGHTHANDLE_D_14	61082
#define	P5_GLT_ARK2RIGHTHANDLE_D_16	61083
#define	P5_GLT_ARK2RIGHTHANDLE_D_18	61084
#define	P5_GLT_ARK2_D_00	61085
#define	P5_GLT_ARK2_D_01	61086
#define	P5_GLT_ARK2_D_02	61087
#define	P5_GLT_ARK2_D_03	61088
#define	MISC_CRS_P0_01	69001
#define	MISC_CRS_P0_02	69003
#define	MISC_CRS_P0_03	69005
#define	MISC_CRS_P0_04	69007
#define	MISC_CRS_P1_01	69009
#define	MISC_CRS_P1_02	69011
#define	MISC_CRS_P1_03	69013
#define	MISC_CRS_P1_04	69015
#define	MISC_CRS_P4_01	69017
#define	MISC_CRS_P4_02	69019
#define	MISC_CRS_P4_03	69021
#define	MISC_CRS_P5_01	69023
#define	MISC_CRS_P5_02	69025
#define	MISC_CRS_P5_03	69027
#define	MISC_CRS_P6_01	69029
#define	MISC_CRS_P7_01	69031
#define	MISC_YOKE	73001
#define	MISC_DUMMY	77001

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
