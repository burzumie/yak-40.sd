/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../VersionX.h"

#include "kln_sizeX.h"

#define	KLN90B_BTN_IN_D	1001
#define	KLN90B_BTN_IN_M	1002
#define	KLN90B_BTN_IN_P	1003
#define	KLN90B_BTN_IN_R	1004
#define	KLN90B_BTN_OUT_D	1005
#define	KLN90B_BTN_OUT_M	1006
#define	KLN90B_BTN_OUT_P	1007
#define	KLN90B_BTN_OUT_R	1008
#define	KLN90B_BACKGROUND	5001
#define	KLN90B_BACKGROUND_D	5002
#define	KLN90B_BACKGROUND_M	5003
#define	KLN90B_BACKGROUND_P	5004
#define	KLN90B_BACKGROUND_R	5005
#define	KLN90B_BCK_DISPLAY	9001

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
