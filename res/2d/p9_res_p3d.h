/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../Version.h"

#include "p9_size_p3d.h"

#define	P9_AZS_911_D_00	1001
#define	P9_AZS_911_D_01	1002
#define	P9_AZS_911_P_00	1003
#define	P9_AZS_911_P_01	1004
#define	P9_AZS_912_D_00	1005
#define	P9_AZS_912_D_01	1006
#define	P9_AZS_912_P_00	1007
#define	P9_AZS_912_P_01	1008
#define	P9_AZS_913_D_00	1009
#define	P9_AZS_913_D_01	1010
#define	P9_AZS_913_P_00	1011
#define	P9_AZS_913_P_01	1012
#define	P9_AZS_914_D_00	1013
#define	P9_AZS_914_D_01	1014
#define	P9_AZS_914_P_00	1015
#define	P9_AZS_914_P_01	1016
#define	P9_AZS_915_D_00	1017
#define	P9_AZS_915_D_01	1018
#define	P9_AZS_915_P_00	1019
#define	P9_AZS_915_P_01	1020
#define	P9_AZS_916_D_00	1021
#define	P9_AZS_916_D_01	1022
#define	P9_AZS_916_P_00	1023
#define	P9_AZS_916_P_01	1024
#define	P9_AZS_917_D_00	1025
#define	P9_AZS_917_D_01	1026
#define	P9_AZS_917_P_00	1027
#define	P9_AZS_917_P_01	1028
#define	P9_AZS_918_D_00	1029
#define	P9_AZS_918_D_01	1030
#define	P9_AZS_918_P_00	1031
#define	P9_AZS_918_P_01	1032
#define	P9_AZS_919_D_00	1033
#define	P9_AZS_919_D_01	1034
#define	P9_AZS_919_P_00	1035
#define	P9_AZS_919_P_01	1036
#define	P9_AZS_921_D_00	1037
#define	P9_AZS_921_D_01	1038
#define	P9_AZS_921_P_00	1039
#define	P9_AZS_921_P_01	1040
#define	P9_AZS_922_D_00	1041
#define	P9_AZS_922_D_01	1042
#define	P9_AZS_922_P_00	1043
#define	P9_AZS_922_P_01	1044
#define	P9_AZS_923_D_00	1045
#define	P9_AZS_923_D_01	1046
#define	P9_AZS_923_P_00	1047
#define	P9_AZS_923_P_01	1048
#define	P9_AZS_924_D_00	1049
#define	P9_AZS_924_D_01	1050
#define	P9_AZS_924_P_00	1051
#define	P9_AZS_924_P_01	1052
#define	P9_AZS_925_D_00	1053
#define	P9_AZS_925_D_01	1054
#define	P9_AZS_925_P_00	1055
#define	P9_AZS_925_P_01	1056
#define	P9_AZS_926_D_00	1057
#define	P9_AZS_926_D_01	1058
#define	P9_AZS_926_P_00	1059
#define	P9_AZS_926_P_01	1060
#define	P9_AZS_927_D_00	1061
#define	P9_AZS_927_D_01	1062
#define	P9_AZS_927_P_00	1063
#define	P9_AZS_927_P_01	1064
#define	P9_AZS_928_D_00	1065
#define	P9_AZS_928_D_01	1066
#define	P9_AZS_928_P_00	1067
#define	P9_AZS_928_P_01	1068
#define	P9_AZS_929_D_00	1069
#define	P9_AZS_929_D_01	1070
#define	P9_AZS_929_P_00	1071
#define	P9_AZS_929_P_01	1072
#define	P9_AZS_931_D_00	1073
#define	P9_AZS_931_D_01	1074
#define	P9_AZS_931_P_00	1075
#define	P9_AZS_931_P_01	1076
#define	P9_AZS_932_D_00	1077
#define	P9_AZS_932_D_01	1078
#define	P9_AZS_932_P_00	1079
#define	P9_AZS_932_P_01	1080
#define	P9_AZS_933_D_00	1081
#define	P9_AZS_933_D_01	1082
#define	P9_AZS_933_P_00	1083
#define	P9_AZS_933_P_01	1084
#define	P9_AZS_934_D_00	1085
#define	P9_AZS_934_D_01	1086
#define	P9_AZS_934_P_00	1087
#define	P9_AZS_934_P_01	1088
#define	P9_AZS_935_D_00	1089
#define	P9_AZS_935_D_01	1090
#define	P9_AZS_935_P_00	1091
#define	P9_AZS_935_P_01	1092
#define	P9_AZS_936_D_00	1093
#define	P9_AZS_936_D_01	1094
#define	P9_AZS_936_P_00	1095
#define	P9_AZS_936_P_01	1096
#define	P9_AZS_937_D_00	1097
#define	P9_AZS_937_D_01	1098
#define	P9_AZS_937_P_00	1099
#define	P9_AZS_937_P_01	1100
#define	P9_AZS_938_D_00	1101
#define	P9_AZS_938_D_01	1102
#define	P9_AZS_938_P_00	1103
#define	P9_AZS_938_P_01	1104
#define	P9_AZS_939_D_00	1105
#define	P9_AZS_939_D_01	1106
#define	P9_AZS_939_P_00	1107
#define	P9_AZS_939_P_01	1108
#define	P9_AZS_941_D_00	1109
#define	P9_AZS_941_D_01	1110
#define	P9_AZS_941_P_00	1111
#define	P9_AZS_941_P_01	1112
#define	P9_AZS_942_D_00	1113
#define	P9_AZS_942_D_01	1114
#define	P9_AZS_942_P_00	1115
#define	P9_AZS_942_P_01	1116
#define	P9_AZS_943_D_00	1117
#define	P9_AZS_943_D_01	1118
#define	P9_AZS_943_P_00	1119
#define	P9_AZS_943_P_01	1120
#define	P9_AZS_944_D_00	1121
#define	P9_AZS_944_D_01	1122
#define	P9_AZS_944_P_00	1123
#define	P9_AZS_944_P_01	1124
#define	P9_AZS_945_D_00	1125
#define	P9_AZS_945_D_01	1126
#define	P9_AZS_945_P_00	1127
#define	P9_AZS_945_P_01	1128
#define	P9_AZS_946_D_00	1129
#define	P9_AZS_946_D_01	1130
#define	P9_AZS_946_P_00	1131
#define	P9_AZS_946_P_01	1132
#define	P9_AZS_947_D_00	1133
#define	P9_AZS_947_D_01	1134
#define	P9_AZS_947_P_00	1135
#define	P9_AZS_947_P_01	1136
#define	P9_AZS_948_D_00	1137
#define	P9_AZS_948_D_01	1138
#define	P9_AZS_948_P_00	1139
#define	P9_AZS_948_P_01	1140
#define	P9_AZS_949_D_00	1141
#define	P9_AZS_949_D_01	1142
#define	P9_AZS_949_P_00	1143
#define	P9_AZS_949_P_01	1144
#define	P9_BACKGROUND_D	5001
#define	P9_BACKGROUND_P	5002
#define	MISC_CRS_P0_01	13001
#define	MISC_CRS_P0_02	13003
#define	MISC_CRS_P0_03	13005
#define	MISC_CRS_P0_04	13007
#define	MISC_CRS_P1_01	13009
#define	MISC_CRS_P1_02	13011
#define	MISC_CRS_P1_03	13013
#define	MISC_CRS_P1_04	13015
#define	MISC_CRS_P4_01	13017
#define	MISC_CRS_P4_02	13019
#define	MISC_CRS_P4_03	13021
#define	MISC_CRS_P5_01	13023
#define	MISC_CRS_P5_02	13025
#define	MISC_CRS_P5_03	13027
#define	MISC_CRS_P6_01	13029
#define	MISC_CRS_P7_01	13031
#define	MISC_YOKE	17001
#define	MISC_DUMMY	21001

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
