/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "../Version.h"

#include "p8_size_p.h"

#define	P8_AZS_811_D_00	1001
#define	P8_AZS_811_D_01	1002
#define	P8_AZS_811_P_00	1003
#define	P8_AZS_811_P_01	1004
#define	P8_AZS_812_D_00	1005
#define	P8_AZS_812_D_01	1006
#define	P8_AZS_812_P_00	1007
#define	P8_AZS_812_P_01	1008
#define	P8_AZS_813_D_00	1009
#define	P8_AZS_813_D_01	1010
#define	P8_AZS_813_P_00	1011
#define	P8_AZS_813_P_01	1012
#define	P8_AZS_814_D_00	1013
#define	P8_AZS_814_D_01	1014
#define	P8_AZS_814_P_00	1015
#define	P8_AZS_814_P_01	1016
#define	P8_AZS_815_D_00	1017
#define	P8_AZS_815_D_01	1018
#define	P8_AZS_815_P_00	1019
#define	P8_AZS_815_P_01	1020
#define	P8_AZS_816_D_00	1021
#define	P8_AZS_816_D_01	1022
#define	P8_AZS_816_P_00	1023
#define	P8_AZS_816_P_01	1024
#define	P8_AZS_817_D_00	1025
#define	P8_AZS_817_D_01	1026
#define	P8_AZS_817_P_00	1027
#define	P8_AZS_817_P_01	1028
#define	P8_AZS_818_D_00	1029
#define	P8_AZS_818_D_01	1030
#define	P8_AZS_818_P_00	1031
#define	P8_AZS_818_P_01	1032
#define	P8_AZS_819_D_00	1033
#define	P8_AZS_819_D_01	1034
#define	P8_AZS_819_P_00	1035
#define	P8_AZS_819_P_01	1036
#define	P8_AZS_821_D_00	1037
#define	P8_AZS_821_D_01	1038
#define	P8_AZS_821_P_00	1039
#define	P8_AZS_821_P_01	1040
#define	P8_AZS_822_D_00	1041
#define	P8_AZS_822_D_01	1042
#define	P8_AZS_822_P_00	1043
#define	P8_AZS_822_P_01	1044
#define	P8_AZS_823_D_00	1045
#define	P8_AZS_823_D_01	1046
#define	P8_AZS_823_P_00	1047
#define	P8_AZS_823_P_01	1048
#define	P8_AZS_824_D_00	1049
#define	P8_AZS_824_D_01	1050
#define	P8_AZS_824_P_00	1051
#define	P8_AZS_824_P_01	1052
#define	P8_AZS_825_D_00	1053
#define	P8_AZS_825_D_01	1054
#define	P8_AZS_825_P_00	1055
#define	P8_AZS_825_P_01	1056
#define	P8_AZS_826_D_00	1057
#define	P8_AZS_826_D_01	1058
#define	P8_AZS_826_P_00	1059
#define	P8_AZS_826_P_01	1060
#define	P8_AZS_827_D_00	1061
#define	P8_AZS_827_D_01	1062
#define	P8_AZS_827_P_00	1063
#define	P8_AZS_827_P_01	1064
#define	P8_AZS_828_D_00	1065
#define	P8_AZS_828_D_01	1066
#define	P8_AZS_828_P_00	1067
#define	P8_AZS_828_P_01	1068
#define	P8_AZS_829_D_00	1069
#define	P8_AZS_829_D_01	1070
#define	P8_AZS_829_P_00	1071
#define	P8_AZS_829_P_01	1072
#define	P8_AZS_831_D_00	1073
#define	P8_AZS_831_D_01	1074
#define	P8_AZS_831_P_00	1075
#define	P8_AZS_831_P_01	1076
#define	P8_AZS_832_D_00	1077
#define	P8_AZS_832_D_01	1078
#define	P8_AZS_832_P_00	1079
#define	P8_AZS_832_P_01	1080
#define	P8_AZS_833_D_00	1081
#define	P8_AZS_833_D_01	1082
#define	P8_AZS_833_P_00	1083
#define	P8_AZS_833_P_01	1084
#define	P8_AZS_834_D_00	1085
#define	P8_AZS_834_D_01	1086
#define	P8_AZS_834_P_00	1087
#define	P8_AZS_834_P_01	1088
#define	P8_AZS_835_D_00	1089
#define	P8_AZS_835_D_01	1090
#define	P8_AZS_835_P_00	1091
#define	P8_AZS_835_P_01	1092
#define	P8_AZS_836_D_00	1093
#define	P8_AZS_836_D_01	1094
#define	P8_AZS_836_P_00	1095
#define	P8_AZS_836_P_01	1096
#define	P8_AZS_837_D_00	1097
#define	P8_AZS_837_D_01	1098
#define	P8_AZS_837_P_00	1099
#define	P8_AZS_837_P_01	1100
#define	P8_AZS_838_D_00	1101
#define	P8_AZS_838_D_01	1102
#define	P8_AZS_838_P_00	1103
#define	P8_AZS_838_P_01	1104
#define	P8_AZS_839_D_00	1105
#define	P8_AZS_839_D_01	1106
#define	P8_AZS_839_P_00	1107
#define	P8_AZS_839_P_01	1108
#define	P8_AZS_841_D_00	1109
#define	P8_AZS_841_D_01	1110
#define	P8_AZS_841_P_00	1111
#define	P8_AZS_841_P_01	1112
#define	P8_AZS_842_D_00	1113
#define	P8_AZS_842_D_01	1114
#define	P8_AZS_842_P_00	1115
#define	P8_AZS_842_P_01	1116
#define	P8_AZS_843_D_00	1117
#define	P8_AZS_843_D_01	1118
#define	P8_AZS_843_P_00	1119
#define	P8_AZS_843_P_01	1120
#define	P8_AZS_844_D_00	1121
#define	P8_AZS_844_D_01	1122
#define	P8_AZS_844_P_00	1123
#define	P8_AZS_844_P_01	1124
#define	P8_AZS_845_D_00	1125
#define	P8_AZS_845_D_01	1126
#define	P8_AZS_845_P_00	1127
#define	P8_AZS_845_P_01	1128
#define	P8_AZS_846_D_00	1129
#define	P8_AZS_846_D_01	1130
#define	P8_AZS_846_P_00	1131
#define	P8_AZS_846_P_01	1132
#define	P8_AZS_847_D_00	1133
#define	P8_AZS_847_D_01	1134
#define	P8_AZS_847_P_00	1135
#define	P8_AZS_847_P_01	1136
#define	P8_AZS_848_D_00	1137
#define	P8_AZS_848_D_01	1138
#define	P8_AZS_848_P_00	1139
#define	P8_AZS_848_P_01	1140
#define	P8_AZS_849_D_00	1141
#define	P8_AZS_849_D_01	1142
#define	P8_AZS_849_P_00	1143
#define	P8_AZS_849_P_01	1144
#define	P8_BACKGROUND_D	5001
#define	P8_BACKGROUND_P	5002
#define	MISC_CRS_P0_01	13001
#define	MISC_CRS_P0_02	13003
#define	MISC_CRS_P0_03	13005
#define	MISC_CRS_P0_04	13007
#define	MISC_CRS_P1_01	13009
#define	MISC_CRS_P1_02	13011
#define	MISC_CRS_P1_03	13013
#define	MISC_CRS_P1_04	13015
#define	MISC_CRS_P4_01	13017
#define	MISC_CRS_P4_02	13019
#define	MISC_CRS_P4_03	13021
#define	MISC_CRS_P5_01	13023
#define	MISC_CRS_P5_02	13025
#define	MISC_CRS_P5_03	13027
#define	MISC_CRS_P6_01	13029
#define	MISC_CRS_P7_01	13031
#define	MISC_YOKE	17001
#define	MISC_DUMMY	21001

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
