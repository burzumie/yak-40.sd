/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#define	MISC_CRS_P0_01_SX	81
#define	MISC_CRS_P0_01_SY	80
#define	MISC_CRS_P0_02_SX	103
#define	MISC_CRS_P0_02_SY	51
#define	MISC_CRS_P0_03_SX	103
#define	MISC_CRS_P0_03_SY	51
#define	MISC_CRS_P0_04_SX	54
#define	MISC_CRS_P0_04_SY	106
#define	MISC_CRS_P1_01_SX	81
#define	MISC_CRS_P1_01_SY	80
#define	MISC_CRS_P1_02_SX	103
#define	MISC_CRS_P1_02_SY	51
#define	MISC_CRS_P1_03_SX	103
#define	MISC_CRS_P1_03_SY	51
#define	MISC_CRS_P1_04_SX	54
#define	MISC_CRS_P1_04_SY	106
#define	MISC_CRS_P4_01_SX	81
#define	MISC_CRS_P4_01_SY	80
#define	MISC_CRS_P4_02_SX	105
#define	MISC_CRS_P4_02_SY	73
#define	MISC_CRS_P4_03_SX	81
#define	MISC_CRS_P4_03_SY	80
#define	MISC_CRS_P5_01_SX	81
#define	MISC_CRS_P5_01_SY	80
#define	MISC_CRS_P5_02_SX	105
#define	MISC_CRS_P5_02_SY	73
#define	MISC_CRS_P5_03_SX	81
#define	MISC_CRS_P5_03_SY	80
#define	MISC_CRS_P6_01_SX	81
#define	MISC_CRS_P6_01_SY	80
#define	MISC_CRS_P7_01_SX	81
#define	MISC_CRS_P7_01_SY	80
#define	MISC_YOKE_SX	100
#define	MISC_YOKE_SY	48
#define	MISC_DUMMY_SX	1
#define	MISC_DUMMY_SY	1
