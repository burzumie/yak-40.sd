/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#define	KLN90B_BTN_IN_D_SX	100
#define	KLN90B_BTN_IN_D_SY	60
#define	KLN90B_BTN_IN_M_SX	100
#define	KLN90B_BTN_IN_M_SY	60
#define	KLN90B_BTN_IN_P_SX	100
#define	KLN90B_BTN_IN_P_SY	60
#define	KLN90B_BTN_IN_R_SX	100
#define	KLN90B_BTN_IN_R_SY	60
#define	KLN90B_BTN_OUT_D_SX	100
#define	KLN90B_BTN_OUT_D_SY	60
#define	KLN90B_BTN_OUT_M_SX	100
#define	KLN90B_BTN_OUT_M_SY	60
#define	KLN90B_BTN_OUT_P_SX	100
#define	KLN90B_BTN_OUT_P_SY	60
#define	KLN90B_BTN_OUT_R_SX	100
#define	KLN90B_BTN_OUT_R_SY	60
#define	KLN90B_BACKGROUND_SX	514
#define	KLN90B_BACKGROUND_SY	167
#define	KLN90B_BACKGROUND_D_SX	514
#define	KLN90B_BACKGROUND_D_SY	167
#define	KLN90B_BACKGROUND_M_SX	514
#define	KLN90B_BACKGROUND_M_SY	167
#define	KLN90B_BACKGROUND_P_SX	514
#define	KLN90B_BACKGROUND_P_SY	167
#define	KLN90B_BACKGROUND_R_SX	514
#define	KLN90B_BACKGROUND_R_SY	167
#define	KLN90B_BCK_DISPLAY_SX	253
#define	KLN90B_BCK_DISPLAY_SY	101
