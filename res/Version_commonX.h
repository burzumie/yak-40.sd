/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Res/2d/Version.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#define VERSION_MAJOR         3
#define VERSION_MINOR         3
#define VERSION_BUILD         3

#define COMPANY_NAME         ""
#define FILE_DESCRIPTION     "Yakovlev Yak-40 for Microsoft Flight Simulator X"
#define LEGAL_COPYRIGHT      "(c) 2006-2012 by Nick Sharmanzhinov (except) and [Igor Suprunov]"
#define LEGAL_TRADEMARK      ""
#define PRODUCT_NAME         "Yak-40"

#define lita(arg) #arg
#define xlita(arg) lita(arg)
#define cat3(w,x,z) w##.##x##.##z##\000
#define xcat3(w,x,z) cat3(w,x,z)
#define VERSION_STRING xlita(xcat3(VERSION_MAJOR,VERSION_MINOR,VERSION_BUILD))

