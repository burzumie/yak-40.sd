/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Lib/NamedVar.cpp $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include"NamedVar.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool CNamedVar::m_bEnumsInitialised = false;
ENUM CNamedVar::m_boolEnum          = 0;
ENUM CNamedVar::m_numberEnum        = 0;
ENUM CNamedVar::m_degreeEnum        = 0;
ENUM CNamedVar::m_radianEnum        = 0;
ENUM CNamedVar::m_feet_per_secEnum  = 0;
ENUM CNamedVar::m_km_per_hourEnum   = 0;
ENUM CNamedVar::m_kmEnum            = 0;
ENUM CNamedVar::m_meter_per_secEnum = 0;
ENUM CNamedVar::m_meters_Enum       = 0;
ENUM CNamedVar::m_percent_Enum      = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CNamedVar::CNamedVar(const char *nme,bool unregister_after_use) : m_bUnregisterAfterUse(unregister_after_use)
{
	strcpy_s(name,BUFSIZ,nme);
	if((m_VarID=check_named_variable(name))==-1)
		m_VarID=register_named_variable(name);
    init_enums();
}

CNamedVar::CNamedVar(const std::string nme,bool unregister_after_use) : m_bUnregisterAfterUse(unregister_after_use)
{
	strcpy_s(name,BUFSIZ,nme.c_str());
	if((m_VarID=check_named_variable(name))==-1)
		m_VarID=register_named_variable(name);
	init_enums();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CNamedVar::~CNamedVar(void)
{
	if(m_bUnregisterAfterUse&&m_VarID!=-1) 
		unregister_var_by_name((PSTRINGZ)get_name_of_named_variable(m_VarID));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CNamedVar::init_enums()
{
	SECUREBEGIN_K

	if(m_bEnumsInitialised)return;

	m_boolEnum          = get_units_enum( "bool"   );
	m_numberEnum        = get_units_enum( "number" );
	m_degreeEnum        = get_units_enum( "degree" );
	m_radianEnum        = get_units_enum( "radian" );
	m_feet_per_secEnum  = get_units_enum( "feet per second" ); 
	m_km_per_hourEnum   = get_units_enum( "kilometer per hour" );
	m_kmEnum            = get_units_enum( "kilometer" );
	m_meter_per_secEnum = get_units_enum( "meters per second" );
	m_meters_Enum       = get_units_enum( "meters" );
	m_percent_Enum      = get_units_enum( "percent" );

	m_bEnumsInitialised = true;

	SECUREEND_K
}
