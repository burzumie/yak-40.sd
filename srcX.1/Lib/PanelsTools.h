/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Lib/PanelsTools.h $

  Last modification:
    $Date: 19.02.06 9:19 $
    $Revision: 4 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include "../../Res/VersionX.h"
#include "../Common/CommonSys.h"

void	__cdecl CheckBright(PELEMENT_ICON pelement,int state);
void	__cdecl CheckBright(PELEMENT_NEEDLE pelement,int state);
void	__cdecl CheckBright(PELEMENT_MOVING_IMAGE pelement,int state);
void	__cdecl CheckBright(PELEMENT_STRING pelement,int state);
void	__cdecl CheckBright(PELEMENT_SPRITE pelement,int state);
void	__cdecl CheckBright(PELEMENT_SLIDER pelement,int state);

int		__cdecl CheckVisible(PELEMENT_ICON pelement,int state,int estate);
int		__cdecl CheckVisible(PELEMENT_NEEDLE pelement,int state,int estate);
int		__cdecl CheckVisible(PELEMENT_MOVING_IMAGE pelement,int state,int estate);
int		__cdecl CheckVisible(PELEMENT_STRING pelement,int state,int estate);
int		__cdecl CheckVisible(PELEMENT_SPRITE pelement,int state,int estate);
int		__cdecl CheckVisible(PELEMENT_SLIDER pelement,int state,int estate);

bool	__cdecl ChkColor(PELEMENT_ICON pelement,int st,double curst); 
bool	__cdecl ChkColor(PELEMENT_NEEDLE pelement,int st,double curst); 
bool	__cdecl ChkColor(PELEMENT_MOVING_IMAGE pelement,int st,double curst); 
bool	__cdecl ChkColor(PELEMENT_STRING pelement,int st,double curst); 
bool	__cdecl ChkColor(PELEMENT_SPRITE pelement,int st,double curst); 
bool	__cdecl ChkColor(PELEMENT_SLIDER pelement,int st,double curst); 

