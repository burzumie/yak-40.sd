/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: $

  Last modification:
    $Date: $
    $Revision: $
    $Author: $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include <stdio.h>
#include <algorithm>

#include "EString.h"

EString::EString()
{
}

EString::EString(const char *str)
{
	assign(str);
}

EString::EString(std::string str)
{
	assign(str);
}

EString::EString(char Ch)
{
	//clear();
	erase();
	append((char *)Ch);
	//push_back(Ch);
}

EString::EString(int FromInt)
{
	char buffer[20];

	_itoa_s(FromInt, buffer,19,10);

	assign(buffer);
}

EString::EString(size_t FromSizeT)
{
	*this = EString( static_cast<int>(FromSizeT) );
}

EString::EString(double FromDouble)
{
	EString Str(FromDouble, 3);

	assign(Str);
}

EString::EString(double FromDouble, int Digits)
{
	static const int BUFFER = 80;

	char buf[BUFFER];
	EString Format = "%."+EString(Digits)+"f";
	_snprintf_s(buf, BUFFER,BUFFER-1, Format, FromDouble);

	assign(buf);
}

EString::EString(const EStringBuffer& Buffer)
{
	assign( Buffer.FBuffer );
}

EStringBuffer EString::GetStringBuffer() const
{
	EStringBuffer Buffer;

	size_t Size = ( length() > BufferSize ) ? BufferSize-1 : length();

	memcpy( Buffer.FBuffer, c_str(), Size );

	Buffer.FBuffer[Size] = 0;

	return Buffer;
}                       

void EString::AppendInt(int I, int Format)
{
	EString Buff(I);

	size_t LeadZeroes = Format-Buff.length();

	for (size_t i=1; i<=LeadZeroes; ++i) 
		append("0");

	append(Buff);
}

void EString::AddTrailingChar(char Ch)
{
	if (data()[length()-1]==Ch) 
		return;

	append((char *)Ch);
	//push_back(Ch);
}

EString::operator const char*() const
{
	return c_str();
}

void EString::TrimSpaces()
{
	size_t i = 0;
	
	while ( i != length() && IsSeparator(at(i)) ) 
		++i;

	size_t j = length();
	while ( j != i && IsSeparator(at(j-1)) ) 
		--j;

	assign(substr(i, j-i));
}

void EString::TrimBrackets()
{
	EString T = *this;

	T.TrimSpaces();

	if ( T.size() < 2 ) 
		return;

	if ( T.at(0)=='"' && T.at(T.length()-1)=='"' ) {
		T = T.substr(1, T.length()-2);
		if ( T.find("\"") == T.npos ) 
			assign(T);
	}
}

void EString::ToUpper()
{
	for (size_t i = 0; i<length(); ++i ) {
		if ( CharIsLower(data()[i]) ) 
			at(i) -= ( 'a' - 'A' );
	}
}

bool EString::CharIsLower(char Ch)
{
	return ( (Ch >= 'a') && (Ch <= 'z') );
}

void EString::ReplaceAll(char OldChar, char NewChar)
{
	for (size_t i = 0; i<length(); ++i ) {
		if ( data()[i] == OldChar ) 
			at(i)=NewChar;
	}
}

bool EString::IsDigit(const char Ch)
{
	return ( (Ch >= '0') && (Ch <= '9') );
}

bool EString::IsSeparator(const char Ch)
{
	// NOTE: blistering fast 
	//       (compiled into 13 intructions via VC++ 7's native compiler)

	static const char Separators[]={ ' ', 9 };

	for (size_t i=0; i<sizeof(Separators); ++i) {
		if (Separators[i]==Ch) 
			return true;
	}

	return false;

	/*
	// NOTE: this piece of code uses STL gracefuly, but assembler output 
	//       i've got for it is insane:

	static const LString Separators = " \t";
	return std::find(Separators.begin(), Separators.end(), Ch) != Separators.end();
	*/
}

EString EString::ToUpper(const EString& Str)
{
	EString TempStr = Str;

	TempStr.ToUpper();

	return TempStr;
}

EString EString::ReplaceAll(const EString& Str, char OldChar, char NewChar)
{
	EString TempStr = Str;

	TempStr.ReplaceAll( OldChar, NewChar );

	return TempStr;
}

void EString::Fetch(const int Len)
{
	while ((int)length()<Len) 	append((char *)' ');
// push_back(' ');
}

EString EString::GetToken(const size_t Num) const
{
	size_t TokenNum = 0;
	size_t i = 0;
	size_t j = 0;

	while ( i != length() ) {
		// bypass spaces & delimiting chars
		while ( i != length() && IsSeparator(at(i)) ) 
			++i;

		if ( i == length() ) 
			return "";

		bool InsideQuotes = ( at(i) == '\"' );

		if ( InsideQuotes ) {
			// inside quotes
			j = ++i;                     // exclude first " from token
			while ( j != length() && at(j) != '\"' ) 
				j++;
		} else {
			// outside quotes
			j = i;
			while ( j != length() && !IsSeparator(at(j)) ) 
				j++;
		}
		// extract token
		if ( i != j ) {
			TokenNum++;
			if ( TokenNum == Num ) {
				EString TokenFound = substr(i, j-i);

				TokenFound.TrimSpaces();

				return TokenFound;
			}
			i = j;
			if ( i != length() ) 
				++i;    // exclude last " from token, handle EOL
		}
	}
	return EString("");
}

EString EString::GetFormattedString(const int ExpandZeroes, const int Num) const
{
	EString OutputString = *this;

	size_t Position = OutputString.find("%i");

	if (Position>0) {
		EString S = EString(Num);

		while (S.length()<(size_t)ExpandZeroes) 
			S = "0"+S;

		OutputString.erase(Position, 2);
		OutputString.insert(Position, S);
	}

	return OutputString;
}

void EString::pop_back()
{
	if ( !empty()) 
		assign( substr(0, length()-1 ) );
}

void EString::pop_front()
{
	if ( !empty()) 
		assign( substr(1, length()-1 ) );
}

