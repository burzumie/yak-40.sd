/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Lib/PanelsTools.h $

  Last modification:
    $Date: 19.02.06 9:19 $
    $Revision: 4 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once 

#include "../Common/CommonSys.h"

class CSimKey
{
private:
	friend bool FSAPI fnSimKeyHnd(ID32 event,UINT32 evdata,PVOID userdata);
	bool started;
public:
	CSimKey();
	virtual ~CSimKey();
	void StartKey();
	void StopKey();
	bool IsStarted(){return started;};
protected:
	virtual bool fnKeyHandler(ID32 event,UINT32 evdata)=NULL;
};

class CTimerMixin;

struct STimerInfo
{
	unsigned int  uUserID;   // user-assigned timer ID
	CTimerMixin  *pInstance; // pointer to CTimerMixin instance

	STimerInfo() : uUserID(0), pInstance(NULL) {};

	STimerInfo(unsigned int uID, CTimerMixin *pInst)
	{
		uUserID   = uID;
		pInstance = pInst;
	}
};

typedef map <unsigned int, UINT_PTR> mapUserID2SysID;  // map user-assigned timer IDs to IDs from SetTimer function
typedef map <UINT_PTR, STimerInfo>   mapSysID2Info;    // map timer IDs to timer-info structures

class CTimerMixin
{
public:
	CTimerMixin ();
	~CTimerMixin ();

	bool SetTimer      (unsigned int uID, unsigned int uTime);
	void KillTimer     (unsigned int uID);
	void KillAllTimers (void);

	virtual void OnTimer (unsigned int uID) = 0;


private:
	static mapSysID2Info   m_mapSysID2Info;
	mapUserID2SysID m_mapUserID2SysID;

	static void CALLBACK TimerProc (HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime);
};
