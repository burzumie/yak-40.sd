#pragma once 

#include <crtdbg.h>

template <class T>
class CDynaLinkResolver  
{
protected:
	T         m_pRef;
	HINSTANCE m_hLib;

public:

	CDynaLinkResolver() : m_pRef( NULL ), m_hLib( NULL )
	{
	}

	CDynaLinkResolver( LPCTSTR pszLib, LPCSTR pszName ) : m_pRef( NULL ), m_hLib( NULL )
	{
		Link( pszLib, pszName );
	}

	virtual ~CDynaLinkResolver()
	{
		Unlink();
	}

	BOOL IsValid()
	{ 
		return BOOL( m_pRef ); 
	}

	BOOL Link( LPCTSTR pszLib, LPCSTR pszName )
	{
		_ASSERTE( !m_hLib );
		m_hLib = ::LoadLibrary( pszLib );
		if( m_hLib )
		{
			if( !( m_pRef = (T) ::GetProcAddress( m_hLib, pszName ) ) )
				Unlink();
		}
		_ASSERTE( m_pRef ); // func isn't resolved OK
		return BOOL( m_pRef );
	}

	BOOL Unlink()
	{
		_ASSERTE( m_hLib );
		if( m_hLib )
		{
			BOOL res = ::FreeLibrary( m_hLib );
			_ASSERTE( res );
			m_pRef = NULL;
			m_hLib = NULL;
			return res;
		}
		return FALSE;
	}

	HINSTANCE GetInstance()
	{
		return m_hLib;
	}

	T Refer()
	{
		_ASSERTE(m_pRef);
		return m_pRef;
	}

	T operator()()
	{
		_ASSERTE(m_pRef);
		return m_pRef; 
	}
};

#ifndef stringer
#define stringer( x ) ( #x )
#endif

#define DYNA_LINK_NAME( funcname, libname, type ) \
	CDynaLinkResolver< type > funcname ( _T( #libname ), stringer( funcname ) );

#define DYNA_LINK_NAME_( objname, funcname, libname, type ) \
	CDynaLinkResolver< type > objname ( _T( #libname ), stringer( funcname ) );

#define DYNA_LINK_PTR( ptrname, type ) \
	CDynaLinkResolver< type > * ptrname = new CDynaLinkResolver< type >;

#define DYNA_LINK_PTR_( ptrname, funcname, libname, type ) \
	CDynaLinkResolver< type > * ptrname =                  \
	new CDynaLinkResolver< type > ( _T( #libname ), stringer( funcname ) );



