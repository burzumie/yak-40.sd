/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Lib/PanelsTools.cpp $

  Last modification:
    $Date: 18.02.06 11:43 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "PanelsTools.h"
#include "simtools.h"
#include "../api/ClientDef.h"
#include "../api/APIEntry.h"

void CheckBright(PELEMENT_ICON pelement,int state)
{
	if(POS_GET(POS_TIME_OF_DAY)==TIME_OF_DAY_DAY)DARKEN_IMAGE(pelement);
	else {
		if(state!=0) {
			LIGHT_IMAGE(pelement);
		} else DARKEN_IMAGE(pelement);
	}
}

void CheckBright(PELEMENT_NEEDLE pelement,int state)
{
	if(POS_GET(POS_TIME_OF_DAY)==TIME_OF_DAY_DAY)DARKEN_IMAGE(pelement);
	else {
		if(state!=0) {
			LIGHT_IMAGE(pelement);
		} else DARKEN_IMAGE(pelement);
	}
}

void CheckBright(PELEMENT_MOVING_IMAGE pelement,int state)
{
	if(POS_GET(POS_TIME_OF_DAY)==TIME_OF_DAY_DAY)DARKEN_IMAGE(pelement);
	else {
		if(state!=0) {
			LIGHT_IMAGE(pelement);
		} else DARKEN_IMAGE(pelement);
	}
}

void CheckBright(PELEMENT_STRING pelement,int state)
{
	if(POS_GET(POS_TIME_OF_DAY)==TIME_OF_DAY_DAY)DARKEN_IMAGE(pelement);
	else {
		if(state!=0) {
			LIGHT_IMAGE(pelement);
		} else DARKEN_IMAGE(pelement);
	}
}

void CheckBright(PELEMENT_SPRITE pelement,int state)
{
	if(POS_GET(POS_TIME_OF_DAY)==TIME_OF_DAY_DAY)DARKEN_IMAGE(pelement);
	else {
		if(state!=0) {
			LIGHT_IMAGE(pelement);
		} else DARKEN_IMAGE(pelement);
	}
}

void CheckBright(PELEMENT_SLIDER pelement,int state)
{
	if(POS_GET(POS_TIME_OF_DAY)==TIME_OF_DAY_DAY)DARKEN_IMAGE(pelement);
	else {
		if(state!=0) {
			LIGHT_IMAGE(pelement);
		} else DARKEN_IMAGE(pelement);
	}
}

int CheckVisible(PELEMENT_ICON pelement,int state,int estate)
{
	if(state==estate) {
		SHOW_IMAGE(pelement);
		return 0;
	} else {
		HIDE_IMAGE(pelement);
		return -1;
	}
}

int CheckVisible(PELEMENT_NEEDLE pelement,int state,int estate)
{
	if(state==estate) {
		SHOW_IMAGE(pelement);
		return 0;
	} else {
		HIDE_IMAGE(pelement);
		return -1;
	}
}

int CheckVisible(PELEMENT_MOVING_IMAGE pelement,int state,int estate)
{
	if(state==estate) {
		SHOW_IMAGE(pelement);
		return 0;
	} else {
		HIDE_IMAGE(pelement);
		return -1;
	}
}

int CheckVisible(PELEMENT_STRING pelement,int state,int estate)
{
	if(state==estate) {
		SHOW_IMAGE(pelement);
		return 0;
	} else {
		HIDE_IMAGE(pelement);
		return -1;
	}
}

int CheckVisible(PELEMENT_SPRITE pelement,int state,int estate)
{
	if(state==estate) {
		SHOW_IMAGE(pelement);
		return 0;
	} else {
		HIDE_IMAGE(pelement);
		return -1;
	}
}

int CheckVisible(PELEMENT_SLIDER pelement,int state,int estate)
{
	if(state==estate) {
		SHOW_IMAGE(pelement);
		return 0;
	} else {
		HIDE_IMAGE(pelement);
		return -1;
	}
}

bool ChkColor(PELEMENT_ICON pelement,int st,double curst) 
{ 
	if(curst==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

bool ChkColor(PELEMENT_NEEDLE pelement,int st,double curst) 
{ 
	if(curst==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

bool ChkColor(PELEMENT_MOVING_IMAGE pelement,int st,double curst) 
{ 
	if(curst==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

bool ChkColor(PELEMENT_STRING pelement,int st,double curst) 
{ 
	if(curst==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

bool ChkColor(PELEMENT_SPRITE pelement,int st,double curst) 
{ 
	if(curst==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

bool ChkColor(PELEMENT_SLIDER pelement,int st,double curst) 
{ 
	if(curst==st) {
		SHOW_IMAGE(pelement);
		return true;
	} else {
		HIDE_IMAGE(pelement);
		return false;
	}
}; 

