/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

Last modification:
$Date: 19.02.06 7:21 $
$Revision: 9 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "../Common/CommonSys.h"
#include "../Common/CommonSimconnect.h"
#include "../Common/Macros.h"
#include <tchar.h>

#ifdef _DEBUG
_CrtMemState memoryState; 
#endif

CSimConnect		*g_SimData=NULL;

static enum EVENT_SUBSCRIBE_TO_SYSTEM_EVENT_ID {
	EVENT_SIM_1SEC						,
	EVENT_SIM_4SEC						,
	EVENT_SIM_6HZ						,
	EVENT_SIM_CRASHED					,
	EVENT_SIM_CRASHRESET				,
	EVENT_FLIGHT_LOAD					,
	EVENT_FLIGHT_SAVE					,
	EVENT_SIM_FLIGHTPLAN_ACTIVATED		,
	EVENT_SIM_FLIGHTPLAN_DEACTIVATED	,
	EVENT_SIM_FRAME						,
	EVENT_SIM_PAUSE						,
	EVENT_SIM_PAUSED					,
	EVENT_SIM_PAUSEFRAME				,
	EVENT_SIM_POSITION_CHANGED			,
	EVENT_SIM_SIM						,
	EVENT_SIM_START						,
	EVENT_SIM_STOP						,
	EVENT_SIM_SOUND						,
	EVENT_SIM_UNPAUSED					,
	EVENT_SIM_VIEW						,
	EVENT_SIM_WEATHER_MODE_CHANGED		,
};

bool CreatePipe();
void ClosePipe();

extern void CALLBACK fnSimConnectDispatchExt(SIMCONNECT_RECV *pData,DWORD cbData,void *pContext)
{
	CSimConnect *pMyHandle=(CSimConnect *)pContext;

/*
	if(pMyHandle->m_ClearRequest) {
		SimConnect_ClearDataDefinition(pMyHandle->hSimConnect,DEFINITION_YAK40_1);
		pMyHandle->m_ClearRequest=false;		
	}

	if(pMyHandle->m_Defined) {
		for(size_t i=0;i<pMyHandle->m_Definitions.size();i++) {
			SimConnect_AddToDataDefinition(pMyHandle->hSimConnect,DEFINITION_YAK40_1,pMyHandle->m_Definitions[i].Name,pMyHandle->m_Definitions[i].Units,pMyHandle->m_Definitions[i].Type);
		}
		pMyHandle->m_Defined=false;
	}

	if(pMyHandle->m_Requested) {
		SimConnect_RequestDataOnSimObject(pMyHandle->hSimConnect,REQUEST_YAK40_1,DEFINITION_YAK40_1,SIMCONNECT_SIMOBJECT_TYPE_USER,SIMCONNECT_PERIOD_SIM_FRAME);
		pMyHandle->m_Requested=false;
	}
*/

	return pMyHandle->fnSimConnectDispatch(pData,cbData);
}

void CSimConnect::Init()
{
	m_QuitSignalCatch=false;

	m_SavedFileName[0]='\0';
	m_LoadedFileName[0]='\0';
	m_FlightPlanActivated[0]='\0';

	m_FlightPlanDeActivated=false;

	m_SimPaused=true;

	m_Crashed=false;
	m_CrashReset=false;

	m_SimFrameRate=0;
	m_SimSpeed=0;

	m_SimPauseFrameRate=0;
	m_SimPauseSpeed=0;

	if(SUCCEEDED(SimConnect_Open(&hSimConnect,"SD6015 Yak40 Module",NULL,0,0,0))) {
		PRINTINFO("Module Connected to Flight Simulator!\n","");   

		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_1SEC					,"1sec");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_4SEC					,"4sec");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_6HZ						,"6Hz");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_CRASHED					,"Crashed");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_CRASHRESET				,"CrashReset");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_FLIGHT_LOAD					,"FlightLoaded");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_FLIGHT_SAVE					,"FlightSaved");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_FLIGHTPLAN_ACTIVATED	,"FlightPlanActivated");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_FLIGHTPLAN_DEACTIVATED	,"FlightPlanDeactivated");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_FRAME					,"Frame");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_PAUSE					,"Pause");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_PAUSED					,"Paused");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_PAUSEFRAME				,"PauseFrame");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_POSITION_CHANGED		,"PositionChanged");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_SIM						,"Sim");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_START					,"SimStart");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_STOP					,"SimStop");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_SOUND					,"Sound");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_UNPAUSED				,"Unpaused");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_VIEW					,"View");
		SimConnect_SubscribeToSystemEvent(hSimConnect,EVENT_SIM_WEATHER_MODE_CHANGED	,"WeatherModeChanged");

		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"TIME OF DAY"								, "Enum"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"AMBIENT TEMPERATURE"						, "Celsius" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"ELECTRICAL BATTERY VOLTAGE"				, "Volts"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"ELECTRICAL BATTERY LOAD"					, "Amperes" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"ELECTRICAL TOTAL LOAD AMPS"				, "Amperes" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"ELECTRICAL MASTER BATTERY"				, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"NUMBER OF ENGINES"						, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG EXHAUST GAS TEMPERATURE:1"	, "Rankine" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG EXHAUST GAS TEMPERATURE:2"	, "Rankine" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG EXHAUST GAS TEMPERATURE:3"	, "Rankine" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG OIL PRESSURE:1"				, "Psi"		);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG OIL PRESSURE:2"				, "Psi"		);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG OIL PRESSURE:3"				, "Psi"		);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG OIL TEMPERATURE:1"			, "Celsius" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG OIL TEMPERATURE:2"			, "Celsius" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG OIL TEMPERATURE:3"			, "Celsius" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL PRESSURE:1"				, "Psi"		);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL PRESSURE:2"				, "Psi"		);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL PRESSURE:3"				, "Psi"		);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL VALVE:1"				, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL VALVE:2"				, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL VALVE:3"				, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL PUMP SWITCH:1"			, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL PUMP SWITCH:2"			, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL PUMP SWITCH:3"			, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL PUMP ON:1"				, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL PUMP ON:2"				, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"GENERAL ENG FUEL PUMP ON:3"				, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"AIRSPEED TRUE"							, "Knots"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"AIRSPEED INDICATED"						, "Knots"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"FUEL TANK LEFT MAIN QUANTITY"			, "Gallons"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"FUEL TANK RIGHT MAIN QUANTITY"			, "Gallons"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"DELTA HEADING RATE"						, "Radians per second"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"ADF SIGNAL:1"							, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"ADF SIGNAL:2"							, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"ADF RADIAL:1"							, "Degrees"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"ADF RADIAL:2"							, "Degrees"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"NAV CDI:1"								, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"NAV CDI:2"								, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"NAV GSI:1"								, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"NAV GSI:2"								, "Number"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT STROBE"							, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT PANEL"								, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT LANDING"							, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT TAXI"								, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT BEACON"							, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT NAV"								, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT LOGO"								, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT WING"								, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT RECOGNITION"						, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"LIGHT CABIN"								, "Bool"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"THROTTLE LOWER LIMIT"					, "Percent"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION COUNT"					, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"MAX GROSS WEIGHT"						, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"EMPTY WEIGHT"							, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:1"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:2"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:3"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:4"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:5"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:6"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:7"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:8"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:9"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:10"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:11"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:12"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:13"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:14"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:15"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:16"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:17"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:18"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:19"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:20"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:21"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:22"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:23"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:24"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:25"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:26"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:27"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:28"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:29"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:30"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:31"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_1,"PAYLOAD STATION WEIGHT:32"				, "Pounds"	);

		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"ELECTRICAL BATTERY VOLTAGE"				, "Volts"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"ELECTRICAL TOTAL LOAD AMPS"				, "Amperes" );
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"FUEL TANK LEFT MAIN QUANTITY"			, "Gallons"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"FUEL TANK RIGHT MAIN QUANTITY"			, "Gallons"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:1"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:2"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:3"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:4"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:5"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:6"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:7"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:8"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:9"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:10"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:11"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:12"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:13"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:14"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:15"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:16"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:17"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:18"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:19"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:20"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:21"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:22"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:23"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:24"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:25"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:26"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:27"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:28"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:29"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:30"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:31"				, "Pounds"	);
		SimConnect_AddToDataDefinition(hSimConnect,DEFINITION_YAK40_2,"PAYLOAD STATION WEIGHT:32"				, "Pounds"	);
	}
}

void CSimConnect::DeInit()
{
	SimConnect_Close(hSimConnect);
}

void CSimConnect::Update()
{
	SimConnect_CallDispatch(hSimConnect,fnSimConnectDispatchExt,this);
}

void CSimConnect::fnSimConnectDispatch(SIMCONNECT_RECV *pData,DWORD cbData)
{
	switch(pData->dwID) {
		case SIMCONNECT_RECV_ID_EVENT_FILENAME: {
			SIMCONNECT_RECV_EVENT_FILENAME *evt = (SIMCONNECT_RECV_EVENT_FILENAME*)pData;
			switch(evt->uEventID) {
				case EVENT_FLIGHT_SAVE:
					strcpy_s(m_SavedFileName,MAX_PATH,evt->szFileName);
				break;
				case EVENT_FLIGHT_LOAD:
					strcpy_s(m_LoadedFileName,MAX_PATH,evt->szFileName);
				break;
				case EVENT_SIM_FLIGHTPLAN_ACTIVATED:
					strcpy_s(m_FlightPlanActivated,MAX_PATH,evt->szFileName);
				break;
			}
		}break;

		case SIMCONNECT_RECV_ID_EVENT: {
			SIMCONNECT_RECV_EVENT *evt = (SIMCONNECT_RECV_EVENT*)pData;

			switch(evt->uEventID) {
				case EVENT_SIM_CRASHED:
					m_Crashed=true;
				break;
				case EVENT_SIM_CRASHRESET:
					m_CrashReset=true;
				break;
				case EVENT_SIM_FLIGHTPLAN_DEACTIVATED:
					m_FlightPlanDeActivated=true;
				break;
				case EVENT_SIM_PAUSE:
					m_SimPaused=(bool)evt->dwData;
				break;
				case EVENT_SIM_PAUSED:
					m_SimPaused=true;
				break;
				case EVENT_SIM_UNPAUSED:
					m_SimPaused=false;
				break;
				case EVENT_SIM_POSITION_CHANGED:
					m_PositionChanged=true;
				break;
				case EVENT_SIM_SIM:
					m_SimRunning=(bool)evt->dwData;
				break;
				case EVENT_SIM_START:
					SimConnect_RequestDataOnSimObject(hSimConnect,REQUEST_YAK40_1,DEFINITION_YAK40_1,SIMCONNECT_SIMOBJECT_TYPE_USER,SIMCONNECT_PERIOD_SIM_FRAME);
					SimConnect_RequestDataOnSimObject(hSimConnect,REQUEST_YAK40_2,DEFINITION_YAK40_2,SIMCONNECT_SIMOBJECT_TYPE_USER,SIMCONNECT_PERIOD_SIM_FRAME);
					m_SimRunning=true;
				break;
				case EVENT_SIM_STOP:
					m_SimRunning=false;
				break;
				case EVENT_SIM_SOUND:
					m_SoundMaster=(bool)evt->dwData;
				break;
				case EVENT_SIM_VIEW:
					m_ActiveViewMode=evt->dwData;
				break;
				case EVENT_SIM_WEATHER_MODE_CHANGED:
					m_WeatherModeChanged=true;
				break;
			}
		}break;

		case SIMCONNECT_RECV_ID_EVENT_FRAME: {
			SIMCONNECT_RECV_EVENT_FRAME  *evt = (SIMCONNECT_RECV_EVENT_FRAME *)pData;

			switch(evt->uEventID) {
				case EVENT_SIM_FRAME:
					m_SimFrameRate=evt->fFrameRate;
					m_SimSpeed=evt->fSimSpeed;
				break;
				case EVENT_SIM_PAUSEFRAME:
					m_SimPauseFrameRate=evt->fFrameRate;
					m_SimPauseSpeed=evt->fSimSpeed;
				break;
			}
		}break;

		case SIMCONNECT_RECV_ID_SIMOBJECT_DATA: {
			SIMCONNECT_RECV_SIMOBJECT_DATA *pObjData = (SIMCONNECT_RECV_SIMOBJECT_DATA*)pData;

			switch(pObjData->dwRequestID) {
				case REQUEST_YAK40_1: {
					PSIMCONNECT_PARAMS tmp=(PSIMCONNECT_PARAMS)&pObjData->dwData;
					memcpy(&m_In,tmp,sizeof(SIMCONNECT_PARAMS));
				}break;
				case REQUEST_YAK40_2: {
					PSIMCONNECT_PARAMS_WRITE tmp=(PSIMCONNECT_PARAMS_WRITE)&pObjData->dwData;
					if(m_LetSetValue) {
						void *tmp2=(void *)&m_Out;
						SimConnect_SetDataOnSimObject(hSimConnect,DEFINITION_YAK40_2,SIMCONNECT_OBJECT_ID_USER,0,0,sizeof(SIMCONNECT_PARAMS_WRITE),tmp2);
						m_LetSetValue=false;
					} else {
						memcpy(&m_Out,tmp,sizeof(SIMCONNECT_PARAMS_WRITE));
					}
				}break;
			}
		}break;

		case SIMCONNECT_RECV_ID_QUIT:
			m_QuitSignalCatch=true;
		break;
	}
}

void MainLoop()
{
	bool AlreadyRunning;

	HANDLE hMutexOneInstance=::CreateMutex(NULL,TRUE,_T("SD6015YAK40-848E6D9F-8A60-47ee-BB06-45B9D4F778F0"));

	AlreadyRunning=(GetLastError()==ERROR_ALREADY_EXISTS);

	if(hMutexOneInstance!=NULL) {
		::ReleaseMutex(hMutexOneInstance);
	}

	if(AlreadyRunning)
		return;

	if(!CreatePipe()) 
		return;

	while(0==g_SimData->m_QuitSignalCatch&&::FindWindowA("FS98MAIN", NULL)) {
		g_SimData->Update();
		Sleep(1);
	} 

	ClosePipe();
}

int __stdcall DLLStart(void)
{
	bool AlreadyRunning;

	HANDLE hMutexOneInstance=::CreateMutex(NULL,TRUE,_T("SD6015YAK40-848E6D9F-8A60-47ee-BB06-45B9D4F778F0"));

	AlreadyRunning=(GetLastError()==ERROR_ALREADY_EXISTS);

	if(hMutexOneInstance!=NULL) {
		::ReleaseMutex(hMutexOneInstance);
	}

	if(AlreadyRunning)
		return -2;

	if(!CreatePipe()) 
		return -1;

	g_SimData->Update();

	return 0;
}

//
// The DLLStop function must be present.
//
void __stdcall DLLStop(void)
{
	ClosePipe();
}
/*
int __cdecl _tmain(int argc, _TCHAR* argv[])
{
#ifdef _DEBUG
	_CrtMemCheckpoint(&memoryState);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
#endif
	MainLoop();
#ifdef _DEBUG
	CHECK_MEM_LEAK("C:\\MemLeaks\\Yak-40\\Module.log");
#endif
	return 0;
}
*/

HANDLE hFileMapping=NULL;

bool CreatePipe()
{
	DWORD size=sizeof(CSimConnect);

	hFileMapping=CreateFileMappingA(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,size,SIM_SHARE_NAME);

	if(hFileMapping==NULL) {
		return false;
	}

	g_SimData=(CSimConnect *)MapViewOfFile(hFileMapping,FILE_MAP_READ|FILE_MAP_WRITE,0,0,size);

	if(g_SimData==NULL) {
		return false;
	}

	g_SimData->Init();

	return true;
}

void ClosePipe()
{
	if(g_SimData) {	
		g_SimData->DeInit();
		UnmapViewOfFile(g_SimData);
		g_SimData=NULL;
	}
	if(hFileMapping) {
		CloseHandle(hFileMapping);
		hFileMapping=NULL;
	}
}
