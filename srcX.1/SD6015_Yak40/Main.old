/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

Last modification:
$Date: 19.02.06 7:21 $
$Revision: 9 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "../Common/CommonSys.h"
#include <tchar.h>
#include <SimConnect.h>
#include "../Common/CommonSimconnect.h"

#ifdef _DEBUG
#define PRINTINFO(str1,str2)	printf(str1,str2);
#else
#define PRINTINFO(str1,str2)	
#endif

int     quit=0;
HANDLE  hSimConnect=NULL;

SD6015_SIM *g_pSD6015_SIM=NULL;

CHAR lpFileShareName[]="$SD6015YAK40$";
HANDLE hFileMapping=NULL;

bool	MenuAdded=false;

static enum EVENT_SUBSCRIBE_TO_SYSTEM_EVENT_ID {
	EVENT_FLIGHT_LOAD,
	EVENT_FLIGHT_SAVE,
	EVENT_SIM_PAUSED,
	EVENT_SIM_UNPAUSED,
	EVENT_SIM_START,
	EVENT_SIM_STOP,
	EVENT_SIM_SOUND,
	EVENT_SIM_VIEW,
};

static enum DATA_DEFINE_ID {
	DEFINITION_TIME_OF_DAY,
	DEFINITION_PAYLOAD,
	DEFINITION_PAYLOAD_MAIN,
	DEFINITION_FUEL_QTY,
};

static enum DATA_REQUEST_ID {
	REQUEST_TIME_OF_DAY,
	REQUEST_PAYLOAD,
	REQUEST_PAYLOAD_MAIN,
	REQUEST_FUEL_QTY,
};

static enum GROUP_ID {
	GROUP_MENU
};

static enum EVENT_ID {
	EVENT_MENU_SD_ROOT	=EVENT_SIM_VIEW+1,
	EVENT_MENU_SD_CONFIG,	
	EVENT_MENU_SD_ABOUT	,
};

struct structTimeOfDay
{
	double  TimeOfDay;
};

struct structPayload
{
	double  StationPilot;
	double  StationCoPilot;
	double  StationEngeneer;
	double  StationRow1Seat1;
	double  StationRow1Seat2;
	double  StationRow1Seat3;
	double  StationRow2Seat1;
	double  StationRow2Seat2;
	double  StationRow2Seat3;
	double  StationRow3Seat1;
	double  StationRow3Seat2;
	double  StationRow3Seat3;
	double  StationRow4Seat1;
	double  StationRow4Seat2;
	double  StationRow4Seat3;
	double  StationRow5Seat1;
	double  StationRow5Seat2;
	double  StationRow5Seat3;
	double  StationRow6Seat1;
	double  StationRow6Seat2;
	double  StationRow6Seat3;
	double  StationRow7Seat1;
	double  StationRow7Seat2;
	double  StationRow7Seat3;
	double  StationRow8Seat1;
	double  StationRow8Seat2;
	double  StationRow8Seat3;
	double  StationRow9Seat1;
	double  StationRow9Seat2;
	double  StationRow9Seat3;
	double  StationCargo;
	double  StationEquip;
};

struct structPayloadMain
{
	double	PayloadCount;
	double	EmptyWeight;
	double	MaxGrossWeight;
};

struct structFuelTanks
{
	double	LeftQty;
	double	RightQty;
};

void ClearSD6015_SIM(PSD6015_SIM sim)
{
	sim->FlightLoaded=false;
	sim->FlightLoadedFileName[0]='\0';
	sim->FlightSaved=false;
	sim->FlightSavedFileName[0]='\0';
	sim->SimPaused=false;
	sim->SimLoading=false;
	sim->SimMasterSound=SIMCONNECT_SOUND_SYSTEM_EVENT_DATA_MASTER;
	sim->ActiveViewMode=SIMCONNECT_VIEW_SYSTEM_EVENT_DATA_COCKPIT_2D;
	sim->UpdatePayload=false;
	sim->UpdatePayloadSet=false;
	for(int i=0;i<SEAT_MAX;i++) {
		sim->PayloadStations[i][0]=0;
		sim->PayloadStations[i][1]=0;
		sim->PayloadStationsReq[i][0]=false;
		sim->PayloadStationsReq[i][1]=false;
	}
	sim->PayloadNumStations=0;
	sim->EmptyWeight=0;
	sim->MaxGrossWeight=0;
	sim->LeftTankQty=0;
	sim->RightTankQty=0;
	sim->LeftTankQtySet=0;
	sim->RightTankQtySet=0;
	sim->LeftTankQtySeted=false;
	sim->RightTankQtySeted=false;
	sim->ProcessFuel=false;
	sim->AddMenu=false;
	sim->ConfigRequest=false;
	sim->AboutRequest=false;
}

bool CreatePipe()
{
	DWORD size=sizeof(SD6015_SIM);

	hFileMapping=CreateFileMappingA(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,size,lpFileShareName);

	if(hFileMapping==NULL) {
		PRINTINFO("\nCreateFileMapping: Error %ld\n",GetLastError());
		return false;
	}

	g_pSD6015_SIM=(PSD6015_SIM)MapViewOfFile(hFileMapping,FILE_MAP_READ|FILE_MAP_WRITE,0,0,size);

	if(g_pSD6015_SIM==NULL) {
		PRINTINFO("\nMapViewOfFile: Error %ld\n",GetLastError());
		return false;
	} else {
		PRINTINFO("\nMapViewOfFile: %p",g_pSD6015_SIM);
	}

	ClearSD6015_SIM(g_pSD6015_SIM);

	return true;
}

void ClosePipe()
{
	if(g_pSD6015_SIM) {	
		UnmapViewOfFile(g_pSD6015_SIM);
		g_pSD6015_SIM=NULL;
	}
	if(hFileMapping) {
		CloseHandle(hFileMapping);
		hFileMapping=NULL;
	}
}

#define SET_STATION(ST,STS) /**/ \
	if(g_pSD6015_SIM->PayloadStationsReq[ST][1]) { \
		Payload->STS=g_pSD6015_SIM->PayloadStations[ST][1]; \
		g_pSD6015_SIM->PayloadStationsReq[ST][1]=false; \
		g_pSD6015_SIM->PayloadStations[ST][1]=0;	\
	}

void CALLBACK MyDispatchProc(SIMCONNECT_RECV *pData,DWORD cbData,void *pContext)
{
	structTimeOfDay		*TimeOfDay;
	structPayload		*Payload;
	structPayloadMain	*PayloadMain;
	structFuelTanks		*FuelTanks;
	HRESULT				hr;

	switch(pData->dwID) {
		case SIMCONNECT_RECV_ID_EVENT_FILENAME: {
			SIMCONNECT_RECV_EVENT_FILENAME *evt=(SIMCONNECT_RECV_EVENT_FILENAME*)pData;
			switch(evt->uEventID) {
				case EVENT_FLIGHT_LOAD:
					PRINTINFO("\nNew Flight Loaded: %s", evt->szFileName);
					if(g_pSD6015_SIM) {
						g_pSD6015_SIM->FlightLoaded=true;
						strcpy_s(g_pSD6015_SIM->FlightLoadedFileName,MAX_PATH,evt->szFileName);
					}
				break;
				case EVENT_FLIGHT_SAVE:
					PRINTINFO("\nNew Flight Saved: %s", evt->szFileName); 
					if(g_pSD6015_SIM) {
						g_pSD6015_SIM->FlightSaved=true;
						strcpy_s(g_pSD6015_SIM->FlightSavedFileName,MAX_PATH,evt->szFileName);
					}
				break;
				default:
				break;
			}
			break;
		}

		case SIMCONNECT_RECV_ID_EVENT: {
			SIMCONNECT_RECV_EVENT *evt = (SIMCONNECT_RECV_EVENT*)pData;
			
			switch(evt->uEventID) {
				case EVENT_SIM_PAUSED:
					g_pSD6015_SIM->SimPaused=1;
					PRINTINFO("\nSIM_PAUSED: %d", evt->dwData); 
				break;
				case EVENT_SIM_UNPAUSED:
					g_pSD6015_SIM->SimPaused=0;
					PRINTINFO("\nSIM_UNPAUSED: %d", evt->dwData); 
				break;
				case EVENT_SIM_START:
					g_pSD6015_SIM->SimLoading=0;
					PRINTINFO("\nSIM_START: %d", evt->dwData); 
				break;
				case EVENT_SIM_STOP:
					g_pSD6015_SIM->SimLoading=1;
					PRINTINFO("\nSIM_STOP: %d", evt->dwData); 
				break;
				case EVENT_SIM_SOUND:
					g_pSD6015_SIM->SimMasterSound=evt->dwData;
					PRINTINFO("\nSIM_SOUND: %d", evt->dwData); 
				break;
				case EVENT_SIM_VIEW:
					g_pSD6015_SIM->ActiveViewMode=evt->dwData;
					PRINTINFO("\nSIM_ACTIVE_VIEW_MODE: %d", evt->dwData); 
				break;
				case EVENT_MENU_SD_CONFIG:
					g_pSD6015_SIM->ConfigRequest=true;
					PRINTINFO("\nRequest config dialog", ""); 
				break;
				case EVENT_MENU_SD_ABOUT:
					g_pSD6015_SIM->AboutRequest=true;
					PRINTINFO("\nRequest about dialog", ""); 
				break;
				default:
				break;
			}

			break;
		}

		case SIMCONNECT_RECV_ID_SIMOBJECT_DATA: {
			SIMCONNECT_RECV_SIMOBJECT_DATA *pObjData = (SIMCONNECT_RECV_SIMOBJECT_DATA*)pData;

			switch(pObjData->dwRequestID) {
				case REQUEST_TIME_OF_DAY:
					TimeOfDay = (structTimeOfDay*)&pObjData->dwData;
					g_pSD6015_SIM->TimeOfDay=(DWORD)TimeOfDay->TimeOfDay;
				break;
				case REQUEST_PAYLOAD:
					if(g_pSD6015_SIM->UpdatePayload) {
						Payload = (structPayload*)&pObjData->dwData;
						g_pSD6015_SIM->PayloadStations[SEAT_PILOT		][0]=Payload->StationPilot;
						g_pSD6015_SIM->PayloadStations[SEAT_COPILOT		][0]=Payload->StationCoPilot;
						g_pSD6015_SIM->PayloadStations[SEAT_ENGINEER	][0]=Payload->StationEngeneer;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW1_SEAT1	][0]=Payload->StationRow1Seat1;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW1_SEAT2	][0]=Payload->StationRow1Seat2;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW1_SEAT3	][0]=Payload->StationRow1Seat3;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW2_SEAT1	][0]=Payload->StationRow2Seat1;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW2_SEAT2	][0]=Payload->StationRow2Seat2;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW2_SEAT3	][0]=Payload->StationRow2Seat3;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW3_SEAT1	][0]=Payload->StationRow3Seat1;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW3_SEAT2	][0]=Payload->StationRow3Seat2;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW3_SEAT3	][0]=Payload->StationRow3Seat3;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW4_SEAT1	][0]=Payload->StationRow4Seat1;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW4_SEAT2	][0]=Payload->StationRow4Seat2;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW4_SEAT3	][0]=Payload->StationRow4Seat3;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW5_SEAT1	][0]=Payload->StationRow5Seat1;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW5_SEAT2	][0]=Payload->StationRow5Seat2;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW5_SEAT3	][0]=Payload->StationRow5Seat3;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW6_SEAT1	][0]=Payload->StationRow6Seat1;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW6_SEAT2	][0]=Payload->StationRow6Seat2;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW6_SEAT3	][0]=Payload->StationRow6Seat3;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW7_SEAT1	][0]=Payload->StationRow7Seat1;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW7_SEAT2	][0]=Payload->StationRow7Seat2;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW7_SEAT3	][0]=Payload->StationRow7Seat3;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW8_SEAT1	][0]=Payload->StationRow8Seat1;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW8_SEAT2	][0]=Payload->StationRow8Seat2;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW8_SEAT3	][0]=Payload->StationRow8Seat3;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW9_SEAT1	][0]=Payload->StationRow9Seat1;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW9_SEAT2	][0]=Payload->StationRow9Seat2;
						g_pSD6015_SIM->PayloadStations[SEAT_ROW9_SEAT3	][0]=Payload->StationRow9Seat3;
						g_pSD6015_SIM->PayloadStations[SEAT_CARGO		][0]=Payload->StationCargo;
						g_pSD6015_SIM->PayloadStations[SEAT_EQUIP		][0]=Payload->StationEquip;

						if(g_pSD6015_SIM->UpdatePayloadSet) {
							Payload = (structPayload*)&pObjData->dwData;
							SET_STATION(SEAT_PILOT		,StationPilot)
							SET_STATION(SEAT_COPILOT	,StationCoPilot)
							SET_STATION(SEAT_ENGINEER	,StationEngeneer)
							SET_STATION(SEAT_ROW1_SEAT1	,StationRow1Seat1)
							SET_STATION(SEAT_ROW1_SEAT2	,StationRow1Seat2)
							SET_STATION(SEAT_ROW1_SEAT3	,StationRow1Seat3)
							SET_STATION(SEAT_ROW2_SEAT1	,StationRow2Seat1)
							SET_STATION(SEAT_ROW2_SEAT2	,StationRow2Seat2)
							SET_STATION(SEAT_ROW2_SEAT3	,StationRow2Seat3)
							SET_STATION(SEAT_ROW3_SEAT1	,StationRow3Seat1)
							SET_STATION(SEAT_ROW3_SEAT2	,StationRow3Seat2)
							SET_STATION(SEAT_ROW3_SEAT3	,StationRow3Seat3)
							SET_STATION(SEAT_ROW4_SEAT1	,StationRow4Seat1)
							SET_STATION(SEAT_ROW4_SEAT2	,StationRow4Seat2)
							SET_STATION(SEAT_ROW4_SEAT3	,StationRow4Seat3)
							SET_STATION(SEAT_ROW5_SEAT1	,StationRow5Seat1)
							SET_STATION(SEAT_ROW5_SEAT2	,StationRow5Seat2)
							SET_STATION(SEAT_ROW5_SEAT3	,StationRow5Seat3)
							SET_STATION(SEAT_ROW6_SEAT1	,StationRow6Seat1)
							SET_STATION(SEAT_ROW6_SEAT2	,StationRow6Seat2)
							SET_STATION(SEAT_ROW6_SEAT3	,StationRow6Seat3)
							SET_STATION(SEAT_ROW7_SEAT1	,StationRow7Seat1)
							SET_STATION(SEAT_ROW7_SEAT2	,StationRow7Seat2)
							SET_STATION(SEAT_ROW7_SEAT3	,StationRow7Seat3)
							SET_STATION(SEAT_ROW8_SEAT1	,StationRow8Seat1)
							SET_STATION(SEAT_ROW8_SEAT2	,StationRow8Seat2)
							SET_STATION(SEAT_ROW8_SEAT3	,StationRow8Seat3)
							SET_STATION(SEAT_ROW9_SEAT1	,StationRow9Seat1)
							SET_STATION(SEAT_ROW9_SEAT2	,StationRow9Seat2)
							SET_STATION(SEAT_ROW9_SEAT3	,StationRow9Seat3)
							SET_STATION(SEAT_CARGO		,StationCargo)
							SET_STATION(SEAT_EQUIP		,StationEquip)

							HRESULT hr = SimConnect_SetDataOnSimObject(hSimConnect, DEFINITION_PAYLOAD, SIMCONNECT_OBJECT_ID_USER, 0, 0, sizeof(structPayload), Payload);

							g_pSD6015_SIM->UpdatePayloadSet=false;
						}
					}

				break;
				case REQUEST_PAYLOAD_MAIN: {
					if(g_pSD6015_SIM->UpdatePayload) {
						PayloadMain = (structPayloadMain*)&pObjData->dwData;

						g_pSD6015_SIM->PayloadNumStations				=(DWORD)PayloadMain->PayloadCount;
						g_pSD6015_SIM->EmptyWeight						=PayloadMain->EmptyWeight;
						g_pSD6015_SIM->MaxGrossWeight					=PayloadMain->MaxGrossWeight;
					}
					if(g_pSD6015_SIM->AddMenu&&!MenuAdded) {
						hr = SimConnect_MenuAddItem		(hSimConnect, "Suprunov Design"		,			  EVENT_MENU_SD_ROOT	,6015);
						hr = SimConnect_MenuAddSubItem	(hSimConnect, EVENT_MENU_SD_ROOT	, "Config"	, EVENT_MENU_SD_CONFIG	,60152);
						hr = SimConnect_MenuAddSubItem	(hSimConnect, EVENT_MENU_SD_ROOT	, "About"	, EVENT_MENU_SD_ABOUT	,60153);
						MenuAdded=true;
					} else if(!g_pSD6015_SIM->AddMenu) {
						hr = SimConnect_MenuDeleteSubItem(hSimConnect,EVENT_MENU_SD_ROOT	,EVENT_MENU_SD_ABOUT);
						hr = SimConnect_MenuDeleteSubItem(hSimConnect,EVENT_MENU_SD_ROOT	,EVENT_MENU_SD_CONFIG);
						hr = SimConnect_MenuDeleteItem   (hSimConnect,EVENT_MENU_SD_ROOT  );
						MenuAdded=false;
					}
				}
				break;
				case REQUEST_FUEL_QTY: {
					//if(g_pSD6015_SIM->ProcessFuel) {
						FuelTanks = (structFuelTanks*)&pObjData->dwData;

						g_pSD6015_SIM->LeftTankQty			=FuelTanks->LeftQty;
						g_pSD6015_SIM->RightTankQty			=FuelTanks->RightQty;

						if(g_pSD6015_SIM->LeftTankQtySeted) {
							FuelTanks->LeftQty=g_pSD6015_SIM->LeftTankQtySet;
							g_pSD6015_SIM->LeftTankQtySet=0;
						}
						if(g_pSD6015_SIM->RightTankQtySeted) {
							FuelTanks->RightQty=g_pSD6015_SIM->RightTankQtySet;
							g_pSD6015_SIM->RightTankQtySet=0;
						}

						if(g_pSD6015_SIM->LeftTankQtySeted||g_pSD6015_SIM->RightTankQtySeted) {
							HRESULT hr = SimConnect_SetDataOnSimObject(hSimConnect, DEFINITION_FUEL_QTY, SIMCONNECT_OBJECT_ID_USER, 0, 0, sizeof(structFuelTanks), FuelTanks);
							g_pSD6015_SIM->LeftTankQtySeted=false;
							g_pSD6015_SIM->RightTankQtySeted=false;
						}

					//}
			   }
			   break;
			}
			break;
		}

		case SIMCONNECT_RECV_ID_QUIT: {
			quit=1;
			break;
		}

		default:
		break;
	}
}

void MainLoop()
{
	HRESULT hr;

	if(!CreatePipe()) 
		return;

	if(SUCCEEDED(SimConnect_Open(&hSimConnect,"SD6015 Yak40",NULL,0,0,0))) {
		PRINTINFO("\nConnected to Flight Simulator!","");   

		hr = SimConnect_SubscribeToSystemEvent(hSimConnect, EVENT_FLIGHT_LOAD	, "FlightLoaded");
		hr = SimConnect_SubscribeToSystemEvent(hSimConnect, EVENT_FLIGHT_SAVE	, "FlightSaved");
		hr = SimConnect_SubscribeToSystemEvent(hSimConnect, EVENT_SIM_PAUSED	, "Paused");
		hr = SimConnect_SubscribeToSystemEvent(hSimConnect, EVENT_SIM_UNPAUSED	, "Unpaused");
		hr = SimConnect_SubscribeToSystemEvent(hSimConnect, EVENT_SIM_START		, "SimStart");
		hr = SimConnect_SubscribeToSystemEvent(hSimConnect, EVENT_SIM_STOP		, "SimStop");
		hr = SimConnect_SubscribeToSystemEvent(hSimConnect, EVENT_SIM_SOUND		, "Sound");
		hr = SimConnect_SubscribeToSystemEvent(hSimConnect, EVENT_SIM_VIEW		, "View");

		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_TIME_OF_DAY, "Time of day", "Enum");

		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:1" , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:2" , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:3" , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:4" , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:5" , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:6" , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:7" , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:8" , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:9" , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:10", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:11", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:12", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:13", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:14", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:15", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:16", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:17", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:18", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:19", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:20", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:21", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:22", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:23", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:24", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:25", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:26", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:27", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:28", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:29", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:30", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:31", "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD	, "PAYLOAD STATION WEIGHT:32", "Pounds");

		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD_MAIN	, "PAYLOAD STATION COUNT"	 , "Number");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD_MAIN	, "EMPTY WEIGHT"			 , "Pounds");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_PAYLOAD_MAIN	, "MAX GROSS WEIGHT"		 , "Pounds");

		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_FUEL_QTY	, "FUEL TANK LEFT MAIN QUANTITY" , "Gallons");
		hr = SimConnect_AddToDataDefinition(hSimConnect, DEFINITION_FUEL_QTY	, "FUEL TANK RIGHT MAIN QUANTITY", "Gallons");

		hr = SimConnect_RequestDataOnSimObject(hSimConnect, REQUEST_TIME_OF_DAY	, DEFINITION_TIME_OF_DAY	, SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD_SECOND); 
		hr = SimConnect_RequestDataOnSimObject(hSimConnect, REQUEST_PAYLOAD		, DEFINITION_PAYLOAD		, SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD_SIM_FRAME); 
		hr = SimConnect_RequestDataOnSimObject(hSimConnect, REQUEST_PAYLOAD_MAIN, DEFINITION_PAYLOAD_MAIN	, SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD_SECOND); 
		hr = SimConnect_RequestDataOnSimObject(hSimConnect, REQUEST_FUEL_QTY	, DEFINITION_FUEL_QTY		, SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD_SIM_FRAME); 

		hr = SimConnect_MapClientEventToSimEvent(hSimConnect, EVENT_MENU_SD_CONFIG);
		hr = SimConnect_MapClientEventToSimEvent(hSimConnect, EVENT_MENU_SD_ABOUT);

		hr = SimConnect_AddClientEventToNotificationGroup(hSimConnect, GROUP_MENU, EVENT_MENU_SD_CONFIG);
		hr = SimConnect_AddClientEventToNotificationGroup(hSimConnect, GROUP_MENU, EVENT_MENU_SD_ABOUT);

		hr = SimConnect_SetNotificationGroupPriority(hSimConnect, GROUP_MENU, SIMCONNECT_GROUP_PRIORITY_HIGHEST);

		while(0==quit&&::FindWindowA("FS98MAIN", NULL)) {
			if(g_pSD6015_SIM->AddMenu)
				SimConnect_CallDispatch(hSimConnect,MyDispatchProc,NULL);
			Sleep(1);
		} 
		hr=SimConnect_Close(hSimConnect);
	}

	ClosePipe();
}

int __cdecl _tmain(int argc, _TCHAR* argv[])
{
	MainLoop();
	return 0;
}
