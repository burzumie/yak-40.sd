/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"

SIMPLE_GAUGE_VC(MISC01		,VC11_BCK_RADSCOBA_D					);
SIMPLE_GAUGE_VC(MISC02		,VC11_BCK_YARCHECAP_D					);
SIMPLE_GAUGE_VC(MISC03		,VC11_BCK_YARCHESIDE_D					);
SIMPLE_GAUGE_VC(lmp00		,VC11_LMP_SIDE_D						);

#define LAMP(N,I,L) /**/ \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) {\
	SHOW_LMP(L,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3); \
	return !LMPE_GET(L)?POS_GET(POS_PANEL_STATE)*2:POS_GET(POS_PANEL_STATE)*2+1; \
} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,2*PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define LAMPP(N,I,L) /**/ \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) {\
	return !POS_GET(L)?POS_GET(POS_PANEL_STATE)*2:POS_GET(POS_PANEL_STATE)*2+1; \
} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,2*PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

#define LAMPT(N,I,L) /**/ \
	static double FSAPI icb_##N(PELEMENT_ICON pelement) {\
	return !TBLE_GET(L)?POS_GET(POS_PANEL_STATE)*2:POS_GET(POS_PANEL_STATE)*2+1; \
} \
	extern PELEMENT_HEADER N##_list; \
	GAUGE_HEADER_FS700_EX(I##_SX, #N, &N##_list, 0, 0, 0, 0, 0, N); \
	MY_ICON2(N##Ico,I,NULL,0,0,icb_##N,2*PANEL_LIGHT_MAX, N); \
	MY_STATIC2(N##bg,N##_list,I,&l_##N##Ico, N);

LAMP(lmp01,VC11_LMP_XXGREEN_D_00,LMP_ENG1PK);
LAMP(lmp02,VC11_LMP_XXGREEN_D_00,LMP_ENG2PK);
LAMP(lmp03,VC11_LMP_XXGREEN_D_00,LMP_ENG3PK);
LAMP(lmp04,VC11_LMP_XXRED_D_00,LMP_STAIRS_DWN);
LAMP(lmp05,VC11_LMP_XXRED_D_00,LMP_INV_115V_FAIL);
LAMP(lmp06,VC11_LMP_XXYELLOW_D_00,LMP_GROUND_ELEC_CONNECTED);
LAMP(lmp07,VC11_LMP_XXGREEN_D_00,LMP_DIF_AIR_PRESS_DROP);
LAMP(lmp08,VC11_LMP_XXYELLOW_D_00,LMP_AIR_PRESSBCKCNTRUNITON);
LAMP(lmp09,VC11_LMP_XXRED_D_00,LMP_AIR_PRESS_COND_SYS_OFF);
LAMP(lmp10,VC11_LMP_XXGREEN_D_00,LMP_AIR_CONDSYSTURBOCOOLERON);
LAMP(lmp11,VC11_LMP_XXYELLOW_D_00,LMP_REVERSEON);
LAMP(lmp12,VC11_LMP_XXGREEN_D_00,LMP_REVERSEOFF);
LAMP(lmp13,VC11_LMP_XXGREEN_D_00,LMP_TOPL_LEV);
LAMP(lmp14,VC11_LMP_XXGREEN_D_00,LMP_TOPL_PRAV);
LAMP(lmp15,VC11_LMP_XXGREEN_D_00,LMP_OBYED);
LAMP(lmp16,VC11_LMP_XXGREEN_D_00,LMP_KOLTSEV);
LAMP(lmp17,VC11_LMP_XXGREEN_D_00,LMP_ACT_TEST_OK);
LAMP(lmp18,VC11_LMP_XXRED_D_00,LMP_ZAR_AVAR_TORM);
LAMP(lmp19,VC11_LMP_XXRED_D_00,LMP_ACT_TEST_FAIL);
LAMP(lmp20,VC11_LMP_XXGREEN_D_00,LMP_TRIM_ELERON);
LAMP(lmp21,VC11_LMP_XXGREEN_D_00,LMP_TRIM_RUDDER);
LAMP(lmp22,VC11_LMP_XXYELLOW_D_00,LMP_FIRE_SND_ALARM_OFF);
LAMPP(lmp23,VC11_LMP_RV3M_D_00,POS_RV3M_LAMP);
LAMPT(lmp24,VC11_LMP_XXGREEN_D_00,TBL_APU_ON_FIRE);
LAMP(lmp25,VC11_LMP_XXGREEN_D_00,LMP_ENG1_DEICE);
LAMP(lmp26,VC11_LMP_XXGREEN_D_00,LMP_ENG2_DEICE);
LAMP(lmp27,VC11_LMP_XXGREEN_D_00,LMP_ENG3_DEICE);

static double FSAPI icb_obsleft(PELEMENT_ICON pelement)
{
	CHK_BRT();
	double val=HND_GET(HND_OBS6);
	if(val>9)
		val=0;
	return val+POS_GET(POS_PANEL_STATE)*10; 
}

MAKE_ICB(icb_obsl1scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS61     	,4)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl2scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS62     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl3scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS63     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl1scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS61     	,4)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl2scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS62     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl3scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS63     	,10)} else {HIDE_IMAGE(pelement); return -1;})

double FSAPI icb_Ico(PELEMENT_ICON pelement) 
{
	CHK_BRT();
	return POS_GET(POS_PANEL_STATE); 
} 

double FSAPI icb_IcoLit(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return PWR_GET(PWR_KURSMP1)&&!POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):-1;
}

double FSAPI icb_IcoIntl(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return !PWR_GET(PWR_KURSMP1)&&POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):-1;
}

double FSAPI icb_IcoIntlLit(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return POS_GET(POS_PANEL_LANG)&&PWR_GET(PWR_KURSMP1)?POS_GET(POS_PANEL_STATE):-1;
}

BOOL FSAPI mcb_glt_obs_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int o1=(int)POS_GET(POS_OBS61);
	int o2=(int)POS_GET(POS_OBS62);
	int o3=(int)POS_GET(POS_OBS63);


	HND_DEC(HND_OBS6);

	if(!CFG_GET(CFG_OBS_ZERO_START)) {
		if(o3==1&&o2==0&&o1==0) {
			o1=3;
			o2=6;
			o3=0;
		} else {
			// OBS3
			o3--;
			if(o3<0) {
				o3=9;
				// OBS2
				o2--;
				if(o2<0) {
					o2=9;
					// OBS1
					o1--;
					if(o1<0) {
						o1=3;
					}
				}
			}
		}
	} else {
		if(o3==0&&o2==0&&o1==0) {
			o1=3;
			o2=5;
			o3=9;
		} else {
			// OBS3
			o3--;
			if(o3<0) {
				o3=9;
				// OBS2
				o2--;
				if(o2<0) {
					o2=9;
					// OBS1
					o1--;
					if(o1<0) {
						o1=3;
					}
				}
			}
		}
	}

	POS_SET(POS_OBS61,o1);
	POS_SET(POS_OBS62,o2);
	POS_SET(POS_OBS63,o3);

	return TRUE;
}

BOOL FSAPI mcb_glt_obs_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int o1=(int)POS_GET(POS_OBS61);
	int o2=(int)POS_GET(POS_OBS62);
	int o3=(int)POS_GET(POS_OBS63);

	HND_INC(HND_OBS6);

	if(!CFG_GET(CFG_OBS_ZERO_START)) {
		if(o3==0&&o2==6&&o1==3) {
			o1=0;
			o2=0;
			o3=1;
		} else {
			// OBS3
			o3++;
			if(o3>9) {
				o3=0;
				// OBS2
				o2++;
				if(o2>9) {
					o2=0;
					// OBS1
					o1++;
					if(o1>3) {
						o1=0;
					}
				}
			}
		}
	} else {
		if(o3==9&&o2==5&&o1==3) {
			o1=0;
			o2=0;
			o3=0;
		} else {
			// OBS3
			o3++;
			if(o3>9) {
				o3=0;
				// OBS2
				o2++;
				if(o2>9) {
					o2=0;
					// OBS1
					o1++;
					if(o1>3) {
						o1=0;
					}
				}
			}
		}
	}
	POS_SET(POS_OBS61,o1);
	POS_SET(POS_OBS62,o2);
	POS_SET(POS_OBS63,o3);

	return TRUE;
}

BOOL FSAPI mcb_obs1_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P24);
	return true;
}

static MAKE_TCB(tcb_01	,HND_TT(HND_OBS6			    ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p6_10,HELP_NONE,0,0)
MOUSE_PBOX(30,30,VC11_BCK_MPOBS1_D_SX,VC11_BCK_MPOBS1_D_SY,CURSOR_HAND,MOUSE_LR,mcb_obs1_big)
//MOUSE_TSHB(  "1", 252,  75, 90, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_obs_l,mcb_glt_obs_r)
MOUSE_END       

extern PELEMENT_HEADER p6_10_list; 
GAUGE_HEADER_FS700_EX(VC11_BCK_MPOBS1_D_SX, "p6_10", &p6_10_list, rect_p6_10, 0, 0, 0, 0, p6_10); 

MY_ICON2	(p6_10Scl3Lit      	, VC11_SCL_OBS3LIT_D_00		,NULL               ,      211,       35,icb_obsl3scll		,10* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl2Lit      	, VC11_SCL_OBS2LIT_D_00		,&l_p6_10Scl3Lit    ,      171,       35,icb_obsl2scll		,10* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl1Lit      	, VC11_SCL_OBS1LIT_D_00		,&l_p6_10Scl2Lit    ,      133,       35,icb_obsl1scll		, 4* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl3      	, VC11_SCL_OBS3_D_00			,&l_p6_10Scl1Lit    ,      211,       35,icb_obsl3scl		,10* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl2      	, VC11_SCL_OBS2_D_00			,&l_p6_10Scl3      	,      171,       35,icb_obsl2scl		,10* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl1      	, VC11_SCL_OBS1_D_00			,&l_p6_10Scl2      	,      133,       35,icb_obsl1scl		, 4* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10IcoIntlLit	, VC11_BCK_MPOBS1INTLLIT_D	,&l_p6_10Scl1      	,        0,        0,icb_IcoIntlLit		,    PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10IcoIntl		, VC11_BCK_MPOBS1INTL_D		,&l_p6_10IcoIntlLit	,        0,        0,icb_IcoIntl		,    PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10IcoLit		, VC11_BCK_MPOBS1LIT_D		,&l_p6_10IcoIntl	,        0,        0,icb_IcoLit			,    PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Ico			, VC11_BCK_MPOBS1_D			,&l_p6_10IcoLit		,        0,        0,icb_Ico			,    PANEL_LIGHT_MAX,p6_10)
MY_STATIC2	(p6_10bg,p6_10_list	, VC11_BCK_MPOBS1_D			,&l_p6_10Ico		,p6_10)

/*
static double FSAPI icb_obsleft1(PELEMENT_ICON pelement)
{
CHK_BRT();
double val=HND_GET(HND_OBS7);
if(val>9)
val=0;
return val+POS_GET(POS_PANEL_STATE)*10; 
}
*/

MAKE_ICB(icb_obsr1scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS71     	,4)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr2scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS72     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr3scll		,if( PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS73     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr1scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS71     	,4)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr2scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS72     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsr3scl		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS73     	,10)} else {HIDE_IMAGE(pelement); return -1;})

double FSAPI icb_IcoLit2(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return PWR_GET(PWR_KURSMP2)&&!POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):-1;
}

double FSAPI icb_IcoIntl2(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return !PWR_GET(PWR_KURSMP2)&&POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):-1;
}

double FSAPI icb_IcoIntlLit2(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return POS_GET(POS_PANEL_LANG)&&PWR_GET(PWR_KURSMP2)?POS_GET(POS_PANEL_STATE):-1;
}

BOOL FSAPI mcb_obs2_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P24);
	return true;
}

BOOL FSAPI mcb_glt_obs_l2(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int o1=(int)POS_GET(POS_OBS71);
	int o2=(int)POS_GET(POS_OBS72);
	int o3=(int)POS_GET(POS_OBS73);

	HND_DEC(HND_OBS7);

	if(!CFG_GET(CFG_OBS_ZERO_START)) {
		if(o3==1&&o2==0&&o1==0) {
			o1=3;
			o2=6;
			o3=0;
		} else {
			// OBS3
			o3--;
			if(o3<0) {
				o3=9;
				// OBS2
				o2--;
				if(o2<0) {
					o2=9;
					// OBS1
					o1--;
					if(o1<0) {
						o1=3;
					}
				}
			}
		}
	} else {
		if(o3==0&&o2==0&&o1==0) {
			o1=3;
			o2=5;
			o3=9;
		} else {
			// OBS3
			o3--;
			if(o3<0) {
				o3=9;
				// OBS2
				o2--;
				if(o2<0) {
					o2=9;
					// OBS1
					o1--;
					if(o1<0) {
						o1=3;
					}
				}
			}
		}
	}

	POS_SET(POS_OBS71,o1);
	POS_SET(POS_OBS72,o2);
	POS_SET(POS_OBS73,o3);

	return TRUE;
}

BOOL FSAPI mcb_glt_obs_r2(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int o1=(int)POS_GET(POS_OBS71);
	int o2=(int)POS_GET(POS_OBS72);
	int o3=(int)POS_GET(POS_OBS73);

	HND_INC(HND_OBS7);

	if(!CFG_GET(CFG_OBS_ZERO_START)) {
		if(o3==0&&o2==6&&o1==3) {
			o1=0;
			o2=0;
			o3=1;
		} else {
			// OBS3
			o3++;
			if(o3>9) {
				o3=0;
				// OBS2
				o2++;
				if(o2>9) {
					o2=0;
					// OBS1
					o1++;
					if(o1>3) {
						o1=0;
					}
				}
			}
		}
	} else {
		if(o3==9&&o2==5&&o1==3) {
			o1=0;
			o2=0;
			o3=0;
		} else {
			// OBS3
			o3++;
			if(o3>9) {
				o3=0;
				// OBS2
				o2++;
				if(o2>9) {
					o2=0;
					// OBS1
					o1++;
					if(o1>3) {
						o1=0;
					}
				}
			}
		}
	}

	POS_SET(POS_OBS71,o1);
	POS_SET(POS_OBS72,o2);
	POS_SET(POS_OBS73,o3);

	return TRUE;
}

static MAKE_TCB(p7_11_tcb_01	,HND_TT(HND_OBS7			    ))

static MOUSE_TOOLTIP_ARGS(p7_11_ttargs)
MAKE_TTA(p7_11_tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p7_11,HELP_NONE,0,0)
MOUSE_PBOX(30,30,VC11_BCK_MPOBS2_D_SX,VC11_BCK_MPOBS2_D_SY,CURSOR_HAND,MOUSE_LR,mcb_obs2_big)
//MOUSE_TSHB2(  "1", 192,  75,150, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_obs_l2,mcb_glt_obs_r2,p7_11)
MOUSE_END       

extern PELEMENT_HEADER p7_11_list; 
GAUGE_HEADER_FS700_EX(VC11_BCK_MPOBS2_D_SX, "p7_11", &p7_11_list, rect_p7_11, 0, 0, 0, 0, p7_11); 

MY_ICON2	(p7_11Scl3Lit      	, VC11_SCL_OBS3LIT_D_00		,NULL               ,      211,       35,icb_obsr3scll		,10* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl2Lit      	, VC11_SCL_OBS2LIT_D_00		,&l_p7_11Scl3Lit    ,      171,       35,icb_obsr2scll		,10* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl1Lit      	, VC11_SCL_OBS1LIT_D_00		,&l_p7_11Scl2Lit    ,      133,       35,icb_obsr1scll		, 4* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl3      	, VC11_SCL_OBS3_D_00			,&l_p7_11Scl1Lit    ,      211,       35,icb_obsr3scl		,10* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl2      	, VC11_SCL_OBS2_D_00			,&l_p7_11Scl3      	,      171,       35,icb_obsr2scl		,10* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Scl1      	, VC11_SCL_OBS1_D_00			,&l_p7_11Scl2      	,      133,       35,icb_obsr1scl		, 4* PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11IcoIntlLit	, VC11_BCK_MPOBS2INTLLIT_D	,&l_p7_11Scl1      	,        0,        0,icb_IcoIntlLit2		,    PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11IcoIntl		, VC11_BCK_MPOBS2INTL_D		,&l_p7_11IcoIntlLit	,        0,        0,icb_IcoIntl2		,    PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11IcoLit		, VC11_BCK_MPOBS2LIT_D		,&l_p7_11IcoIntl	,        0,        0,icb_IcoLit2			,    PANEL_LIGHT_MAX,p7_11)
MY_ICON2	(p7_11Ico			, VC11_BCK_MPOBS2_D			,&l_p7_11IcoLit		,        0,        0,icb_Ico			,    PANEL_LIGHT_MAX,p7_11)
MY_STATIC2	(p7_11bg,p7_11_list	, VC11_BCK_MPOBS2_D			,&l_p7_11Ico		,p7_11)

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER MPV01_list; 
GAUGE_HEADER_FS700_EX(VC11_BCK_MPHOUSING_D_SX, "MPV01", &MPV01_list, 0, 0, 0, 0, 0, MPV01); 

MY_ICON2	(MPV01IcoR			,VC11_BCK_MPHOUSING_D	,NULL		 ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, MPV01); 
MY_STATIC2  (MPV01bg,MPV01_list	,VC11_BCK_MPHOUSING_D	,&l_MPV01IcoR, MPV01);

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_glt_obs1(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		mcb_glt_obs_l(relative_point,mouse_flags);
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		mcb_glt_obs_r(relative_point,mouse_flags);
	}
	return TRUE;
}

static MAKE_TCB(mvp02_tcb_01	,HND_TT(HND_OBS6			    ))

static MOUSE_TOOLTIP_ARGS(MVP02_ttargs)
MAKE_TTA(mvp02_tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_MVP02,HELP_NONE,0,0)
MOUSE_TBOX2(  "1", 0,  0,VC11_KNB_OBS1_D_SX, VC11_KNB_OBS1_D_SY,CURSOR_HAND,MOUSE_DLR,mcb_glt_obs1,MVP02)
MOUSE_END       

extern PELEMENT_HEADER MPV02_list; 
GAUGE_HEADER_FS700_EX(VC11_KNB_OBS1_D_SX, "MPV02", &MPV02_list, rect_MVP02, 0, 0, 0, 0, MPV02); 

MY_ICON2	(MPV02IcoR			,VC11_KNB_OBS1_D	,NULL		 ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, MPV02); 
MY_STATIC2  (MPV02bg,MPV02_list	,VC11_KNB_OBS1_D	,&l_MPV02IcoR, MPV02);

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_glt_obs2(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		mcb_glt_obs_l2(relative_point,mouse_flags);
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		mcb_glt_obs_r2(relative_point,mouse_flags);
	}
	return TRUE;
}

static MAKE_TCB(mvp03_tcb_01	,HND_TT(HND_OBS7			    ))

static MOUSE_TOOLTIP_ARGS(MVP03_ttargs)
MAKE_TTA(mvp03_tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_MVP03,HELP_NONE,0,0)
MOUSE_TBOX2(  "1", 0,  0,VC11_KNB_OBS2_D_SX, VC11_KNB_OBS2_D_SY,CURSOR_HAND,MOUSE_DLR,mcb_glt_obs2,MVP02)
MOUSE_END       

extern PELEMENT_HEADER MPV03_list; 
GAUGE_HEADER_FS700_EX(VC11_KNB_OBS2_D_SX, "MPV03", &MPV03_list, rect_MVP03, 0, 0, 0, 0, MPV03); 

MY_ICON2	(MPV03IcoR			,VC11_KNB_OBS2_D	,NULL		 ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, MPV03); 
MY_STATIC2  (MPV03bg,MPV03_list	,VC11_KNB_OBS2_D	,&l_MPV03IcoR, MPV03);

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_glt_frq1_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV6_FREQ1);
	int f2=(int)POS_GET(POS_NAV6_FREQ2);
	int f3=(int)POS_GET(POS_NAV6_FREQ3);
	int f4=(int)POS_GET(POS_NAV6_FREQ4);
	int f5=(int)POS_GET(POS_NAV6_FREQ5);

	if(mouse_flags&MOUSE_LEFTSINGLE) {
		HND_DEC(HND_KMP1FREQ);

		if(f1==1&&f2==0&&f3==8) {
			f1=1;
			f2=1;
			f3=7;
		} else {
			// NAV3
			f3--;
			if(f3<0) {
				f3+=9;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		HND_INC(HND_KMP1FREQ);

		if(f1==1&&f2==1&&f3==7) {
			f1=1;
			f2=0;
			f3=8;
		} else {
			// NAV3
			f3++;
			if(f3>9) {
				f3=0;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}
	}

	POS_SET(POS_NAV6_FREQ1,f1);
	POS_SET(POS_NAV6_FREQ2,f2);
	POS_SET(POS_NAV6_FREQ3,f3);
	POS_SET(POS_NAV6_FREQ4,f4);
	POS_SET(POS_NAV6_FREQ5,f5);
	return TRUE;
}

static MAKE_TCB(mvp04_tcb_01	,HND_TT(HND_KMP1FREQ			    ))

static MOUSE_TOOLTIP_ARGS(MVP04_ttargs)
MAKE_TTA(mvp04_tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_MVP04,HELP_NONE,0,0)
MOUSE_TBOX2(  "1", 0,  0,VC11_KNB_FRQ1_D_SX, VC11_KNB_FRQ1_D_SY,CURSOR_HAND,MOUSE_DLR,mcb_glt_frq1_l,MVP04)
MOUSE_END       

extern PELEMENT_HEADER MPV04_list; 
GAUGE_HEADER_FS700_EX(VC11_KNB_FRQ1_D_SX, "MPV04", &MPV04_list, rect_MVP04, 0, 0, 0, 0, MPV04); 

MY_ICON2	(MPV04IcoR			,VC11_KNB_FRQ1_D	,NULL		 ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, MPV04); 
MY_STATIC2  (MPV04bg,MPV04_list	,VC11_KNB_FRQ1_D	,&l_MPV04IcoR, MPV04);

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_glt_frq1_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV6_FREQ1);
	int f2=(int)POS_GET(POS_NAV6_FREQ2);
	int f3=(int)POS_GET(POS_NAV6_FREQ3);
	int f4=(int)POS_GET(POS_NAV6_FREQ4);
	int f5=(int)POS_GET(POS_NAV6_FREQ5);

	if(mouse_flags&MOUSE_LEFTSINGLE) {
		HND_DEC(HND_KMP1FREQ2);

		if(f5==5) {
			f5=0;
		} else {
			f4--;
			if(f4<0)
				f4=9;
			f5=5;
		}
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		HND_INC(HND_KMP1FREQ2);

		if(f5==5) {
			f4++;
			if(f4>9)
				f4=0;
			f5=0;
		} else {
			f5=5;
		}
	}

	POS_SET(POS_NAV6_FREQ1,f1);
	POS_SET(POS_NAV6_FREQ2,f2);
	POS_SET(POS_NAV6_FREQ3,f3);
	POS_SET(POS_NAV6_FREQ4,f4);
	POS_SET(POS_NAV6_FREQ5,f5);
	return TRUE;
}

static MAKE_TCB(mvp05_tcb_01	,HND_TT(HND_KMP1FREQ2			    ))

static MOUSE_TOOLTIP_ARGS(MVP05_ttargs)
MAKE_TTA(mvp05_tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_MVP05,HELP_NONE,0,0)
MOUSE_TBOX2(  "1", 0,  0,VC11_KNB_FRQ2_D_SX, VC11_KNB_FRQ2_D_SY,CURSOR_HAND,MOUSE_DLR,mcb_glt_frq1_r,MVP05)
MOUSE_END       

extern PELEMENT_HEADER MPV05_list; 
GAUGE_HEADER_FS700_EX(VC11_KNB_FRQ2_D_SX, "MPV06", &MPV05_list, rect_MVP05, 0, 0, 0, 0, MPV05); 

MY_ICON2	(MPV05IcoR			,VC11_KNB_FRQ2_D	,NULL		 ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, MPV05); 
MY_STATIC2  (MPV05bg,MPV05_list	,VC11_KNB_FRQ2_D	,&l_MPV05IcoR, MPV05);

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_glt_frq2_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV7_FREQ1);
	int f2=(int)POS_GET(POS_NAV7_FREQ2);
	int f3=(int)POS_GET(POS_NAV7_FREQ3);
	int f4=(int)POS_GET(POS_NAV7_FREQ4);
	int f5=(int)POS_GET(POS_NAV7_FREQ5);

	if(mouse_flags&MOUSE_LEFTSINGLE) {
		HND_DEC(HND_KMP2FREQ);

		if(f1==1&&f2==0&&f3==8) {
			f1=1;
			f2=1;
			f3=7;
		} else {
			// NAV3
			f3--;
			if(f3<0) {
				f3=9;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		HND_INC(HND_KMP2FREQ);

		if(f1==1&&f2==1&&f3==7) {
			f1=1;
			f2=0;
			f3=8;
		} else {
			// NAV3
			f3++;
			if(f3>9) {
				f3=0;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}
	}

	POS_SET(POS_NAV7_FREQ1,f1);
	POS_SET(POS_NAV7_FREQ2,f2);
	POS_SET(POS_NAV7_FREQ3,f3);
	POS_SET(POS_NAV7_FREQ4,f4);
	POS_SET(POS_NAV7_FREQ5,f5);
	return TRUE;
}

static MAKE_TCB(mvp06_tcb_01	,HND_TT(HND_KMP2FREQ			    ))

static MOUSE_TOOLTIP_ARGS(MVP06_ttargs)
MAKE_TTA(mvp06_tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_MVP06,HELP_NONE,0,0)
MOUSE_TBOX2(  "1", 0,  0,VC11_KNB_FRQ1_D_SX, VC11_KNB_FRQ1_D_SY,CURSOR_HAND,MOUSE_DLR,mcb_glt_frq2_l,MVP06)
MOUSE_END       

extern PELEMENT_HEADER MPV06_list; 
GAUGE_HEADER_FS700_EX(VC11_KNB_FRQ1_D_SX, "MPV05", &MPV06_list, rect_MVP06, 0, 0, 0, 0, MPV06); 

MY_ICON2	(MPV06IcoR			,VC11_KNB_FRQ1_D	,NULL		 ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, MPV06); 
MY_STATIC2  (MPV06bg,MPV06_list	,VC11_KNB_FRQ1_D	,&l_MPV06IcoR, MPV06);

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_btn_kmp1ID(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	BTN_TGL(BTN_KMP1ID);
	return TRUE;
}

static MAKE_TCB(mvp07_tcb_01	,BTN_TT(BTN_KMP1ID			    ))

static MOUSE_TOOLTIP_ARGS(MVP07_ttargs)
MAKE_TTA(mvp07_tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_MVP07,HELP_NONE,0,0)
MOUSE_TBOX2(  "1", 0,  0,VC11_BTN_MPAUDIO1_D_SX, VC11_BTN_MPAUDIO1_D_SY,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1ID,MVP07)
MOUSE_END       

extern PELEMENT_HEADER MPV07_list; 
GAUGE_HEADER_FS700_EX(VC11_BTN_MPAUDIO1_D_SX, "MPV07", &MPV07_list, rect_MVP07, 0, 0, 0, 0, MPV07); 

MY_ICON2	(MPV07IcoR			,VC11_BTN_MPAUDIO1_D	,NULL		 ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, MPV07); 
MY_STATIC2  (MPV07bg,MPV07_list	,VC11_BTN_MPAUDIO1_D	,&l_MPV07IcoR, MPV07);

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_btn_kmp2ID(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	BTN_TGL(BTN_KMP2ID);
	return TRUE;
}

static MAKE_TCB(mvp08_tcb_01	,BTN_TT(BTN_KMP2ID			    ))

static MOUSE_TOOLTIP_ARGS(MVP08_ttargs)
MAKE_TTA(mvp08_tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_MVP08,HELP_NONE,0,0)
MOUSE_TBOX2(  "1", 0,  0,VC11_BTN_MPAUDIO2_D_SX, VC11_BTN_MPAUDIO2_D_SY,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2ID,MVP08)
MOUSE_END       

extern PELEMENT_HEADER MPV08_list; 
GAUGE_HEADER_FS700_EX(VC11_BTN_MPAUDIO2_D_SX, "MPV08", &MPV08_list, rect_MVP08, 0, 0, 0, 0, MPV08); 

MY_ICON2	(MPV08IcoR			,VC11_BTN_MPAUDIO2_D	,NULL		 ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, MPV08); 
MY_STATIC2  (MPV08bg,MPV08_list	,VC11_BTN_MPAUDIO2_D	,&l_MPV08IcoR, MPV08);

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_glt_frq2_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV7_FREQ1);
	int f2=(int)POS_GET(POS_NAV7_FREQ2);
	int f3=(int)POS_GET(POS_NAV7_FREQ3);
	int f4=(int)POS_GET(POS_NAV7_FREQ4);
	int f5=(int)POS_GET(POS_NAV7_FREQ5);

	if(mouse_flags&MOUSE_LEFTSINGLE) {
		HND_DEC(HND_KMP2FREQ2);

		if(f5==5) {
			f5=0;
		} else {
			f4--;
			if(f4<0)
				f4=9;
			f5=5;
		}
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		HND_INC(HND_KMP2FREQ2);

		if(f5==5) {
			f4++;
			if(f4>9)
				f4=0;
			f5=0;
		} else {
			f5=5;
		}
	}

	POS_SET(POS_NAV7_FREQ1,f1);
	POS_SET(POS_NAV7_FREQ2,f2);
	POS_SET(POS_NAV7_FREQ3,f3);
	POS_SET(POS_NAV7_FREQ4,f4);
	POS_SET(POS_NAV7_FREQ5,f5);
	return TRUE;
}

static MAKE_TCB(mvp09_tcb_01	,HND_TT(HND_KMP2FREQ2			    ))

static MOUSE_TOOLTIP_ARGS(MVP09_ttargs)
MAKE_TTA(mvp09_tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_MVP09,HELP_NONE,0,0)
MOUSE_TBOX2(  "1", 0,  0,VC11_KNB_FRQ2_D_SX, VC11_KNB_FRQ2_D_SY,CURSOR_HAND,MOUSE_DLR,mcb_glt_frq2_r,MVP09)
MOUSE_END       

extern PELEMENT_HEADER MPV09_list; 
GAUGE_HEADER_FS700_EX(VC11_KNB_FRQ2_D_SX, "MPV09", &MPV09_list, rect_MVP09, 0, 0, 0, 0, MPV09); 

MY_ICON2	(MPV09IcoR			,VC11_KNB_FRQ2_D	,NULL		 ,  0,  0,icb_Ico,PANEL_LIGHT_MAX, MPV09); 
MY_STATIC2  (MPV09bg,MPV09_list	,VC11_KNB_FRQ2_D	,&l_MPV09IcoR, MPV09);

#ifdef _DEBUG
DLLMAIND("F:\\MemLeaks\\Yak40\\VC11.log")
#else
DLLMAIN()
#endif

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&MISC01	),
		(&MISC02	),
		(&MISC03	),
		(&lmp00 	),
		(&lmp01 	),
		(&lmp02 	),
		(&lmp03 	),
		(&lmp04 	),
		(&lmp05 	),
		(&lmp06 	),
		(&lmp07 	),
		(&lmp08 	),
		(&lmp09 	),
		(&lmp10 	),
		(&lmp11 	),
		(&lmp12 	),
		(&lmp13 	),
		(&lmp14 	),
		(&lmp15 	),
		(&lmp16 	),
		(&lmp17 	),
		(&lmp18 	),
		(&lmp19 	),
		(&lmp20 	),
		(&lmp21 	),
		(&lmp22 	),
		(&lmp23 	),
		(&lmp24 	),
		(&lmp25 	),
		(&lmp26 	),
		(&lmp27 	),
		(&p6_10		),
		(&p6_11		),
		(&p7_11		),
		(&p7_12		),
		(&MPV01		),
		(&MPV02		),
		(&MPV03		),
		(&MPV04		),
		(&MPV05		),
		(&MPV06		),
		(&MPV07		),
		(&MPV08		),
		(&MPV09		),
		0					
	}											
};												
							
