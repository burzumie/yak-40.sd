/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"

SIMPLE_GAUGE_VC				(Bbk1front			,VC4_BBK1_FRONT_D			);
SIMPLE_GAUGE_VC				(Bbk1top			,VC4_BBK1_TOP_D				);
SIMPLE_GAUGE_VC				(Bck_swt_radalt		,VC4_BCK_SWT_RADALT_D		);
SIMPLE_GAUGE_VC				(Bolts1				,VC4_BCK_BOLTS1_D			);
SIMPLE_GAUGE_VC				(Bolts2				,VC4_BCK_BOLTS2_D			);
SIMPLE_GAUGE_VC				(Bolts3				,VC4_BCK_BOLTS3_D			);
SIMPLE_GAUGE_VC				(Bolts4				,VC4_BCK_BOLTS4_D			);
SIMPLE_GAUGE_VC				(Bolts5				,VC4_BCK_BOLTS5_D			);
SIMPLE_GAUGE_VC				(Bolts6				,VC4_BCK_BOLTS6_D			);
SIMPLE_GAUGE_VC				(Bolts7				,VC4_BCK_BOLTS7_D			);
SIMPLE_GAUGE_VC				(Glare1front		,VC4_GLARE1_FRONT_D			);
SIMPLE_GAUGE_VC				(Glare1top			,VC4_GLARE1_TOP_D			);
SIMPLE_GAUGE_VC				(Glare2front		,VC4_GLARE2_FRONT_D			);
SIMPLE_GAUGE_VC				(Glare2top			,VC4_GLARE2_TOP_D			);
SIMPLE_GAUGE_VC				(Mic_holder			,VC4_MICROPHONEHOLDER_D		);
SIMPLE_GAUGE_VC				(Microphone			,VC4_MICROPHONE_D			);
SIMPLE_GAUGE_VC				(Oxygen				,VC4_BCK_OXYGEN_D			);
SIMPLE_GAUGE_VC				(Radar_handle1		,VC4_RADAR_HANDLE1_D		);
SIMPLE_GAUGE_VC				(Sgu				,VC4_BCK_SGU_D				);
//SIMPLE_GAUGE_VC				(So72				,VC4_BCK_SO72_D				);
SIMPLE_GAUGE_VC				(Startup_axis		,VC4_STARTUP_AXIS_D			);
SIMPLE_GAUGE_VC				(Startup_handle		,VC4_STARTUP_HANDLE_D		);
SIMPLE_GAUGE_VC				(Startup_wall		,VC4_STARTUP_WALL_D			);
SIMPLE_GAUGE_VC_TGL_XML		(Startup_covins		,"SD6015 Startupcov"		,VC4_STARTUP_COVINS_D	,VC4_STARTUP_COVINS_D_SX	,VC4_STARTUP_COVINS_D_SY	,"");
SIMPLE_GAUGE_VC_TGL_XML		(Startup_covout		,"SD6015 Startupcov"		,VC4_STARTUP_COVOUT_D	,VC4_STARTUP_COVOUT_D_SX	,VC4_STARTUP_COVOUT_D_SY	,POS_TTGET(POS_GROUND_STARTUP));

double FSAPI icb_Ico(PELEMENT_ICON pelement) 
{
	return POS_GET(POS_PANEL_STATE); 
} 

static MAKE_MSCB(mcb_azs_startai9pwr	    ,AZS_TGL(AZS_STARTAI9PWR				))
static MAKE_MSCB(mcb_azs_startai9mode_cs	,AZS_SET(AZS_STARTAI9MODE				,3))
static MAKE_MSCB(mcb_azs_startai9mode_start	,AZS_SET(AZS_STARTAI9MODE				,1))
static MAKE_MSCB(mcb_azs_startai9mode_stop	,AZS_SET(AZS_STARTAI9MODE				,2))
static MAKE_MSCB(mcb_azs_startai9mode_cntr	,AZS_SET(AZS_STARTAI9MODE				,0))
static MAKE_MSCB(mcb_azs_startai25pwr	    ,AZS_TGL(AZS_STARTAI25PWR				))
static MAKE_MSCB(mcb_azs_startai25mode_cs	,AZS_SET(AZS_STARTAI25MODE				,1))
static MAKE_MSCB(mcb_azs_startai25mode_start,AZS_SET(AZS_STARTAI25MODE				,2))
static MAKE_MSCB(mcb_azs_startai25mode_stop	,AZS_SET(AZS_STARTAI25MODE				,3))
static MAKE_MSCB(mcb_azs_startai25mode_cntr	,AZS_SET(AZS_STARTAI25MODE				,0))
static MAKE_MSCB(mcb_azs_startai25sel1		,AZS_SET(AZS_STARTAI25SEL				,2))
static MAKE_MSCB(mcb_azs_startai25sel2		,AZS_SET(AZS_STARTAI25SEL				,1))
static MAKE_MSCB(mcb_azs_startai25sel3		,AZS_SET(AZS_STARTAI25SEL				,3))
static MAKE_MSCB(mcb_azs_startai25sel4		,AZS_SET(AZS_STARTAI25SEL				,0))
static MAKE_MSCB(mcb_btn_startai9		    ,PRS_BTN(BTN_STARTAI9   				))
static MAKE_MSCB(mcb_btn_stopai9		    ,PRS_BTN(BTN_STOPAI9  					))
static MAKE_MSCB(mcb_btn_startai25		    ,PRS_BTN(BTN_STARTAI25  				))
static MAKE_MSCB(mcb_btn_stopai25		    ,PRS_BTN(BTN_STOPAI25  					))

static MAKE_TCB(tcb_07	,AZS_TT(AZS_STARTAI9PWR			))
static MAKE_TCB(tcb_08	,AZS_TT(AZS_STARTAI9MODE		))
static MAKE_TCB(tcb_09	,AZS_TT(AZS_STARTAI25PWR		))
static MAKE_TCB(tcb_10	,AZS_TT(AZS_STARTAI25MODE		))
static MAKE_TCB(tcb_11	,AZS_TT(AZS_STARTAI25SEL		))
static MAKE_TCB(tcb_17	,BTN_TT(BTN_STARTAI9	        ))
static MAKE_TCB(tcb_18	,BTN_TT(BTN_STOPAI9				))
static MAKE_TCB(tcb_19	,BTN_TT(BTN_STARTAI25	        ))
static MAKE_TCB(tcb_20	,BTN_TT(BTN_STOPAI25	        ))

static MOUSE_TOOLTIP_ARGS(ttargs) 
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MAKE_TTA(tcb_20	)
MOUSE_TOOLTIP_ARGS_END 

MOUSE_BEGIN(rect_Startup,HELP_NONE,0,0) 
MOUSE_TBOX(  "1", 205,   10,155, 50,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9pwr)
MOUSE_TBOX(  "2", 280,   90, 30, 55,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9mode_cs)
MOUSE_TBOX(  "2", 347,   62, 30, 53,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9mode_start)
MOUSE_TBOX(  "2", 347,  116, 30, 53,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9mode_stop)
MOUSE_TBOX(  "2", 310,   80, 35, 75,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9mode_cntr)
MOUSE_TBOX(  "3",  40,   10,155, 50,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25pwr)
MOUSE_TBOX(  "4",   0,   90, 37, 55,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25mode_cs)
MOUSE_TBOX(  "4",  70,   60, 15, 53,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25mode_start)
MOUSE_TBOX(  "4",  70,  116, 15, 53,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25mode_stop)
MOUSE_TBOX(  "4",  40,   70, 30, 85,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25mode_cntr)
MOUSE_TBOX(  "5", 110,   57, 30, 43,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25sel1)
MOUSE_TBOX(  "5", 165,   70, 25, 90,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25sel2)
MOUSE_TBOX(  "5", 110,  130, 30, 43,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25sel3)
MOUSE_TBOX(  "5", 110,  105, 50, 25,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25sel4)
MOUSE_TBOX(  "6", 300,  180, 45, 45,CURSOR_HAND,MOUSE_DLR,mcb_btn_startai9)
MOUSE_TBOX(  "7", 200,  180, 45, 45,CURSOR_HAND,MOUSE_DLR,mcb_btn_stopai9)
MOUSE_TBOX(  "8", 115,  180, 45, 45,CURSOR_HAND,MOUSE_DLR,mcb_btn_startai25)
MOUSE_TBOX(  "9",  25,  180, 45, 45,CURSOR_HAND,MOUSE_DLR,mcb_btn_stopai25)
MOUSE_END 

extern PELEMENT_HEADER Startup_list; 
GAUGE_HEADER_FS700_EX(VC4_DET_STARTUP_D_SX, "Startup", &Startup_list, rect_Startup, 0, 0, 0, 0, Startup); 

MY_ICON2	(StartupIco				,VC4_DET_STARTUP_D	,NULL			,  0,  0,icb_Ico	,PANEL_LIGHT_MAX, Startup); 
MY_STATIC2  (Startupbg,Startup_list	,VC4_DET_STARTUP_D	,&l_StartupIco	, Startup);

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_so72control		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return BTN_GET(BTN_SO72CONTROL	)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72controll	,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return BTN_GET(BTN_SO72CONTROL	)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72id			,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return BTN_GET(BTN_SO72ID		)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72idl			,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return BTN_GET(BTN_SO72ID		)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72pwr			,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return AZS_GET(AZS_SO72PWR		)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72pwrl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return AZS_GET(AZS_SO72PWR		)+POS_GET(POS_PANEL_STATE)*2;});

static MAKE_ICB(icb_xpdr1     		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return HND_GET(HND_XPDR1		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr1lit  		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_XPDR1		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr2     		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return HND_GET(HND_XPDR2		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr2lit  		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_XPDR2		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr3     		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return HND_GET(HND_XPDR3		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr3lit  		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_XPDR3		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr4     		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return HND_GET(HND_XPDR4		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr4lit  		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_XPDR4		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr1scl  		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return POS_GET(POS_XPDR1		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr1litscl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_XPDR1		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr2scl  		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return POS_GET(POS_XPDR2		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr2litscl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_XPDR2		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr3scl  		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return POS_GET(POS_XPDR3		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr3litscl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_XPDR3		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr4scl  		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return POS_GET(POS_XPDR4		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr4litscl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_XPDR4		)+POS_GET(POS_PANEL_STATE)*10;})

static MAKE_ICB(icb_xpdrmayday 		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return AZS_GET(AZS_XPDRMAYDAY 	)+POS_GET(POS_PANEL_STATE)*3;})
static MAKE_ICB(icb_xpdrmaydayl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return AZS_GET(AZS_XPDRMAYDAY 	)+POS_GET(POS_PANEL_STATE)*3;})
static MAKE_ICB(icb_xpdrm      		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return GLT_GET(GLT_XPDRMODE   	)+POS_GET(POS_PANEL_STATE)*4;})
static MAKE_ICB(icb_xpdrml     		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return GLT_GET(GLT_XPDRMODE   	)+POS_GET(POS_PANEL_STATE)*4;})

double FSAPI icb_IcoE(PELEMENT_ICON pelement) 
{
	if(!POS_GET(POS_PANEL_LANG)||PWR_GET(PWR_SO72)) {
		HIDE_IMAGE(pelement);
		return -1;
	}
	SHOW_IMAGE(pelement);

	return POS_GET(POS_PANEL_STATE); 
} 

double FSAPI icb_IcoR(PELEMENT_ICON pelement) 
{
	if(POS_GET(POS_PANEL_LANG)||PWR_GET(PWR_SO72)) {
		HIDE_IMAGE(pelement);
		return -1;
	}
	SHOW_IMAGE(pelement);

	return POS_GET(POS_PANEL_STATE);
} 

double FSAPI icb_IcoEL(PELEMENT_ICON pelement) 
{
	if(!POS_GET(POS_PANEL_LANG)||!PWR_GET(PWR_SO72)) {
		HIDE_IMAGE(pelement);
		return -1;
	}
	SHOW_IMAGE(pelement);

	return POS_GET(POS_PANEL_STATE);
} 

double FSAPI icb_IcoRL(PELEMENT_ICON pelement) 
{
	if(POS_GET(POS_PANEL_LANG)||!PWR_GET(PWR_SO72)) {
		HIDE_IMAGE(pelement);
		return -1;
	}
	SHOW_IMAGE(pelement);

	return POS_GET(POS_PANEL_STATE);
} 

static MAKE_MSCB(mcb_azs_so72pwr		    ,AZS_TGL(AZS_SO72PWR    				))
static MAKE_MSCB(mcb_azs_so72emerg		    ,if(mouse_flags&MOUSE_RIGHTSINGLE){if(AZS_GET(AZS_XPDRMAYDAY)==0)AZS_SET(AZS_XPDRMAYDAY,1); else AZS_SET(AZS_XPDRMAYDAY,0);} else {if(AZS_GET(AZS_XPDRMAYDAY)==1)AZS_SET(AZS_XPDRMAYDAY,2); else if(AZS_GET(AZS_XPDRMAYDAY)==2)AZS_SET(AZS_XPDRMAYDAY,1);} )
static MAKE_MSCB(mcb_btn_so72control	    ,PRS_BTN(BTN_SO72CONTROL				))
static MAKE_MSCB(mcb_btn_so72id     	    ,PRS_BTN(BTN_SO72ID     				))

static BOOL FSAPI mcb_glt_xpdr1_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_DEC(POS_XPDR1);
	HND_DEC(HND_XPDR1);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr1_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_INC(POS_XPDR1);
	HND_INC(HND_XPDR1);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr2_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_DEC(POS_XPDR2);
	HND_DEC(HND_XPDR2);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr2_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_INC(POS_XPDR2);
	HND_INC(HND_XPDR2);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr3_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_DEC(POS_XPDR3);
	HND_DEC(HND_XPDR3);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr3_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_INC(POS_XPDR3);
	HND_INC(HND_XPDR3);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr4_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_DEC(POS_XPDR4);
	HND_DEC(HND_XPDR4);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr4_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_INC(POS_XPDR4);
	HND_INC(HND_XPDR4);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdrm_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	GLT_DEC(GLT_XPDRMODE);
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdrm_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	GLT_INC(GLT_XPDRMODE);
	return TRUE;
}

static MAKE_TCB(So72_tcb_01	,AZS_TT(AZS_SO72PWR				))
static MAKE_TCB(So72_tcb_02	,AZS_TT(AZS_XPDRMAYDAY			))
static MAKE_TCB(So72_tcb_03	,BTN_TT(BTN_SO72CONTROL			))
static MAKE_TCB(So72_tcb_04	,BTN_TT(BTN_SO72ID				))
static MAKE_TCB(So72_tcb_05	,HND_TT(HND_XPDR1		        ))
static MAKE_TCB(So72_tcb_06	,HND_TT(HND_XPDR2		        ))
static MAKE_TCB(So72_tcb_07	,HND_TT(HND_XPDR3		        ))
static MAKE_TCB(So72_tcb_08	,HND_TT(HND_XPDR4		        ))
static MAKE_TCB(So72_tcb_09	,GLT_TT(GLT_XPDRMODE	        ))

static MOUSE_TOOLTIP_ARGS(So72_ttargs) 
MAKE_TTA(So72_tcb_01	)
MAKE_TTA(So72_tcb_02	) 
MAKE_TTA(So72_tcb_03	)
MAKE_TTA(So72_tcb_04	)
MAKE_TTA(So72_tcb_05	)
MAKE_TTA(So72_tcb_06	)
MAKE_TTA(So72_tcb_07	) 
MAKE_TTA(So72_tcb_08	)
MAKE_TTA(So72_tcb_09	)
MOUSE_TOOLTIP_ARGS_END 

MOUSE_BEGIN(rect_So72,HELP_NONE,0,0) 
MOUSE_TBOX2(  "1", 376,   67, 42, 59,CURSOR_HAND,MOUSE_LR,mcb_azs_so72pwr,So72)
MOUSE_TBOX2(  "2",  47,  131, 63,112,CURSOR_HAND,MOUSE_LR,mcb_azs_so72emerg,So72)
MOUSE_TBOX2(  "3",  65,   68, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_btn_so72control,So72)
MOUSE_TBOX2(  "4", 370,  180, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_btn_so72id,So72)
MOUSE_TSVB2(  "5", 125,  178, 13, 83,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdr1_l,mcb_glt_xpdr1_r,So72)
MOUSE_TSVB2(  "6", 148,  178, 13, 83,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdr2_l,mcb_glt_xpdr2_r,So72)
MOUSE_TSVB2(  "7", 319,  178, 13, 83,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdr3_l,mcb_glt_xpdr3_r,So72)
MOUSE_TSVB2(  "8", 344,  178, 13, 83,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdr4_l,mcb_glt_xpdr4_r,So72)
MOUSE_TSHB2(  "9", 148,    0,180,105,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdrm_l,mcb_glt_xpdrm_r,So72)
MOUSE_END 

extern PELEMENT_HEADER So72_list; 
GAUGE_HEADER_FS700_EX(VC4_BCK_XDPRRUS_D_SX, "So72", &So72_list, rect_So72, 0, 0, 0, 0, So72); 

MY_ICON2	(p6_swt_xpdrmd			,VC4_SWT_XDPREMERG_D_00			,NULL              			,  47,  131,icb_xpdrmayday	, 3*PANEL_LIGHT_MAX, So72)
MY_ICON2	(p6_swt_xpdrmdl			,VC4_SWT_XDPREMERGLIT_D_00		,&l_p6_swt_xpdrmd			,  47,  131,icb_xpdrmaydayl	, 3*PANEL_LIGHT_MAX, So72)
MY_ICON2	(p6_azs_so72pwr			,VC4_SWT_XDPRPOWER_D_00			,&l_p6_swt_xpdrmdl			, 376,   67,icb_so72pwr    	, 2*PANEL_LIGHT_MAX, So72)
MY_ICON2	(p6_azs_so72pwrl		,VC4_SWT_XDPRPOWERLIT_D_00		,&l_p6_azs_so72pwr			, 376,   67,icb_so72pwrl    , 2*PANEL_LIGHT_MAX, So72)
MY_ICON2	(knb_xpdr1				,VC4_KNB_XDPR1_D_00				,&l_p6_azs_so72pwrl			, 125,  178,icb_xpdr1     	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(knb_xpdr1l				,VC4_KNB_XDPR1LIT_D_00			,&l_knb_xpdr1				, 125,  178,icb_xpdr1lit  	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(knb_xpdr2				,VC4_KNB_XDPR2_D_00				,&l_knb_xpdr1l				, 148,  178,icb_xpdr2     	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(knb_xpdr2l				,VC4_KNB_XDPR2LIT_D_00			,&l_knb_xpdr2				, 148,  178,icb_xpdr2lit  	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(knb_xpdr3				,VC4_KNB_XDPR3_D_00				,&l_knb_xpdr2l				, 319,  178,icb_xpdr3     	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(knb_xpdr3l				,VC4_KNB_XDPR3LIT_D_00			,&l_knb_xpdr3				, 319,  178,icb_xpdr3lit  	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(knb_xpdr4				,VC4_KNB_XDPR4_D_00				,&l_knb_xpdr3l				, 344,  178,icb_xpdr4     	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(knb_xpdr4l				,VC4_KNB_XDPR4LIT_D_00			,&l_knb_xpdr4				, 344,  178,icb_xpdr4lit  	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(scl_xpdr1				,VC4_SCL_XDPR1_D_00				,&l_knb_xpdr4l				, 185,  139,icb_xpdr1scl  	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(scl_xpdr1l				,VC4_SCL_XDPR1LIT_D_00			,&l_scl_xpdr1				, 185,  139,icb_xpdr1litscl	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(scl_xpdr2				,VC4_SCL_XDPR2_D_00				,&l_scl_xpdr1l				, 214,  139,icb_xpdr2scl  	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(scl_xpdr2l				,VC4_SCL_XDPR2LIT_D_00			,&l_scl_xpdr2				, 214,  139,icb_xpdr2litscl	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(scl_xpdr3				,VC4_SCL_XDPR3_D_00				,&l_scl_xpdr2l				, 239,  139,icb_xpdr3scl  	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(scl_xpdr3l				,VC4_SCL_XDPR3LIT_D_00			,&l_scl_xpdr3				, 239,  139,icb_xpdr3litscl	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(scl_xpdr4				,VC4_SCL_XDPR4_D_00				,&l_scl_xpdr3l				, 264,  139,icb_xpdr4scl  	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(scl_xpdr4l				,VC4_SCL_XDPR4LIT_D_00			,&l_scl_xpdr4				, 264,  139,icb_xpdr4litscl	,10*PANEL_LIGHT_MAX, So72)	
MY_ICON2	(So72IcoEL				,VC4_BCK_XDPRINTLLIT_D			,&l_scl_xpdr4l				,   0,    0,icb_IcoEL		,   PANEL_LIGHT_MAX, So72) 
MY_ICON2	(So72IcoE				,VC4_BCK_XDPRINTL_D				,&l_So72IcoEL				,   0,    0,icb_IcoE		,   PANEL_LIGHT_MAX, So72) 
MY_ICON2	(So72IcoRL				,VC4_BCK_XDPRRUSLIT_D			,&l_So72IcoE				,   0,    0,icb_IcoRL		,   PANEL_LIGHT_MAX, So72) 
MY_ICON2	(So72IcoR				,VC4_BCK_XDPRRUS_D				,&l_So72IcoRL				,   0,    0,icb_IcoR		,   PANEL_LIGHT_MAX, So72) 
MY_STATIC2  (So72bg,So72_list		,VC4_BCK_XDPRRUS_D				,&l_So72IcoR				, So72);

//////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
DLLMAIND("F:\\MemLeaks\\Yak40\\VC4.log")
#else
DLLMAIN()
#endif

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&Bbk1front				),
		(&Bbk1top				),
		(&Bck_swt_radalt		),
		(&Bolts1				),
		(&Bolts2				),        
		(&Bolts3				),        
		(&Bolts4				),        
		(&Bolts5				),        
		(&Bolts6				),        
		(&Bolts7				),        
		(&Glare1front			),        
		(&Glare1top				),        
		(&Glare2front			),        
		(&Glare2top				),        
		(&Mic_holder			),        
		(&Microphone			),        
		(&Oxygen				),        
		(&Radar_handle1			),        
		(&Sgu					),        
		(&So72					),        
		(&Startup				),            
		(&Startup_axis			),        
		(&Startup_handle		),        
		(&Startup_wall			),        
		(&Startup_covins		),
		(&Startup_covout		),
		0					
	}											
};												
							
