/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

#define OFFX	0
#define OFFY	470

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_lamp13			,SHOW_LMP(LMP_FIRE_SND_ALARM_OFF	,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo22			,SHOW_TBL(TBL_OUTER_MARKER			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo23			,SHOW_TBL(TBL_MIDDLE_MARKER			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo24			,SHOW_TBL(TBL_INNER_MARKER			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo25			,SHOW_TBL(TBL_APU_STARTER			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo26			,SHOW_TBL(TBL_APU_OIL_PRESS_NORM	,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo27			,SHOW_TBL(TBL_APU_FUEL_VALVE_OPEN	,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo28			,SHOW_TBL(TBL_APU_ON_FIRE			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo29			,SHOW_TBL(TBL_APU_RPM_NORM			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo30			,SHOW_TBL(TBL_APU_RPM_HIGH			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_ltstest			,SHOW_BTN(BTN_LIGHTS_TEST0			,2))
static MAKE_ICB(icb_sound_alarm_fire,SHOW_AZS(AZS_FIRE_SOUND_ALARM		,2))
static MAKE_ICB(icb_gear_warning	,SHOW_AZS(AZS_GEAR_WARNING			,2))
static MAKE_ICB(icb_auto_skid		,SHOW_AZS(AZS_AUTO_SKID				,2))
static MAKE_ICB(icb_agb_ruchn		,SHOW_AZS(AZS_ADI_BACKUP_POWER		,2))
static MAKE_ICB(icb_apu_pk9			,SHOW_AZS(AZS_APU_FUEL_VALVE  		,2))
static MAKE_ICB(icb_warn_lights		,SHOW_AZS(AZS_WARNING_LIGHTS		,2))
static MAKE_ICB(icb_emerge_exit		,SHOW_AZS(AZS_EMERG_EXIT_LIGHTS		,2))
static MAKE_ICB(icb_plafon_light	,SHOW_AZS(AZS_WHITE_DECK_FLOOD_LIGHT,2))
static MAKE_ICB(icb_red_light		,SHOW_AZS(AZS_RED_PANEL_LIGHT		,2))

static NONLINEARITY tbl_aputemp[]={
	{{ 403-OFFX,832-OFFY},   0,0},
	{{ 411-OFFX,792-OFFY}, 300,0},
	{{ 451-OFFX,792-OFFY}, 600,0},
	{{ 458-OFFX,828-OFFY}, 900,0}
};

static MAKE_NCB(icb_aputempD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_APU_TEMP);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_aputempM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_APU_TEMP);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_aputempP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_APU_TEMP);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_aputempR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_APU_TEMP);}else HIDE_IMAGE(pelement);return -1;)

// �������
static NONLINEARITY tbl_starter[]={
	{{398-OFFX,1088-OFFY},   0,0},
	{{402-OFFX,1083-OFFY},   1,0},
	{{409-OFFX,1075-OFFY},   2,0},
	{{416-OFFX,1070-OFFY},   3,0},
	{{423-OFFX,1067-OFFY},   4,0},
	{{431-OFFX,1066-OFFY},   5,0},
	{{465-OFFX,1085-OFFY},  10,0}
};				   

static MAKE_NCB(icb_starterD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_STARTER);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_starterM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_STARTER);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_starterP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_STARTER);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_starterR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_STARTER);}else HIDE_IMAGE(pelement);return -1;)

static MAKE_ICB(icb_crs_p0_01		,SHOW_POSM(POS_CRS0_01,1,0))
static MAKE_ICB(icb_yoke			,SHOW_POSM(POS_YOKE0_ICON,1,0))

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_plafon_light		,AZS_TGL(AZS_WHITE_DECK_FLOOD_LIGHT		))
static MAKE_MSCB(mcb_red_light			,AZS_TGL(AZS_RED_PANEL_LIGHT			))
static MAKE_MSCB(mcb_sound_alarm_fire	,AZS_TGL(AZS_FIRE_SOUND_ALARM			))
static MAKE_MSCB(mcb_gear_warning		,AZS_TGL(AZS_GEAR_WARNING				))
static MAKE_MSCB(mcb_auto_skid			,AZS_TGL(AZS_AUTO_SKID					))
static MAKE_MSCB(mcb_apu_pk9			,AZS_TGL(AZS_APU_FUEL_VALVE				))
static MAKE_MSCB(mcb_warn_lights		,AZS_TGL(AZS_WARNING_LIGHTS				))
static MAKE_MSCB(mcb_emerge_exit		,AZS_TGL(AZS_EMERG_EXIT_LIGHTS			))
static MAKE_MSCB(mcb_agb_ruchn			,AZS_TGL(AZS_ADI_BACKUP_POWER			))
static MAKE_MSCB(mcb_check_lamps		,PRS_BTN(BTN_LIGHTS_TEST0				)) 

static BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS0_01,0);
		POS_SET(POS_CRS0_02,0);
		POS_SET(POS_CRS0_03,0);
		POS_SET(POS_CRS0_04,0);
		POS_SET(POS_YOKE0_ICON,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p0_01(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS0_01,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P2);
		panel_window_close_ident(IDENT_P0);
		panel_window_open_ident(IDENT_P6);
	}
	return true;
}

static BOOL FSAPI mcb_yoke_icon(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_YOKE0_ICON,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_open_ident(IDENT_P2);
	}
	return true;
}

static MAKE_TCB(tcb_01	,POS_TT(POS_CRS0_01						))
static MAKE_TCB(tcb_02	,POS_TT(POS_YOKE0_ICON					))
static MAKE_TCB(tcb_03	,AZS_TT(AZS_WHITE_DECK_FLOOD_LIGHT		))
static MAKE_TCB(tcb_04	,AZS_TT(AZS_RED_PANEL_LIGHT				))
static MAKE_TCB(tcb_05	,AZS_TT(AZS_FIRE_SOUND_ALARM		    ))
static MAKE_TCB(tcb_06	,AZS_TT(AZS_GEAR_WARNING  				))
static MAKE_TCB(tcb_07	,AZS_TT(AZS_AUTO_SKID			        ))
static MAKE_TCB(tcb_08	,AZS_TT(AZS_APU_FUEL_VALVE				))
static MAKE_TCB(tcb_09	,AZS_TT(AZS_WARNING_LIGHTS				))
static MAKE_TCB(tcb_10	,AZS_TT(AZS_EMERG_EXIT_LIGHTS			))
static MAKE_TCB(tcb_11	,AZS_TT(AZS_ADI_BACKUP_POWER			))
static MAKE_TCB(tcb_12	,LMP_TT(LMP_FIRE_SND_ALARM_OFF			))
static MAKE_TCB(tcb_13	,TBL_TT(TBL_OUTER_MARKER				))
static MAKE_TCB(tcb_14	,TBL_TT(TBL_MIDDLE_MARKER				))
static MAKE_TCB(tcb_15	,TBL_TT(TBL_INNER_MARKER				))
static MAKE_TCB(tcb_16	,TBL_TT(TBL_APU_STARTER					))
static MAKE_TCB(tcb_17	,TBL_TT(TBL_APU_OIL_PRESS_NORM			))
static MAKE_TCB(tcb_18	,TBL_TT(TBL_APU_FUEL_VALVE_OPEN			))
static MAKE_TCB(tcb_19	,TBL_TT(TBL_APU_ON_FIRE					))
static MAKE_TCB(tcb_20	,TBL_TT(TBL_APU_RPM_NORM				))
static MAKE_TCB(tcb_21	,TBL_TT(TBL_APU_RPM_HIGH				))
static MAKE_TCB(tcb_22  ,NDL_TT(NDL_APU_TEMP			        ))
static MAKE_TCB(tcb_23  ,NDL_TT(NDL_STARTER						))
static MAKE_TCB(tcb_24	,BTN_TT(BTN_LIGHTS_TEST0				))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MAKE_TTA(tcb_20	)
MAKE_TTA(tcb_21	)
MAKE_TTA(tcb_22	)
MAKE_TTA(tcb_23	)
MAKE_TTA(tcb_24	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_2,HELP_NONE,0,0)

// �������� ��������
MOUSE_PBOX(0,0,P0_BACKGROUND2_D_SX,P0_BACKGROUND2_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

// �������
MOUSE_TBOX(  "1",   10-OFFX, 1106-OFFY,MISC_CRS_P0_01_SX,MISC_CRS_P0_01_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p0_01)
//MOUSE_TBOX(  "2",  135-OFFX, 1140-OFFY,MISC_YOKE_SX     ,MISC_YOKE_SY     ,CURSOR_HAND,MOUSE_MLR,mcb_yoke_icon)

MOUSE_TTPB( "22",  389-OFFX, 774-OFFY, 100, 100)    // NDL_APU_TEMP			        
MOUSE_TTPB( "23",  387-OFFX,1056-OFFY, 100, 100)    // NDL_STARTER			        

// ��������
MOUSE_TBOX(  "3",  281-OFFX, 1066-OFFY,P0_AZS_PLAFONLIGHT_D_00_SX		,P0_AZS_PLAFONLIGHT_D_00_SY		,CURSOR_HAND,MOUSE_LR,mcb_plafon_light)
MOUSE_TBOX(  "4",  321-OFFX, 1066-OFFY,P0_AZS_REDLIGHT_D_00_SX			,P0_AZS_REDLIGHT_D_00_SY		,CURSOR_HAND,MOUSE_LR,mcb_red_light)
MOUSE_TBOX(  "5",  189-OFFX,  933-OFFY,P0_AZS_FIREALARMSOUND_D_00_SX	,P0_AZS_FIREALARMSOUND_D_00_SY	,CURSOR_HAND,MOUSE_LR,mcb_sound_alarm_fire)
MOUSE_TBOX(  "6",  233-OFFX,  932-OFFY,P0_AZS_GEARALARMSOUND_D_00_SX	,P0_AZS_GEARALARMSOUND_D_00_SY	,CURSOR_HAND,MOUSE_LR,mcb_gear_warning)
MOUSE_TBOX(  "7",  283-OFFX,  932-OFFY,P0_AZS_AUTODEBRAKE_D_00_SX		,P0_AZS_AUTODEBRAKE_D_00_SY		,CURSOR_HAND,MOUSE_LR,mcb_auto_skid)
MOUSE_TBOX(  "8",  368-OFFX,  931-OFFY,P0_AZS_APUVALVE_D_00_SX			,P0_AZS_APUVALVE_D_00_SY		,CURSOR_HAND,MOUSE_LR,mcb_apu_pk9)
MOUSE_TBOX(  "9",  402-OFFX,  933-OFFY,P0_AZS_TABLODAYNIGHT_D_00_SX		,P0_AZS_TABLODAYNIGHT_D_00_SY	,CURSOR_HAND,MOUSE_LR,mcb_warn_lights)
MOUSE_TBOX( "10",  442-OFFX,  933-OFFY,P0_AZS_EXITTABLO_D_00_SX			,P0_AZS_EXITTABLO_D_00_SY		,CURSOR_HAND,MOUSE_LR,mcb_emerge_exit)
MOUSE_TBOX( "11",  317-OFFX,  932-OFFY,P0_AZS_AGBMANUAL_D_00_SX			,P0_AZS_AGBMANUAL_D_00_SY		,CURSOR_HAND,MOUSE_LR,mcb_agb_ruchn)

// ������      
MOUSE_TBOX( "24",  389-OFFX,  702-OFFY, 32, 29,CURSOR_HAND,MOUSE_DLR,mcb_check_lamps)

// �����
MOUSE_TTPB( "12",  133-OFFX,  936-OFFY, 55,56)	// LMP_FIRE_SND_ALARM_OFF      		

// �����            
MOUSE_TTPB( "13",  425-OFFX,  569-OFFY, 63,49)    // TBL_OUTER_MARKER              		
MOUSE_TTPB( "14",  425-OFFX,  618-OFFY, 63,33)    // TBL_MIDDLE_MARKER             		
MOUSE_TTPB( "15",  425-OFFX,  651-OFFY, 63,49)    // TBL_INNER_MARKER              		
MOUSE_TTPB( "16",  287-OFFX,  745-OFFY, 75,57)    // TBL_APU_STARTER               		
MOUSE_TTPB( "17",  321-OFFX,  802-OFFY, 55,40)    // TBL_APU_OIL_PRESS_NORM        		
MOUSE_TTPB( "18",  321-OFFX,  842-OFFY, 55,45)    // TBL_APU_FUEL_VALVE_OPEN       		
MOUSE_TTPB( "19",  269-OFFX,  802-OFFY, 52,40)    // TBL_APU_ON_FIRE               		
MOUSE_TTPB( "20",  269-OFFX,  842-OFFY, 52,45)    // TBL_APU_RPM_NORM              		
MOUSE_TTPB( "21",  210-OFFX,  831-OFFY, 59,51)    // TBL_APU_RPM_HIGH              		

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p0_2_list; 
GAUGE_HEADER_FS700_EX(P0_BACKGROUND2_D_SX, "p0_2", &p0_2_list, rect_p0_2, 0, 0, 0, 0, p0_2); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_yoke			,MISC_YOKE					,NULL					, 135-OFFX, 1140-OFFY,			icb_yoke				,1					, p0_2)	
MY_ICON2	(p0_crs_01			,MISC_CRS_P0_01				,&l_p0_yoke				,  10-OFFX, 1106-OFFY,			icb_crs_p0_01			,1					, p0_2)	
MY_ICON2	(p0_lamp13			,P0_LMP_13_D_00				,&l_p0_crs_01			, 133-OFFX,  936-OFFY,			icb_lamp13				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo22			,P0_TBL_22_D_00				,&l_p0_lamp13			, 425-OFFX,  569-OFFY,			icb_tablo22				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo23			,P0_TBL_23_D_00				,&l_p0_tablo22			, 425-OFFX,  618-OFFY,			icb_tablo23				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo24			,P0_TBL_24_D_00				,&l_p0_tablo23			, 425-OFFX,  651-OFFY,			icb_tablo24				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo25			,P0_TBL_25_D_00				,&l_p0_tablo24			, 287-OFFX,  745-OFFY,			icb_tablo25				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo26			,P0_TBL_26_D_00				,&l_p0_tablo25			, 321-OFFX,  802-OFFY,			icb_tablo26				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo27			,P0_TBL_27_D_00				,&l_p0_tablo26			, 321-OFFX,  842-OFFY,			icb_tablo27				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo28			,P0_TBL_28_D_00				,&l_p0_tablo27			, 269-OFFX,  802-OFFY,			icb_tablo28				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo29			,P0_TBL_29_D_00				,&l_p0_tablo28			, 269-OFFX,  842-OFFY,			icb_tablo29				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo30			,P0_TBL_30_D_00				,&l_p0_tablo29			, 210-OFFX,  831-OFFY,			icb_tablo30				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(sound_alarm_fire	,P0_AZS_FIREALARMSOUND_D_00	,&l_p0_tablo30			, 188-OFFX,  936-OFFY,			icb_sound_alarm_fire	,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(gear_warning		,P0_AZS_GEARALARMSOUND_D_00	,&l_sound_alarm_fire	, 221-OFFX,  936-OFFY,			icb_gear_warning		,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(auto_skid			,P0_AZS_AUTODEBRAKE_D_00	,&l_gear_warning		, 282-OFFX,  936-OFFY,			icb_auto_skid			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(agb_ruchn			,P0_AZS_AGBMANUAL_D_00		,&l_auto_skid			, 316-OFFX,  936-OFFY,			icb_agb_ruchn			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(apu_pk9			,P0_AZS_APUVALVE_D_00		,&l_agb_ruchn			, 372-OFFX,  936-OFFY,			icb_apu_pk9				,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(warn_lights		,P0_AZS_TABLODAYNIGHT_D_00	,&l_apu_pk9				, 400-OFFX,  936-OFFY,			icb_warn_lights			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(emerge_exit		,P0_AZS_EXITTABLO_D_00		,&l_warn_lights			, 437-OFFX,  936-OFFY,			icb_emerge_exit			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(plafon_light		,P0_AZS_PLAFONLIGHT_D_00	,&l_emerge_exit			, 273-OFFX, 1067-OFFY,			icb_plafon_light		,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(red_light			,P0_AZS_REDLIGHT_D_00		,&l_plafon_light		, 312-OFFX, 1067-OFFY,			icb_red_light			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(ltstest			,P0_BTN_LTSTEST_D_00		,&l_red_light			, 389-OFFX,  702-OFFY,			icb_ltstest				,2*PANEL_LIGHT_MAX	, p0_2)	
MY_ICON2    (cov_p0_ai9_temp    ,P0_COV_APUTEMP_D			,&l_ltstest				, 413-OFFX,  799-OFFY,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2)	    
MY_NEEDLE2  (ndl_p0_ai9_tempD   ,P0_NDL_APUTEMP_D			,&l_cov_p0_ai9_temp		, 431-OFFX,  816-OFFY, 2, 4,	icb_aputempD			,tbl_aputemp,10		, p0_2) 
MY_NEEDLE2  (ndl_p0_ai9_tempM   ,P0_NDL_APUTEMP_M			,&l_ndl_p0_ai9_tempD	, 431-OFFX,  816-OFFY, 2, 4,	icb_aputempM			,tbl_aputemp,10		, p0_2) 
MY_NEEDLE2  (ndl_p0_ai9_tempP   ,P0_NDL_APUTEMP_P			,&l_ndl_p0_ai9_tempM	, 431-OFFX,  816-OFFY, 2, 4,	icb_aputempP			,tbl_aputemp,10		, p0_2) 
MY_NEEDLE2  (ndl_p0_ai9_tempR   ,P0_NDL_APUTEMP_R			,&l_ndl_p0_ai9_tempP	, 431-OFFX,  816-OFFY, 2, 4,	icb_aputempR			,tbl_aputemp,10		, p0_2) 
MY_ICON2	(cov_p0_starter     ,P0_COV_STARTER_D			,&l_ndl_p0_ai9_tempR	, 389-OFFX, 1051-OFFY,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2)	
MY_NEEDLE2	(ndl_p0_starterD    ,P0_NDL_STARTER_D			,&l_cov_p0_starter		, 431-OFFX, 1097-OFFY, 3, 5,	icb_starterD			,tbl_starter,10		, p0_2) 
MY_NEEDLE2	(ndl_p0_starterM    ,P0_NDL_STARTER_M			,&l_ndl_p0_starterD		, 431-OFFX, 1097-OFFY, 3, 5,	icb_starterM			,tbl_starter,10		, p0_2) 
MY_NEEDLE2	(ndl_p0_starterP    ,P0_NDL_STARTER_P			,&l_ndl_p0_starterM		, 431-OFFX, 1097-OFFY, 3, 5,	icb_starterP			,tbl_starter,10		, p0_2) 
MY_NEEDLE2	(ndl_p0_starterR    ,P0_NDL_STARTER_R			,&l_ndl_p0_starterP		, 431-OFFX, 1097-OFFY, 3, 5,	icb_starterR			,tbl_starter,10		, p0_2) 
MY_ICON2	(p0_2Ico			,P0_BACKGROUND2_D			,&l_ndl_p0_starterR		,		 0,			0,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2) 
MY_STATIC2  (p0_2bg,p0_2_list	,P0_BACKGROUND2				,&l_p0_2Ico				, p0_2);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p0_yoke			,MISC_YOKE					,NULL					, 135-OFFX, 1140-OFFY,			icb_yoke				,1					, p0_2)	
MY_ICON2	(p0_crs_01			,MISC_CRS_P0_01				,&l_p0_yoke			,  10-OFFX, 1106-OFFY,			icb_crs_p0_01			,1					, p0_2)	
MY_ICON2	(p0_lamp13			,P0_LMP_13_D_00				,&l_p0_crs_01			, 133-OFFX,  936-OFFY,			icb_lamp13				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo22			,P0_TBL_22_D_00				,&l_p0_lamp13			, 425-OFFX,  569-OFFY,			icb_tablo22				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo23			,P0_TBL_23_D_00				,&l_p0_tablo22			, 425-OFFX,  618-OFFY,			icb_tablo23				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo24			,P0_TBL_24_D_00				,&l_p0_tablo23			, 425-OFFX,  651-OFFY,			icb_tablo24				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo25			,P0_TBL_25_D_00				,&l_p0_tablo24			, 287-OFFX,  745-OFFY,			icb_tablo25				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo26			,P0_TBL_26_D_00				,&l_p0_tablo25			, 321-OFFX,  802-OFFY,			icb_tablo26				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo27			,P0_TBL_27_D_00				,&l_p0_tablo26			, 321-OFFX,  842-OFFY,			icb_tablo27				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo28			,P0_TBL_28_D_00				,&l_p0_tablo27			, 269-OFFX,  802-OFFY,			icb_tablo28				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo29			,P0_TBL_29_D_00				,&l_p0_tablo28			, 269-OFFX,  842-OFFY,			icb_tablo29				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo30			,P0_TBL_30_D_00				,&l_p0_tablo29			, 210-OFFX,  831-OFFY,			icb_tablo30				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(sound_alarm_fire	,P0_AZS_FIREALARMSOUND_D_00	,&l_p0_tablo30			, 188-OFFX,  936-OFFY,			icb_sound_alarm_fire	,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(gear_warning		,P0_AZS_GEARALARMSOUND_D_00	,&l_sound_alarm_fire	, 221-OFFX,  936-OFFY,			icb_gear_warning		,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(auto_skid			,P0_AZS_AUTODEBRAKE_D_00	,&l_gear_warning		, 282-OFFX,  936-OFFY,			icb_auto_skid			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(agb_ruchn			,P0_AZS_AGBMANUAL_D_00		,&l_auto_skid			, 316-OFFX,  936-OFFY,			icb_agb_ruchn			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(apu_pk9			,P0_AZS_APUVALVE_D_00		,&l_agb_ruchn			, 372-OFFX,  936-OFFY,			icb_apu_pk9				,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(warn_lights		,P0_AZS_TABLODAYNIGHT_D_00	,&l_apu_pk9			, 400-OFFX,  936-OFFY,			icb_warn_lights			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(emerge_exit		,P0_AZS_EXITTABLO_D_00		,&l_warn_lights		, 437-OFFX,  936-OFFY,			icb_emerge_exit			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(plafon_light		,P0_AZS_PLAFONLIGHT_D_00	,&l_emerge_exit		, 273-OFFX, 1067-OFFY,			icb_plafon_light		,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(red_light			,P0_AZS_REDLIGHT_D_00		,&l_plafon_light		, 312-OFFX, 1067-OFFY,			icb_red_light			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(ltstest			,P0_BTN_LTSTEST_D_00		,&l_red_light			, 389-OFFX,  702-OFFY,			icb_ltstest				,2*PANEL_LIGHT_MAX	, p0_2)	
MY_ICON2    (cov_p0_ai9_temp    ,P0_COV_APUTEMP_D			,&l_ltstest			, 413-OFFX,  799-OFFY,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2)	    
MY_NEEDLE2  (ndl_p0_ai9_tempD   ,P0_NDL_APUTEMP_D			,&l_cov_p0_ai9_temp    , 431-OFFX,  816-OFFY, 2, 4,	icb_aputempD			,tbl_aputemp,10		, p0_2) 
MY_NEEDLE2  (ndl_p0_ai9_tempP   ,P0_NDL_APUTEMP_P			,&l_ndl_p0_ai9_tempD   , 431-OFFX,  816-OFFY, 2, 4,	icb_aputempP			,tbl_aputemp,10		, p0_2) 
MY_ICON2	(cov_p0_starter     ,P0_COV_STARTER_D			,&l_ndl_p0_ai9_tempP   , 389-OFFX, 1051-OFFY,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2)	
MY_NEEDLE2	(ndl_p0_starterD    ,P0_NDL_STARTER_D			,&l_cov_p0_starter     , 431-OFFX, 1097-OFFY, 3, 5,	icb_starterD			,tbl_starter,10		, p0_2) 
MY_NEEDLE2	(ndl_p0_starterP    ,P0_NDL_STARTER_P			,&l_ndl_p0_starterD    , 431-OFFX, 1097-OFFY, 3, 5,	icb_starterP			,tbl_starter,10		, p0_2) 
MY_ICON2	(p0_2Ico			,P0_BACKGROUND2_D			,&l_ndl_p0_starterP    ,		 0,			0,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2) 
MY_STATIC2  (p0_2bg,p0_2_list	,P0_BACKGROUND2				,&l_p0_2Ico			, p0_2);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_yoke			,MISC_YOKE					,NULL					, 135-OFFX, 1140-OFFY,			icb_yoke				,1					, p0_2)	
MY_ICON2	(p0_crs_01			,MISC_CRS_P0_01				,&l_p0_yoke			,  10-OFFX, 1106-OFFY,			icb_crs_p0_01			,1					, p0_2)	
MY_ICON2	(p0_lamp13			,P0_LMP_13_D_00				,&l_p0_crs_01			, 133-OFFX,  936-OFFY,			icb_lamp13				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo22			,P0_TBL_22_D_00				,&l_p0_lamp13			, 425-OFFX,  569-OFFY,			icb_tablo22				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo23			,P0_TBL_23_D_00				,&l_p0_tablo22			, 425-OFFX,  618-OFFY,			icb_tablo23				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo24			,P0_TBL_24_D_00				,&l_p0_tablo23			, 425-OFFX,  651-OFFY,			icb_tablo24				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo25			,P0_TBL_25_D_00				,&l_p0_tablo24			, 287-OFFX,  745-OFFY,			icb_tablo25				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo26			,P0_TBL_26_D_00				,&l_p0_tablo25			, 321-OFFX,  802-OFFY,			icb_tablo26				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo27			,P0_TBL_27_D_00				,&l_p0_tablo26			, 321-OFFX,  842-OFFY,			icb_tablo27				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo28			,P0_TBL_28_D_00				,&l_p0_tablo27			, 269-OFFX,  802-OFFY,			icb_tablo28				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo29			,P0_TBL_29_D_00				,&l_p0_tablo28			, 269-OFFX,  842-OFFY,			icb_tablo29				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo30			,P0_TBL_30_D_00				,&l_p0_tablo29			, 210-OFFX,  831-OFFY,			icb_tablo30				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(sound_alarm_fire	,P0_AZS_FIREALARMSOUND_D_00	,&l_p0_tablo30			, 188-OFFX,  936-OFFY,			icb_sound_alarm_fire	,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(gear_warning		,P0_AZS_GEARALARMSOUND_D_00	,&l_sound_alarm_fire	, 221-OFFX,  936-OFFY,			icb_gear_warning		,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(auto_skid			,P0_AZS_AUTODEBRAKE_D_00	,&l_gear_warning		, 282-OFFX,  936-OFFY,			icb_auto_skid			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(agb_ruchn			,P0_AZS_AGBMANUAL_D_00		,&l_auto_skid			, 316-OFFX,  936-OFFY,			icb_agb_ruchn			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(apu_pk9			,P0_AZS_APUVALVE_D_00		,&l_agb_ruchn			, 372-OFFX,  936-OFFY,			icb_apu_pk9				,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(warn_lights		,P0_AZS_TABLODAYNIGHT_D_00	,&l_apu_pk9			, 400-OFFX,  936-OFFY,			icb_warn_lights			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(emerge_exit		,P0_AZS_EXITTABLO_D_00		,&l_warn_lights		, 437-OFFX,  936-OFFY,			icb_emerge_exit			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(plafon_light		,P0_AZS_PLAFONLIGHT_D_00	,&l_emerge_exit		, 273-OFFX, 1067-OFFY,			icb_plafon_light		,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(red_light			,P0_AZS_REDLIGHT_D_00		,&l_plafon_light		, 312-OFFX, 1067-OFFY,			icb_red_light			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(ltstest			,P0_BTN_LTSTEST_D_00		,&l_red_light			, 389-OFFX,  702-OFFY,			icb_ltstest				,2*PANEL_LIGHT_MAX	, p0_2)	
MY_ICON2    (cov_p0_ai9_temp    ,P0_COV_APUTEMP_D			,&l_ltstest			, 413-OFFX,  799-OFFY,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2)	    
MY_NEEDLE2  (ndl_p0_ai9_tempD   ,P0_NDL_APUTEMP_D			,&l_cov_p0_ai9_temp    , 431-OFFX,  816-OFFY, 2, 4,	icb_aputempD			,tbl_aputemp,10		, p0_2) 
MY_NEEDLE2  (ndl_p0_ai9_tempR   ,P0_NDL_APUTEMP_R			,&l_ndl_p0_ai9_tempD   , 431-OFFX,  816-OFFY, 2, 4,	icb_aputempR			,tbl_aputemp,10		, p0_2) 
MY_ICON2	(cov_p0_starter     ,P0_COV_STARTER_D			,&l_ndl_p0_ai9_tempR   , 389-OFFX, 1051-OFFY,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2)	
MY_NEEDLE2	(ndl_p0_starterD    ,P0_NDL_STARTER_D			,&l_cov_p0_starter     , 431-OFFX, 1097-OFFY, 3, 5,	icb_starterD			,tbl_starter,10		, p0_2) 
MY_NEEDLE2	(ndl_p0_starterR    ,P0_NDL_STARTER_R			,&l_ndl_p0_starterD    , 431-OFFX, 1097-OFFY, 3, 5,	icb_starterR			,tbl_starter,10		, p0_2) 
MY_ICON2	(p0_2Ico			,P0_BACKGROUND2_D			,&l_ndl_p0_starterR    ,		 0,			0,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2) 
MY_STATIC2  (p0_2bg,p0_2_list	,P0_BACKGROUND2				,&l_p0_2Ico			, p0_2);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p0_yoke			,MISC_YOKE					,NULL					, 135-OFFX, 1140-OFFY,			icb_yoke				,1					, p0_2)	
MY_ICON2	(p0_crs_01			,MISC_CRS_P0_01				,&l_p0_yoke			,  10-OFFX, 1106-OFFY,			icb_crs_p0_01			,1					, p0_2)	
MY_ICON2	(p0_lamp13			,P0_LMP_13_D_00				,&l_p0_crs_01			, 133-OFFX,  936-OFFY,			icb_lamp13				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo22			,P0_TBL_22_D_00				,&l_p0_lamp13			, 425-OFFX,  569-OFFY,			icb_tablo22				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo23			,P0_TBL_23_D_00				,&l_p0_tablo22			, 425-OFFX,  618-OFFY,			icb_tablo23				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo24			,P0_TBL_24_D_00				,&l_p0_tablo23			, 425-OFFX,  651-OFFY,			icb_tablo24				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo25			,P0_TBL_25_D_00				,&l_p0_tablo24			, 287-OFFX,  745-OFFY,			icb_tablo25				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo26			,P0_TBL_26_D_00				,&l_p0_tablo25			, 321-OFFX,  802-OFFY,			icb_tablo26				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo27			,P0_TBL_27_D_00				,&l_p0_tablo26			, 321-OFFX,  842-OFFY,			icb_tablo27				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo28			,P0_TBL_28_D_00				,&l_p0_tablo27			, 269-OFFX,  802-OFFY,			icb_tablo28				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo29			,P0_TBL_29_D_00				,&l_p0_tablo28			, 269-OFFX,  842-OFFY,			icb_tablo29				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(p0_tablo30			,P0_TBL_30_D_00				,&l_p0_tablo29			, 210-OFFX,  831-OFFY,			icb_tablo30				,3*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(sound_alarm_fire	,P0_AZS_FIREALARMSOUND_D_00	,&l_p0_tablo30			, 188-OFFX,  936-OFFY,			icb_sound_alarm_fire	,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(gear_warning		,P0_AZS_GEARALARMSOUND_D_00	,&l_sound_alarm_fire	, 221-OFFX,  936-OFFY,			icb_gear_warning		,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(auto_skid			,P0_AZS_AUTODEBRAKE_D_00	,&l_gear_warning		, 282-OFFX,  936-OFFY,			icb_auto_skid			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(agb_ruchn			,P0_AZS_AGBMANUAL_D_00		,&l_auto_skid			, 316-OFFX,  936-OFFY,			icb_agb_ruchn			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(apu_pk9			,P0_AZS_APUVALVE_D_00		,&l_agb_ruchn			, 372-OFFX,  936-OFFY,			icb_apu_pk9				,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(warn_lights		,P0_AZS_TABLODAYNIGHT_D_00	,&l_apu_pk9			, 400-OFFX,  936-OFFY,			icb_warn_lights			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(emerge_exit		,P0_AZS_EXITTABLO_D_00		,&l_warn_lights		, 437-OFFX,  936-OFFY,			icb_emerge_exit			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(plafon_light		,P0_AZS_PLAFONLIGHT_D_00	,&l_emerge_exit		, 273-OFFX, 1067-OFFY,			icb_plafon_light		,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(red_light			,P0_AZS_REDLIGHT_D_00		,&l_plafon_light		, 312-OFFX, 1067-OFFY,			icb_red_light			,2*PANEL_LIGHT_MAX	, p0_2)
MY_ICON2	(ltstest			,P0_BTN_LTSTEST_D_00		,&l_red_light			, 389-OFFX,  702-OFFY,			icb_ltstest				,2*PANEL_LIGHT_MAX	, p0_2)	
MY_ICON2    (cov_p0_ai9_temp    ,P0_COV_APUTEMP_D			,&l_ltstest			, 413-OFFX,  799-OFFY,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2)	    
MY_NEEDLE2  (ndl_p0_ai9_tempD   ,P0_NDL_APUTEMP_D			,&l_cov_p0_ai9_temp    , 431-OFFX,  816-OFFY, 2, 4,	icb_aputempD			,tbl_aputemp,10		, p0_2) 
MY_NEEDLE2  (ndl_p0_ai9_tempM   ,P0_NDL_APUTEMP_M			,&l_ndl_p0_ai9_tempD   , 431-OFFX,  816-OFFY, 2, 4,	icb_aputempM			,tbl_aputemp,10		, p0_2) 
MY_ICON2	(cov_p0_starter     ,P0_COV_STARTER_D			,&l_ndl_p0_ai9_tempM   , 389-OFFX, 1051-OFFY,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2)	
MY_NEEDLE2	(ndl_p0_starterD    ,P0_NDL_STARTER_D			,&l_cov_p0_starter     , 431-OFFX, 1097-OFFY, 3, 5,	icb_starterD			,tbl_starter,10		, p0_2) 
MY_NEEDLE2	(ndl_p0_starterM    ,P0_NDL_STARTER_M			,&l_ndl_p0_starterD    , 431-OFFX, 1097-OFFY, 3, 5,	icb_starterM			,tbl_starter,10		, p0_2) 
MY_ICON2	(p0_2Ico			,P0_BACKGROUND2_D			,&l_ndl_p0_starterM    ,		 0,			0,			icb_Ico					,PANEL_LIGHT_MAX	, p0_2) 
MY_STATIC2  (p0_2bg,p0_2_list	,P0_BACKGROUND2				,&l_p0_2Ico			, p0_2);
#endif

#endif