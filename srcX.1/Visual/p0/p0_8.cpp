/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

//////////////////////////////////////////////////////////////////////////

#ifdef HAVE_DAY_PANEL
static MAKE_SPCB(icb_scaleD			,if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; SHOW_NDL(NDL_KPPMS0_SCALE		))
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_SPCB(icb_scaleM			,if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; SHOW_NDL(NDL_KPPMS0_SCALE		))
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_SPCB(icb_scaleP			,if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; SHOW_NDL(NDL_KPPMS0_SCALE		))
#endif
#ifdef HAVE_RED_PANEL
static MAKE_SPCB(icb_scaleR			,if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; SHOW_NDL(NDL_KPPMS0_SCALE		))
#endif

static double FSAPI icb_knbp0_8(PELEMENT_ICON pelement) 
{ 
	CHK_BRT();
	return KRM_GET(KRM_KPPMS0)+(POS_GET(POS_PANEL_STATE)*20);
}

#ifdef HAVE_DAY_PANEL
static double FSAPI icbndlhdgd(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS0_SCALE)-90; };
#endif
#ifdef HAVE_MIX_PANEL
static double FSAPI icbndlhdgm(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS0_SCALE)-90; };
#endif
#ifdef HAVE_PLF_PANEL
static double FSAPI icbndlhdgp(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS0_SCALE)-90; };
#endif
#ifdef HAVE_RED_PANEL
static double FSAPI icbndlhdgr(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS0_SCALE)-90; };
#endif
#ifdef HAVE_DAY_PANEL
static double FSAPI icbndlscld(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_DAY,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_KPPMS0_SCALE); };
#endif
#ifdef HAVE_MIX_PANEL
static double FSAPI icbndlsclm(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_MIX,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_KPPMS0_SCALE); };
#endif
#ifdef HAVE_PLF_PANEL
static double FSAPI icbndlsclp(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_PLF,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_KPPMS0_SCALE); };
#endif
#ifdef HAVE_RED_PANEL
static double FSAPI icbndlsclr(PELEMENT_NEEDLE pelement) { if(!ChkColor(pelement,PANEL_LIGHT_RED,POS_GET(POS_PANEL_STATE)))return 0; CHK_BRT(); return NDL_GET(NDL_KPPMS0_SCALE); };
#endif
#ifdef HAVE_DAY_PANEL
static double FSAPI icbsldgd(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS0_ILS_GLIDE))<<1;}HIDE_IMAGE(pelement);return 0;};
#endif
#ifdef HAVE_MIX_PANEL
static double FSAPI icbsldgm(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS0_ILS_GLIDE))<<1;}HIDE_IMAGE(pelement);return 0;};
#endif
#ifdef HAVE_PLF_PANEL
static double FSAPI icbsldgp(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS0_ILS_GLIDE))<<1;}HIDE_IMAGE(pelement);return 0;};
#endif
#ifdef HAVE_RED_PANEL
static double FSAPI icbsldgr(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS0_ILS_GLIDE))<<1;}HIDE_IMAGE(pelement);return 0;};
#endif
#ifdef HAVE_DAY_PANEL
static double FSAPI icbsldcd(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS0_ILS_COURSE))<<1;}HIDE_IMAGE(pelement);return 0;};
#endif
#ifdef HAVE_MIX_PANEL
static double FSAPI icbsldcm(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS0_ILS_COURSE))<<1;}HIDE_IMAGE(pelement);return 0;};
#endif
#ifdef HAVE_PLF_PANEL
static double FSAPI icbsldcp(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS0_ILS_COURSE))<<1;}HIDE_IMAGE(pelement);return 0;};
#endif
#ifdef HAVE_RED_PANEL
static double FSAPI icbsldcr(PELEMENT_SLIDER pelement) { if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED) {SHOW_IMAGE(pelement);CHK_BRT(); return int(NDL_GET(NDL_KPPMS0_ILS_COURSE))<<1;}HIDE_IMAGE(pelement);return 0;};
#endif

static double FSAPI icbblkc(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	if(!POS_GET(POS_KPPMS0_BLK_COURSE)) 
		return POS_GET(POS_PANEL_STATE); 
	else 
		return -1;
}

static double FSAPI icbblkg(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	if(!POS_GET(POS_KPPMS0_BLK_GLIDE)) 
		return POS_GET(POS_PANEL_STATE); 
	else 
		return -1;
} 

static double FSAPI icbcov_p0_8(PELEMENT_ICON pelement) 
{ 
	CHK_BRT(); 
	if(POS_GET(POS_PANEL_LANG)) {
		SHOW_IMAGE(pelement);
		return POS_GET(POS_PANEL_STATE); 
	} 
	HIDE_IMAGE(pelement);
	return -1;
} 

//////////////////////////////////////////////////////////////////////////

static BOOL FSAPI mcb_knblp0_8(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE)
		NDL_DEC(NDL_KPPMS0_SCALE);
	else if(mouse_flags&MOUSE_RIGHTSINGLE)
		NDL_DEC2(NDL_KPPMS0_SCALE,10);
	KRM_DEC(KRM_KPPMS0);
	return TRUE;
}

static BOOL FSAPI mcb_knbrp0_8(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE)
		NDL_INC(NDL_KPPMS0_SCALE);
	else if(mouse_flags&MOUSE_RIGHTSINGLE)
		NDL_INC2(NDL_KPPMS0_SCALE,10);
	KRM_INC(KRM_KPPMS0);
	return TRUE;
}

BOOL FSAPI mcb_kppms_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01,return NDL_TTGET(NDL_KPPMS0_SCALE);) 

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p0_8,HELP_NONE,0,0)
	MOUSE_TBOX("1", 30, 30,P12_BACKGROUND_D_SX,P12_BACKGROUND_D_SY,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_kppms_big)    
	MOUSE_TSHB("1",232,248,60,60,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_knblp0_8,mcb_knbrp0_8) 
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p0_8_list; 
GAUGE_HEADER_FS700_EX(P12_BACKGROUND_D_SX, "p0_8", &p0_8_list, rect_p0_8, 0, 0, 0, 0, p0_8); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_8Spot2			,P12_COV_KPPMS1SPSPOT2_D	,NULL			, 224,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8spot1			,P12_COV_KPPMS1SPSPOT1_D	,&l_p0_8Spot2	,  40,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8knb			,P12_KNB_KPPMS1SP_D_00		,&l_p0_8spot1	, 232, 248,icb_knbp0_8			,20*PANEL_LIGHT_MAX,p0_8)
MY_NEEDLE2	(p0_8NdlHdgR		,P12_NDL_KPPMS1HDG_R		,&l_p0_8knb		, 151, 169, 0, 13,icbndlhdgr	,0,0,p0_8); 
MY_NEEDLE2	(p0_8NdlHdgP		,P12_NDL_KPPMS1HDG_P		,&l_p0_8NdlHdgR	, 151, 169, 0, 13,icbndlhdgp	,0,0,p0_8); 
MY_NEEDLE2	(p0_8NdlHdgM		,P12_NDL_KPPMS1HDG_M		,&l_p0_8NdlHdgP	, 151, 169, 0, 13,icbndlhdgm	,0,0,p0_8); 
MY_NEEDLE2	(p0_8NdlHdgD		,P12_NDL_KPPMS1HDG_D		,&l_p0_8NdlHdgM	, 151, 169, 0, 13,icbndlhdgd	,0,0,p0_8); 
MY_STATIC2A	(p0_8Alpha			,P12_ALPHA_KPPMS1			,&l_p0_8NdlHdgD	,  25,  39,p0_8); 
MY_SPRITE2	(p0_8ScaleR			,P12_SCL_KPPMS1SP_R			,&l_p0_8Alpha	,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleR,-1,p0_8)	
MY_SPRITE2	(p0_8ScaleP			,P12_SCL_KPPMS1SP_P			,&l_p0_8ScaleR	,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleP,-1,p0_8)	
MY_SPRITE2	(p0_8ScaleM			,P12_SCL_KPPMS1SP_M			,&l_p0_8ScaleP	,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleM,-1,p0_8)	
MY_SPRITE2	(p0_8ScaleD			,P12_SCL_KPPMS1SP_D			,&l_p0_8ScaleM	,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleD,-1,p0_8)	
MY_SLIDER2	(p0_8SldGR			,P12_NDL_KPPMS1ILSGLIDE_R	,&l_p0_8ScaleD	,  48, 159,0,0,icbsldgr,0.25,p0_8); 
MY_SLIDER2	(p0_8SldGP			,P12_NDL_KPPMS1ILSGLIDE_P	,&l_p0_8SldGR	,  48, 159,0,0,icbsldgp,0.25,p0_8); 
MY_SLIDER2	(p0_8SldGM			,P12_NDL_KPPMS1ILSGLIDE_M	,&l_p0_8SldGP	,  48, 159,0,0,icbsldgm,0.25,p0_8); 
MY_SLIDER2	(p0_8SldGD			,P12_NDL_KPPMS1ILSGLIDE_D	,&l_p0_8SldGM	,  48, 159,0,0,icbsldgd,0.25,p0_8); 
MY_SLIDER2	(p0_8SldCR			,P12_NDL_KPPMS1ILSCOURSE_R	,&l_p0_8SldGD	, 147,  79,icbsldcr,0.25,0,0,p0_8); 
MY_SLIDER2	(p0_8SldCP			,P12_NDL_KPPMS1ILSCOURSE_P	,&l_p0_8SldCR	, 147,  79,icbsldcp,0.25,0,0,p0_8); 
MY_SLIDER2	(p0_8SldCM			,P12_NDL_KPPMS1ILSCOURSE_M	,&l_p0_8SldCP	, 147,  79,icbsldcm,0.25,0,0,p0_8); 
MY_SLIDER2	(p0_8SldCD			,P12_NDL_KPPMS1ILSCOURSE_D	,&l_p0_8SldCM	, 147,  79,icbsldcd,0.25,0,0,p0_8); 
MY_ICON2	(p0_8BlkG  			,P12_BLK_KPPMS1GLIDE_D		,&l_p0_8SldCD	, 113, 130,icbblkg				,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8BlkC  			,P12_BLK_KPPMS1COURSE_D		,&l_p0_8BlkG  	, 171, 171,icbblkc				,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8IcoCov			,P12_COV_KPPMS1INTL_D		,&l_p0_8BlkC  	,  78,  94,icbcov_p0_8			,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8Ico			,P12_BACKGROUND_D			,&l_p0_8IcoCov	,   0,   0, icb_Ico				,PANEL_LIGHT_MAX  , p0_8) 
MY_STATIC2	(p0_8bg,p0_8_list	,P12_BACKGROUND_D			,&l_p0_8Ico			, p0_8);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p0_8Spot2			,P12_COV_KPPMS1SPSPOT2_D	,NULL			, 224,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8spot1			,P12_COV_KPPMS1SPSPOT1_D	,&l_p0_8Spot2			,  40,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8knb			,P12_KNB_KPPMS1SP_D_00		,&l_p0_8spot1			, 232, 248,icb_knbp0_8			,20*PANEL_LIGHT_MAX,p0_8)
MY_NEEDLE2	(p0_8NdlHdgP		,P12_NDL_KPPMS1HDG_P		,&l_p0_8knb			, 151, 169, 0, 13,icbndlhdgp	,0,0,p0_8); 
MY_NEEDLE2	(p0_8NdlHdgD		,P12_NDL_KPPMS1HDG_D		,&l_p0_8NdlHdgP		, 151, 169, 0, 13,icbndlhdgd	,0,0,p0_8); 
MY_STATIC2A	(p0_8Alpha			,P12_ALPHA_KPPMS1			,&l_p0_8NdlHdgD		,  25,  39,p0_8); 
MY_SPRITE2	(p0_8ScaleP			,P12_SCL_KPPMS1SP_P			,&l_p0_8Alpha			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleP,-1,p0_8)	
MY_SPRITE2	(p0_8ScaleD			,P12_SCL_KPPMS1SP_D			,&l_p0_8ScaleP			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleD,-1,p0_8)	
MY_SLIDER2	(p0_8SldGP			,P12_NDL_KPPMS1ILSGLIDE_P	,&l_p0_8ScaleD			,  48, 159,0,0,icbsldgp,0.25,p0_8); 
MY_SLIDER2	(p0_8SldGD			,P12_NDL_KPPMS1ILSGLIDE_D	,&l_p0_8SldGP			,  48, 159,0,0,icbsldgd,0.25,p0_8); 
MY_SLIDER2	(p0_8SldCP			,P12_NDL_KPPMS1ILSCOURSE_P	,&l_p0_8SldGD			, 147,  79,icbsldcp,0.25,0,0,p0_8); 
MY_SLIDER2	(p0_8SldCD			,P12_NDL_KPPMS1ILSCOURSE_D	,&l_p0_8SldCP			, 147,  79,icbsldcd,0.25,0,0,p0_8); 
MY_ICON2	(p0_8BlkG  			,P12_BLK_KPPMS1GLIDE_D		,&l_p0_8SldCD			, 113, 130,icbblkg				,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8BlkC  			,P12_BLK_KPPMS1COURSE_D		,&l_p0_8BlkG  			, 171, 171,icbblkc				,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8IcoCov			,P12_COV_KPPMS1INTL_D		,&l_p0_8BlkC  			,  78,  94,icbcov_p0_8			,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8Ico			,P12_BACKGROUND_D			,&l_p0_8IcoCov			,   0,   0, icb_Ico				,PANEL_LIGHT_MAX  , p0_8) 
MY_STATIC2	(p0_8bg,p0_8_list	,P12_BACKGROUND_D			,&l_p0_8Ico				, p0_8);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_8Spot2			,P12_COV_KPPMS1SPSPOT2_D	,NULL			, 224,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8spot1			,P12_COV_KPPMS1SPSPOT1_D	,&l_p0_8Spot2			,  40,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8knb			,P12_KNB_KPPMS1SP_D_00		,&l_p0_8spot1			, 232, 248,icb_knbp0_8			,20*PANEL_LIGHT_MAX,p0_8)
MY_NEEDLE2	(p0_8NdlHdgR		,P12_NDL_KPPMS1HDG_R		,&l_p0_8knb			, 151, 169, 0, 13,icbndlhdgr	,0,0,p0_8); 
MY_NEEDLE2	(p0_8NdlHdgD		,P12_NDL_KPPMS1HDG_D		,&l_p0_8NdlHdgR		, 151, 169, 0, 13,icbndlhdgd	,0,0,p0_8); 
MY_STATIC2A	(p0_8Alpha			,P12_ALPHA_KPPMS1			,&l_p0_8NdlHdgD		,  25,  39,p0_8); 
MY_SPRITE2	(p0_8ScaleR			,P12_SCL_KPPMS1SP_R			,&l_p0_8Alpha			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleR,-1,p0_8)	
MY_SPRITE2	(p0_8ScaleD			,P12_SCL_KPPMS1SP_D			,&l_p0_8ScaleR			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleD,-1,p0_8)	
MY_SLIDER2	(p0_8SldGR			,P12_NDL_KPPMS1ILSGLIDE_R	,&l_p0_8ScaleD			,  48, 159,0,0,icbsldgr,0.25,p0_8); 
MY_SLIDER2	(p0_8SldGD			,P12_NDL_KPPMS1ILSGLIDE_D	,&l_p0_8SldGR			,  48, 159,0,0,icbsldgd,0.25,p0_8); 
MY_SLIDER2	(p0_8SldCR			,P12_NDL_KPPMS1ILSCOURSE_R	,&l_p0_8SldGD			, 147,  79,icbsldcr,0.25,0,0,p0_8); 
MY_SLIDER2	(p0_8SldCD			,P12_NDL_KPPMS1ILSCOURSE_D	,&l_p0_8SldCR			, 147,  79,icbsldcd,0.25,0,0,p0_8); 
MY_ICON2	(p0_8BlkG  			,P12_BLK_KPPMS1GLIDE_D		,&l_p0_8SldCD			, 113, 130,icbblkg				,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8BlkC  			,P12_BLK_KPPMS1COURSE_D		,&l_p0_8BlkG  			, 171, 171,icbblkc				,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8IcoCov			,P12_COV_KPPMS1INTL_D		,&l_p0_8BlkC  			,  78,  94,icbcov_p0_8			,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8Ico			,P12_BACKGROUND_D			,&l_p0_8IcoCov			,   0,   0, icb_Ico				,PANEL_LIGHT_MAX  , p0_8) 
MY_STATIC2	(p0_8bg,p0_8_list	,P12_BACKGROUND_D			,&l_p0_8Ico				, p0_8);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p0_8Spot2			,P12_COV_KPPMS1SPSPOT2_D	,NULL			, 224,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8spot1			,P12_COV_KPPMS1SPSPOT1_D	,&l_p0_8Spot2			,  40,   5,icb_Ico				, 4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8knb			,P12_KNB_KPPMS1SP_D_00		,&l_p0_8spot1			, 232, 248,icb_knbp0_8			,20*PANEL_LIGHT_MAX,p0_8)
MY_NEEDLE2	(p0_8NdlHdgM		,P12_NDL_KPPMS1HDG_M		,&l_p0_8knb			, 151, 169, 0, 13,icbndlhdgm	,0,0,p0_8); 
MY_NEEDLE2	(p0_8NdlHdgD		,P12_NDL_KPPMS1HDG_D		,&l_p0_8NdlHdgM		, 151, 169, 0, 13,icbndlhdgd	,0,0,p0_8); 
MY_STATIC2A	(p0_8Alpha			,P12_ALPHA_KPPMS1			,&l_p0_8NdlHdgD		,  25,  39,p0_8); 
MY_SPRITE2	(p0_8ScaleM			,P12_SCL_KPPMS1SP_M			,&l_p0_8Alpha			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleM,-1,p0_8)	
MY_SPRITE2	(p0_8ScaleD			,P12_SCL_KPPMS1SP_D			,&l_p0_8ScaleM			,  27,  40,152,167,1,1,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,NULL,0,MODULE_VAR_NONE,icb_scaleD,-1,p0_8)	
MY_SLIDER2	(p0_8SldGM			,P12_NDL_KPPMS1ILSGLIDE_M	,&l_p0_8ScaleD			,  48, 159,0,0,icbsldgm,0.25,p0_8); 
MY_SLIDER2	(p0_8SldGD			,P12_NDL_KPPMS1ILSGLIDE_D	,&l_p0_8SldGM			,  48, 159,0,0,icbsldgd,0.25,p0_8); 
MY_SLIDER2	(p0_8SldCM			,P12_NDL_KPPMS1ILSCOURSE_M	,&l_p0_8SldGD			, 147,  79,icbsldcm,0.25,0,0,p0_8); 
MY_SLIDER2	(p0_8SldCD			,P12_NDL_KPPMS1ILSCOURSE_D	,&l_p0_8SldCM			, 147,  79,icbsldcd,0.25,0,0,p0_8); 
MY_ICON2	(p0_8BlkG  			,P12_BLK_KPPMS1GLIDE_D		,&l_p0_8SldCD			, 113, 130,icbblkg				,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8BlkC  			,P12_BLK_KPPMS1COURSE_D		,&l_p0_8BlkG  			, 171, 171,icbblkc				,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8IcoCov			,P12_COV_KPPMS1INTL_D		,&l_p0_8BlkC  			,  78,  94,icbcov_p0_8			,4*PANEL_LIGHT_MAX,p0_8)
MY_ICON2	(p0_8Ico			,P12_BACKGROUND_D			,&l_p0_8IcoCov			,   0,   0, icb_Ico				,PANEL_LIGHT_MAX  , p0_8) 
MY_STATIC2	(p0_8bg,p0_8_list	,P12_BACKGROUND_D			,&l_p0_8Ico				, p0_8);
#endif

