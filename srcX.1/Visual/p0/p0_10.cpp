/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

double ChkLangRus(PELEMENT_NEEDLE pelement, int st, int nd1); 
double ChkLangEng(PELEMENT_NEEDLE pelement, int st, int nd1); 

//////////////////////////////////////////////////////////////////////////

static NONLINEARITY tbl_tasr[]={
	{{129,216},   0,0},
	{{113,212}, 400,0},
	{{ 76,177}, 500,0},
	{{ 77,125}, 600,0},
	{{165, 90}, 800,0},
};

static NONLINEARITY tbl_iasr[]={
	{{138, 39},   0,0},
	{{190, 52}, 100,0},
	{{249,133}, 200,0},
	{{221,231}, 300,0},
	{{121,260}, 400,0},
	{{ 54,223}, 500,0},
	{{ 26,153}, 600,0},
	{{ 52, 80}, 700,0}
};

static NONLINEARITY tbl_tase[]={
	{{ 87,190},   0,0},
	{{ 78,178}, 200,0},
	{{ 72,151}, 250,0},
	{{ 77,125}, 300,0},
	{{ 93,104}, 350,0},
	{{114, 91}, 400,0}
};

static NONLINEARITY tbl_iase[]={
	{{138, 39},   0,0},
	{{190, 52},  50,0},
	{{230, 85}, 100,0},
	{{251,133}, 120,0},
	{{248,187}, 140,0},
	{{220,231}, 160,0},
	{{175,257}, 180,0},
	{{120,262}, 200,0},
	{{ 54,224}, 250,0},
	{{ 25,153}, 300,0},
	{{ 53, 82}, 350,0},
};

static double FSAPI icbndliaser(PELEMENT_NEEDLE pelement) { return ChkLangEng(pelement,PANEL_LIGHT_RED,NDL_KUS_IAS_EN); };
static double FSAPI icbndliasep(PELEMENT_NEEDLE pelement) { return ChkLangEng(pelement,PANEL_LIGHT_PLF,NDL_KUS_IAS_EN); }; 
static double FSAPI icbndliasem(PELEMENT_NEEDLE pelement) { return ChkLangEng(pelement,PANEL_LIGHT_MIX,NDL_KUS_IAS_EN); }; 
static double FSAPI icbndliased(PELEMENT_NEEDLE pelement) { return ChkLangEng(pelement,PANEL_LIGHT_DAY,NDL_KUS_IAS_EN); }; 
static double FSAPI icbndltaser(PELEMENT_NEEDLE pelement) { return ChkLangEng(pelement,PANEL_LIGHT_RED,NDL_KUS_TAS_EN); };
static double FSAPI icbndltasep(PELEMENT_NEEDLE pelement) { return ChkLangEng(pelement,PANEL_LIGHT_PLF,NDL_KUS_TAS_EN); }; 
static double FSAPI icbndltasem(PELEMENT_NEEDLE pelement) { return ChkLangEng(pelement,PANEL_LIGHT_MIX,NDL_KUS_TAS_EN); }; 
static double FSAPI icbndltased(PELEMENT_NEEDLE pelement) { return ChkLangEng(pelement,PANEL_LIGHT_DAY,NDL_KUS_TAS_EN); }; 
static double FSAPI icbndliasrr(PELEMENT_NEEDLE pelement) { return ChkLangRus(pelement,PANEL_LIGHT_RED,NDL_KUS_IAS);	};
static double FSAPI icbndliasrp(PELEMENT_NEEDLE pelement) { return ChkLangRus(pelement,PANEL_LIGHT_PLF,NDL_KUS_IAS);	}; 
static double FSAPI icbndliasrm(PELEMENT_NEEDLE pelement) { return ChkLangRus(pelement,PANEL_LIGHT_MIX,NDL_KUS_IAS);	}; 
static double FSAPI icbndliasrd(PELEMENT_NEEDLE pelement) { return ChkLangRus(pelement,PANEL_LIGHT_DAY,NDL_KUS_IAS);	}; 
static double FSAPI icbndltasrr(PELEMENT_NEEDLE pelement) { return ChkLangRus(pelement,PANEL_LIGHT_RED,NDL_KUS_TAS);	};
static double FSAPI icbndltasrp(PELEMENT_NEEDLE pelement) { return ChkLangRus(pelement,PANEL_LIGHT_PLF,NDL_KUS_TAS);	}; 
static double FSAPI icbndltasrm(PELEMENT_NEEDLE pelement) { return ChkLangRus(pelement,PANEL_LIGHT_MIX,NDL_KUS_TAS);	}; 
static double FSAPI icbndltasrd(PELEMENT_NEEDLE pelement) { return ChkLangRus(pelement,PANEL_LIGHT_DAY,NDL_KUS_TAS);	}; 

double FSAPI icbscl_p0_9(PELEMENT_ICON pelement); 

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_kus_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01,return NDL_TTGET(NDL_KUS_TAS);) 

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_10,HELP_NONE,0,0)
	MOUSE_TBOX( "1", 30, 30,P10_BACKGROUND_D_SX,P10_BACKGROUND_D_SY,CURSOR_NONE,MOUSE_RIGHTSINGLE,mcb_kus_big)    
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p0_10_list; 
GAUGE_HEADER_FS700_EX(P16_BACKGROUND_D_SX, "p0_10", &p0_10_list, rect_p0_10, 0, 0, 0, 0, p0_10); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_10Spot1        ,P16_COV_ASPDALTSPSPOT1_D,NULL             		, 32,  8,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10Spot2        ,P16_COV_ASPDALTSPSPOT2_D,&l_p0_10Spot1     		,204,  8,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10IcoCovKus		,P16_COV_KUS1_D			,&l_p0_10Spot2     		,129,140,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_NEEDLE2	(p0_10NdlIasER		,P16_NDL_KUS1IASEN_R	,&l_p0_10IcoCovKus		,139,149, 77, 12,icbndliaser ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasEP		,P16_NDL_KUS1IASEN_P	,&l_p0_10NdlIasER		,139,149, 77, 12,icbndliasep ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasEM		,P16_NDL_KUS1IASEN_M	,&l_p0_10NdlIasEP		,139,149, 77, 12,icbndliasem ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasED		,P16_NDL_KUS1IASEN_D	,&l_p0_10NdlIasEM		,139,149, 77, 12,icbndliased ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasER		,P16_NDL_KUS1TASEN_R	,&l_p0_10NdlIasED		,139,149, 21,  5,icbndltaser ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasEP		,P16_NDL_KUS1TASEN_P	,&l_p0_10NdlTasER		,139,149, 21,  5,icbndltasep ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasEM		,P16_NDL_KUS1TASEN_M	,&l_p0_10NdlTasEP		,139,149, 21,  5,icbndltasem ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasED		,P16_NDL_KUS1TASEN_D	,&l_p0_10NdlTasEM		,139,149, 21,  5,icbndltased ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRR		,P16_NDL_KUS1IAS_R		,&l_p0_10NdlTasED		,139,149, 77, 12,icbndliasrr ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRP		,P16_NDL_KUS1IAS_P		,&l_p0_10NdlIasRR		,139,149, 77, 12,icbndliasrp ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRM		,P16_NDL_KUS1IAS_M		,&l_p0_10NdlIasRP		,139,149, 77, 12,icbndliasrm ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRD		,P16_NDL_KUS1IAS_D		,&l_p0_10NdlIasRM		,139,149, 77, 12,icbndliasrd ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRR		,P16_NDL_KUS1TAS_R		,&l_p0_10NdlIasRD		,139,149, 21,  5,icbndltasrr ,tbl_tasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRP		,P16_NDL_KUS1TAS_P		,&l_p0_10NdlTasRR		,139,149, 21,  5,icbndltasrp ,tbl_tasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRM		,P16_NDL_KUS1TAS_M		,&l_p0_10NdlTasRP		,139,149, 21,  5,icbndltasrm ,tbl_tasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRD		,P16_NDL_KUS1TAS_D		,&l_p0_10NdlTasRM		,139,149, 21,  5,icbndltasrd ,tbl_tasr,0,p0_10)
MY_ICON2	(p0_10IcoSclKusEng	,P16_SCL_KUS1INTL_D		,&l_p0_10NdlTasRD		, 21, 34,icbscl_p0_9,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10Ico			,P16_BACKGROUND_D		,&l_p0_10IcoSclKusEng	,  0,  0,icb_Ico	,PANEL_LIGHT_MAX,p0_10) 
MY_STATIC2	(p0_10bg,p0_10_list	,P16_BACKGROUND_D		,&l_p0_10Ico			, p0_10);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p0_10Spot1        ,P16_COV_ASPDALTSPSPOT1_D,NULL             		, 32,  8,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10Spot2        ,P16_COV_ASPDALTSPSPOT2_D,&l_p0_10Spot1          ,204,  8,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10IcoCovKus		,P16_COV_KUS1_D			,&l_p0_10Spot2          ,129,140,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_NEEDLE2	(p0_10NdlIasEP		,P16_NDL_KUS1IASEN_P	,&l_p0_10IcoCovKus		,139,149, 77, 12,icbndliasep ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasED		,P16_NDL_KUS1IASEN_D	,&l_p0_10NdlIasEP		,139,149, 77, 12,icbndliased ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasEP		,P16_NDL_KUS1TASEN_P	,&l_p0_10NdlIasED		,139,149, 21,  5,icbndltasep ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasED		,P16_NDL_KUS1TASEN_D	,&l_p0_10NdlTasEP		,139,149, 21,  5,icbndltased ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRP		,P16_NDL_KUS1IAS_P		,&l_p0_10NdlTasED		,139,149, 77, 12,icbndliasrp ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRD		,P16_NDL_KUS1IAS_D		,&l_p0_10NdlIasRP		,139,149, 77, 12,icbndliasrd ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRP		,P16_NDL_KUS1TAS_P		,&l_p0_10NdlIasRD		,139,149, 21,  5,icbndltasrp ,tbl_tasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRD		,P16_NDL_KUS1TAS_D		,&l_p0_10NdlTasRP		,139,149, 21,  5,icbndltasrd ,tbl_tasr,0,p0_10)
MY_ICON2	(p0_10IcoSclKusEng	,P16_SCL_KUS1INTL_D		,&l_p0_10NdlTasRD		, 21, 34,icbscl_p0_9,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10Ico			,P16_BACKGROUND_D		,&l_p0_10IcoSclKusEng	,  0,  0,icb_Ico	,PANEL_LIGHT_MAX,p0_10) 
MY_STATIC2	(p0_10bg,p0_10_list	,P16_BACKGROUND_D		,&l_p0_10Ico			, p0_10);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_10Spot1        ,P16_COV_ASPDALTSPSPOT1_D,NULL             		, 32,  8,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10Spot2        ,P16_COV_ASPDALTSPSPOT2_D,&l_p0_10Spot1          ,204,  8,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10IcoCovKus		,P16_COV_KUS1_D			,&l_p0_10Spot2          ,129,140,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_NEEDLE2	(p0_10NdlIasER		,P16_NDL_KUS1IASEN_R	,&l_p0_10IcoCovKus		,139,149, 77, 12,icbndliaser ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasED		,P16_NDL_KUS1IASEN_D	,&l_p0_10NdlIasER		,139,149, 77, 12,icbndliased ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasER		,P16_NDL_KUS1TASEN_R	,&l_p0_10NdlIasED		,139,149, 21,  5,icbndltaser ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasED		,P16_NDL_KUS1TASEN_D	,&l_p0_10NdlTasER		,139,149, 21,  5,icbndltased ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRR		,P16_NDL_KUS1IAS_R		,&l_p0_10NdlTasED		,139,149, 77, 12,icbndliasrr ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRD		,P16_NDL_KUS1IAS_D		,&l_p0_10NdlIasRR		,139,149, 77, 12,icbndliasrd ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRR		,P16_NDL_KUS1TAS_R		,&l_p0_10NdlIasRD		,139,149, 21,  5,icbndltasrr ,tbl_tasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRD		,P16_NDL_KUS1TAS_D		,&l_p0_10NdlTasRR		,139,149, 21,  5,icbndltasrd ,tbl_tasr,0,p0_10)
MY_ICON2	(p0_10IcoSclKusEng	,P16_SCL_KUS1INTL_D		,&l_p0_10NdlTasRD		, 21, 34,icbscl_p0_9,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10Ico			,P16_BACKGROUND_D		,&l_p0_10IcoSclKusEng	,  0,  0,icb_Ico	,PANEL_LIGHT_MAX,p0_10) 
MY_STATIC2	(p0_10bg,p0_10_list	,P16_BACKGROUND_D		,&l_p0_10Ico			, p0_10);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p0_10Spot1        ,P16_COV_ASPDALTSPSPOT1_D,NULL             		, 32,  8,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10Spot2        ,P16_COV_ASPDALTSPSPOT2_D,&l_p0_10Spot1          ,204,  8,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10IcoCovKus		,P16_COV_KUS1_D			,&l_p0_10Spot2          ,129,140,icb_Ico,PANEL_LIGHT_MAX,p0_10)
MY_NEEDLE2	(p0_10NdlIasEM		,P16_NDL_KUS1IASEN_M	,&l_p0_10IcoCovKus		,139,149, 77, 12,icbndliasem ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasED		,P16_NDL_KUS1IASEN_D	,&l_p0_10NdlIasEM		,139,149, 77, 12,icbndliased ,tbl_iase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasEM		,P16_NDL_KUS1TASEN_M	,&l_p0_10NdlIasED		,139,149, 21,  5,icbndltasem ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasED		,P16_NDL_KUS1TASEN_D	,&l_p0_10NdlTasEM		,139,149, 21,  5,icbndltased ,tbl_tase,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRM		,P16_NDL_KUS1IAS_M		,&l_p0_10NdlTasED		,139,149, 77, 12,icbndliasrm ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlIasRD		,P16_NDL_KUS1IAS_D		,&l_p0_10NdlIasRM		,139,149, 77, 12,icbndliasrd ,tbl_iasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRM		,P16_NDL_KUS1TAS_M		,&l_p0_10NdlIasRD		,139,149, 21,  5,icbndltasrm ,tbl_tasr,0,p0_10)
MY_NEEDLE2	(p0_10NdlTasRD		,P16_NDL_KUS1TAS_D		,&l_p0_10NdlTasRM		,139,149, 21,  5,icbndltasrd ,tbl_tasr,0,p0_10)
MY_ICON2	(p0_10IcoSclKusEng	,P16_SCL_KUS1INTL_D		,&l_p0_10NdlTasRD		, 21, 34,icbscl_p0_9,PANEL_LIGHT_MAX,p0_10)
MY_ICON2	(p0_10Ico			,P16_BACKGROUND_D		,&l_p0_10IcoSclKusEng	,  0,  0,icb_Ico	,PANEL_LIGHT_MAX,p0_10) 
MY_STATIC2	(p0_10bg,p0_10_list	,P16_BACKGROUND_D		,&l_p0_10Ico			, p0_10);
#endif

#endif