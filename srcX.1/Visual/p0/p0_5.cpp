/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

#define OFFX	1170
#define OFFY	0

//////////////////////////////////////////////////////////////////////////

// ����
static MAKE_NCB(icb_secD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_secM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_secP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_secR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_SEC			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_minR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_MIN			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_hrsR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDLM(NDL_MAIN_HRS			,90);}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_secR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return NDL_GET(NDL_MINI_SEC)*12-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_minR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_MIN)*5)+5/(60/NDL_GET(NDL_MINI_SEC_HIDE)))*1.2)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsD		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsM		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsP		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_NCB(icb_mini_hrsR		,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);CHK_BRT(); return (((NDL_GET(NDL_MINI_HRS)*5)+5/(60/NDL_GET(NDL_MINI_MIN)))*6)-90;}else HIDE_IMAGE(pelement);return -1;)
static MAKE_ICB(icb_blenker			,SHOW_POS(POS_ACHS_BLENKER		,3))

static MAKE_ICB(icb_fire3      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY3,2))
static MAKE_ICB(icb_fire6      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG3    ,2))
static MAKE_ICB(icb_fire7      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_APU     ,2))
static MAKE_ICB(icb_fire8      		,SHOW_BTN(BTN_FIRE_LIGHTS_TEST		 ,2))
static MAKE_ICB(icb_achs_right		,SHOW_BTN(BTN_CLOCK_START			 ,2))
static MAKE_ICB(icb_achs_left		,SHOW_BTN(BTN_CLOCK_HRONO			 ,2))

static MAKE_ICB(icb_lamp01,SHOW_LMP(LMP_ENG1_BAY_ON_FIRE	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp02,SHOW_LMP(LMP_ENG2_BAY_ON_FIRE	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp03,SHOW_LMP(LMP_ENG3_BAY_ON_FIRE	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp04,SHOW_LMP(LMP_ENG1_ON_FIRE		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp05,SHOW_LMP(LMP_ENG2_ON_FIRE		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp06,SHOW_LMP(LMP_ENG3_ON_FIRE		,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp07,SHOW_LMP(LMP_EXTING1_VALVE_OPEN1	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp08,SHOW_LMP(LMP_EXTING2_VALVE_OPEN1	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp09,SHOW_LMP(LMP_EXTING3_VALVE_OPEN1	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp10,SHOW_LMP(LMP_EXTING1_VALVE_OPEN2	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp11,SHOW_LMP(LMP_EXTING2_VALVE_OPEN2	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))
static MAKE_ICB(icb_lamp12,SHOW_LMP(LMP_EXTING3_VALVE_OPEN2	,BTN_GET(BTN_FIRE_LIGHTS_TEST),3))

//////////////////////////////////////////////////////////////////////////

// ������
static MAKE_MSCB(mcb_fire3				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY3	))
static MAKE_MSCB(mcb_fire6				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG3		))
static MAKE_MSCB(mcb_fire7				,PRS_BTN(BTN_MAN_FIRE_EXTNG_APU			))
static MAKE_MSCB(mcb_fire8				,PRS_BTN(BTN_FIRE_LIGHTS_TEST			)) 
static BOOL FSAPI mcb_clock_left(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) {
		BTN_SET(BTN_CLOCK_HRONO,0);
		POS_INC(POS_ACHS_BLENKER);
	} else {
		BTN_SET(BTN_CLOCK_HRONO,1);
	}
	return TRUE;
}

static BOOL FSAPI mcb_clock_right(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) {
		BTN_SET(BTN_CLOCK_START,0);
		POS_INC(POS_ACHS_TIMER);
	} else {
		BTN_SET(BTN_CLOCK_START,1);
	}
	return TRUE;
}

static BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS0_01,0);
		POS_SET(POS_CRS0_02,0);
		POS_SET(POS_CRS0_03,0);
		POS_SET(POS_CRS0_04,0);
		POS_SET(POS_YOKE0_ICON,0);
	}
	return true;
}

BOOL FSAPI mcb_clock_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P19);
	return true;
}

static MAKE_TCB(tcb_01	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG_BAY3		))
static MAKE_TCB(tcb_02	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG3			))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_MAN_FIRE_EXTNG_APU			))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_FIRE_LIGHTS_TEST			))
static MAKE_TCB(tcb_05	,BTN_TT(BTN_CLOCK_HRONO					))
static MAKE_TCB(tcb_06	,BTN_TT(BTN_CLOCK_START					))
static MAKE_TCB(tcb_07	,LMP_TT(LMP_ENG1_BAY_ON_FIRE			))
static MAKE_TCB(tcb_08	,LMP_TT(LMP_ENG2_BAY_ON_FIRE			))
static MAKE_TCB(tcb_09	,LMP_TT(LMP_ENG3_BAY_ON_FIRE			))
static MAKE_TCB(tcb_10	,LMP_TT(LMP_ENG1_ON_FIRE				))
static MAKE_TCB(tcb_11	,LMP_TT(LMP_ENG2_ON_FIRE				))
static MAKE_TCB(tcb_12	,LMP_TT(LMP_ENG3_ON_FIRE				))
static MAKE_TCB(tcb_13	,LMP_TT(LMP_EXTING1_VALVE_OPEN1			))
static MAKE_TCB(tcb_14	,LMP_TT(LMP_EXTING2_VALVE_OPEN1			))
static MAKE_TCB(tcb_15	,LMP_TT(LMP_EXTING3_VALVE_OPEN1			))
static MAKE_TCB(tcb_16	,LMP_TT(LMP_EXTING1_VALVE_OPEN2			))
static MAKE_TCB(tcb_17	,LMP_TT(LMP_EXTING2_VALVE_OPEN2			))
static MAKE_TCB(tcb_18	,LMP_TT(LMP_EXTING3_VALVE_OPEN2			))
static MAKE_TCB(tcb_19	,NDL_TT(NDL_MAIN_MIN					))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_5,HELP_NONE,0,0)

MOUSE_PBOX(0,0,P0_BACKGROUND5_D_SX,P0_BACKGROUND5_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_TBOX("19",1319-OFFX,  56-OFFY, 130, 130,CURSOR_HAND,MOUSE_LR,mcb_clock_big)    // NDL_MAIN_MIN					

MOUSE_TBOX( "1", 1193-OFFX,    0-OFFY, 38, 30,CURSOR_HAND,MOUSE_DLR,mcb_fire3)
MOUSE_TBOX( "2", 1182-OFFX,   30-OFFY, 34, 29,CURSOR_HAND,MOUSE_DLR,mcb_fire6)
MOUSE_TBOX( "3", 1223-OFFX,   30-OFFY, 37, 29,CURSOR_HAND,MOUSE_DLR,mcb_fire7)
MOUSE_TBOX( "4", 1237-OFFX,    0-OFFY, 38, 30,CURSOR_HAND,MOUSE_DLR,mcb_fire8)
MOUSE_TBOX( "5", 1322-OFFX,  142-OFFY, 23, 23,CURSOR_HAND,MOUSE_DLR,mcb_clock_left)
MOUSE_TBOX( "6", 1451-OFFX,  143-OFFY, 25, 23,CURSOR_HAND,MOUSE_DLR,mcb_clock_right)

// �����
MOUSE_TTPB(  "7", 1339-OFFX,    0-OFFY, 50,37)    // LMP_ENG1_BAY_ON_FIRE        		
MOUSE_TTPB(  "8", 1389-OFFX,    0-OFFY, 40,37)	// LMP_ENG2_BAY_ON_FIRE        		
MOUSE_TTPB(  "9", 1429-OFFX,    0-OFFY, 46,37)	// LMP_ENG3_BAY_ON_FIRE        		
MOUSE_TTPB( "10", 1494-OFFX,    0-OFFY, 45,37)	// LMP_ENG1_ON_FIRE            		
MOUSE_TTPB( "11", 1539-OFFX,    0-OFFY, 38,37)	// LMP_ENG2_ON_FIRE            		
MOUSE_TTPB( "12", 1577-OFFX,    0-OFFY, 23,37)	// LMP_ENG3_ON_FIRE            		
MOUSE_TTPB( "13", 1327-OFFX,   37-OFFY, 48,34)	// LMP_EXTING1_VALVE_OPEN1     		
MOUSE_TTPB( "14", 1375-OFFX,   37-OFFY, 38,34)	// LMP_EXTING2_VALVE_OPEN1     		
MOUSE_TTPB( "15", 1413-OFFX,   37-OFFY, 47,34)	// LMP_EXTING3_VALVE_OPEN1     		
MOUSE_TTPB( "16", 1481-OFFX,   37-OFFY, 40,34)	// LMP_EXTING1_VALVE_OPEN2     		
MOUSE_TTPB( "17", 1521-OFFX,   37-OFFY, 37,34)	// LMP_EXTING2_VALVE_OPEN2     		
MOUSE_TTPB( "18", 1558-OFFX,   37-OFFY, 42,34)	// LMP_EXTING3_VALVE_OPEN2     		

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p0_5_list; 
GAUGE_HEADER_FS700_EX(P0_BACKGROUND5_D_SX, "p0_5", &p0_5_list, rect_p0_5, 0, 0, 0, 0, p0_5); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_lamp01				,P0_LMP_01_D_00			,NULL						,1339-OFFX,   0-OFFY,		icb_lamp01		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp02				,P0_LMP_02_D_00			,&l_p0_lamp01				,1389-OFFX,   0-OFFY,		icb_lamp02		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp03				,P0_LMP_03_D_00			,&l_p0_lamp02				,1429-OFFX,   0-OFFY,		icb_lamp03		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp04				,P0_LMP_04_D_00			,&l_p0_lamp03				,1494-OFFX,   0-OFFY,		icb_lamp04		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp05				,P0_LMP_05_D_00			,&l_p0_lamp04				,1539-OFFX,   0-OFFY,		icb_lamp05		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp06				,P0_LMP_06_D_00			,&l_p0_lamp05				,1577-OFFX,   0-OFFY,		icb_lamp06		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp07				,P0_LMP_07_D_00			,&l_p0_lamp06				,1327-OFFX,  37-OFFY,		icb_lamp07		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp08				,P0_LMP_08_D_00			,&l_p0_lamp07				,1375-OFFX,  37-OFFY,		icb_lamp08		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp09				,P0_LMP_09_D_00			,&l_p0_lamp08				,1413-OFFX,  37-OFFY,		icb_lamp09		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp10				,P0_LMP_10_D_00			,&l_p0_lamp09				,1481-OFFX,  37-OFFY,		icb_lamp10		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp11				,P0_LMP_11_D_00			,&l_p0_lamp10				,1521-OFFX,  37-OFFY,		icb_lamp11		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp12				,P0_LMP_12_D_00			,&l_p0_lamp11				,1558-OFFX,  37-OFFY,		icb_lamp12		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(fire3					,P0_BTN_FIRE3_D_00		,&l_p0_lamp12				,1193-OFFX,   0-OFFY,		icb_fire3		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire6					,P0_BTN_FIRE6_D_00		,&l_fire3					,1182-OFFX,  30-OFFY,		icb_fire6		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire7					,P0_BTN_FIRE7_D_00		,&l_fire6					,1223-OFFX,  30-OFFY,		icb_fire7		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire8					,P0_BTN_FIRE8_D_00		,&l_fire7					,1237-OFFX,   0-OFFY,		icb_fire8		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(achs_right				,P0_BTN_ACHSRIGHT_D_00	,&l_fire8					,1451-OFFX, 143-OFFY,		icb_achs_right	,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(achs_left				,P0_BTN_ACHSLEFT_D_00	,&l_achs_right				,1322-OFFX, 142-OFFY,		icb_achs_left	,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(cov_p0_achs			,P0_COV_ACHS_D			,&l_achs_left				,1392-OFFX,  93-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX, p0_5)		
MY_NEEDLE2	(ndl_p0_achs_secD		,P0_NDL_ACHSSEC_D		,&l_cov_p0_achs				,1397-OFFX, 123-OFFY,3,5,	icb_secD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_secM		,P0_NDL_ACHSSEC_M		,&l_ndl_p0_achs_secD		,1397-OFFX, 123-OFFY,3,5,	icb_secM		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_secP		,P0_NDL_ACHSSEC_P		,&l_ndl_p0_achs_secM		,1397-OFFX, 123-OFFY,3,5,	icb_secP		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_secR		,P0_NDL_ACHSSEC_R		,&l_ndl_p0_achs_secP		,1397-OFFX, 123-OFFY,3,5,	icb_secR		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minD		,P0_NDL_ACHSMIN_D		,&l_ndl_p0_achs_secR		,1397-OFFX, 123-OFFY,3,5,	icb_minD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minM		,P0_NDL_ACHSMIN_M		,&l_ndl_p0_achs_minD		,1397-OFFX, 123-OFFY,3,5,	icb_minM		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minP		,P0_NDL_ACHSMIN_P		,&l_ndl_p0_achs_minM		,1397-OFFX, 123-OFFY,3,5,	icb_minP		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minR		,P0_NDL_ACHSMIN_R		,&l_ndl_p0_achs_minP		,1397-OFFX, 123-OFFY,3,5,	icb_minR		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursD		,P0_NDL_ACHSHOURS_D		,&l_ndl_p0_achs_minR		,1397-OFFX, 123-OFFY,2,6,	icb_hrsD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursM		,P0_NDL_ACHSHOURS_M		,&l_ndl_p0_achs_hoursD		,1397-OFFX, 123-OFFY,2,6,	icb_hrsM		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursP		,P0_NDL_ACHSHOURS_P		,&l_ndl_p0_achs_hoursM		,1397-OFFX, 123-OFFY,2,6,	icb_hrsP		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursR		,P0_NDL_ACHSHOURS_R		,&l_ndl_p0_achs_hoursP		,1397-OFFX, 123-OFFY,2,6,	icb_hrsR		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secD	,P0_NDL_ACHSMINISEC_D	,&l_ndl_p0_achs_hoursR		,1396-OFFX, 149-OFFY,7,2,	icb_mini_secD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secM	,P0_NDL_ACHSMINISEC_M	,&l_ndl_p0_achs_mini_secD	,1396-OFFX, 149-OFFY,7,2,	icb_mini_secM	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secP	,P0_NDL_ACHSMINISEC_P	,&l_ndl_p0_achs_mini_secM	,1396-OFFX, 149-OFFY,7,2,	icb_mini_secP	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secR	,P0_NDL_ACHSMINISEC_R	,&l_ndl_p0_achs_mini_secP	,1396-OFFX, 149-OFFY,7,2,	icb_mini_secR	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minD	,P0_NDL_ACHSMINIMIN_D	,&l_ndl_p0_achs_mini_secR	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minM	,P0_NDL_ACHSMINIMIN_M	,&l_ndl_p0_achs_mini_minD	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minM	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minP	,P0_NDL_ACHSMINIMIN_P	,&l_ndl_p0_achs_mini_minM	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minP	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minR	,P0_NDL_ACHSMINIMIN_R	,&l_ndl_p0_achs_mini_minP	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minR	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursD,P0_NDL_ACHSMINIHOURS_D	,&l_ndl_p0_achs_mini_minR	,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursM,P0_NDL_ACHSMINIHOURS_M	,&l_ndl_p0_achs_mini_hoursD,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsM	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursP,P0_NDL_ACHSMINIHOURS_P	,&l_ndl_p0_achs_mini_hoursM,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsP	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursR,P0_NDL_ACHSMINIHOURS_R	,&l_ndl_p0_achs_mini_hoursP,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsR	,0,18, p0_5)	
MY_ICON2	(blenker_p0_achs		,P0_BLK_ACHSMODE_D_00	,&l_ndl_p0_achs_mini_hoursR,1390-OFFX,  97-OFFY,		icb_blenker		,3*PANEL_LIGHT_MAX, p0_5)     
MY_ICON2	(p0_5Ico				,P0_BACKGROUND5_D		,&l_blenker_p0_achs			,		 0,		   0,		icb_Ico			,PANEL_LIGHT_MAX	, p0_5) 
MY_STATIC2	(p0_5bg,p0_5_list		,P0_BACKGROUND5			,&l_p0_5Ico					, p0_5);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p0_lamp01				,P0_LMP_01_D_00			,NULL						,1339-OFFX,   0-OFFY,		icb_lamp01		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp02				,P0_LMP_02_D_00			,&l_p0_lamp01				,1389-OFFX,   0-OFFY,		icb_lamp02		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp03				,P0_LMP_03_D_00			,&l_p0_lamp02				,1429-OFFX,   0-OFFY,		icb_lamp03		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp04				,P0_LMP_04_D_00			,&l_p0_lamp03				,1494-OFFX,   0-OFFY,		icb_lamp04		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp05				,P0_LMP_05_D_00			,&l_p0_lamp04				,1539-OFFX,   0-OFFY,		icb_lamp05		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp06				,P0_LMP_06_D_00			,&l_p0_lamp05				,1577-OFFX,   0-OFFY,		icb_lamp06		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp07				,P0_LMP_07_D_00			,&l_p0_lamp06				,1327-OFFX,  37-OFFY,		icb_lamp07		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp08				,P0_LMP_08_D_00			,&l_p0_lamp07				,1375-OFFX,  37-OFFY,		icb_lamp08		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp09				,P0_LMP_09_D_00			,&l_p0_lamp08				,1413-OFFX,  37-OFFY,		icb_lamp09		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp10				,P0_LMP_10_D_00			,&l_p0_lamp09				,1481-OFFX,  37-OFFY,		icb_lamp10		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp11				,P0_LMP_11_D_00			,&l_p0_lamp10				,1521-OFFX,  37-OFFY,		icb_lamp11		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp12				,P0_LMP_12_D_00			,&l_p0_lamp11				,1558-OFFX,  37-OFFY,		icb_lamp12		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(fire3					,P0_BTN_FIRE3_D_00		,&l_p0_lamp12				,1193-OFFX,   0-OFFY,		icb_fire3		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire6					,P0_BTN_FIRE6_D_00		,&l_fire3					,1182-OFFX,  30-OFFY,		icb_fire6		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire7					,P0_BTN_FIRE7_D_00		,&l_fire6					,1223-OFFX,  30-OFFY,		icb_fire7		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire8					,P0_BTN_FIRE8_D_00		,&l_fire7					,1237-OFFX,   0-OFFY,		icb_fire8		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(achs_right				,P0_BTN_ACHSRIGHT_D_00	,&l_fire8					,1451-OFFX, 143-OFFY,		icb_achs_right	,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(achs_left				,P0_BTN_ACHSLEFT_D_00	,&l_achs_right				,1322-OFFX, 142-OFFY,		icb_achs_left	,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(cov_p0_achs			,P0_COV_ACHS_D			,&l_achs_left				,1392-OFFX,  93-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX, p0_5)		
MY_NEEDLE2	(ndl_p0_achs_secD		,P0_NDL_ACHSSEC_D		,&l_cov_p0_achs			,1397-OFFX, 123-OFFY,3,5,	icb_secD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_secP		,P0_NDL_ACHSSEC_P		,&l_ndl_p0_achs_secD		,1397-OFFX, 123-OFFY,3,5,	icb_secP		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minD		,P0_NDL_ACHSMIN_D		,&l_ndl_p0_achs_secP		,1397-OFFX, 123-OFFY,3,5,	icb_minD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minP		,P0_NDL_ACHSMIN_P		,&l_ndl_p0_achs_minD		,1397-OFFX, 123-OFFY,3,5,	icb_minP		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursD		,P0_NDL_ACHSHOURS_D		,&l_ndl_p0_achs_minP		,1397-OFFX, 123-OFFY,2,6,	icb_hrsD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursP		,P0_NDL_ACHSHOURS_P		,&l_ndl_p0_achs_hoursD		,1397-OFFX, 123-OFFY,2,6,	icb_hrsP		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secD	,P0_NDL_ACHSMINISEC_D	,&l_ndl_p0_achs_hoursP		,1396-OFFX, 149-OFFY,7,2,	icb_mini_secD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secP	,P0_NDL_ACHSMINISEC_P	,&l_ndl_p0_achs_mini_secD	,1396-OFFX, 149-OFFY,7,2,	icb_mini_secP	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minD	,P0_NDL_ACHSMINIMIN_D	,&l_ndl_p0_achs_mini_secP	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minP	,P0_NDL_ACHSMINIMIN_P	,&l_ndl_p0_achs_mini_minD	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minP	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursD,P0_NDL_ACHSMINIHOURS_D	,&l_ndl_p0_achs_mini_minP	,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursP,P0_NDL_ACHSMINIHOURS_P	,&l_ndl_p0_achs_mini_hoursD ,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsP	,0,18, p0_5)	
MY_ICON2	(blenker_p0_achs		,P0_BLK_ACHSMODE_D_00	,&l_ndl_p0_achs_mini_hoursP ,1390-OFFX,  97-OFFY,		icb_blenker		,3*PANEL_LIGHT_MAX, p0_5)     
MY_ICON2	(p0_5Ico				,P0_BACKGROUND5_D		,&l_blenker_p0_achs		,		 0,		   0,		icb_Ico			,PANEL_LIGHT_MAX	, p0_5) 
MY_STATIC2	(p0_5bg,p0_5_list		,P0_BACKGROUND5			,&l_p0_5Ico				, p0_5);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p0_lamp01				,P0_LMP_01_D_00			,NULL						,1339-OFFX,   0-OFFY,		icb_lamp01		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp02				,P0_LMP_02_D_00			,&l_p0_lamp01				,1389-OFFX,   0-OFFY,		icb_lamp02		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp03				,P0_LMP_03_D_00			,&l_p0_lamp02				,1429-OFFX,   0-OFFY,		icb_lamp03		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp04				,P0_LMP_04_D_00			,&l_p0_lamp03				,1494-OFFX,   0-OFFY,		icb_lamp04		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp05				,P0_LMP_05_D_00			,&l_p0_lamp04				,1539-OFFX,   0-OFFY,		icb_lamp05		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp06				,P0_LMP_06_D_00			,&l_p0_lamp05				,1577-OFFX,   0-OFFY,		icb_lamp06		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp07				,P0_LMP_07_D_00			,&l_p0_lamp06				,1327-OFFX,  37-OFFY,		icb_lamp07		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp08				,P0_LMP_08_D_00			,&l_p0_lamp07				,1375-OFFX,  37-OFFY,		icb_lamp08		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp09				,P0_LMP_09_D_00			,&l_p0_lamp08				,1413-OFFX,  37-OFFY,		icb_lamp09		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp10				,P0_LMP_10_D_00			,&l_p0_lamp09				,1481-OFFX,  37-OFFY,		icb_lamp10		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp11				,P0_LMP_11_D_00			,&l_p0_lamp10				,1521-OFFX,  37-OFFY,		icb_lamp11		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp12				,P0_LMP_12_D_00			,&l_p0_lamp11				,1558-OFFX,  37-OFFY,		icb_lamp12		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(fire3					,P0_BTN_FIRE3_D_00		,&l_p0_lamp12				,1193-OFFX,   0-OFFY,		icb_fire3		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire6					,P0_BTN_FIRE6_D_00		,&l_fire3					,1182-OFFX,  30-OFFY,		icb_fire6		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire7					,P0_BTN_FIRE7_D_00		,&l_fire6					,1223-OFFX,  30-OFFY,		icb_fire7		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire8					,P0_BTN_FIRE8_D_00		,&l_fire7					,1237-OFFX,   0-OFFY,		icb_fire8		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(achs_right				,P0_BTN_ACHSRIGHT_D_00	,&l_fire8					,1451-OFFX, 143-OFFY,		icb_achs_right	,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(achs_left				,P0_BTN_ACHSLEFT_D_00	,&l_achs_right				,1322-OFFX, 142-OFFY,		icb_achs_left	,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(cov_p0_achs			,P0_COV_ACHS_D			,&l_achs_left				,1392-OFFX,  93-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX, p0_5)		
MY_NEEDLE2	(ndl_p0_achs_secD		,P0_NDL_ACHSSEC_D		,&l_cov_p0_achs			,1397-OFFX, 123-OFFY,3,5,	icb_secD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_secR		,P0_NDL_ACHSSEC_R		,&l_ndl_p0_achs_secD		,1397-OFFX, 123-OFFY,3,5,	icb_secR		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minD		,P0_NDL_ACHSMIN_D		,&l_ndl_p0_achs_secR		,1397-OFFX, 123-OFFY,3,5,	icb_minD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minR		,P0_NDL_ACHSMIN_R		,&l_ndl_p0_achs_minD		,1397-OFFX, 123-OFFY,3,5,	icb_minR		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursD		,P0_NDL_ACHSHOURS_D		,&l_ndl_p0_achs_minR		,1397-OFFX, 123-OFFY,2,6,	icb_hrsD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursR		,P0_NDL_ACHSHOURS_R		,&l_ndl_p0_achs_hoursD		,1397-OFFX, 123-OFFY,2,6,	icb_hrsR		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secD	,P0_NDL_ACHSMINISEC_D	,&l_ndl_p0_achs_hoursR		,1396-OFFX, 149-OFFY,7,2,	icb_mini_secD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secR	,P0_NDL_ACHSMINISEC_R	,&l_ndl_p0_achs_mini_secD	,1396-OFFX, 149-OFFY,7,2,	icb_mini_secR	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minD	,P0_NDL_ACHSMINIMIN_D	,&l_ndl_p0_achs_mini_secR	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minR	,P0_NDL_ACHSMINIMIN_R	,&l_ndl_p0_achs_mini_minD	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minR	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursD,P0_NDL_ACHSMINIHOURS_D	,&l_ndl_p0_achs_mini_minR	,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursR,P0_NDL_ACHSMINIHOURS_R	,&l_ndl_p0_achs_mini_hoursD ,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsR	,0,18, p0_5)	
MY_ICON2	(blenker_p0_achs		,P0_BLK_ACHSMODE_D_00	,&l_ndl_p0_achs_mini_hoursR ,1390-OFFX,  97-OFFY,		icb_blenker		,3*PANEL_LIGHT_MAX, p0_5)     
MY_ICON2	(p0_5Ico				,P0_BACKGROUND5_D		,&l_blenker_p0_achs		,		 0,		   0,		icb_Ico			,PANEL_LIGHT_MAX	, p0_5) 
MY_STATIC2	(p0_5bg,p0_5_list		,P0_BACKGROUND5			,&l_p0_5Ico				, p0_5);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p0_lamp01				,P0_LMP_01_D_00			,NULL						,1339-OFFX,   0-OFFY,		icb_lamp01		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp02				,P0_LMP_02_D_00			,&l_p0_lamp01				,1389-OFFX,   0-OFFY,		icb_lamp02		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp03				,P0_LMP_03_D_00			,&l_p0_lamp02				,1429-OFFX,   0-OFFY,		icb_lamp03		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp04				,P0_LMP_04_D_00			,&l_p0_lamp03				,1494-OFFX,   0-OFFY,		icb_lamp04		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp05				,P0_LMP_05_D_00			,&l_p0_lamp04				,1539-OFFX,   0-OFFY,		icb_lamp05		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp06				,P0_LMP_06_D_00			,&l_p0_lamp05				,1577-OFFX,   0-OFFY,		icb_lamp06		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp07				,P0_LMP_07_D_00			,&l_p0_lamp06				,1327-OFFX,  37-OFFY,		icb_lamp07		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp08				,P0_LMP_08_D_00			,&l_p0_lamp07				,1375-OFFX,  37-OFFY,		icb_lamp08		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp09				,P0_LMP_09_D_00			,&l_p0_lamp08				,1413-OFFX,  37-OFFY,		icb_lamp09		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp10				,P0_LMP_10_D_00			,&l_p0_lamp09				,1481-OFFX,  37-OFFY,		icb_lamp10		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp11				,P0_LMP_11_D_00			,&l_p0_lamp10				,1521-OFFX,  37-OFFY,		icb_lamp11		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(p0_lamp12				,P0_LMP_12_D_00			,&l_p0_lamp11				,1558-OFFX,  37-OFFY,		icb_lamp12		,3*PANEL_LIGHT_MAX, p0_5)	
MY_ICON2	(fire3					,P0_BTN_FIRE3_D_00		,&l_p0_lamp12				,1193-OFFX,   0-OFFY,		icb_fire3		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire6					,P0_BTN_FIRE6_D_00		,&l_fire3					,1182-OFFX,  30-OFFY,		icb_fire6		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire7					,P0_BTN_FIRE7_D_00		,&l_fire6					,1223-OFFX,  30-OFFY,		icb_fire7		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(fire8					,P0_BTN_FIRE8_D_00		,&l_fire7					,1237-OFFX,   0-OFFY,		icb_fire8		,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(achs_right				,P0_BTN_ACHSRIGHT_D_00	,&l_fire8					,1451-OFFX, 143-OFFY,		icb_achs_right	,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(achs_left				,P0_BTN_ACHSLEFT_D_00	,&l_achs_right				,1322-OFFX, 142-OFFY,		icb_achs_left	,2*PANEL_LIGHT_MAX, p0_5)			
MY_ICON2	(cov_p0_achs			,P0_COV_ACHS_D			,&l_achs_left				,1392-OFFX,  93-OFFY,		icb_Ico			,1*PANEL_LIGHT_MAX, p0_5)		
MY_NEEDLE2	(ndl_p0_achs_secD		,P0_NDL_ACHSSEC_D		,&l_cov_p0_achs			,1397-OFFX, 123-OFFY,3,5,	icb_secD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_secM		,P0_NDL_ACHSSEC_M		,&l_ndl_p0_achs_secD		,1397-OFFX, 123-OFFY,3,5,	icb_secM		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minD		,P0_NDL_ACHSMIN_D		,&l_ndl_p0_achs_secM		,1397-OFFX, 123-OFFY,3,5,	icb_minD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_minM		,P0_NDL_ACHSMIN_M		,&l_ndl_p0_achs_minD		,1397-OFFX, 123-OFFY,3,5,	icb_minM		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursD		,P0_NDL_ACHSHOURS_D		,&l_ndl_p0_achs_minM		,1397-OFFX, 123-OFFY,2,6,	icb_hrsD		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_hoursM		,P0_NDL_ACHSHOURS_M		,&l_ndl_p0_achs_hoursD		,1397-OFFX, 123-OFFY,2,6,	icb_hrsM		,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secD	,P0_NDL_ACHSMINISEC_D	,&l_ndl_p0_achs_hoursM		,1396-OFFX, 149-OFFY,7,2,	icb_mini_secD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_secM	,P0_NDL_ACHSMINISEC_M	,&l_ndl_p0_achs_mini_secD	,1396-OFFX, 149-OFFY,7,2,	icb_mini_secM	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minD	,P0_NDL_ACHSMINIMIN_D	,&l_ndl_p0_achs_mini_secM	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_minM	,P0_NDL_ACHSMINIMIN_M	,&l_ndl_p0_achs_mini_minD	,1398-OFFX,  97-OFFY,1,3,	icb_mini_minM	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursD,P0_NDL_ACHSMINIHOURS_D	,&l_ndl_p0_achs_mini_minM	,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsD	,0,18, p0_5)	
MY_NEEDLE2	(ndl_p0_achs_mini_hoursM,P0_NDL_ACHSMINIHOURS_M	,&l_ndl_p0_achs_mini_hoursD ,1398-OFFX,  97-OFFY,2,2,	icb_mini_hrsM	,0,18, p0_5)	
MY_ICON2	(blenker_p0_achs		,P0_BLK_ACHSMODE_D_00	,&l_ndl_p0_achs_mini_hoursM ,1390-OFFX,  97-OFFY,		icb_blenker		,3*PANEL_LIGHT_MAX, p0_5)     
MY_ICON2	(p0_5Ico				,P0_BACKGROUND5_D		,&l_blenker_p0_achs		,		 0,		   0,		icb_Ico			,PANEL_LIGHT_MAX	, p0_5) 
MY_STATIC2	(p0_5bg,p0_5_list		,P0_BACKGROUND5			,&l_p0_5Ico				, p0_5);
#endif


#endif