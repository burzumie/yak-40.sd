/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "P0.h"

#ifdef _DEBUG
DLLMAIND("F:\\MemLeaks\\Yak40\\P0.log")
#else
DLLMAIN()
#endif

double FSAPI icb_Ico(PELEMENT_ICON pelement) 
{
	CHK_BRT();
	return POS_GET(POS_PANEL_STATE); 
} 

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
#ifndef ONLY3D
		(&p0_1),
		(&p0_2),
		(&p0_3),
		(&p0_4),
		(&p0_5),
		(&p0_6),
		(&p0_7),
		(&p0_9),
		(&p0_10),
		(&p0_11),
		(&p0_12),
		(&p0_13),
#endif
		(&p0_8),
		0
	}
};

