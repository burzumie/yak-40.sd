/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P0.h"

#define OFFX	1170
#define OFFY	470

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_crs_p0_03,SHOW_POSM(POS_CRS0_03,1,0))
static MAKE_ICB(icb_crs_p0_04,SHOW_POSM(POS_CRS0_04,1,0))
static MAKE_ICB(icb_tablo34,SHOW_TBL(TBL_FIRE				,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo35,SHOW_TBL(TBL_CABIN_CREW			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo36,SHOW_TBL(TBL_ENG_OVERHEAT		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo37,SHOW_TBL(TBL_AIR_STARTER_ON		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo01,SHOW_TBG(TBG_WARN_GEAR			,BTN_GET(BTN_GEAR_LIGHTS_TEST),3))
static MAKE_ICB(icb_tablo02,SHOW_TBG(TBG_GEAR1_UP			,BTN_GET(BTN_GEAR_LIGHTS_TEST),3))
static MAKE_ICB(icb_tablo03,SHOW_TBG(TBG_GEAR2_UP			,BTN_GET(BTN_GEAR_LIGHTS_TEST),3))
static MAKE_ICB(icb_tablo04,SHOW_TBG(TBG_GEAR3_UP			,BTN_GET(BTN_GEAR_LIGHTS_TEST),3))
static MAKE_ICB(icb_tablo05,SHOW_TBG(TBG_WARN_FLAP			,BTN_GET(BTN_GEAR_LIGHTS_TEST),3))
static MAKE_ICB(icb_tablo06,SHOW_TBG(TBG_GEAR1_DOWN			,BTN_GET(BTN_GEAR_LIGHTS_TEST),3))
static MAKE_ICB(icb_tablo07,SHOW_TBG(TBG_GEAR2_DOWN			,BTN_GET(BTN_GEAR_LIGHTS_TEST),3))
static MAKE_ICB(icb_tablo08,SHOW_TBG(TBG_GEAR3_DOWN			,BTN_GET(BTN_GEAR_LIGHTS_TEST),3))
static MAKE_ICB(icb_fire9  ,SHOW_BTN(BTN_DIS_FIRE_ALARM     ,2))

// ������������
static NONLINEARITY tbl_stab[]={
	{{1277-OFFX,688-OFFY},  -9,0},
	{{1283-OFFX,694-OFFY},  -7,0},
	{{1286-OFFX,702-OFFY},  -5,0},
	{{1287-OFFX,711-OFFY},  -3,0},
	{{1287-OFFX,715-OFFY},  -1,0},
	{{1287-OFFX,720-OFFY},   1,0},
	{{1285-OFFX,724-OFFY},   3,0}
};

#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_stabD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_HTAIL_DEG);}else{return -3;}}else HIDE_IMAGE(pelement);return -1;)
#endif																																				   
#ifdef HAVE_MIX_PANEL																																   
static MAKE_NCB(icb_stabM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_HTAIL_DEG);}else{return -3;}}else HIDE_IMAGE(pelement);return -1;)
#endif																																				   
#ifdef HAVE_PLF_PANEL																																   
static MAKE_NCB(icb_stabP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_HTAIL_DEG);}else{return -3;}}else HIDE_IMAGE(pelement);return -1;)
#endif																																				   
#ifdef HAVE_RED_PANEL																																   
static MAKE_NCB(icb_stabR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_HTAIL_DEG);}else{return -3;}}else HIDE_IMAGE(pelement);return -1;)
#endif

// ��������
static NONLINEARITY tbl_flaps[]={
	{{1511-OFFX,682-OFFY},   0,0},
	{{1524-OFFX,701-OFFY},  20,0},
	{{1526-OFFX,715-OFFY},  35,0}
};

#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_flapsD			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_FLAPS);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_flapsM			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_FLAPS);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_flapsP			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_FLAPS);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_flapsR			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(AZS_GET(AZS_MANOM)){SHOW_NDL(NDL_FLAPS);}else{return 0;}}else HIDE_IMAGE(pelement);return -1;)
#endif

// ���
static NONLINEARITY tbl_fuel_press_eng1[]={
	{{1196-OFFX,1070-OFFY}, -10,0},
	{{1197-OFFX,1067-OFFY},   0,0},
	{{1237-OFFX,1044-OFFY},  50,0},
	{{1276-OFFX,1066-OFFY}, 100,0}
};					

static NONLINEARITY tbl_oil_press_eng1[]={
	{{1212-OFFX,1086-OFFY},   8,0},
	{{1224-OFFX,1098-OFFY},   6,0},
	{{1229-OFFX,1115-OFFY},   4,0},
	{{1225-OFFX,1131-OFFY},   2,0},
	{{1213-OFFX,1144-OFFY},   0,0},
	{{1209-OFFX,1144-OFFY}, -10,0}
};					

static NONLINEARITY tbl_oil_temp_eng1[]={
	{{1263-OFFX,1146-OFFY},-100,0},
	{{1260-OFFX,1144-OFFY}, -50,0},
	{{1248-OFFX,1131-OFFY},   0,0},
	{{1244-OFFX,1115-OFFY},  50,0},
	{{1248-OFFX,1099-OFFY}, 100,0},
	{{1261-OFFX,1086-OFFY}, 150,0}
};					

static NONLINEARITY tbl_fuel_press_eng2[]={
	{{1339-OFFX,1071-OFFY}, -10,0},
	{{1342-OFFX,1066-OFFY},   0,0},
	{{1383-OFFX,1043-OFFY},  50,0},
	{{1423-OFFX,1066-OFFY}, 100,0}
};					

static NONLINEARITY tbl_oil_press_eng2[]={
	{{1358-OFFX,1086-OFFY},   8,0},
	{{1370-OFFX,1098-OFFY},   6,0},
	{{1374-OFFX,1115-OFFY},   4,0},
	{{1370-OFFX,1131-OFFY},   2,0},
	{{1358-OFFX,1143-OFFY},   0,0},
	{{1355-OFFX,1145-OFFY}, -10,0}
};					

static NONLINEARITY tbl_oil_temp_eng2[]={
	{{1410-OFFX,1146-OFFY},-100,0},
	{{1406-OFFX,1143-OFFY}, -50,0},
	{{1394-OFFX,1131-OFFY},   0,0},
	{{1390-OFFX,1114-OFFY},  50,0},
	{{1394-OFFX,1098-OFFY}, 100,0},
	{{1406-OFFX,1086-OFFY}, 150,0}
};					

static NONLINEARITY tbl_fuel_press_eng3[]={
	{{1488-OFFX,1070-OFFY}, -10,0},
	{{1490-OFFX,1067-OFFY},   0,0},
	{{1530-OFFX,1043-OFFY},  50,0},
	{{1570-OFFX,1066-OFFY}, 100,0}
};					

static NONLINEARITY tbl_oil_press_eng3[]={
	{{1505-OFFX,1086-OFFY},   8,0},
	{{1517-OFFX,1098-OFFY},   6,0},
	{{1522-OFFX,1114-OFFY},   4,0},
	{{1518-OFFX,1132-OFFY},   2,0},
	{{1506-OFFX,1144-OFFY},   0,0},
	{{1502-OFFX,1145-OFFY}, -10,0}
};					

static NONLINEARITY tbl_oil_temp_eng3[]={
	{{1557-OFFX,1146-OFFY},-100,0},
	{{1554-OFFX,1143-OFFY}, -50,0},
	{{1541-OFFX,1131-OFFY},   0,0},
	{{1537-OFFX,1115-OFFY},  50,0},
	{{1541-OFFX,1099-OFFY}, 100,0},
	{{1553-OFFX,1086-OFFY}, 150,0}
};					

#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_fuel_press_eng1D,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_fuel_press_eng1M,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_fuel_press_eng1P,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_fuel_press_eng1R,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_oil_press_eng1D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_oil_press_eng1M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_oil_press_eng1P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_oil_press_eng1R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_oil_temp_eng1D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_oil_temp_eng1M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_oil_temp_eng1P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_oil_temp_eng1R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_fuel_press_eng2D,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_fuel_press_eng2M,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_fuel_press_eng2P,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_fuel_press_eng2R,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_oil_press_eng2D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_oil_press_eng2M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_oil_press_eng2P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_oil_press_eng2R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_oil_temp_eng2D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_oil_temp_eng2M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_oil_temp_eng2P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_oil_temp_eng2R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_fuel_press_eng3D,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_fuel_press_eng3M,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_fuel_press_eng3P,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_fuel_press_eng3R,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_FUEL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_oil_press_eng3D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_oil_press_eng3M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_oil_press_eng3P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_oil_press_eng3R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_PSI		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_oil_temp_eng3D	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_oil_temp_eng3M	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_oil_temp_eng3P	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_oil_temp_eng3R	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_OIL_TEMP		);}else HIDE_IMAGE(pelement);return -1;)
#endif

static NONLINEARITY tbl_eng1[]={
	{{1293-OFFX,900-OFFY},   0,0},
	{{1292-OFFX,965-OFFY},  20,0},
	{{1237-OFFX,997-OFFY},  40,0},
	{{1183-OFFX,965-OFFY},  60,0},
	{{1183-OFFX,902-OFFY},  80,0},
	{{1221-OFFX,873-OFFY},  95,0},
	{{1237-OFFX,869-OFFY}, 100,0}
};		  

static NONLINEARITY tbl_eng2[]={
	{{1439-OFFX,900-OFFY},   0,0},
	{{1437-OFFX,966-OFFY},  20,0},
	{{1383-OFFX,997-OFFY},  40,0},
	{{1330-OFFX,965-OFFY},  60,0},
	{{1330-OFFX,903-OFFY},  80,0},
	{{1366-OFFX,872-OFFY},  95,0},
	{{1383-OFFX,870-OFFY}, 100,0}
};		  

static NONLINEARITY tbl_eng3[]={
	{{1586-OFFX,900-OFFY},   0,0},
	{{1585-OFFX,965-OFFY},  20,0},
	{{1531-OFFX,998-OFFY},  40,0},
	{{1477-OFFX,965-OFFY},  60,0},
	{{1477-OFFX,903-OFFY},  80,0},
	{{1514-OFFX,873-OFFY},  95,0},
	{{1531-OFFX,869-OFFY}, 100,0}
};		  

#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_eng1_n1D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_eng1_n1M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_eng1_n1P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_eng1_n1R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_eng1_n2D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_eng1_n2M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_eng1_n2P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_eng1_n2R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_eng2_n1D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_eng2_n1M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_eng2_n1P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_eng2_n1R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_eng2_n2D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_eng2_n2M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_eng2_n2P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_eng2_n2R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_eng3_n1D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_eng3_n1M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_eng3_n1P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_eng3_n1R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N1			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_eng3_n2D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_eng3_n2M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_eng3_n2P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_eng3_n2R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_N2			);}else HIDE_IMAGE(pelement);return -1;)
#endif

static NONLINEARITY tbl_teng1[]={
	{{1248-OFFX,832-OFFY},   0,0},
	{{1257-OFFX,788-OFFY}, 300,0},
	{{1305-OFFX,787-OFFY}, 600,0},
	{{1314-OFFX,832-OFFY}, 900,0}
};

static NONLINEARITY tbl_teng2[]={
	{{1354-OFFX,832-OFFY},   0,0},
	{{1361-OFFX,788-OFFY}, 300,0},
	{{1410-OFFX,786-OFFY}, 600,0},
	{{1418-OFFX,832-OFFY}, 900,0}
};

static NONLINEARITY tbl_teng3[]={
	{{1460-OFFX,832-OFFY},   0,0},
	{{1468-OFFX,788-OFFY}, 300,0},
	{{1517-OFFX,786-OFFY}, 600,0},
	{{1525-OFFX,832-OFFY}, 900,0}
};

#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_teng1D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_teng1M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_teng1P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_teng1R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG1_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_teng2D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_teng2M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_teng2P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_teng2R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG2_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_teng3D			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_teng3M			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_teng3P			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_teng3R			,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);SHOW_NDL(NDL_ENG3_TEMP			);}else HIDE_IMAGE(pelement);return -1;)
#endif

#ifdef HAVE_DAY_PANEL
static MAKE_ICB(icb_CovED	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT(); SHOW_IMAGE(pelement);return 0;}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_ICB(icb_CovEM	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT(); SHOW_IMAGE(pelement);return 0;}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_ICB(icb_CovEP	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT(); SHOW_IMAGE(pelement);return 0;}else HIDE_IMAGE(pelement);return -1;)
#endif
#ifdef HAVE_RED_PANEL
static MAKE_ICB(icb_CovER	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT(); SHOW_IMAGE(pelement);return 0;}else HIDE_IMAGE(pelement);return -1;)
#endif

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_fire9				,PRS_BTN(BTN_DIS_FIRE_ALARM				))
static MAKE_MSCB(mcb_check_lamps_gear	,PRS_BTN(BTN_GEAR_LIGHTS_TEST			))

// �������
static BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS0_01,0);
		POS_SET(POS_CRS0_02,0);
		POS_SET(POS_CRS0_03,0);
		POS_SET(POS_CRS0_04,0);
		POS_SET(POS_YOKE0_ICON,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p0_03(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS0_03,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P2);
		panel_window_close_ident(IDENT_P0);
		panel_window_open_ident(IDENT_P4);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p0_04(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS0_04,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P2);
		panel_window_close_ident(IDENT_P0);
		panel_window_open_ident(IDENT_P1);
	}
	return true;
}

static BOOL FSAPI mcb_navpanel_toggle(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P23);
	return TRUE;
}

static MAKE_TCB(tcb_01	,POS_TT(POS_CRS0_03						))
static MAKE_TCB(tcb_02	,POS_TT(POS_CRS0_04						))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_DIS_FIRE_ALARM				))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_GEAR_LIGHTS_TEST			))
static MAKE_TCB(tcb_05	,TBL_TT(TBL_FIRE						))
static MAKE_TCB(tcb_06	,TBL_TT(TBL_CABIN_CREW					))
static MAKE_TCB(tcb_07	,TBL_TT(TBL_ENG_OVERHEAT				))
static MAKE_TCB(tcb_08	,TBL_TT(TBL_AIR_STARTER_ON				))
static MAKE_TCB(tcb_09	,TBG_TT(TBG_WARN_GEAR		            ))
static MAKE_TCB(tcb_10	,TBG_TT(TBG_GEAR1_UP		            ))
static MAKE_TCB(tcb_11	,TBG_TT(TBG_GEAR2_UP		            ))
static MAKE_TCB(tcb_12	,TBG_TT(TBG_GEAR3_UP		            ))
static MAKE_TCB(tcb_13	,TBG_TT(TBG_WARN_FLAP		            ))
static MAKE_TCB(tcb_14	,TBG_TT(TBG_GEAR1_DOWN					))
static MAKE_TCB(tcb_15	,TBG_TT(TBG_GEAR2_DOWN					))
static MAKE_TCB(tcb_16	,TBG_TT(TBG_GEAR3_DOWN					))
static MAKE_TCB(tcb_17	,NDL_TT(NDL_HTAIL_DEG					))
static MAKE_TCB(tcb_18	,NDL_TT(NDL_FLAPS						))
static MAKE_TCB(tcb_19	,NDL_TT(NDL_ENG1_TEMP					))
static MAKE_TCB(tcb_20	,NDL_TT(NDL_ENG2_TEMP					))
static MAKE_TCB(tcb_21	,NDL_TT(NDL_ENG3_TEMP					))
static MAKE_TCB(tcb_22	,NDL_TT(NDL_ENG1_N1						))
static MAKE_TCB(tcb_23	,NDL_TT(NDL_ENG2_N1						))
static MAKE_TCB(tcb_24	,NDL_TT(NDL_ENG3_N1						))
static MAKE_TCB(tcb_25	,NDL_TT(NDL_ENG1_FUEL_PSI				))
static MAKE_TCB(tcb_26	,NDL_TT(NDL_ENG2_FUEL_PSI				))
static MAKE_TCB(tcb_27	,NDL_TT(NDL_ENG3_FUEL_PSI				))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MAKE_TTA(tcb_20	)
MAKE_TTA(tcb_21	)
MAKE_TTA(tcb_22	) 
MAKE_TTA(tcb_23	)
MAKE_TTA(tcb_24	)
MAKE_TTA(tcb_25	)
MAKE_TTA(tcb_26	)
MAKE_TTA(tcb_27	) 
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p0_6,HELP_NONE,0,0)

// �������� ��������
MOUSE_PBOX(0,0,P0_BACKGROUND6_D_SX,P0_BACKGROUND6_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_PBOX(  0, 18,60,164-18,CURSOR_HAND,MOUSE_LR,mcb_navpanel_toggle)

// �������
MOUSE_TBOX(  "1",1407-OFFX, 1152-OFFY,MISC_CRS_P0_03_SX,MISC_CRS_P0_03_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p0_03)
MOUSE_TBOX(  "2",1550-OFFX,  555-OFFY,MISC_CRS_P0_04_SX,MISC_CRS_P0_04_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p0_04)

// �������
MOUSE_TTPB("17",1238-OFFX, 679-OFFY,  60,  60)    // NDL_HTAIL_DEG					
MOUSE_TTPB("18",1470-OFFX, 679-OFFY,  60,  60)    // NDL_FLAPS						
MOUSE_TTPB("19",1239-OFFX, 774-OFFY,  80,  80)    // NDL_ENG1_TEMP					
MOUSE_TTPB("20",1343-OFFX, 774-OFFY,  80,  80)    // NDL_ENG2_TEMP					
MOUSE_TTPB("21",1450-OFFX, 774-OFFY,  80,  80)    // NDL_ENG3_TEMP					
MOUSE_TTPB("22",1167-OFFX, 863-OFFY, 140, 140)    // NDL_ENG1_N1					
MOUSE_TTPB("23",1313-OFFX, 863-OFFY, 140, 140)    // NDL_ENG2_N1					
MOUSE_TTPB("24",1461-OFFX, 863-OFFY, 140, 140)    // NDL_ENG3_N1					
MOUSE_TTPB("25",1167-OFFX,1025-OFFY, 140, 140)    // NDL_ENG1_FUEL_PSI				
MOUSE_TTPB("26",1314-OFFX,1025-OFFY, 140, 140)    // NDL_ENG2_FUEL_PSI				
MOUSE_TTPB("27",1461-OFFX,1025-OFFY, 140, 140)    // NDL_ENG3_FUEL_PSI				

// ������      
MOUSE_TBOX( "3", 1173-OFFX,  831-OFFY, 33, 32,CURSOR_HAND,MOUSE_DLR,mcb_fire9)
MOUSE_TBOX( "4", 1377-OFFX,  718-OFFY, 30, 30,CURSOR_HAND,MOUSE_DLR,mcb_check_lamps_gear)

// ����� 
MOUSE_TTPB( "5", 1173-OFFX,  715-OFFY, 64,46)    // TBL_FIRE                      		
MOUSE_TTPB( "6", 1535-OFFX,  715-OFFY, 65,46)    // TBL_CABIN_CREW                		
MOUSE_TTPB( "7", 1535-OFFX,  761-OFFY, 65,47)    // TBL_ENG_OVERHEAT              		
MOUSE_TTPB( "8", 1535-OFFX,  808-OFFY, 65,48)    // TBL_AIR_STARTER_ON            		

// ����� �����
MOUSE_TTPB(  "9", 1309-OFFX,  669-OFFY, 48,36)    // TBG_WARN_GEAR		           		
MOUSE_TTPB( "10", 1357-OFFX,  669-OFFY, 21,34)    // TBG_GEAR1_UP		           		
MOUSE_TTPB( "11", 1378-OFFX,  662-OFFY, 24,30)    // TBG_GEAR2_UP		           		
MOUSE_TTPB( "12", 1402-OFFX,  669-OFFY, 21,34)    // TBG_GEAR3_UP		           		
MOUSE_TTPB( "13", 1423-OFFX,  669-OFFY, 43,34)    // TBG_WARN_FLAP		           		
MOUSE_TTPB( "14", 1357-OFFX,  703-OFFY, 21,26)    // TBG_GEAR1_DOWN		           		
MOUSE_TTPB( "15", 1378-OFFX,  692-OFFY, 24,37)    // TBG_GEAR2_DOWN		           		
MOUSE_TTPB( "16", 1402-OFFX,  703-OFFY, 21,26)    // TBG_GEAR3_DOWN	

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p0_6_list; 
GAUGE_HEADER_FS700_EX(P0_BACKGROUND6_D_SX, "p0_6", &p0_6_list, rect_p0_6, 0, 0, 0, 0, p0_6); 

#define GAU_ALL4(S,E,CX,CY,NX,NY,NXC,NYC,L) /**/ \
	MY_ICON2  (cov_p0_temp_eng##E##_##S	,P0_COV_TEMPENG##E##_##S	,L,CX-OFFX,CY-OFFY,icb_CovE##S,1, p0_6) \
	MY_NEEDLE2(ndl_p0_temp_eng##E##_##S	,P0_NDL_TEMPENG##E##_##S	,&l_cov_p0_temp_eng##E##_##S,NX-OFFX,NY-OFFY,NXC,NYC,icb_teng##E##S,tbl_teng##E##,18, p0_6)

#define GAU_ALL3(S,E,CX,CY,NX1,NY1,NCX1,NCY1,L) /**/ \
	MY_ICON2	 (cov_p0_rpm_eng##E##_##S	,P0_COV_RPMENG##E##_##S		,L,CX-OFFX,CY-OFFY,icb_CovE##S,1, p0_6)	\
	MY_NEEDLE2(ndl_p0_rpm_eng##E##_n2_##S,P0_NDL_RPMENG##E##N2_##S	,&l_cov_p0_rpm_eng##E##_##S,NX1-OFFX,NY1-OFFY,NCX1,NCY1,icb_eng##E##_n2##S,tbl_eng##E##,18, p0_6)	\
	MY_NEEDLE2(ndl_p0_rpm_eng##E##_n1_##S,P0_NDL_RPMENG##E##N1_##S	,&l_ndl_p0_rpm_eng##E##_n2_##S,NX1-OFFX,NY1-OFFY,NCX1,NCY1,icb_eng##E##_n1##S,tbl_eng##E##,18, p0_6)

#define GAU_ALL2(S,E,CX,CY,NX1,NY1,NCX1,NCY1,NX2,NY2,NCX2,NCY2,NX3,NY3,NCX3,NCY3,L)	/* */ \
	MY_ICON2  (cov_p0_emi_eng##E##_##S			,P0_COV_EMIENG##E##_##S			,L,										CX-OFFX,CY-OFFY,icb_CovE##S,1, p0_6) \
	MY_NEEDLE2(ndl_p0_emi_eng##E##_oil_temp_##S	,P0_NDL_EMIENG##E##OILTEMP_##S	,&l_cov_p0_emi_eng##E##_##S,			NX1-OFFX,NY1-OFFY,NCX1,NCY1,	icb_oil_temp_eng##E##S,		tbl_oil_temp_eng##E,18, p0_6) \
	MY_NEEDLE2(ndl_p0_emi_eng##E##_oil_press_##S	,P0_NDL_EMIENG##E##OILPSI_##S	,&l_ndl_p0_emi_eng##E##_oil_temp_##S,	NX2-OFFX,NY2-OFFY,NCX2,NCY2,	icb_oil_press_eng##E##S,	tbl_oil_press_eng##E,18, p0_6) \
	MY_NEEDLE2(ndl_p0_emi_eng##E##_fuel_press_##S,P0_NDL_EMIENG##E##FUELPSI_##S	,&l_ndl_p0_emi_eng##E##_oil_press_##S,	NX3-OFFX,NY3-OFFY,NCX3,NCY3,	icb_fuel_press_eng##E##S,	tbl_fuel_press_eng##E,18, p0_6)


#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
GAU_ALL2(D,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,NULL)
GAU_ALL2(D,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_D)
GAU_ALL2(D,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_D)
GAU_ALL2(M,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,&l_ndl_p0_emi_eng3_fuel_press_D)
GAU_ALL2(M,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_M)
GAU_ALL2(M,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_M)
GAU_ALL2(P,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,&l_ndl_p0_emi_eng3_fuel_press_M)
GAU_ALL2(P,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_P)
GAU_ALL2(P,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_P)
GAU_ALL2(R,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,&l_ndl_p0_emi_eng3_fuel_press_P)
GAU_ALL2(R,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_R)
GAU_ALL2(R,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_R)

GAU_ALL3(D,1,1231,928,1237,934,29,6,&l_ndl_p0_emi_eng3_fuel_press_R)
GAU_ALL3(D,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_D)
GAU_ALL3(D,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_D)
GAU_ALL3(M,1,1231,928,1237,934,29,6,&l_ndl_p0_rpm_eng3_n1_D)
GAU_ALL3(M,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_M)
GAU_ALL3(M,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_M)
GAU_ALL3(P,1,1231,928,1237,934,29,6,&l_ndl_p0_rpm_eng3_n1_M)
GAU_ALL3(P,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_P)
GAU_ALL3(P,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_P)
GAU_ALL3(R,1,1231,928,1237,934,29,6,&l_ndl_p0_rpm_eng3_n1_P)
GAU_ALL3(R,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_R)
GAU_ALL3(R,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_R)

GAU_ALL4(D,1,1262,798,1280,815,3,4,&l_ndl_p0_rpm_eng3_n1_R)
GAU_ALL4(D,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_D)
GAU_ALL4(D,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_D)
GAU_ALL4(M,1,1262,798,1280,815,3,4,&l_ndl_p0_temp_eng3_D)
GAU_ALL4(M,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_M)
GAU_ALL4(M,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_M)
GAU_ALL4(P,1,1262,798,1280,815,3,4,&l_ndl_p0_temp_eng3_M)
GAU_ALL4(P,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_P)
GAU_ALL4(P,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_P)
GAU_ALL4(R,1,1262,798,1280,815,3,4,&l_ndl_p0_temp_eng3_P)
GAU_ALL4(R,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_R)
GAU_ALL4(R,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_R)

MY_ICON2	(cov_p0_flaps       ,P0_COV_FLAPS_D         ,&l_ndl_p0_temp_eng3_R	,1472-OFFX, 679-OFFY,	icb_Ico,PANEL_LIGHT_MAX	, p0_6)	
MY_NEEDLE2	(ndl_p0_flapsD      ,P0_NDL_FLAPS_D         ,&l_cov_p0_flaps		,1492-OFFX, 711-OFFY, 3, 4,icb_flapsD	,tbl_flaps,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_flapsM      ,P0_NDL_FLAPS_M         ,&l_ndl_p0_flapsD		,1492-OFFX, 711-OFFY, 3, 4,icb_flapsM	,tbl_flaps,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_flapsP      ,P0_NDL_FLAPS_P         ,&l_ndl_p0_flapsM		,1492-OFFX, 711-OFFY, 3, 4,icb_flapsP	,tbl_flaps,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_flapsR      ,P0_NDL_FLAPS_R         ,&l_ndl_p0_flapsP		,1492-OFFX, 711-OFFY, 3, 4,icb_flapsR	,tbl_flaps,18, p0_6)	 
MY_ICON2	(cov_p0_stab        ,P0_COV_HTAIL_D         ,&l_ndl_p0_flapsR		,1240-OFFX, 681-OFFY,	icb_Ico,PANEL_LIGHT_MAX	, p0_6)	
MY_NEEDLE2	(ndl_p0_stabD       ,P0_NDL_HTAIL_D         ,&l_cov_p0_stab			,1262-OFFX, 712-OFFY, 2, 5,icb_stabD	,tbl_stab,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_stabM       ,P0_NDL_HTAIL_M         ,&l_ndl_p0_stabD		,1262-OFFX, 712-OFFY, 2, 5,icb_stabM	,tbl_stab,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_stabP       ,P0_NDL_HTAIL_P         ,&l_ndl_p0_stabM		,1262-OFFX, 712-OFFY, 2, 5,icb_stabP	,tbl_stab,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_stabR       ,P0_NDL_HTAIL_R         ,&l_ndl_p0_stabP		,1262-OFFX, 712-OFFY, 2, 5,icb_stabR	,tbl_stab,18, p0_6)	 
MY_ICON2	(p0_gear_tablo01	,P0_TBL_GEAR1_D_00		,&l_ndl_p0_stabR		,1309-OFFX, 669-OFFY,	icb_tablo01		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo02	,P0_TBL_GEAR2_D_00		,&l_p0_gear_tablo01		,1357-OFFX, 669-OFFY,	icb_tablo02		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo03	,P0_TBL_GEAR3_D_00		,&l_p0_gear_tablo02		,1378-OFFX, 662-OFFY,	icb_tablo03		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo04	,P0_TBL_GEAR4_D_00		,&l_p0_gear_tablo03		,1402-OFFX, 669-OFFY,	icb_tablo04		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo05	,P0_TBL_GEAR5_D_00		,&l_p0_gear_tablo04		,1423-OFFX, 669-OFFY,	icb_tablo05		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo06	,P0_TBL_GEAR6_D_00		,&l_p0_gear_tablo05		,1357-OFFX, 703-OFFY,	icb_tablo06		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo07	,P0_TBL_GEAR7_D_00		,&l_p0_gear_tablo06		,1378-OFFX, 692-OFFY,	icb_tablo07		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo08	,P0_TBL_GEAR8_D_00		,&l_p0_gear_tablo07		,1402-OFFX, 703-OFFY,	icb_tablo08		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_tablo34			,P0_TBL_34_D_00			,&l_p0_gear_tablo08		,1173-OFFX, 714-OFFY,	icb_tablo34		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo35			,P0_TBL_35_D_00			,&l_p0_tablo34			,1535-OFFX, 715-OFFY,	icb_tablo35		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo36			,P0_TBL_36_D_00			,&l_p0_tablo35			,1535-OFFX, 761-OFFY,	icb_tablo36		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo37			,P0_TBL_37_D_00			,&l_p0_tablo36			,1535-OFFX, 808-OFFY,	icb_tablo37		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(fire9				,P0_BTN_FIRE9_D_00		,&l_p0_tablo37			,1173-OFFX, 831-OFFY,	icb_fire9		,2*PANEL_LIGHT_MAX	, p0_6) 
MY_ICON2	(p0_crs_03			,MISC_CRS_P0_03			,&l_fire9				,1407-OFFX,1152-OFFY,	icb_crs_p0_03	,1					, p0_6) 
MY_ICON2	(p0_crs_04			,MISC_CRS_P0_04			,&l_p0_crs_03			,1550-OFFX, 555-OFFY,	icb_crs_p0_04	,1					, p0_6) 
MY_ICON2	(p0_6Ico			,P0_BACKGROUND6_D		,&l_p0_crs_04			,		 0,		   0,	icb_Ico			,PANEL_LIGHT_MAX	, p0_6) 
MY_STATIC2	(p0_6bg,p0_6_list	,P0_BACKGROUND6			,&l_p0_6Ico				, p0_6);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
GAU_ALL2(D,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,NULL)
GAU_ALL2(D,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_D)
GAU_ALL2(D,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_D)
GAU_ALL2(P,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,&l_ndl_p0_emi_eng3_fuel_press_D)
GAU_ALL2(P,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_P)
GAU_ALL2(P,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_P)

GAU_ALL3(D,1,1231,928,1237,934,29,6,&l_ndl_p0_emi_eng3_fuel_press_P)
GAU_ALL3(D,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_D)
GAU_ALL3(D,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_D)
GAU_ALL3(P,1,1231,928,1237,934,29,6,&l_ndl_p0_rpm_eng3_n1_D)
GAU_ALL3(P,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_P)
GAU_ALL3(P,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_P)

GAU_ALL4(D,1,1262,798,1280,815,3,4,&l_ndl_p0_rpm_eng3_n1_P)
GAU_ALL4(D,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_D)
GAU_ALL4(D,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_D)
GAU_ALL4(P,1,1262,798,1280,815,3,4,&l_ndl_p0_temp_eng3_D)
GAU_ALL4(P,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_P)
GAU_ALL4(P,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_P)

MY_ICON2	(cov_p0_flaps       ,P0_COV_FLAPS_D         ,&l_ndl_p0_temp_eng3_P	,1472-OFFX, 679-OFFY,	icb_Ico,PANEL_LIGHT_MAX	, p0_6)	
MY_NEEDLE2	(ndl_p0_flapsD      ,P0_NDL_FLAPS_D         ,&l_cov_p0_flaps       ,1492-OFFX, 711-OFFY, 3, 4,icb_flapsD	,tbl_flaps,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_flapsP      ,P0_NDL_FLAPS_P         ,&l_ndl_p0_flapsD      ,1492-OFFX, 711-OFFY, 3, 4,icb_flapsP	,tbl_flaps,18, p0_6)	 
MY_ICON2	(cov_p0_stab        ,P0_COV_HTAIL_D         ,&l_ndl_p0_flapsP      ,1240-OFFX, 681-OFFY,	icb_Ico,PANEL_LIGHT_MAX	, p0_6)	
MY_NEEDLE2	(ndl_p0_stabD       ,P0_NDL_HTAIL_D         ,&l_cov_p0_stab        ,1262-OFFX, 712-OFFY, 2, 5,icb_stabD	,tbl_stab,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_stabP       ,P0_NDL_HTAIL_P         ,&l_ndl_p0_stabD       ,1262-OFFX, 712-OFFY, 2, 5,icb_stabP	,tbl_stab,18, p0_6)	 
MY_ICON2	(p0_gear_tablo01	,P0_TBL_GEAR1_D_00		,&l_ndl_p0_stabP       ,1309-OFFX, 669-OFFY,	icb_tablo01		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo02	,P0_TBL_GEAR2_D_00		,&l_p0_gear_tablo01	,1357-OFFX, 669-OFFY,	icb_tablo02		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo03	,P0_TBL_GEAR3_D_00		,&l_p0_gear_tablo02	,1378-OFFX, 662-OFFY,	icb_tablo03		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo04	,P0_TBL_GEAR4_D_00		,&l_p0_gear_tablo03	,1402-OFFX, 669-OFFY,	icb_tablo04		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo05	,P0_TBL_GEAR5_D_00		,&l_p0_gear_tablo04	,1423-OFFX, 669-OFFY,	icb_tablo05		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo06	,P0_TBL_GEAR6_D_00		,&l_p0_gear_tablo05	,1357-OFFX, 703-OFFY,	icb_tablo06		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo07	,P0_TBL_GEAR7_D_00		,&l_p0_gear_tablo06	,1378-OFFX, 692-OFFY,	icb_tablo07		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo08	,P0_TBL_GEAR8_D_00		,&l_p0_gear_tablo07	,1402-OFFX, 703-OFFY,	icb_tablo08		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_tablo34			,P0_TBL_34_D_00			,&l_p0_gear_tablo08	,1173-OFFX, 714-OFFY,	icb_tablo34		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo35			,P0_TBL_35_D_00			,&l_p0_tablo34			,1535-OFFX, 715-OFFY,	icb_tablo35		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo36			,P0_TBL_36_D_00			,&l_p0_tablo35			,1535-OFFX, 761-OFFY,	icb_tablo36		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo37			,P0_TBL_37_D_00			,&l_p0_tablo36			,1535-OFFX, 808-OFFY,	icb_tablo37		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(fire9				,P0_BTN_FIRE9_D_00		,&l_p0_tablo37			,1173-OFFX, 831-OFFY,	icb_fire9		,2*PANEL_LIGHT_MAX	, p0_6) 
MY_ICON2	(p0_crs_03			,MISC_CRS_P0_03			,&l_fire9				,1407-OFFX,1152-OFFY,	icb_crs_p0_03	,1					, p0_6) 
MY_ICON2	(p0_crs_04			,MISC_CRS_P0_04			,&l_p0_crs_03			,1550-OFFX, 555-OFFY,	icb_crs_p0_04	,1					, p0_6) 
MY_ICON2	(p0_6Ico			,P0_BACKGROUND6_D		,&l_p0_crs_04			,		 0,		   0,	icb_Ico			,PANEL_LIGHT_MAX	, p0_6) 
MY_STATIC2	(p0_6bg,p0_6_list	,P0_BACKGROUND6			,&l_p0_6Ico			, p0_6);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
GAU_ALL2(D,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,NULL)
GAU_ALL2(D,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_D)
GAU_ALL2(D,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_D)
GAU_ALL2(R,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,&l_ndl_p0_emi_eng3_fuel_press_D)
GAU_ALL2(R,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_R)
GAU_ALL2(R,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_R)

GAU_ALL3(D,1,1231,928,1237,934,29,6,&l_ndl_p0_emi_eng3_fuel_press_R)
GAU_ALL3(D,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_D)
GAU_ALL3(D,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_D)
GAU_ALL3(R,1,1231,928,1237,934,29,6,&l_ndl_p0_rpm_eng3_n1_D)
GAU_ALL3(R,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_R)
GAU_ALL3(R,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_R)

GAU_ALL4(D,1,1262,798,1280,815,3,4,&l_ndl_p0_rpm_eng3_n1_R)
GAU_ALL4(D,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_D)
GAU_ALL4(D,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_D)
GAU_ALL4(R,1,1262,798,1280,815,3,4,&l_ndl_p0_temp_eng3_D)
GAU_ALL4(R,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_R)
GAU_ALL4(R,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_R)

MY_ICON2	(cov_p0_flaps       ,P0_COV_FLAPS_D         ,&l_ndl_p0_temp_eng3_R	,1472-OFFX, 679-OFFY,	icb_Ico,PANEL_LIGHT_MAX	, p0_6)	
MY_NEEDLE2	(ndl_p0_flapsD      ,P0_NDL_FLAPS_D         ,&l_cov_p0_flaps       ,1492-OFFX, 711-OFFY, 3, 4,icb_flapsD	,tbl_flaps,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_flapsR      ,P0_NDL_FLAPS_R         ,&l_ndl_p0_flapsD      ,1492-OFFX, 711-OFFY, 3, 4,icb_flapsR	,tbl_flaps,18, p0_6)	 
MY_ICON2	(cov_p0_stab        ,P0_COV_HTAIL_D         ,&l_ndl_p0_flapsR      ,1240-OFFX, 681-OFFY,	icb_Ico,PANEL_LIGHT_MAX	, p0_6)	
MY_NEEDLE2	(ndl_p0_stabD       ,P0_NDL_HTAIL_D         ,&l_cov_p0_stab        ,1262-OFFX, 712-OFFY, 2, 5,icb_stabD	,tbl_stab,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_stabR       ,P0_NDL_HTAIL_R         ,&l_ndl_p0_stabD       ,1262-OFFX, 712-OFFY, 2, 5,icb_stabR	,tbl_stab,18, p0_6)	 
MY_ICON2	(p0_gear_tablo01	,P0_TBL_GEAR1_D_00		,&l_ndl_p0_stabR       ,1309-OFFX, 669-OFFY,	icb_tablo01		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo02	,P0_TBL_GEAR2_D_00		,&l_p0_gear_tablo01	,1357-OFFX, 669-OFFY,	icb_tablo02		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo03	,P0_TBL_GEAR3_D_00		,&l_p0_gear_tablo02	,1378-OFFX, 662-OFFY,	icb_tablo03		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo04	,P0_TBL_GEAR4_D_00		,&l_p0_gear_tablo03	,1402-OFFX, 669-OFFY,	icb_tablo04		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo05	,P0_TBL_GEAR5_D_00		,&l_p0_gear_tablo04	,1423-OFFX, 669-OFFY,	icb_tablo05		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo06	,P0_TBL_GEAR6_D_00		,&l_p0_gear_tablo05	,1357-OFFX, 703-OFFY,	icb_tablo06		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo07	,P0_TBL_GEAR7_D_00		,&l_p0_gear_tablo06	,1378-OFFX, 692-OFFY,	icb_tablo07		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo08	,P0_TBL_GEAR8_D_00		,&l_p0_gear_tablo07	,1402-OFFX, 703-OFFY,	icb_tablo08		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_tablo34			,P0_TBL_34_D_00			,&l_p0_gear_tablo08	,1173-OFFX, 714-OFFY,	icb_tablo34		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo35			,P0_TBL_35_D_00			,&l_p0_tablo34			,1535-OFFX, 715-OFFY,	icb_tablo35		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo36			,P0_TBL_36_D_00			,&l_p0_tablo35			,1535-OFFX, 761-OFFY,	icb_tablo36		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo37			,P0_TBL_37_D_00			,&l_p0_tablo36			,1535-OFFX, 808-OFFY,	icb_tablo37		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(fire9				,P0_BTN_FIRE9_D_00		,&l_p0_tablo37			,1173-OFFX, 831-OFFY,	icb_fire9		,2*PANEL_LIGHT_MAX	, p0_6) 
MY_ICON2	(p0_crs_03			,MISC_CRS_P0_03			,&l_fire9				,1407-OFFX,1152-OFFY,	icb_crs_p0_03	,1					, p0_6) 
MY_ICON2	(p0_crs_04			,MISC_CRS_P0_04			,&l_p0_crs_03			,1550-OFFX, 555-OFFY,	icb_crs_p0_04	,1					, p0_6) 
MY_ICON2	(p0_6Ico			,P0_BACKGROUND6_D		,&l_p0_crs_04			,		 0,		   0,	icb_Ico			,PANEL_LIGHT_MAX	, p0_6) 
MY_STATIC2	(p0_6bg,p0_6_list	,P0_BACKGROUND6			,&l_p0_6Ico			, p0_6);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
GAU_ALL2(D,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,NULL)
GAU_ALL2(D,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_D)
GAU_ALL2(D,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_D)
GAU_ALL2(M,1,1186,1065,1276,1116,2,5,1202,1116,4,5,1238,1084,3,5,&l_ndl_p0_emi_eng3_fuel_press_D)
GAU_ALL2(M,2,1333,1066,1422,1116,3,5,1348,1116,3,5,1384,1084,3,5,&l_ndl_p0_emi_eng1_fuel_press_M)
GAU_ALL2(M,3,1479,1066,1570,1116,3,5,1496,1116,3,5,1532,1084,3,5,&l_ndl_p0_emi_eng2_fuel_press_M)

GAU_ALL3(D,1,1231,928,1237,934,29,6,&l_ndl_p0_emi_eng3_fuel_press_M)
GAU_ALL3(D,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_D)
GAU_ALL3(D,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_D)
GAU_ALL3(M,1,1231,928,1237,934,29,6,&l_ndl_p0_rpm_eng3_n1_D)
GAU_ALL3(M,2,1377,928,1384,934,29,6,&l_ndl_p0_rpm_eng1_n1_M)
GAU_ALL3(M,3,1524,928,1531,934,29,6,&l_ndl_p0_rpm_eng2_n1_M)

GAU_ALL4(D,1,1262,798,1280,815,3,4,&l_ndl_p0_rpm_eng3_n1_M)
GAU_ALL4(D,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_D)
GAU_ALL4(D,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_D)
GAU_ALL4(M,1,1262,798,1280,815,3,4,&l_ndl_p0_temp_eng3_D)
GAU_ALL4(M,2,1368,799,1384,815,2,5,&l_ndl_p0_temp_eng1_M)
GAU_ALL4(M,3,1474,798,1491,815,3,5,&l_ndl_p0_temp_eng2_M)

MY_ICON2	(cov_p0_flaps       ,P0_COV_FLAPS_D         ,&l_ndl_p0_temp_eng3_M	,1472-OFFX, 679-OFFY,	icb_Ico,PANEL_LIGHT_MAX	, p0_6)	
MY_NEEDLE2	(ndl_p0_flapsD      ,P0_NDL_FLAPS_D         ,&l_cov_p0_flaps       ,1492-OFFX, 711-OFFY, 3, 4,icb_flapsD	,tbl_flaps,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_flapsM      ,P0_NDL_FLAPS_M         ,&l_ndl_p0_flapsD      ,1492-OFFX, 711-OFFY, 3, 4,icb_flapsM	,tbl_flaps,18, p0_6)	 
MY_ICON2	(cov_p0_stab        ,P0_COV_HTAIL_D         ,&l_ndl_p0_flapsM      ,1240-OFFX, 681-OFFY,	icb_Ico,PANEL_LIGHT_MAX	, p0_6)	
MY_NEEDLE2	(ndl_p0_stabD       ,P0_NDL_HTAIL_D         ,&l_cov_p0_stab        ,1262-OFFX, 712-OFFY, 2, 5,icb_stabD	,tbl_stab,18, p0_6)	 
MY_NEEDLE2	(ndl_p0_stabM       ,P0_NDL_HTAIL_M         ,&l_ndl_p0_stabD       ,1262-OFFX, 712-OFFY, 2, 5,icb_stabM	,tbl_stab,18, p0_6)	 
MY_ICON2	(p0_gear_tablo01	,P0_TBL_GEAR1_D_00		,&l_ndl_p0_stabM       ,1309-OFFX, 669-OFFY,	icb_tablo01		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo02	,P0_TBL_GEAR2_D_00		,&l_p0_gear_tablo01	,1357-OFFX, 669-OFFY,	icb_tablo02		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo03	,P0_TBL_GEAR3_D_00		,&l_p0_gear_tablo02	,1378-OFFX, 662-OFFY,	icb_tablo03		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo04	,P0_TBL_GEAR4_D_00		,&l_p0_gear_tablo03	,1402-OFFX, 669-OFFY,	icb_tablo04		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo05	,P0_TBL_GEAR5_D_00		,&l_p0_gear_tablo04	,1423-OFFX, 669-OFFY,	icb_tablo05		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo06	,P0_TBL_GEAR6_D_00		,&l_p0_gear_tablo05	,1357-OFFX, 703-OFFY,	icb_tablo06		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo07	,P0_TBL_GEAR7_D_00		,&l_p0_gear_tablo06	,1378-OFFX, 692-OFFY,	icb_tablo07		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_gear_tablo08	,P0_TBL_GEAR8_D_00		,&l_p0_gear_tablo07	,1402-OFFX, 703-OFFY,	icb_tablo08		,3*PANEL_LIGHT_MAX	, p0_6)	
MY_ICON2	(p0_tablo34			,P0_TBL_34_D_00			,&l_p0_gear_tablo08	,1173-OFFX, 714-OFFY,	icb_tablo34		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo35			,P0_TBL_35_D_00			,&l_p0_tablo34			,1535-OFFX, 715-OFFY,	icb_tablo35		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo36			,P0_TBL_36_D_00			,&l_p0_tablo35			,1535-OFFX, 761-OFFY,	icb_tablo36		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(p0_tablo37			,P0_TBL_37_D_00			,&l_p0_tablo36			,1535-OFFX, 808-OFFY,	icb_tablo37		,3*PANEL_LIGHT_MAX	, p0_6) 	
MY_ICON2	(fire9				,P0_BTN_FIRE9_D_00		,&l_p0_tablo37			,1173-OFFX, 831-OFFY,	icb_fire9		,2*PANEL_LIGHT_MAX	, p0_6) 
MY_ICON2	(p0_crs_03			,MISC_CRS_P0_03			,&l_fire9				,1407-OFFX,1152-OFFY,	icb_crs_p0_03	,1					, p0_6) 
MY_ICON2	(p0_crs_04			,MISC_CRS_P0_04			,&l_p0_crs_03			,1550-OFFX, 555-OFFY,	icb_crs_p0_04	,1					, p0_6) 
MY_ICON2	(p0_6Ico			,P0_BACKGROUND6_D		,&l_p0_crs_04			,		 0,		   0,	icb_Ico			,PANEL_LIGHT_MAX	, p0_6) 
MY_STATIC2	(p0_6bg,p0_6_list	,P0_BACKGROUND6			,&l_p0_6Ico			, p0_6);
#endif

#endif