/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"

SIMPLE_GAUGE_VC		(Wbc01		,VC8_WBC01_D		);
SIMPLE_GAUGE_VC		(Wbc02		,VC8_WBC02_D		);
SIMPLE_GAUGE_VC		(Wbc03		,VC8_WBC03_D		);
SIMPLE_GAUGE_VC		(Wbc04		,VC8_WBC04_D		);
SIMPLE_GAUGE_VC		(Wbc05		,VC8_WBC05_D		);
SIMPLE_GAUGE_VC		(Wbc06		,VC8_WBC06_D		);
SIMPLE_GAUGE_VC		(Wbc07		,VC8_WBC07_D		);
SIMPLE_GAUGE_VC		(Wbc08		,VC8_WBC08_D		);
SIMPLE_GAUGE_VC		(Wbc09		,VC8_WBC09_D		);
SIMPLE_GAUGE_VC		(Wbc10		,VC8_WBC10_D		);
SIMPLE_GAUGE_VC		(Wbc11		,VC8_WBC11_D		);
SIMPLE_GAUGE_VC		(Wbc12		,VC8_WBC12_D		);
SIMPLE_GAUGE_VC_LANG1(Wbc13		,VC8_WBC13_D		,VC8_WBC13INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC		(Wbc14		,VC8_WBC14_D		);
SIMPLE_GAUGE_VC		(Wbc15		,VC8_WBC15_D		);
SIMPLE_GAUGE_VC_LANG1(Wbc16		,VC8_WBC16_D		,VC8_WBC16INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC_LANG1(Wbc17		,VC8_WBC17_D		,VC8_WBC17INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC		(Wbc18		,VC8_WBC18_D		);
SIMPLE_GAUGE_VC_LANG1(Wbc19		,VC8_WBC19_D		,VC8_WBC19INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC_LANG1(Wbc20		,VC8_WBC20_D		,VC8_WBC20INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC		(Wbc21		,VC8_WBC21_D		);
SIMPLE_GAUGE_VC_LANG1(Wbc22		,VC8_WBC22_D		,VC8_WBC22INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC		(Wbc23		,VC8_WBC23_D		);
SIMPLE_GAUGE_VC		(Wbc24		,VC8_WBC24_D		);
SIMPLE_GAUGE_VC_LANG1(Wbc25		,VC8_WBC25_D		,VC8_WBC25INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC		(Wbc26		,VC8_WBC26_D		);
SIMPLE_GAUGE_VC		(Wbc27		,VC8_WBC27_D		);
SIMPLE_GAUGE_VC_LANG1(Wbc28		,VC8_WBC28_D		,VC8_WBC28INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC		(Wbc29		,VC8_WBC29_D		);
SIMPLE_GAUGE_VC_LANG1(Wbc30		,VC8_WBC30_D		,VC8_WBC30INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC		(Wbc31		,VC8_WBC31_D		);
SIMPLE_GAUGE_VC_LANG1(Wbc32		,VC8_WBC32_D		,VC8_WBC32INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC		(Wbc33		,VC8_WBC33_D		);
SIMPLE_GAUGE_VC		(Wbc34		,VC8_WBC34_D		);
SIMPLE_GAUGE_VC		(Wbc35		,VC8_WBC35_D		);
SIMPLE_GAUGE_VC		(Wbc36		,VC8_WBC36_D		);
SIMPLE_GAUGE_VC		(Wbc37		,VC8_WBC37_D		);
SIMPLE_GAUGE_VC		(Wbc38		,VC8_WBC38_D		);
SIMPLE_GAUGE_VC		(Wbc39		,VC8_WBC39_D		);
SIMPLE_GAUGE_VC		(Wbc40		,VC8_WBC40_D		);
SIMPLE_GAUGE_VC		(Wbc41		,VC8_WBC41_D		);
SIMPLE_GAUGE_VC		(Wbc42		,VC8_WBC42_D		);
SIMPLE_GAUGE_VC		(Wbc43		,VC8_WBC43_D		);
SIMPLE_GAUGE_VC		(Wbc44		,VC8_WBC44_D		);
SIMPLE_GAUGE_VC		(Wbc45		,VC8_WBC45_D		);

#ifdef _DEBUG
DLLMAIND("F:\\MemLeaks\\Yak40\\VC8.log")
#else
DLLMAIN()
#endif

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&Wbc01					),
		(&Wbc02					),
		(&Wbc03					),
		(&Wbc04					),
		(&Wbc05					),        
		(&Wbc06					),        
		(&Wbc07					),        
		(&Wbc08					),        
		(&Wbc09					),        
		(&Wbc10					),        
		(&Wbc11					),        
		(&Wbc12					),        
		(&Wbc13					),        
		(&Wbc14					),        
		(&Wbc15					),        
		(&Wbc16					),        
		(&Wbc17					),        
		(&Wbc18					),        
		(&Wbc19					),        
		(&Wbc20					),        
		(&Wbc21					),            
		(&Wbc22					),
		(&Wbc23					),
		(&Wbc24					),
		(&Wbc25					),
		(&Wbc26					),        
		(&Wbc27					),        
		(&Wbc28					),        
		(&Wbc29					),        
		(&Wbc30					),        
		(&Wbc31					),        
		(&Wbc32					),        
		(&Wbc33					),        
		(&Wbc34					),        
		(&Wbc35					),        
		(&Wbc36					),        
		(&Wbc37					),        
		(&Wbc38					),        
		(&Wbc39					),        
		(&Wbc40					),        
		(&Wbc41					),        
		(&Wbc42					),            
		(&Wbc43					),
		(&Wbc44					),
		(&Wbc45					),
		0					
	}											
};												
							
