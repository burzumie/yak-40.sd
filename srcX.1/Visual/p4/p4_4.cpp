/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P4.h"

#define OFFX	0
#define OFFY	784

//////////////////////////////////////////////////////////////////////////

// �������
static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS4_01,0);
		POS_SET(POS_CRS4_02,0);
		POS_SET(POS_CRS4_03,0);
	}
	return true;
}

#ifndef ONLY3D

MOUSE_BEGIN(rect_p4_4,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P4_BACKGROUND4_D_SX,P4_BACKGROUND4_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p4_4_list; 
GAUGE_HEADER_FS700_EX(P4_BACKGROUND4_D_SX, "p4_4", &p4_4_list, rect_p4_4, 0, 0, 0, 0, p4_4); 

MY_ICON2	(p4_4Ico			,P4_BACKGROUND4_D	,NULL		,0,0,icb_Ico,PANEL_LIGHT_MAX,p4_4) 
MY_STATIC2	(p4_4bg,p4_4_list	,P4_BACKGROUND4_D	,&l_p4_4Ico	, p4_4);

#endif


