/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P4.h"

#define OFFX	486
#define OFFY	784

//////////////////////////////////////////////////////////////////////////

FLOAT64 FSAPI icb_appwr(PELEMENT_ICON pelement)
{
	SHOW_AZS(AZS_AP_PWR,2)
}

FLOAT64 FSAPI icb_aptang(PELEMENT_ICON pelement)
{
	SHOW_AZS(AZS_AP_PITCH_HOLD,2)
}

FLOAT64 FSAPI icb_aplmpbtn_cmd(PELEMENT_ICON pelement)
{
	SHOW_AZS(AZS_AP_CMD,2)
}

FLOAT64 FSAPI icb_aplmpbtn_althld(PELEMENT_ICON pelement)
{
	SHOW_AZS(AZS_AP_ALT_HOLD,2)
}

FLOAT64 FSAPI icb_trimrudder(PELEMENT_ICON pelement)
{
	SHOW_AZS(AZS_TRIM_RUDDER,3)
}

FLOAT64 FSAPI icb_flaps(PELEMENT_ICON pelement)
{
	SHOW_AZS(AZS_FLAPS,4)
}

FLOAT64 FSAPI icb_emerg_flaps(PELEMENT_ICON pelement)
{
	SHOW_AZS(AZS_EMERG_FLAPS,3)
}

FLOAT64 FSAPI icb_prkbrk(PELEMENT_ICON pelement)
{
	SHOW_POS(POS_PRKBRK,15)
}

FLOAT64 FSAPI icb_aprdy(PELEMENT_ICON pelement)	
{
	SHOW_LMP(LMP_AP_RDY,BTN_GET(BTN_LIGHTS_TEST4),2);
}

FLOAT64 FSAPI icb_trimrudder_lamp(PELEMENT_ICON pelement)
{
	SHOW_LMP(LMP_TRIM_RUDDER,BTN_GET(BTN_LIGHTS_TEST4),3)
}

FLOAT64 FSAPI icb_ap1(PELEMENT_ICON pelement)
{
	if(HND_GET(HND_AP_VER)==0)
		SHOW_IMAGE(pelement);
	else 
		HIDE_IMAGE(pelement); 
	SHOW_HND(HND_AP_HOR,25)
}

FLOAT64 FSAPI icb_ap2(PELEMENT_ICON pelement)
{
	if(HND_GET(HND_AP_VER)==2)
		SHOW_IMAGE(pelement);
	else 
		HIDE_IMAGE(pelement); 
	SHOW_HND(HND_AP_HOR,25)
}

FLOAT64 FSAPI icb_ap3(PELEMENT_ICON pelement)
{
	if(HND_GET(HND_AP_VER)==1)
		SHOW_IMAGE(pelement);
	else 
		HIDE_IMAGE(pelement); 
	SHOW_HND(HND_AP_HOR,25)
}

//////////////////////////////////////////////////////////////////////////

BOOL FSAPI mcb_p4_flaps_up				(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(AZS_GET(AZS_FLAPS)==0)
			AZS_SET(AZS_FLAPS,1);
		else if(AZS_GET(AZS_FLAPS)==1)
			AZS_SET(AZS_FLAPS,0);
	} else if(mouse_flags&MOUSE_LEFTRELEASE) {
		if(AZS_GET(AZS_FLAPS)!=0) {
			AZS_SET(AZS_FLAPS,1);
			POS_SET(POS_MAIN_FLAPS_WORK,0);
		}
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		if(AZS_GET(AZS_FLAPS)!=0) {
			AZS_SET(AZS_FLAPS,3);
			POS_SET(POS_MAIN_FLAPS_WORK,1);
		}
	}
	return TRUE;
}

BOOL FSAPI mcb_p4_flaps_dn				(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(AZS_GET(AZS_FLAPS)==0)
			AZS_SET(AZS_FLAPS,1);
		else if(AZS_GET(AZS_FLAPS)==1)
			AZS_SET(AZS_FLAPS,0);
	} else if(mouse_flags&MOUSE_LEFTRELEASE) {
		if(AZS_GET(AZS_FLAPS)!=0) {
			AZS_SET(AZS_FLAPS,1);
			POS_SET(POS_MAIN_FLAPS_WORK,0);
		}
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		if(AZS_GET(AZS_FLAPS)!=0) {
			AZS_SET(AZS_FLAPS,2);
			POS_SET(POS_MAIN_FLAPS_WORK,1);
		}
	}
	return TRUE;
}

BOOL FSAPI mcb_p4_emerg_flaps_dn		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		AZS_TGL(AZS_EMERG_FLAPS);
	} else if(mouse_flags&MOUSE_LEFTRELEASE) {
		if(AZS_GET(AZS_EMERG_FLAPS)!=0) {
			AZS_SET(AZS_EMERG_FLAPS,1);
		}
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {
		if(AZS_GET(AZS_EMERG_FLAPS)!=0) {
			AZS_SET(AZS_EMERG_FLAPS,2);
		}
	}
	return TRUE;
}

BOOL FSAPI mcb_trim_rudder_center	(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_RUDDER_TRIM_BUS))
		KEY_SET(KEY_RUDDER_CENTER_TRIM,TRIMMER_CENTER);
	return TRUE;
}

BOOL FSAPI mcb_p4_trimeleva_l			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_RUDDER_TRIM_BUS))
		trigger_key_event(KEY_RUDDER_TRIM_LEFT,0);
	PRS_AZS(AZS_TRIM_RUDDER,1);
	return TRUE;
}

BOOL FSAPI mcb_p4_trimeleva_r			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_RUDDER_TRIM_BUS))
		trigger_key_event(KEY_RUDDER_TRIM_RIGHT,0);
	PRS_AZS(AZS_TRIM_RUDDER,2);
	return TRUE;
}

BOOL FSAPI mcb_p4_appwr  			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	AZS_TGL(AZS_AP_PWR);
	return TRUE;
}

BOOL FSAPI mcb_p4_appitchhld		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	AZS_TGL(AZS_AP_PITCH_HOLD);
	return TRUE;
}

BOOL FSAPI mcb_p4_apcmd			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	AZS_SET(AZS_AP_CMD,1);
	return TRUE;
}

BOOL FSAPI mcb_p4_apalthld		(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	AZS_SET(AZS_AP_ALT_HOLD,1);
	return TRUE;
}

static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS4_01,0);
		POS_SET(POS_CRS4_02,0);
		POS_SET(POS_CRS4_03,0);
	}
	return true;
}

BOOL FSAPI mcb_p4_aphandle_l			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		int tmp=(int)HND_GET(HND_AP_HOR_CORRECTED);
		int tmp2=(int)HND_GET(HND_AP_HOR);
		tmp--;
		if(tmp<-12)tmp=-12;
		if(tmp<0)
			tmp2=LimitValue<int>(abs(tmp),0,25);
		else if(tmp>0)
			tmp2=LimitValue<int>(tmp+12,0,25);
		else 
			tmp2=0;
		tmp2=LimitValue<int>(tmp2,0,25);
		HND_SET(HND_AP_HOR_CORRECTED,tmp);
		HND_SET(HND_AP_HOR,tmp2);
	}
	return TRUE;
}

BOOL FSAPI mcb_p4_aphandle_r			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		int tmp=(int)HND_GET(HND_AP_HOR_CORRECTED);
		int tmp2=(int)HND_GET(HND_AP_HOR);
		tmp++;
		if(tmp>12)tmp=12;
		if(tmp<0)
			tmp2=LimitValue<int>(abs(tmp),0,25);
		else if(tmp>0)
			tmp2=LimitValue<int>(tmp+12,0,25);
		else 
			tmp2=0;
		tmp2=LimitValue<int>(tmp2,0,25);
		HND_SET(HND_AP_HOR_CORRECTED,tmp);
		HND_SET(HND_AP_HOR,tmp2);
	}
	return TRUE;
}

BOOL FSAPI mcb_p4_aphandle_d			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) { 
		HND_SET(HND_AP_VER,0);
		POS_SET(POS_AP_MOUSE_HND_PRESS,0);
	} else { 
		if(mouse_flags&MOUSE_LEFTSINGLE) { 
			HND_SET(HND_AP_VER,2);
			AZS_SET(AZS_AP_ALT_HOLD,0);
			POS_SET(POS_AP_MOUSE_HND_PRESS,1);
		} else if(mouse_flags&MOUSE_RIGHTSINGLE) { 
			HND_SET(HND_AP_VER,2);
			AZS_SET(AZS_AP_ALT_HOLD,0);
			POS_SET(POS_AP_MOUSE_HND_PRESS,2);
		}
	}	
	return TRUE;
}

BOOL FSAPI mcb_p4_aphandle_u			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTRELEASE||mouse_flags&MOUSE_RIGHTRELEASE) { 
		HND_SET(HND_AP_VER,0);
		POS_SET(POS_AP_MOUSE_HND_PRESS,0);
	} else { 
		if(mouse_flags&MOUSE_LEFTSINGLE) { 
			HND_SET(HND_AP_VER,1);
			AZS_SET(AZS_AP_ALT_HOLD,0);
			POS_SET(POS_AP_MOUSE_HND_PRESS,1);
		} else if(mouse_flags&MOUSE_RIGHTSINGLE) { 
			HND_SET(HND_AP_VER,1);
			AZS_SET(AZS_AP_ALT_HOLD,0);
			POS_SET(POS_AP_MOUSE_HND_PRESS,2);
		}
	}	
	return TRUE;
}

BOOL FSAPI mcb_p4_aphandle_c 			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	HND_SET(HND_AP_HOR,0);
	HND_SET(HND_AP_HOR_CORRECTED,0);
	return TRUE;
}

BOOL FSAPI mcb_p4_prkbrk				(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	trigger_key_event(KEY_PARKING_BRAKES,0);
	return TRUE;
}

BOOL FSAPI mcb_ap_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P14);
	return true;
}

static MAKE_TCB(tcb_01  ,AZS_TT(AZS_AP_PWR				    ))	
static MAKE_TCB(tcb_02  ,AZS_TT(AZS_AP_PITCH_HOLD		    ))	
static MAKE_TCB(tcb_03  ,AZS_TT(AZS_AP_CMD				    ))	
static MAKE_TCB(tcb_04  ,AZS_TT(AZS_AP_ALT_HOLD			    ))	
static MAKE_TCB(tcb_05  ,AZS_TT(AZS_FLAPS				    ))	
static MAKE_TCB(tcb_06  ,AZS_TT(AZS_EMERG_FLAPS			    ))	
static MAKE_TCB(tcb_07  ,AZS_TT(AZS_TRIM_RUDDER			    ))	
static MAKE_TCB(tcb_08  ,LMP_TT(LMP_TRIM_RUDDER		        ))
static MAKE_TCB(tcb_09  ,LMP_TT(LMP_AP_RDY				    ))
static MAKE_TCB(tcb_10  ,POS_TT(POS_PRKBRK       		    ))
static MAKE_TCB(tcb_11  ,HND_TT(HND_AP_HOR_CORRECTED        ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p4_5,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P4_BACKGROUND2_D_SX,P4_BACKGROUND2_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_BOX( 560-OFFX, 828-OFFY, 323, 209,CURSOR_NONE,MOUSE_LR,mcb_ap_big)    // AP BIG

MOUSE_TBOX( "8",  811-OFFX, 798-OFFY,  65,  59,CURSOR_HAND,MOUSE_LR,mcb_trim_rudder_center)    // LMP_TRIM_RUDDER		        
MOUSE_TTPB( "9",  598-OFFX, 846-OFFY,  67,  62)    // LMP_AP_RDY				        

MOUSE_TSHB( "7", 709-OFFX,  781-OFFY, 106,  98,CURSOR_LEFTARROW,CURSOR_RIGHTARROW,MOUSE_DLR,mcb_p4_trimeleva_l,mcb_p4_trimeleva_r)
MOUSE_TBOX( "1", 571-OFFX,  908-OFFY,  73, 109,CURSOR_HAND,MOUSE_LR,mcb_p4_appwr)
MOUSE_TBOX( "2", 806-OFFX,  908-OFFY,  83, 109,CURSOR_HAND,MOUSE_LR,mcb_p4_appitchhld)
MOUSE_TBOX( "3", 690-OFFX,  846-OFFY,  67,  62,CURSOR_HAND,MOUSE_DLR,mcb_p4_apcmd)
MOUSE_TBOX( "4", 772-OFFX,  857-OFFY,  81,  51,CURSOR_HAND,MOUSE_DLR,mcb_p4_apalthld)
MOUSE_TSVB( "5", 889-OFFX,  809-OFFY, 168, 107,CURSOR_UPARROW,CURSOR_DOWNARROW,MOUSE_DLR,mcb_p4_flaps_up,mcb_p4_flaps_dn)
MOUSE_TBOX( "6", 888-OFFX,  915-OFFY, 168,  77,CURSOR_DOWNARROW,MOUSE_DLR,mcb_p4_emerg_flaps_dn)

MOUSE_TBOX( "10", 639-OFFX, 1026-OFFY, 197,  83,CURSOR_HAND,MOUSE_LR,mcb_p4_prkbrk)

MOUSE_PARENT_BEGIN( 656-OFFX, 912-OFFY, 140, 140,HELP_NONE)
MOUSE_TOOLTIP_TEXT_STRING("%11!s!",ttargs)
MOUSE_CHILD_FUNCT( 46,  0, 46, 46,CURSOR_UPARROW,   MOUSE_LEFTALL|MOUSE_RIGHTALL|MOUSE_DOWN_REPEAT,	mcb_p4_aphandle_u				)
MOUSE_CHILD_FUNCT( 46, 94, 46, 46,CURSOR_DOWNARROW, MOUSE_LEFTALL|MOUSE_RIGHTALL|MOUSE_DOWN_REPEAT,	mcb_p4_aphandle_d				)
MOUSE_CHILD_FUNCT(  0, 46, 46, 46,CURSOR_LEFTARROW, MOUSE_LEFTALL|MOUSE_RIGHTALL|MOUSE_DOWN_REPEAT,	mcb_p4_aphandle_l				)
MOUSE_CHILD_FUNCT( 94, 46, 46, 46,CURSOR_RIGHTARROW,MOUSE_LEFTALL|MOUSE_RIGHTALL|MOUSE_DOWN_REPEAT,	mcb_p4_aphandle_r				)
MOUSE_CHILD_FUNCT( 46, 46, 46, 46,CURSOR_HAND,      MOUSE_LEFTSINGLE|MOUSE_RIGHTSINGLE,				mcb_p4_aphandle_c				)
MOUSE_PARENT_END																						

MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p4_5_list; 
GAUGE_HEADER_FS700_EX(P4_BACKGROUND5_D_SX, "p4_5", &p4_5_list, rect_p4_5, 0, 0, 0, 0, p4_5); 

MY_ICON2	(p4_knb_ap_hnd_0	,P4_KNB_AP0_D_00		,NULL					, 656-OFFX,  912-OFFY,icb_ap1				, 25 *PANEL_LIGHT_MAX	, p4_5)	
MY_ICON2	(p4_knb_ap_hnd_1	,P4_KNB_AP1_D_00		,&l_p4_knb_ap_hnd_0		, 656-OFFX,  912-OFFY,icb_ap2				, 25 *PANEL_LIGHT_MAX	, p4_5)	
MY_ICON2	(p4_knb_ap_hnd_2	,P4_KNB_AP2_D_00		,&l_p4_knb_ap_hnd_1		, 656-OFFX,  912-OFFY,icb_ap3				, 25 *PANEL_LIGHT_MAX	, p4_5)	
MY_ICON2	(p4_prkbrk			,P4_SLD_PARKBRAKE_D_00	,&l_p4_knb_ap_hnd_2		, 639-OFFX, 1026-OFFY,icb_prkbrk			, 15 *PANEL_LIGHT_MAX	, p4_5)	
MY_ICON2	(azs_flaps			,P4_SWT_FLAPS_D_00		,&l_p4_prkbrk			, 889-OFFX,  809-OFFY,icb_flaps				, 4	 *PANEL_LIGHT_MAX	, p4_5)
MY_ICON2	(azs_emerg_flaps	,P4_SWT_FLAPSEMERG_D_00	,&l_azs_flaps			, 889-OFFX,  916-OFFY,icb_emerg_flaps		, 3	 *PANEL_LIGHT_MAX	, p4_5)
MY_ICON2	(azs_trimrudder		,P4_SWT_TRIMRUDDER_D_00	,&l_azs_emerg_flaps		, 709-OFFX,	 781-OFFY,icb_trimrudder		, 3	 *PANEL_LIGHT_MAX	, p4_5)
MY_ICON2	(lamp_trimrudder	,P4_LMP_TRIMRUDDER_D_00	,&l_azs_trimrudder		, 811-OFFX,	 798-OFFY,icb_trimrudder_lamp	, 3	 *PANEL_LIGHT_MAX	, p4_5)
MY_ICON2	(lampbtn_ap_althld	,P4_BTN_AP3_D_00		,&l_lamp_trimrudder		, 772-OFFX,	 857-OFFY,icb_aplmpbtn_althld	, 2	 *PANEL_LIGHT_MAX	, p4_5)
MY_ICON2	(lampbtn_ap_cmd		,P4_BTN_AP2_D_00		,&l_lampbtn_ap_althld	, 690-OFFX,	 846-OFFY,icb_aplmpbtn_cmd		, 2	 *PANEL_LIGHT_MAX	, p4_5)
MY_ICON2	(lamp_ap_rdy		,P4_LMP_AP1_D_00		,&l_lampbtn_ap_cmd		, 598-OFFX,	 846-OFFY,icb_aprdy				, 2	 *PANEL_LIGHT_MAX	, p4_5)
MY_ICON2	(switch_ap_tang		,P4_AZS_APPITCHHLD_D_00	,&l_lamp_ap_rdy			, 806-OFFX,  908-OFFY,icb_aptang			, 2	 *PANEL_LIGHT_MAX	, p4_5)
MY_ICON2	(switch_ap_pwr		,P4_AZS_APPOWER_D_00	,&l_switch_ap_tang		, 571-OFFX,  908-OFFY,icb_appwr				, 2	 *PANEL_LIGHT_MAX	, p4_5)
MY_ICON2	(p4_5Ico			,P4_BACKGROUND5_D		,&l_switch_ap_pwr		,		 0,			0,icb_Ico				, PANEL_LIGHT_MAX		, p4_5) 
MY_STATIC2	(p4_5bg,p4_5_list	,P4_BACKGROUND5_D		,&l_p4_5Ico				, p4_5);

#endif
