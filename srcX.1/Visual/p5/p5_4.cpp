/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	0
#define OFFY	276

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_azsvhf1on  		,SHOW_AZS(AZS_VHF1ON,2));
static MAKE_ICB(icb_covvh1on		,if(!PWR_GET(PWR_COM1)){return -1;} else {CHK_BRT(); return POS_GET(POS_PANEL_STATE); });
static MAKE_ICB(icb_sclvhf1leftoff  ,if(!PWR_GET(PWR_COM1)){SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF1LEFT  )+POS_GET(POS_PANEL_STATE)*18;}else{HIDE_IMAGE(pelement);} return -1;);
static MAKE_ICB(icb_sclvhf1lefton   ,if(!PWR_GET(PWR_COM1)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF1LEFT  )+POS_GET(POS_PANEL_STATE)*18;} return -1;);
static MAKE_ICB(icb_sclvhf1midoff   ,if(!PWR_GET(PWR_COM1)){SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF1MID)+POS_GET(POS_PANEL_STATE)*10;}else{HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_sclvhf1midon    ,if(!PWR_GET(PWR_COM1)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF1MID)+POS_GET(POS_PANEL_STATE)*10;} return 0;);
static MAKE_ICB(icb_sclvhf1rightoff ,if(!PWR_GET(PWR_COM1)){SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF1RIGHT)+POS_GET(POS_PANEL_STATE)*4;}else{HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_sclvhf1righton  ,if(!PWR_GET(PWR_COM1)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_SCLVHF1RIGHT)+POS_GET(POS_PANEL_STATE)*4;} return 0;);
static MAKE_ICB(icb_knbvhf1righton  ,if(!PWR_GET(PWR_COM1)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_KNBVHF1RIGHT )+POS_GET(POS_PANEL_STATE)*20;} return 0;);
static MAKE_ICB(icb_knbvhf1lefton   ,if(!PWR_GET(PWR_COM1)){HIDE_IMAGE(pelement);}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_KNBVHF1LEFT  )+POS_GET(POS_PANEL_STATE)*20;} return 0;);
static MAKE_ICB(icb_covvhf1volon    ,if(!PWR_GET(PWR_COM1)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_PANEL_STATE);});
static MAKE_ICB(icb_knbvhf1rightoff ,if(!PWR_GET(PWR_COM1)){SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_KNBVHF1RIGHT )+POS_GET(POS_PANEL_STATE)*20;}else{HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_knbvhf1leftoff  ,if(!PWR_GET(PWR_COM1)){SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_KNBVHF1LEFT  )+POS_GET(POS_PANEL_STATE)*20;}else{HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_covvhf1voloff   ,if(!PWR_GET(PWR_COM1)){SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_PANEL_STATE);}else{HIDE_IMAGE(pelement); return -1;});
static MAKE_ICB(icb_ark1leftright	,if(!PWR_GET(PWR_COM1)){SHOW_IMAGE(pelement); CHK_BRT(); return AZS_GET(AZS_ARK1LEFTRIGHT)+POS_GET(POS_PANEL_STATE)*2;} else {HIDE_IMAGE(pelement);} return 0;);
static MAKE_ICB(icb_ark1leftrighton	,if(!PWR_GET(PWR_COM1)){HIDE_IMAGE(pelement);} else {SHOW_IMAGE(pelement); CHK_BRT(); return AZS_GET(AZS_ARK1LEFTRIGHT)+POS_GET(POS_PANEL_STATE)*2;} return 0;);

//////////////////////////////////////////////////////////////////////////

// ��������
static MAKE_MSCB(mcb_azs_vhf1on		    ,AZS_TGL(AZS_VHF1ON        ))
static MAKE_MSCB(mcb_ark1leftright	    ,AZS_TGL(AZS_ARK1LEFTRIGHT ))

static BOOL FSAPI mcb_glt_vhf1left1_l	(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	HND_DEC(HND_KNBVHF1LEFT);
	trigger_key_event(KEY_COM_RADIO_WHOLE_DEC,0);
	return true;
}

static BOOL FSAPI mcb_glt_vhf1left1_r	(PPIXPOINT relative_point,FLAGS32 mouse_flags)	
{
	HND_INC(HND_KNBVHF1LEFT);
	trigger_key_event(KEY_COM_RADIO_WHOLE_INC,0);
	return true;
}

static BOOL FSAPI mcb_glt_vhf1right1_l	(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	trigger_key_event(KEY_COM_RADIO_FRACT_DEC,0);
	HND_DEC(HND_KNBVHF1RIGHT);
	return TRUE; 
}

static BOOL FSAPI mcb_glt_vhf1right1_r	(PPIXPOINT relative_point,FLAGS32 mouse_flags) 
{
	trigger_key_event(KEY_COM_RADIO_FRACT_INC,0);
	HND_INC(HND_KNBVHF1RIGHT);
	return TRUE; 
}

static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_VHF1ON                  ))
static MAKE_TCB(tcb_02	,HND_TT(HND_KNBVHF1RIGHT            ))
static MAKE_TCB(tcb_03	,HND_TT(HND_KNBVHF1LEFT             ))
static MAKE_TCB(tcb_04	,AZS_TT(AZS_ARK1LEFTRIGHT           ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p5_4,HELP_NONE,0,0)

MOUSE_PBOX(0,0,P5_BACKGROUND4_D_SX,P5_BACKGROUND4_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

// �������� 
//MOUSE_TBOX(  "1",  341-OFFX,  463-OFFY, 42, 69,CURSOR_HAND,MOUSE_LR,mcb_azs_vhf1on)
MOUSE_TBOX( "4",  117-OFFX,  500-OFFY,117, 76,CURSOR_HAND,MOUSE_LR,mcb_ark1leftright)

// ���������
MOUSE_TSHB( "2",  670-OFFX,  455-OFFY, 80, 55,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_vhf1right1_l,mcb_glt_vhf1right1_r)
MOUSE_TSHB( "3",  390-OFFX,  455-OFFY, 80, 55,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_vhf1left1_l,mcb_glt_vhf1left1_r)


MOUSE_END       


//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p5_4_list; 
GAUGE_HEADER_FS700_EX(P5_BACKGROUND4_D_SX, "p5_4", &p5_4_list, rect_p5_4, 0, 0, 0, 0, p5_4); 

MY_ICON2	(p5_azs_ark1leftright	,P5_AZS_ARK1LEFTRIGHT_D_00	,NULL                   	,  90-OFFX,  500-OFFY,icb_ark1leftright		, 2 *PANEL_LIGHT_MAX	, p5_4)	
MY_ICON2	(p5_azs_ark1leftrighton	,P5_AZS_ARK1LEFTRIGHTON_D_00,&l_p5_azs_ark1leftright	,  90-OFFX,  500-OFFY,icb_ark1leftrighton	, 2 *PANEL_LIGHT_MAX	, p5_4)	
MY_ICON2	(p5_scl_vhf1righton		,P5_SCL_VHF1RIGHTON_D_00	,&l_p5_azs_ark1leftrighton	, 599-OFFX,  397-OFFY,icb_sclvhf1righton	,4 *PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_scl_vhf1rightoff	,P5_SCL_VHF1RIGHTOFF_D_00	,&l_p5_scl_vhf1righton	, 599-OFFX,  397-OFFY,icb_sclvhf1rightoff	,4 *PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_scl_vhf1midon		,P5_SCL_VHF1MIDON_D_00		,&l_p5_scl_vhf1rightoff	, 569-OFFX,  397-OFFY,icb_sclvhf1midon		,10*PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_scl_vhf1midoff		,P5_SCL_VHF1MIDOFF_D_00		,&l_p5_scl_vhf1midon	, 569-OFFX,  397-OFFY,icb_sclvhf1midoff		,10*PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_cov_vhf1volumeon	,P5_COV_VHF1VOLUMEON_D		,&l_p5_scl_vhf1midoff	, 324-OFFX,  384-OFFY,icb_covvhf1volon		,4 *PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_knb_vhf1righton		,P5_KNB_VHF1RIGHTON_D_00	,&l_p5_cov_vhf1volumeon	, 683-OFFX,  453-OFFY,icb_knbvhf1righton	,20*PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_knb_vhf1lefton		,P5_KNB_VHF1LEFTON_D_00		,&l_p5_knb_vhf1righton	, 400-OFFX,  453-OFFY,icb_knbvhf1lefton		,20*PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_cov_vhf1volumeoff	,P5_COV_VHF1VOLUMEOFF_D		,&l_p5_knb_vhf1lefton	, 324-OFFX,  384-OFFY,icb_covvhf1voloff 	,4 *PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_knb_vhf1rightoff	,P5_KNB_VHF1RIGHTOFF_D_00	,&l_p5_cov_vhf1volumeoff, 683-OFFX,  453-OFFY,icb_knbvhf1rightoff	,20*PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_knb_vhf1leftoff		,P5_KNB_VHF1LEFTOFF_D_00	,&l_p5_knb_vhf1rightoff	, 400-OFFX,  453-OFFY,icb_knbvhf1leftoff	,20*PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_scl_vhf1lefton		,P5_SCL_VHF1LEFTON_D_00		,&l_p5_knb_vhf1leftoff	, 484-OFFX,  397-OFFY,icb_sclvhf1lefton		,18*PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_scl_vhf1leftoff		,P5_SCL_VHF1LEFTOFF_D_00	,&l_p5_scl_vhf1lefton	, 484-OFFX,  397-OFFY,icb_sclvhf1leftoff	,18*PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_azs_vhf1on			,P5_AZS_VHF1ON_D_00			,&l_p5_scl_vhf1leftoff	, 341-OFFX,  463-OFFY,icb_azsvhf1on  		,4 *PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_cov_vhf1on			,P5_COV_VHF1ON_D			,&l_p5_azs_vhf1on		,  89-OFFX,  276-OFFY,icb_covvh1on			,4 *PANEL_LIGHT_MAX	, p5_4)
MY_ICON2	(p5_4Ico				,P5_BACKGROUND4_D			,&l_p5_cov_vhf1on		,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX	, p5_4) 
MY_STATIC2	(p5_4bg,p5_4_list		,P5_BACKGROUND4_D			,&l_p5_4Ico				,	 p5_4);

#endif
