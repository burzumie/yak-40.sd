/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "P5.h"

#ifdef _DEBUG
DLLMAIND("F:\\MemLeaks\\Yak40\\P5.log")
#else
DLLMAIN()
#endif

double FSAPI icb_Ico(PELEMENT_ICON pelement) 
{
	CHK_BRT();
	return POS_GET(POS_PANEL_STATE); 
} 

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&p5_1),
#ifndef ONLY3D
		(&p5_2),
		(&p5_3),
		(&p5_4),
		(&p5_5),
		(&p5_6),
		(&p5_7),
		(&p5_8),
		(&p5_9),
		(&p5_10),
#endif
		0
	}
};

