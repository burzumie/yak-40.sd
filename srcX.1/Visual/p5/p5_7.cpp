/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	815
#define OFFY	637

//////////////////////////////////////////////////////////////////////////

static NONLINEARITY tbl_ark2signal[]={
	{{418+585-OFFX, 696-OFFY},     0,0},
	{{426+585-OFFX, 691-OFFY},  1000,0},
	{{436+585-OFFX, 689-OFFY},  2500,0},
	{{445+585-OFFX, 688-OFFY},  7500,0},
	{{455+585-OFFX, 689-OFFY}, 25000,0},
	{{465+585-OFFX, 691-OFFY}, 67500,0},
	{{472+585-OFFX, 696-OFFY},200000,0}
};

static MAKE_ICB(icb_ark2right1knb	,SHOW_GLT(GLT_ARK2RIGHT1KNB,10));
static MAKE_ICB(icb_ark2left1knb	,SHOW_GLT(GLT_ARK2LEFT1KNB,10));
static MAKE_ICB(icb_ark2righthnd	,SHOW_GLT(GLT_ARK2RIGHTHND,10));
static MAKE_ICB(icb_ark2lefthnd		,SHOW_GLT(GLT_ARK2LEFTHND,10));
static MAKE_ICB(icb_ark2right1		,SHOW_GLT(GLT_ARK2RIGHT1,10));
static MAKE_ICB(icb_ark2left1		,SHOW_GLT(GLT_ARK2LEFT1,10));
static MAKE_ICB(icb_ark2right10		,SHOW_GLT(GLT_ARK2RIGHT10,10));
static MAKE_ICB(icb_ark2left10		,SHOW_GLT(GLT_ARK2LEFT10,10));
static MAKE_ICB(icb_ark2right100	,SHOW_GLT(GLT_ARK2RIGHT100,12));
static MAKE_ICB(icb_ark2left100		,SHOW_GLT(GLT_ARK2LEFT100,12));
static MAKE_ICB(icb_ark2       		,SHOW_GLT(GLT_ARK2,4));
#ifdef HAVE_DAY_PANEL
static MAKE_NCB(icb_ndlark2signalD	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){SHOW_IMAGE(pelement);if(!PWR_GET(PWR_ADF2)){return 0;}else{CHK_BRT(); return NDL_GET(NDL_ARK2SIGNAL);}}else HIDE_IMAGE(pelement);return -1;);
#endif
#ifdef HAVE_MIX_PANEL
static MAKE_NCB(icb_ndlark2signalM	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){SHOW_IMAGE(pelement);if(!PWR_GET(PWR_ADF2)){return 0;}else{CHK_BRT(); return NDL_GET(NDL_ARK2SIGNAL);}}else HIDE_IMAGE(pelement);return -1;);
#endif
#ifdef HAVE_PLF_PANEL
static MAKE_NCB(icb_ndlark2signalP	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){SHOW_IMAGE(pelement);if(!PWR_GET(PWR_ADF2)){return 0;}else{CHK_BRT(); return NDL_GET(NDL_ARK2SIGNAL);}}else HIDE_IMAGE(pelement);return -1;);
#endif
#ifdef HAVE_RED_PANEL
static MAKE_NCB(icb_ndlark2signalR	,if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){SHOW_IMAGE(pelement);if(!PWR_GET(PWR_ADF2)){return 0;}else{CHK_BRT(); return NDL_GET(NDL_ARK2SIGNAL);}}else HIDE_IMAGE(pelement);return -1;);
#endif
static MAKE_ICB(icb_sclark2signal	,if( PWR_GET(PWR_ADF2)){CHK_BRT(); return POS_GET(POS_PANEL_STATE);} else {return -1;});
static MAKE_ICB(icb_ark2dir     	,SHOW_AZS(AZS_ARK2DIR ,3));
static MAKE_ICB(icb_ark2cw      	,SHOW_AZS(AZS_ARK2CW  ,2));
static MAKE_ICB(icb_ark2a			,if(!PWR_GET(PWR_ADF2)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark2b			,if(!PWR_GET(PWR_ADF2)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark2c			,if(!PWR_GET(PWR_ADF2)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark2d			,if(!PWR_GET(PWR_ADF2)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark2e			,if(!PWR_GET(PWR_ADF2)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark2f			,if(!PWR_GET(PWR_ADF2)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});
static MAKE_ICB(icb_ark2g			,if(!PWR_GET(PWR_ADF2)){HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return 0;});

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_ark2dir_l          ,PRS_AZS(AZS_ARK2DIR       ,1))
static MAKE_MSCB(mcb_ark2dir_r          ,PRS_AZS(AZS_ARK2DIR       ,2))
static MAKE_MSCB(mcb_ark2cw		        ,AZS_TGL(AZS_ARK2CW        ))

// ���������
static MAKE_MSCB(mcb_glt_ark2_l		    ,GLT_DEC(GLT_ARK2          ))
static MAKE_MSCB(mcb_glt_ark2_r		    ,GLT_INC(GLT_ARK2          ))
static MAKE_MSCB(mcb_glt_ark2left100_l	,GLT_DEC(GLT_ARK2LEFT100   ))
static MAKE_MSCB(mcb_glt_ark2left100_r	,GLT_INC(GLT_ARK2LEFT100   ))
static MAKE_MSCB(mcb_glt_ark2right100_l ,GLT_DEC(GLT_ARK2RIGHT100  ))
static MAKE_MSCB(mcb_glt_ark2right100_r ,GLT_INC(GLT_ARK2RIGHT100  ))
static MAKE_MSCB(mcb_glt_ark2left10_l	,GLT_DEC(GLT_ARK2LEFT10    ); GLT_DEC(GLT_ARK2LEFTHND     ))
static MAKE_MSCB(mcb_glt_ark2left10_r	,GLT_INC(GLT_ARK2LEFT10    ); GLT_INC(GLT_ARK2LEFTHND     ))
static MAKE_MSCB(mcb_glt_ark2right10_l  ,GLT_DEC(GLT_ARK2RIGHT10   ); GLT_DEC(GLT_ARK2RIGHTHND    ))
static MAKE_MSCB(mcb_glt_ark2right10_r  ,GLT_INC(GLT_ARK2RIGHT10   ); GLT_INC(GLT_ARK2RIGHTHND    ))
static MAKE_MSCB(mcb_glt_ark2left1_l	,GLT_DEC(GLT_ARK2LEFT1     ); GLT_DEC(GLT_ARK2LEFT1KNB    ))
static MAKE_MSCB(mcb_glt_ark2left1_r	,GLT_INC(GLT_ARK2LEFT1     ); GLT_INC(GLT_ARK2LEFT1KNB    ))
static MAKE_MSCB(mcb_glt_ark2right1_l   ,GLT_DEC(GLT_ARK2RIGHT1    ); GLT_DEC(GLT_ARK2RIGHT1KNB   ))
static MAKE_MSCB(mcb_glt_ark2right1_r   ,GLT_INC(GLT_ARK2RIGHT1    ); GLT_INC(GLT_ARK2RIGHT1KNB   ))

static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_ARK2DIR                 ))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_ARK2CW                  ))
static MAKE_TCB(tcb_03	,GLT_TT(GLT_ARK2                    ))
static MAKE_TCB(tcb_04	,GLT_TT(GLT_ARK2LEFT100             ))
static MAKE_TCB(tcb_05	,GLT_TT(GLT_ARK2RIGHT100            ))
static MAKE_TCB(tcb_06	,GLT_TT(GLT_ARK2LEFTHND             ))
static MAKE_TCB(tcb_07	,GLT_TT(GLT_ARK2RIGHTHND            ))
static MAKE_TCB(tcb_08	,GLT_TT(GLT_ARK2LEFT1KNB            ))
static MAKE_TCB(tcb_09	,GLT_TT(GLT_ARK2RIGHT1KNB           ))
static MAKE_TCB(tcb_10	,NDL_TT(NDL_ARK2SIGNAL				))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p5_7,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P5_BACKGROUND7_D_SX,P5_BACKGROUND7_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_TTPB( "10",  870-OFFX, 666-OFFY,110, 77)    // NDL_ARK1SIGNAL				    	   
//MOUSE_TSHB( "1", 1233-OFFX,  681-OFFY, 43, 33,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_ark2dir_l,mcb_ark2dir_r)
//MOUSE_TBOX( "2",  860-OFFX,  764-OFFY, 34, 46,CURSOR_HAND,MOUSE_LR,mcb_ark2cw)

// ���������
MOUSE_TSHB( "3", 1095-OFFX,  709-OFFY,128, 96,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark2_l,mcb_glt_ark2_r)
MOUSE_TSHB( "4",  948-OFFX,  825-OFFY,160,148,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark2left100_l,mcb_glt_ark2left100_r)
MOUSE_TSHB( "5", 1190-OFFX,  825-OFFY,160,148,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark2right100_l,mcb_glt_ark2right100_r)
MOUSE_TSHB( "6",  979-OFFX,  859-OFFY, 98, 95,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark2left10_l,mcb_glt_ark2left10_r)
MOUSE_TSHB( "7", 1224-OFFX,  859-OFFY, 98, 95,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark2right10_l,mcb_glt_ark2right10_r)
MOUSE_TSHB( "8",  877-OFFX,  883-OFFY, 55, 54,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark2left1_l,mcb_glt_ark2left1_r)
MOUSE_TSHB( "9", 1121-OFFX,  883-OFFY, 55, 54,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_ark2right1_l,mcb_glt_ark2right1_r)
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p5_7_list; 
GAUGE_HEADER_FS700_EX(P5_BACKGROUND7_D_SX, "p5_7", &p5_7_list, rect_p5_7, 0, 0, 0, 0, p5_7); 

#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p5_swt_ark2dir			,P5_SWT_ARK2DIR_D_00		,NULL						,1233-OFFX,  681-OFFY,icb_ark2dir     		, 3 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_azs_ark2cw			,P5_AZS_ARK2_CW_D_00		,&l_p5_swt_ark2dir			, 860-OFFX,  764-OFFY,icb_ark2cw      		, 2 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2volume		,P5_COV_ARK2VOLUME_D		,&l_p5_azs_ark2cw			,1297-OFFX,  729-OFFY,icb_Ico          		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right1knb	,P5_KNB_ARK2RIGHT1_D_00		,&l_p5_cov_ark2volume		,1121-OFFX,  883-OFFY,icb_ark2right1knb		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left1knb	,P5_KNB_ARK2LEFT1_D_00		,&l_p5_glt_ark2right1knb	, 877-OFFX,  883-OFFY,icb_ark2left1knb		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2righthnd	,P5_GLT_ARK2RIGHTHANDLE_D_00,&l_p5_glt_ark2left1knb		,1205-OFFX,  844-OFFY,icb_ark2righthnd 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2lefthnd		,P5_GLT_ARK2LEFTHANDLE_D_00	,&l_p5_glt_ark2righthnd		, 958-OFFX,  844-OFFY,icb_ark2lefthnd 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2right		,P5_COV_ARK2RIGHT_D			,&l_p5_glt_ark2lefthnd		,1186-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2left		,P5_COV_ARK2LEFT_D			,&l_p5_cov_ark2right		, 942-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right1		,P5_GLT_ARK2RIGHT1_D_00		,&l_p5_cov_ark2left			,1235-OFFX,  871-OFFY,icb_ark2right1 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left1		,P5_GLT_ARK2LEFT1_D_00		,&l_p5_glt_ark2right1		, 991-OFFX,  871-OFFY,icb_ark2left1 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right10		,P5_GLT_ARK2RIGHT10_D_00	,&l_p5_glt_ark2left1		,1224-OFFX,  859-OFFY,icb_ark2right10		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left10		,P5_GLT_ARK2LEFT10_D_00		,&l_p5_glt_ark2right10		, 979-OFFX,  859-OFFY,icb_ark2left10		, 10*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2right100	,P5_GLT_ARK2RIGHT100_D_00	,&l_p5_glt_ark2left10		,1190-OFFX,  825-OFFY,icb_ark2right100		, 12*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2left100		,P5_GLT_ARK2LEFT100_D_00	,&l_p5_glt_ark2right100		, 948-OFFX,  825-OFFY,icb_ark2left100		, 12*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2			,P5_GLT_ARK2_D_00			,&l_p5_glt_ark2left100		,1095-OFFX,  709-OFFY,icb_ark2       		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_cov_ark2signal		,P5_COV_ARK2SIGNAL_D		,&l_p5_glt_ark2				, 995-OFFX,  721-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_NEEDLE2	(p5_ndl_ark2signalD		,P5_NDL_ARK2SIGNAL_D		,&l_p5_cov_ark2signal		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalD, tbl_ark2signal, 18  , p5_7)		  
MY_NEEDLE2	(p5_ndl_ark2signalM		,P5_NDL_ARK2SIGNAL_M		,&l_p5_ndl_ark2signalD		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalM, tbl_ark2signal, 18  , p5_7)		  
MY_NEEDLE2	(p5_ndl_ark2signalP		,P5_NDL_ARK2SIGNAL_P		,&l_p5_ndl_ark2signalM		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalP, tbl_ark2signal, 18  , p5_7)		  
MY_NEEDLE2	(p5_ndl_ark2signalR		,P5_NDL_ARK2SIGNAL_R		,&l_p5_ndl_ark2signalP		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalR, tbl_ark2signal, 18  , p5_7)		  
MY_ICON2	(p5_scl_ark2signal		,P5_SCL_ARK2SIGNAL_D		,&l_p5_ndl_ark2signalR		, 969-OFFX,  661-OFFY,icb_sclark2signal		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2a			,P5_TBL_ARK2A				,&l_p5_scl_ark2signal		, 856-OFFX,  732-OFFY,icb_ark2a       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2b			,P5_TBL_ARK2B				,&l_p5_tbl_ark2a			, 856-OFFX,  795-OFFY,icb_ark2b       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2c			,P5_TBL_ARK2C				,&l_p5_tbl_ark2b			, 858-OFFX,  855-OFFY,icb_ark2c       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2d			,P5_TBL_ARK2D				,&l_p5_tbl_ark2c			,1103-OFFX,  855-OFFY,icb_ark2d       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2e			,P5_TBL_ARK2E				,&l_p5_tbl_ark2d			,1101-OFFX,  834-OFFY,icb_ark2e       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_tbl_ark2f			,P5_TBL_ARK2F				,&l_p5_tbl_ark2e			,1095-OFFX,  685-OFFY,icb_ark2f       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_tbl_ark2g			,P5_TBL_ARK2G				,&l_p5_tbl_ark2f			,1287-OFFX,  721-OFFY,icb_ark2g       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_7Ico				,P5_BACKGROUND7_D			,&l_p5_tbl_ark2g			,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX		, p5_7) 
MY_STATIC2	(p5_7bg,p5_7_list		,P5_BACKGROUND7_D			,&l_p5_7Ico					,	 p5_7);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_ICON2	(p5_swt_ark2dir			,P5_SWT_ARK2DIR_D_00		,NULL						,1233-OFFX,  681-OFFY,icb_ark2dir     		, 3 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_azs_ark2cw			,P5_AZS_ARK2_CW_D_00		,&l_p5_swt_ark2dir			, 860-OFFX,  764-OFFY,icb_ark2cw      		, 2 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2volume		,P5_COV_ARK2VOLUME_D		,&l_p5_azs_ark2cw			,1297-OFFX,  729-OFFY,icb_Ico          		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right1knb	,P5_KNB_ARK2RIGHT1_D_00		,&l_p5_cov_ark2volume		,1121-OFFX,  883-OFFY,icb_ark2right1knb		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left1knb	,P5_KNB_ARK2LEFT1_D_00		,&l_p5_glt_ark2right1knb	, 877-OFFX,  883-OFFY,icb_ark2left1knb		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2righthnd	,P5_GLT_ARK2RIGHTHANDLE_D_00,&l_p5_glt_ark2left1knb	,1205-OFFX,  844-OFFY,icb_ark2righthnd 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2lefthnd		,P5_GLT_ARK2LEFTHANDLE_D_00	,&l_p5_glt_ark2righthnd	, 958-OFFX,  844-OFFY,icb_ark2lefthnd 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2right		,P5_COV_ARK2RIGHT_D			,&l_p5_glt_ark2lefthnd		,1186-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2left		,P5_COV_ARK2LEFT_D			,&l_p5_cov_ark2right		, 942-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right1		,P5_GLT_ARK2RIGHT1_D_00		,&l_p5_cov_ark2left		,1235-OFFX,  871-OFFY,icb_ark2right1 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left1		,P5_GLT_ARK2LEFT1_D_00		,&l_p5_glt_ark2right1		, 991-OFFX,  871-OFFY,icb_ark2left1 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right10		,P5_GLT_ARK2RIGHT10_D_00	,&l_p5_glt_ark2left1		,1224-OFFX,  859-OFFY,icb_ark2right10		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left10		,P5_GLT_ARK2LEFT10_D_00		,&l_p5_glt_ark2right10		, 979-OFFX,  859-OFFY,icb_ark2left10		, 10*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2right100	,P5_GLT_ARK2RIGHT100_D_00	,&l_p5_glt_ark2left10		,1190-OFFX,  825-OFFY,icb_ark2right100		, 12*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2left100		,P5_GLT_ARK2LEFT100_D_00	,&l_p5_glt_ark2right100	, 948-OFFX,  825-OFFY,icb_ark2left100		, 12*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2			,P5_GLT_ARK2_D_00			,&l_p5_glt_ark2left100		,1095-OFFX,  709-OFFY,icb_ark2       		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_cov_ark2signal		,P5_COV_ARK2SIGNAL_D		,&l_p5_glt_ark2			, 995-OFFX,  721-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_NEEDLE2	(p5_ndl_ark2signalD		,P5_NDL_ARK2SIGNAL_D		,&l_p5_cov_ark2signal		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalD, tbl_ark2signal, 18  , p5_7)		  
MY_NEEDLE2	(p5_ndl_ark2signalP		,P5_NDL_ARK2SIGNAL_P		,&l_p5_ndl_ark2signalD		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalP, tbl_ark2signal, 18  , p5_7)		  
MY_ICON2	(p5_scl_ark2signal		,P5_SCL_ARK2SIGNAL_D		,&l_p5_ndl_ark2signalP		, 969-OFFX,  661-OFFY,icb_sclark2signal		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2a			,P5_TBL_ARK2A				,&l_p5_scl_ark2signal		, 856-OFFX,  732-OFFY,icb_ark2a       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2b			,P5_TBL_ARK2B				,&l_p5_tbl_ark2a			, 856-OFFX,  795-OFFY,icb_ark2b       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2c			,P5_TBL_ARK2C				,&l_p5_tbl_ark2b			, 858-OFFX,  855-OFFY,icb_ark2c       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2d			,P5_TBL_ARK2D				,&l_p5_tbl_ark2c			,1103-OFFX,  855-OFFY,icb_ark2d       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2e			,P5_TBL_ARK2E				,&l_p5_tbl_ark2d			,1101-OFFX,  834-OFFY,icb_ark2e       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_tbl_ark2f			,P5_TBL_ARK2F				,&l_p5_tbl_ark2e			,1095-OFFX,  685-OFFY,icb_ark2f       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_tbl_ark2g			,P5_TBL_ARK2G				,&l_p5_tbl_ark2f			,1287-OFFX,  721-OFFY,icb_ark2g       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_7Ico				,P5_BACKGROUND7_D			,&l_p5_tbl_ark2g			,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX		, p5_7) 
MY_STATIC2	(p5_7bg,p5_7_list		,P5_BACKGROUND7_D			,&l_p5_7Ico				,	 p5_7);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_ICON2	(p5_swt_ark2dir			,P5_SWT_ARK2DIR_D_00		,NULL						,1233-OFFX,  681-OFFY,icb_ark2dir     		, 3 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_azs_ark2cw			,P5_AZS_ARK2_CW_D_00		,&l_p5_swt_ark2dir			, 860-OFFX,  764-OFFY,icb_ark2cw      		, 2 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2volume		,P5_COV_ARK2VOLUME_D		,&l_p5_azs_ark2cw			,1297-OFFX,  729-OFFY,icb_Ico          		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right1knb	,P5_KNB_ARK2RIGHT1_D_00		,&l_p5_cov_ark2volume		,1121-OFFX,  883-OFFY,icb_ark2right1knb		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left1knb	,P5_KNB_ARK2LEFT1_D_00		,&l_p5_glt_ark2right1knb	, 877-OFFX,  883-OFFY,icb_ark2left1knb		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2righthnd	,P5_GLT_ARK2RIGHTHANDLE_D_00,&l_p5_glt_ark2left1knb	,1205-OFFX,  844-OFFY,icb_ark2righthnd 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2lefthnd		,P5_GLT_ARK2LEFTHANDLE_D_00	,&l_p5_glt_ark2righthnd	, 958-OFFX,  844-OFFY,icb_ark2lefthnd 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2right		,P5_COV_ARK2RIGHT_D			,&l_p5_glt_ark2lefthnd		,1186-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2left		,P5_COV_ARK2LEFT_D			,&l_p5_cov_ark2right		, 942-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right1		,P5_GLT_ARK2RIGHT1_D_00		,&l_p5_cov_ark2left		,1235-OFFX,  871-OFFY,icb_ark2right1 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left1		,P5_GLT_ARK2LEFT1_D_00		,&l_p5_glt_ark2right1		, 991-OFFX,  871-OFFY,icb_ark2left1 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right10		,P5_GLT_ARK2RIGHT10_D_00	,&l_p5_glt_ark2left1		,1224-OFFX,  859-OFFY,icb_ark2right10		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left10		,P5_GLT_ARK2LEFT10_D_00		,&l_p5_glt_ark2right10		, 979-OFFX,  859-OFFY,icb_ark2left10		, 10*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2right100	,P5_GLT_ARK2RIGHT100_D_00	,&l_p5_glt_ark2left10		,1190-OFFX,  825-OFFY,icb_ark2right100		, 12*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2left100		,P5_GLT_ARK2LEFT100_D_00	,&l_p5_glt_ark2right100	, 948-OFFX,  825-OFFY,icb_ark2left100		, 12*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2			,P5_GLT_ARK2_D_00			,&l_p5_glt_ark2left100		,1095-OFFX,  709-OFFY,icb_ark2       		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_cov_ark2signal		,P5_COV_ARK2SIGNAL_D		,&l_p5_glt_ark2			, 995-OFFX,  721-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_NEEDLE2	(p5_ndl_ark2signalD		,P5_NDL_ARK2SIGNAL_D		,&l_p5_cov_ark2signal		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalD, tbl_ark2signal, 18  , p5_7)		  
MY_NEEDLE2	(p5_ndl_ark2signalR		,P5_NDL_ARK2SIGNAL_R		,&l_p5_ndl_ark2signalD		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalR, tbl_ark2signal, 18  , p5_7)		  
MY_ICON2	(p5_scl_ark2signal		,P5_SCL_ARK2SIGNAL_D		,&l_p5_ndl_ark2signalR		, 969-OFFX,  661-OFFY,icb_sclark2signal		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2a			,P5_TBL_ARK2A				,&l_p5_scl_ark2signal		, 856-OFFX,  732-OFFY,icb_ark2a       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2b			,P5_TBL_ARK2B				,&l_p5_tbl_ark2a			, 856-OFFX,  795-OFFY,icb_ark2b       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2c			,P5_TBL_ARK2C				,&l_p5_tbl_ark2b			, 858-OFFX,  855-OFFY,icb_ark2c       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2d			,P5_TBL_ARK2D				,&l_p5_tbl_ark2c			,1103-OFFX,  855-OFFY,icb_ark2d       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2e			,P5_TBL_ARK2E				,&l_p5_tbl_ark2d			,1101-OFFX,  834-OFFY,icb_ark2e       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_tbl_ark2f			,P5_TBL_ARK2F				,&l_p5_tbl_ark2e			,1095-OFFX,  685-OFFY,icb_ark2f       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_tbl_ark2g			,P5_TBL_ARK2G				,&l_p5_tbl_ark2f			,1287-OFFX,  721-OFFY,icb_ark2g       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_7Ico				,P5_BACKGROUND7_D			,&l_p5_tbl_ark2g			,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX		, p5_7) 
MY_STATIC2	(p5_7bg,p5_7_list		,P5_BACKGROUND7_D			,&l_p5_7Ico				,	 p5_7);
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_ICON2	(p5_swt_ark2dir			,P5_SWT_ARK2DIR_D_00		,NULL						,1233-OFFX,  681-OFFY,icb_ark2dir     		, 3 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_azs_ark2cw			,P5_AZS_ARK2_CW_D_00		,&l_p5_swt_ark2dir			, 860-OFFX,  764-OFFY,icb_ark2cw      		, 2 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2volume		,P5_COV_ARK2VOLUME_D		,&l_p5_azs_ark2cw			,1297-OFFX,  729-OFFY,icb_Ico          		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right1knb	,P5_KNB_ARK2RIGHT1_D_00		,&l_p5_cov_ark2volume		,1121-OFFX,  883-OFFY,icb_ark2right1knb		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left1knb	,P5_KNB_ARK2LEFT1_D_00		,&l_p5_glt_ark2right1knb	, 877-OFFX,  883-OFFY,icb_ark2left1knb		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2righthnd	,P5_GLT_ARK2RIGHTHANDLE_D_00,&l_p5_glt_ark2left1knb	,1205-OFFX,  844-OFFY,icb_ark2righthnd 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2lefthnd		,P5_GLT_ARK2LEFTHANDLE_D_00	,&l_p5_glt_ark2righthnd	, 958-OFFX,  844-OFFY,icb_ark2lefthnd 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2right		,P5_COV_ARK2RIGHT_D			,&l_p5_glt_ark2lefthnd		,1186-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_cov_ark2left		,P5_COV_ARK2LEFT_D			,&l_p5_cov_ark2right		, 942-OFFX,  842-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right1		,P5_GLT_ARK2RIGHT1_D_00		,&l_p5_cov_ark2left		,1235-OFFX,  871-OFFY,icb_ark2right1 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left1		,P5_GLT_ARK2LEFT1_D_00		,&l_p5_glt_ark2right1		, 991-OFFX,  871-OFFY,icb_ark2left1 		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2right10		,P5_GLT_ARK2RIGHT10_D_00	,&l_p5_glt_ark2left1		,1224-OFFX,  859-OFFY,icb_ark2right10		, 10*PANEL_LIGHT_MAX	, p5_7)	
MY_ICON2	(p5_glt_ark2left10		,P5_GLT_ARK2LEFT10_D_00		,&l_p5_glt_ark2right10		, 979-OFFX,  859-OFFY,icb_ark2left10		, 10*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2right100	,P5_GLT_ARK2RIGHT100_D_00	,&l_p5_glt_ark2left10		,1190-OFFX,  825-OFFY,icb_ark2right100		, 12*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2left100		,P5_GLT_ARK2LEFT100_D_00	,&l_p5_glt_ark2right100	, 948-OFFX,  825-OFFY,icb_ark2left100		, 12*PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_glt_ark2			,P5_GLT_ARK2_D_00			,&l_p5_glt_ark2left100		,1095-OFFX,  709-OFFY,icb_ark2       		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_cov_ark2signal		,P5_COV_ARK2SIGNAL_D		,&l_p5_glt_ark2			, 995-OFFX,  721-OFFY,icb_Ico        		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_NEEDLE2	(p5_ndl_ark2signalD		,P5_NDL_ARK2SIGNAL_D		,&l_p5_cov_ark2signal		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalD, tbl_ark2signal, 18  , p5_7)		  
MY_NEEDLE2	(p5_ndl_ark2signalM		,P5_NDL_ARK2SIGNAL_M		,&l_p5_ndl_ark2signalD		,1030-OFFX,  724-OFFY, 4, 7,icb_ndlark2signalM, tbl_ark2signal, 18  , p5_7)		  
MY_ICON2	(p5_scl_ark2signal		,P5_SCL_ARK2SIGNAL_D		,&l_p5_ndl_ark2signalM		, 969-OFFX,  661-OFFY,icb_sclark2signal		, 4 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2a			,P5_TBL_ARK2A				,&l_p5_scl_ark2signal		, 856-OFFX,  732-OFFY,icb_ark2a       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2b			,P5_TBL_ARK2B				,&l_p5_tbl_ark2a			, 856-OFFX,  795-OFFY,icb_ark2b       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2c			,P5_TBL_ARK2C				,&l_p5_tbl_ark2b			, 858-OFFX,  855-OFFY,icb_ark2c       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2d			,P5_TBL_ARK2D				,&l_p5_tbl_ark2c			,1103-OFFX,  855-OFFY,icb_ark2d       		, 1 *PANEL_LIGHT_MAX	, p5_7)		
MY_ICON2	(p5_tbl_ark2e			,P5_TBL_ARK2E				,&l_p5_tbl_ark2d			,1101-OFFX,  834-OFFY,icb_ark2e       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_tbl_ark2f			,P5_TBL_ARK2F				,&l_p5_tbl_ark2e			,1095-OFFX,  685-OFFY,icb_ark2f       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_tbl_ark2g			,P5_TBL_ARK2G				,&l_p5_tbl_ark2f			,1287-OFFX,  721-OFFY,icb_ark2g       		, 1 *PANEL_LIGHT_MAX	, p5_7)			
MY_ICON2	(p5_7Ico				,P5_BACKGROUND7_D			,&l_p5_tbl_ark2g			,		 0,			0,icb_Ico				,PANEL_LIGHT_MAX		, p5_7) 
MY_STATIC2	(p5_7bg,p5_7_list		,P5_BACKGROUND7_D			,&l_p5_7Ico				,	 p5_7);
#endif

#endif
