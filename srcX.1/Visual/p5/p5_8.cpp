/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P5.h"

#define OFFX	0
#define OFFY	989

//////////////////////////////////////////////////////////////////////////

static MAKE_ICB(icb_fire1      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY1,2))
static MAKE_ICB(icb_fire2      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY2,2))
static MAKE_ICB(icb_fire3      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY3,2))
static MAKE_ICB(icb_fire4      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG1    ,2))
static MAKE_ICB(icb_fire5      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG2    ,2))
static MAKE_ICB(icb_fire6      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_ENG3    ,2))
static MAKE_ICB(icb_fire7      		,SHOW_BTN(BTN_MAN_FIRE_EXTNG_APU     ,2))
static MAKE_ICB(icb_fire8      		,SHOW_BTN(BTN_FIRE_LIGHTS_TEST		 ,2))
static MAKE_ICB(icb_crs_01			,SHOW_POSM(POS_CRS5_01,1,0))

//////////////////////////////////////////////////////////////////////////

// ������ 
static MAKE_MSCB(mcb_fire1				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY1	))
static MAKE_MSCB(mcb_fire2				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY2	))
static MAKE_MSCB(mcb_fire3				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG_BAY3	))
static MAKE_MSCB(mcb_fire4				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG1		))
static MAKE_MSCB(mcb_fire5				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG2		))
static MAKE_MSCB(mcb_fire6				,PRS_BTN(BTN_MAN_FIRE_EXTNG_ENG3		))
static MAKE_MSCB(mcb_fire7				,PRS_BTN(BTN_MAN_FIRE_EXTNG_APU			))
static MAKE_MSCB(mcb_fire8				,PRS_BTN(BTN_FIRE_LIGHTS_TEST			))

// �������
static BOOL FSAPI mcb_crs_clear			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,0);
		POS_SET(POS_CRS5_02,0);
		POS_SET(POS_CRS5_03,0);
	}
	return true;
}

static BOOL FSAPI mcb_crs_p0_01			(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS5_01,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P5);
		panel_window_open_ident(IDENT_P0);
	}
	return true;
}

static MAKE_TCB(tcb_01	,POS_TT(POS_CRS5_01	                ))
static MAKE_TCB(tcb_02	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG_BAY1	))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG_BAY2	))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG_BAY3	))
static MAKE_TCB(tcb_05	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG1		))
static MAKE_TCB(tcb_06	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG2		))
static MAKE_TCB(tcb_07	,BTN_TT(BTN_MAN_FIRE_EXTNG_ENG3		))
static MAKE_TCB(tcb_08	,BTN_TT(BTN_MAN_FIRE_EXTNG_APU		))
static MAKE_TCB(tcb_09	,BTN_TT(BTN_FIRE_LIGHTS_TEST		))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	) 
MAKE_TTA(tcb_07	)
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p5_8,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P5_BACKGROUND8_D_SX,P5_BACKGROUND8_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

// �������  
MOUSE_TBOX(  "1",  10-OFFX, 1106-OFFY, 81,80,CURSOR_HAND,MOUSE_MLR,mcb_crs_p0_01)

// ������
MOUSE_TBOX( "2",  215-OFFX, 1051-OFFY, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_fire1)
MOUSE_TBOX( "3",  291-OFFX, 1051-OFFY, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_fire2)
MOUSE_TBOX( "4",  390-OFFX, 1051-OFFY, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_fire3)
MOUSE_TBOX( "5",  233-OFFX, 1141-OFFY, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_fire4)
MOUSE_TBOX( "6",  303-OFFX, 1141-OFFY, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_fire5)
MOUSE_TBOX( "7",  400-OFFX, 1141-OFFY, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_fire6)
MOUSE_TBOX( "8",  470-OFFX, 1141-OFFY, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_fire7)
MOUSE_TBOX( "9",  465-OFFX, 1051-OFFY, 60, 60,CURSOR_HAND,MOUSE_DLR,mcb_fire8)

MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p5_8_list; 
GAUGE_HEADER_FS700_EX(P5_BACKGROUND8_D_SX, "p5_8", &p5_8_list, rect_p5_8, 0, 0, 0, 0, p5_8); 

MY_ICON2	(p5_crs_01			,MISC_CRS_P5_01				,NULL			,  10-OFFX, 1106-OFFY,icb_crs_01,1                	, p5_8)
MY_ICON2	(fire1			    ,P5_BTN_FIREENGBAY2_D_00	,&l_p5_crs_01	, 220-OFFX, 1055-OFFY,icb_fire1	,2*PANEL_LIGHT_MAX	, p5_8)
MY_ICON2	(fire2			    ,P5_BTN_FIREENGBAY2_D_00	,&l_fire1		, 294-OFFX, 1055-OFFY,icb_fire2	,2*PANEL_LIGHT_MAX	, p5_8)
MY_ICON2	(fire3			    ,P5_BTN_FIREENGBAY2_D_00	,&l_fire2		, 395-OFFX, 1055-OFFY,icb_fire3	,2*PANEL_LIGHT_MAX	, p5_8)
MY_ICON2	(fire4			    ,P5_BTN_FIREENG1_D_00		,&l_fire3		, 230-OFFX, 1143-OFFY,icb_fire4	,2*PANEL_LIGHT_MAX	, p5_8)
MY_ICON2	(fire5			    ,P5_BTN_FIREENG2_D_00		,&l_fire4		, 304-OFFX, 1143-OFFY,icb_fire5	,2*PANEL_LIGHT_MAX	, p5_8)
MY_ICON2	(fire6			    ,P5_BTN_FIREENG3_D_00		,&l_fire5		, 405-OFFX, 1143-OFFY,icb_fire6	,2*PANEL_LIGHT_MAX	, p5_8)
MY_ICON2	(fire7			    ,P5_BTN_FIREENG4_D_00		,&l_fire6		, 477-OFFX, 1143-OFFY,icb_fire7	,2*PANEL_LIGHT_MAX	, p5_8)
MY_ICON2	(fire8			    ,P5_BTN_FIRELTSTEST_D_00	,&l_fire7		, 467-OFFX, 1055-OFFY,icb_fire8	,2*PANEL_LIGHT_MAX	, p5_8)
MY_ICON2	(p5_8Ico			,P5_BACKGROUND8_D			,&l_fire8		,		 0,			0,icb_Ico	,PANEL_LIGHT_MAX	, p5_8) 
MY_STATIC2	(p5_8bg,p5_8_list	,P5_BACKGROUND8_D			,&l_p5_8Ico		, p5_8);


#endif