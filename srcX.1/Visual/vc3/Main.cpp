/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"

SIMPLE_GAUGE_VC				(Cnc01			,VC3_CNC01_D				);
SIMPLE_GAUGE_V�_LANG2		(Cnc02			,VC3_CNC02_D,VC3_CNC02INTL1_D,VC3_CNC02INTL2_D,0,0,4,0,4,168);
SIMPLE_GAUGE_VC_LANG1		(Cnc03			,VC3_CNC03_D,VC3_CNC03INTL_D,0,0,32,4);
SIMPLE_GAUGE_VC_LANG1_MOUSE	(Cnc04			,VC3_CNC04_D,VC3_CNC04INTL_D,0,0,45,16,AZS_KMP_RCVR,0,0,VC3_CNC04_D_SX,VC3_CNC04_D_SY);
SIMPLE_GAUGE_VC				(Cnc05			,VC3_CNC05_D				);
SIMPLE_GAUGE_VC				(Cnc06			,VC3_CNC06_D				);
SIMPLE_GAUGE_V�_LANG2		(Cnc07			,VC3_CNC07_D,VC3_CNC07INTL1_D,VC3_CNC07INTL2_D,0,0,6,27,168,41);
SIMPLE_GAUGE_V�_LANG2		(Cnc08			,VC3_CNC08_D,VC3_CNC08INTL1_D,VC3_CNC08INTL2_D,0,0,5,25,35,115);
SIMPLE_GAUGE_VC_LANG1		(Cnc09			,VC3_CNC09_D,VC3_CNC09INTL_D,0,0,39,5);
SIMPLE_GAUGE_VC_LANG1		(Cnc10			,VC3_CNC10_D,VC3_CNC10INTL_D,0,0,7,0);
SIMPLE_GAUGE_VC_LANG1		(Cnc11			,VC3_CNC11_D,VC3_CNC11INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC_LANG1		(Cnc12			,VC3_CNC12_D,VC3_CNC12INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC_LANG1		(Cnc13			,VC3_CNC13_D,VC3_CNC13INTL_D,0,0,27,0);
SIMPLE_GAUGE_VC_LANG1		(Cnc14			,VC3_CNC14_D,VC3_CNC14INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC				(Cnc15			,VC3_CNC15_D				);
SIMPLE_GAUGE_VC				(Cnc16			,VC3_CNC16_D				);
SIMPLE_GAUGE_VC				(Cnc17			,VC3_CNC17_D				);
SIMPLE_GAUGE_VC_LANG1		(Cnc18			,VC3_CNC18_D,VC3_CNC18INTL_D,0,0,9,18);
SIMPLE_GAUGE_VC				(Cnc19			,VC3_CNC19_D				);
SIMPLE_GAUGE_V�_LANG1_TRIM	(Cnc20		,VC3_CNC20_D,VC3_CNC20INTL_D,0,0,11,0,KEY_AILERON_TRIM_LEFT,KEY_AILERON_TRIM_RIGHT,AZS_TRIM_ELERON,0,110,140,79,AZS_ALER_TRIM_BUS);
SIMPLE_GAUGE_VC				(Cnc21			,VC3_CNC21_D				);
SIMPLE_GAUGE_V�_LANG3		(Cnc22			,VC3_CNC22_D,VC3_CNC22INTL1_D,VC3_CNC22INTL2_D,VC3_CNC22INTL3_D,0,0,67,17,7,17,7,199);
SIMPLE_GAUGE_VC				(Cnc23			,VC3_CNC23_D				);
SIMPLE_GAUGE_VC				(Cnc24			,VC3_CNC24_D				);
SIMPLE_GAUGE_VC_LANG1		(Cnc25			,VC3_CNC25_D,VC3_CNC25INTL_D,0,0,125,8);
SIMPLE_GAUGE_VC_LANG1		(Cnc26			,VC3_CNC26_D,VC3_CNC26INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC				(Cnc26side		,VC3_CNC26SIDE_D			);
SIMPLE_GAUGE_VC				(Cnc27			,VC3_CNC27_D				);
SIMPLE_GAUGE_V�_LANG1_TRIM	(Cnc28		,VC3_CNC28_D,VC3_CNC28INTL_D,0,0,0,0,KEY_RUDDER_TRIM_LEFT,KEY_RUDDER_TRIM_RIGHT,AZS_TRIM_RUDDER,180,0,130,63,AZS_RUDDER_TRIM_BUS);
SIMPLE_GAUGE_VC_LANG1		(Cnc29			,VC3_CNC29_D,VC3_CNC29INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC				(Cnc30			,VC3_CNC30_D				);
SIMPLE_GAUGE_VC				(Cnc31			,VC3_CNC31_D				);
SIMPLE_GAUGE_VC_LANG1		(Cnc32			,VC3_CNC32_D,VC3_CNC32INTL_D,0,0,29,35);
SIMPLE_GAUGE_VC_LANG1		(Cnc33			,VC3_CNC33_D,VC3_CNC33INTL_D,0,0,0,0);
SIMPLE_GAUGE_VC				(Cnc34			,VC3_CNC34_D				);
SIMPLE_GAUGE_VC				(Cnc35			,VC3_CNC35_D				);
SIMPLE_GAUGE_VC				(Cnc36			,VC3_CNC36_D				);
SIMPLE_GAUGE_VC				(Cnc37			,VC3_CNC37_D				);
SIMPLE_GAUGE_VC				(Cnc38			,VC3_CNC38_D				);
SIMPLE_GAUGE_VC				(CncSide		,VC3_BCK_CNCSIDE_D			);
SIMPLE_GAUGE_VC				(CncRud1		,VC3_CNCRUD1_D				);
SIMPLE_GAUGE_VC				(CncRud2		,VC3_CNCRUD2_D				);
SIMPLE_GAUGE_VC				(CncRud3		,VC3_CNCRUD3_D				);
SIMPLE_GAUGE_VC				(CncRudHndlSide	,VC3_CNCRUDHNDLSIDE_D		);
SIMPLE_GAUGE_VC				(CncRudLever	,VC3_CNCRUDLEVER_D			);
SIMPLE_GAUGE_VC				(CncRudStop		,VC3_CNCRUDSTOP_D			);
SIMPLE_GAUGE_VC				(CncRudStopSide	,VC3_CNCRUDSTOPSIDE_D		);
SIMPLE_GAUGE_VC				(CncRudStoptop	,VC3_CNCRUDSTOPTOP_D		);
SIMPLE_GAUGE_VC				(CncTop			,VC3_BCK_CNCTOP_D			);
SIMPLE_GAUGE_VC				(CncFront		,VC3_BCK_CNCFRONT_D			);

#ifdef _DEBUG
DLLMAIND("F:\\MemLeaks\\Yak40\\VC3.log")
#else
DLLMAIN()
#endif

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&Cnc01					),
		(&Cnc02					),
		(&Cnc03					),
		(&Cnc04					),
		(&Cnc05					),        
		(&Cnc06					),        
		(&Cnc07					),        
		(&Cnc08					),        
		(&Cnc09					),        
		(&Cnc10					),        
		(&Cnc11					),        
		(&Cnc12					),        
		(&Cnc13					),        
		(&Cnc14					),        
		(&Cnc15					),        
		(&Cnc16					),        
		(&Cnc17					),        
		(&Cnc18					),        
		(&Cnc19					),        
		(&Cnc20					),        
		(&Cnc21					),            
		(&Cnc22					),
		(&Cnc23					),
		(&Cnc24					),
		(&Cnc25					),
		(&Cnc26					),        
		(&Cnc26side				),        
		(&Cnc27					),        
		(&Cnc28					),        
		(&Cnc29					),        
		(&Cnc30					),        
		(&Cnc31					),        
		(&Cnc32					),        
		(&Cnc33					),        
		(&Cnc34					),        
		(&Cnc35					),        
		(&Cnc36					),        
		(&Cnc37					),        
		(&Cnc38					),        
		(&CncSide				),        
		(&CncRud1				),        
		(&CncRud2				),            
		(&CncRud3				),
		(&CncRudHndlSide		),
		(&CncRudLever			),
		(&CncRudStop			),
		(&CncRudStopSide		),        
		(&CncRudStoptop			),        
		(&CncTop				),        
		(&CncFront				),        
		0					
	}											
};												
							
