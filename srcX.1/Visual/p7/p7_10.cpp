/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

#define OFFX	1259
#define OFFY	794

static int curs=0;

static MAKE_ICB(icb_crs_p7_02		,CHK_BRT(); return curs-1;)

static BOOL FSAPI mcb_crs_p10_01(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		curs=1;
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_toggle(IDENT_P9);
	}
	return true;
}

static BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		curs=0;
	}
	return true;
}

static MAKE_TCB(tcb_01,return "Toggle right AZS panel";)

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p7_10,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P7_BACKGROUND10_D_SX,P7_BACKGROUND10_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_TBOX("1",277,0, 54,106,CURSOR_HAND,MOUSE_MLR,mcb_crs_p10_01)
MOUSE_END       

extern PELEMENT_HEADER p7_10_list; 
GAUGE_HEADER_FS700_EX(P7_BACKGROUND10_D_SX, "p7_10", &p7_10_list, rect_p7_10, 0, 0, 0, 0, p7_10); 

MY_ICON2	(p7_crs_02			,MISC_CRS_P0_04			,NULL			,277, 0,icb_crs_p7_02 ,1,p7_10)
MY_ICON2	(p7_10Ico			,P7_BACKGROUND10_D		,&l_p7_crs_02	,        0,         0,icb_Ico       ,   PANEL_LIGHT_MAX,p7_10)
MY_STATIC2	(p7_10bg,p7_10_list	,P7_BACKGROUND10_D		,&l_p7_10Ico		,p7_10)


#endif