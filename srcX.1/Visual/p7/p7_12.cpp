/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

double FSAPI icb_IcoLit(PELEMENT_ICON pelement);
double FSAPI icb_IcoIntl(PELEMENT_ICON pelement);
double FSAPI icb_IcoIntlLit(PELEMENT_ICON pelement);

static double FSAPI icb_kmprfreq(PELEMENT_ICON pelement)
{
	CHK_BRT();
	double val=HND_GET(HND_KMP2FREQ);
	if(val>9)
		val=0;
	return val+POS_GET(POS_PANEL_STATE)*10; 
}

double FSAPI icb_kmprherz	(PELEMENT_ICON pelement);
double FSAPI icb_kmp2vorl  	(PELEMENT_ICON pelement);
double FSAPI icb_kmp2pwrl  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst1l  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst2l 	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst3l  	(PELEMENT_ICON pelement);
double FSAPI icb_kmp2vor  	(PELEMENT_ICON pelement);
double FSAPI icb_kmp2pwr  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst1  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst2  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst3  	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr1scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr2scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr3scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr4scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr5scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr1scl	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr2scl	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr3scl	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr4scl	(PELEMENT_ICON pelement);
double FSAPI icb_kmpr5scl	(PELEMENT_ICON pelement);

BOOL FSAPI mcb_kmp2_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_kmpfreq_r(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_kmpfreq_l(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_kmpfreqr_l(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_kmpfreqr_r(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_MSCB(mcb_azs_kmp2vor		    ,AZS_TGL(AZS_KMP2VOR    	))
static MAKE_MSCB(mcb_azs_kmp2pwr		    ,AZS_TGL(AZS_KMP2PWR    	))
static MAKE_MSCB(mcb_btn_kmp2id				,BTN_TGL(BTN_KMP2ID			))
static MAKE_MSCB(mcb_btn_kmp2test1		    ,PRS_BTN(BTN_KMP2TEST1  	))
static MAKE_MSCB(mcb_btn_kmp2test2		    ,PRS_BTN(BTN_KMP2TEST2  	))
static MAKE_MSCB(mcb_btn_kmp2test3		    ,PRS_BTN(BTN_KMP2TEST3  	))

static MAKE_TCB(tcb_01	,AZS_TT(AZS_KMP2VOR				))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_KMP2PWR				))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_KMP2TEST1	        ))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_KMP2TEST2	        ))
static MAKE_TCB(tcb_05	,BTN_TT(BTN_KMP2TEST3	        ))
static MAKE_TCB(tcb_06	,BTN_TT(BTN_KMP2ID				))
static MAKE_TCB(tcb_08	,HND_TT(HND_KMP2FREQ	        ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MAKE_TTA(tcb_02) 
MAKE_TTA(tcb_03)
MAKE_TTA(tcb_04)
MAKE_TTA(tcb_05)
MAKE_TTA(tcb_06)
MAKE_TTA(tcb_08)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p7_12,HELP_NONE,0,0)
MOUSE_PBOX(30,30,P20_BCK_MPNAV2_D_SX,P20_BCK_MPNAV2_D_SY,CURSOR_NONE,MOUSE_LR,mcb_kmp2_big)

MOUSE_TBOX(  "1",  94,   28, 33, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp2vor)
MOUSE_TBOX(  "2", 363,   28, 33, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp2pwr)
MOUSE_TBOX(  "3", 152,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2test1)
MOUSE_TBOX(  "4", 223,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2test2)
MOUSE_TBOX(  "5", 297,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp2test3)
MOUSE_TSHB(  "7", 340,   83,120, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreqr_l,mcb_glt_kmpfreqr_r)
MOUSE_TBOX(  "6", 370,   87, 35, 90,CURSOR_HAND,MOUSE_LR ,mcb_btn_kmp2id)
MOUSE_TSHB(  "7",  50,   84, 90, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreq_l,mcb_glt_kmpfreq_r)

MOUSE_END       

extern PELEMENT_HEADER p7_12_list; 
GAUGE_HEADER_FS700_EX(P20_BCK_MPNAV2_D_SX, "p7_12", &p7_12_list, rect_p7_12, 0, 0, 0, 0, p7_12); 

MY_ICON2	(p7_12knb_kmplf		, P20_KNB_MPFREQ_D_00			,NULL            		,  54,   87,icb_kmprfreq  	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12knb_kmplh		, P20_BTN_MPAUDIO_D_00			,&l_p7_12knb_kmplf		, 349,   87,icb_kmprherz  	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12azs_kmp1vor	, P20_AZS_MPFREQDME_D_00		,&l_p7_12knb_kmplh		,  94,   28,icb_kmp2vor    	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12azs_kmp1vorl	, P20_AZS_MPFREQDMELIT_D_00		,&l_p7_12azs_kmp1vor	,  94,   28,icb_kmp2vorl    , 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12azs_kmp1pwr	, P20_AZS_MPFREQPWR_D_00		,&l_p7_12azs_kmp1vorl	, 363,   28,icb_kmp2pwr    	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12azs_kmp1pwrl	, P20_AZS_MPFREQPWRLIT_D_00		,&l_p7_12azs_kmp1pwr	, 363,   28,icb_kmp2pwrl    , 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl1		, P20_SCL_MPFREQ1_D_00			,&l_p7_12azs_kmp1pwrl	, 181,   16,icb_kmpr1scl  	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl1l	, P20_SCL_MPFREQ1LIT_D_00		,&l_p7_12scl_kmpl1		, 181,   16,icb_kmpr1scll 	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl2		, P20_SCL_MPFREQ2_D_00			,&l_p7_12scl_kmpl1l		, 204,   16,icb_kmpr2scl  	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl2l	, P20_SCL_MPFREQ2LIT_D_00		,&l_p7_12scl_kmpl2		, 204,   16,icb_kmpr2scll 	, 2 *PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl3		, P20_SCL_MPFREQ3_D_00			,&l_p7_12scl_kmpl2l		, 227,   16,icb_kmpr3scl  	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl3l	, P20_SCL_MPFREQ3LIT_D_00		,&l_p7_12scl_kmpl3		, 227,   16,icb_kmpr3scll 	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl4		, P20_SCL_MPFREQ4_D_00			,&l_p7_12scl_kmpl3l		, 250,   16,icb_kmpr4scl  	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl4l	, P20_SCL_MPFREQ4LIT_D_00		,&l_p7_12scl_kmpl4		, 250,   16,icb_kmpr4scll 	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl5		, P20_SCL_MPFREQ5_D_00			,&l_p7_12scl_kmpl4l		, 286,   16,icb_kmpr5scl  	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12scl_kmpl5l	, P20_SCL_MPFREQ5LIT_D_00		,&l_p7_12scl_kmpl5		, 286,   16,icb_kmpr5scll 	, 10*PANEL_LIGHT_MAX,p7_12)	
MY_ICON2	(p7_12IcoIntlLit	, P20_BCK_MPNAV2INTLLIT_D		,&l_p7_12scl_kmpl5l		,   0,    0,icb_IcoIntlLit	,    PANEL_LIGHT_MAX,p7_12)
MY_ICON2	(p7_12IcoIntl		, P20_BCK_MPNAV2INTL_D			,&l_p7_12IcoIntlLit		,   0,    0,icb_IcoIntl		,    PANEL_LIGHT_MAX,p7_12)
MY_ICON2	(p7_12IcoLit		, P20_BCK_MPNAV2LIT_D			,&l_p7_12IcoIntl		,   0,    0,icb_IcoLit		,    PANEL_LIGHT_MAX,p7_12)
MY_ICON2	(p7_12Ico			, P20_BCK_MPNAV2_D				,&l_p7_12IcoLit			,   0,    0,icb_Ico			,    PANEL_LIGHT_MAX,p7_12)
MY_STATIC2	(p7_12bg,p7_12_list	, P20_BCK_MPNAV2_D				,&l_p7_12Ico			,p7_12)
