/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

#define OFFX	355
#define OFFY	0

static MAKE_ICB(icb_pitot1heat		,SHOW_AZS(AZS_PITOTHEAT1	,2))
static MAKE_ICB(icb_pitot2heat		,SHOW_AZS(AZS_PITOTHEAT2	,2))
static MAKE_ICB(icb_icewarn1  		,SHOW_AZS(AZS_ICEWARN1		,2))
static MAKE_ICB(icb_icewarn2  		,SHOW_AZS(AZS_ICEWARN2		,2))
static MAKE_ICB(icb_eng1heat		,SHOW_AZS(AZS_ENG1HEAT		,2))
static MAKE_ICB(icb_eng2heat		,SHOW_AZS(AZS_ENG2HEAT		,2))
static MAKE_ICB(icb_eng3heat		,SHOW_AZS(AZS_ENG3HEAT		,2))
static MAKE_ICB(icb_ltstest  		,SHOW_BTN(BTN_LIGHTS_TEST7	,2))
static MAKE_ICB(icb_eng1deice       ,SHOW_LMP(LMP_ENG1_DEICE	,BTN_GET(BTN_LIGHTS_TEST7),3) )
static MAKE_ICB(icb_eng2deice       ,SHOW_LMP(LMP_ENG2_DEICE	,BTN_GET(BTN_LIGHTS_TEST7),3) )
static MAKE_ICB(icb_eng3deice       ,SHOW_LMP(LMP_ENG3_DEICE	,BTN_GET(BTN_LIGHTS_TEST7),3) )

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_azs_pitot1h        ,AZS_TGL(AZS_PITOTHEAT1		); AZS_SET(AZS_PITOTHEAT2,AZS_GET(AZS_PITOTHEAT1));)
static MAKE_MSCB(mcb_azs_pitot2h        ,AZS_TGL(AZS_PITOTHEAT2		); AZS_SET(AZS_PITOTHEAT1,AZS_GET(AZS_PITOTHEAT2));)
static MAKE_MSCB(mcb_azs_icewarn1       ,AZS_TGL(AZS_ICEWARN1	    ))
static MAKE_MSCB(mcb_azs_icewarn2       ,AZS_TGL(AZS_ICEWARN2	    ))
static MAKE_MSCB(mcb_btn_ltstest  		,PRS_BTN(BTN_LIGHTS_TEST7	))
static MAKE_MSCB(mcb_azs_eng1heat       ,AZS_TGL(AZS_ENG1HEAT	    ))
static MAKE_MSCB(mcb_azs_eng2heat       ,AZS_TGL(AZS_ENG2HEAT	    ))
static MAKE_MSCB(mcb_azs_eng3heat       ,AZS_TGL(AZS_ENG3HEAT	    ))

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01	,AZS_TT(AZS_ENG1HEAT	        ))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_ENG2HEAT	        ))
static MAKE_TCB(tcb_03	,AZS_TT(AZS_ENG3HEAT	        ))
static MAKE_TCB(tcb_04	,AZS_TT(AZS_PITOTHEAT1			))
static MAKE_TCB(tcb_05	,AZS_TT(AZS_PITOTHEAT2			))
static MAKE_TCB(tcb_06	,AZS_TT(AZS_ICEWARN1	        ))
static MAKE_TCB(tcb_07	,AZS_TT(AZS_ICEWARN2	        ))
static MAKE_TCB(tcb_08	,BTN_TT(BTN_LIGHTS_TEST7	    ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p7_2,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P7_BACKGROUND2_D_SX,P7_BACKGROUND2_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_TBOX(  "1", 594-OFFX,  416-OFFY,108, 70,CURSOR_HAND,MOUSE_LR,mcb_azs_eng1heat)
MOUSE_TBOX(  "2", 602-OFFX,  346-OFFY,108, 70,CURSOR_HAND,MOUSE_LR,mcb_azs_eng2heat)
MOUSE_TBOX(  "3", 602-OFFX,  287-OFFY,108, 59,CURSOR_HAND,MOUSE_LR,mcb_azs_eng3heat)
MOUSE_TBOX(  "4", 355-OFFX,  403-OFFY, 92, 52,CURSOR_HAND,MOUSE_LR,mcb_azs_pitot1h)
MOUSE_TBOX(  "5", 363-OFFX,  362-OFFY, 92, 41,CURSOR_HAND,MOUSE_LR,mcb_azs_pitot2h)
MOUSE_TBOX(  "6", 371-OFFX,  316-OFFY, 92, 46,CURSOR_HAND,MOUSE_LR,mcb_azs_icewarn1)
MOUSE_TBOX(  "7", 378-OFFX,  270-OFFY, 92, 46,CURSOR_HAND,MOUSE_LR,mcb_azs_icewarn2)
MOUSE_TBOX(  "8", 627-OFFX,  228-OFFY, 83, 59,CURSOR_HAND,MOUSE_DLR,mcb_btn_ltstest)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p7_2_list; 
GAUGE_HEADER_FS700_EX(P7_BACKGROUND2_D_SX, "p7_2", &p7_2_list, rect_p7_2, 0, 0, 0, 0, p7_2); 

MY_ICON2	(p7_azs_pitot1heat	, P7_AZS_PITOT1HEAT_D_00,NULL               	, 355-OFFX,  403-OFFY,icb_pitot1heat	, 2 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_azs_pitot2heat	, P7_AZS_PITOT2HEAT_D_00,&l_p7_azs_pitot1heat	, 363-OFFX,  362-OFFY,icb_pitot2heat	, 2 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_azs_icewarn1	, P7_AZS_ICEWARN1_D_00	,&l_p7_azs_pitot2heat	, 371-OFFX,  316-OFFY,icb_icewarn1  	, 2 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_azs_icewarn2	, P7_AZS_ICEWARN2_D_00	,&l_p7_azs_icewarn1		, 378-OFFX,  270-OFFY,icb_icewarn2  	, 2 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_azs_eng1heat	, P7_AZS_ENG1HEAT_D_00	,&l_p7_azs_icewarn2		, 594-OFFX,  416-OFFY,icb_eng1heat		, 2 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_azs_eng2heat	, P7_AZS_ENG2HEAT_D_00	,&l_p7_azs_eng1heat		, 602-OFFX,  346-OFFY,icb_eng2heat		, 2 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_azs_eng3heat	, P7_AZS_ENG3HEAT_D_00	,&l_p7_azs_eng2heat		, 602-OFFX,  287-OFFY,icb_eng3heat		, 2 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_btn_ltstest		, P7_BTN_LTSTEST_D_00	,&l_p7_azs_eng3heat		, 627-OFFX,  228-OFFY,icb_ltstest  		, 2 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_lmp_eng1deice	, P7_LMP_ENG1DEICE_D_00	,&l_p7_btn_ltstest		, 470-OFFX,  394-OFFY,icb_eng1deice 	, 3 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_lmp_eng2deice	, P7_LMP_ENG2DEICE_D_00	,&l_p7_lmp_eng1deice	, 470-OFFX,  331-OFFY,icb_eng2deice 	, 3 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_lmp_eng3deice	, P7_LMP_ENG3DEICE_D_00	,&l_p7_lmp_eng2deice	, 470-OFFX,  270-OFFY,icb_eng3deice 	, 3 *PANEL_LIGHT_MAX,p7_2)
MY_ICON2	(p7_2Ico			, P7_BACKGROUND2_D		,&l_p7_lmp_eng3deice	,        0,         0,icb_Ico			,    PANEL_LIGHT_MAX,p7_2)
MY_STATIC2	(p7_2bg,p7_2_list	, P7_BACKGROUND2_D		,&l_p7_2Ico				,p7_2)


#endif