/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

#define OFFX	1259
#define OFFY	493

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_ICB(icb_dmeleft1m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME71M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft2m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME72M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft3m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME73M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft4m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME74M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft5m		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME75M)>-1?POS_GET(POS_PANEL_STATE):-1; )
static MAKE_ICB(icb_dmeleft1		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME71 )>-1?POS_GET(POS_DME71)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft2		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME72 )>-1?POS_GET(POS_DME72)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft3		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME73 )>-1?POS_GET(POS_DME73)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft4		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME74 )>-1?POS_GET(POS_DME74)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dmeleft5		,LIGHT_IMAGE(pelement); return POS_GET(POS_DME75 )>-1?POS_GET(POS_DME75)+POS_GET(POS_PANEL_STATE)*10:-1;)
static MAKE_ICB(icb_dme1			,SHOW_GLT(GLT_DME7	,2))

//////////////////////////////////////////////////////////////////////////

static BOOL FSAPI mcb_glt_dme(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	GLT_TGL(GLT_DME7);
	return TRUE;
}

BOOL FSAPI mcb_dme2_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P25);
	return true;
}

static MAKE_TCB(tcb_01	,GLT_TT(GLT_DME7			    ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p7_7,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P7_BACKGROUND7_D_SX,P7_BACKGROUND7_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_PBOX(0,0,P7_BACKGROUND7_D_SX,P7_BACKGROUND7_D_SY,CURSOR_HAND,MOUSE_LR,mcb_dme2_big)
MOUSE_TBOX( "1", 1370-OFFX,  703-OFFY, 55, 55,CURSOR_HAND,MOUSE_LR,mcb_glt_dme)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p7_7_list; 
GAUGE_HEADER_FS700_EX(P7_BACKGROUND7_D_SX, "p7_7", &p7_7_list, rect_p7_7, 0, 0, 0, 0, p7_7); 

MY_ICON2	(p7_scl_dmeleft1m	, P7_SCL_1DMERIGHT1_D__ 	,NULL              	, 1305-OFFX,  622-OFFY,icb_dmeleft1m ,PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_scl_dmeleft1	, P7_SCL_DMERIGHT1_D_00	,&l_p7_scl_dmeleft1m, 1305-OFFX,  622-OFFY,icb_dmeleft1  , 10*PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_scl_dmeleft2m	, P7_SCL_1DMERIGHT2_D__ ,&l_p7_scl_dmeleft1	, 1349-OFFX,  622-OFFY,icb_dmeleft2m ,    PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_scl_dmeleft2	, P7_SCL_DMERIGHT2_D_00	,&l_p7_scl_dmeleft2m, 1349-OFFX,  622-OFFY,icb_dmeleft2  , 10*PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_scl_dmeleft3m	, P7_SCL_1DMERIGHT3_D__ ,&l_p7_scl_dmeleft2	, 1375-OFFX,  622-OFFY,icb_dmeleft3m ,    PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_scl_dmeleft3	, P7_SCL_DMERIGHT3_D_00	,&l_p7_scl_dmeleft3m, 1375-OFFX,  622-OFFY,icb_dmeleft3  , 10*PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_scl_dmeleft4m	, P7_SCL_1DMERIGHT4_D__ ,&l_p7_scl_dmeleft3	, 1401-OFFX,  622-OFFY,icb_dmeleft4m ,    PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_scl_dmeleft4	, P7_SCL_DMERIGHT4_D_00	,&l_p7_scl_dmeleft4m, 1401-OFFX,  622-OFFY,icb_dmeleft4  , 10*PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_scl_dmeleft5m	, P7_SCL_1DMERIGHT5_D__ ,&l_p7_scl_dmeleft4	, 1436-OFFX,  622-OFFY,icb_dmeleft5m ,    PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_scl_dmeleft5	, P7_SCL_DMERIGHT5_D_00	,&l_p7_scl_dmeleft5m, 1436-OFFX,  622-OFFY,icb_dmeleft5  , 10*PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_knb_dme1		, P7_KNB_DME2_D_00		,&l_p7_scl_dmeleft5	, 1370-OFFX,  703-OFFY,icb_dme1      , 2 *PANEL_LIGHT_MAX,p7_7)
MY_ICON2	(p7_7Ico			, P7_BACKGROUND7_D		,&l_p7_knb_dme1		,        0,         0,icb_Ico       ,     PANEL_LIGHT_MAX,p7_7)
MY_STATIC2	(p7_7bg,p7_7_list	, P7_BACKGROUND7_D		,&l_p7_7Ico			,p7_7)


#endif
