/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P7.h"

#define OFFX	710
#define OFFY	0

#ifndef ONLY3D
SIMPLE_GAUGE(p7_3,P7_BACKGROUND3_D,P7_BACKGROUND3_D);
#endif

