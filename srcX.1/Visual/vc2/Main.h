/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 3 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../../Common/CommonSys.h"
#include "../../Common/Macros.h"
#include "../../Common/MacrosVisual.h"
#include "../../Common/CommonAircraft.h"
#include "../../api/ClientDef.h"
#include "../../api/APIEntry.h"
#include "../../Lib/PanelsTools.h"

#ifdef LITE_VERSION
#if		defined(HAVE_MIX_PANEL)
#include "../../../res/3d/vc2_res_m.h"
#elif	defined(HAVE_PLF_PANEL)
#include "../../../res/3d/vc2_res_p.h"
#elif	defined(HAVE_RED_PANEL)
#include "../../../res/3d/vc2_res_r.h"
#endif
#else
#include "../../../res/3d/vc2_resX.h"
#endif

GAUGE(Rad_btn_side		)
GAUGE(Rad_btn1			)
GAUGE(Rad_btn2			)
GAUGE(Rad_btn3			)
GAUGE(Rad_btn4			)
GAUGE(Rad_btnr_side		)
GAUGE(Rad_btnr1			)
GAUGE(Rad_btnr2			)
GAUGE(Rad_btnr3			)
GAUGE(Rad_btnr4			)
GAUGE(Rad_det1			)
GAUGE(Rad_det2			)
GAUGE(Rad_frn			)
GAUGE(Rad_knb1			)
GAUGE(Rad_knb2			)
GAUGE(Rad_knb3			)
GAUGE(Rad_knob_side		)
GAUGE(Rad_sde			)
GAUGE(Rad_shd1			)
GAUGE(Rad_shd2			)
GAUGE(Rad_shd3			)
GAUGE(Rad_shd4			)
GAUGE(Rad_shd5			)
GAUGE(Rad_shd6			)
GAUGE(Rad_shd7			)
GAUGE(Rad_shd8			)
GAUGE(Rad_shd9			)
GAUGE(Rad_shd10			)
GAUGE(Rad_shd11			)
GAUGE(Rad_shd12			)
GAUGE(Rad_shd13			)
GAUGE(Rad_top			)
GAUGE(Spg_sde			)
GAUGE(Spg_top			)
GAUGE(Clmn1				)
GAUGE(Clmn2				)
GAUGE(Clmn3				)
GAUGE(Clmn4				)
GAUGE(Clmn_nbld1_front	)
GAUGE(Clmn_nbld1_side	)
GAUGE(Clmn_nbld1_top	)
GAUGE(Clmn_rubber		)
GAUGE(Clmn_shaft		)
GAUGE(Clmn_shaft_base	)
GAUGE(Clmna_hndl1a		)
GAUGE(Clmna_hndl1b		)
GAUGE(Clmna_hndl2a		)
GAUGE(Clmna_hndl2b		)
GAUGE(Clmna_logo		)
GAUGE(Clmna_nbld2_front	)
GAUGE(Clmna_nbld2_side	)
GAUGE(Clmna_nbld2_top	)
GAUGE(Clmna_btn1_front	)
GAUGE(Clmna_btn1_side	)
GAUGE(Clmna_btn2_front	)
GAUGE(Clmna_btn2_side	)
GAUGE(Clmn_trim1		)
GAUGE(Clmna_trim2		)
GAUGE(Sts01				)
GAUGE(Sts02				)
GAUGE(Sts03				)
GAUGE(Sts04				)
GAUGE(Sts05				)
GAUGE(Sts06				)
GAUGE(Sts07				)
GAUGE(Sts08				)
GAUGE(Sts09				)
GAUGE(Sts10				)
GAUGE(Sts11				)
GAUGE(Sts12				)
GAUGE(Sts13				)
GAUGE(Sts14				)
GAUGE(Sts15				)
GAUGE(Sts16				)
GAUGE(StsHandleSide		)
GAUGE(StsHandleTop		)
GAUGE(StsRailSide		)
GAUGE(StsRailTop		)
GAUGE(StsSupport		)
GAUGE(PTSSpot01			)
GAUGE(PTSSpot02			)
GAUGE(PTSSpot03			)
GAUGE(PTSSpot04			)
GAUGE(PTSSpot05			)
GAUGE(SpotLightCap		)
GAUGE(SpotLightTop		)
