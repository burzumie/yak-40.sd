/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "PM.h"

#ifdef _DEBUG
DLLMAIND("f:\\MemLeaks\\Yak40\\PM.log")
#else
DLLMAIN()
#endif

double FSAPI icb_Ico(PELEMENT_ICON pelement) 
{
	CHK_BRT();
	return POS_GET(POS_PANEL_STATE); 
} 

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&pm_1),
		0
	}
};

