/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

  Last modification:
    $Date: 18.02.06 18:04 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "P1.h"

#ifdef _DEBUG
DLLMAIND("F:\\MemLeaks\\Yak40\\P1.log")
#else
DLLMAIN()
#endif

double FSAPI icb_Ico(PELEMENT_ICON pelement) 
{
	CHK_BRT();
	return POS_GET(POS_PANEL_STATE); 
} 

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
#ifndef ONLY3D
		(&p1_1),
		(&p1_2),
		(&p1_3),
		(&p1_4),
		(&p1_5),
		(&p1_7),
#endif
		(&p1_6),
		0
	}
};

