/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P1.h"

#define OFFX	1110
#define OFFY	468

static MAKE_ICB(icb_pwrsrc			,SHOW_AZS(AZS_POWERSOURCE                   ,3))
static MAKE_ICB(icb_pt500_rls		,SHOW_AZS(AZS_INV1_36V                      ,2))
static MAKE_ICB(icb_pt500_ap		,SHOW_AZS(AZS_INV2_36V                      ,2))
static MAKE_ICB(icb_radio_ruchn		,SHOW_AZS(AZS_115V_BCKUP                    ,2))
static MAKE_ICB(icb_po_steklo		,SHOW_AZS(AZS_INV1_115V                     ,2))
static MAKE_ICB(icb_po_radio		,SHOW_AZS(AZS_INV2_115V                     ,2))
static MAKE_ICB(icb_sbros_davl		,SHOW_AZS(AZS_AIR_PRESS_DROP_EMERG          ,2))
static MAKE_ICB(icb_dubler_reg_davl	,SHOW_AZS(AZS_AIR_PRESS_CONTROL_BCKUP       ,2))
static MAKE_ICB(icb_sist_vkl_avar	,SHOW_AZS(AZS_AIR_PRESS_SYS_MASTER_EMERG    ,3))
static MAKE_ICB(icb_zvuk_signal		,SHOW_AZS(AZS_AIR_PRESS_SYS_SOUND_ALARM     ,2))
static MAKE_ICB(icb_sist_vkl_norm	,SHOW_AZS(AZS_AIR_PRESS_SYS_MASTER          ,3))
static MAKE_ICB(icb_temp_salon		,SHOW_AZS(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN ,4))
static MAKE_ICB(icb_temp_turboholod	,SHOW_AZS(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT  ,4))
static MAKE_ICB(icb_rashod_avto		,SHOW_AZS(AZS_BLEEDAIR_EXTRACT_VALVE        ,4))
static MAKE_ICB(icb_rashod_rezh		,SHOW_AZS(AZS_BLEEDAIR_EXTRACT_RATIO        ,2))
static MAKE_ICB(icb_vozduh			,SHOW_AZS(AZS_DECK_COND                     ,2))
static MAKE_ICB(icb_trap_pitan		,SHOW_AZS(AZS_STAIRS_PWR                    ,2))
static MAKE_ICB(icb_trap_motor		,SHOW_AZS(AZS_STAIRS                        ,2))
static MAKE_ICB(icb_remni			,SHOW_AZS(AZS_NO_SMOKING_SIGNS              ,2))
static MAKE_ICB(icb_svet_red		,SHOW_AZS(AZS_REDLIGHT1	                    ,2))
static MAKE_ICB(icb_tablo21			,SHOW_TBL(TBL_OUTER_MARKER		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo22			,SHOW_TBL(TBL_MIDDLE_MARKER		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_tablo23			,SHOW_TBL(TBL_INNER_MARKER		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))

// ���
static FLOAT64 FSAPI icb_gmk_scale_offD	(PELEMENT_NEEDLE pelement)	{if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT();if(!PWR_GET(PWR_GMK)){SHOW_IMAGE(pelement);} else {HIDE_IMAGE(pelement);} return NDL_GET(NDL_GMK_SCALE)*4;}else HIDE_IMAGE(pelement);return -1;}
static FLOAT64 FSAPI icb_gmk_scale_offM	(PELEMENT_NEEDLE pelement)	{if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT();if(!PWR_GET(PWR_GMK)){SHOW_IMAGE(pelement);} else {HIDE_IMAGE(pelement);} return NDL_GET(NDL_GMK_SCALE)*4;}else HIDE_IMAGE(pelement);return -1;}
static FLOAT64 FSAPI icb_gmk_scale_offP	(PELEMENT_NEEDLE pelement)	{if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT();if(!PWR_GET(PWR_GMK)){SHOW_IMAGE(pelement);} else {HIDE_IMAGE(pelement);} return NDL_GET(NDL_GMK_SCALE)*4;}else HIDE_IMAGE(pelement);return -1;}
static FLOAT64 FSAPI icb_gmk_scale_offR	(PELEMENT_NEEDLE pelement)	{if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT();if(!PWR_GET(PWR_GMK)){SHOW_IMAGE(pelement);} else {HIDE_IMAGE(pelement);} return NDL_GET(NDL_GMK_SCALE)*4;}else HIDE_IMAGE(pelement);return -1;}
static FLOAT64 FSAPI icb_gmk_scale_onD	(PELEMENT_NEEDLE pelement)	{if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_DAY){CHK_BRT();if( PWR_GET(PWR_GMK)){SHOW_IMAGE(pelement);} else {HIDE_IMAGE(pelement);} return NDL_GET(NDL_GMK_SCALE)*4;}else HIDE_IMAGE(pelement);return -1;}
static FLOAT64 FSAPI icb_gmk_scale_onM	(PELEMENT_NEEDLE pelement)	{if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_MIX){CHK_BRT();if( PWR_GET(PWR_GMK)){SHOW_IMAGE(pelement);} else {HIDE_IMAGE(pelement);} return NDL_GET(NDL_GMK_SCALE)*4;}else HIDE_IMAGE(pelement);return -1;}
static FLOAT64 FSAPI icb_gmk_scale_onP	(PELEMENT_NEEDLE pelement)	{if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_PLF){CHK_BRT();if( PWR_GET(PWR_GMK)){SHOW_IMAGE(pelement);} else {HIDE_IMAGE(pelement);} return NDL_GET(NDL_GMK_SCALE)*4;}else HIDE_IMAGE(pelement);return -1;}
static FLOAT64 FSAPI icb_gmk_scale_onR	(PELEMENT_NEEDLE pelement)	{if(POS_GET(POS_PANEL_STATE)==PANEL_LIGHT_RED){CHK_BRT();if( PWR_GET(PWR_GMK)){SHOW_IMAGE(pelement);} else {HIDE_IMAGE(pelement);} return NDL_GET(NDL_GMK_SCALE)*4;}else HIDE_IMAGE(pelement);return -1;}

static FLOAT64 FSAPI icb_lamp_zap		(PELEMENT_ICON pelement)	{SHOW_LMP(LMP_GMK_ZAP			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),2)}																					   
static FLOAT64 FSAPI icb_lamp_osn		(PELEMENT_ICON pelement)	{SHOW_LMP(LMP_GMK_OSN			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),2)}
static FLOAT64 FSAPI icb_tablo_01		(PELEMENT_ICON pelement)	{CHK_BRT();return PWR_GET(PWR_GMK)?POS_GET(POS_PANEL_STATE)*2:0;}
static FLOAT64 FSAPI icb_tablo_02		(PELEMENT_ICON pelement)	{CHK_BRT();return PWR_GET(PWR_GMK)?POS_GET(POS_PANEL_STATE)*2:0;}
static FLOAT64 FSAPI icb_tablo_03		(PELEMENT_ICON pelement)	{CHK_BRT();return PWR_GET(PWR_GMK)?POS_GET(POS_PANEL_STATE)*2:0;}
static FLOAT64 FSAPI icb_tablo_04		(PELEMENT_ICON pelement)	{CHK_BRT();return PWR_GET(PWR_GMK)?POS_GET(POS_PANEL_STATE)*2:0;}
static FLOAT64 FSAPI icb_tablo_05		(PELEMENT_ICON pelement)	{CHK_BRT();return PWR_GET(PWR_GMK)?POS_GET(POS_PANEL_STATE)*2:0;}
static FLOAT64 FSAPI icb_tablo_06		(PELEMENT_ICON pelement)	{CHK_BRT();return PWR_GET(PWR_GMK)?POS_GET(POS_PANEL_STATE)*2:0;}
static FLOAT64 FSAPI icb_switch_01		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_HEMISPHERE	,2)}
static FLOAT64 FSAPI icb_switch_02		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_MODE          ,2)}
static FLOAT64 FSAPI icb_switch_03		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_TEST          ,3)}
static FLOAT64 FSAPI icb_switch_04		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_COURSE        ,3)}
static FLOAT64 FSAPI icb_switch_05		(PELEMENT_ICON pelement)	{SHOW_AZS(AZS_GMK_PRI_AUX       ,2)}
static FLOAT64 FSAPI icb_gmkknob		(PELEMENT_ICON pelement)	{SHOW_HND(HND_GMK               ,90)}   

static MAKE_ICB(icb_lamp17,SHOW_LMP(LMP_STAIRS_DWN					,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_lamp18,SHOW_LMP(LMP_INV_115V_FAIL				,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_lamp19,SHOW_LMPEXT(LMP_GROUND_ELEC_CONNECTED	,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_lamp20,SHOW_LMP(LMP_DIF_AIR_PRESS_DROP			,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_lamp21,SHOW_LMP(LMP_AIR_PRESSBCKCNTRUNITON		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_lamp22,SHOW_LMP(LMP_AIR_PRESS_COND_SYS_OFF		,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))
static MAKE_ICB(icb_lamp23,SHOW_LMP(LMP_AIR_CONDSYSTURBOCOOLERON	,(BTN_GET(BTN_LIGHTS_TEST0)||BTN_GET(BTN_LIGHTS_TEST1)),3))

static MAKE_ICB(icb_crs_01,SHOW_POSM(POS_CRS1_01,1,0))
static MAKE_ICB(icb_yoke  ,SHOW_POSM(POS_YOKE1_ICON,1,0))

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_p1_battery_top     ,AZS_SET(AZS_POWERSOURCE				,POWER_SOURCE_BAT))
static MAKE_MSCB(mcb_p1_battery_cnt     ,AZS_SET(AZS_POWERSOURCE				,POWER_SOURCE_OFF))
static MAKE_MSCB(mcb_p1_battery_btm     ,AZS_SET(AZS_POWERSOURCE				,POWER_SOURCE_RAP))
static MAKE_MSCB(mcb_p1_pt500_rls       ,AZS_TGL(AZS_INV1_36V					))
static MAKE_MSCB(mcb_p1_pt500_ap        ,AZS_TGL(AZS_INV2_36V					))
static MAKE_MSCB(mcb_p1_po1500_ruc      ,AZS_TGL(AZS_115V_BCKUP					))
static MAKE_MSCB(mcb_p1_po1500_ste      ,AZS_TGL(AZS_INV1_115V					))
static MAKE_MSCB(mcb_p1_po1500_rad      ,AZS_TGL(AZS_INV2_115V					))
static MAKE_MSCB(mcb_p1_sbros_davl      ,AZS_TGL(AZS_AIR_PRESS_DROP_EMERG		))
static MAKE_MSCB(mcb_p1_dubler_reg_davl ,AZS_TGL(AZS_AIR_PRESS_CONTROL_BCKUP	))
static BOOL FSAPI mcb_p1_sist_vkl_avar_top(PPIXPOINT relative_point,FLAGS32 mouse_flags)        
{
	if(AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG)==0||AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG)==2) {
		AZS_SET(AZS_AIR_PRESS_SYS_MASTER_EMERG,1);
	} else {
		AZS_SET(AZS_AIR_PRESS_SYS_MASTER_EMERG,0);
	}
	return TRUE;
}

static BOOL FSAPI mcb_p1_sist_vkl_avar_btm(PPIXPOINT relative_point,FLAGS32 mouse_flags)        
{
	if(AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG)==0||AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG)==2) {
		AZS_SET(AZS_AIR_PRESS_SYS_MASTER_EMERG,1);
	} else {
		AZS_SET(AZS_AIR_PRESS_SYS_MASTER_EMERG,2);
	}
	return TRUE;
}

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_MSCB(mcb_p1_zvuk_signal		,AZS_TGL(AZS_AIR_PRESS_SYS_SOUND_ALARM	))
static MAKE_MSCB(mcb_p1_sist_vkl_norm_top,AZS_SET(AZS_AIR_PRESS_SYS_MASTER		,1))
static MAKE_MSCB(mcb_p1_sist_vkl_norm_cnt,AZS_SET(AZS_AIR_PRESS_SYS_MASTER		,0))
static MAKE_MSCB(mcb_p1_sist_vkl_norm_btm,AZS_SET(AZS_AIR_PRESS_SYS_MASTER		,2))
static MAKE_MSCB(mcb_p1_temp_salon_top	,AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,3))
static MAKE_MSCB(mcb_p1_temp_salon_blt	,AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,1))
static MAKE_MSCB(mcb_p1_temp_salon_brt	,AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,2))
static MAKE_MSCB(mcb_p1_temp_salon_cnt	,AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN,0))
static MAKE_MSCB(mcb_p1_temp_turbo_top	,AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,3))
static MAKE_MSCB(mcb_p1_temp_turbo_blt	,AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,1))
static MAKE_MSCB(mcb_p1_temp_turbo_brt	,AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,2))
static MAKE_MSCB(mcb_p1_temp_turbo_cnt	,AZS_SET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT,0))
static MAKE_MSCB(mcb_p1_rash_auto		,AZS_SET(AZS_BLEEDAIR_EXTRACT_VALVE		,3))
static MAKE_MSCB(mcb_p1_rash_open		,AZS_SET(AZS_BLEEDAIR_EXTRACT_VALVE		,1))
static MAKE_MSCB(mcb_p1_rash_clse		,AZS_SET(AZS_BLEEDAIR_EXTRACT_VALVE		,2))
static MAKE_MSCB(mcb_p1_rash_cntr		,AZS_SET(AZS_BLEEDAIR_EXTRACT_VALVE		,0))
static MAKE_MSCB(mcb_p1_rash_rezh		,AZS_TGL(AZS_BLEEDAIR_EXTRACT_RATIO		))
static MAKE_MSCB(mcb_p1_vozduh			,AZS_TGL(AZS_DECK_COND					))
static MAKE_MSCB(mcb_p1_trap_pitan		,AZS_TGL(AZS_STAIRS_PWR					))
static MAKE_MSCB(mcb_p1_trap_motor		,AZS_TGL(AZS_STAIRS						))
static MAKE_MSCB(mcb_p1_signs			,AZS_TGL(AZS_NO_SMOKING_SIGNS			))
static MAKE_MSCB(mcb_p1_red_light		,AZS_TGL(AZS_REDLIGHT1); AZS_SET(AZS_REDLIGHT2,AZS_GET(AZS_REDLIGHT1)))
static BOOL FSAPI mcb_p1_gmk_01(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	AZS_TGL(AZS_GMK_HEMISPHERE);
	/*
	if(AZS_GET(AZS_GMK_01)==1)
	AZS_SET(AZS_GMK_01,2);
	else if(AZS_GET(AZS_GMK_01)==2)
	AZS_SET(AZS_GMK_01,1);
	*/
	return TRUE;
}

static BOOL FSAPI mcb_p1_gmk_02(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	AZS_TGL(AZS_GMK_MODE);
	/*
	if(AZS_GET(AZS_GMK_02)==1)
	AZS_SET(AZS_GMK_02,2);
	else if(AZS_GET(AZS_GMK_02)==2)
	AZS_SET(AZS_GMK_02,1);
	*/
	return TRUE;
}

static MAKE_MSCB(mcb_p1_gmk_03_left		,PRS_AZS(AZS_GMK_TEST					,1))
static MAKE_MSCB(mcb_p1_gmk_03_right	,PRS_AZS(AZS_GMK_TEST					,2))
static MAKE_MSCB(mcb_p1_gmk_04_left		,PRS_AZS(AZS_GMK_COURSE					,1))
static MAKE_MSCB(mcb_p1_gmk_04_right	,PRS_AZS(AZS_GMK_COURSE					,2))
static BOOL FSAPI mcb_p1_gmk_05(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	AZS_TGL(AZS_GMK_PRI_AUX);
	/*
	if(AZS_GET(AZS_GMK_05)==1)
	AZS_SET(AZS_GMK_05,2);
	else if(AZS_GET(AZS_GMK_05)==2)
	AZS_SET(AZS_GMK_05,1);
	*/
	return TRUE;
}
static MAKE_MSCB(mcb_check_lamps		,PRS_BTN(BTN_LIGHTS_TEST1				))

static BOOL FSAPI mcb_gmk_right(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		NDL_INC(NDL_GMK_SCALE);
		HND_INC(HND_GMK);
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		NDL_INC2(NDL_GMK_SCALE,10);
		HND_INC2(HND_GMK,10);
		if(HND_GET(HND_GMK)>89)HND_DEC2(HND_GMK,89);
	}
	return TRUE;
}

static BOOL FSAPI mcb_gmk_left(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_LEFTSINGLE) {
		NDL_DEC(NDL_GMK_SCALE);
		HND_DEC(HND_GMK);
	} else if(mouse_flags&MOUSE_RIGHTSINGLE) {
		NDL_DEC2(NDL_GMK_SCALE,10);
		HND_DEC2(HND_GMK,10);
		if(HND_GET(HND_GMK)<0)HND_INC2(HND_GMK,89);
	}
	return TRUE;
}

static BOOL FSAPI mcb_crs_p1_01(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS1_01,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P3);
		panel_window_close_ident(IDENT_P1);
		panel_window_open_ident(IDENT_P7);
	}
	return true;
}

static BOOL FSAPI mcb_yoke_icon(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_YOKE1_ICON,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_open_ident(IDENT_P3);
	}
	return true;
}

static BOOL FSAPI mcb_gmk_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P15);
	return true;
}


static MAKE_TCB(tcb_01	,POS_TT(POS_CRS1_01						))
static MAKE_TCB(tcb_02	,POS_TT(POS_YOKE1_ICON					))
static MAKE_TCB(tcb_03	,AZS_TT(AZS_POWERSOURCE					))
static MAKE_TCB(tcb_04	,AZS_TT(AZS_INV1_36V					))
static MAKE_TCB(tcb_05	,AZS_TT(AZS_INV2_36V					))
static MAKE_TCB(tcb_06	,AZS_TT(AZS_115V_BCKUP					))
static MAKE_TCB(tcb_07	,AZS_TT(AZS_INV1_115V					))
static MAKE_TCB(tcb_08	,AZS_TT(AZS_INV2_115V					))
static MAKE_TCB(tcb_09	,AZS_TT(AZS_AIR_PRESS_DROP_EMERG		))
static MAKE_TCB(tcb_10	,AZS_TT(AZS_AIR_PRESS_CONTROL_BCKUP		))
static MAKE_TCB(tcb_11	,AZS_TT(AZS_AIR_PRESS_SYS_MASTER_EMERG	))
static MAKE_TCB(tcb_12	,AZS_TT(AZS_AIR_PRESS_SYS_SOUND_ALARM	))
static MAKE_TCB(tcb_13	,AZS_TT(AZS_AIR_PRESS_SYS_MASTER		))
static MAKE_TCB(tcb_14	,AZS_TT(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN))
static MAKE_TCB(tcb_15	,AZS_TT(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT))
static MAKE_TCB(tcb_16	,AZS_TT(AZS_BLEEDAIR_EXTRACT_VALVE		))
static MAKE_TCB(tcb_17	,AZS_TT(AZS_BLEEDAIR_EXTRACT_RATIO		))
static MAKE_TCB(tcb_18	,AZS_TT(AZS_DECK_COND					))
static MAKE_TCB(tcb_19	,AZS_TT(AZS_STAIRS_PWR					))
static MAKE_TCB(tcb_20	,AZS_TT(AZS_STAIRS						))
static MAKE_TCB(tcb_21	,AZS_TT(AZS_NO_SMOKING_SIGNS			))
static MAKE_TCB(tcb_22	,AZS_TT(AZS_REDLIGHT1					))
static MAKE_TCB(tcb_23	,AZS_TT(AZS_GMK_HEMISPHERE				))
static MAKE_TCB(tcb_24	,AZS_TT(AZS_GMK_MODE					))
static MAKE_TCB(tcb_25	,AZS_TT(AZS_GMK_TEST					))
static MAKE_TCB(tcb_26	,AZS_TT(AZS_GMK_COURSE					))
static MAKE_TCB(tcb_27	,AZS_TT(AZS_GMK_PRI_AUX					))
static MAKE_TCB(tcb_28	,BTN_TT(BTN_LIGHTS_TEST1				))
static MAKE_TCB(tcb_29	,LMP_TT(LMP_STAIRS_DWN					))
static MAKE_TCB(tcb_30	,LMP_TT(LMP_INV_115V_FAIL				))
static MAKE_TCB(tcb_31	,LMP_TT(LMP_GROUND_ELEC_CONNECTED		))
static MAKE_TCB(tcb_32	,LMP_TT(LMP_DIF_AIR_PRESS_DROP			))
static MAKE_TCB(tcb_33	,LMP_TT(LMP_AIR_PRESSBCKCNTRUNITON		))
static MAKE_TCB(tcb_34	,LMP_TT(LMP_AIR_PRESS_COND_SYS_OFF		))
static MAKE_TCB(tcb_35	,LMP_TT(LMP_AIR_CONDSYSTURBOCOOLERON	))
static MAKE_TCB(tcb_36	,LMP_TT(LMP_GMK_ZAP						))
static MAKE_TCB(tcb_37	,LMP_TT(LMP_GMK_OSN						))
static MAKE_TCB(tcb_38	,TBL_TT(TBL_OUTER_MARKER				))
static MAKE_TCB(tcb_39	,TBL_TT(TBL_MIDDLE_MARKER				))
static MAKE_TCB(tcb_40	,TBL_TT(TBL_INNER_MARKER				))
static MAKE_TCB(tcb_41	,HND_TT(HND_GMK							))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MAKE_TTA(tcb_20	)
MAKE_TTA(tcb_21	)
MAKE_TTA(tcb_22	) 
MAKE_TTA(tcb_23	)
MAKE_TTA(tcb_24	)
MAKE_TTA(tcb_25	)
MAKE_TTA(tcb_26	)
MAKE_TTA(tcb_27	) 
MAKE_TTA(tcb_28	) 
MAKE_TTA(tcb_29	)
MAKE_TTA(tcb_30	)
MAKE_TTA(tcb_31	)
MAKE_TTA(tcb_32	) 
MAKE_TTA(tcb_33	)
MAKE_TTA(tcb_34	)
MAKE_TTA(tcb_35	)
MAKE_TTA(tcb_36	)
MAKE_TTA(tcb_37	) 
MAKE_TTA(tcb_38	)
MAKE_TTA(tcb_39	)
MAKE_TTA(tcb_40	)
MAKE_TTA(tcb_41	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p1_5,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P1_BACKGROUND5_SX,P1_BACKGROUND5_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_BOX(               18,       610, 190, 120,CURSOR_NONE,MOUSE_LR,mcb_gmk_big)    // GMK BIG
MOUSE_TBOX(  "1", 1479-OFFX, 1012-OFFY,MISC_CRS_P1_01_SX,MISC_CRS_P1_01_SY,CURSOR_HAND,MOUSE_MLR,mcb_crs_p1_01)
//MOUSE_TBOX(  "2", 1365-OFFX, 1040-OFFY,MISC_YOKE_SX     ,MISC_YOKE_SY     ,CURSOR_HAND,MOUSE_MLR,mcb_yoke_icon)
MOUSE_TSV3(  "3", 1128-OFFX,  771-OFFY, 47, 99,CURSOR_HAND,CURSOR_HAND,CURSOR_HAND,MOUSE_LR,mcb_p1_battery_top,mcb_p1_battery_cnt,mcb_p1_battery_btm)
MOUSE_TBOX(  "4", 1177-OFFX,  771-OFFY, 30, 99,CURSOR_HAND,MOUSE_LR,mcb_p1_pt500_rls)
MOUSE_TBOX(  "5", 1207-OFFX,  771-OFFY, 30, 99,CURSOR_HAND,MOUSE_LR,mcb_p1_pt500_ap)
MOUSE_TBOX(  "6", 1236-OFFX,  771-OFFY, 37, 99,CURSOR_HAND,MOUSE_LR,mcb_p1_po1500_ruc)
MOUSE_TBOX(  "7", 1274-OFFX,  771-OFFY, 40, 99,CURSOR_HAND,MOUSE_LR,mcb_p1_po1500_ste)
MOUSE_TBOX(  "8", 1304-OFFX,  771-OFFY, 40, 99,CURSOR_HAND,MOUSE_LR,mcb_p1_po1500_rad)
MOUSE_TBOX(  "9", 1115-OFFX,  900-OFFY, 40, 99,CURSOR_HAND,MOUSE_LR,mcb_p1_sbros_davl)
MOUSE_TBOX( "10", 1155-OFFX,  900-OFFY, 40, 99,CURSOR_HAND,MOUSE_LR,mcb_p1_dubler_reg_davl)
MOUSE_TSVB( "11", 1195-OFFX,  900-OFFY, 40,100,CURSOR_HAND,CURSOR_HAND,MOUSE_LR,mcb_p1_sist_vkl_avar_top,mcb_p1_sist_vkl_avar_btm)
MOUSE_TBOX( "12", 1235-OFFX,  900-OFFY, 40, 99,CURSOR_HAND,MOUSE_LR,mcb_p1_zvuk_signal)
MOUSE_TSV3( "13", 1275-OFFX,  900-OFFY, 40,133,CURSOR_HAND,CURSOR_HAND,CURSOR_HAND,MOUSE_LR,mcb_p1_sist_vkl_norm_top,mcb_p1_sist_vkl_norm_cnt,mcb_p1_sist_vkl_norm_btm)
MOUSE_TS4B( "14", 1315-OFFX,  900-OFFY, 75,110,CURSOR_HAND,CURSOR_HAND,CURSOR_HAND,CURSOR_HAND,MOUSE_LR,mcb_p1_temp_salon_top,mcb_p1_temp_salon_blt,mcb_p1_temp_salon_brt,mcb_p1_temp_salon_cnt)
MOUSE_TS4B( "15", 1385-OFFX,  900-OFFY, 75,110,CURSOR_HAND,CURSOR_HAND,CURSOR_HAND,CURSOR_HAND,MOUSE_LR,mcb_p1_temp_turbo_top,mcb_p1_temp_turbo_blt,mcb_p1_temp_turbo_brt,mcb_p1_temp_turbo_cnt)
MOUSE_TS4B( "16", 1120-OFFX, 1000-OFFY, 68, 70,CURSOR_HAND,CURSOR_HAND,CURSOR_HAND,CURSOR_HAND,MOUSE_LR,mcb_p1_rash_auto,mcb_p1_rash_open,mcb_p1_rash_clse,mcb_p1_rash_cntr)
MOUSE_TBOX( "17", 1189-OFFX, 1000-OFFY, 33, 70,CURSOR_HAND,MOUSE_LR,mcb_p1_rash_rezh)
MOUSE_TBOX( "18", 1223-OFFX, 1000-OFFY, 46, 70,CURSOR_HAND,MOUSE_LR,mcb_p1_vozduh)
MOUSE_TBOX( "19", 1335-OFFX, 1100-OFFY, 40,100,CURSOR_HAND,MOUSE_LR,mcb_p1_trap_pitan)
MOUSE_TBOX( "20", 1375-OFFX, 1100-OFFY, 48,100,CURSOR_HAND,MOUSE_LR,mcb_p1_trap_motor)
MOUSE_TBOX( "21", 1430-OFFX, 1090-OFFY, 50,110,CURSOR_HAND,MOUSE_LR,mcb_p1_signs)
MOUSE_TBOX( "22", 1485-OFFX, 1100-OFFY, 70,100,CURSOR_HAND,MOUSE_LR,mcb_p1_red_light)
MOUSE_TBOX( "23", 1145-OFFX, 1085-OFFY, 33, 31,CURSOR_HAND,MOUSE_LR,mcb_p1_gmk_01)
MOUSE_TBOX( "24", 1275-OFFX, 1085-OFFY, 34, 31,CURSOR_HAND,MOUSE_LR,mcb_p1_gmk_02)
MOUSE_TSHB( "25", 1125-OFFX, 1167-OFFY, 60, 33,CURSOR_LEFTARROW,CURSOR_RIGHTARROW,MOUSE_DLR,mcb_p1_gmk_03_left,mcb_p1_gmk_03_right)
MOUSE_TSHB( "26", 1265-OFFX, 1167-OFFY, 60, 33,CURSOR_LEFTARROW,CURSOR_RIGHTARROW,MOUSE_DLR,mcb_p1_gmk_04_left,mcb_p1_gmk_04_right)
MOUSE_TBOX( "27", 1210-OFFX, 1085-OFFY, 34, 31,CURSOR_HAND,MOUSE_LR,mcb_p1_gmk_05)
MOUSE_TBOX( "28", 1385-OFFX,  874-OFFY, 32, 29,CURSOR_HAND,MOUSE_DLR,mcb_check_lamps)
MOUSE_TTPB( "29", 1110-OFFX,  721-OFFY, 50,37)    // LMP_STAIRS_DWN              
MOUSE_TTPB( "30", 1168-OFFX,  721-OFFY, 50,37)    // LMP_INV_115V_FAIL           
MOUSE_TTPB( "31", 1216-OFFX,  721-OFFY, 50,37)    // LMP_GROUND_ELEC_CONNECTED   
MOUSE_TTPB( "32", 1110-OFFX,  861-OFFY, 50,37)    // LMP_DIF_AIR_PRESS_DROP      
MOUSE_TTPB( "33", 1160-OFFX,  861-OFFY, 50,37)    // LMP_AIR_PRESSBCKCNTRUNITON  
MOUSE_TTPB( "34", 1199-OFFX,  861-OFFY, 50,37)    // LMP_AIR_PRESS_COND_SYS_OFF  
MOUSE_TTPB( "35", 1290-OFFX,  861-OFFY, 50,37)    // LMP_AIR_CONDSYSTURBOCOOLERON
MOUSE_TTPB( "36", 1178-OFFX, 1080-OFFY, 32,39)    // LMP_GMK_ZAP
MOUSE_TTPB( "37", 1244-OFFX, 1080-OFFY, 32,39)    // LMP_GMK_OSN
MOUSE_TTPB( "38", 1110-OFFX,  562-OFFY, 57,51)    // TBL_OUTER_MARKER                   		
MOUSE_TTPB( "39", 1110-OFFX,  621-OFFY, 57,51)    // TBL_MIDDLE_MARKER                  		
MOUSE_TTPB( "40", 1110-OFFX,  652-OFFY, 57,51)    // TBL_INNER_MARKER                   		
MOUSE_TSHB( "41", 1195-OFFX, 1135-OFFY, 60, 65,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR ,mcb_gmk_left,mcb_gmk_right) 
MOUSE_END

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p1_5_list; 
GAUGE_HEADER_FS700_EX(P1_BACKGROUND5_SX, "p1_5", &p1_5_list, rect_p1_5, 0, 0, 0, 0, p1_5); 

MY_ICON2	(p1_yoke				,MISC_YOKE					,NULL						,1365-OFFX,1040-OFFY,icb_yoke     ,1,p1_5)
MY_ICON2	(p1_crs_01				,MISC_CRS_P1_01				,&l_p1_yoke					,1479-OFFX,1012-OFFY,icb_crs_01	,1,p1_5)
MY_ICON2	(p1_lamp17				,P1_LMP_17_D_00				,&l_p1_crs_01				,1110-OFFX, 721-OFFY,icb_lamp17,3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(p1_lamp18				,P1_LMP_18_D_00				,&l_p1_lamp17				,1168-OFFX, 721-OFFY,icb_lamp18,3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(p1_lamp19				,P1_LMP_19_D_00				,&l_p1_lamp18				,1216-OFFX, 721-OFFY,icb_lamp19,3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(p1_lamp20				,P1_LMP_20_D_00				,&l_p1_lamp19				,1110-OFFX, 861-OFFY,icb_lamp20,3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(p1_lamp21				,P1_LMP_21_D_00				,&l_p1_lamp20				,1160-OFFX, 861-OFFY,icb_lamp21,3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(p1_lamp22				,P1_LMP_22_D_00				,&l_p1_lamp21				,1199-OFFX, 861-OFFY,icb_lamp22,3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(p1_lamp23				,P1_LMP_23_D_00				,&l_p1_lamp22				,1290-OFFX, 861-OFFY,icb_lamp23,3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(redlight				,P1_AZS_REDLIGHT_D_00		,&l_p1_lamp23				,1484-OFFX,1125-OFFY,icb_svet_red			, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(remni					,P1_AZS_REMNI_D_00			,&l_redlight				,1439-OFFX,1125-OFFY,icb_remni			, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(trap_motor				,P1_AZS_TRAPMOTOR_D_00		,&l_remni					,1380-OFFX,1125-OFFY,icb_trap_motor		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(trap_pitan				,P1_AZS_TRAPPOWER_D_00		,&l_trap_motor				,1336-OFFX,1125-OFFY,icb_trap_pitan		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(vozduh					,P1_AZS_AIR_D_00			,&l_trap_pitan				,1225-OFFX,1007-OFFY,icb_vozduh			, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(rashod_rezh			,P1_AZS_RASHODMODE_D_00		,&l_vozduh					,1187-OFFX,1007-OFFY,icb_rashod_rezh		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(rashod_avto			,P1_AZS_RASHODAUTO_D_00		,&l_rashod_rezh				,1110-OFFX,1007-OFFY,icb_rashod_avto		, 4*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(temp_turboholod		,P1_AZS_TEMPTURBOCOLD_D_00	,&l_rashod_avto				,1394-OFFX, 936-OFFY,icb_temp_turboholod	, 4*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(temp_salon				,P1_AZS_TEMPSALON_D_00		,&l_temp_turboholod			,1323-OFFX, 936-OFFY,icb_temp_salon		, 4*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(sist_vkl_norm			,P1_AZS_SYSONNORM_D_00		,&l_temp_salon				,1280-OFFX, 936-OFFY,icb_sist_vkl_norm	, 3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(zvuk_signal			,P1_AZS_SOUND_D_00			,&l_sist_vkl_norm			,1248-OFFX, 936-OFFY,icb_zvuk_signal		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(sist_vkl_avar			,P1_AZS_SYSONEMERG_D_00		,&l_zvuk_signal				,1205-OFFX, 936-OFFY,icb_sist_vkl_avar	, 3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(dubler_reg_davl		,P1_AZS_TUNEPSIAUXL_D_00	,&l_sist_vkl_avar			,1164-OFFX, 936-OFFY,icb_dubler_reg_davl	, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(sbros_davl				,P1_AZS_PSIRESET_D_00		,&l_dubler_reg_davl			,1110-OFFX, 936-OFFY,icb_sbros_davl		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(po_radio				,P1_AZS_PORADIO_D_00		,&l_sbros_davl				,1320-OFFX, 793-OFFY,icb_po_radio			, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(po_steklo				,P1_AZS_POGLASS_D_00		,&l_po_radio				,1290-OFFX, 793-OFFY,icb_po_steklo		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(radio_ruchn			,P1_AZS_RADIOMANUAL_D_00	,&l_po_steklo				,1242-OFFX, 793-OFFY,icb_radio_ruchn		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(pt500_ap				,P1_AZS_PT500AP_D_00		,&l_radio_ruchn				,1210-OFFX, 793-OFFY,icb_pt500_ap			, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(pt500_rls				,P1_AZS_PT500RLS_D_00		,&l_pt500_ap				,1170-OFFX, 793-OFFY,icb_pt500_rls		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(pwrsrc					,P1_AZS_POWERSOURCE_D_00	,&l_pt500_rls				,1110-OFFX, 793-OFFY,icb_pwrsrc			, 3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(tablo_p1_gmk_06		,P1_TBL_GMK06_D_00			,&l_pwrsrc					,1143-OFFX,1155-OFFY,icb_tablo_06		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(tablo_p1_gmk_05		,P1_TBL_GMK05_D_00			,&l_tablo_p1_gmk_06			,1143-OFFX,1117-OFFY,icb_tablo_05		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(tablo_p1_gmk_04		,P1_TBL_GMK04_D_00	    	,&l_tablo_p1_gmk_05			,1272-OFFX,1155-OFFY,icb_tablo_04		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(tablo_p1_gmk_03		,P1_TBL_GMK03_D_00	    	,&l_tablo_p1_gmk_04			,1272-OFFX,1117-OFFY,icb_tablo_03		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(tablo_p1_gmk_02		,P1_TBL_GMK02_D_00	    	,&l_tablo_p1_gmk_03			,1250-OFFX,1109-OFFY,icb_tablo_02		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(tablo_p1_gmk_01		,P1_TBL_GMK01_D_00	    	,&l_tablo_p1_gmk_02			,1185-OFFX,1109-OFFY,icb_tablo_01		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(lamp_p1_gmk_osn		,P1_LMP_GMKMAIN_D_00    	,&l_tablo_p1_gmk_01			,1244-OFFX,1080-OFFY,icb_lamp_osn		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(lamp_p1_gmk_zap		,P1_LMP_GMKAUXL_D_00    	,&l_lamp_p1_gmk_osn			,1178-OFFX,1080-OFFY,icb_lamp_zap		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(knob_p1_gmk_scale      ,P1_KNB_GMK_D_00	    	,&l_lamp_p1_gmk_zap			,1199-OFFX,1139-OFFY,icb_gmkknob			, 90*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(switch_p1_gmk_05	    ,P1_SWT_GMK_05_D_01	    	,&l_knob_p1_gmk_scale		,1210-OFFX,1085-OFFY,icb_switch_05		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(switch_p1_gmk_04	    ,P1_SWT_GMK_04_D_00	    	,&l_switch_p1_gmk_05	    ,1275-OFFX,1167-OFFY,icb_switch_04		, 3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(switch_p1_gmk_03	    ,P1_SWT_GMK_03_D_00	    	,&l_switch_p1_gmk_04	    ,1145-OFFX,1167-OFFY,icb_switch_03		, 3*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(switch_p1_gmk_02	    ,P1_SWT_GMK_02_D_01	    	,&l_switch_p1_gmk_03	    ,1275-OFFX,1085-OFFY,icb_switch_02		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(switch_p1_gmk_01	    ,P1_SWT_GMK_01_D_01	    	,&l_switch_p1_gmk_02	    ,1145-OFFX,1085-OFFY,icb_switch_01		, 2*PANEL_LIGHT_MAX,p1_5)
MY_ICON2	(cov_p1_gmk_scale		,P1_COV_GMKSCALE_D	    	,&l_switch_p1_gmk_01	    ,1189-OFFX,1123-OFFY,icb_Ico  			, 1*PANEL_LIGHT_MAX,p1_5)
#if defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL) && defined(HAVE_PLF_PANEL) && defined(HAVE_RED_PANEL)
MY_NEEDLE2	(ndl_p1_gmk_scale_onD	,P1_NDL_GMKSCALEON_D		,&l_cov_p1_gmk_scale		,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onD	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_onM	,P1_NDL_GMKSCALEON_M		,&l_ndl_p1_gmk_scale_onD	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onM	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_onP	,P1_NDL_GMKSCALEON_P		,&l_ndl_p1_gmk_scale_onM	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onP	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_onR	,P1_NDL_GMKSCALEON_R		,&l_ndl_p1_gmk_scale_onP	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onR	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offD	,P1_NDL_GMKSCALEOFF_D		,&l_ndl_p1_gmk_scale_onR	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offD	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offM	,P1_NDL_GMKSCALEOFF_M		,&l_ndl_p1_gmk_scale_offD	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offM	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offP	,P1_NDL_GMKSCALEOFF_P		,&l_ndl_p1_gmk_scale_offM	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offP	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offR	,P1_NDL_GMKSCALEOFF_R		,&l_ndl_p1_gmk_scale_offP	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offR	, 0, 18, p1_5)
MY_ICON2	(p1_5Ico				,P1_BACKGROUND5_D			,&l_ndl_p1_gmk_scale_offR	,	     0,		   0,	icb_Ico			,PANEL_LIGHT_MAX				, p1_5) 
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_PLF_PANEL)
MY_NEEDLE2	(ndl_p1_gmk_scale_onD	,P1_NDL_GMKSCALEON_D		,&l_cov_p1_gmk_scale		,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onD	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_onP	,P1_NDL_GMKSCALEON_P		,&l_ndl_p1_gmk_scale_onD	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onP	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offD	,P1_NDL_GMKSCALEOFF_D		,&l_ndl_p1_gmk_scale_onP	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offD	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offP	,P1_NDL_GMKSCALEOFF_P		,&l_ndl_p1_gmk_scale_offD	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offP	, 0, 18, p1_5)
MY_ICON2	(p1_5Ico				,P1_BACKGROUND5_D			,&l_ndl_p1_gmk_scale_offP	,	     0,		   0,	icb_Ico			,PANEL_LIGHT_MAX				, p1_5) 
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_RED_PANEL)
MY_NEEDLE2	(ndl_p1_gmk_scale_onD	,P1_NDL_GMKSCALEON_D		,&l_cov_p1_gmk_scale		,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onD	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_onP	,P1_NDL_GMKSCALEON_R		,&l_ndl_p1_gmk_scale_onD	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onR	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offD	,P1_NDL_GMKSCALEOFF_D		,&l_ndl_p1_gmk_scale_onP	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offD	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offP	,P1_NDL_GMKSCALEOFF_R		,&l_ndl_p1_gmk_scale_offD	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offR	, 0, 18, p1_5)
MY_ICON2	(p1_5Ico				,P1_BACKGROUND5_D			,&l_ndl_p1_gmk_scale_offP	,	     0,		   0,	icb_Ico			,PANEL_LIGHT_MAX				, p1_5) 
#elif defined(HAVE_DAY_PANEL) && defined(HAVE_MIX_PANEL)
MY_NEEDLE2	(ndl_p1_gmk_scale_onD	,P1_NDL_GMKSCALEON_D		,&l_cov_p1_gmk_scale		,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onD	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_onP	,P1_NDL_GMKSCALEON_M		,&l_ndl_p1_gmk_scale_onD	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_onM	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offD	,P1_NDL_GMKSCALEOFF_D		,&l_ndl_p1_gmk_scale_onP	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offD	, 0, 18, p1_5)
MY_NEEDLE2	(ndl_p1_gmk_scale_offP	,P1_NDL_GMKSCALEOFF_M		,&l_ndl_p1_gmk_scale_offD	,1226-OFFX,1163-OFFY, 38, 40,	icb_gmk_scale_offM	, 0, 18, p1_5)
MY_ICON2	(p1_5Ico				,P1_BACKGROUND5_D			,&l_ndl_p1_gmk_scale_offP	,	     0,		   0,	icb_Ico			,PANEL_LIGHT_MAX				, p1_5) 
#endif

MY_STATIC2	(p1_5bg,p1_5_list		,P1_BACKGROUND5				,&l_p1_5Ico					, p1_5);

#endif