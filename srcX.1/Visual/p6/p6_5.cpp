/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P6.h"

#define OFFX	267
#define OFFY	521

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_ICB(icb_kmplfreq		,SHOW_HND(HND_KMP1FREQ		,20))
MAKE_ICB(icb_kmplherz		,SHOW_BTN(BTN_KMP1ID  		,2))
static MAKE_ICB(icb_obsleft 		,SHOW_HND(HND_OBS6			,20))
MAKE_ICB(icb_kmp1vorl  		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP1VOR		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmp1pwrl  		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP1PWR		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst1l  		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST1		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst2l 		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST2		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst3l  		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST3		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmp1vor  		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP1VOR		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmp1pwr  		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_AZS(AZS_KMP1PWR		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst1  		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST1		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst2  		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST2		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmptst3  		,if(!PWR_GET(PWR_KURSMP2)){SHOW_IMAGE(pelement); SHOW_BTN(BTN_KMP1TEST3		,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl1scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ1	,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl2scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ2	,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl3scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ3	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl4scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ4	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl5scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ5	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl1scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS61     	,4)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl2scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS62     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl3scll		,if( PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS63     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl1scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ1	,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl2scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ2	,2)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl3scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ3	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl4scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ4	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_kmpl5scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_NAV6_FREQ5	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl1scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS61     	,4)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl2scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS62     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_obsl3scl		,if(!PWR_GET(PWR_KURSMP1)){SHOW_IMAGE(pelement); SHOW_POS(POS_OBS63     	,10)} else {HIDE_IMAGE(pelement); return -1;})
MAKE_ICB(icb_bkgl			,if(!PWR_GET(PWR_KURSMP1)){ HIDE_IMAGE(pelement); return -1;} else {SHOW_IMAGE(pelement); return 1;}	)

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_azs_kmp1vor		    ,AZS_TGL(AZS_KMP1VOR    				))
static MAKE_MSCB(mcb_azs_kmp1pwr		    ,AZS_TGL(AZS_KMP1PWR    				))

static MAKE_MSCB(mcb_btn_kmp1test1		    ,PRS_BTN(BTN_KMP1TEST1  				))
static MAKE_MSCB(mcb_btn_kmp1test2		    ,PRS_BTN(BTN_KMP1TEST2  				))
static MAKE_MSCB(mcb_btn_kmp1test3		    ,PRS_BTN(BTN_KMP1TEST3  				))

// ���������
BOOL FSAPI mcb_glt_kmpfreq_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV6_FREQ1);
	int f2=(int)POS_GET(POS_NAV6_FREQ2);
	int f3=(int)POS_GET(POS_NAV6_FREQ3);
	int f4=(int)POS_GET(POS_NAV6_FREQ4);
	int f5=(int)POS_GET(POS_NAV6_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(f5==5) {
			f5=0;
		} else {
			f4--;
			if(f4<0)
				f4=9;
			f5=5;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);

		return TRUE;
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {

		HND_DEC(HND_KMP1FREQ);

		if(f1==1&&f2==0&&f3==8) {
			f1=1;
			f2=1;
			f3=7;
		} else {
			// NAV3
			f3--;
			if(f3<0) {
				f3+=9;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);
	}
	return TRUE;
}

BOOL FSAPI mcb_glt_kmpfreq_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV6_FREQ1);
	int f2=(int)POS_GET(POS_NAV6_FREQ2);
	int f3=(int)POS_GET(POS_NAV6_FREQ3);
	int f4=(int)POS_GET(POS_NAV6_FREQ4);
	int f5=(int)POS_GET(POS_NAV6_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		if(f5==5) {
			f4++;
			if(f4>9)
				f4=0;
			f5=0;
		} else {
			f5=5;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);

		return TRUE;
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {

		HND_INC(HND_KMP1FREQ);

		if(f1==1&&f2==1&&f3==7) {
			f1=1;
			f2=0;
			f3=8;
		} else {
			// NAV3
			f3++;
			if(f3>9) {
				f3=0;
				// NAV2
				if(f3==8||f3==9) {
					f2=0;
				} else{
					f2=1;
				}
			}
			f1=1;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);
	}
	return TRUE;
}

BOOL FSAPI mcb_glt_kmpfreqr_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV6_FREQ1);
	int f2=(int)POS_GET(POS_NAV6_FREQ2);
	int f3=(int)POS_GET(POS_NAV6_FREQ3);
	int f4=(int)POS_GET(POS_NAV6_FREQ4);
	int f5=(int)POS_GET(POS_NAV6_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE||mouse_flags&MOUSE_LEFTSINGLE) {
		if(f5==5) {
			f5=0;
		} else {
			f4--;
			if(f4<0)
				f4=9;
			f5=5;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);
	}
	return TRUE;
}

BOOL FSAPI mcb_glt_kmpfreqr_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int f1=(int)POS_GET(POS_NAV6_FREQ1);
	int f2=(int)POS_GET(POS_NAV6_FREQ2);
	int f3=(int)POS_GET(POS_NAV6_FREQ3);
	int f4=(int)POS_GET(POS_NAV6_FREQ4);
	int f5=(int)POS_GET(POS_NAV6_FREQ5);

	if(mouse_flags&MOUSE_RIGHTSINGLE||mouse_flags&MOUSE_LEFTSINGLE) {
		if(f5==5) {
			f4++;
			if(f4>9)
				f4=0;
			f5=0;
		} else {
			f5=5;
		}

		POS_SET(POS_NAV6_FREQ1,f1);
		POS_SET(POS_NAV6_FREQ2,f2);
		POS_SET(POS_NAV6_FREQ3,f3);
		POS_SET(POS_NAV6_FREQ4,f4);
		POS_SET(POS_NAV6_FREQ5,f5);

		return TRUE;
	}
	return TRUE;
}

BOOL FSAPI mcb_btn_kmp1id(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	BTN_TGL(BTN_KMP1ID);
	return TRUE;
}

BOOL FSAPI mcb_glt_obs_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int o1=(int)POS_GET(POS_OBS61);
	int o2=(int)POS_GET(POS_OBS62);
	int o3=(int)POS_GET(POS_OBS63);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		for(int i=0;i<10;i++) {
			HND_DEC(HND_OBS6);

			if(!CFG_GET(CFG_OBS_ZERO_START)) {
				if(o3==1&&o2==0&&o1==0) {
					o1=3;
					o2=6;
					o3=0;
				} else {
					// OBS3
					o3--;
					if(o3<0) {
						o3=9;
						// OBS2
						o2--;
						if(o2<0) {
							o2=9;
							// OBS1
							o1--;
							if(o1<0) {
								o1=3;
							}
						}
					}
				}
			} else {
				if(o3==0&&o2==0&&o1==0) {
					o1=3;
					o2=5;
					o3=9;
				} else {
					// OBS3
					o3--;
					if(o3<0) {
						o3=9;
						// OBS2
						o2--;
						if(o2<0) {
							o2=9;
							// OBS1
							o1--;
							if(o1<0) {
								o1=3;
							}
						}
					}
				}
			}
		}
	} else if(mouse_flags&MOUSE_LEFTSINGLE) {

		HND_DEC(HND_OBS6);

		if(!CFG_GET(CFG_OBS_ZERO_START)) {
			if(o3==1&&o2==0&&o1==0) {
				o1=3;
				o2=6;
				o3=0;
			} else {
				// OBS3
				o3--;
				if(o3<0) {
					o3=9;
					// OBS2
					o2--;
					if(o2<0) {
						o2=9;
						// OBS1
						o1--;
						if(o1<0) {
							o1=3;
						}
					}
				}
			}
		} else {
			if(o3==0&&o2==0&&o1==0) {
				o1=3;
				o2=5;
				o3=9;
			} else {
				// OBS3
				o3--;
				if(o3<0) {
					o3=9;
					// OBS2
					o2--;
					if(o2<0) {
						o2=9;
						// OBS1
						o1--;
						if(o1<0) {
							o1=3;
						}
					}
				}
			}
		}
	}
	POS_SET(POS_OBS61,o1);
	POS_SET(POS_OBS62,o2);
	POS_SET(POS_OBS63,o3);

	return TRUE;
}

BOOL FSAPI mcb_glt_obs_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	int o1=(int)POS_GET(POS_OBS61);
	int o2=(int)POS_GET(POS_OBS62);
	int o3=(int)POS_GET(POS_OBS63);

	if(mouse_flags&MOUSE_RIGHTSINGLE) {
		for(int i=0;i<10;i++) {
			HND_INC(HND_OBS6);

			if(!CFG_GET(CFG_OBS_ZERO_START)) {
				if(o3==0&&o2==6&&o1==3) {
					o1=0;
					o2=0;
					o3=1;
				} else {
					// OBS3
					o3++;
					if(o3>9) {
						o3=0;
						// OBS2
						o2++;
						if(o2>9) {
							o2=0;
							// OBS1
							o1++;
							if(o1>3) {
								o1=0;
							}
						}
					}
				}
			} else {
				if(o3==9&&o2==5&&o1==3) {
					o1=0;
					o2=0;
					o3=0;
				} else {
					// OBS3
					o3++;
					if(o3>9) {
						o3=0;
						// OBS2
						o2++;
						if(o2>9) {
							o2=0;
							// OBS1
							o1++;
							if(o1>3) {
								o1=0;
							}
						}
					}
				}
			}
		}
	} else if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {

		HND_INC(HND_OBS6);

		if(!CFG_GET(CFG_OBS_ZERO_START)) {
			if(o3==0&&o2==6&&o1==3) {
				o1=0;
				o2=0;
				o3=1;
			} else {
				// OBS3
				o3++;
				if(o3>9) {
					o3=0;
					// OBS2
					o2++;
					if(o2>9) {
						o2=0;
						// OBS1
						o1++;
						if(o1>3) {
							o1=0;
						}
					}
				}
			}
		} else {
			if(o3==9&&o2==5&&o1==3) {
				o1=0;
				o2=0;
				o3=0;
			} else {
				// OBS3
				o3++;
				if(o3>9) {
					o3=0;
					// OBS2
					o2++;
					if(o2>9) {
						o2=0;
						// OBS1
						o1++;
						if(o1>3) {
							o1=0;
						}
					}
				}
			}
		}
	}
	POS_SET(POS_OBS61,o1);
	POS_SET(POS_OBS62,o2);
	POS_SET(POS_OBS63,o3);

	return TRUE;
}

BOOL FSAPI mcb_obs1_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P24);
	return true;
}

BOOL FSAPI mcb_kmp1_big(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	panel_window_toggle(IDENT_P24);
	return true;
}

static MAKE_TCB(tcb_01	,AZS_TT(AZS_KMP1VOR				))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_KMP1PWR				))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_KMP1TEST1	        ))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_KMP1TEST2	        ))
static MAKE_TCB(tcb_05	,BTN_TT(BTN_KMP1TEST3	        ))
static MAKE_TCB(tcb_06	,BTN_TT(BTN_KMP1ID				))
static MAKE_TCB(tcb_07	,HND_TT(HND_OBS6			    ))
static MAKE_TCB(tcb_08	,HND_TT(HND_KMP1FREQ	        ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MAKE_TTA(tcb_02) 
MAKE_TTA(tcb_03)
MAKE_TTA(tcb_04)
MAKE_TTA(tcb_05)
MAKE_TTA(tcb_06)
MAKE_TTA(tcb_07) 
MAKE_TTA(tcb_08)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p6_5,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P6_BACKGROUND5_D_SX,P6_BACKGROUND5_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_PBOX(60,90,370,160,CURSOR_HAND,MOUSE_LR,mcb_obs1_big)
MOUSE_PBOX(440,70,420,160,CURSOR_HAND,MOUSE_LR,mcb_kmp1_big)

MOUSE_TBOX(  "1", 772-OFFX,  593-OFFY, 41, 57,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp1vor)
MOUSE_TBOX(  "2",1008-OFFX,  575-OFFY, 40, 60,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp1pwr)
MOUSE_TBOX(  "3", 834-OFFX,  684-OFFY, 49, 50,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1test1)
MOUSE_TBOX(  "4", 897-OFFX,  681-OFFY, 49, 50,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1test2)
MOUSE_TBOX(  "5", 959-OFFX,  676-OFFY, 49, 50,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1test3)
MOUSE_TSHB(  "8",1005-OFFX,  625-OFFY,120, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreqr_l,mcb_glt_kmpfreqr_r)
MOUSE_TBOX(  "6",1032-OFFX,  625-OFFY, 35, 90,CURSOR_HAND,MOUSE_LR ,mcb_btn_kmp1id)
MOUSE_TSHB(  "7", 575-OFFX,  652-OFFY, 90, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_obs_l,mcb_glt_obs_r)
MOUSE_TSHB(  "8", 746-OFFX,  640-OFFY, 90, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreq_l,mcb_glt_kmpfreq_r)

MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p6_5_list; 
GAUGE_HEADER_FS700_EX(P6_BACKGROUND5_D_SX, "p6_5", &p6_5_list, rect_p6_5, 0, 0, 0, 0, p6_5); 

MY_ICON2	(p6_knb_kmplf		, P6_KNB_KURSMPLEFTFREQ_D_00	,NULL            		, 746-OFFX,  640-OFFY,icb_kmplfreq  	, 20*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_knb_kmplh		, P6_BTN_KURSMPLEFTID_D_00		,&l_p6_knb_kmplf		,1009-OFFX,  611-OFFY,icb_kmplherz  	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_knb_obsl		, P6_KNB_OBSLEFT_D_00			,&l_p6_knb_kmplh		, 575-OFFX,  652-OFFY,icb_obsleft		, 20*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_azs_kmp1vor		, P6_AZS_KURSMP1VOR_D_00		,&l_p6_knb_obsl			, 772-OFFX,  593-OFFY,icb_kmp1vor    	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_azs_kmp1vorl	, P6_AZS_KURSMP1VORLIT_D_00		,&l_p6_azs_kmp1vor		, 772-OFFX,  593-OFFY,icb_kmp1vorl    	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_azs_kmp1pwr		, P6_AZS_KURSMP1PWR_D_00		,&l_p6_azs_kmp1vorl		,1008-OFFX,  575-OFFY,icb_kmp1pwr    	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_azs_kmp1pwrl	, P6_AZS_KURSMP1PWRLIT_D_00		,&l_p6_azs_kmp1pwr		,1008-OFFX,  575-OFFY,icb_kmp1pwrl    	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_btn_kmp1test1	, P6_BTN_KURSMP1TEST1_D_00		,&l_p6_azs_kmp1pwrl		, 834-OFFX,  684-OFFY,icb_kmptst1  		, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_btn_kmp1test1l	, P6_BTN_KURSMP1TEST1LIT_D_00	,&l_p6_btn_kmp1test1	, 834-OFFX,  684-OFFY,icb_kmptst1l  	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_btn_kmp1test2	, P6_BTN_KURSMP1TEST2_D_00		,&l_p6_btn_kmp1test1l	, 897-OFFX,  681-OFFY,icb_kmptst2  		, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_btn_kmp1test2l	, P6_BTN_KURSMP1TEST2LIT_D_00	,&l_p6_btn_kmp1test2	, 897-OFFX,  681-OFFY,icb_kmptst2l 		, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_btn_kmp1test3	, P6_BTN_KURSMP1TEST3_D_00		,&l_p6_btn_kmp1test2l	, 959-OFFX,  676-OFFY,icb_kmptst3  		, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_btn_kmp1test3l	, P6_BTN_KURSMP1TEST3LIT_D_00	,&l_p6_btn_kmp1test3	, 959-OFFX,  676-OFFY,icb_kmptst3l  	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl1		, P6_SCL_KURSMPLEFT1_D_00		,&l_p6_btn_kmp1test3l	, 848-OFFX,  588-OFFY,icb_kmpl1scl  	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl1l		, P6_SCL_KURSMPLEFT1LIT_D_00	,&l_p6_scl_kmpl1		, 848-OFFX,  588-OFFY,icb_kmpl1scll 	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl2		, P6_SCL_KURSMPLEFT2_D_00		,&l_p6_scl_kmpl1l		, 875-OFFX,  588-OFFY,icb_kmpl2scl  	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl2l		, P6_SCL_KURSMPLEFT2LIT_D_00	,&l_p6_scl_kmpl2		, 875-OFFX,  588-OFFY,icb_kmpl2scll 	, 2 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl3		, P6_SCL_KURSMPLEFT3_D_00		,&l_p6_scl_kmpl2l		, 896-OFFX,  588-OFFY,icb_kmpl3scl  	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl3l		, P6_SCL_KURSMPLEFT3LIT_D_00	,&l_p6_scl_kmpl3		, 896-OFFX,  588-OFFY,icb_kmpl3scll 	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl4		, P6_SCL_KURSMPLEFT4_D_00		,&l_p6_scl_kmpl3l		, 926-OFFX,  588-OFFY,icb_kmpl4scl  	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl4l		, P6_SCL_KURSMPLEFT4LIT_D_00	,&l_p6_scl_kmpl4		, 926-OFFX,  588-OFFY,icb_kmpl4scll 	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl5		, P6_SCL_KURSMPLEFT5_D_00		,&l_p6_scl_kmpl4l		, 946-OFFX,  588-OFFY,icb_kmpl5scl  	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_kmpl5l		, P6_SCL_KURSMPLEFT5LIT_D_00	,&l_p6_scl_kmpl5		, 946-OFFX,  588-OFFY,icb_kmpl5scll 	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_obsl1		, P6_SCL_OBSLEFT1_D_00			,&l_p6_scl_kmpl5l		, 442-OFFX,  622-OFFY,icb_obsl1scl  	, 4 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_obsl1l		, P6_SCL_OBSLEFT1LIT_D_00		,&l_p6_scl_obsl1		, 442-OFFX,  622-OFFY,icb_obsl1scll 	, 4 *PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_obsl2		, P6_SCL_OBSLEFT2_D_00			,&l_p6_scl_obsl1l		, 492-OFFX,  622-OFFY,icb_obsl2scl  	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_obsl2l		, P6_SCL_OBSLEFT2LIT_D_00		,&l_p6_scl_obsl2		, 492-OFFX,  622-OFFY,icb_obsl2scll 	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_obsl3		, P6_SCL_OBSLEFT3_D_00			,&l_p6_scl_obsl2l		, 529-OFFX,  622-OFFY,icb_obsl3scl  	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_scl_obsl3l		, P6_SCL_OBSLEFT3LIT_D_00		,&l_p6_scl_obsl3		, 529-OFFX,  622-OFFY,icb_obsl3scll 	, 10*PANEL_LIGHT_MAX,p6_5)	
MY_ICON2	(p6_5Ico			, P6_BACKGROUND5_D				,&l_p6_scl_obsl3l		,        0,        0,icb_Ico			,    PANEL_LIGHT_MAX,p6_5)
MY_STATIC2	(p6_5bg,p6_5_list	, P6_BACKGROUND5_D				,&l_p6_5Ico				,p6_5)


#endif