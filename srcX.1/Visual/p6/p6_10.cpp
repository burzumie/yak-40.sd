/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P6.h"

static double FSAPI icb_obsleft(PELEMENT_ICON pelement)
{
	CHK_BRT();
	double val=HND_GET(HND_OBS6);
	if(val>9)
		val=0;
	return val+POS_GET(POS_PANEL_STATE)*10; 
}

double FSAPI icb_obsl1scll(PELEMENT_ICON pelement);
double FSAPI icb_obsl2scll(PELEMENT_ICON pelement);
double FSAPI icb_obsl3scll(PELEMENT_ICON pelement);
double FSAPI icb_obsl1scl(PELEMENT_ICON pelement);
double FSAPI icb_obsl2scl(PELEMENT_ICON pelement);
double FSAPI icb_obsl3scl(PELEMENT_ICON pelement);

double FSAPI icb_Ico(PELEMENT_ICON pelement);

double FSAPI icb_IcoLit(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return PWR_GET(PWR_KURSMP1)&&!POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):-1;
}

double FSAPI icb_IcoIntl(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return !PWR_GET(PWR_KURSMP1)&&POS_GET(POS_PANEL_LANG)?POS_GET(POS_PANEL_STATE):-1;
}

double FSAPI icb_IcoIntlLit(PELEMENT_ICON pelement)
{
	CHK_BRT();
	return POS_GET(POS_PANEL_LANG)&&PWR_GET(PWR_KURSMP1)?POS_GET(POS_PANEL_STATE):-1;
}

BOOL FSAPI mcb_obs1_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_obs_l(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_obs_r(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01	,HND_TT(HND_OBS6			    ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p6_10,HELP_NONE,0,0)
MOUSE_PBOX(30,30,P20_BCK_MPOBS1_D_SX,P20_BCK_MPOBS1_D_SY,CURSOR_HAND,MOUSE_LR,mcb_obs1_big)
MOUSE_TSHB(  "1", 252,  75, 90, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_obs_l,mcb_glt_obs_r)
MOUSE_END       

extern PELEMENT_HEADER p6_10_list; 
GAUGE_HEADER_FS700_EX(P20_BCK_MPOBS1_D_SX, "p6_10", &p6_10_list, rect_p6_10, 0, 0, 0, 0, p6_10); 

MY_ICON2	(p6_10Knb          	, P20_KNB_OBS_D_00			,NULL            	,      257,       89,icb_obsleft 		,10* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl3Lit      	, P20_SCL_OBS3LIT_D_00		,&l_p6_10Knb        ,      211,       35,icb_obsl3scll		,10* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl2Lit      	, P20_SCL_OBS2LIT_D_00		,&l_p6_10Scl3Lit    ,      171,       35,icb_obsl2scll		,10* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl1Lit      	, P20_SCL_OBS1LIT_D_00		,&l_p6_10Scl2Lit    ,      133,       35,icb_obsl1scll		, 4* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl3      	, P20_SCL_OBS3_D_00			,&l_p6_10Scl1Lit    ,      211,       35,icb_obsl3scl		,10* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl2      	, P20_SCL_OBS2_D_00			,&l_p6_10Scl3      	,      171,       35,icb_obsl2scl		,10* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Scl1      	, P20_SCL_OBS1_D_00			,&l_p6_10Scl2      	,      133,       35,icb_obsl1scl		, 4* PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10IcoIntlLit	, P20_BCK_MPOBS1INTLLIT_D	,&l_p6_10Scl1      	,        0,        0,icb_IcoIntlLit		,    PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10IcoIntl		, P20_BCK_MPOBS1INTL_D		,&l_p6_10IcoIntlLit	,        0,        0,icb_IcoIntl		,    PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10IcoLit		, P20_BCK_MPOBS1LIT_D		,&l_p6_10IcoIntl	,        0,        0,icb_IcoLit			,    PANEL_LIGHT_MAX,p6_10)
MY_ICON2	(p6_10Ico			, P20_BCK_MPOBS1_D			,&l_p6_10IcoLit		,        0,        0,icb_Ico			,    PANEL_LIGHT_MAX,p6_10)
MY_STATIC2	(p6_10bg,p6_10_list	, P20_BCK_MPOBS1_D			,&l_p6_10Ico		,p6_10)
