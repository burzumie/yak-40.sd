/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P6.h"

#define OFFX	267
#define OFFY	0

static MAKE_ICB(icb_so72control		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return BTN_GET(BTN_SO72CONTROL	)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72controll	,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return BTN_GET(BTN_SO72CONTROL	)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72id			,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return BTN_GET(BTN_SO72ID		)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72idl			,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return BTN_GET(BTN_SO72ID		)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72pwr			,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return AZS_GET(AZS_SO72PWR		)+POS_GET(POS_PANEL_STATE)*2;});
static MAKE_ICB(icb_so72pwrl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return AZS_GET(AZS_SO72PWR		)+POS_GET(POS_PANEL_STATE)*2;});

static MAKE_ICB(icb_xpdr1     		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return HND_GET(HND_XPDR1		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr1lit  		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_XPDR1		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr2     		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return HND_GET(HND_XPDR2		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr2lit  		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_XPDR2		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr3     		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return HND_GET(HND_XPDR3		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr3lit  		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_XPDR3		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr4     		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return HND_GET(HND_XPDR4		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr4lit  		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return HND_GET(HND_XPDR4		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr1scl  		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return POS_GET(POS_XPDR1		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr1litscl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_XPDR1		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr2scl  		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return POS_GET(POS_XPDR2		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr2litscl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_XPDR2		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr3scl  		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return POS_GET(POS_XPDR3		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr3litscl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_XPDR3		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr4scl  		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return POS_GET(POS_XPDR4		)+POS_GET(POS_PANEL_STATE)*10;})
static MAKE_ICB(icb_xpdr4litscl		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT(); return POS_GET(POS_XPDR4		)+POS_GET(POS_PANEL_STATE)*10;})

static MAKE_ICB(icb_xpdrmayday 		,CHK_BRT();				return AZS_GET(AZS_XPDRMAYDAY 	)+POS_GET(POS_PANEL_STATE)*3;)
static MAKE_ICB(icb_xpdrm      		,if( PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return GLT_GET(GLT_XPDRMODE   	)+POS_GET(POS_PANEL_STATE)*4;})
static MAKE_ICB(icb_xpdrml     		,if(!PWR_GET(PWR_SO72))	{HIDE_IMAGE(pelement); return -1;}else{SHOW_IMAGE(pelement); CHK_BRT();	return GLT_GET(GLT_XPDRMODE   	)+POS_GET(POS_PANEL_STATE)*4;})

static MAKE_ICB(icb_startai9pwr		,SHOW_AZS(AZS_STARTAI9PWR	,2));
static MAKE_ICB(icb_startai9mode	,SHOW_AZS(AZS_STARTAI9MODE	,4));
static MAKE_ICB(icb_startai9   		,SHOW_BTN(BTN_STARTAI9		,2));
static MAKE_ICB(icb_stopai9   		,SHOW_BTN(BTN_STOPAI9		,2));
static MAKE_ICB(icb_startai25pwr	,SHOW_AZS(AZS_STARTAI25PWR	,2));
static MAKE_ICB(icb_startai25mode	,SHOW_AZS(AZS_STARTAI25MODE	,4));
static MAKE_ICB(icb_startai25sel	,SHOW_AZS(AZS_STARTAI25SEL	,4));
static MAKE_ICB(icb_startai25   	,SHOW_BTN(BTN_STARTAI25		,2));
static MAKE_ICB(icb_stopai25  		,SHOW_BTN(BTN_STOPAI25		,2));
static MAKE_ICB(icb_sgu        		,SHOW_GLT(GLT_SGU6        	,5));

//////////////////////////////////////////////////////////////////////////

static MAKE_MSCB(mcb_azs_so72pwr		    ,AZS_TGL(AZS_SO72PWR    				))
static MAKE_MSCB(mcb_azs_so72emerg		    ,if(mouse_flags&MOUSE_RIGHTSINGLE){if(AZS_GET(AZS_XPDRMAYDAY)==0)AZS_SET(AZS_XPDRMAYDAY,1); else AZS_SET(AZS_XPDRMAYDAY,0);} else {if(AZS_GET(AZS_XPDRMAYDAY)==1)AZS_SET(AZS_XPDRMAYDAY,2); else if(AZS_GET(AZS_XPDRMAYDAY)==2)AZS_SET(AZS_XPDRMAYDAY,1);} )
static MAKE_MSCB(mcb_azs_startai9pwr	    ,AZS_TGL(AZS_STARTAI9PWR				))
static MAKE_MSCB(mcb_azs_startai9mode_cs	,AZS_SET(AZS_STARTAI9MODE				,3))
static MAKE_MSCB(mcb_azs_startai9mode_start	,AZS_SET(AZS_STARTAI9MODE				,1))
static MAKE_MSCB(mcb_azs_startai9mode_stop	,AZS_SET(AZS_STARTAI9MODE				,2))
static MAKE_MSCB(mcb_azs_startai9mode_cntr	,AZS_SET(AZS_STARTAI9MODE				,0))
static MAKE_MSCB(mcb_azs_startai25pwr	    ,AZS_TGL(AZS_STARTAI25PWR				))
static MAKE_MSCB(mcb_azs_startai25mode_cs	,AZS_SET(AZS_STARTAI25MODE				,1))
static MAKE_MSCB(mcb_azs_startai25mode_start,AZS_SET(AZS_STARTAI25MODE				,2))
static MAKE_MSCB(mcb_azs_startai25mode_stop	,AZS_SET(AZS_STARTAI25MODE				,3))
static MAKE_MSCB(mcb_azs_startai25mode_cntr	,AZS_SET(AZS_STARTAI25MODE				,0))
static MAKE_MSCB(mcb_azs_startai25sel1		,AZS_SET(AZS_STARTAI25SEL				,2))
static MAKE_MSCB(mcb_azs_startai25sel2		,AZS_SET(AZS_STARTAI25SEL				,1))
static MAKE_MSCB(mcb_azs_startai25sel3		,AZS_SET(AZS_STARTAI25SEL				,3))
static MAKE_MSCB(mcb_azs_startai25sel4		,AZS_SET(AZS_STARTAI25SEL				,0))
static MAKE_MSCB(mcb_btn_so72control	    ,PRS_BTN(BTN_SO72CONTROL				))
static MAKE_MSCB(mcb_btn_so72id     	    ,PRS_BTN(BTN_SO72ID     				))
static MAKE_MSCB(mcb_btn_startai9		    ,PRS_BTN(BTN_STARTAI9   				))
static MAKE_MSCB(mcb_btn_stopai9		    ,PRS_BTN(BTN_STOPAI9  					))
static MAKE_MSCB(mcb_btn_startai25		    ,PRS_BTN(BTN_STARTAI25  				))
static MAKE_MSCB(mcb_btn_stopai25		    ,PRS_BTN(BTN_STOPAI25  					))
static BOOL FSAPI mcb_glt_xpdr1_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_DEC(POS_XPDR1);
	HND_DEC(HND_XPDR1);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr1_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_INC(POS_XPDR1);
	HND_INC(HND_XPDR1);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr2_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_DEC(POS_XPDR2);
	HND_DEC(HND_XPDR2);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr2_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_INC(POS_XPDR2);
	HND_INC(HND_XPDR2);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr3_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_DEC(POS_XPDR3);
	HND_DEC(HND_XPDR3);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr3_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_INC(POS_XPDR3);
	HND_INC(HND_XPDR3);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr4_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_DEC(POS_XPDR4);
	HND_DEC(HND_XPDR4);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdr4_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	POS_INC(POS_XPDR4);
	HND_INC(HND_XPDR4);
	SINT32 myf=(SINT32)(POS_GET(POS_XPDR1)*1000+POS_GET(POS_XPDR2)*100+POS_GET(POS_XPDR3)*10+POS_GET(POS_XPDR4));
	trigger_key_event(KEY_XPNDR_SET,Dec2Bcd(myf));
	return TRUE;
}

static BOOL FSAPI mcb_glt_sgu_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	GLT_DEC(GLT_SGU6);
	return TRUE;
}

static BOOL FSAPI mcb_glt_sgu_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	GLT_INC(GLT_SGU6);
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdrm_l(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	GLT_DEC(GLT_XPDRMODE);
	return TRUE;
}

static BOOL FSAPI mcb_glt_xpdrm_r(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	GLT_INC(GLT_XPDRMODE);
	return TRUE;
}

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01	,AZS_TT(AZS_SO72PWR				))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_XPDRMAYDAY			))
static MAKE_TCB(tcb_03	,AZS_TT(AZS_STARTAI9PWR			))
static MAKE_TCB(tcb_04	,AZS_TT(AZS_STARTAI9MODE		))
static MAKE_TCB(tcb_05	,AZS_TT(AZS_STARTAI25PWR		))
static MAKE_TCB(tcb_06	,AZS_TT(AZS_STARTAI25MODE		))
static MAKE_TCB(tcb_07	,AZS_TT(AZS_STARTAI25SEL		))
static MAKE_TCB(tcb_08	,BTN_TT(BTN_SO72CONTROL			))
static MAKE_TCB(tcb_09	,BTN_TT(BTN_SO72ID				))
static MAKE_TCB(tcb_10	,BTN_TT(BTN_STARTAI9	        ))
static MAKE_TCB(tcb_11	,BTN_TT(BTN_STOPAI9				))
static MAKE_TCB(tcb_12	,BTN_TT(BTN_STARTAI25	        ))
static MAKE_TCB(tcb_13	,BTN_TT(BTN_STOPAI25	        ))
static MAKE_TCB(tcb_14	,HND_TT(HND_XPDR1		        ))
static MAKE_TCB(tcb_15	,HND_TT(HND_XPDR2		        ))
static MAKE_TCB(tcb_16	,HND_TT(HND_XPDR3		        ))
static MAKE_TCB(tcb_17	,HND_TT(HND_XPDR4		        ))
static MAKE_TCB(tcb_18	,GLT_TT(GLT_SGU6			    ))
static MAKE_TCB(tcb_19	,GLT_TT(GLT_XPDRMODE	        ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01	)
MAKE_TTA(tcb_02	) 
MAKE_TTA(tcb_03	)
MAKE_TTA(tcb_04	)
MAKE_TTA(tcb_05	)
MAKE_TTA(tcb_06	)
MAKE_TTA(tcb_07	) 
MAKE_TTA(tcb_08	)
MAKE_TTA(tcb_09	)
MAKE_TTA(tcb_10	)
MAKE_TTA(tcb_11	)
MAKE_TTA(tcb_12	) 
MAKE_TTA(tcb_13	)
MAKE_TTA(tcb_14	)
MAKE_TTA(tcb_15	)
MAKE_TTA(tcb_16	)
MAKE_TTA(tcb_17	) 
MAKE_TTA(tcb_18	)
MAKE_TTA(tcb_19	)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p6_2,HELP_NONE,0,0)
// �������� ��������
MOUSE_PBOX(0,0,P6_BACKGROUND2_D_SX,P6_BACKGROUND2_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)

MOUSE_TBOX(  "1",1022-OFFX,  294-OFFY, 47, 71,CURSOR_HAND,MOUSE_LR,mcb_azs_so72pwr)
MOUSE_TBOX(  "2", 803-OFFX,  334-OFFY, 53, 94,CURSOR_HAND,MOUSE_LR,mcb_azs_so72emerg)
MOUSE_TBOX(  "3", 480-OFFX,  282-OFFY,197, 70,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9pwr)
MOUSE_TBOX(  "4", 577-OFFX,  395-OFFY, 32, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9mode_cs)
MOUSE_TBOX(  "4", 644-OFFX,  366-OFFY, 38, 46,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9mode_start)
MOUSE_TBOX(  "4", 648-OFFX,  418-OFFY, 32, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9mode_stop)
MOUSE_TBOX(  "4", 607-OFFX,  370-OFFY, 40, 76,CURSOR_HAND,MOUSE_LR,mcb_azs_startai9mode_cntr)
MOUSE_TBOX(  "5", 267-OFFX,  282-OFFY,213, 83,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25pwr)
MOUSE_TBOX(  "6", 289-OFFX,  409-OFFY, 32, 50,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25mode_cs)
MOUSE_TBOX(  "6", 355-OFFX,  377-OFFY, 38, 46,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25mode_start)
MOUSE_TBOX(  "6", 356-OFFX,  430-OFFY, 38, 46,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25mode_stop)
MOUSE_TBOX(  "6", 322-OFFX,  390-OFFY, 40, 76,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25mode_cntr)
MOUSE_TBOX(  "7", 393-OFFX,  373-OFFY, 27, 37,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25sel1)
MOUSE_TBOX(  "7", 455-OFFX,  380-OFFY, 35, 95,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25sel2)
MOUSE_TBOX(  "7", 400-OFFX,  440-OFFY, 20, 40,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25sel3)
MOUSE_TBOX(  "7", 422-OFFX,  370-OFFY, 30,107,CURSOR_HAND,MOUSE_LR,mcb_azs_startai25sel4)

// ������
MOUSE_TBOX(  "8", 813-OFFX,  319-OFFY, 39, 39,CURSOR_HAND,MOUSE_DLR,mcb_btn_so72control)
MOUSE_TBOX(  "9",1048-OFFX,  382-OFFY, 39, 39,CURSOR_HAND,MOUSE_DLR,mcb_btn_so72id)
MOUSE_TBOX( "10", 566-OFFX,  460-OFFY,100, 47,CURSOR_HAND,MOUSE_DLR,mcb_btn_startai9)
MOUSE_TBOX( "11", 480-OFFX,  460-OFFY, 53, 54,CURSOR_HAND,MOUSE_DLR,mcb_btn_stopai9)
MOUSE_TBOX( "12", 370-OFFX,  472-OFFY, 91, 49,CURSOR_HAND,MOUSE_DLR,mcb_btn_startai25)
MOUSE_TBOX( "13", 267-OFFX,  472-OFFY,103, 49,CURSOR_HAND,MOUSE_DLR,mcb_btn_stopai25)
					 
// ���������		 
MOUSE_TSVB( "14", 858-OFFX,  388-OFFY, 20, 60,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdr1_l,mcb_glt_xpdr1_r)
MOUSE_TSVB( "15", 877-OFFX,  388-OFFY, 20, 60,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdr2_l,mcb_glt_xpdr2_r)
MOUSE_TSVB( "16",1000-OFFX,  380-OFFY, 20, 60,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdr3_l,mcb_glt_xpdr3_r)
MOUSE_TSVB( "17",1019-OFFX,  380-OFFY, 20, 60,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdr4_l,mcb_glt_xpdr4_r)
MOUSE_TSVB( "18",1211-OFFX,  340-OFFY, 80, 80,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_sgu_l,mcb_glt_sgu_r)
MOUSE_TSHB( "19", 898-OFFX,  277-OFFY, 74, 67,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_LR,mcb_glt_xpdrm_l,mcb_glt_xpdrm_r)
					 
MOUSE_END			 

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p6_2_list; 
GAUGE_HEADER_FS700_EX(P6_BACKGROUND2_D_SX, "p6_2", &p6_2_list, rect_p6_2, 0, 0, 0, 0, p6_2); 

MY_ICON2	(p6_swt_xpdrmd			, P6_SWT_XDPRMAYDAY_D_00		,NULL              			, 803-OFFX,  334-OFFY,icb_xpdrmayday	, 3	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_knb_sgu				, P6_KNB_SGU_D_00				,&l_p6_swt_xpdrmd			,1211-OFFX,  340-OFFY,icb_sgu       	, 5	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_knb_xpdrm			, P6_KNB_XDPRMODE_D_00			,&l_p6_knb_sgu				, 898-OFFX,  277-OFFY,icb_xpdrm     	, 4	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_knb_xpdrml			, P6_KNB_XDPRMODELIT_D_00		,&l_p6_knb_xpdrm			, 898-OFFX,  277-OFFY,icb_xpdrml    	, 4	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_btn_so72control		, P6_BTN_SO72CONTROL_D_00		,&l_p6_knb_xpdrml			, 813-OFFX,  319-OFFY,icb_so72control	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_btn_so72controll	, P6_BTN_SO72CONTROLLIT_D_00	,&l_p6_btn_so72control		, 813-OFFX,  319-OFFY,icb_so72controll	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_btn_so72id			, P6_BTN_SO72ID_D_00			,&l_p6_btn_so72controll		,1048-OFFX,  382-OFFY,icb_so72id     	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_btn_so72idl			, P6_BTN_SO72IDLIT_D_00			,&l_p6_btn_so72id			,1048-OFFX,  382-OFFY,icb_so72idl     	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_azs_so72pwr			, P6_AZS_SO72POWER_D_00			,&l_p6_btn_so72idl			,1022-OFFX,  294-OFFY,icb_so72pwr    	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_azs_so72pwrl		, P6_AZS_SO72POWERLIT_D_00		,&l_p6_azs_so72pwr			,1022-OFFX,  294-OFFY,icb_so72pwrl    	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_azs_startai9pwr		, P6_AZS_STARTAI9POWER_D_00		,&l_p6_azs_so72pwrl			, 480-OFFX,  282-OFFY,icb_startai9pwr	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_azs_startai9mode	, P6_AZS_STARTAI9MODE_D_00		,&l_p6_azs_startai9pwr		, 533-OFFX,  352-OFFY,icb_startai9mode	, 4	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_btn_startai9		, P6_BTN_STARTAI9_D_00			,&l_p6_azs_startai9mode		, 566-OFFX,  460-OFFY,icb_startai9   	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_btn_stopai9			, P6_BTN_STOPAI9_D_00			,&l_p6_btn_startai9			, 480-OFFX,  460-OFFY,icb_stopai9   	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_azs_startai25pwr	, P6_AZS_STARTAI25POWER_D_00	,&l_p6_btn_stopai9			, 267-OFFX,  282-OFFY,icb_startai25pwr	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_azs_startai25mode	, P6_AZS_STARTAI25MODE_D_00		,&l_p6_azs_startai25pwr		, 267-OFFX,  365-OFFY,icb_startai25mode	, 4	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_azs_startai25sel	, P6_AZS_STARTAI25SELECTOR_D_00	,&l_p6_azs_startai25mode	, 361-OFFX,  365-OFFY,icb_startai25sel	, 4	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_btn_startai25		, P6_BTN_STARTAI25_D_00			,&l_p6_azs_startai25sel		, 370-OFFX,  472-OFFY,icb_startai25   	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_btn_stopai25		, P6_BTN_STOPAI25_D_00			,&l_p6_btn_startai25		, 267-OFFX,  472-OFFY,icb_stopai25  	, 2	*PANEL_LIGHT_MAX	,p6_2)
MY_ICON2	(p6_knb_xpdr1			, P6_KNB_XPDR1_D_00				,&l_p6_btn_stopai25			, 858-OFFX,  388-OFFY,icb_xpdr1     	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_knb_xpdr1l			, P6_KNB_XPDR1LIT_D_00			,&l_p6_knb_xpdr1			, 858-OFFX,  388-OFFY,icb_xpdr1lit  	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_knb_xpdr2			, P6_KNB_XPDR2_D_00				,&l_p6_knb_xpdr1l			, 877-OFFX,  388-OFFY,icb_xpdr2     	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_knb_xpdr2l			, P6_KNB_XPDR2LIT_D_00			,&l_p6_knb_xpdr2			, 877-OFFX,  388-OFFY,icb_xpdr2lit  	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_knb_xpdr3			, P6_KNB_XPDR3_D_00				,&l_p6_knb_xpdr2l			,1000-OFFX,  380-OFFY,icb_xpdr3     	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_knb_xpdr3l			, P6_KNB_XPDR3LIT_D_00			,&l_p6_knb_xpdr3			,1000-OFFX,  380-OFFY,icb_xpdr3lit  	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_knb_xpdr4			, P6_KNB_XDPR4_D_00				,&l_p6_knb_xpdr3l			,1019-OFFX,  380-OFFY,icb_xpdr4     	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_knb_xpdr4l			, P6_KNB_XDPR4LIT_D_00			,&l_p6_knb_xpdr4			,1019-OFFX,  380-OFFY,icb_xpdr4lit  	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_scl_xpdr1			, P6_SCL_XPDR1_D_00				,&l_p6_knb_xpdr4l			, 903-OFFX,  356-OFFY,icb_xpdr1scl  	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_scl_xpdr1l			, P6_SCL_XPDR1LIT_D_00			,&l_p6_scl_xpdr1			, 903-OFFX,  356-OFFY,icb_xpdr1litscl	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_scl_xpdr2			, P6_SCL_XPDR2_D_00				,&l_p6_scl_xpdr1l			, 924-OFFX,  356-OFFY,icb_xpdr2scl  	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_scl_xpdr2l			, P6_SCL_XPDR2LIT_D_00			,&l_p6_scl_xpdr2			, 924-OFFX,  356-OFFY,icb_xpdr2litscl	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_scl_xpdr3			, P6_SCL_XDPR3_D_00				,&l_p6_scl_xpdr2l			, 941-OFFX,  356-OFFY,icb_xpdr3scl  	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_scl_xpdr3l			, P6_SCL_XDPR3LIT_D_00			,&l_p6_scl_xpdr3			, 941-OFFX,  356-OFFY,icb_xpdr3litscl	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_scl_xpdr4			, P6_SCL_XDPR4_D_00				,&l_p6_scl_xpdr3l			, 958-OFFX,  356-OFFY,icb_xpdr4scl  	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_scl_xpdr4l			, P6_SCL_XDPR4LIT_D_00			,&l_p6_scl_xpdr4			, 958-OFFX,  356-OFFY,icb_xpdr4litscl	, 10*PANEL_LIGHT_MAX	,p6_2)	
MY_ICON2	(p6_2Ico				, P6_BACKGROUND2_D				,&l_p6_scl_xpdr4l				      ,         0,         0,icb_Ico       ,   PANEL_LIGHT_MAX,p6_2)
MY_STATIC2	(p6_2bg,p6_2_list		, P6_BACKGROUND2_D				,&l_p6_2Ico	,p6_2)

#endif