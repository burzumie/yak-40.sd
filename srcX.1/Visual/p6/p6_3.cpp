/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P6.h"

#define OFFX	1342
#define OFFY	0

static MAKE_ICB(icb_crs_p6_01,SHOW_POSM(POS_CRS6_01,1,0))

//////////////////////////////////////////////////////////////////////////

static BOOL FSAPI mcb_crs_p0_01(PPIXPOINT relative_point,FLAGS32 mouse_flags)
{
	if(mouse_flags&MOUSE_MOVE) {
		POS_SET(POS_CRS6_01,1);
	}
	if(mouse_flags&MOUSE_LEFTSINGLE||mouse_flags&MOUSE_RIGHTSINGLE) {
		panel_window_close_ident(IDENT_P6);
		panel_window_open_ident(IDENT_P0);
	}
	return true;
}

BOOL FSAPI mcb_crs_clear(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_TCB(tcb_01	,POS_TT(POS_CRS6_01				))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MOUSE_TOOLTIP_ARGS_END

#ifndef ONLY3D

MOUSE_BEGIN(rect_p6_3,HELP_NONE,0,0)
MOUSE_PBOX(0,0,P6_BACKGROUND3_D_SX,P6_BACKGROUND3_D_SY,CURSOR_NONE,MOUSE_MOVE,mcb_crs_clear)
MOUSE_TBOX(  "1",1506-OFFX,    9-OFFY, 81,80,CURSOR_HAND,MOUSE_MLR,mcb_crs_p0_01)
MOUSE_END       

//////////////////////////////////////////////////////////////////////////

extern PELEMENT_HEADER p6_3_list; 
GAUGE_HEADER_FS700_EX(P6_BACKGROUND3_D_SX, "p6_3", &p6_3_list, rect_p6_3, 0, 0, 0, 0, p6_3); 

MY_ICON2	(p6_crs_01			,MISC_CRS_P6_01			,NULL				,1506-OFFX,   9-OFFY,icb_crs_p6_01 ,1,p6_3)
MY_ICON2	(p6_3Ico			,P6_BACKGROUND3_D		,&l_p6_crs_01		,        0,        0,icb_Ico       ,PANEL_LIGHT_MAX,p6_3)
MY_STATIC2	(p6_3bg,p6_3_list	,P6_BACKGROUND3_D		,&l_p6_3Ico	,p6_3)

#endif