/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Visual/p0/Main.cpp $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "P6.h"

double FSAPI icb_IcoLit(PELEMENT_ICON pelement);
double FSAPI icb_IcoIntl(PELEMENT_ICON pelement);
double FSAPI icb_IcoIntlLit(PELEMENT_ICON pelement);

static double FSAPI icb_kmplfreq(PELEMENT_ICON pelement)
{
	CHK_BRT();
	double val=HND_GET(HND_KMP1FREQ);
	if(val>9)
		val=0;
	return val+POS_GET(POS_PANEL_STATE)*10; 
}

double FSAPI icb_kmplherz	(PELEMENT_ICON pelement);
double FSAPI icb_kmp1vorl  	(PELEMENT_ICON pelement);
double FSAPI icb_kmp1pwrl  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst1l  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst2l 	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst3l  	(PELEMENT_ICON pelement);
double FSAPI icb_kmp1vor  	(PELEMENT_ICON pelement);
double FSAPI icb_kmp1pwr  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst1  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst2  	(PELEMENT_ICON pelement);
double FSAPI icb_kmptst3  	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl1scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl2scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl3scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl4scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl5scll	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl1scl	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl2scl	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl3scl	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl4scl	(PELEMENT_ICON pelement);
double FSAPI icb_kmpl5scl	(PELEMENT_ICON pelement);

BOOL FSAPI mcb_kmp1_big(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_btn_kmp1id(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_kmpfreq_r(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_kmpfreq_l(PPIXPOINT relative_point,FLAGS32 mouse_flags);

BOOL FSAPI mcb_glt_kmpfreqr_l(PPIXPOINT relative_point,FLAGS32 mouse_flags);
BOOL FSAPI mcb_glt_kmpfreqr_r(PPIXPOINT relative_point,FLAGS32 mouse_flags);

static MAKE_MSCB(mcb_azs_kmp1vor		    ,AZS_TGL(AZS_KMP1VOR    				))
static MAKE_MSCB(mcb_azs_kmp1pwr		    ,AZS_TGL(AZS_KMP1PWR    				))

static MAKE_MSCB(mcb_btn_kmp1test1		    ,PRS_BTN(BTN_KMP1TEST1  				))
static MAKE_MSCB(mcb_btn_kmp1test2		    ,PRS_BTN(BTN_KMP1TEST2  				))
static MAKE_MSCB(mcb_btn_kmp1test3		    ,PRS_BTN(BTN_KMP1TEST3  				))

static MAKE_TCB(tcb_01	,AZS_TT(AZS_KMP1VOR				))
static MAKE_TCB(tcb_02	,AZS_TT(AZS_KMP1PWR				))
static MAKE_TCB(tcb_03	,BTN_TT(BTN_KMP1TEST1	        ))
static MAKE_TCB(tcb_04	,BTN_TT(BTN_KMP1TEST2	        ))
static MAKE_TCB(tcb_05	,BTN_TT(BTN_KMP1TEST3	        ))
static MAKE_TCB(tcb_06	,BTN_TT(BTN_KMP1ID				))
static MAKE_TCB(tcb_08	,HND_TT(HND_KMP1FREQ	        ))

static MOUSE_TOOLTIP_ARGS(ttargs)
MAKE_TTA(tcb_01)
MAKE_TTA(tcb_02) 
MAKE_TTA(tcb_03)
MAKE_TTA(tcb_04)
MAKE_TTA(tcb_05)
MAKE_TTA(tcb_06)
MAKE_TTA(tcb_08)
MOUSE_TOOLTIP_ARGS_END

MOUSE_BEGIN(rect_p6_11,HELP_NONE,0,0)
MOUSE_PBOX(30,30,P20_BCK_MPNAV1_D_SX,P20_BCK_MPNAV1_D_SY,CURSOR_NONE,MOUSE_LR,mcb_kmp1_big)

MOUSE_TBOX(  "1",  94,   28, 33, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp1vor)
MOUSE_TBOX(  "2", 363,   28, 33, 49,CURSOR_HAND,MOUSE_LR,mcb_azs_kmp1pwr)
MOUSE_TBOX(  "3", 152,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1test1)
MOUSE_TBOX(  "4", 223,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1test2)
MOUSE_TBOX(  "5", 297,  126, 42, 42,CURSOR_HAND,MOUSE_DLR,mcb_btn_kmp1test3)
MOUSE_TSHB(  "7", 340,   83,120, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreqr_l,mcb_glt_kmpfreqr_r)
MOUSE_TBOX(  "6", 370,   87, 35, 90,CURSOR_HAND,MOUSE_LR ,mcb_btn_kmp1id)
MOUSE_TSHB(  "7",  50,   84, 90, 90,CURSOR_DOWNARROW,CURSOR_UPARROW,MOUSE_DLR,mcb_glt_kmpfreq_l,mcb_glt_kmpfreq_r)

MOUSE_END       

extern PELEMENT_HEADER p6_11_list; 
GAUGE_HEADER_FS700_EX(P20_BCK_MPNAV1_D_SX, "p6_11", &p6_11_list, rect_p6_11, 0, 0, 0, 0, p6_11); 

MY_ICON2	(p6_11knb_kmplf		, P20_KNB_MPFREQ_D_00			,NULL            		,  54,   87,icb_kmplfreq  	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11knb_kmplh		, P20_BTN_MPAUDIO_D_00			,&l_p6_11knb_kmplf		, 349,   87,icb_kmplherz  	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11azs_kmp1vor	, P20_AZS_MPFREQDME_D_00		,&l_p6_11knb_kmplh		,  94,   28,icb_kmp1vor    	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11azs_kmp1vorl	, P20_AZS_MPFREQDMELIT_D_00		,&l_p6_11azs_kmp1vor	,  94,   28,icb_kmp1vorl    , 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11azs_kmp1pwr	, P20_AZS_MPFREQPWR_D_00		,&l_p6_11azs_kmp1vorl	, 363,   28,icb_kmp1pwr    	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11azs_kmp1pwrl	, P20_AZS_MPFREQPWRLIT_D_00		,&l_p6_11azs_kmp1pwr	, 363,   28,icb_kmp1pwrl    , 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl1		, P20_SCL_MPFREQ1_D_00			,&l_p6_11azs_kmp1pwrl	, 181,   16,icb_kmpl1scl  	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl1l	, P20_SCL_MPFREQ1LIT_D_00		,&l_p6_11scl_kmpl1		, 181,   16,icb_kmpl1scll 	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl2		, P20_SCL_MPFREQ2_D_00			,&l_p6_11scl_kmpl1l		, 204,   16,icb_kmpl2scl  	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl2l	, P20_SCL_MPFREQ2LIT_D_00		,&l_p6_11scl_kmpl2		, 204,   16,icb_kmpl2scll 	, 2 *PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl3		, P20_SCL_MPFREQ3_D_00			,&l_p6_11scl_kmpl2l		, 227,   16,icb_kmpl3scl  	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl3l	, P20_SCL_MPFREQ3LIT_D_00		,&l_p6_11scl_kmpl3		, 227,   16,icb_kmpl3scll 	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl4		, P20_SCL_MPFREQ4_D_00			,&l_p6_11scl_kmpl3l		, 250,   16,icb_kmpl4scl  	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl4l	, P20_SCL_MPFREQ4LIT_D_00		,&l_p6_11scl_kmpl4		, 250,   16,icb_kmpl4scll 	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl5		, P20_SCL_MPFREQ5_D_00			,&l_p6_11scl_kmpl4l		, 286,   16,icb_kmpl5scl  	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11scl_kmpl5l	, P20_SCL_MPFREQ5LIT_D_00		,&l_p6_11scl_kmpl5		, 286,   16,icb_kmpl5scll 	, 10*PANEL_LIGHT_MAX,p6_11)	
MY_ICON2	(p6_11IcoIntlLit	, P20_BCK_MPNAV1INTLLIT_D		,&l_p6_11scl_kmpl5l		,   0,    0,icb_IcoIntlLit	,    PANEL_LIGHT_MAX,p6_11)
MY_ICON2	(p6_11IcoIntl		, P20_BCK_MPNAV1INTL_D			,&l_p6_11IcoIntlLit		,   0,    0,icb_IcoIntl		,    PANEL_LIGHT_MAX,p6_11)
MY_ICON2	(p6_11IcoLit		, P20_BCK_MPNAV1LIT_D			,&l_p6_11IcoIntl		,   0,    0,icb_IcoLit		,    PANEL_LIGHT_MAX,p6_11)
MY_ICON2	(p6_11Ico			, P20_BCK_MPNAV1_D				,&l_p6_11IcoLit			,   0,    0,icb_Ico			,    PANEL_LIGHT_MAX,p6_11)
MY_STATIC2	(p6_11bg,p6_11_list	, P20_BCK_MPNAV1_D				,&l_p6_11Ico			,p6_11)
