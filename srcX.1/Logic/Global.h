/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "Main.h"
#include "../Lib/simtools.h"
#include "Container.h"

class SDGlobal : public CSimKey
{
public:
	SDGlobal();
	~SDGlobal();

	void Init();
	void Update(bool first=false);
	void Load(const char *name);
	void Save(const char *name);

private:
	auto_ptr<SDLogicContainer>	m_Container;
	bool						m_Paused;
	std::string					m_PrevoiusFlight;
	std::string					m_UIGeneratedFlight;
	char						m_LoadFileName[MAX_PATH];
	char						m_SaveFileName[MAX_PATH];

	bool						m_EnginePressed;

	bool						fnKeyHandler(ID32 event,UINT32 evdata);
	void						PrepareSimSaveFiles();
	void						SaveLoadCheck();

};

extern SDGlobal *g_pGlobal;

