/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "Global.h"
#include "Systems/HTail.h"

SDGlobal *g_pGlobal=NULL;
HHOOK	g_hKeyHook;
bool	g_bGndServicesVisible=false;

extern LRESULT CALLBACK fnKeyboardProc(int nCode,WPARAM wParam,LPARAM lParam)
{
	if (nCode < 0)  
		return CallNextHookEx(g_hKeyHook, nCode, wParam, lParam); 

	BYTE ls=HIBYTE(GetKeyState(VK_LSHIFT));
	BYTE rs=HIBYTE(GetKeyState(VK_RSHIFT));

	if((ls||rs)&&(wParam==48&&((lParam>>30)&1))) {
		POS_TGL(POS_GROUND_STARTUP);
		return 1;
	}

	return CallNextHookEx(g_hKeyHook, nCode, wParam, lParam); 
}

SDGlobal::SDGlobal() : CSimKey()
{
	m_PrevoiusFlight	= "";
	m_UIGeneratedFlight	= "";
	m_LoadFileName[0]	= '\0';
	m_SaveFileName[0]	= '\0';

	PrepareSimSaveFiles();

	m_Container			= auto_ptr<SDLogicContainer>(new SDLogicContainer());
	StartKey();

	g_hKeyHook = SetWindowsHookEx( WH_KEYBOARD, fnKeyboardProc, (HINSTANCE) NULL, GetCurrentThreadId()); 

	m_EnginePressed=false;
}

SDGlobal::~SDGlobal()
{
	SaveLoadCheck();
	UnhookWindowsHookEx(g_hKeyHook);
	StopKey();
}

void SDGlobal::Init()
{
	POS_SET(POS_TIME_AFTER_LOAD,360);
	POS_SET(POS_PANEL_LANG,g_pSDAPI->m_Config.m_CfgCommon.m_iLanguage);
	SDLogicBase::InitAll();
}

void SDGlobal::Update(bool first)
{
	if(POS_GET(POS_MAIN_IS_LOADING)||g_SimData->m_SimPaused) {
		m_Paused=true;
	} else {
		m_Paused=false;
	}
	
	POS_SET(POS_SIM_PAUSED,m_Paused);

	POS_SET(POS_TIME_OF_DAY,g_SimData->GetParam(SIMPARAM_TIME_OF_DAY));

	if(!first)
		SaveLoadCheck();

	leaveon(m_Paused);

	POS_INC(POS_TIME_AFTER_LOAD);

	SDLogicBase::UpdateAll();
}

bool SDGlobal::fnKeyHandler(ID32 event,UINT32 evdata)
{
	CHtail *m_Htail		= CHtail::Instance();
	CFlaps *m_Flaps		= CFlaps::Instance();
	CRTU   *m_RTU		= CRTU::Instance();
	int tmp=0;
	int tmp2=0;
	switch(event) {
		case KEY_RELOAD_PANELS:
		case KEY_CONTROL_RELOAD_USER_AIRCRAFT:
			Init();
		break;
		case KEY_AUTOPILOT_ON:
		case KEY_AUTOPILOT_OFF:
		case KEY_AP_MASTER:
			AZS_TGL(AZS_AP_PWR);
			break;
		case KEY_AP_ALT_HOLD:
		case KEY_AP_ALT_HOLD_ON:
		case KEY_AP_ALT_HOLD_OFF:
			AZS_SET(AZS_AP_ALT_HOLD,1);
			break;
		case KEY_AP_WING_LEVELER_OFF:
		case KEY_AP_WING_LEVELER_ON:
		case KEY_AP_WING_LEVELER:
			AZS_TGL(AZS_AP_PITCH_HOLD);
			break;
		case KEY_TOGGLE_FLIGHT_DIRECTOR:
			AZS_SET(AZS_AP_CMD,1);
			break;
		case KEY_GEAR_TOGGLE:
			if(g_pSDAPI->GetN(GEAR_POS_NOSE)<10&&g_pSDAPI->GetN(GEAR_POS_LEFT)<10&&g_pSDAPI->GetN(GEAR_POS_RIGHT)<10&&!POS_GET(POS_MAIN_HYDRO_WORK))
				trigger_key_event(KEY_GEAR_UP,0);
			if(g_pSDAPI->GetN(GEAR_POS_NOSE)>16300&&g_pSDAPI->GetN(GEAR_POS_LEFT)>16300&&g_pSDAPI->GetN(GEAR_POS_RIGHT)>16300&&!POS_GET(POS_MAIN_HYDRO_WORK))
				trigger_key_event(KEY_GEAR_DOWN,0);

			KEY_SET(KEY_GEAR,1);
			break;   
		case KEY_SITUATION_RESET:
			Init();
			break;
		case KEY_ELEV_TRIM_UP:
			if(POS_GET(POS_AP_PITCH_MODE)&&!POS_GET(POS_AP_HAND_MODE))return true;
			m_Htail->Dec(0);
			break;
		case KEY_ELEV_TRIM_DN:
			if(POS_GET(POS_AP_PITCH_MODE)&&!POS_GET(POS_AP_HAND_MODE))return true;
			m_Htail->Inc(0);
			break;
		case KEY_BRAKES:
			POS_SET(POS_BRAKE_TRIGGER,1);
			break;
		case KEY_FLAPS_DOWN:
		case KEY_FLAPS_UP:
			//if(g_pSDAPI->m_Config.m_CfgFlaps.m_bRealControl) {
			if(!POS_GET(POS_MAIN_HYDRO_WORK)) {
				m_Flaps->ReturnFlapTab(0);
				return true;
			} 
			m_Flaps->CalculateFlapTab();
			//}
			break;
		case KEY_FLAPS_INCR:
			//if(g_pSDAPI->m_Config.m_CfgFlaps.m_bRealControl) {
			if(!POS_GET(POS_MAIN_HYDRO_WORK)) {
				m_Flaps->ReturnFlapTab(1);
				return true;
			} 
			m_Flaps->CalculateFlapTab();
			/*
			} else {
			if(NDL_GET(NDL_MAIN_HYD)<g_pSDAPI->m_Config.m_CfgFlaps.m_dHydLimit) {
			m_Flaps->ReturnFlapTab(1);
			return true;
			}
			if(!m_Flaps->GetLeftPos()) {
			for(int i=0;i<20;i++)
			m_Flaps->CalculateFlapTab();
			} else {
			for(int i=0;i<15;i++)
			m_Flaps->CalculateFlapTab();
			}
			}
			*/
			break;
		case KEY_FLAPS_DECR:
			//if(g_pSDAPI->m_Config.m_CfgFlaps.m_bRealControl) {
			if(!POS_GET(POS_MAIN_HYDRO_WORK)) {
				m_Flaps->ReturnFlapTab(2);
				return true;
			} 
			m_Flaps->CalculateFlapTab();
			/*
			} else {
			if(NDL_GET(NDL_MAIN_HYD)<g_pSDAPI->m_Config.m_CfgFlaps.m_dHydLimit) {
			m_Flaps->ReturnFlapTab(2);
			return true;
			}
			if(m_Flaps->GetLeftPos()==35) {
			for(int i=0;i<15;i++)
			m_Flaps->CalculateFlapTab();
			} else {
			for(int i=0;i<20;i++)
			m_Flaps->CalculateFlapTab();
			}
			}
			*/
			break;
		case KEY_ENGINE_AUTO_START:
			for(int i=AZS_COM1_BUS;i<AZS_EXT_PWR;i++)
				AZS_SET(i,1);

			AZS_SET(AZS_POWERSOURCE,POWER_SOURCE_BAT);
			AZS_SET(AZS_INV1_36V,1);
			AZS_SET(AZS_INV2_36V,1);
			AZS_SET(AZS_INV1_115V,1);
			AZS_SET(AZS_INV2_115V,1);
			AZS_SET(AZS_GENERATOR1,1);
			AZS_SET(AZS_GENERATOR2,1);
			AZS_SET(AZS_GENERATOR3,1);
			AZS_SET(AZS_EMERG_ENG1_CUT,0);
			AZS_SET(AZS_EMERG_ENG2_CUT,0);
			AZS_SET(AZS_EMERG_ENG3_CUT,0);
			AZS_SET(AZS_TOPL_LEV,1);
			AZS_SET(AZS_TOPL_PRAV,1);
			AZS_SET(AZS_PKAI251,3);
			AZS_SET(AZS_PKAI252,3);
			AZS_SET(AZS_PKAI253,3);
			AZS_SET(AZS_MANOM,1);
			AZS_SET(AZS_RADALT,1);
			GLT_SET(GLT_ARK1,1);
			GLT_SET(GLT_ARK2,1);
			AZS_SET(AZS_KMP1PWR,1);
			AZS_SET(AZS_KMP2PWR,1);
			AZS_SET(AZS_SO72PWR,1);
			break;
		case KEY_THROTTLE_DECR:
		case KEY_THROTTLE_DECR_SMALL:
		case KEY_DECREASE_THROTTLE:
			POS_SET(POS_RUD1_STOP,0);
			POS_SET(POS_RUD2_STOP,0);
			POS_SET(POS_RUD3_STOP,0);
			if(g_pSDAPI->GetN(GENERAL_ENGINE1_THROTTLE_LEVER_POS)==0&&g_pSDAPI->GetN(GENERAL_ENGINE2_THROTTLE_LEVER_POS)==0&&g_pSDAPI->GetN(GENERAL_ENGINE3_THROTTLE_LEVER_POS)==0)
				m_RTU->Process();
			break;
		case KEY_ENGINE:
			m_EnginePressed=true;
			break;
/*
		case KEY_SELECT_1:
			if(m_EnginePressed) {
				ImportTable.pGlobal->engine_control_select=1;
				m_EnginePressed=false;
			}
			break;
		case KEY_SELECT_2:
			if(m_EnginePressed) {
				ImportTable.pGlobal->engine_control_select=2;
				m_EnginePressed=false;
			}
			break;
		case KEY_SELECT_3:
			if(m_EnginePressed) {
				ImportTable.pGlobal->engine_control_select=4;
				m_EnginePressed=false;
			}
			break;
		case KEY_SELECT_4:
			if(m_EnginePressed) {
				ImportTable.pGlobal->engine_control_select=7;
				m_EnginePressed=false;
			}
			break;
*/
		default:
			POS_SET(POS_BRAKE_TRIGGER,0);
	}

	return true;

}

#define SAVE_SECTION(T,M,S,TYP) /**/ \
	for(i=0;i<M;i++) { \
		char buf[6]; \
		for(j=0;j<VAL_MAX;j++) { \
			sprintf(buf,T"%d%d",i,j); \
			ini->Write##TYP(SIM_FLT_MY_SECTION.c_str(),buf,g_SDAPIEntrys.S[i].m_Val[j]); \
		} \
	}

#define LOAD_SECTION(T,M,S,TYP,TYP2) /**/ \
	for(i=0;i<M;i++) { \
		TYP2 tmp; \
		char buf[6]; \
		for(j=0;j<VAL_MAX;j++) { \
			sprintf(buf,T"%d%d",i,j); \
			tmp=ini->Read##TYP(SIM_FLT_MY_SECTION.c_str(),buf,g_SDAPIEntrys.S[i].m_Val[j]); \
			g_SDAPIEntrys.S[i].m_Val[j]=tmp; \
		} \
	}

void SDGlobal::Load(const char *name)
{
	CIniFile *ini=new CIniFile(name);

	int i,j;

	LOAD_SECTION("0",AZS_MAX,SDAZS,Int,int);
	LOAD_SECTION("1",BTN_MAX,SDBTN,Int,int);
	LOAD_SECTION("2",KRM_MAX,SDKRM,Int,int);
	LOAD_SECTION("3",GLT_MAX,SDGLT,Int,int);
	LOAD_SECTION("4",POS_MAX,SDPOS,Double,double);
	LOAD_SECTION("5",NDL_MAX,SDNDL,Double,double);
	LOAD_SECTION("6",HND_MAX,SDHND,Int,int);
	LOAD_SECTION("7",TBL_MAX,SDTBL,Int,int);
	LOAD_SECTION("8",TBG_MAX,SDTBG,Int,int);
	LOAD_SECTION("9",LMP_MAX,SDLMP,Int,int);
	LOAD_SECTION("a",PWR_MAX,SDPWR,Int,int);
	LOAD_SECTION("b",SND_MAX,SDSND,Int,int);

	SDLogicBase::LoadAll(ini);

	delete ini;

	POS_SET(POS_TIME_AFTER_LOAD,0);
}

void SDGlobal::Save(const char *name)
{
	CIniFile *ini=new CIniFile(name);

	int i,j;

	SAVE_SECTION("0",AZS_MAX,SDAZS,Int);
	SAVE_SECTION("1",BTN_MAX,SDBTN,Int);
	SAVE_SECTION("2",KRM_MAX,SDKRM,Int);
	SAVE_SECTION("3",GLT_MAX,SDGLT,Int);
	SAVE_SECTION("4",POS_MAX,SDPOS,Double);
	SAVE_SECTION("5",NDL_MAX,SDNDL,Double);
	SAVE_SECTION("6",HND_MAX,SDHND,Int);
	SAVE_SECTION("7",TBL_MAX,SDTBL,Int);
	SAVE_SECTION("8",TBG_MAX,SDTBG,Int);
	SAVE_SECTION("9",LMP_MAX,SDLMP,Int);
	SAVE_SECTION("a",PWR_MAX,SDPWR,Int);
	SAVE_SECTION("b",SND_MAX,SDSND,Int);

	SDLogicBase::SaveAll(ini);

	delete ini;

}

#define UI_GENERATEDFLIGHT_STRING_RES	8705
#define PREVIOUSFLIGHT_STRING_RES		8706

void SDGlobal::PrepareSimSaveFiles()
{
	std::string tmp;
	tmp=g_pSDAPI->m_SimPath;
	tmp+=SIM_LANG_MODULE;
	GetStringFromResource(tmp,UI_GENERATEDFLIGHT_STRING_RES,m_UIGeneratedFlight,SIM_FLT_GENERATED);
	m_UIGeneratedFlight+="."+SIM_FLT_EXTENSION;
	GetStringFromResource(tmp,PREVIOUSFLIGHT_STRING_RES,m_PrevoiusFlight,SIM_FLT_PREVIOUS);
	m_PrevoiusFlight+="."+SIM_FLT_EXTENSION;
}

void SDGlobal::SaveLoadCheck()
{
	if(g_SimData) {
		if(g_SimData->m_SavedFileName[0]!='\0') {
			strcpy_s(m_SaveFileName,MAX_PATH,g_SimData->m_SavedFileName);
			g_SimData->m_SavedFileName[0]='\0';
			char *a=strrchr(m_SaveFileName,'\\')+1;
			if(_stricoll(a,m_UIGeneratedFlight.c_str())) {
				Save(m_SaveFileName);
			}
		}

		if(g_SimData->m_LoadedFileName[0]!='\0') {
			strcpy_s(m_LoadFileName,MAX_PATH,g_SimData->m_LoadedFileName);
			g_SimData->m_LoadedFileName[0]='\0';
			char *a=strrchr(m_LoadFileName,'\\')+1;
			if(_stricoll(a,m_UIGeneratedFlight.c_str())) {
				SDLogicBase::InitAll();
				Load(m_LoadFileName);
			}
		}
	}
}

