#pragma once

#include "../Main.h"
#include "../LogicBase.h"

#include "AirStarter.h"
#include "RTU.h"
#include "Flaps.h"
#include "Gear.h"
#include "Htail.h"

class SDSystemAircraft : public SDLogicBase
{
private:
	CAirStarter		*m_AirStarter;
	CRTU			*m_RTU;
	CHtail			*m_Htail;
	CFlaps			*m_Flaps;
	CGear			*m_Gear;

public:
	SDSystemAircraft();
	virtual ~SDSystemAircraft();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};
