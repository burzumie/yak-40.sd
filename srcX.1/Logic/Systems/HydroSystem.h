/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/HydroSystem.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"
#include "Flaps.h"
#include "Htail.h"
#include "RTU.h"
#include "Gear.h"

class SDHydroSystemBase
{
protected:
	CGear	*m_Gear;
	CFlaps	*m_Flaps;
	CHtail	*m_Htail;
	CRTU	*m_RTU;

	bool   m_Available;
	double m_Quantity;
	double m_BrakeLeftQuantity;
	double m_BrakeRightQuantity;
	static float m_BrakeCoeffDefault;

public:
	SDHydroSystemBase() {
		m_Gear	= CGear::Instance();
		m_Flaps	= CFlaps::Instance();
		m_Htail	= CHtail::Instance();
		m_RTU	= CRTU::Instance();

		m_Available=false;
		m_Quantity=-10;
		m_BrakeLeftQuantity=-10;
		m_BrakeRightQuantity=-10;
		m_BrakeCoeffDefault=0;
	};

	virtual void Update()=0;

	void SetBrakingCf(float val) {
		Sim1_main_class *sim1_class=(Sim1_main_class *)g_SimModules->m_Sim1->UserAircraftGet();
		if(sim1_class) {
			PSIM_DATA sim1_data=sim1_class->sim1_data;
			sim1_data->pStaticData->breaks->toe_brake_scale=val;
		}
	};

	double GetLeftBrakePos() {
		return g_pSDAPI->GetN(BRAKES_LEFT_POS)/32000;
	};

	double GetRightBrakePos() {
		return g_pSDAPI->GetN(BRAKES_RIGHT_POS)/32000;
	};

	bool GetParkingBrake() {
		return g_pSDAPI->GetN(PARKING_BRAKE_POS)!=0;
	};

};

class SDHydroSystemMain : protected SDHydroSystemBase
{
	bool m_Played;
public:
	SDHydroSystemMain() {
		m_Played=false;
	};
	virtual void Update();
	void AnimateGearSwitch();
};

class SDHydroSystemEmer : protected SDHydroSystemBase
{
public:
	SDHydroSystemEmer() {
		m_Quantity=150;
		m_Available=true;
	};
	virtual void Update();
};

class SDSystemHydro : public SDLogicBase, public SDHydroSystemMain, public SDHydroSystemEmer
{
private:
	auto_ptr<CNamedVar> xml11;
	auto_ptr<CNamedVar> xml12;
	auto_ptr<CNamedVar> xml21;
	auto_ptr<CNamedVar> xml22;
	auto_ptr<CNamedVar> xml31;
	auto_ptr<CNamedVar> xml32;

	auto_ptr<CNamedVar> VCAnimHydPri;
	auto_ptr<CNamedVar> VCAnimHydEmerg;
	auto_ptr<CNamedVar> VCAnimBrakesPriLeft;
	auto_ptr<CNamedVar> VCAnimBrakesPriRight;
	auto_ptr<CNamedVar> VCAnimBrakesEmergLeft;
	auto_ptr<CNamedVar> VCAnimBrakesEmergRight;

public:
	SDSystemHydro();
	virtual ~SDSystemHydro();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

	float GetBrakingCf() {
		float Ret=0;
		Sim1_main_class *sim1_class=(Sim1_main_class *)g_SimModules->m_Sim1->UserAircraftGet();
		if(sim1_class) {
			PSIM_DATA sim1_data=sim1_class->sim1_data;
			Ret=sim1_data->pStaticData->breaks->toe_brake_scale;
		}
		return Ret;
	};
};
