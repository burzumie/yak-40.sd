/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/Autopilot.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"
#include "HTail.h"
#include "../../Lib/simtools.h"

enum APVAL {
	APCUR,		
	APOLD,		
	APDIF,		
	APINT,
	APMAX
};

#ifdef _DEBUG
#define TIME_TO_POWER 4*18
#else
#define TIME_TO_POWER g_pSDAPI->m_Config.m_CfgAutopilot.m_dSecToLight*18
#endif

class SDSystemAutopilot : public SDLogicBase, public CTimerMixin
{
private:
	auto_ptr<CNamedVar> VCAnimBank;
	auto_ptr<CNamedVar> VCAnimPitch;

	CHtail *m_Htail;

	bool m_ApOnPlayed;

	int m_OldAutoCoord;
	bool m_AutoCoordSet;

	int m_Timer;
	bool m_Powered;
	bool m_TimerReseted;
	bool m_Ready;
	bool m_HandMode;
	bool m_BankMode;
	bool m_PitchMode;
	bool m_AltHoldMode;

	double m_Bank[APMAX];
	double m_Pitch[APMAX];
	double m_VS[APMAX];
	double m_AltHold[APMAX];

	double m_PitchHandle;
	double m_AltitudeToHold;
	bool m_AltitudeToHoldSet;

	_LARGE_INTEGER m_TimeStart,m_TimeEnd,m_TimeFreq;
	double m_TimeDelta;

	void GetData();
	void GetTimerDeltaStart();
	void GetTimerDeltaEnd();

	void BankPlane(double val);
	void PitchPlane(double val);

	void BankHandleMode();
	void PitchHandleMode();
	void AltitudeMode();
	void AutoHtail();

	void OnTimer(unsigned int uID=0);

public:
	SDSystemAutopilot();
	virtual ~SDSystemAutopilot();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

	void On();
	void HandModeOn()				{m_HandMode=true;		};
	void HandModeOff()				{m_HandMode=false;		};
	void SetPitch(double val)		{m_PitchHandle=val;		};
	void SetAlt(double val)			{m_AltitudeToHold=val;	};
	void IncPitch(double val=0.04)	{m_PitchHandle+=val;	};
	void DecPitch(double val=0.04)	{m_PitchHandle-=val;	};

	bool IsOn()				const	{ return m_Ready;		};
	bool IsHandMode()		const	{ return m_HandMode;	};
	bool IsPitchMode()		const	{ return m_PitchMode;	};
	bool IsBankMode()		const	{ return m_BankMode;	};
	bool IsAltholdmode()	const	{ return m_AltHoldMode;	};

};
