/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/MiscSystem.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "VCAzsGlt.h"

SDSystemVCAZSGLT::SDSystemVCAZSGLT() 
{
	m_XmlPanelLang=auto_ptr<CNamedVar>(new CNamedVar("sd6015_panel_lang",false));

	for(int i=0;i<MAX_AZS_IN_VC;i++) {
		m_AzsVC[i]=NULL;
	}

	m_AzsVC[  0]=new CNamedVar("p0_001azs",false);
	m_AzsVC[  1]=new CNamedVar("p0_002cov",false);
	m_AzsVC[  2]=new CNamedVar("p0_002azs",false);
	m_AzsVC[  3]=new CNamedVar("p0_003azs",false);
	m_AzsVC[  4]=new CNamedVar("p0_004cov",false);
	m_AzsVC[  5]=new CNamedVar("p0_004azs",false);
	m_AzsVC[  6]=new CNamedVar("p0_005azs",false);
	m_AzsVC[  7]=new CNamedVar("p0_006azs",false);
	m_AzsVC[  8]=new CNamedVar("p0_007azs",false);
	m_AzsVC[  9]=new CNamedVar("p0_008azs",false);
	m_AzsVC[ 10]=new CNamedVar("p0_009azs",false);
	m_AzsVC[ 11]=new CNamedVar("p0_010azs",false);
	m_AzsVC[ 12]=new CNamedVar("p1_011azs",false);
	m_AzsVC[ 13]=new CNamedVar("p1_012azs",false);
	m_AzsVC[ 14]=new CNamedVar("p1_013azs",false);
	m_AzsVC[ 15]=new CNamedVar("p1_014cov",false);
	m_AzsVC[ 16]=new CNamedVar("p1_014azs",false);
	m_AzsVC[ 17]=new CNamedVar("p1_015azs",false);
	m_AzsVC[ 18]=new CNamedVar("p1_016azs",false);
	m_AzsVC[ 19]=new CNamedVar("p1_017cov",false);
	m_AzsVC[ 20]=new CNamedVar("p1_017azs",false);
	m_AzsVC[ 21]=new CNamedVar("p1_018cov",false);
	m_AzsVC[ 22]=new CNamedVar("p1_018azs",false);
	m_AzsVC[ 23]=new CNamedVar("p1_019cov",false);
	m_AzsVC[ 24]=new CNamedVar("p1_019azs",false);
	m_AzsVC[ 25]=new CNamedVar("p1_020azs",false);
	m_AzsVC[ 26]=new CNamedVar("p1_021azs",false);
	m_AzsVC[ 27]=new CNamedVar("p1_022azs",false);
	m_AzsVC[ 28]=new CNamedVar("p1_023azs",false);
	m_AzsVC[ 29]=new CNamedVar("p1_024azs",false);
	m_AzsVC[ 30]=new CNamedVar("p1_025azs",false);
	m_AzsVC[ 31]=new CNamedVar("p1_026azs",false);
	m_AzsVC[ 32]=new CNamedVar("p1_027azs",false);
	m_AzsVC[ 33]=new CNamedVar("p1_028cov",false);
	m_AzsVC[ 34]=new CNamedVar("p1_028azs",false);
	m_AzsVC[ 35]=new CNamedVar("p1_029azs",false);
	m_AzsVC[ 36]=new CNamedVar("p1_030azs",false);
	m_AzsVC[ 37]=new CNamedVar("p1_030azs",false);
	m_AzsVC[ 38]=new CNamedVar("p1_031azs",false);
	m_AzsVC[ 39]=new CNamedVar("p1_032azs",false);
	m_AzsVC[ 40]=new CNamedVar("p1_033azs",false);
	m_AzsVC[ 41]=new CNamedVar("p4_034cov",false);
	m_AzsVC[ 42]=new CNamedVar("p4_034azs",false);
	m_AzsVC[ 43]=new CNamedVar("p4_035cov",false);
	m_AzsVC[ 44]=new CNamedVar("p4_035azs",false);
	m_AzsVC[ 45]=new CNamedVar("p4_036cov",false);
	m_AzsVC[ 46]=new CNamedVar("p4_036azs",false);
	m_AzsVC[ 47]=new CNamedVar("p4_037azs",false);
	m_AzsVC[ 48]=new CNamedVar("p4_038azs",false);
	m_AzsVC[ 49]=new CNamedVar("p4_039azs",false);
	m_AzsVC[ 50]=new CNamedVar("p4_040cov",false);
	m_AzsVC[ 51]=new CNamedVar("p4_040azs",false);
	m_AzsVC[ 52]=new CNamedVar("p4_041azs",false);
	m_AzsVC[ 53]=new CNamedVar("p4_042cov",false);
	m_AzsVC[ 54]=new CNamedVar("p4_042azs",false);
	m_AzsVC[ 55]=new CNamedVar("p4_043cov",false);
	m_AzsVC[ 56]=new CNamedVar("p4_043azs",false);
	m_AzsVC[ 57]=new CNamedVar("p4_044cov",false);
	m_AzsVC[ 58]=new CNamedVar("p4_044azs",false);
	m_AzsVC[ 59]=new CNamedVar("p4_045cov",false);
	m_AzsVC[ 60]=new CNamedVar("p4_045azs",false);
	m_AzsVC[ 61]=new CNamedVar("p4_046azs",false);
	m_AzsVC[ 62]=new CNamedVar("p4_047cov",false);
	m_AzsVC[ 63]=new CNamedVar("p4_047azs",false);
	m_AzsVC[ 64]=new CNamedVar("p4_048cov",false);
	m_AzsVC[ 65]=new CNamedVar("p4_048azs",false);
	m_AzsVC[ 66]=new CNamedVar("p4_049cov",false);
	m_AzsVC[ 67]=new CNamedVar("p4_049azs",false);
	m_AzsVC[ 68]=new CNamedVar("p4_050azs",false);
	m_AzsVC[ 69]=new CNamedVar("p4_051azs",false);
	m_AzsVC[ 70]=new CNamedVar("p5_052azs",false);
	m_AzsVC[ 71]=new CNamedVar("p5_053azs",false);
	m_AzsVC[ 72]=new CNamedVar("p5_054azs",false);
	m_AzsVC[ 73]=new CNamedVar("p5_055azs",false);
	m_AzsVC[ 74]=new CNamedVar("p5_056azs",false);
	m_AzsVC[ 75]=new CNamedVar("p5_057cov",false);
	m_AzsVC[ 76]=new CNamedVar("p5_057azs",false);
	m_AzsVC[ 77]=new CNamedVar("p5_058cov",false);
	m_AzsVC[ 78]=new CNamedVar("p5_058azs",false);
	m_AzsVC[ 79]=new CNamedVar("p5_059cov",false);
	m_AzsVC[ 80]=new CNamedVar("p5_059azs",false);
	m_AzsVC[ 81]=new CNamedVar("p5_060azs",false);
	m_AzsVC[ 82]=new CNamedVar("p5_061azs",false);
	m_AzsVC[ 83]=new CNamedVar("p5_062azs",false);
	m_AzsVC[ 84]=new CNamedVar("p5_063azs",false);
	m_AzsVC[ 85]=new CNamedVar("p5_064azs",false);
	m_AzsVC[ 86]=new CNamedVar("p6_065azs",false);
	m_AzsVC[ 87]=new CNamedVar("p6_066azs",false);
	m_AzsVC[ 88]=new CNamedVar("p6_067azs",false);
	m_AzsVC[ 89]=new CNamedVar("p6_068azs",false);
	m_AzsVC[ 90]=new CNamedVar("p6_069azs",false);
	m_AzsVC[ 91]=new CNamedVar("p6_070azs",false);
	m_AzsVC[ 92]=new CNamedVar("p7_071azs",false);
	m_AzsVC[ 93]=new CNamedVar("p7_072azs",false);
	m_AzsVC[ 94]=new CNamedVar("p7_073azs",false);
	m_AzsVC[ 95]=new CNamedVar("p7_074azs",false);
	m_AzsVC[ 96]=new CNamedVar("p7_075azs",false);
	m_AzsVC[ 97]=new CNamedVar("p7_076azs",false);
	m_AzsVC[ 98]=new CNamedVar("p7_077azs",false);
	m_AzsVC[ 99]=new CNamedVar("p8_078azs",false);
	m_AzsVC[100]=new CNamedVar("p8_079azs",false);
	m_AzsVC[101]=new CNamedVar("p8_080azs",false);
	m_AzsVC[102]=new CNamedVar("p8_081azs",false);
	m_AzsVC[103]=new CNamedVar("p8_082azs",false);
	m_AzsVC[104]=new CNamedVar("p8_083azs",false);
	m_AzsVC[105]=new CNamedVar("p8_084azs",false);
	m_AzsVC[106]=new CNamedVar("p8_085azs",false);
	m_AzsVC[107]=new CNamedVar("p8_086azs",false);
	m_AzsVC[108]=new CNamedVar("p8_087azs",false);
	m_AzsVC[109]=new CNamedVar("p8_088azs",false);
	m_AzsVC[110]=new CNamedVar("p8_089azs",false);
	m_AzsVC[111]=new CNamedVar("p8_090azs",false);
	m_AzsVC[112]=new CNamedVar("p8_091azs",false);
	m_AzsVC[113]=new CNamedVar("p8_092azs",false);
	m_AzsVC[114]=new CNamedVar("p8_093azs",false);
	m_AzsVC[115]=new CNamedVar("p8_094azs",false);
	m_AzsVC[116]=new CNamedVar("p8_095azs",false);
	m_AzsVC[117]=new CNamedVar("p8_096azs",false);
	m_AzsVC[118]=new CNamedVar("p8_097azs",false);
	m_AzsVC[119]=new CNamedVar("p8_098azs",false);
	m_AzsVC[120]=new CNamedVar("p8_099azs",false);
	m_AzsVC[121]=new CNamedVar("p8_100azs",false);
	m_AzsVC[122]=new CNamedVar("p8_101azs",false);
	m_AzsVC[123]=new CNamedVar("p8_102azs",false);
	m_AzsVC[124]=new CNamedVar("p8_103azs",false);
	m_AzsVC[125]=new CNamedVar("p8_104azs",false);
	m_AzsVC[126]=new CNamedVar("p8_105azs",false);
	m_AzsVC[127]=new CNamedVar("p8_106azs",false);
	m_AzsVC[128]=new CNamedVar("p8_107azs",false);
	m_AzsVC[129]=new CNamedVar("p8_108azs",false);
	m_AzsVC[130]=new CNamedVar("p8_109azs",false);
	m_AzsVC[131]=new CNamedVar("p8_110azs",false);
	m_AzsVC[132]=new CNamedVar("p8_111azs",false);
	m_AzsVC[133]=new CNamedVar("p8_112azs",false);
	m_AzsVC[134]=new CNamedVar("p8_113azs",false);
	m_AzsVC[135]=new CNamedVar("p9_114azs",false);
	m_AzsVC[136]=new CNamedVar("p9_115azs",false);
	m_AzsVC[137]=new CNamedVar("p9_116azs",false);
	m_AzsVC[138]=new CNamedVar("p9_117azs",false);
	m_AzsVC[139]=new CNamedVar("p9_118azs",false);
	m_AzsVC[140]=new CNamedVar("p9_119azs",false);
	m_AzsVC[141]=new CNamedVar("p9_120azs",false);
	m_AzsVC[142]=new CNamedVar("p9_121azs",false);
	m_AzsVC[143]=new CNamedVar("p9_122azs",false);
	m_AzsVC[144]=new CNamedVar("p9_123azs",false);
	m_AzsVC[145]=new CNamedVar("p9_124azs",false);
	m_AzsVC[146]=new CNamedVar("p9_125azs",false);
	m_AzsVC[147]=new CNamedVar("p9_126azs",false);
	m_AzsVC[148]=new CNamedVar("p9_127azs",false);
	m_AzsVC[149]=new CNamedVar("p9_128azs",false);
	m_AzsVC[150]=new CNamedVar("p9_129azs",false);
	m_AzsVC[151]=new CNamedVar("p9_130azs",false);
	m_AzsVC[152]=new CNamedVar("p9_131azs",false);
	m_AzsVC[153]=new CNamedVar("p9_132azs",false);
	m_AzsVC[154]=new CNamedVar("p9_133azs",false);
	m_AzsVC[155]=new CNamedVar("p9_134azs",false);
	m_AzsVC[156]=new CNamedVar("p9_135azs",false);
	m_AzsVC[157]=new CNamedVar("p9_136azs",false);
	m_AzsVC[158]=new CNamedVar("p9_137azs",false);
	m_AzsVC[159]=new CNamedVar("p9_138azs",false);
	m_AzsVC[160]=new CNamedVar("p9_139azs",false);
	m_AzsVC[161]=new CNamedVar("p9_140azs",false);
	m_AzsVC[162]=new CNamedVar("p9_141azs",false);
	m_AzsVC[163]=new CNamedVar("p9_142azs",false);
	m_AzsVC[164]=new CNamedVar("p9_143azs",false);
	m_AzsVC[165]=new CNamedVar("p9_144azs",false);
	m_AzsVC[166]=new CNamedVar("p9_145azs",false);
	m_AzsVC[167]=new CNamedVar("p9_146azs",false);
	m_AzsVC[168]=new CNamedVar("p9_147azs",false);
	m_AzsVC[169]=new CNamedVar("p9_148azs",false);
	m_AzsVC[170]=new CNamedVar("p9_149azs",false);
	m_AzsVC[171]=new CNamedVar("p4_052azs",false);
	m_AzsVC[172]=new CNamedVar("p4_053azs",false);

	m_GltVC[  0]=new CNamedVar("sd6015_glt_sgu_left",false);
	m_GltVC[  1]=new CNamedVar("sd6015_glt_sgu_right",false);
	m_GltVC[  2]=new CNamedVar("sd6015_glt_xpdr_mode",false);
	m_GltVC[  3]=new CNamedVar("sd6015_glt__temp",false);
	m_GltVC[  4]=new CNamedVar("sd6015_glt_v27",false);
	m_GltVC[  5]=new CNamedVar("sd6015_glt_v36",false);
	m_GltVC[  6]=new CNamedVar("sd6015_glt_v115",false);
};

SDSystemVCAZSGLT::~SDSystemVCAZSGLT() 
{
	for(int i=0;i<MAX_AZS_IN_VC;i++) {
		if(m_AzsVC[i]) {
			delete m_AzsVC[i];
			m_AzsVC[i]=NULL;
		}
	}

	for(int i=0;i<MAX_GLT_IN_VC;i++) {
		if(m_GltVC[i]) {
			delete m_GltVC[i];
			m_GltVC[i]=NULL;
		}
	}

};

void SDSystemVCAZSGLT::Init() 
{
};

void SDSystemVCAZSGLT::Update()
{
	m_XmlPanelLang->set_number(POS_GET(POS_PANEL_LANG)+1);
	UpdateVCAZS();
}

void SDSystemVCAZSGLT::Load(CIniFile *ini)
{
}

void SDSystemVCAZSGLT::Save(CIniFile *ini)
{
}

void SDSystemVCAZSGLT::UpdateVCAZS()
{
	static int m_Tick=0;

	if(POS_GET(POS_ACTIVE_VIEW_MODE)!=VIEW_MODE_VIRTUAL_COCKPIT)
		return;

	m_Tick=++m_Tick%2;

	switch(m_Tick) {
		case 0:
			m_AzsVC[  0]->set_number(AZS_GET(AZS_FIRE_SOUND_ALARM		));
			m_AzsVC[  1]->set_number(AZS_GET(AZS_GEAR_WARNING_COV		));
			m_AzsVC[  2]->set_number(AZS_GET(AZS_GEAR_WARNING			));
			m_AzsVC[  3]->set_number(AZS_GET(AZS_AUTO_SKID				));
			m_AzsVC[  4]->set_number(AZS_GET(AZS_ADI_BACKUP_POWER_COV	));
			m_AzsVC[  5]->set_number(AZS_GET(AZS_ADI_BACKUP_POWER		));
			m_AzsVC[  6]->set_number(AZS_GET(AZS_APU_FUEL_VALVE			));
			m_AzsVC[  7]->set_number(AZS_GET(AZS_WARNING_LIGHTS			));
			m_AzsVC[  8]->set_number(AZS_GET(AZS_EMERG_EXIT_LIGHTS		));
			m_AzsVC[  9]->set_number(AZS_GET(AZS_WHITE_DECK_FLOOD_LIGHT	));
			m_AzsVC[ 10]->set_number(AZS_GET(AZS_RED_PANEL_LIGHT		));
			m_AzsVC[ 11]->set_number(!AZS_GET(AZS_FUEL_IND)?1:AZS_GET(AZS_FUEL_IND)==1?0:2);
			m_AzsVC[ 12]->set_number(AZS_GET(AZS_POWERSOURCE)==POWER_SOURCE_BAT?0:AZS_GET(AZS_POWERSOURCE)==POWER_SOURCE_OFF?1:2);
			m_AzsVC[ 13]->set_number(AZS_GET(AZS_INV1_36V				));
			m_AzsVC[ 14]->set_number(AZS_GET(AZS_INV2_36V				));
			m_AzsVC[ 15]->set_number(AZS_GET(AZS_115V_BCKUP_COV			));
			m_AzsVC[ 16]->set_number(AZS_GET(AZS_115V_BCKUP				));
			m_AzsVC[ 17]->set_number(AZS_GET(AZS_INV1_115V				));
			m_AzsVC[ 18]->set_number(AZS_GET(AZS_INV2_115V				));
			m_AzsVC[ 19]->set_number(AZS_GET(AZS_AIR_PRESS_DROP_EMERG_COV));
			m_AzsVC[ 20]->set_number(AZS_GET(AZS_AIR_PRESS_DROP_EMERG	));
			m_AzsVC[ 21]->set_number(AZS_GET(AZS_AIR_PRESS_CONTROL_BCKUP_COV));
			m_AzsVC[ 22]->set_number(AZS_GET(AZS_AIR_PRESS_CONTROL_BCKUP));
			m_AzsVC[ 23]->set_number(AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG_COV));
			m_AzsVC[ 24]->set_number(AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG)==0?1:AZS_GET(AZS_AIR_PRESS_SYS_MASTER_EMERG)==1?0:2);
			m_AzsVC[ 25]->set_number(AZS_GET(AZS_AIR_PRESS_SYS_SOUND_ALARM));
			m_AzsVC[ 26]->set_number(AZS_GET(AZS_AIR_PRESS_SYS_MASTER	)==0?1:AZS_GET(AZS_AIR_PRESS_SYS_MASTER)==1?0:2);
			m_AzsVC[ 27]->set_number(AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN)==0?0:AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN)==1?2:AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_CABIN)==2?3:1);
			m_AzsVC[ 28]->set_number(AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT)==0?0:AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT)==1?2:AZS_GET(AZS_AIR_COND_SYS_TEMP_CNTRL_DUCT)==2?3:1);
			m_AzsVC[ 29]->set_number(AZS_GET(AZS_BLEEDAIR_EXTRACT_VALVE	)==0?0:AZS_GET(AZS_BLEEDAIR_EXTRACT_VALVE	)==1?2:AZS_GET(AZS_BLEEDAIR_EXTRACT_VALVE	)==2?3:1);
			m_AzsVC[ 30]->set_number(AZS_GET(AZS_BLEEDAIR_EXTRACT_RATIO	));
			m_AzsVC[ 31]->set_number(AZS_GET(AZS_DECK_COND				));
			m_AzsVC[ 32]->set_number(AZS_GET(AZS_STAIRS_PWR				));
			m_AzsVC[ 33]->set_number(AZS_GET(AZS_STAIRS_COV				));
			m_AzsVC[ 34]->set_number(AZS_GET(AZS_STAIRS					));
			m_AzsVC[ 35]->set_number(AZS_GET(AZS_NO_SMOKING_SIGNS		));
			m_AzsVC[ 36]->set_number(AZS_GET(AZS_REDLIGHT1				));
			m_AzsVC[ 37]->set_number(AZS_GET(AZS_REDLIGHT2				));
			m_AzsVC[ 38]->set_number(AZS_GET(AZS_GENERATOR1				));
			m_AzsVC[ 39]->set_number(AZS_GET(AZS_GENERATOR2				));
			m_AzsVC[ 40]->set_number(AZS_GET(AZS_GENERATOR3				));
			m_AzsVC[ 41]->set_number(AZS_GET(AZS_EMERG_ENG1_CUT_COV		));
			m_AzsVC[ 42]->set_number(AZS_GET(AZS_EMERG_ENG1_CUT			));
			m_AzsVC[ 43]->set_number(AZS_GET(AZS_EMERG_ENG2_CUT_COV		));
			m_AzsVC[ 44]->set_number(AZS_GET(AZS_EMERG_ENG2_CUT			));
			m_AzsVC[ 45]->set_number(AZS_GET(AZS_EMERG_ENG3_CUT_COV		));
			m_AzsVC[ 46]->set_number(AZS_GET(AZS_EMERG_ENG3_CUT			));
			m_AzsVC[ 47]->set_number(AZS_GET(AZS_ACT					));
			m_AzsVC[ 48]->set_number(AZS_GET(AZS_TOPL_LEV				));
			m_AzsVC[ 49]->set_number(AZS_GET(AZS_TOPL_PRAV				));
			m_AzsVC[ 50]->set_number(AZS_GET(AZS_TOPL_PRAV_AVAR_COV		));
			m_AzsVC[ 51]->set_number(AZS_GET(AZS_TOPL_PRAV_AVAR			));
			m_AzsVC[ 52]->set_number(AZS_GET(AZS_MANOM					));
			m_AzsVC[ 53]->set_number(AZS_GET(AZS_STAB_MAIN_CONTR_COV	));
			m_AzsVC[ 54]->set_number(AZS_GET(AZS_STAB_MAIN_CONTR		));
			m_AzsVC[ 55]->set_number(AZS_GET(AZS_EMERG_GEAR_COV			));
			m_AzsVC[ 56]->set_number(AZS_GET(AZS_EMERG_GEAR				));
			m_AzsVC[ 57]->set_number(AZS_GET(AZS_FLAPS_COV				));
			m_AzsVC[ 58]->set_number(AZS_GET(AZS_FLAPS					));
			m_AzsVC[ 59]->set_number(AZS_GET(AZS_EMERG_FLAPS_COV		));
			m_AzsVC[ 60]->set_number(AZS_GET(AZS_EMERG_FLAPS			));
			m_AzsVC[ 61]->set_number(AZS_GET(AZS_FUEL_PUMP_LO			));
			m_AzsVC[ 62]->set_number(AZS_GET(AZS_XFEED_TANKS_COV		));
			m_AzsVC[ 63]->set_number(AZS_GET(AZS_XFEED_TANKS			));
			m_AzsVC[ 64]->set_number(AZS_GET(AZS_XFEED_PUMPS_COV		));
			m_AzsVC[ 65]->set_number(AZS_GET(AZS_XFEED_PUMPS			));
			m_AzsVC[ 66]->set_number(AZS_GET(AZS_HYD_PUMP_COV			));
			m_AzsVC[ 67]->set_number(AZS_GET(AZS_HYD_PUMP				));
			m_AzsVC[ 68]->set_number(AZS_GET(AZS_TRIM_ELERON)==0?1:AZS_GET(AZS_TRIM_ELERON)==1?0:2);
			m_AzsVC[ 69]->set_number(AZS_GET(AZS_TRIM_RUDDER)==0?1:AZS_GET(AZS_TRIM_RUDDER)==1?0:2);
			m_AzsVC[ 70]->set_number(AZS_GET(AZS_LLIGHTLEFT	)==0?1:AZS_GET(AZS_LLIGHTLEFT	)==1?2:0);
			m_AzsVC[ 71]->set_number(AZS_GET(AZS_LLIGHTRIGHT)==0?1:AZS_GET(AZS_LLIGHTRIGHT  )==1?2:0);
			m_AzsVC[ 72]->set_number(AZS_GET(AZS_LLIGHTSMOTOR			));
			m_AzsVC[ 73]->set_number(AZS_GET(AZS_LIGHTSNAV	));
			m_AzsVC[ 74]->set_number(AZS_GET(AZS_LIGHTSBCN	));
			m_AzsVC[ 75]->set_number(AZS_GET(AZS_PKAI251_COV			));
			break;
		case 1:
			m_AzsVC[ 76]->set_number(AZS_GET(AZS_PKAI251)==2||AZS_GET(AZS_PKAI251)==3?1:0);
			m_AzsVC[ 77]->set_number(AZS_GET(AZS_PKAI252_COV			));
			m_AzsVC[ 78]->set_number(AZS_GET(AZS_PKAI252)==2||AZS_GET(AZS_PKAI252)==3?1:0);
			m_AzsVC[ 79]->set_number(AZS_GET(AZS_PKAI253_COV			));
			m_AzsVC[ 80]->set_number(AZS_GET(AZS_PKAI253)==2||AZS_GET(AZS_PKAI253)==3?1:0);
			m_AzsVC[ 81]->set_number(AZS_GET(AZS_WINDOWHEATL			));
			m_AzsVC[ 82]->set_number(AZS_GET(AZS_WINDOWHEATR			));
			m_AzsVC[ 83]->set_number(AZS_GET(AZS_WINDOWHEATPWR			));
			m_AzsVC[ 84]->set_number(AZS_GET(AZS_ARK1LEFTRIGHT			));
			m_AzsVC[ 85]->set_number(AZS_GET(AZS_ARK2LEFTRIGHT			));
			m_AzsVC[ 86]->set_number(AZS_GET(AZS_RADALT					));
			m_AzsVC[ 87]->set_number(AZS_GET(AZS_STARTAI9PWR			));
			m_AzsVC[ 88]->set_number(AZS_GET(AZS_STARTAI9MODE)==0?0:AZS_GET(AZS_STARTAI9MODE)==1?2:AZS_GET(AZS_STARTAI9MODE)==2?3:1);
			m_AzsVC[ 89]->set_number(AZS_GET(AZS_STARTAI25PWR			));
			m_AzsVC[153]->set_number(AZS_GET(AZS_TURBO_COOLER_UNIT_BUS	));
			m_AzsVC[ 90]->set_number(AZS_GET(AZS_STARTAI25MODE			));
			m_AzsVC[ 91]->set_number(AZS_GET(AZS_STARTAI25SEL			));
			m_AzsVC[ 92]->set_number(AZS_GET(AZS_ENG1HEAT				));
			m_AzsVC[ 93]->set_number(AZS_GET(AZS_ENG2HEAT				));
			m_AzsVC[ 94]->set_number(AZS_GET(AZS_ENG3HEAT				));
			m_AzsVC[ 95]->set_number(AZS_GET(AZS_PITOTHEAT1				));
			m_AzsVC[ 96]->set_number(AZS_GET(AZS_PITOTHEAT2				));
			m_AzsVC[ 97]->set_number(AZS_GET(AZS_ICEWARN1				));
			m_AzsVC[ 98]->set_number(AZS_GET(AZS_ICEWARN2				));
			m_AzsVC[154]->set_number(AZS_GET(AZS_NOSE_LEG_STEER_BUS		));
			m_AzsVC[ 99]->set_number(AZS_GET(AZS_COM1_BUS				));
			m_AzsVC[100]->set_number(AZS_GET(AZS_CAPT_VSI_BUS			));
			m_AzsVC[101]->set_number(AZS_GET(AZS_CAPT_ADI_MAIN_BUS		));
			m_AzsVC[102]->set_number(AZS_GET(AZS_FUEL_IND_BUS			));
			m_AzsVC[103]->set_number(AZS_GET(AZS_LAMP_TEST_BUS			));
			m_AzsVC[104]->set_number(AZS_GET(AZS_ENG1_IND_BUS			));
			m_AzsVC[105]->set_number(AZS_GET(AZS_ENG2_IND_BUS			));
			m_AzsVC[106]->set_number(AZS_GET(AZS_ENG3_IND_BUS			));
			m_AzsVC[107]->set_number(AZS_GET(AZS_CAPT_ADI_BACKUP_BUS	));
			m_AzsVC[155]->set_number(AZS_GET(AZS_FUEL_X_FEED9_BUS		));
			m_AzsVC[108]->set_number(AZS_GET(AZS_ADF1_BUS				));
			m_AzsVC[109]->set_number(AZS_GET(AZS_XPDR_BUS				));
			m_AzsVC[110]->set_number(AZS_GET(AZS_ELEC_36V_BACKUP_BUS	));
			m_AzsVC[111]->set_number(AZS_GET(AZS_INV1_115V_BUS			));
			m_AzsVC[112]->set_number(AZS_GET(AZS_INV2_115V_BUS			));
			m_AzsVC[113]->set_number(AZS_GET(AZS_AUTO_BACKUP_115V_BUS	));
			m_AzsVC[114]->set_number(AZS_GET(AZS_ENG1_FIRE_ALARM_BUS	));
			m_AzsVC[115]->set_number(AZS_GET(AZS_ENG2_FIRE_ALARM_BUS	));
			m_AzsVC[116]->set_number(AZS_GET(AZS_ENG3_FIRE_ALARM_BUS	));
			m_AzsVC[156]->set_number(AZS_GET(AZS_ENG1_DE_ICE_BUS		));
			m_AzsVC[117]->set_number(AZS_GET(AZS_STALL_WARN_BUS			));
			m_AzsVC[118]->set_number(AZS_GET(AZS_FUEL_PUMP_AUTO_BACKUP_BUS));
			m_AzsVC[119]->set_number(AZS_GET(AZS_FUEL_X_FEED8_BUS		));
			m_AzsVC[120]->set_number(AZS_GET(AZS_ENG1_FUEL_VALVE_BUS	));
			m_AzsVC[121]->set_number(AZS_GET(AZS_ENG2_FUEL_VALVE_BUS	));
			m_AzsVC[122]->set_number(AZS_GET(AZS_ENG3_FUEL_VALVE_BUS	));
			m_AzsVC[123]->set_number(AZS_GET(AZS_FIRE_EXT_1_BUS			));
			m_AzsVC[124]->set_number(AZS_GET(AZS_FIRE_EXT_2_BUS			));
			m_AzsVC[125]->set_number(AZS_GET(AZS_FIRE_EXT_3_BUS			));
			m_AzsVC[157]->set_number(AZS_GET(AZS_ENG2_DE_ICE_BUS		));
			m_AzsVC[126]->set_number(AZS_GET(AZS_AIR_STARTER_IND_BUS	));
			m_AzsVC[127]->set_number(AZS_GET(AZS_GEAR_UP_HORN_BUS		));
			m_AzsVC[128]->set_number(AZS_GET(AZS_APU_IGNIT_BUS			));
			m_AzsVC[129]->set_number(AZS_GET(AZS_ENG1_IGNIT_BUS			));
			m_AzsVC[130]->set_number(AZS_GET(AZS_ENG2_IGNIT_BUS			));
			m_AzsVC[131]->set_number(AZS_GET(AZS_ENG3_IGNIT_BUS			));
			m_AzsVC[132]->set_number(AZS_GET(AZS_FIRE_EXT1_VALVE_BUS	));
			m_AzsVC[133]->set_number(AZS_GET(AZS_FIRE_EXT2_VALVE_BUS	));
			m_AzsVC[134]->set_number(AZS_GET(AZS_FIRE_EXT3_VALVE_BUS	));
			m_AzsVC[158]->set_number(AZS_GET(AZS_ENG3_DE_ICE_BUS		));
			m_AzsVC[135]->set_number(AZS_GET(AZS_COM2_BUS				));
			m_AzsVC[136]->set_number(AZS_GET(AZS_GROUND_PWR_BUS			));
			m_AzsVC[137]->set_number(AZS_GET(AZS_COPLT_ADI_BUS			));
			m_AzsVC[138]->set_number(AZS_GET(AZS_GMK_COURSE_SYS_BUS		));
			m_AzsVC[139]->set_number(AZS_GET(AZS_HYDRO_SYS_BUS			));
			m_AzsVC[140]->set_number(AZS_GET(AZS_AUTOPILOT_BUS			));
			m_AzsVC[141]->set_number(AZS_GET(AZS_GEAR_IND_BUS			));
			m_AzsVC[142]->set_number(AZS_GET(AZS_SIGNAL_ROCKETS_BUS		));
			m_AzsVC[143]->set_number(AZS_GET(AZS_ILS_SYS_BUS			));
			m_AzsVC[159]->set_number(AZS_GET(AZS_BRAKE_SYS_BUS			));
			m_AzsVC[144]->set_number(AZS_GET(AZS_ADF2_BUS				));
			m_AzsVC[145]->set_number(AZS_GET(AZS_INV_115V_EMERG_SWITCH_BUS));
			m_AzsVC[146]->set_number(AZS_GET(AZS_STAB_EMERG_BUS			));
			m_AzsVC[147]->set_number(AZS_GET(AZS_FLAPS_EMERG_BUS		));
			m_AzsVC[148]->set_number(AZS_GET(AZS_GEAR_EMERG_BUS			));
			m_AzsVC[149]->set_number(AZS_GET(AZS_ANTI_ICE_SYS_BUS		));
			m_AzsVC[150]->set_number(AZS_GET(AZS_EXTREME_BANK_IND_BUS	));
			m_AzsVC[151]->set_number(AZS_GET(AZS_AIR_COND_SYS_BUS		));
			m_AzsVC[152]->set_number(AZS_GET(AZS_STAIRS_SERVO_BUS		));
			m_AzsVC[160]->set_number(AZS_GET(AZS_DECK_HEATER_BUS		));
			m_AzsVC[161]->set_number(AZS_GET(AZS_CABIN_HEATER_BUS		));
			m_AzsVC[162]->set_number(AZS_GET(AZS_TAXI_LIGHT_L_BUS		));
			m_AzsVC[163]->set_number(AZS_GET(AZS_LANDING_LIGHT_L_BUS	));
			m_AzsVC[164]->set_number(AZS_GET(AZS_TAXI_LIGHT_R_BUS		));
			m_AzsVC[165]->set_number(AZS_GET(AZS_LANDING_LIGHT_R_BUS	));
			m_AzsVC[166]->set_number(AZS_GET(AZS_ALER_TRIM_BUS			));
			m_AzsVC[167]->set_number(AZS_GET(AZS_RUDDER_TRIM_BUS		));
			m_AzsVC[168]->set_number(AZS_GET(AZS_STAB_MAIN_BUS			));
			m_AzsVC[169]->set_number(AZS_GET(AZS_FLAPS_MAIN_BUS			));
			m_AzsVC[170]->set_number(AZS_GET(AZS_GEAR_MAIN_BUS			));
			m_AzsVC[171]->set_number(AZS_GET(AZS_AP_PWR					));
			m_AzsVC[172]->set_number(AZS_GET(AZS_AP_PITCH_HOLD			));
			break;
	}

	m_GltVC[0]->set_number(GLT_GET(GLT_SGU6));
	m_GltVC[1]->set_number(GLT_GET(GLT_SGU7));
	m_GltVC[2]->set_number(GLT_GET(GLT_XPDRMODE));
	m_GltVC[3]->set_number(GLT_GET(GLT_TEMP_DUCT));
	m_GltVC[4]->set_number(GLT_GET(GLT_VOLT27));
	m_GltVC[5]->set_number(GLT_GET(GLT_VOLT36));
	m_GltVC[6]->set_number(GLT_GET(GLT_VOLT115));

}
