/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/HTail.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"

class CGear
{
private:
	bool m_Working;
	GEAR_MOVEMENT_TYPE m_GearState[3];

	CNamedVar m_LGearXml;
	CNamedVar m_CGearXml;
	CNamedVar m_RGearXml;

	double m_LPos[2];
	double m_RPos[2];
	double m_CPos[2];

protected:
	static CGear *m_Self;

	CGear() :	m_LGearXml("sd6015_gear_left"),
				m_CGearXml("sd6015_gear_nose"),
				m_RGearXml("sd6015_gear_right")
	{
		m_CGearXml.set_number(16384);
		m_LGearXml.set_number(16384);
		m_RGearXml.set_number(16384);
		m_GearState[0]=MOVEMENT_NONE;
		m_GearState[1]=MOVEMENT_NONE;
		m_GearState[2]=MOVEMENT_NONE;
		m_RetrPlayed=false;
		m_LPos[0]=m_LPos[1]=m_RPos[0]=m_RPos[1]=m_CPos[0]=m_CPos[1]=0;
	};

public:
	bool m_RetrPlayed;

	static CGear *Instance() {
		if(!m_Self) {
			m_Self=new CGear();
		}
		return m_Self;
	};

	static void Release() {
		if(m_Self) {
			delete m_Self;
			m_Self=NULL;
		}
	}

	GEAR_MOVEMENT_TYPE IsLeftGearMovement() {
		GEAR_MOVEMENT_TYPE Ret;

		m_LPos[0]=g_pSDAPI->GetN(GEAR_POS_LEFT);

		if(m_LPos[0]>m_LPos[1]) {
			Ret=MOVEMENT_EXTENDING;
		} else if(m_LPos[0]<m_LPos[1]) {
			Ret=MOVEMENT_RETRACTING;
		} else {
			Ret=MOVEMENT_NONE;
		}

		m_LPos[1]=m_LPos[0];

		return Ret;
	};

	GEAR_MOVEMENT_TYPE IsCenterGearMovement() {
		GEAR_MOVEMENT_TYPE Ret;

		m_CPos[0]=g_pSDAPI->GetN(GEAR_POS_NOSE);

		if(m_CPos[0]>m_CPos[1]) {
			Ret=MOVEMENT_EXTENDING;
		} else if(m_CPos[0]<m_CPos[1]) {
			Ret=MOVEMENT_RETRACTING;
		} else {
			Ret=MOVEMENT_NONE;
		}

		m_CPos[1]=m_CPos[0];

		return Ret;
	};

	GEAR_MOVEMENT_TYPE IsRightGearMovement() {
		GEAR_MOVEMENT_TYPE Ret;

		m_RPos[0]=g_pSDAPI->GetN(GEAR_POS_RIGHT);

		if(m_RPos[0]>m_RPos[1]) {
			Ret=MOVEMENT_EXTENDING;
		} else if(m_RPos[0]<m_RPos[1]) {
			Ret=MOVEMENT_RETRACTING;
		} else {
			Ret=MOVEMENT_NONE;
		}

		m_RPos[1]=m_RPos[0];

		return Ret;
	};

	void Update() {
		m_GearState[0]=IsLeftGearMovement();
		m_GearState[1]=IsCenterGearMovement();
		m_GearState[2]=IsRightGearMovement();

		if(NDL_GET(NDL_MAIN_HYD)>30&&AZS_GET(AZS_GEAR_MAIN_BUS)&&m_GearState[0]==MOVEMENT_RETRACTING&&m_GearState[1]==MOVEMENT_RETRACTING&&m_GearState[2]==MOVEMENT_RETRACTING) {
			if(!m_RetrPlayed) {	
				SND_SET(SND_GEAR_RETRACTING,1);
				m_RetrPlayed=true;
			}
		}

		if(((POS_GET(POS_MAIN_HYDRO_WORK)&&NDL_GET(NDL_MAIN_HYD)>30)&&AZS_GET(AZS_GEAR_MAIN_BUS))||(NDL_GET(NDL_EMERG_HYD)>30&&AZS_GET(AZS_GEAR_EMERG_BUS))) {
			m_CGearXml.set_number(g_pSDAPI->GetN(GEAR_POS_NOSE));
			m_LGearXml.set_number(g_pSDAPI->GetN(GEAR_POS_LEFT));
			m_RGearXml.set_number(g_pSDAPI->GetN(GEAR_POS_RIGHT));

/*
			g_pSimData->pStaticData->ContactFixedData.pContactPointsFixedData[0].Pos.alt=0;
			g_pSimData->pStaticData->ContactFixedData.pContactPointsFixedData[1].Pos.alt=0;
			g_pSimData->pStaticData->ContactFixedData.pContactPointsFixedData[2].Pos.alt=0;
*/
		}

		FrontLeg();
	};

	inline GEAR_MOVEMENT_TYPE GetState(int idx) const { return m_GearState[idx]; };

	double GetPos(int idx) {if(idx==0)return m_LGearXml.get_number();if(idx==1)return m_CGearXml.get_number();if(idx==2)return m_RGearXml.get_number();return 0;}

	void SetNoseLegMaxSteerAngle(double val) {
		Sim1_main_class *sim1_class=(Sim1_main_class *)g_SimModules->m_Sim1->UserAircraftGet();
		if(sim1_class) {
			PSIM_DATA sim1_data=sim1_class->sim1_data;
			sim1_data->pStaticData->ContactFixedData->MaxSteerAngle=dgrd(val);
		}

	};

	void FrontLeg() {
		if(g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
			if(NDL_GET(NDL_MAIN_HYD)>30&&AZS_GET(AZS_NOSE_LEG_STEER_BUS)&&PWR_GET(PWR_BUS27)) {
				SetNoseLegMaxSteerAngle(55);
			} else {
				SetNoseLegMaxSteerAngle(0);
			}
		} else {
			SetNoseLegMaxSteerAngle(55);
		}
	};
};
