/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/Engines.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Engines.h"

#define SECOND			18
#define MINUTE			18*60

#define TIME_TO_LIGHT_PK	2*SECOND
#define	ENG_START			SECOND
#define ENG_AIR_CLOSE		21*SECOND
#define ENG_STARTED  		32*SECOND

#define SOUND_SPINING			2
#define SOUND_RPM_RAISE			5*SECOND
#define SOUND_OILPRESS_RAISE	8*SECOND
#define SOUND_FIREING			15*SECOND
#define SOUND_TEMP_RAISE		19*SECOND
#define SOUND_SV_CLOSE			23*SECOND
#define SOUND_TEMP_OIL_NORMAL	27*SECOND
#define SOUND_STARTED			31*SECOND

CEngine::CEngine(int num)
{
	eng_idx=num;
	fEGTToAmientAirCorrelationCoeff=0.3;
}

void CEngine::Update()
{
	switch(eng_idx) {
		case 1:
			egt			= (g_pSDAPI->GetN(GENERAL_ENGINE1_EGT)-459.67-32)*5/9+((-273-g_pSDAPI->GetN(AMBIENT_TEMP_DEGREES_C))*fEGTToAmientAirCorrelationCoeff);
			oil_psi		= g_SimData->GetParam(SIMPARAM_GENERAL_ENG_OIL_PRESSURE1)/10;
			oil_temp	= g_SimData->GetParam(SIMPARAM_GENERAL_ENG_OIL_TEMPERATURE1);
			fuel_psi	= g_SimData->GetParam(SIMPARAM_GENERAL_ENG_FUEL_PRESSURE1);
			throttle	= g_pSDAPI->GetN(GENERAL_ENGINE1_THROTTLE_LEVER_POS);
			rpm_n1		= g_pSDAPI->GetN(TURB_ENGINE_1_CORRECTED_N1);
			rpm_n2		= g_pSDAPI->GetN(TURB_ENGINE_1_CORRECTED_N2);
		break;
		case 2:
			egt			= (g_pSDAPI->GetN(GENERAL_ENGINE2_EGT)-459.67-32)*5/9+((-273-g_pSDAPI->GetN(AMBIENT_TEMP_DEGREES_C))*fEGTToAmientAirCorrelationCoeff);
			rpm_n1		= g_pSDAPI->GetN(TURB_ENGINE_2_CORRECTED_N1);
			rpm_n2		= g_pSDAPI->GetN(TURB_ENGINE_2_CORRECTED_N2);
			oil_psi		= g_SimData->GetParam(SIMPARAM_GENERAL_ENG_OIL_PRESSURE2)/10;
			oil_temp	= g_SimData->GetParam(SIMPARAM_GENERAL_ENG_OIL_TEMPERATURE2);
			fuel_psi	= g_SimData->GetParam(SIMPARAM_GENERAL_ENG_FUEL_PRESSURE2);
			throttle	= g_pSDAPI->GetN(GENERAL_ENGINE2_THROTTLE_LEVER_POS);
			break;
		case 3:
			egt			= (g_pSDAPI->GetN(GENERAL_ENGINE3_EGT)-459.67-32)*5/9+((-273-g_pSDAPI->GetN(AMBIENT_TEMP_DEGREES_C))*fEGTToAmientAirCorrelationCoeff);
			rpm_n1		= g_pSDAPI->GetN(TURB_ENGINE_3_CORRECTED_N1);
			rpm_n2		= g_pSDAPI->GetN(TURB_ENGINE_3_CORRECTED_N2);
			oil_psi		= g_SimData->GetParam(SIMPARAM_GENERAL_ENG_OIL_PRESSURE3)/10;
			oil_temp	= g_SimData->GetParam(SIMPARAM_GENERAL_ENG_OIL_TEMPERATURE3);
			fuel_psi	= g_SimData->GetParam(SIMPARAM_GENERAL_ENG_FUEL_PRESSURE3);
			throttle	= g_pSDAPI->GetN(GENERAL_ENGINE3_THROTTLE_LEVER_POS);
			break;
	}
	if(egt<0)egt=0;
}

void CEngine::SetRudPos(int *pos,int apipos,bool rtuworking)
{
	if(rtuworking) {
		*pos=14;
		return;
	}
	if(POS_GET(apipos)) *pos=0;
	else if ( throttle >= 0.000 && throttle < 0.090 ) *pos	= 4;
	else if ( throttle >= 0.090 && throttle < 0.180 ) *pos	= 5;
	else if ( throttle >= 0.180 && throttle < 0.270 ) *pos	= 6;
	else if ( throttle >= 0.270 && throttle < 0.360 ) *pos	= 7;
	else if ( throttle >= 0.360 && throttle < 0.450 ) *pos	= 8;
	else if ( throttle >= 0.450 && throttle < 0.540 ) *pos	= 9;
	else if ( throttle >= 0.540 && throttle < 0.650 ) *pos	= 10;

	else if ( throttle >= 0.650 && throttle < 0.670 ) *pos	= 11;
	else if ( throttle >= 0.670 && throttle < 0.710 ) *pos	= 12;
	else if ( throttle >= 0.710 && throttle < 0.750 ) *pos	= 12;
	else if ( throttle >= 0.750 && throttle < 0.770 ) *pos	= 14;

	else if ( throttle >= 0.770 && throttle < 0.830 ) *pos	= 15;
	else if ( throttle >= 0.830 && throttle < 0.890 ) *pos	= 16;
	else if ( throttle >= 0.890 && throttle < 0.920 ) *pos	= 17;
	else if ( throttle >= 0.920 && throttle < 0.950 ) *pos	= 18;
	else if ( throttle >= 0.950 )					  *pos	= 19;
}

//////////////////////////////////////////////////////////////////////////

SDSystemEngines::SDSystemEngines() : m_Eng1(1), m_Eng2(2), m_Eng3(3)
{
}

SDSystemEngines::~SDSystemEngines()
{
}

void SDSystemEngines::Init()
{
	xml11=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_508"));
	xml12=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_509"));
	xml13=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_510"));
	xml14=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_517"));
	xml15=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_518"));
	xml16=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_524"));

	xml21=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_511"));
	xml22=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_512"));
	xml23=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_513"));
	xml24=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_519"));
	xml25=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_520"));
	xml26=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_525"));

	xml31=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_514"));
	xml32=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_515"));
	xml33=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_516"));
	xml34=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_521"));
	xml35=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_522"));
	xml36=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_526"));

	xmlRudStop1=auto_ptr<CNamedVar>(new CNamedVar("RudStop1"));
	xmlRudStop2=auto_ptr<CNamedVar>(new CNamedVar("RudStop2"));
	xmlRudStop3=auto_ptr<CNamedVar>(new CNamedVar("RudStop3"));

	VCAnimE1N1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng1_N1"));
	VCAnimE1N2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng1_N2"));
	VCAnimE2N1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng2_N1"));
	VCAnimE2N2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng2_N2"));
	VCAnimE3N1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng3_N1"));
	VCAnimE3N2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng3_N2"));

	VCAnimE1EGT=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng1_egt"));
	VCAnimE2EGT=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng2_egt"));
	VCAnimE3EGT=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng3_egt"));

	VCAnimE1PO=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng1_oil_psi"));
	VCAnimE1PF=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng1_fuel_psi"));
	VCAnimE1TO=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng1_oil_temp"));

	VCAnimE2PO=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng2_oil_psi"));
	VCAnimE2PF=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng2_fuel_psi"));
	VCAnimE2TO=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng2_oil_temp"));

	VCAnimE3PO=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng3_oil_psi"));
	VCAnimE3PF=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng3_fuel_psi"));
	VCAnimE3TO=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_eng3_oil_temp"));

	m_Airstarter=CAirStarter::Instance();
	m_RTU=CRTU::Instance();
	m_PDAWorking=false;
	m_TestStart=false;
	m_StopRequest[0]=false;
	m_StopRequest[1]=false;
	m_StopRequest[2]=false;

	AZS_SET(AZS_PKAI251,1);
	AZS_SET(AZS_PKAI252,1);
	AZS_SET(AZS_PKAI253,1);

	m_SoundTimer=0;
	m_SoundTimerForced=false;
}

void SDSystemEngines::Update()
{
	if(m_SoundTimerForced)
		m_SoundTimer++;

	GetAllParams();

	CheckAll();
	StartUp();
	UpdateNeedles();
	SoundProcess();

	SetAllParams();

	xml11->set_number(m_Eng1.fuel_psi);
	xml12->set_number(m_Eng1.oil_psi);
	xml13->set_number(m_Eng1.oil_temp);
	xml14->set_number(m_Eng1.rpm_n1);
	xml15->set_number(m_Eng1.rpm_n2);
	xml16->set_number(m_Eng1.egt);

	xml21->set_number(m_Eng2.fuel_psi);
	xml22->set_number(m_Eng2.oil_psi);
	xml23->set_number(m_Eng2.oil_temp);
	xml24->set_number(m_Eng2.rpm_n1);
	xml25->set_number(m_Eng2.rpm_n2);
	xml26->set_number(m_Eng2.egt);

	xml31->set_number(m_Eng3.fuel_psi);
	xml32->set_number(m_Eng3.oil_psi);
	xml33->set_number(m_Eng3.oil_temp);
	xml34->set_number(m_Eng3.rpm_n1);
	xml35->set_number(m_Eng3.rpm_n2);
	xml36->set_number(m_Eng3.egt);

	xmlRudStop1->set_number(POS_GET(POS_RUD1_STOP));
	xmlRudStop2->set_number(POS_GET(POS_RUD2_STOP));
	xmlRudStop3->set_number(POS_GET(POS_RUD3_STOP));

	if(!m_SoundTimerForced)
		return;

	switch(m_SoundTimer) {
		case SOUND_SPINING				:
			if(m_EngineStartSelectSwitch==1)
				SND_SET(SND_ENG1_SPINING,1);
			else if(m_EngineStartSelectSwitch==2)
				SND_SET(SND_ENG2_SPINING,1);
			else if(m_EngineStartSelectSwitch==3)
				SND_SET(SND_ENG3_SPINING,1);
		break;
		case SOUND_RPM_RAISE			:
			if(m_EngineStartSelectSwitch==1)
				SND_SET(SND_ENG1_RPM_RAISE,1);
			else if(m_EngineStartSelectSwitch==2)
				SND_SET(SND_ENG2_RPM_RAISE,1);
			else if(m_EngineStartSelectSwitch==3)
				SND_SET(SND_ENG3_RPM_RAISE,1);
		break;
		case SOUND_OILPRESS_RAISE		:
			if(m_EngineStartSelectSwitch==1)
				SND_SET(SND_ENG1_OILPRESS_RAISE,1);
			else if(m_EngineStartSelectSwitch==2)
				SND_SET(SND_ENG2_OILPRESS_RAISE,1);
			else if(m_EngineStartSelectSwitch==3)
				SND_SET(SND_ENG3_OILPRESS_RAISE,1);
	    break;
		case SOUND_FIREING				:
			if(!m_TestStart) {
				if(m_EngineStartSelectSwitch==1)
					SND_SET(SND_ENG1_FIREING,1);
				else if(m_EngineStartSelectSwitch==2)
					SND_SET(SND_ENG2_FIREING,1);
				else if(m_EngineStartSelectSwitch==3)
					SND_SET(SND_ENG3_FIREING,1);
			}
	    break;
		case SOUND_TEMP_RAISE			:
			if(!m_TestStart) {
				if(m_EngineStartSelectSwitch==1)
					SND_SET(SND_ENG1_TEMP_RAISE,1);
				else if(m_EngineStartSelectSwitch==2)
					SND_SET(SND_ENG2_TEMP_RAISE,1);
				else if(m_EngineStartSelectSwitch==3)
					SND_SET(SND_ENG3_TEMP_RAISE,1);
			}
		break;
		case SOUND_SV_CLOSE				:
			if(!m_TestStart) {
				if(m_EngineStartSelectSwitch==1)
					SND_SET(SND_ENG1_SV_CLOSE,1);
				else if(m_EngineStartSelectSwitch==2)
					SND_SET(SND_ENG2_SV_CLOSE,1);
				else if(m_EngineStartSelectSwitch==3)
					SND_SET(SND_ENG3_SV_CLOSE,1);
			}
		break;
		case SOUND_TEMP_OIL_NORMAL		:
			if(!m_TestStart) {
				if(m_EngineStartSelectSwitch==1)
					SND_SET(SND_ENG1_TEMP_OIL_NORMAL,1);
				else if(m_EngineStartSelectSwitch==2)
					SND_SET(SND_ENG2_TEMP_OIL_NORMAL,1);
				else if(m_EngineStartSelectSwitch==3)
					SND_SET(SND_ENG3_TEMP_OIL_NORMAL,1);
			}
	    break;
		case SOUND_STARTED				:
			if(!m_TestStart) {
				if(m_EngineStartSelectSwitch==1)
					SND_SET(SND_ENG1_STARTED,1);
				else if(m_EngineStartSelectSwitch==2)
					SND_SET(SND_ENG2_STARTED,1);
				else if(m_EngineStartSelectSwitch==3)
					SND_SET(SND_ENG3_STARTED,1);

				m_SoundTimerForced=false;
			}
	    break;
	}

}

void SDSystemEngines::Load(CIniFile *ini)
{
	m_PK[0].Load(ini,"PK1");
	m_PK[1].Load(ini,"PK2");
	m_PK[2].Load(ini,"PK3");
}

void SDSystemEngines::Save(CIniFile *ini)
{
	m_PK[0].Save(ini,"PK1");
	m_PK[1].Save(ini,"PK2");
	m_PK[2].Save(ini,"PK3");
}

void SDSystemEngines::GetAllParams()
{
	m_PK[0].m_aPK=(int)AZS_GET(AZS_PKAI251);
	m_PK[1].m_aPK=(int)AZS_GET(AZS_PKAI252);
	m_PK[2].m_aPK=(int)AZS_GET(AZS_PKAI253);
	m_PK[0].m_aPKChanged=(int)AZS_CHG(AZS_PKAI251);
	m_PK[1].m_aPKChanged=(int)AZS_CHG(AZS_PKAI252);
	m_PK[2].m_aPKChanged=(int)AZS_CHG(AZS_PKAI253);
	m_PK[0].m_aPKPrev=AZS_GETPREV(AZS_PKAI251);
	m_PK[1].m_aPKPrev=AZS_GETPREV(AZS_PKAI252);
	m_PK[2].m_aPKPrev=AZS_GETPREV(AZS_PKAI253);
	m_EngineStartMasterSwitch=(int)AZS_GET(AZS_STARTAI25PWR);
	//m_AmbientTemp=;
	//m_EngineStartHeaterSwitch=
	switch((int)AZS_GET(AZS_STARTAI25SEL)) {
		case 2:
			m_EngineStartSelectSwitch=1;
		break;
		case 1:
			m_EngineStartSelectSwitch=2;
		break;
		case 3:
			m_EngineStartSelectSwitch=3;
		break;
		default:
			m_EngineStartSelectSwitch=0;
	}
	switch((int)AZS_GET(AZS_STARTAI25MODE)) {
		case 2:
			m_EngineStartModeSwitch=1;
		break;
		case 1:
			m_EngineStartModeSwitch=0;
		break;
		default:
			m_EngineStartModeSwitch=-1;
	}
	m_EngineN2[0]=g_pSDAPI->GetN(TURB_ENGINE_1_CORRECTED_N2);
	m_EngineN2[1]=g_pSDAPI->GetN(TURB_ENGINE_2_CORRECTED_N2);
	m_EngineN2[2]=g_pSDAPI->GetN(TURB_ENGINE_3_CORRECTED_N2);

	m_BtnStart=BTN_GET(BTN_STARTAI25);
	m_BtnStop=BTN_GET(BTN_STOPAI25);
	m_BtnStartChanged=BTN_CHG(BTN_STARTAI25);
	m_BtnStopChanged=BTN_CHG(BTN_STOPAI25);

	m_BtnStartInAir[0]=BTN_GET(BTN_ENGSTART1);
	m_BtnStartInAir[1]=BTN_GET(BTN_ENGSTART2);
	m_BtnStartInAir[2]=BTN_GET(BTN_ENGSTART3);

}

void SDSystemEngines::SetAllParams()
{
}

void SDSystemEngines::CheckAll()
{
	m_PK[0].Check(LMP_ENG1PK,TIME_TO_LIGHT_PK,SND_FUEL_AI25_1);
	m_PK[1].Check(LMP_ENG2PK,TIME_TO_LIGHT_PK,SND_FUEL_AI25_2);
	m_PK[2].Check(LMP_ENG3PK,TIME_TO_LIGHT_PK,SND_FUEL_AI25_3);

	if(m_PK[0].m_bPK&&AZS_GET(AZS_ENG1_FUEL_VALVE_BUS)&&AZS_GET(AZS_ENG1_IGNIT_BUS)&&PWR_GET(PWR_BUS27)&&(AZS_GET(AZS_EMERG_ENG1_CUT)==0||AZS_GET(AZS_EMERG_ENG1_CUT)==1)/*&&m_Airstarter->GetAir()>2*/&&!m_StopRequest[0]&&!POS_GET(POS_RUD1_STOP)) {
		if(!m_TestStart) {
			POS_SET(POS_ENGINE1_MIXTURE,16383);
			send_key_event(KEY_MIXTURE1_SET,(UINT32)POS_GET(POS_ENGINE1_MIXTURE));
		}
	} else {
		trigger_key_event(KEY_MIXTURE1_LEAN,0);
	}
	if(m_PK[1].m_bPK&&AZS_GET(AZS_ENG2_FUEL_VALVE_BUS)&&AZS_GET(AZS_ENG2_IGNIT_BUS)&&PWR_GET(PWR_BUS27)&&(AZS_GET(AZS_EMERG_ENG2_CUT)==0||AZS_GET(AZS_EMERG_ENG2_CUT)==1)/*&&m_Airstarter->GetAir()>2*/&&!m_StopRequest[1]&&!POS_GET(POS_RUD2_STOP)) {
		if(!m_TestStart) {
			POS_SET(POS_ENGINE2_MIXTURE,16383);
			trigger_key_event(KEY_MIXTURE2_SET,(UINT32)POS_GET(POS_ENGINE2_MIXTURE));
		}
	} else {
		trigger_key_event(KEY_MIXTURE2_LEAN,0);
	}
	if(m_PK[2].m_bPK&&AZS_GET(AZS_ENG3_FUEL_VALVE_BUS)&&AZS_GET(AZS_ENG3_IGNIT_BUS)&&PWR_GET(PWR_BUS27)&&(AZS_GET(AZS_EMERG_ENG3_CUT)==0||AZS_GET(AZS_EMERG_ENG3_CUT)==1)/*&&m_Airstarter->GetAir()>2*/&&!m_StopRequest[2]&&!POS_GET(POS_RUD3_STOP)) {
		if(!m_TestStart) {
			POS_SET(POS_ENGINE3_MIXTURE,16383);
			trigger_key_event(KEY_MIXTURE3_SET,(UINT32)POS_GET(POS_ENGINE3_MIXTURE));
		}
	} else {
		trigger_key_event(KEY_MIXTURE3_LEAN,0);
	}
}

void SDSystemEngines::StartUp()
{
	if(m_BtnStart&&g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
		StartEngine();
	} 
	if(m_BtnStartInAir[0]&&!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
		StartEngineInAir(1);
	} 
	if(m_BtnStartInAir[1]&&!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
		StartEngineInAir(2);
	} 
	if(m_BtnStartInAir[2]&&!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
		StartEngineInAir(3);
	} 
	if(m_BtnStopChanged) {
		m_SoundTimerForced=false;
		if(m_BtnStop) {
			switch (m_EngineStartSelectSwitch) {
				case 1:
					m_StopRequest[0]=true;
				break;
				case 2:
					m_StopRequest[1]=true;
				break;
				case 3:
					m_StopRequest[2]=true;
				break;
				default:
					m_StopRequest[0]=false;
					m_StopRequest[1]=false;
					m_StopRequest[2]=false;
			}
		}
	}
	if (m_EngineN2[0] < 10&&m_StopRequest[0]) {
		m_StopRequest[0]=false;
		m_Airstarter->RequestAir(0,AIR_REQUESTER_ENG);
		TBL_SET(TBL_APU_STARTER,0);
		TBL_SET(TBL_AIR_STARTER_ON,0);
	}
	if (m_EngineN2[1] < 10&&m_StopRequest[1]) {
		m_StopRequest[1]=false;
		m_Airstarter->RequestAir(0,AIR_REQUESTER_ENG);
		TBL_SET(TBL_APU_STARTER,0);
		TBL_SET(TBL_AIR_STARTER_ON,0);
	}
	if (m_EngineN2[2] < 10&&m_StopRequest[2]) {
		m_StopRequest[2]=false;
		m_Airstarter->RequestAir(0,AIR_REQUESTER_ENG);
		TBL_SET(TBL_APU_STARTER,0);
		TBL_SET(TBL_AIR_STARTER_ON,0);
	}
	if (m_PDAWorking) {
		if (!PWR_GET(PWR_BUS27))
			m_PDAWorking = FALSE; // ��������� ��� ��� ���������� �������
		switch (m_EngineStarting) {
				case 1:
					if (m_EngineN2[0] < 50&&!m_StopRequest[0]) {
						//trigger_key_event (KEY_TOGGLE_STARTER1, 0);
						m_Airstarter->RequestAir(2,AIR_REQUESTER_ENG);
						TBL_SET(TBL_APU_STARTER,1);
						TBL_SET(TBL_AIR_STARTER_ON,1);
					} else {
						m_Airstarter->RequestAir(0,AIR_REQUESTER_ENG);
						TBL_SET(TBL_APU_STARTER,0);
						TBL_SET(TBL_AIR_STARTER_ON,0);
					}
					break;
				case 2:
					if (m_EngineN2[1] < 50&&!m_StopRequest[1]) {
						//trigger_key_event (KEY_TOGGLE_STARTER2, 0);
						m_Airstarter->RequestAir(2,AIR_REQUESTER_ENG);
						TBL_SET(TBL_APU_STARTER,1);
						TBL_SET(TBL_AIR_STARTER_ON,1);
					} else {
						m_Airstarter->RequestAir(0,AIR_REQUESTER_ENG);
						TBL_SET(TBL_APU_STARTER,0);
						TBL_SET(TBL_AIR_STARTER_ON,0);
					}
					break;
				case 3:
					if (m_EngineN2[2] < 50&&!m_StopRequest[2]) {
						//trigger_key_event (KEY_TOGGLE_STARTER3, 0);
						m_Airstarter->RequestAir(2,AIR_REQUESTER_ENG);
						TBL_SET(TBL_APU_STARTER,1);
						TBL_SET(TBL_AIR_STARTER_ON,1);
					} else {
						m_Airstarter->RequestAir(0,AIR_REQUESTER_ENG);
						TBL_SET(TBL_APU_STARTER,0);
						TBL_SET(TBL_AIR_STARTER_ON,0);
					}
					break;
		}

		if (m_EngineN2[0]>50||m_EngineN2[1]>50||m_EngineN2[2]>50) {
			m_Airstarter->RequestAir(0,AIR_REQUESTER_ENG);
			TBL_SET(TBL_APU_STARTER,0);
			TBL_SET(TBL_AIR_STARTER_ON,0);
			m_PDAWorking = FALSE;
		}

	}

}

void SDSystemEngines::StartEngine() 
{
	BOOL bOK = TRUE;
	if (g_pSDAPI->GetN(AIRCRAFT_ON_GROUND) && m_EngineStartMasterSwitch && !m_PDAWorking) {
		// ������ � �������� ��������

		// ��������� ����� ������� �� ��� ��� �� ������ ���������
		if (m_Airstarter->GetAir()<1.8)
			bOK = FALSE;
/*
		if (m_AmbientTemp < 6) {
			// ��� ������ ������������ ��������� ������� ���������� ����������
			if (!m_EngineStartHeaterSwitch)
				bOK = FALSE;
		}
		*/
		// ����������� ��������� ������ ���� ����������
		// � ������������� ������ ��������� ������ ���� � ��������� ���������
		switch (m_EngineStartSelectSwitch) {
			case 1:
				if (m_EngineN2[0] > 15)
					bOK = FALSE;
			break;
			case 2:
				if (m_EngineN2[1] > 15)
					bOK = FALSE;
			break;
			case 3:
				if (m_EngineN2[2] > 15)
					bOK = FALSE;
			break;
			default:
				bOK = FALSE;
			break;
		}


		if (bOK) {
			if(!m_SoundTimerForced) {
				m_SoundTimerForced=true;
				m_SoundTimer=0;
			}
			// ���������� ������ / �������� ���������
			if (m_EngineStartModeSwitch==1) {
				_lStartEngine (m_EngineStartSelectSwitch, 1);
			} else if (m_EngineStartModeSwitch==0) {
				_lStartEngine (m_EngineStartSelectSwitch, 0);
			}
		}
	}
}

void SDSystemEngines::StartEngineInAir(int index) 
{
	BOOL bOK = FALSE;
	if (!g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)) {
		switch (index) {
			case 1:
				if (m_EngineN2[0] < 50)
					bOK  = TRUE;
			break;
			case 2:
				if (m_EngineN2[1] < 50)
					bOK  = TRUE;
			break;
			case 3:
				if (m_EngineN2[2] < 50)
					bOK  = TRUE;
			break;
		}
	}
	if (bOK)
		_lStartEngine (index, 1);
}

// ������ ��������� � ������������ ������� (������� ������� ���������)
// ������:
// 0 - �������� ���������
// 1 - ������
void SDSystemEngines::_lStartEngine (int index, int mode) 
{
	m_EngineStarting = index;
	m_PDAWorking = TRUE;
	if(!mode)
		m_TestStart=true;
	else
		m_TestStart=false;
	
	switch (index) {
		case 1:
			if (!mode)
				trigger_key_event (KEY_MIXTURE1_LEAN, 0);
			trigger_key_event (KEY_TOGGLE_STARTER1, 0);
		break;
		case 2:
			if (!mode)
				trigger_key_event (KEY_MIXTURE2_LEAN, 0);
			trigger_key_event (KEY_TOGGLE_STARTER2, 0);
		break;
		case 3:
			if (!mode)
				trigger_key_event (KEY_MIXTURE3_LEAN, 0);
			trigger_key_event (KEY_TOGGLE_STARTER3, 0);
		break;
	}

}

void SDSystemEngines::SetRudPos()
{
	int r1,r2,r3;
	m_Eng1.SetRudPos(&r1,POS_RUD1_STOP,false);
	m_Eng2.SetRudPos(&r2,POS_RUD2_STOP,m_RTU->IsWorking());
	m_Eng3.SetRudPos(&r3,POS_RUD3_STOP,false);

	POS_SET(POS_RUD1,r1);
	POS_SET(POS_RUD2,r2);
	POS_SET(POS_RUD3,r3);
}

#define PROCESS_ENG(N) /**/ \
	if(AZS_GET(AZS_ENG##N##_IND_BUS)&&PWR_GET(PWR_BUS27)) { \
	NDL_SET(NDL_ENG##N##_TEMP			, m_Eng##N.egt); \
	NDL_SET(NDL_ENG##N##_N1				, m_Eng##N.rpm_n1); \
	NDL_SET(NDL_ENG##N##_N2				, m_Eng##N.rpm_n2); \
	NDL_SET(NDL_ENG##N##_THROTTLE_POS	, m_Eng##N.throttle); \
	} else { \
	NDL_SET(NDL_ENG##N##_TEMP			, 0); \
	NDL_SET(NDL_ENG##N##_N1				, 0); \
	NDL_SET(NDL_ENG##N##_N2				, 0); \
	NDL_SET(NDL_ENG##N##_THROTTLE_POS	, 0); \
	}

#define PROCESS_ENG_EMI(N) /**/ \
	if(AZS_GET(AZS_ENG##N##_IND_BUS)&&AZS_GET(AZS_MANOM)&&PWR_GET(PWR_BUS27)) { \
	NDL_SET(NDL_ENG##N##_OIL_PSI		, m_Eng##N.oil_psi); \
	NDL_SET(NDL_ENG##N##_OIL_TEMP		, m_Eng##N.oil_temp); \
	NDL_SET(NDL_ENG##N##_FUEL_PSI		, m_Eng##N.fuel_psi); \
	} else { \
	NDL_SET(NDL_ENG##N##_OIL_PSI		, 0); \
	NDL_SET(NDL_ENG##N##_OIL_TEMP		, 0); \
	NDL_SET(NDL_ENG##N##_FUEL_PSI		, 0); \
	}

void SDSystemEngines::UpdateNeedles()
{
	m_Eng1.Update();
	m_Eng2.Update();
	m_Eng3.Update();

	PROCESS_ENG(1)
	PROCESS_ENG(2)
	PROCESS_ENG(3)

	PROCESS_ENG_EMI(1)
	PROCESS_ENG_EMI(2)
	PROCESS_ENG_EMI(3)

	VCAnimE1N1->set_degree(dgrd((NDL_GET(NDL_ENG1_N1)*3)+1.));
	VCAnimE1N2->set_degree(dgrd((NDL_GET(NDL_ENG1_N2)*3)+1.));
	VCAnimE2N1->set_degree(dgrd((NDL_GET(NDL_ENG2_N1)*3)+1.));
	VCAnimE2N2->set_degree(dgrd((NDL_GET(NDL_ENG2_N2)*3)+1.));
	VCAnimE3N1->set_degree(dgrd((NDL_GET(NDL_ENG3_N1)*3)+1.));
	VCAnimE3N2->set_degree(dgrd((NDL_GET(NDL_ENG3_N2)*3)+1.));

	VCAnimE1EGT->set_degree(dgrd(NDL_GET(NDL_ENG1_TEMP)*0.273));
	VCAnimE2EGT->set_degree(dgrd(NDL_GET(NDL_ENG2_TEMP)*0.273));
	VCAnimE3EGT->set_degree(dgrd(NDL_GET(NDL_ENG3_TEMP)*0.273));

	VCAnimE1PO->set_degree(dgrd(NDL_GET(NDL_ENG1_OIL_PSI)*-16.25));
	VCAnimE1PF->set_degree(dgrd(NDL_GET(NDL_ENG1_FUEL_PSI)*1.38));
	VCAnimE1TO->set_degree(dgrd(NDL_GET(NDL_ENG1_OIL_TEMP)*0.6));

	VCAnimE2PO->set_degree(dgrd(NDL_GET(NDL_ENG2_OIL_PSI)*-16.25));
	VCAnimE2PF->set_degree(dgrd(NDL_GET(NDL_ENG2_FUEL_PSI)*1.38));
	VCAnimE2TO->set_degree(dgrd(NDL_GET(NDL_ENG2_OIL_TEMP)*0.6));

	VCAnimE3PO->set_degree(dgrd(NDL_GET(NDL_ENG3_OIL_PSI)*-16.25));
	VCAnimE3PF->set_degree(dgrd(NDL_GET(NDL_ENG3_FUEL_PSI)*1.38));
	VCAnimE3TO->set_degree(dgrd(NDL_GET(NDL_ENG3_OIL_TEMP)*0.6));

	SetRudPos();
}

/*
������ ��� ������� ����������� � ��� RPM ����� ��������� 0 � ��� �������� 
��������� ����� � ��� ��� �� ����� � ��� ��������� ���� ������ ��� ������� �� ������ �������
*/

void SDSystemEngines::SoundProcess()
{
	if(AZS_CHG(AZS_STARTAI25SEL)||AZS_CHG(AZS_STARTAI25MODE)||AZS_CHG(AZS_STARTAI25PWR)) {
		double rud=0;
		int eng=0;
		switch(m_EngineStartSelectSwitch) {
			case 1:
				rud=m_Eng1.throttle;
				eng=1;
			break;
			case 2:
				rud=m_Eng2.throttle;
				eng=2;
			break;
			case 3:
				rud=m_Eng3.throttle;
				eng=3;
			break;
			default:
				rud=0;
		}
		int mode=AZS_GET(AZS_STARTAI25MODE);
		if(m_EngineStartSelectSwitch>0&&mode==2&&m_EngineStartMasterSwitch&&g_pSDAPI->GetN(PARKING_BRAKE_POS)>0&&m_EngineN2[m_EngineStartSelectSwitch-1]<1&&rud<0.01&&m_PK[m_EngineStartSelectSwitch-1].m_bPK) {
			switch(eng)	{
				case 1:
					SND_SET(SND_RUD1_MG,1);
				break;
				case 2:
					SND_SET(SND_RUD2_MG,1);
				break;
				case 3:
					SND_SET(SND_RUD3_MG,1);
				break;
			}
		}
	}
}

