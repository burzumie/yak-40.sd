/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Flaps.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Flaps.h"

CFlaps *CFlaps::m_Self=NULL;
double CFlaps::m_Min=0;
double CFlaps::m_Max=35;
double CFlaps::m_Step=0.1;
double CFlaps::m_HydLimit=30;
