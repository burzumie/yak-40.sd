/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/Engines.h $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"
#include "AirStarter.h"
#include "RTU.h"
#include "../../Lib/EString.h"
#include "../../Lib/NamedVar.h"

class CEngine 
{
private:
	int eng_idx;
	double fEGTToAmientAirCorrelationCoeff;

public:
	double egt;
	double rpm_n1,rpm_n2;
	double oil_psi,oil_temp,fuel_psi;
	double throttle;

	CEngine(int num);

	void Update();

	void SetRudPos(int *pos,int apipos,bool rtuworking);
	inline double get_rud_pct() const { return throttle; };

};

class SDSystemEngines : public SDLogicBase
{
private:
	CEngine m_Eng1;
	CEngine m_Eng2;
	CEngine m_Eng3;

	auto_ptr<CNamedVar> xml11;
	auto_ptr<CNamedVar> xml12;
	auto_ptr<CNamedVar> xml13;
	auto_ptr<CNamedVar> xml14;
	auto_ptr<CNamedVar> xml15;
	auto_ptr<CNamedVar> xml16;

	auto_ptr<CNamedVar> xml21;
	auto_ptr<CNamedVar> xml22;
	auto_ptr<CNamedVar> xml23;
	auto_ptr<CNamedVar> xml24;
	auto_ptr<CNamedVar> xml25;
	auto_ptr<CNamedVar> xml26;

	auto_ptr<CNamedVar> xml31;
	auto_ptr<CNamedVar> xml32;
	auto_ptr<CNamedVar> xml33;
	auto_ptr<CNamedVar> xml34;
	auto_ptr<CNamedVar> xml35;
	auto_ptr<CNamedVar> xml36;

	auto_ptr<CNamedVar> xmlRudStop1;
	auto_ptr<CNamedVar> xmlRudStop2;
	auto_ptr<CNamedVar> xmlRudStop3;

	auto_ptr<CNamedVar> VCAnimE1N1;
	auto_ptr<CNamedVar> VCAnimE1N2;
	auto_ptr<CNamedVar> VCAnimE2N1;
	auto_ptr<CNamedVar> VCAnimE2N2;
	auto_ptr<CNamedVar> VCAnimE3N1;
	auto_ptr<CNamedVar> VCAnimE3N2;
	auto_ptr<CNamedVar> VCAnimE1EGT;
	auto_ptr<CNamedVar> VCAnimE2EGT;
	auto_ptr<CNamedVar> VCAnimE3EGT;
	auto_ptr<CNamedVar> VCAnimE1PO;
	auto_ptr<CNamedVar> VCAnimE1PF;
	auto_ptr<CNamedVar> VCAnimE1TO;
	auto_ptr<CNamedVar> VCAnimE2PO;
	auto_ptr<CNamedVar> VCAnimE2PF;
	auto_ptr<CNamedVar> VCAnimE2TO;
	auto_ptr<CNamedVar> VCAnimE3PO;
	auto_ptr<CNamedVar> VCAnimE3PF;
	auto_ptr<CNamedVar> VCAnimE3TO;

private:
	struct PK 
	{
		int m_aPK;					// �������� �������� ������ ���������� � ������� �����
		int m_aPKChanged;			// ������� ��������� ��������� ���������
		int m_bPK;					// ������� ��������� �������� ������
		int m_TimerPK;				// ������ ��� ��������� ����
		int m_TimerPKReseted;		// ���� ������ ������� ��� ��������� ����
		int m_PKState;				// ���� ��������� ��� ���������� �� (0 - ������, 1 - ��������, 2 - ���������)
		int m_aPKPrev;				// ���������� ��������� ��������. ��� �������
		bool m_OpenPlayed;
		bool m_ClosePlayed;
		PK() {
			m_aPK=0;
			m_aPKChanged=false;
			m_bPK=false;
			m_TimerPK=0;
			m_TimerPKReseted=false;
			m_PKState=0;
			m_OpenPlayed=false;
			m_ClosePlayed=false;
		};
		void Check(LMP lmp,int ttl,int snd) {
			if(m_aPKChanged) {
				if(m_aPK==2||m_aPK==3) {
					m_PKState=1;
				} else {
					m_PKState=2;
				}
			}

			if(m_PKState==1) {
				if(!m_TimerPKReseted) {
					m_TimerPK=0;
					m_TimerPKReseted=true;
				} else {
					if(!m_OpenPlayed) {	
						SND_SET(snd,1);
						m_OpenPlayed=true;
						m_ClosePlayed=false;
					}
					m_TimerPK++;
					if(m_TimerPK>ttl) {
						LMP_SET(lmp,1);
						m_TimerPKReseted=false;
						m_bPK=true;
						m_PKState=0;
						SND_SET(SND_AI25_VALVE1,1);
					}
				}
			} else if(m_PKState==2) {
				if(LMP_GET(lmp)) {
					if(!m_TimerPKReseted) {
						m_TimerPK=0;
						m_TimerPKReseted=true;
					} else {
						if(!m_ClosePlayed) {	
							SND_SET(snd,2);
							m_OpenPlayed=false;
							m_ClosePlayed=true;
						}
						m_TimerPK++;
						if(m_TimerPK>ttl) {
							LMP_SET(lmp,0);
							m_TimerPKReseted=false;
							m_bPK=false;
							m_PKState=0;
						}
					}
				} else {
					LMP_SET(lmp,0);
					m_TimerPKReseted=false;
					m_bPK=false;
					m_PKState=0;
				}
			}
			m_bPK=LMP_GET(lmp);
		}
		virtual void Load(CIniFile *ini,EString name) {
			EString buf;
			buf="APK"+name;
			m_aPK=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
			buf="APKC"+name;
			m_aPKChanged=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
			buf="BPK"+name;
			m_bPK=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
			buf="TPK"+name;
			m_TimerPK=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
			buf="TPKR"+name;
			m_TimerPKReseted=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
			buf="PKS"+name;
			m_PKState=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		};

		virtual void Save(CIniFile *ini,EString name) {
			EString buf;
			buf="APK"+name;
			ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_aPK);
			buf="APKC"+name;
			ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_aPKChanged);
			buf="BPK"+name;
			ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_bPK);
			buf="TPK"+name;
			ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_TimerPK);
			buf="TPKR"+name;
			ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_TimerPKReseted);
			buf="PKS"+name;
			ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_PKState);
		};
	};

	void GetAllParams();
	void SetAllParams();
	void CheckAll();
	void StartUp();
	void StartEngine();
	void _lStartEngine (int index, int mode);
	void StartEngineInAir(int index);
	void SoundProcess();

	CAirStarter*	m_Airstarter;		// �������� � ��������� ������� �� ������
	CRTU*			m_RTU;
	PK				m_PK[3];
	int				m_EngineStartMasterSwitch;
	double			m_AmbientTemp;
	//int				m_EngineStartHeaterSwitch;
	int				m_EngineStartSelectSwitch;
	double			m_EngineN2[3];
	int				m_EngineStartModeSwitch;
	int				m_EngineStarting;
	bool			m_PDAWorking;
	bool			m_TestStart;
	int				m_BtnStart;
	int				m_BtnStartChanged;
	int				m_BtnStop;
	int				m_BtnStopChanged;
	bool			m_StopRequest[3];
	int 			m_BtnStartInAir[3];
	int				m_SoundTimer;
	bool			m_SoundTimerForced;

public:
	SDSystemEngines();
	virtual ~SDSystemEngines();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

	void SetRudPos();
	void UpdateNeedles();

};
