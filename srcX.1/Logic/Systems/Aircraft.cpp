#include "Aircraft.h"

SDSystemAircraft::SDSystemAircraft() 
{
	m_Htail			= CHtail::Instance();
	m_Flaps			= CFlaps::Instance();
	m_RTU			= CRTU::Instance();
	m_AirStarter	= CAirStarter::Instance();
	m_Gear			= CGear::Instance();
};

SDSystemAircraft::~SDSystemAircraft() 
{
	CHtail::Release();
	CFlaps::Release();
	CRTU::Release();
	CAirStarter::Release();
	CGear::Release();
};

void SDSystemAircraft::Init() 
{
	AZS_SET(AZS_LLIGHTSMOTOR,1);
};

void SDSystemAircraft::Update()
{
	m_Htail->Update();
	m_Flaps->Update();
	m_RTU->Update();
	m_AirStarter->Update();
	m_Gear->Update();
}

void SDSystemAircraft::Load(CIniFile *ini)
{
	m_AirStarter->Load(ini);
	m_Flaps->Load(ini);
	m_Htail->Load(ini);
}

void SDSystemAircraft::Save(CIniFile *ini)
{
	m_AirStarter->Save(ini);
	m_Flaps->Save(ini);
	m_Htail->Save(ini);
}

