/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/Fuel.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

enum ACT
{
	ACT_OFF,
	ACT_AUTO,
	ACT_LEFT,
	ACT_RIGHT,
	ACT_MAX
};

class SDSystemFuel : public SDLogicBase
{
private:
	void GetAllParams();
	void SetAllParams();

	void ACTCheck();
	void PumpsCheck();

	void Work();

	int		m_Bus27;				// ���� 27�
	double	m_Generators[3];		// ����������

	int		m_aACT;					// ������� ��� (������� ������������)
	int		m_aACTManual;			// ������� ��� ���� (����������� �����)
	ACT		m_bACT;					// ������� ������ ���

	int		m_aLeftPump;			// ������� ���. ������ ������
	bool	m_bLeftPump;			// ������� ������ ������ ������

	int		m_aRightPump;			// ������� ������� ������
	int		m_aRightPumpEmerge;		// ������� ���������������  ����������� ������� ������ � ���� 27�
	bool	m_bRightPump;			// ������� ������ ������� ������

	int		m_aXFeedTank;			// ������� ������������ ������� ��� ����������� ������� � �����
	bool	m_bXFeedTank;			// ������� ������ �������

	int		m_aXFeedPump;			// ������� ������������ ������ ��� ����������� ������� � �������
	bool	m_bXFeedPump;			// ������� ������ �������

	double  m_LeftTankQuantityGl;	// ���������� ������� � ����� ���� � �������
	double  m_RightTankQuantityGl;	// ���������� ������� � ������ ���� � �������

	double  m_LeftTankQuantityKg;	// ���������� ������� � ����� ���� � �����������
	double  m_RightTankQuantityKg;	// ���������� ������� � ������ ���� � �����������

	int		m_Tick;					// ���� ��� ������������ ����� � ������ ���

	bool	m_ACTPlayed;

public:
	SDSystemFuel();
	virtual ~SDSystemFuel();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

};
