/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/MiscSystem.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

#define MAX_AZS_IN_VC 173
#define MAX_GLT_IN_VC 7

class SDSystemVCAZSGLT : public SDLogicBase
{
private:
	CNamedVar					*m_AzsVC[MAX_AZS_IN_VC];
	CNamedVar					*m_GltVC[MAX_GLT_IN_VC];
	auto_ptr<CNamedVar>			m_XmlPanelLang;

	void						UpdateVCAZS();

public:
	SDSystemVCAZSGLT();
	virtual ~SDSystemVCAZSGLT();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

};
