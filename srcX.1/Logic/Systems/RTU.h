/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/RTU.h $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"

#define SIM1_READ_VALUE -1000000.0f

class CRTU
{
private:
  bool  m_Working;
  double  m_CurrentRud;
  double  m_MinThrottleLevel;
  int   m_NumEngines;
  double  m_CurTL;
  int   m_ProcessForced;

  unsigned short GetNumEngines(void) {
    return (USHORT) (g_SimData->GetParam(SIMPARAM_NUMBER_OF_ENGINES));
  }

  double GetMinThrottleLevel(void) {
    return -0.48; //g_SimData->GetParam(SIMPARAM_THROTTLE_LOWER_LIMIT);
  }

  double SetEngLeverPos(double value,int eng_index) {
    Sim1_main_class *sim1_class=(Sim1_main_class *)g_SimModules->m_Sim1->UserAircraftGet();
    if(sim1_class) {
      PSIM_DATA sim1_data=sim1_class->sim1_data;
    double old_value;
    switch(eng_index) {
      case 0:
        old_value = sim1_data->engines[0].engine_leverer;
      break;
      case 1:
        old_value = sim1_data->engines[1].engine_leverer;
      break;
      case 2:
        old_value = sim1_data->engines[2].engine_leverer;
      break;
    }
    if(value != SIM1_READ_VALUE) {
      switch(eng_index) {
        case 0:
          sim1_data->engines[0].engine_leverer=value;
        break;
        case 1:
          sim1_data->engines[1].engine_leverer=value;
        break;
        case 2:
          sim1_data->engines[2].engine_leverer=value;
        break;
      }
    }

    return old_value;
    }
    return 0;
  };

  double SetRud(double value) {
    if(value == SIM1_READ_VALUE) 
      return SetEngLeverPos(value,0);

    SetEngLeverPos(0,0);
    SetEngLeverPos(value,1);
    SetEngLeverPos(0,2);

    return SetEngLeverPos(SIM1_READ_VALUE,0);
  };

protected:
  static CRTU *m_Self;

  CRTU() {
    m_Working     = false;
    m_CurrentRud    = SetRud(-1000000.0f);
    m_MinThrottleLevel  = GetMinThrottleLevel()/10;
    m_NumEngines    = GetNumEngines();
    m_CurTL       = 0;
    m_ProcessForced   = 0;
  };

public:
  static CRTU *Instance() {
    SAFE_NEW(m_Self,CRTU());
    return m_Self;
  };

  static void Release() {
    SAFE_DELETE(m_Self);
  }

  void Update() {
    //m_CurrentRud = SetRud(SIM1_READ_VALUE);
    m_Working=(g_pSDAPI->GetN(GENERAL_ENGINE2_THROTTLE_LEVER_POS)<0);
    if(m_Working) {
      SetEngLeverPos(0,0);
      SetEngLeverPos(0,2);
      LMP_SET(LMP_REVERSEON,1);
      LMP_SET(LMP_REVERSEOFF,0);
    } else {
      LMP_SET(LMP_REVERSEON,0);
      LMP_SET(LMP_REVERSEOFF,1);
    }
    if(AZS_CHG(AZS_REVERSE)) {
      if(AZS_GET(AZS_REVERSE)==2) {
        m_ProcessForced=1;
      } else if(AZS_GET(AZS_REVERSE)==3) {
        m_ProcessForced=2;
      }
    }
    if(m_ProcessForced==1) {
      m_CurTL-=0.1;
      if(m_CurTL<m_MinThrottleLevel) {
        m_CurTL=m_MinThrottleLevel;
        m_ProcessForced=0;
        SND_SET(SND_REVERS_ON,1);
      }
      SetRud(m_CurTL);
    } else if(m_ProcessForced==2) {
      m_CurTL+=0.1;
      if(m_CurTL>0) {
        m_CurTL=0;
        m_ProcessForced=0;
      }
      SetRud(m_CurTL);
    } else {
      m_CurTL=0;
    }
  };

  bool IsWorking() const {return m_Working;};

  void Process() {
    m_ProcessForced=1;
  }

  void Reset() {m_CurTL=0;m_ProcessForced=false;}

};
