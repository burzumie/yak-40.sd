/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Flaps.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"

enum FLAPS_STATE
{
	FLAPS_STATE_NOT_MOVING=0,
	FLAPS_STATE_RETRACTING=1,
	FLAPS_STATE_EXTENDING=2
};

class CFlaps
{
private:
	auto_ptr<CNamedVar> xml1;
	auto_ptr<CNamedVar> xmlLFlap;
	auto_ptr<CNamedVar> xmlRFlap;
	auto_ptr<CNamedVar> VCAnimNdl;

	double m_FlapsLPos[2];
	double m_FlapsRPos[2];
	FLAPS_STATE m_FlapsLState;
	FLAPS_STATE m_FlapsRState;
	double m_Deg;
	UINT32 m_MaxHandlePos;
	UINT32 m_OldHandlePos[2];
	static double m_Min,m_Max,m_Step,m_HydLimit;

	void SendToSim() {
		m_Deg=LimitValue(m_Deg,m_Min,m_Max);
/*
		SetLeftFlapPosDg(m_Deg);
		SetRightFlapPosDg(m_Deg);
*/
	};

protected:
	static CFlaps *m_Self;

	CFlaps() {
		xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_527"));
		xmlLFlap=auto_ptr<CNamedVar>(new CNamedVar("sd6015_flaps_left"));
		xmlRFlap=auto_ptr<CNamedVar>(new CNamedVar("sd6015_flaps_right"));
		VCAnimNdl=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_flaps"));

		m_Min=g_pSDAPI->m_Config.m_CfgFlaps.m_dMin;
		m_Max=g_pSDAPI->m_Config.m_CfgFlaps.m_dMax;
		m_Step=g_pSDAPI->m_Config.m_CfgFlaps.m_dStep;
		m_HydLimit=g_pSDAPI->m_Config.m_CfgFlaps.m_dHydLimit;

		m_FlapsLPos[0]=m_FlapsLPos[1]=g_pSDAPI->GetN(TRAILING_EDGE_FLAPS0_LEFT_ANGLE);
		m_FlapsRPos[0]=m_FlapsRPos[1]=g_pSDAPI->GetN(TRAILING_EDGE_FLAPS0_RIGHT_ANGLE);
		m_FlapsLState=FLAPS_STATE_NOT_MOVING;
		m_FlapsRState=FLAPS_STATE_NOT_MOVING;
		m_Deg=m_FlapsLPos[0];

/*
		m_MaxHandlePos=(UINT32)(m_Max/m_Step);
		SetMaxFlapHandleIndex(m_MaxHandlePos);
		SetMaxFlapIndex(m_MaxHandlePos);
*/

		xmlLFlap->set_number(m_FlapsLPos[0]*468);
		xmlRFlap->set_number(m_FlapsRPos[0]*468);
	};

public:
	static CFlaps *Instance() {
		if(!m_Self) {
			m_Self=new CFlaps();
		}
		return m_Self;
	};

	static void Release() {
		if(m_Self) {
			delete m_Self;
			m_Self=NULL;
		}
	}

	void Update() {
		static bool ExtendingPlayed=false;
		static bool Flaps20Played=false;

		m_OldHandlePos[1]=m_OldHandlePos[0];
		//m_OldHandlePos[0]=GetFlapHandleIndex(); 

		m_FlapsLPos[0]=g_pSDAPI->GetN(TRAILING_EDGE_FLAPS0_LEFT_ANGLE);
		m_FlapsRPos[0]=g_pSDAPI->GetN(TRAILING_EDGE_FLAPS0_RIGHT_ANGLE);

		if(m_FlapsLPos[0]!=m_FlapsLPos[1]) {
			if(m_FlapsLPos[0]<m_FlapsLPos[1])m_FlapsLState=FLAPS_STATE_RETRACTING;
			else if(m_FlapsLPos[0]>m_FlapsLPos[1])m_FlapsLState=FLAPS_STATE_EXTENDING;
			m_FlapsLPos[1]=m_FlapsLPos[0];
		} else {
			m_FlapsLState=FLAPS_STATE_NOT_MOVING;
		}

		if(m_FlapsRPos[0]!=m_FlapsRPos[1]) {
			if(m_FlapsRPos[0]<m_FlapsRPos[1])m_FlapsRState=FLAPS_STATE_RETRACTING;
			else if(m_FlapsRPos[0]>m_FlapsRPos[1])m_FlapsRState=FLAPS_STATE_EXTENDING;
			m_FlapsRPos[1]=m_FlapsRPos[0];
		} else {
			m_FlapsRState=FLAPS_STATE_NOT_MOVING;
		}

		if(POS_GET(POS_MAIN_HYDRO_WORK)) {
			int flapsm=AZS_GET(AZS_FLAPS);
			if(flapsm==2) {
				trigger_key_event(KEY_FLAPS_INCR,0);
			} else if(flapsm==3) {
				trigger_key_event(KEY_FLAPS_DECR,0);
			}
		}

		int flapse=AZS_GET(AZS_EMERG_FLAPS)==2;

		if(flapse) {
			if(NDL_GET(NDL_EMERG_HYD)>30&&AZS_GET(AZS_FLAPS_EMERG_BUS)) {
				ReturnFlapTab(2);
			} else {
				CalculateFlapTab();
			}
			POS_SET(POS_AUXL_FLAPS_WORK,1);
		} else {
			POS_SET(POS_AUXL_FLAPS_WORK,0);
		}

		xml1->set_number(NDL_GET(NDL_FLAPS));
		VCAnimNdl->set_degree(dgrd(NDL_GET(NDL_FLAPS)*0.8428*2));

		xmlLFlap->set_number(m_FlapsLPos[0]*468);
		xmlRFlap->set_number(m_FlapsRPos[0]*468);

		if(m_FlapsLState==FLAPS_STATE_EXTENDING&&m_FlapsRState==FLAPS_STATE_EXTENDING) {
			if(m_FlapsLPos[0]>19&&m_FlapsLPos[0]<20&&m_FlapsRPos[0]>19&&m_FlapsRPos[0]<20) {
				if(!Flaps20Played) {
					if(!SND_GET(SND_FLAPS_EXTENDING))
						SND_SET(SND_FLAPS20,1);
					Flaps20Played=true;
				}
			}
			if(!ExtendingPlayed&&m_FlapsLPos[0]>3&&m_FlapsRPos[0]>3&&m_FlapsLPos[0]<10&&m_FlapsRPos[0]<10) {
				SND_SET(SND_FLAPS_EXTENDING,1);
				ExtendingPlayed=true;
			}
		}
		if(m_FlapsLState==FLAPS_STATE_RETRACTING&&m_FlapsRState==FLAPS_STATE_RETRACTING) {
			ExtendingPlayed=false;
			Flaps20Played=false;
		}
		

	};

	inline FLAPS_STATE GetLeftState()  const { return m_FlapsLState; };
	inline FLAPS_STATE GetRightState() const { return m_FlapsRState; };
	inline double      GetLeftPos()    const { return m_FlapsLPos[0];};
	inline double      GetRightPos()   const { return m_FlapsRPos[0];};
	inline UINT32	   GetOldHandlePos(int i) const {return m_OldHandlePos[i];};

	double Inc(int sender,double val=m_Step) {
		if(!AZS_GET(AZS_FLAPS_MAIN_BUS))
			return m_Deg;
		if(NDL_GET(NDL_MAIN_HYD)<m_HydLimit)
			return m_Deg;
		m_Deg+=val;
		SendToSim();
		return m_Deg;
	};

	double Dec(int sender,double val=m_Step) {
		if(!AZS_GET(AZS_FLAPS_MAIN_BUS))
			return m_Deg;
		if(NDL_GET(NDL_MAIN_HYD)<m_HydLimit)
			return m_Deg;
		m_Deg-=val;
		SendToSim();
		return m_Deg;
	};

	void Set(int sender,double val) {
		if(!AZS_GET(AZS_FLAPS_MAIN_BUS))
			return;
		if(NDL_GET(NDL_MAIN_HYD)<m_HydLimit)
			return;
		m_Deg=val;
		SendToSim();
	};

	void CalculateFlapTab() {
		//if(!AZS_GET(AZS_FLAPS_MAIN_BUS))
		//	return;
/*
		if(GetFlapHandleIndex()>0) {
			SetHandleToPosTableIn(0,GetFlapHandleIndex()); 
			SetHandleToPosTableIn(1,GetFlapHandleIndex()+1); 
			SetHandleToPosTableIn(2,GetFlapHandleIndex()+2); 
			SetHandleToPosTableOut(0,dgrd(m_Step*GetFlapHandleIndex())); 
			SetHandleToPosTableOut(1,dgrd(m_Step*GetFlapHandleIndex()+1)); 
			SetHandleToPosTableOut(2,dgrd(m_Step*GetFlapHandleIndex()+2)); 

			SetHandleToPosTableIn2(0,GetFlapHandleIndex()); 
			SetHandleToPosTableIn2(1,GetFlapHandleIndex()+1); 
			SetHandleToPosTableIn2(2,GetFlapHandleIndex()+2); 
			SetHandleToPosTableOut2(0,dgrd(m_Step*GetFlapHandleIndex())); 
			SetHandleToPosTableOut2(1,dgrd(m_Step*GetFlapHandleIndex()+1)); 
			SetHandleToPosTableOut2(2,dgrd(m_Step*GetFlapHandleIndex()+2)); 
		} else {
			SetHandleToPosTableIn(0,0); 
			SetHandleToPosTableIn(1,1); 
			SetHandleToPosTableIn(2,2); 
			SetHandleToPosTableOut(0,0); 
			SetHandleToPosTableOut(1,dgrd(m_Step)); 
			SetHandleToPosTableOut(2,dgrd(m_Step+m_Step)); 

			SetHandleToPosTableIn2(0,0); 
			SetHandleToPosTableIn2(1,1); 
			SetHandleToPosTableIn2(2,2); 
			SetHandleToPosTableOut2(0,0); 
			SetHandleToPosTableOut2(1,dgrd(m_Step)); 
			SetHandleToPosTableOut2(2,dgrd(m_Step+m_Step)); 
		}
*/
	};

	void ReturnFlapTab(int i) {
		//if(!AZS_GET(AZS_FLAPS_MAIN_BUS)||!AZS_GET(AZS_FLAPS_EMERG_BUS))
		//	return;
/*
		if(i==0) {
			SetFlapHandleIndex(GetOldHandlePos(1));
		} else if(i==1) {
			DecFlapHandleIndex();
		} else {
			IncFlapHandleIndex();
		}

		SetHandleToPosTableIn(0,GetFlapHandleIndex()); 
		SetHandleToPosTableIn(1,GetFlapHandleIndex()+1); 
		SetHandleToPosTableIn(2,GetFlapHandleIndex()+2); 
		SetHandleToPosTableOut(0,dgrd(m_Step*GetFlapHandleIndex())); 
		SetHandleToPosTableOut(1,dgrd(m_Step*GetFlapHandleIndex())); 
		SetHandleToPosTableOut(2,dgrd(m_Step*GetFlapHandleIndex())); 

		SetHandleToPosTableIn2(0,GetFlapHandleIndex()); 
		SetHandleToPosTableIn2(1,GetFlapHandleIndex()+1); 
		SetHandleToPosTableIn2(2,GetFlapHandleIndex()+2); 
		SetHandleToPosTableOut2(0,dgrd(m_Step*GetFlapHandleIndex())); 
		SetHandleToPosTableOut2(1,dgrd(m_Step*GetFlapHandleIndex())); 
		SetHandleToPosTableOut2(2,dgrd(m_Step*GetFlapHandleIndex())); 
*/
	};

	virtual void Load(CIniFile *ini) {
		EString buf;

/*
		buf="FLPHTPtin0";
		SetHandleToPosTableIn(0,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableIn(0)));
		buf="FLPHTPtin1";
		SetHandleToPosTableIn(1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableIn(1)));
		buf="FLPHTPtin2";
		SetHandleToPosTableIn(2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableIn(2)));
		buf="FLPHTPtout0";
		SetHandleToPosTableOut(0,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableOut(0)));
		buf="FLPHTPtout1";
		SetHandleToPosTableOut(1,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableOut(1)));
		buf="FLPHTPtout2";
		SetHandleToPosTableOut(2,ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableOut(2)));
		buf="FLPMFHI";
		SetMaxFlapHandleIndex(ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetMaxFlapHandleIndex()));
		buf="FLPMFI";
		SetMaxFlapIndex(ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetMaxFlapIndex()));
		buf="FLPFSLFP0";
		SetLeftFlapPosRd(ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetLeftFlapPosRd()));
		buf="FLPFSLFP1";
		SetRightFlapPosRd(ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetRightFlapPosRd()));

		buf="FLPMFLP0";
		m_FlapsLPos[0]=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_FlapsLPos[0]);
		buf="FLPMFLP1";
		m_FlapsLPos[1]=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_FlapsLPos[1]);
		buf="FLPMFRP0";
		m_FlapsRPos[0]=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_FlapsRPos[0]);
		buf="FLPMFRP1";
		m_FlapsRPos[1]=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_FlapsRPos[1]);
		buf="FLPFLDG";
		m_Deg=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Deg);
		buf="FLPMMHP";
		m_MaxHandlePos=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MaxHandlePos);
		buf="FLPMOHP0";
		m_OldHandlePos[0]=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_OldHandlePos[0]);
		buf="FLPMOHP1";
		m_OldHandlePos[1]=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_OldHandlePos[1]);

		xmlLFlap->set_number(m_FlapsLPos[0]*468);
		xmlRFlap->set_number(m_FlapsRPos[0]*468);
*/
	}

	virtual void Save(CIniFile *ini) {
		EString buf;

/*
		buf="FLPHTPtin0";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableIn(0));
		buf="FLPHTPtin1";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableIn(1));
		buf="FLPHTPtin2";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableIn(2));
		buf="FLPHTPtout0";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableOut(0));
		buf="FLPHTPtout1";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableOut(1));
		buf="FLPHTPtout2";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetHandleToPosTableOut(2));
		buf="FLPMFHI";
		ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetMaxFlapHandleIndex());
		buf="FLPMFI";
		ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetMaxFlapIndex());
		buf="FLPFSLFP0";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetLeftFlapPosRd());
		buf="FLPFSLFP1";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),GetRightFlapPosRd());

		buf="FLPMFLP0";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_FlapsLPos[0]);
		buf="FLPMFLP1";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_FlapsLPos[1]);
		buf="FLPMFRP0";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_FlapsRPos[0]);
		buf="FLPMFRP1";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_FlapsRPos[1]);
		buf="FLPFLDG";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Deg);
		buf="FLPMMHP";
		ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MaxHandlePos);
		buf="FLPMOHP0";
		ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_OldHandlePos[0]);
		buf="FLPMOHP1";
		ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_OldHandlePos[1]);
*/
	}

};
