/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/KursMP.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

class CKursMPBase
{
public:
	UINT32 m_VOROBS;
	UINT32 m_NAVFreq;
	double m_DMEDist;
	std::auto_ptr<CNamedVar> VCAnimHndMhz;
	std::auto_ptr<CNamedVar> VCAnimHndKhz;
	std::auto_ptr<CNamedVar> VCAnimHndObs;
	std::auto_ptr<CNamedVar> VCAnimBtnAud;

	virtual void Init()=0;
	virtual void Update()=0;
};

class CKursMP1 : public CKursMPBase
{
public:
	virtual void Init();
	virtual void Update();
};

class CKursMP2 : public CKursMPBase
{
public:
	virtual void Init();
	virtual void Update();
};

class SDSystemKMP : public SDLogicBase
{
private:
	CKursMP1 m_KMP1;
	CKursMP2 m_KMP2;

public:
	SDSystemKMP();
	virtual ~SDSystemKMP();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};

