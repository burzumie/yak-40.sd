/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/AirStarter.h $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../../Lib/EString.h"

#define CONNECT_TIME 15*TICKS_PER_SEC

enum AIR_STARTERS {
	AIR_STARTERS_NONE,
	AIR_STARTERS_EXT,
	AIR_STARTERS_APU,
	AIR_STARTERS_ENG,
	AIR_STARTERS_MAX
};

enum AIR_REQUESTER {
	AIR_REQUESTER_NONE,
	AIR_REQUESTER_ENG,
	AIR_REQUESTER_SKV,
	AIR_REQUESTER_MAX
};

#define EXTERNAL_AIR_LIMIT	4.1
#define EXTERNAL_AIR_STEP 	0.01

#define APU_AIR_LIMIT		2.8
#define APU_AIR_STEP 		0.01

#define ENGINES_AIR_COEFF_START		0.05
#define ENGINES_AIR_RPM_INCREMENT	53
#define ENGINES_AIR_COEFF_STEP		0.01
#define ENGINES_AIR_MIN_STEP		1

#define AIR_VALUE_TO_REPORT			1.79

class CAirStarter
{
private:
	bool			m_Starters[AIR_STARTERS_MAX];
	AIR_STARTERS	m_CurrentStarter;
	double			m_AirLimit;
	double			m_Step;
	bool			m_Starting;
	int				m_Timer;
	bool			m_TimerReseted;
	bool			m_RequestConnect;
	bool			m_RequestDisconnect;
	int				m_WorkingEngine[3];
	double			m_Rpm;
	double			m_CurAir;
	double			m_CurAirFinal;
	double			m_RequestAir[AIR_REQUESTER_MAX];
	auto_ptr<CNamedVar> xml_TTAirStarter;
	auto_ptr<CNamedVar> VCAnimNdl;

protected:
	static CAirStarter *m_Self;

	CAirStarter() {
		m_CurrentStarter	= AIR_STARTERS_NONE;
		for(int i=0;i<AIR_STARTERS_MAX;i++)
			m_Starters[i]=false;
		m_AirLimit			= 0;
		m_Step				= 0;
		m_Starting			= false;
		m_RequestConnect	= false; 
		m_RequestDisconnect = false;  
		m_TimerReseted		= false; 
		m_Timer				= 0;
		m_WorkingEngine[0]	= 0;
		m_WorkingEngine[1]	= 0;
		m_WorkingEngine[2]	= 0;
		m_Rpm				= 0;
		m_CurAir			= 0;
		m_CurAirFinal		= 0;
		for(int i=0;i<AIR_REQUESTER_MAX;i++)
			m_RequestAir[i]=0;
		xml_TTAirStarter=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_528"));
		VCAnimNdl=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_airstarter"));
	};

public:
	static CAirStarter *Instance() {
		SAFE_NEW(m_Self,CAirStarter());
		return m_Self;
	};

	static void Release() {
		SAFE_DELETE(m_Self);
	}

	void Update() {
		xml_TTAirStarter->set_number(NDL_GET(NDL_STARTER));
		VCAnimNdl->set_degree(dgrd(NDL_GET(NDL_STARTER)*12.3));

		m_Timer++;

		if(AZS_CHG(AZS_EXT_AIR)) {
			if(AZS_GET(AZS_EXT_AIR)) {
				m_RequestDisconnect = false; 
				m_RequestConnect	= true; 
				m_Timer				= 0;
				m_TimerReseted		= true; 
			} else {
				m_RequestConnect	= false; 
				m_RequestDisconnect = true; 
				m_Timer				= 0;
				m_TimerReseted		= true; 
			}
		}

		if(m_RequestConnect && m_TimerReseted) {
			if(m_Timer>CONNECT_TIME) {
				m_RequestConnect	= false;
				m_TimerReseted		= false;
				Start(AIR_STARTERS_EXT);
			}
		}

		if(m_RequestDisconnect && m_TimerReseted) {
			if(m_Timer>CONNECT_TIME) {
				m_RequestDisconnect = false;
				m_TimerReseted		= false;
				Stop(AIR_STARTERS_EXT);
			}
		}

		if(g_pSDAPI->GetN(TURB_ENGINE_1_CORRECTED_N2)>52) {
			m_WorkingEngine[0]=1;
			Start(AIR_STARTERS_ENG);
		} else {
			m_WorkingEngine[0]=0;
		}
		if(g_pSDAPI->GetN(TURB_ENGINE_2_CORRECTED_N2)>52) {
			m_WorkingEngine[1]=1;
			Start(AIR_STARTERS_ENG);
		} else {
			m_WorkingEngine[1]=0;
		}
		if(g_pSDAPI->GetN(TURB_ENGINE_3_CORRECTED_N2)>52) {
			m_WorkingEngine[2]=1;
			Start(AIR_STARTERS_ENG);
		} else {
			m_WorkingEngine[2]=0;
		}
		
		if(m_WorkingEngine[0]+m_WorkingEngine[1]+m_WorkingEngine[2]==0) {
			Stop(AIR_STARTERS_ENG);
		}

		if(m_Starters[AIR_STARTERS_EXT]) {
			m_CurrentStarter= AIR_STARTERS_EXT;
			m_AirLimit		= EXTERNAL_AIR_LIMIT;
			m_Step			= EXTERNAL_AIR_STEP;
			m_Starting		= true;
		} else if(m_Starters[AIR_STARTERS_APU]) {
			m_CurrentStarter= AIR_STARTERS_APU;
			m_AirLimit		= APU_AIR_LIMIT;
			m_Step			= APU_AIR_STEP;
			m_Starting		= true;
		} else if(m_Starters[AIR_STARTERS_ENG]) {
			m_CurrentStarter=AIR_STARTERS_ENG;
			m_Rpm			= 0;
			double minrpm	= 0;
			double coeff	= ENGINES_AIR_COEFF_START;
			double minair	= 0;
			if(m_WorkingEngine[0]==1) {
				m_Rpm+=g_pSDAPI->GetN(TURB_ENGINE_1_CORRECTED_N2);
				minrpm+=ENGINES_AIR_RPM_INCREMENT;
				coeff-=ENGINES_AIR_COEFF_STEP;
				minair+=ENGINES_AIR_MIN_STEP;
			}
			if(m_WorkingEngine[1]==1) {
				m_Rpm+=g_pSDAPI->GetN(TURB_ENGINE_2_CORRECTED_N2);
				minrpm+=ENGINES_AIR_RPM_INCREMENT;
				coeff-=ENGINES_AIR_COEFF_STEP;
				minair+=ENGINES_AIR_MIN_STEP;
			}
			if(m_WorkingEngine[2]==1) {
				m_Rpm+=g_pSDAPI->GetN(TURB_ENGINE_3_CORRECTED_N2);
				minrpm+=ENGINES_AIR_RPM_INCREMENT;
				coeff-=ENGINES_AIR_COEFF_STEP;
				minair+=ENGINES_AIR_MIN_STEP;
			}

			m_AirLimit=minair+((m_Rpm-minrpm)*coeff);
			m_Step=APU_AIR_STEP;
			m_Starting=true;
		} else {
			m_CurrentStarter=AIR_STARTERS_NONE;
			m_AirLimit=0;
			m_Step=APU_AIR_STEP;
			m_Starting=false;
		}

		//double curAir=NDL_GET(NDL_STARTER);

		if(m_CurAir<m_AirLimit&&m_Starting) {
			m_CurAir+=m_Step;
		}

		if(m_CurAir>m_AirLimit&&!m_Starting) {
			m_CurAir-=m_Step;
			if(m_CurAir<0)
				m_CurAir=0;
			//TBL_SET(TBL_AIR_STARTER_ON,0);
		}

		if(m_Starting) {
			if(m_CurAir>m_AirLimit)
				m_CurAir=m_AirLimit;
		} else {
			if(m_CurAir<0)
				m_CurAir=0;
		}

		static bool AlreadyTalk=false;

		if(m_CurAir>=AIR_VALUE_TO_REPORT) {
			//TBL_SET(TBL_AIR_STARTER_ON,1);
			if(!AlreadyTalk&&!POS_GET(POS_DESCENT)) {
				SND_SET(SND_AIRSTARTER_NORMAL,1);
				AlreadyTalk=true;
			}
		} else if(m_CurAir<0.01){
			//TBL_SET(TBL_AIR_STARTER_ON,0);
			AlreadyTalk=false;
		}

		double tmp=0;
		for(int i=0;i<AIR_REQUESTER_MAX;i++)
			tmp+=m_RequestAir[i];

		m_CurAirFinal=m_CurAir-tmp;

		if(m_CurAirFinal<0)
			m_CurAirFinal=0;

		if(AZS_GET(AZS_AIR_STARTER_IND_BUS)&&PWR_GET(PWR_BUS27))
			NDL_SET(NDL_STARTER,m_CurAirFinal);
	};

	void Start(AIR_STARTERS who) {
		m_Starters[who]=true;
	}

	void Stop(AIR_STARTERS who) {
		m_Starters[who]=false;
	}

	AIR_STARTERS GetCurrentStarter() {return m_CurrentStarter;}
	double GetAir() {return m_CurAirFinal;}
	void RequestAir(double val,AIR_REQUESTER who) {m_RequestAir[who]=val;}

	virtual void Load(CIniFile *ini) {
		EString buf;
		int i;
		for(i=0;i<AIR_STARTERS_MAX;i++) {
			buf="ASStrt"+i;
			m_Starters[i]=ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		}

		buf="ASCurSt";
		m_CurrentStarter=(AIR_STARTERS)ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),AIR_STARTERS_NONE);
		buf="ASAirLm";
		m_AirLimit=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		buf="ASStep";
		m_Step=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		buf="ASStarting";
		m_Starting=ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="ASTmr";
		m_Timer=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		buf="ASTmrRst";
		m_TimerReseted=ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="ASReqCon";
		m_RequestConnect=ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);
		buf="ASReqDiscon";
		m_RequestDisconnect=ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),false);

		for(i=0;i<3;i++) {
			buf="ASWrkEn"+i;
			m_WorkingEngine[i]=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		}

		buf="ASRpm";
		m_Rpm=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		buf="ASCurAir";
		m_CurAir=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		buf="ASCurAirFnl";
		m_CurAirFinal=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);

		for(i=0;i<AIR_REQUESTER_MAX;i++) {
			buf="ASReqAi"+i;
			m_RequestAir[i]=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),0);
		}
	};

	virtual void Save(CIniFile *ini) {
		EString buf;
		int i;
		for(i=0;i<AIR_STARTERS_MAX;i++) {
			buf="ASStrt"+i;
			ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Starters[i]);
		}

		buf="ASCurSt";
		ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_CurrentStarter);
		buf="ASAirLm";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_AirLimit);
		buf="ASStep";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Step);
		buf="ASStarting";
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Starting);
		buf="ASTmr";
		ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Timer);
		buf="ASTmrRst";
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_TimerReseted);
		buf="ASReqCon";
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_RequestConnect);
		buf="ASReqDiscon";
		ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_RequestDisconnect);

		for(i=0;i<3;i++) {
			buf="ASWrkEn"+i;
			ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_WorkingEngine[i]);
		}

		buf="ASRpm";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Rpm);
		buf="ASCurAir";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_CurAir);
		buf="ASCurAirFnl";
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_CurAirFinal);

		for(i=0;i<AIR_REQUESTER_MAX;i++) {
			buf="ASReqAi"+i;
			ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_RequestAir[i]);
		}
	};

};
