/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/HTail.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"

class CHtail
{
private:
	double m_Deg;
	bool m_Working;
	bool m_WorkingEmerg;
	double m_Htail[2];
	auto_ptr<CNamedVar> xml1;
	auto_ptr<CNamedVar> xmlHtail;
	auto_ptr<CNamedVar> VCAnimNdl;

	void SetHtailIncidenceDg(double val) {
		Sim1_main_class *sim1_class=(Sim1_main_class *)g_SimModules->m_Sim1->UserAircraftGet();
		if(sim1_class) {
			PSIM_DATA sim1_data=sim1_class->sim1_data;
			sim1_data->pStaticData->airplane_geometry->htail_incidence=dgrd(val);
		}
	};

	void SendToSim() {
		m_Deg=LimitValue(m_Deg,g_pSDAPI->m_Config.m_CfgHtail.m_dMin,g_pSDAPI->m_Config.m_CfgHtail.m_dMax);
		SetHtailIncidenceDg(m_Deg);
	};

protected:
	static CHtail *m_Self;

	CHtail(){m_Deg=-3;SendToSim();m_Working=false;m_Htail[0]=m_Htail[1]=-3;xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_523"));xmlHtail=auto_ptr<CNamedVar>(new CNamedVar("sd6015_stabilizer"));VCAnimNdl=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_htail"));};

public:
	static CHtail *Instance() {
		if(!m_Self) {
			m_Self=new CHtail();
		}
		return m_Self;
	};

	static void Release() {
		if(m_Self) {
			delete m_Self;
			m_Self=NULL;
		}
	}

	void Update() {
		m_Htail[0]=m_Deg;

		if(m_Htail[0]!=m_Htail[1]) {
			m_Working=true;
			m_Htail[1]=m_Htail[0];
		} else {
			m_Working=false;
		}

		int btnmtrim1=BTN_GET(BTN_TRIMMAIN2);
		int btnmtrim2=BTN_GET(BTN_TRIMMAIN3);

		if(btnmtrim1==1||btnmtrim2==1) {
			if(POS_GET(POS_MAIN_HYDRO_WORK))
				trigger_key_event(KEY_ELEV_TRIM_UP,0);
		} else if(btnmtrim1==2||btnmtrim2==2) {
			if(POS_GET(POS_MAIN_HYDRO_WORK))
				trigger_key_event(KEY_ELEV_TRIM_DN,0);
		}

		int btnemtrim1=BTN_GET(BTN_TRIMEMERG2);
		int btnemtrim2=BTN_GET(BTN_TRIMEMERG3);

		if(btnemtrim1==1||btnemtrim2==1) {
			trigger_key_event(KEY_ELEV_TRIM_UP,0);
/*
			if(POS_GET(POS_AP_PITCH_MODE)&&!POS_GET(POS_AP_HAND_MODE)) {
			} else {
				m_Htail->Dec(1);
			}
*/
		} else if(btnemtrim1==2||btnemtrim2==2) {
			trigger_key_event(KEY_ELEV_TRIM_DN,0);
/*
			if(POS_GET(POS_AP_PITCH_MODE)&&!POS_GET(POS_AP_HAND_MODE)) {
			} else {
				m_Htail->Inc(1);
			}
*/
		}

		xml1->set_number(-1*NDL_GET(NDL_HTAIL_DEG)-3);

		xmlHtail->set_degree(m_Deg*-1);

		VCAnimNdl->set_degree(dgrd((NDL_GET(NDL_HTAIL_DEG)+3)*4.3*2));

	};

	double Inc(int sender,double val=g_pSDAPI->m_Config.m_CfgHtail.m_dStep) {
		bool gotoemerg=false;
		if(sender==0) {
			if(NDL_GET(NDL_MAIN_HYD)<g_pSDAPI->m_Config.m_CfgHtail.m_dHydLimit||!AZS_GET(AZS_STAB_MAIN_BUS)) {
				gotoemerg=true;
				m_Working=false;
			} else {
				m_Working=true;
				m_WorkingEmerg=false;
			}
		} 
		if(sender==1||gotoemerg) {
			if(NDL_GET(NDL_EMERG_HYD)<40||!AZS_GET(AZS_STAB_EMERG_BUS)) {
				m_Working=false;
				m_WorkingEmerg=false;
				return m_Deg;
			} else {
				m_Working=false;
				m_WorkingEmerg=true;
			}
		}
		
		m_Deg+=val;
		SendToSim();
		return m_Deg;
	};

	double Dec(int sender,double val=g_pSDAPI->m_Config.m_CfgHtail.m_dStep) {
		bool gotoemerg=false;
		if(sender==0) {
			if(NDL_GET(NDL_MAIN_HYD)<g_pSDAPI->m_Config.m_CfgHtail.m_dHydLimit||!AZS_GET(AZS_STAB_MAIN_BUS)) {
				gotoemerg=true;
				m_Working=false;
			} else {
				m_Working=true;
				m_WorkingEmerg=false;
			}
		} 
		if(sender==1||gotoemerg) {
			if(NDL_GET(NDL_EMERG_HYD)<40||!AZS_GET(AZS_STAB_EMERG_BUS)) {
				m_Working=false;
				m_WorkingEmerg=false;
				return m_Deg;
			} else {
				m_Working=false;
				m_WorkingEmerg=true;
			}
		}
		
		m_Deg-=val;
		SendToSim();
		return m_Deg;
	};

	void Set(int sender,double val) {
		bool gotoemerg=false;
		if(sender==0) {
			if(NDL_GET(NDL_MAIN_HYD)<g_pSDAPI->m_Config.m_CfgHtail.m_dHydLimit||!AZS_GET(AZS_STAB_MAIN_BUS)) {
				gotoemerg=true;
				m_Working=false;
			} else {
				m_Working=true;
				m_WorkingEmerg=false;
			}
		} 
		if(sender==1||gotoemerg) {
			if(NDL_GET(NDL_EMERG_HYD)<40||!AZS_GET(AZS_STAB_EMERG_BUS)) {
				m_Working=false;
				m_WorkingEmerg=false;
				return;
			} else {
				m_Working=false;
				m_WorkingEmerg=true;
			}
		}
		
		m_Deg=val;
		SendToSim();
	};

	double Get() const {return m_Deg;};

	bool IsWorking() const {return m_Working;};
	bool IsWorkingEmerg() const {return m_WorkingEmerg;};

	void Load(CIniFile *ini) {
		m_Deg=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),"HTAILDG",m_Deg);
		SendToSim();
	};
	void Save(CIniFile *ini) {
		ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),"HTAILDG",m_Deg);
	};

};
