/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/MiscSystem.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Electro.h"

SDGroundSupply	*SDGroundSupply::m_Self	= NULL; 
SDBatterys		*SDBatterys::m_Self		= NULL;

//////////////////////////////////////////////////////////////////////////

void SDBus27::SetTotalLoadAmps(double val)
{
	g_SimData->SetParam(SIMPARAM_WRITE_ELECTRICAL_TOTAL_LOAD_AMPS,val);
}

void SDBus27::CalculateLoad(double load115, double load36)
{
	m_TotalLoad=0;

	for(int i=0;i<LMP_MAX;i++) {
		if(LMPE_GET(i))
			m_TotalLoad+=0.02;
	}

	for(int i=0;i<TBL_MAX;i++) {
		if(TBLE_GET(i))
			m_TotalLoad+=0.02;
	}

	for(int i=0;i<TBG_MAX;i++) {
		if(TBGE_GET(i))
			m_TotalLoad+=0.02;
	}

	m_TotalLoad+=load115;
	m_TotalLoad+=load36;

	SetTotalLoadAmps(/*g_SimData->GetParamFromSimObject(REQUEST_ELECTRICAL_TOTAL_LOAD_AMPS)-*/m_TotalLoad);
}

void SDBus27::Update() 
{
	if(AZS_CHG(AZS_EXT_PWR)) {
		if(AZS_GET(AZS_EXT_PWR)) {
			m_GroundSupply->RequestConnect(SDGroundSupply::SHRAP_27V);
			m_GroundSupply->RequestConnect(SDGroundSupply::SHRAP_115V);
		} else {
			m_GroundSupply->RequestDisconnect(SDGroundSupply::SHRAP_27V);
			m_GroundSupply->RequestDisconnect(SDGroundSupply::SHRAP_115V);
		}
	}

	m_GroundSupply->Update();
	m_Batterys->Update();

	int gec=int(m_GroundSupply->RequestStatus(SDGroundSupply::SHRAP_27V)*g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)*AZS_GET(AZS_GROUND_PWR_BUS));
	PWR_SET(PWR_BUS27EXT,gec);
	LMP_SET(LMP_GROUND_ELEC_CONNECTED,gec);

	if(AZS_GET(AZS_POWERSOURCE)==POWER_SOURCE_RAP&&m_GroundSupply->RequestStatus(SDGroundSupply::SHRAP_27V)&&g_pSDAPI->GetN(AIRCRAFT_ON_GROUND)&&AZS_GET(AZS_GROUND_PWR_BUS)) {
		if(!g_pSDAPI->GetN(MASTER_BATTERY))
			send_key_event(KEY_TOGGLE_MASTER_BATTERY,0);
		//g_SimData->SetParamOnSimObject(REQUEST_ELECTRICAL_MASTER_BATTERY,1);
		if(!g_pSDAPI->GetN(AVIONICS_MASTER_SWITCH))
			send_key_event(KEY_TOGGLE_AVIONICS_MASTER,0);
		//SetAvionicsMasterSwitch(1);
		m_Volts=GetRapVolts27();
		m_Amps=GetRapAmps27()-m_TotalLoad;
		m_Available=true;
		double bv=g_SimData->GetParam(SIMPARAM_ELECTRICAL_BATTERY_VOLTAGE);
		g_SimData->SetParam(SIMPARAM_WRITE_ELECTRICAL_BATTERY_VOLTAGE,LimitValueMax(bv+=0.000600,BUS27_AMPS_DEFAULT));
		PWR_SET(PWR_RAP_AVAIL,1);
	} else if(AZS_GET(AZS_POWERSOURCE)==POWER_SOURCE_BAT) {
		if(!m_Batterys->IsAvailable()) {
			m_Volts		= 0;
			m_Amps		= 0;
			m_Available	= false;
		} else {
			if(!g_pSDAPI->GetN(MASTER_BATTERY))
				send_key_event(KEY_TOGGLE_MASTER_BATTERY,0);
			//g_SimData->SetParamOnSimObject(REQUEST_ELECTRICAL_MASTER_BATTERY,1);
			if(!g_pSDAPI->GetN(AVIONICS_MASTER_SWITCH))
				send_key_event(KEY_TOGGLE_AVIONICS_MASTER,0);
			//SetAvionicsMasterSwitch(1);
			m_Volts		= GetBatVolts27();
			m_Amps		= GetBatAmps27(); 
			m_Available	= (m_Volts>24); 
			double bv=g_SimData->GetParam(SIMPARAM_ELECTRICAL_BATTERY_VOLTAGE);
			if(g_pSDAPI->GetN(GENERATOR_ALTERNATOR_1_BUS_AMPS)||g_pSDAPI->GetN(GENERATOR_ALTERNATOR_2_BUS_AMPS)||g_pSDAPI->GetN(GENERATOR_ALTERNATOR_3_BUS_AMPS))
				g_SimData->SetParam(SIMPARAM_WRITE_ELECTRICAL_BATTERY_VOLTAGE,LimitValueMax(bv+=0.000200,BUS27_AMPS_DEFAULT));
		}
		PWR_SET(PWR_RAP_AVAIL,0);
	} else if(g_pSDAPI->GetN(GENERATOR_ALTERNATOR_1_BUS_AMPS)!=0||g_pSDAPI->GetN(GENERATOR_ALTERNATOR_2_BUS_AMPS)!=0||g_pSDAPI->GetN(GENERATOR_ALTERNATOR_3_BUS_AMPS)!=0) {
		if(!g_pSDAPI->GetN(MASTER_BATTERY))
			send_key_event(KEY_TOGGLE_MASTER_BATTERY,0);
		//g_SimData->SetParamOnSimObject(REQUEST_ELECTRICAL_MASTER_BATTERY,1);
		if(!g_pSDAPI->GetN(AVIONICS_MASTER_SWITCH))
			send_key_event(KEY_TOGGLE_AVIONICS_MASTER,0);
		//SetAvionicsMasterSwitch(1);
		m_Volts		= GetBatVolts27();
		m_Amps		= GetBatAmps27(); 
		m_Available	= true;
		double bv=g_SimData->GetParam(SIMPARAM_ELECTRICAL_BATTERY_VOLTAGE);
		if(g_pSDAPI->GetN(GENERATOR_ALTERNATOR_1_BUS_AMPS)||g_pSDAPI->GetN(GENERATOR_ALTERNATOR_2_BUS_AMPS)||g_pSDAPI->GetN(GENERATOR_ALTERNATOR_3_BUS_AMPS))
			g_SimData->SetParam(SIMPARAM_WRITE_ELECTRICAL_BATTERY_VOLTAGE,LimitValueMax(bv+=0.000200,BUS27_AMPS_DEFAULT));
	} else {
		if(g_pSDAPI->GetN(MASTER_BATTERY))
			send_key_event(KEY_TOGGLE_MASTER_BATTERY,0);
		//g_SimData->SetParamOnSimObject(REQUEST_ELECTRICAL_MASTER_BATTERY,0);
		if(g_pSDAPI->GetN(AVIONICS_MASTER_SWITCH))
			send_key_event(KEY_TOGGLE_AVIONICS_MASTER,0);
		//SetAvionicsMasterSwitch(0);
		m_Volts		= 0;
		m_Amps		= 0;
		m_Available	= false;
		PWR_SET(PWR_RAP_AVAIL,0);
	}
}

void SDBus27::Load(CIniFile *ini)
{
	m_GroundSupply->Load(ini);
}

void SDBus27::Save(CIniFile *ini)
{
	m_GroundSupply->Save(ini);
}

//////////////////////////////////////////////////////////////////////////

double SDSystemElectro::GetVCVolts36(double ndl)
{
	double var=ndl;

	if(var<=15)				return var*1.73;
	if(var> 15&&var<=30)	return 15*1.73+(var-15)*3.53;
	if(var> 30)           	return 15*1.73+( 30-15)*3.53+(var-30)*9.86;

	return 0;
}

double SDSystemElectro::GetVCVolts115(double ndl)
{
	double var=ndl;

	if(var<=90)				return var*0.86;
	if(var> 90&&var<=120)	return 90*0.86+(var-90)*2.13;
	if(var> 120)           	return 90*0.86+(120-90)*2.13+(var-120)*3;

	return 0;
}

SDSystemElectro::SDSystemElectro() :	m_Bus115(&m_Bus27),
										m_Bus36(&m_Bus27)
{
	xmlTT_V115		= auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_514"));
	xmlTT_V36 		= auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_515"));
	xmlTT_V27 		= auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_516"));
	xmlTT_A27 		= auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_517"));
	xmlTT_AG1 		= auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_518"));
	xmlTT_AG2 		= auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_519"));
	xmlTT_AG3 		= auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_520"));

	VCAnimAmps27	= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_amps27"));
	VCAnimAmpsGen1	= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_gen1"));
	VCAnimAmpsGen2	= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_gen2"));
	VCAnimAmpsGen3	= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_gen3"));

	VCAnimVolt27	= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_volts27"));
	VCAnimVolt36	= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_volts36"));
	VCAnimVolt115	= auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_volts115"));
};

SDSystemElectro::~SDSystemElectro() 
{
	SDBatterys::Release();
	SDGroundSupply::Release();
};

void SDSystemElectro::Init() 
{
};

void SDSystemElectro::Update()
{
	xmlTT_V115->set_number(NDL_GET(NDL_VOLTS115));
	xmlTT_V36 ->set_number(NDL_GET(NDL_VOLTS36));
	xmlTT_V27 ->set_number(NDL_GET(NDL_VOLTS27));
	xmlTT_A27 ->set_number(NDL_GET(NDL_AMPS27));
	xmlTT_AG1 ->set_number(NDL_GET(NDL_AMPSGEN1));
	xmlTT_AG2 ->set_number(NDL_GET(NDL_AMPSGEN2));
	xmlTT_AG3 ->set_number(NDL_GET(NDL_AMPSGEN3));

	VCAnimAmps27->set_degree(dgrd(NDL_GET(NDL_AMPS27)*4.4));
	VCAnimAmpsGen1->set_degree(dgrd(NDL_GET(NDL_AMPSGEN1)*4.4));
	VCAnimAmpsGen2->set_degree(dgrd(NDL_GET(NDL_AMPSGEN2)*4.4));
	VCAnimAmpsGen3->set_degree(dgrd(NDL_GET(NDL_AMPSGEN3)*4.4));

	VCAnimVolt27->set_degree(dgrd(NDL_GET(NDL_VOLTS27)*7.93));
	VCAnimVolt36->set_degree(dgrd(GetVCVolts36(NDL_GET(NDL_VOLTS36))));
	VCAnimVolt115->set_degree(dgrd(GetVCVolts115(NDL_GET(NDL_VOLTS115))));

	// Input
	int p1_glt_volts27  = (int)GLT_GET(GLT_VOLT27);
	int p1_glt_volts36  = (int)GLT_GET(GLT_VOLT36);
	int p1_glt_volts115 = (int)GLT_GET(GLT_VOLT115);

	int p1_azs_generator1 = (int)AZS_GET(AZS_GENERATOR1);
	int p1_azs_generator2 = (int)AZS_GET(AZS_GENERATOR2);
	int p1_azs_generator3 = (int)AZS_GET(AZS_GENERATOR3);

	// Output
	bool pa_bus27=false;
	bool pa_bus36=false;
	bool pa_bus115=false;

	bool pa_tbl_gen1_fail=false;
	bool pa_tbl_gen2_fail=false;
	bool pa_tbl_gen3_fail=false;

	double p1_ampsgen1=0;
	double p1_ampsgen2=0;
	double p1_ampsgen3=0;

	double p1_ndl_volts36=0;
	double p1_ndl_volts115=0;
	double p1_ndl_volts27=0;
	double p1_ndl_amps27=0;

	// Code
	m_Bus27.CalculateLoad(m_Bus115.GetTotalLoad(),m_Bus36.GetTotalLoad());

	m_Bus27.Update(); 

	m_Bus115.Update();
	m_Bus36.Update();

	pa_bus27=m_Bus27.IsAvailable();
	pa_bus115=m_Bus115.IsAvailable();
	pa_bus36=m_Bus36.IsAvailable();

	p1_ampsgen1	= g_pSDAPI->GetN(GENERATOR_ALTERNATOR_1_BUS_AMPS);
	p1_ampsgen2	= g_pSDAPI->GetN(GENERATOR_ALTERNATOR_2_BUS_AMPS);
	p1_ampsgen3	= g_pSDAPI->GetN(GENERATOR_ALTERNATOR_3_BUS_AMPS);

	switch(p1_glt_volts27) {
		case GALETA_27_GEN1:
			p1_ndl_volts27	= g_pSDAPI->GetN(GENERATOR_ALTERNATOR_1_BUS_VOLTAGE);
			p1_ndl_amps27	= m_Bus27.GetAmps();
			break;
		case GALETA_27_GEN2:
			p1_ndl_volts27	= g_pSDAPI->GetN(GENERATOR_ALTERNATOR_2_BUS_VOLTAGE);
			p1_ndl_amps27	= m_Bus27.GetAmps();
			break;
		case GALETA_27_GEN3:
			p1_ndl_volts27	= g_pSDAPI->GetN(GENERATOR_ALTERNATOR_3_BUS_VOLTAGE);
			p1_ndl_amps27	= m_Bus27.GetAmps();
			break;
		case GALETA_27_RAP:
			p1_ndl_volts27	= m_Bus27.GetRapVolts27();
			p1_ndl_amps27	= m_Bus27.GetRapAmps27();
			break;
		case GALETA_27_BATR:
			p1_ndl_volts27	= m_Bus27.GetBatVolts27(2);
			p1_ndl_amps27	= m_Bus27.GetBatAmps27(2);
			break;
		case GALETA_27_LINE:
			p1_ndl_volts27	= m_Bus27.GetVolts();
			p1_ndl_amps27	= m_Bus27.GetAmps();
			break;
		case GALETA_27_BATL:
			p1_ndl_volts27	= m_Bus27.GetBatVolts27(1);
			p1_ndl_amps27	= m_Bus27.GetBatAmps27(1);
			break;
	}

	p1_ndl_volts115	= m_Bus115.GetVolts115(p1_glt_volts115);
	p1_ndl_volts36	= m_Bus36.GetVolts36(p1_glt_volts36);

	if(p1_azs_generator1&&g_pSDAPI->GetN(TURB_ENGINE_1_CORRECTED_N2)>=25) {
		if(g_pSDAPI->GetN(GENERATOR_ALTERNATOR_1_BUS_AMPS)==0) {
			trigger_key_event(KEY_TOGGLE_ALTERNATOR1,0);
		}
	} else {
		if(g_pSDAPI->GetN(GENERATOR_ALTERNATOR_1_BUS_AMPS)!=0) {
			trigger_key_event(KEY_TOGGLE_ALTERNATOR1,0);
		}
	}

	if(p1_azs_generator2&&g_pSDAPI->GetN(TURB_ENGINE_2_CORRECTED_N2)>=25) {
		if(g_pSDAPI->GetN(GENERATOR_ALTERNATOR_2_BUS_AMPS)==0) {
			trigger_key_event(KEY_TOGGLE_ALTERNATOR2,0);
		}
	} else {
		if(g_pSDAPI->GetN(GENERATOR_ALTERNATOR_2_BUS_AMPS)!=0) {
			trigger_key_event(KEY_TOGGLE_ALTERNATOR2,0);
		}
	}

	if(p1_azs_generator3&&g_pSDAPI->GetN(TURB_ENGINE_3_CORRECTED_N2)>=25) {
		if(g_pSDAPI->GetN(GENERATOR_ALTERNATOR_3_BUS_AMPS)==0) {
			trigger_key_event(KEY_TOGGLE_ALTERNATOR3,0);
		}
	} else {
		if(g_pSDAPI->GetN(GENERATOR_ALTERNATOR_3_BUS_AMPS)!=0) {
			trigger_key_event(KEY_TOGGLE_ALTERNATOR3,0);
		}
	}

	static bool Gen1Played=false;
	static bool Gen2Played=false;
	static bool Gen3Played=false;

	pa_tbl_gen1_fail = (g_pSDAPI->GetN(TURB_ENGINE_1_CORRECTED_N2)<15||!p1_azs_generator1||!g_pSDAPI->GetN(GENERATOR_ALTERNATOR_1_BUS_AMPS));
	pa_tbl_gen2_fail = (g_pSDAPI->GetN(TURB_ENGINE_2_CORRECTED_N2)<15||!p1_azs_generator2||!g_pSDAPI->GetN(GENERATOR_ALTERNATOR_2_BUS_AMPS));
	pa_tbl_gen3_fail = (g_pSDAPI->GetN(TURB_ENGINE_3_CORRECTED_N2)<15||!p1_azs_generator3||!g_pSDAPI->GetN(GENERATOR_ALTERNATOR_3_BUS_AMPS));

	if(!pa_tbl_gen1_fail) {
		if(!Gen1Played) {
			SND_SET(SND_GEN1_NORMAL,1);
			Gen1Played=true;
		}
	} else {
		Gen1Played=false;
	}

	if(!pa_tbl_gen2_fail) {
		if(!Gen2Played) {
			SND_SET(SND_GEN1_NORMAL,1);
			Gen2Played=true;
		}
	} else {
		Gen2Played=false;
	}

	if(!pa_tbl_gen3_fail) {
		if(!Gen3Played) {
			SND_SET(SND_GEN1_NORMAL,1);
			Gen3Played=true;
		}
	} else {
		Gen3Played=false;
	}

	PWR_SET(PWR_BUS27 ,pa_bus27);
	PWR_SET(PWR_BUS36 ,pa_bus36);
	PWR_SET(PWR_BUS115,pa_bus115);

	TBL_SET(TBL_GEN1_FAIL,pa_tbl_gen1_fail);
	TBL_SET(TBL_GEN2_FAIL,pa_tbl_gen2_fail);
	TBL_SET(TBL_GEN3_FAIL,pa_tbl_gen3_fail);

	NDL_SET(NDL_AMPSGEN1,p1_ampsgen1);
	NDL_SET(NDL_AMPSGEN2,p1_ampsgen2);
	NDL_SET(NDL_AMPSGEN3,p1_ampsgen3);

	NDL_SET(NDL_VOLTS36,p1_ndl_volts36);
	NDL_SET(NDL_VOLTS115,p1_ndl_volts115);
	NDL_SET(NDL_VOLTS27,p1_ndl_volts27);
	NDL_SET(NDL_AMPS27,p1_ndl_amps27);
}

void SDSystemElectro::Load(CIniFile *ini)
{
	m_Bus27.Load(ini);
	m_Bus36.Load(ini);
	m_Bus115.Load(ini);
}

void SDSystemElectro::Save(CIniFile *ini)
{
	m_Bus27.Save(ini);
	m_Bus36.Save(ini);
	m_Bus115.Save(ini);
}

