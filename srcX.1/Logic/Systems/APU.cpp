/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Systems/APU.cpp $

  Last modification:
    $Date: 18.02.06 14:24 $
    $Revision: 2 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "APU.h"

#define SECOND			TICKS_PER_SEC
#define MINUTE			60*SECOND

#define	APU_START		SECOND
#define APU_PWR_NORM	6*SECOND
#define APU_FUEL_FIRE	9*SECOND
#define APU_START_AI9	10*SECOND
#define APU_RPM_NORM	20*SECOND			
#define APU_OIL_NORM	23*SECOND
#define APU_STARTED		30*SECOND
#define APU_FINISH		31*SECOND
#define APU_RESET		32*SECOND
#ifdef _DEBUG
#define APU_RPM_HIGH	3*MINUTE
#else
#define APU_RPM_HIGH	15*MINUTE
#endif

#define FUEL_COEFF1		0.0006
#define FUEL_COEFF2		0.0007
#define FUEL_COEFF3		0.0008
#define FUEL_COEFF4		0.0009
#define FUEL_COEFF5		0.0011
#define FUEL_COEFF6		0.0013

CAPUTemp::CAPUTemp()
{
	m_Ndl							= 0;
	m_Started						= false;
	m_Stoped						= false;
	m_Step1Coeff					= 2.0;
	m_Step2Coeff					= 1.5;
	m_Step3Coeff					= 3.0;
	m_MaxTemp						= 720;
}

void CAPUTemp::Update(bool m_Overheat)
{
	if(g_pSDAPI->GetN(TOTAL_AIR_TEMP)*256>15) 
		m_MaxTemp=720+g_pSDAPI->GetN(TOTAL_AIR_TEMP)*256/2;

	if(m_Started) {
		m_Ndl+=m_Step1Coeff;
		if(m_Ndl>m_MaxTemp)
			m_Started=false;
	}

	if(m_Stoped) {
		m_Ndl-=m_Step2Coeff;
		if(m_Ndl<0) {
			m_Ndl=0;
			m_Started	= false;
			m_Stoped	= false;
			SND_SET(SND_APU_OFF,1);
		}
	}

	if(m_Overheat) {
		m_Ndl+=m_Step3Coeff;
		if(m_Ndl>1000)
			m_Ndl=1000;
	}

	NDL_SET(NDL_APU_TEMP,m_Ndl);
}

void CAPUTemp::Startup()
{
	m_Started=true;
}

void CAPUTemp::Shutdown()
{
	m_Stoped=true;
	m_Started=false;
}

//////////////////////////////////////////////////////////////////////////

SDSystemAPU::SDSystemAPU()
{
}

SDSystemAPU::~SDSystemAPU()
{
}

void SDSystemAPU::Init()
{
	xml_TTAPUTemp=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_507"));
	VCAnimNdl=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_apu_temp"));

	m_StartForced=false;
	m_StopForced=false;
	m_TimerReseted=false;
	m_ProcessStarted=false;

	m_Timer=0;
	m_Timer2=0;
	m_Stage1Done=false;
	m_Overheat=false;

	m_TblRpmNorm		= TBL_GET(TBL_APU_RPM_NORM);
	m_TblOilPressNorm	= TBL_GET(TBL_APU_OIL_PRESS_NORM);
	//m_TblStarter		= TBL_GET(TBL_APU_STARTER);
	m_TblRpmHigh		= TBL_GET(TBL_APU_RPM_HIGH);
	m_TblFire			= TBL_GET(TBL_APU_ON_FIRE);
	//m_NdlAir			= NDL_GET(NDL_STARTER);
	m_ApuReady			= PWR_GET(PWR_APU_READY);

	m_AirStarter		= CAirStarter::Instance();

	SND_SET(SND_APU_START,0);

}

void SDSystemAPU::GetAllParams()
{
	m_Volt28		= PWR_GET(PWR_BUS27);
	m_Ignit			= AZS_GET(AZS_APU_IGNIT_BUS);
	m_EngInd[0]		= 1;//AZS_GET(AZS_ENG1_IND_BUS);
	m_EngInd[1]		= 1;//AZS_GET(AZS_ENG2_IND_BUS);
	m_EngInd[2]		= 1;//AZS_GET(AZS_ENG3_IND_BUS);
	m_Valve			= AZS_GET(AZS_APU_FUEL_VALVE);
	m_LeftPump		= PWR_GET(PWR_LEFT_PUMP);//AZS_GET(AZS_TOPL_LEV);
	m_RightPump		= 1;//AZS_GET(AZS_TOPL_PRAV);
	m_AzsStart		= AZS_GET(AZS_STARTAI9PWR);
	m_StartMode		= (int)AZS_GET(AZS_STARTAI9MODE);
	m_BtnStart		= BTN_GET(BTN_STARTAI9);
	m_BtnStop		= BTN_GET(BTN_STOPAI9);
}

void SDSystemAPU::SetAllParams()
{
	TBL_SET(TBL_APU_RPM_NORM		,m_TblRpmNorm		);
	TBL_SET(TBL_APU_OIL_PRESS_NORM	,m_TblOilPressNorm	);
	//TBL_SET(TBL_APU_STARTER			,m_TblStarter		);
	TBL_SET(TBL_APU_RPM_HIGH		,m_TblRpmHigh		);
	TBL_SET(TBL_APU_ON_FIRE			,m_TblFire			);
	//NDL_SET(NDL_STARTER				,m_NdlAir			);
	PWR_SET(PWR_APU_READY			,m_ApuReady			);

	if(m_TblFire&&SND_GET(SND_FIRE)!=2) {
		//send_key_event(KEY_STROBES_ON,0);
		SND_SET(SND_FIRE,1);
	} else if(!m_TblFire&&SND_GET(SND_FIRE)==1) {
		//send_key_event(KEY_STROBES_OFF,0);
		SND_SET(SND_FIRE,0);
	}

	if(BTN_GET(BTN_DIS_FIRE_ALARM)) {
		SND_SET(SND_FIRE,2);
	}
}

void SDSystemAPU::Update()
{
	xml_TTAPUTemp->set_number(NDL_GET(NDL_APU_TEMP));
	VCAnimNdl->set_degree(dgrd(NDL_GET(NDL_APU_TEMP)*0.273));

	TBL_SET(TBL_APU_FUEL_VALVE_OPEN,AZS_GET(AZS_APU_FUEL_VALVE));

	if(AZS_CHG(AZS_APU_FUEL_VALVE)) {
		if(PWR_GET(PWR_BUS27)&&AZS_GET(AZS_APU_FUEL_VALVE))
			SND_SET(SND_APU_VALVE_ON,1);
	}

	GetAllParams();

	m_Timer++;
	m_Timer2++;

	if(!m_Stage1Done) {
		m_AirStarter->Stop(AIR_STARTERS_APU);
		/*
		if(m_NdlAir>0) 
			m_NdlAir-=0.001;
		if(m_NdlAir<0)
			m_NdlAir=0;
		TBL_SET(TBL_AIR_STARTER_ON,0);
		*/
	}

	if(m_BtnStart&&!m_StartForced&&!m_Starter.GetTemp()&&!m_StopForced) {
		if(	!m_Volt28		||
			!m_Ignit		||
			!m_EngInd[0]	||
			!m_EngInd[1]	||
			!m_EngInd[2]	||
			!m_Valve		||
			!m_LeftPump		||
			!m_RightPump	||
			!m_AzsStart		||
			m_StartMode==0) {

				m_Stage1Done=false;
				m_ApuReady=0;
				SetAllParams();
				return;
		}

		if(!m_TimerReseted) {
			m_Timer=0;
			m_TimerReseted=true;
		}
		if(m_TimerReseted) {
			if(m_Timer>=3*18) {
				if(m_BtnStart) {
					m_StartForced=true;
					m_TimerReseted=false;
				}
			} else {
				if(!m_BtnStart) {
					m_StartForced=false;
					m_TimerReseted=false;
				}
			}
		}
	}

	if((m_BtnStop||!m_Ignit||!m_Valve||!m_LeftPump||!m_RightPump||!m_AzsStart)&&m_StartForced) {
		m_Stage1Done=false;
		m_ApuReady=0;
		m_StartForced=false;
		m_TimerReseted=false;
		m_StopForced=true;
		m_Starter.Shutdown();
	}

	m_Starter.Update(m_Overheat);

	double apuTemp=NDL_GET(NDL_APU_TEMP);

	if(apuTemp>750) {
		m_TblRpmHigh		= 1;
		m_TblRpmNorm		= 0;
	} else {
		m_TblRpmHigh		= 0;
	}

	if(apuTemp>800) {
		m_TblFire			= 1;
		m_TblOilPressNorm	= 0;
		trigger_key_event(KEY_TOGGLE_ENGINE2_FAILURE,0);
	}

	static bool pld1=false;
	static bool pld6=false;
	static bool pld9=false;
	static bool pld20=false;
	static bool pld23=false;
	static bool pld30=false;

	if(m_StopForced) {
		pld1=false;
		pld6=false;
		pld9=false;
		pld20=false;
		pld23=false;
		pld30=false;

		if(SND_GET(SND_APU_START)==2)
			SND_SET(SND_APU_START,3);

		TBL_SET(TBL_APU_STARTER			,0);
		m_Overheat=false;
		m_Stage1Done=false;
		m_ApuReady=0;
		/*
		if(m_AudioMgr->IsPlaying(SOUND_SCDS_AI9)) {
		m_AudioMgr->Stop(SOUND_SCDS_AI9);
		m_AudioMgr->PlayOne(SOUND_SCDS_STOP_AI9);
		}
		*/
		if(!m_TimerReseted) {
			m_Timer=0;
			m_TimerReseted=true;
		}
		double tmp=m_Starter.GetTemp();

		if(tmp>440&&tmp<442) {
			m_TblOilPressNorm		= 0;
		}
		if(tmp>401&&tmp<403) {
			m_TblRpmNorm			= 0;
		}
		if(tmp==0) {
			m_StartForced=false;
			m_TimerReseted=false;
			m_ProcessStarted=false;
			m_TblOilPressNorm		= 0;
			m_TblRpmNorm			= 0;
			//m_TblStarter			= 0;
			//TBL_SET(TBL_APU_STARTER			,0);
			m_TblRpmHigh			= 0;
			m_TblFire				= 0;
			m_StopForced			= false;
		}

	} else if(m_StartForced) {

		if(!m_TimerReseted) {
			m_Timer=0;
			m_ApuReady=0;
			m_TimerReseted=true;
		}

		static double coeff=0;

		static bool rpm_high_at_start=false;
		static int rpm_high_tick=0;

		int randres;

		switch(m_Timer) {
			case APU_START:
				if(!pld1) {
					SND_SET(SND_APU_START,1);
					SND_SET(SND_APU_SPINING,1);
					//m_AudioMgr->PlayOne(SOUND_APU_START);
					pld1=true;
				}
				TBL_SET(TBL_APU_STARTER			,1);
				//m_TblStarter			= 1;
				coeff=FUEL_COEFF1;

			break;
			case APU_PWR_NORM:
				if(!pld6) {
					//m_AudioMgr->PlayOne(SOUND_APU_PWR_NORM);
					pld6=true;
				}
				coeff=FUEL_COEFF2;
			break;
			case APU_FUEL_FIRE:
				if(!pld9) {
					SND_SET(SND_APU_TEMP_RAISE,1);
					//m_AudioMgr->PlayOne(SOUND_APU_FUEL_FIRE);
					//m_AudioMgr->PlayOne(SOUND_SCDS_START_AI9);
					pld9=true;
				}
				coeff=FUEL_COEFF3;

				m_Starter.Startup();

			break;
			case APU_START_AI9:
				//if(!m_AudioMgr->IsPlaying(SOUND_SCDS_START_AI9))m_AudioMgr->PlayOne(SOUND_SCDS_AI9,true);
			break;
			case APU_RPM_NORM:
				if(!pld20) {
					//m_AudioMgr->PlayOne(SOUND_APU_RPM_NORM);
					pld20=true;
				}
				m_TblRpmNorm			= 1;
				coeff=FUEL_COEFF4;
			break;
			case APU_OIL_NORM:
				if(!pld23) {
					SND_SET(SND_APU_OIL_PRESS,1);
					//m_AudioMgr->PlayOne(SOUND_APU_OIL_NORM);
					pld23=true;
				}
				m_TblOilPressNorm		= 1;
				coeff=FUEL_COEFF5;
			break;
			case APU_STARTED:
				if(!pld30) {
					SND_SET(SND_APU_STARTED,1);
					//m_AudioMgr->PlayOne(SOUND_APU_STARTED);
					pld30=true;
				}

				randres=rand()%50;

				if(randres==2) {
					rpm_high_at_start=true;
					rpm_high_tick=0;
				}

				if(rpm_high_at_start) {
					if(rpm_high_tick++<10) {
						m_TblRpmHigh		= 1;
						m_TblRpmNorm		= 0;
					} else {
						m_TblRpmHigh		= 0;
					}
				} else {
					m_TblRpmHigh		= 0;
				}

				TBL_SET(TBL_APU_STARTER			,0);
				//m_TblStarter				= 0;

				m_ProcessStarted=true;
				coeff=FUEL_COEFF6;
			break;
			case APU_FINISH:
				m_TblRpmHigh				= 0;
				rpm_high_at_start=false;
			break;
			case APU_RESET:
				pld1=false;
				pld6=false;
				pld9=false;
				pld20=false;
				pld23=false;
				pld30=false;
				m_Stage1Done=true;
				m_Timer2=0;
			break;
			case APU_RPM_HIGH:
				m_Overheat	 = true;
			break;
			default:
				g_SimData->SetParam(SIMPARAM_WRITE_FUEL_TANK_LEFT_MAIN_QUANTITY,g_SimData->GetParam(SIMPARAM_FUEL_TANK_LEFT_MAIN_QUANTITY)-coeff);

		}

		if(m_Stage1Done) {
			m_AirStarter->Start(AIR_STARTERS_APU);
		}
	}

	SetAllParams();
}

const string APU_INI_START_FORCED		= "APUSF";
const string APU_INI_STOP_FORCED		= "APUSTF";
const string APU_INI_TIMER_RESETED		= "APUTR";
const string APU_INI_PROCESS_STARTED	= "APUPS";
const string APU_INI_TIMER1				= "APUT1";
const string APU_INI_TIMER2				= "APUT2";
const string APU_INI_STAGE1DONE			= "APUS1D";
const string APU_INI_OVERHEAT  			= "APUO";
const string APU_INI_TEMP_STARTED		= "APUTS";
const string APU_INI_TEMP_STOPED 		= "APUTST";
const string APU_INI_TEMP_NDL    		= "APUTN";

void SDSystemAPU::Load(CIniFile *ini)
{
	m_StartForced		= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),APU_INI_START_FORCED.c_str()		,false	);
	m_StopForced		= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),APU_INI_STOP_FORCED.c_str()		,false	);
	m_TimerReseted		= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),APU_INI_TIMER_RESETED.c_str()	,false	);
	m_ProcessStarted	= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),APU_INI_PROCESS_STARTED.c_str()	,false	);
	m_Timer				= ini->ReadInt (SIM_FLT_MY_SECTION.c_str(),APU_INI_TIMER1.c_str()			,0		);
	m_Timer2			= ini->ReadInt (SIM_FLT_MY_SECTION.c_str(),APU_INI_TIMER2.c_str()			,0		);
	m_Stage1Done		= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),APU_INI_STAGE1DONE.c_str()		,false	);
	m_Overheat  		= ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),APU_INI_OVERHEAT.c_str()			,false	);
	m_Starter.Load(ini);
}

void SDSystemAPU::Save(CIniFile *ini)
{
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),APU_INI_START_FORCED.c_str()	,m_StartForced		);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),APU_INI_STOP_FORCED.c_str()		,m_StopForced		);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),APU_INI_TIMER_RESETED.c_str()	,m_TimerReseted		);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),APU_INI_PROCESS_STARTED.c_str()	,m_ProcessStarted	);
	ini->WriteInt 	(SIM_FLT_MY_SECTION.c_str(),APU_INI_TIMER1.c_str()			,m_Timer			);
	ini->WriteInt 	(SIM_FLT_MY_SECTION.c_str(),APU_INI_TIMER2.c_str()			,m_Timer2			);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),APU_INI_STAGE1DONE.c_str()		,m_Stage1Done		);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),APU_INI_OVERHEAT.c_str()		,m_Overheat			);
	m_Starter.Save(ini);
}

void CAPUTemp::Load(CIniFile *ini)
{
	m_Started	= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),APU_INI_TEMP_STARTED.c_str()	,false	);
	m_Stoped	= ini->ReadBool	 (SIM_FLT_MY_SECTION.c_str(),APU_INI_TEMP_STOPED.c_str()	,false	);
	m_Ndl		= ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),APU_INI_TEMP_NDL.c_str()		,0		);
}

void CAPUTemp::Save(CIniFile *ini)
{
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),APU_INI_TEMP_STARTED.c_str()	,m_Started	);
	ini->WriteBool	(SIM_FLT_MY_SECTION.c_str(),APU_INI_TEMP_STOPED.c_str()		,m_Stoped	);
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),APU_INI_TEMP_NDL.c_str()		,m_Ndl		);
}
