/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"
#include "Global.h"

//#pragma comment(lib,"SimConnect.lib")

#ifdef _DEBUG
_CrtMemState memoryState; 
#endif

CSimConnect     *g_SimData        = NULL;
HANDLE        hFileMapping      = NULL;
HINSTANCE     g_hInstance       = NULL;
HWND        g_hWND          = NULL;
bool        g_bDontLoadAtStartup  = false;
SIMCONNECT_PARAMS g_SimConnectParamIn;
SIMCONNECT_PARAMS g_SimConnectParamOut;

BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
#ifdef _DEBUG
  _CrtMemCheckpoint(&memoryState);
  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
  //_CrtSetBreakAlloc(194804);

#endif
  g_hInstance=(HINSTANCE)hModule;
  switch(ul_reason_for_call)  {
    case DLL_PROCESS_ATTACH: 
#ifdef _DEBUG
      AllocConsole();
#endif
      DisableThreadLibraryCalls((HMODULE)hModule);
      g_hWND=::FindWindowA("FS98MAIN", NULL);
    break;
    case DLL_THREAD_ATTACH: 
    break;
    case DLL_THREAD_DETACH:
    break;
    case DLL_PROCESS_DETACH:
#ifdef _DEBUG
    //  FreeConsole();
#endif
    break;
  }
  return TRUE;
}

void UpdatePanel(bool first)
{
  //sim1test();

  if(g_pSDAPI) 
  {
     g_pSDAPI->Update();
  }
  if(g_pGlobal)
  {
     g_pGlobal->Update(first);
  }
  POS_SET(POS_SOUND_MASTER, g_SimData->m_SoundMaster);
  POS_SET(POS_ACTIVE_VIEW_MODE, g_SimData->m_ActiveViewMode);
  POS_SET(POS_MAIN_IS_LOADING, !g_SimData->m_SimRunning);
}

void OpenPipe();
void ClosePipe();

void FSAPI fnInitPanel(void)
{
}

void FSAPI fnDeinitPanel(void)
{
  if(g_pGlobal) {
    delete g_pGlobal;
    g_pGlobal=NULL;
  }
  if(g_pSDAPI) {
    delete g_pSDAPI;
    g_pSDAPI=NULL;
  }
  unregister_all_named_vars();
  if(g_SimModules) {
    delete g_SimModules;
    g_SimModules=NULL;
  }

  ClosePipe();
#ifdef _DEBUG
  CHECK_MEM_LEAK("f:\\MemLeaks\\Yak40\\PA.log");
#endif

}

#ifdef GAUGE_NAME
#undef GAUGE_NAME
#endif

#ifdef GAUGEHDR_VAR_NAME
#undef GAUGEHDR_VAR_NAME
#endif

#ifdef GAUGE_W
#undef GAUGE_W
#endif

#define GAUGE_NAME         "logic"
#define GAUGEHDR_VAR_NAME  PA_hdr
#define GAUGE_W            MISC_DUMMY_SX

extern PELEMENT_HEADER logic_list;
GAUGE_CALLBACK logic_cb;
GAUGE_HEADER_FS700(GAUGE_W, GAUGE_NAME, &logic_list, 0, logic_cb, 0, 0, 0);
MAKE_STATIC(logic_bg,MISC_DUMMY,NULL,NULL,IMAGE_USE_TRANSPARENCY,0,0,0)
PELEMENT_HEADER logic_list = &logic_bg.header;

HANDLE  hSimConnect = NULL;

void FSAPI logic_cb( PGAUGEHDR pgauge, SINT32 service_id, UINT32 extra_data ) 
{
  switch(service_id)
  {
    case PANEL_SERVICE_CONNECT_TO_WINDOW:
      OpenPipe();
      if(!g_SimModules) {
        g_SimModules=new CSimModulesContainer();
      }
      register_var_by_name(&g_SDAPIEntrys,TYPE_PVOID,SDAPISIGN);

      if(!g_pSDAPI)
      {
        g_pSDAPI=new SDAPI(true);
        if(g_pSDAPI)
        {
          g_pSDAPI->Prepare();
          g_pSDAPI->Update();

      g_pGlobal=new SDGlobal();
          if(g_pGlobal)
          {
            g_pGlobal->Init();
          }
        }
      }

    CFG_SET(CFG_OBS_ZERO_START,g_pSDAPI->m_Config.m_CfgNav.m_bOBSZeroStart);
      UpdatePanel(true);
      
    break;

    case PANEL_SERVICE_PRE_UPDATE:
      UpdatePanel(false);
      break;
  }
}

GAUGESIMPORT  ImportTable =           
{                         
{ 0x0000000F, (PPANELS)NULL },            
{ 0x00000000, NULL }                
};                          

GAUGESLINKAGE Linkage={
  0x00000013,
  fnInitPanel,
  fnDeinitPanel,
  0,0,
  FS9LINK_VERSION,
  {
    (&PA_hdr),
    0
  }
};

void OpenPipe()
{
  hFileMapping=OpenFileMapping(FILE_MAP_READ|FILE_MAP_WRITE,FALSE,SIM_SHARE_NAME);

  if(hFileMapping==NULL) {
    g_bDontLoadAtStartup=true;
    return;
  }

  g_SimData=(CSimConnect *)MapViewOfFile(hFileMapping,FILE_MAP_READ|FILE_MAP_WRITE,0,0,sizeof(CSimConnect));

  if(g_SimData==NULL) {
    g_bDontLoadAtStartup=true;
    return;
  }
}

void ClosePipe()
{
  if(g_SimData) { 
    UnmapViewOfFile(g_SimData);
    g_SimData=NULL;
  }
  if(hFileMapping) {
    CloseHandle(hFileMapping);
    hFileMapping=NULL;
  }
}

