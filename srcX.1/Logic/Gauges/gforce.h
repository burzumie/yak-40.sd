/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/gforce.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

class SDGforceGauge : public SDLogicBase
{
private:
	bool m_SetByReset;
	double m_Min,m_Max,m_Cur;
	bool m_Powered;

	auto_ptr<CNamedVar> xml1;
	auto_ptr<CNamedVar> xml2;
	auto_ptr<CNamedVar> xml3;

	auto_ptr<CNamedVar> xmlAnimMin;
	auto_ptr<CNamedVar> xmlAnimMax;
	auto_ptr<CNamedVar> xmlAnimCur;

	void Reset();

public:
	SDGforceGauge();
	virtual ~SDGforceGauge();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

};
