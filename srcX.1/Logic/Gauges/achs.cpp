/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/achs.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "achs.h"

SDAchsGauge::SDAchsGauge()  
{
	m_MainSec=(g_pSDAPI->GetN(CLOCK_SECOND)*5)*1.2;
	m_MainMin=((g_pSDAPI->GetN(CLOCK_MINUTE)*5)+5/(60/g_pSDAPI->GetN(CLOCK_SECOND)))*1.2;
	m_MainHrs=((g_pSDAPI->GetN(CLOCK_HOUR)*5)+5/(60/g_pSDAPI->GetN(CLOCK_MINUTE)))*6;
	m_MiniSec=0;
	m_MiniSecl=0;
	m_MiniMin=0;
	m_MiniHrs=0;
	POS_SET(POS_ACHS_TIMER,0);
	POS_SET(POS_ACHS_BLENKER,0);
	m_Timer=0;
}

SDAchsGauge::~SDAchsGauge()
{
}

void SDAchsGauge::Init()
{
	MainSec=auto_ptr<CNamedVar>(new CNamedVar( "sd6015_ndl_MainSec"));
	MainMin=auto_ptr<CNamedVar>(new CNamedVar( "sd6015_ndl_MainMin"));
	MainHrs=auto_ptr<CNamedVar>(new CNamedVar( "sd6015_ndl_MainHrs"));
	MiniSec=auto_ptr<CNamedVar>(new CNamedVar( "sd6015_ndl_MiniSec"));
	MiniMin=auto_ptr<CNamedVar>(new CNamedVar( "sd6015_ndl_MiniMin"));
	MiniHrs=auto_ptr<CNamedVar>(new CNamedVar( "sd6015_ndl_MiniHrs"));
	BtnLeft=auto_ptr<CNamedVar>(new CNamedVar( "sd6015_btn_ClkLeft"));
	BtnRght=auto_ptr<CNamedVar>(new CNamedVar( "sd6015_btn_ClkRght"));
}

void SDAchsGauge::Update()
{
	if(m_Timer++<18) {
		return;
	}

	m_Timer=0;

	m_MainSec=(g_pSDAPI->GetN(CLOCK_SECOND)*5)*1.2;
	m_MainMin=((g_pSDAPI->GetN(CLOCK_MINUTE)*5)+5/(60/g_pSDAPI->GetN(CLOCK_SECOND)))*1.2;
	m_MainHrs=((g_pSDAPI->GetN(CLOCK_HOUR)*5)+5/(60/g_pSDAPI->GetN(CLOCK_MINUTE)))*6;
	if(POS_GET(POS_ACHS_TIMER)==1) {
		m_MiniSec++;
		if(m_MiniSec>30)m_MiniSec-=30;
	}
	if(POS_GET(POS_ACHS_TIMER)==0) {
		m_MiniSec=0;
	}
	if(POS_GET(POS_ACHS_BLENKER)==1) {
		m_MiniSecl++;
		if(m_MiniSecl>60) {
			m_MiniSecl-=60;
			m_MiniMin++;
			if(m_MiniMin>60) {
				m_MiniMin-=60;
				m_MiniHrs++;
				if(m_MiniHrs>12)m_MiniHrs-=12;
			}
		}
	}
	if(POS_GET(POS_ACHS_BLENKER)==0) {
		m_MiniSecl=0;
		m_MiniMin=0;
		m_MiniHrs=0;
	}
	NDL_SET(NDL_MAIN_SEC,m_MainSec);
	NDL_SET(NDL_MAIN_MIN,m_MainMin);
	NDL_SET(NDL_MAIN_HRS,m_MainHrs);
	NDL_SET(NDL_MINI_SEC,m_MiniSec);
	NDL_SET(NDL_MINI_MIN,m_MiniMin);
	NDL_SET(NDL_MINI_HRS,m_MiniHrs);
	NDL_SET(NDL_MINI_SEC_HIDE,m_MiniSecl);

	MainSec->set_degree(dgrd(m_MainSec));
	MainMin->set_degree(dgrd(m_MainMin));
	MainHrs->set_degree(dgrd(m_MainHrs));
	MiniSec->set_degree(dgrd(m_MiniSec*12));
	MiniMin->set_degree(dgrd(((m_MiniMin*5)+5/(60/m_MiniSecl))*1.2));
	MiniHrs->set_degree(dgrd(((m_MiniHrs*5)+5/(60/m_MiniMin))*6));
	BtnLeft->set_number(BTN_GET(BTN_CLOCK_HRONO));
	BtnRght->set_number(BTN_GET(BTN_CLOCK_START));

}

void SDAchsGauge::Load(CIniFile *ini)
{
	EString buf;

	buf="ACHSMS";
	m_MainSec=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MainSec);
	buf="ACHSMM";
	m_MainMin=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MainMin);
	buf="ACHSMH";
	m_MainHrs=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MainHrs);
	buf="ACHSMSL";
	m_MiniSecl=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MiniSecl);
	buf="ACHSMIS";
	m_MiniSec=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MiniSec);
	buf="ACHSMIM";
	m_MiniMin=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MiniMin);
	buf="ACHSMIH";
	m_MiniHrs=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MiniHrs);
	buf="ACHSTMR";
	m_Timer=ini->ReadInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Timer);
}

void SDAchsGauge::Save(CIniFile *ini)
{
	EString buf;

	buf="ACHSMS";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MainSec);
	buf="ACHSMM";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MainMin);
	buf="ACHSMH";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MainHrs);
	buf="ACHSMSL";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MiniSecl);
	buf="ACHSMIS";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MiniSec);
	buf="ACHSMIM";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MiniMin);
	buf="ACHSMIH";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_MiniHrs);
	buf="ACHSTMR";
	ini->WriteInt(SIM_FLT_MY_SECTION.c_str(),buf.c_str(),m_Timer);
}
