/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/achs.h $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

class SDAchsGauge : public SDLogicBase
{
private:
	double m_MainSec;
	double m_MainMin;
	double m_MainHrs;
	double m_MiniSecl;
	double m_MiniSec;
	double m_MiniMin;
	double m_MiniHrs;

	int m_Timer;

	auto_ptr<CNamedVar> MainSec;
	auto_ptr<CNamedVar> MainMin;
	auto_ptr<CNamedVar> MainHrs;
	auto_ptr<CNamedVar> MiniSec;
	auto_ptr<CNamedVar> MiniMin;
	auto_ptr<CNamedVar> MiniHrs;
	auto_ptr<CNamedVar> BtnLeft;
	auto_ptr<CNamedVar> BtnRght;

public:
	SDAchsGauge();
	virtual ~SDAchsGauge();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

};
