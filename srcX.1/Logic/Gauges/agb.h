/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/agb.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

class SDAGBGauge : public SDLogicBase
{
public:
	auto_ptr<CNamedVar> xml1;
	auto_ptr<CNamedVar> xml2;
	auto_ptr<CNamedVar> VCAnimPlane;
	auto_ptr<CNamedVar> VCAnimFlag;
	auto_ptr<CNamedVar> VCAnimBall;
	auto_ptr<CNamedVar> VCAnimPitch;
	auto_ptr<CNamedVar> VCAnimPitchPos;

	bool m_IsArret;
	bool m_IsPowered;
	bool m_IsArretForced;

	double m_StartPitch;
	double m_StartPlane;

	double m_PitchDelta;
	double m_PlaneDelta;

	double m_Pitch;
	double m_Plane;
	double m_Ball;
	double m_Flag;

	double ArretStepPitch;
	double ArretStepPlane;
	double ArretLimPitch;
	double ArretLimPlane;

	int m_AgbNum;

	SDAGBGauge();
	virtual ~SDAGBGauge();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);

};

class SDAGB1Gauge : public SDAGBGauge
{
public:
	SDAGB1Gauge();
	virtual void Update();
};

class SDAGB2Gauge : public SDAGBGauge
{
public:
	SDAGB2Gauge();
	virtual void Update();
};

class SDAGB3Gauge : public SDAGBGauge
{
public:
	SDAGB3Gauge();
	virtual void Update();
};
