/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/kppms.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "kppms.h"

SDKPPMSGauge::SDKPPMSGauge() 
{
	blenker_kurs=0;blenker_gliss=0;ils_kurs=0;ils_gliss=0;scale=0;have_course=false;have_glide=false;
}

SDKPPMSGauge::~SDKPPMSGauge()
{
}

void SDKPPMSGauge::Init()
{
	xml11=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_522"));
	xml21=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_504"));
	xml2=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_pa_531"));
	VCAnimHdg1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kppms0_hdg"));
	VCAnimHdg2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kppms1_hdg"));
	VCAnimScl1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kppms0_scl"));
	VCAnimScl2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kppms1_scl"));
	VCAnimGlide1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kppms0_glide"));
	VCAnimGlide2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kppms1_glide"));
	VCAnimCourse1=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kppms0_course"));
	VCAnimCourse2=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_kppms1_course"));

	m_LeftPlayed=false;
	m_RightPlayed=false;
	m_UpPlayed=false;
	m_DownPlayed=false;
	m_Ils0Played=false;
}

void SDKPPMSGauge::Update()
{
	xml2->set_number(NDL_GET(NDL_GMK_PRICOURSE));
	if(m_Nav==1) {
		scale=NDL_GET(NDL_KPPMS0_SCALE);

		FLAGS c=g_pSDAPI->GetF(VOR1_CODE);
		if(c&VOR_CODE_IS_LOCALIZER) {
			have_course=true;
			blenker_kurs=1;
		} else {
			have_course=false;
			blenker_kurs=0;
		}

		if(c&VOR_CODE_GLIDESLOPE) {
			have_glide=true;
			blenker_gliss=1;
		} else {
			have_glide=false;
			blenker_gliss=0;
		}

		if(have_course||AZS_GET(AZS_KMP_RCVR))ils_kurs=g_SimData->GetParam(SIMPARAM_NAV_CDI1); //LimitValue(g_pSDAPI->GetN(VOR1_NEEDLE)/127*30+NormaliseDegree360180(g_pSDAPI->GetN(VOR1_OBI)-g_pSDAPI->GetN(PLANE_HEADING_DEGREES_MAGNETIC)),-29.,29.)/30*29+g_pSDAPI->GetN(ATTITUDE_INDICATOR_BANK_DEGREES);
		else ils_kurs=0;//120; //29;
		if(have_glide&&GLT_GET(GLT_KMPSYS)!=1)ils_gliss=g_SimData->GetParam(SIMPARAM_NAV_GSI1);  //LimitValue(g_pSDAPI->GetN(VOR1_GS_NEEDLE)/127*30+g_pSDAPI->GetN(ATTITUDE_INDICATOR_PITCH_DEGREES),-29.,29.);
		else ils_gliss=0;//-120; //-29;

		if(/*GLT_GET(GLT_KMPSYS)==1*/AZS_GET(AZS_KMP_RCVR)) {
			blenker_kurs=0;
			blenker_gliss=0;
		}

		scale=dnorm(scale);

		int pwr27=PWR_GET(PWR_BUS27);
		int pwr115=PWR_GET(PWR_BUS115);
		int vorpwr=AZS_GET(AZS_KMP1PWR);
		int fr1=int(POS_GET(POS_NAV6_FREQ1));
		int fr2=int(POS_GET(POS_NAV6_FREQ2));
		int fr3=int(POS_GET(POS_NAV6_FREQ3));
		int fr4=int(POS_GET(POS_NAV6_FREQ4));
		int fr5=int(POS_GET(POS_NAV6_FREQ5));

		if(BTN_GET(BTN_KMP1TEST1)&&vorpwr&&pwr27&&pwr115&&fr1==1&&fr2==1&&fr3==0&&(fr4==1||fr4==3)&&fr5==0) {
			ils_kurs=-120;
			ils_gliss=120;
			blenker_kurs=1;
			blenker_gliss=1;
			TBL_SET(TBL_MARKER,1);
			TBL_SET(TBL_OUTER_MARKER,1);
			LMP_SET(LMP_K1,1);
			LMP_SET(LMP_G1,1);
			m_Ils0Played=false;
		} else if(BTN_GET(BTN_KMP1TEST2)&&vorpwr&&pwr27&&pwr115&&fr1==1&&fr2==1&&fr3==0&&(fr4==1||fr4==3)&&fr5==0) {
			ils_kurs=0;
			ils_gliss=0;
			blenker_kurs=1;
			blenker_gliss=1;
			TBL_SET(TBL_MARKER,1);
			TBL_SET(TBL_MIDDLE_MARKER,1);
			LMP_SET(LMP_K1,1);
			LMP_SET(LMP_G1,1);
			if(!m_Ils0Played) {
				SND_SET(SND_ILS0_CHECKED,1);
				m_Ils0Played=true;
			}
		} else if(BTN_GET(BTN_KMP1TEST3)&&vorpwr&&pwr27&&pwr115&&fr1==1&&fr2==1&&fr3==0&&(fr4==1||fr4==3)&&fr5==0) {
			ils_kurs=120;
			ils_gliss=-120;
			blenker_kurs=1;
			blenker_gliss=1;
			TBL_SET(TBL_MARKER,1);
			TBL_SET(TBL_INNER_MARKER,1);
			LMP_SET(LMP_K1,1);
			LMP_SET(LMP_G1,1);
			m_Ils0Played=false;
		}

		NDL_SET(NDL_KPPMS0_ILS_COURSE,ils_kurs);
		NDL_SET(NDL_KPPMS0_ILS_GLIDE,ils_gliss);
		POS_SET(POS_KPPMS0_BLK_COURSE,blenker_kurs);
		POS_SET(POS_KPPMS0_BLK_GLIDE,blenker_gliss);
		xml11->set_number(NDL_GET(NDL_KPPMS0_SCALE));
		VCAnimHdg1->set_degree(dgrd(NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS0_SCALE)));
		VCAnimScl1->set_degree(dgrd(NDL_GET(NDL_KPPMS0_SCALE)));
		VCAnimGlide1->set_degree(dgrd(NDL_GET(NDL_KPPMS0_ILS_GLIDE)/20*-1));
		VCAnimCourse1->set_degree(dgrd(NDL_GET(NDL_KPPMS0_ILS_COURSE)/20*-1));

		if(POS_GET(POS_DESCENT)&&NDL_GET(NDL_UVID0_HIDE)<300) {
			if(have_course) {
				if(ils_kurs<-30&&ils_kurs>-120) {
					if(!m_RightPlayed) {
						SND_SET(SND_ILS_RIGHT,1);
						m_RightPlayed=true;
						m_LeftPlayed=false;
					}
				} else if(ils_kurs>-1&&ils_kurs<=0) {
					m_RightPlayed=false;
					m_LeftPlayed=false;
				}
				if(ils_kurs>30&&ils_kurs<120) {
					if(!m_LeftPlayed) {
						SND_SET(SND_ILS_LEFT,1);
						m_RightPlayed=false;
						m_LeftPlayed=true;
					}
				} else if(ils_kurs<1&&ils_kurs>=0) {
					m_RightPlayed=false;
					m_LeftPlayed=false;
				}
			}
			if(have_glide) {
				if(ils_gliss<-30&&ils_gliss>-120) {
					if(!m_DownPlayed) {
						SND_SET(SND_ILS_DOWN,1);
						m_DownPlayed=true;
						m_UpPlayed=false;
					}
				} else if(ils_gliss>-1&&ils_gliss<=0) {
					m_DownPlayed=false;
					m_UpPlayed=false;
				}
				if(ils_gliss>30&&ils_gliss<120) {
					if(!m_UpPlayed) {
						SND_SET(SND_ILS_UP,1);
						m_DownPlayed=false;
						m_UpPlayed=true;
					}
				} else if(ils_gliss<1&&ils_gliss>=0) {
					m_DownPlayed=false;
					m_UpPlayed=false;
				}
			}
		}
	} else if(m_Nav==2) {
		scale=NDL_GET(NDL_KPPMS1_SCALE);

		FLAGS c=g_pSDAPI->GetF(VOR2_CODE);
		if(c&VOR_CODE_IS_LOCALIZER) {
			have_course=true;
			blenker_kurs=1;
		} else {
			have_course=false;
			blenker_kurs=0;
		}

		if(c&VOR_CODE_GLIDESLOPE) {
			have_glide=true;
			blenker_gliss=1;
		} else {
			have_glide=false;
			blenker_gliss=0;
		}

		if(have_course||AZS_GET(AZS_KMP_RCVR))ils_kurs=g_SimData->GetParam(SIMPARAM_NAV_CDI2); //LimitValue(g_pSDAPI->GetN(VOR2_NEEDLE)/127*30+NormaliseDegree360180(g_pSDAPI->GetN(VOR2_OBI)-g_pSDAPI->GetN(PLANE_HEADING_DEGREES_MAGNETIC)),-29.,29.)/30*29+g_pSDAPI->GetN(ATTITUDE_INDICATOR_BANK_DEGREES);
		else ils_kurs=0;//120; //29;
		if(have_glide&&GLT_GET(GLT_KMPSYS)!=1)ils_gliss=g_SimData->GetParam(SIMPARAM_NAV_GSI2); //LimitValue(g_pSDAPI->GetN(VOR2_GS_NEEDLE)/127*30+g_pSDAPI->GetN(ATTITUDE_INDICATOR_PITCH_DEGREES),-29.,29.);
		else ils_gliss=0;//-120; //-29;

		if(AZS_GET(AZS_KMP_RCVR)/*GLT_GET(GLT_KMPSYS)==1*/) {
			blenker_kurs=0;
			blenker_gliss=0;
		}

		scale=dnorm(scale);

		int pwr27=PWR_GET(PWR_BUS27);
		int pwr115=PWR_GET(PWR_BUS115);
		int vorpwr=AZS_GET(AZS_KMP2PWR);
		int fr1=int(POS_GET(POS_NAV7_FREQ1));
		int fr2=int(POS_GET(POS_NAV7_FREQ2));
		int fr3=int(POS_GET(POS_NAV7_FREQ3));
		int fr4=int(POS_GET(POS_NAV7_FREQ4));
		int fr5=int(POS_GET(POS_NAV7_FREQ5));

		if(BTN_GET(BTN_KMP2TEST1)&&vorpwr&&pwr27&&pwr115&&fr1==1&&fr2==1&&fr3==0&&(fr4==1||fr4==3)&&fr5==0) {
			ils_kurs=-120;
			ils_gliss=120;
			blenker_kurs=1;
			blenker_gliss=1;
			TBL_SET(TBL_MARKER,1);
			TBL_SET(TBL_OUTER_MARKER,1);
			LMP_SET(LMP_K2,1);
			LMP_SET(LMP_G2,1);
			m_Ils0Played=false;
		} else if(BTN_GET(BTN_KMP2TEST2)&&vorpwr&&pwr27&&pwr115&&fr1==1&&fr2==1&&fr3==0&&(fr4==1||fr4==3)&&fr5==0) {
			ils_kurs=0;
			ils_gliss=0;
			blenker_kurs=1;
			blenker_gliss=1;
			TBL_SET(TBL_MARKER,1);
			TBL_SET(TBL_MIDDLE_MARKER,1);
			LMP_SET(LMP_K2,1);
			LMP_SET(LMP_G2,1);
			if(!m_Ils0Played) {
				SND_SET(SND_ILS0_CHECKED,1);
				m_Ils0Played=true;
			}
		} else if(BTN_GET(BTN_KMP2TEST3)&&vorpwr&&pwr27&&pwr115&&fr1==1&&fr2==1&&fr3==0&&(fr4==1||fr4==3)&&fr5==0) {
			ils_kurs=120;
			ils_gliss=-120;
			blenker_kurs=1;
			blenker_gliss=1;
			TBL_SET(TBL_MARKER,1);
			TBL_SET(TBL_INNER_MARKER,1);
			LMP_SET(LMP_K2,1);
			LMP_SET(LMP_G2,1);
			m_Ils0Played=false;
		}

		NDL_SET(NDL_KPPMS1_ILS_COURSE,ils_kurs);
		NDL_SET(NDL_KPPMS1_ILS_GLIDE,ils_gliss);
		POS_SET(POS_KPPMS1_BLK_COURSE,blenker_kurs);
		POS_SET(POS_KPPMS1_BLK_GLIDE,blenker_gliss);
		xml21->set_number(NDL_GET(NDL_KPPMS1_SCALE));
		VCAnimHdg2->set_degree(dgrd(NDL_GET(NDL_GMK_PRICOURSE)+NDL_GET(NDL_KPPMS1_SCALE)));
		VCAnimScl2->set_degree(dgrd(NDL_GET(NDL_KPPMS1_SCALE)));
		VCAnimGlide2->set_degree(dgrd(NDL_GET(NDL_KPPMS1_ILS_GLIDE)/20*-1));
		VCAnimCourse2->set_degree(dgrd(NDL_GET(NDL_KPPMS1_ILS_COURSE)/20*-1));
	} else {
		blenker_kurs=1;
		blenker_gliss=1;
		ils_kurs=0;
		ils_gliss=0;
		scale=0;
		have_course=false;
		have_glide=false;
	}
}

void SDKPPMSGauge::Load(CIniFile *ini)
{
}

void SDKPPMSGauge::Save(CIniFile *ini)
{
}
