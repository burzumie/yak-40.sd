/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/htail.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "htailgau.h"

SDHtailGauge::SDHtailGauge() 
{
}

SDHtailGauge::~SDHtailGauge()
{
}

void SDHtailGauge::Init()
{
	m_Htail=CHtail::Instance();
}

void SDHtailGauge::Update()
{
	NDL_SET(NDL_HTAIL_DEG,(PWR_GET(PWR_BUS36)&&AZS_GET(AZS_MANOM))?m_Htail->Get():-3);
}

void SDHtailGauge::Load(CIniFile *ini)
{
}

void SDHtailGauge::Save(CIniFile *ini)
{
}
