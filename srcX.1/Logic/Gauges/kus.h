/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

$Archive: /6015v2.root/6015v2/Src/Logic/Gauges/kus.h $

Last modification:
$Date: 18.02.06 12:05 $
$Revision: 1 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "../Main.h"
#include "../LogicBase.h"

class SDKUSGauge : public SDLogicBase
{
private:
	auto_ptr<CNamedVar> VCAnimIas;
	auto_ptr<CNamedVar> VCAnimTas;

	double GetXmlIasRus(double ndl);
	double GetXmlTasRus(double ndl);
	double GetXmlIasEng(double ndl);
	double GetXmlTasEng(double ndl);

public:
	SDKUSGauge();
	virtual ~SDKUSGauge();

	virtual void Init();
	virtual void Update();
	virtual void Load(CIniFile *ini);
	virtual void Save(CIniFile *ini);
};
