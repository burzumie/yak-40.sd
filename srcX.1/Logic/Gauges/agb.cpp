/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator 2004

  $Archive: /6015v2.root/6015v2/Src/Logic/Gauges/agb.cpp $

  Last modification:
    $Date: 18.02.06 12:05 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "agb.h"

const int ARRET_TICKS=3240;

SDAGBGauge::SDAGBGauge() 
{
}

SDAGBGauge::~SDAGBGauge()
{
}

void SDAGBGauge::Init()
{
	ArretStepPitch=0;
	ArretStepPlane=0;
	ArretLimPitch=0;
	ArretLimPlane=0;

	m_IsArret=false;
	m_IsPowered=false;
	m_IsArretForced=false;

	int TmpRand=rand()%30;

	if(TmpRand>=15)
		TmpRand=abs(TmpRand-80);
	else
		TmpRand*=-1;

	m_StartPitch=TmpRand;

	TmpRand=rand()%180;

	if(TmpRand>=90)
		TmpRand=abs(TmpRand-180);
	else
		TmpRand*=-1;

	m_StartPlane=TmpRand;

	m_Pitch=m_StartPitch;
	m_Plane=m_StartPlane;

	m_PitchDelta=fabs(m_Pitch/ARRET_TICKS);
	m_PlaneDelta=fabs(m_Plane/ARRET_TICKS);

	m_Flag=0;
}

void SDAGBGauge::Update()
{

	if(m_IsPowered) {
		if(!m_IsArret) {
			if(m_IsArretForced) { // ������ ������ �������������
				ArretStepPitch=1;
				ArretStepPlane=1;
				ArretLimPitch=1;
				ArretLimPlane=1;
			} else { // ����� ����� ����� ����������
				ArretStepPitch=m_PitchDelta;
				ArretStepPlane=m_PlaneDelta;
				ArretLimPitch=0.3;
				ArretLimPlane=0.3;
			}
			
			if(m_Pitch>=ArretLimPitch)
				m_Pitch-=ArretStepPitch;
			else if(m_Pitch<=-ArretLimPitch) 
				m_Pitch+=ArretStepPitch;
			else 
				m_Pitch=0;

			if(m_Plane>=ArretLimPlane)
				m_Plane-=ArretStepPlane;
			else if(m_Plane<=-ArretLimPlane) 
				m_Plane+=ArretStepPlane;
			else 
				m_Plane=0;

			if(!m_Plane&&!m_Pitch) {
				m_IsArret=true;
			}
		} else { // if(!m_IsArret) 

		}

		m_Flag=-90;
	} else { // if(m_IsPowered) 
		if(m_IsArretForced) { // ������ ������ �������������
			ArretStepPitch=1;
			ArretStepPlane=1;
			ArretLimPitch=1;
			ArretLimPlane=1;

			if(m_Pitch>=ArretLimPitch)
				m_Pitch-=ArretStepPitch;
			else if(m_Pitch<=-ArretLimPitch) 
				m_Pitch+=ArretStepPitch;
			else 
				m_Pitch=0;

			if(m_Plane>=ArretLimPlane)
				m_Plane-=ArretStepPlane;
			else if(m_Plane<=-ArretLimPlane) 
				m_Plane+=ArretStepPlane;
			else 
				m_Plane=0;

		} else { // ����� ����� ����� �������������
			ArretStepPitch=m_PitchDelta;
			ArretStepPlane=m_PlaneDelta;
			ArretLimPitch=0.3;
			ArretLimPlane=0.3;

			m_IsArret=false;

			if(m_Pitch<m_StartPitch)
				m_Pitch+=ArretStepPitch;
			else
				m_Pitch-=ArretStepPitch;

			if(m_Plane<m_StartPlane)
				m_Plane+=ArretStepPlane;
			else
				m_Plane-=ArretStepPlane;

		}

		m_Flag=0;
	}

	m_Ball = g_pSDAPI->GetN(TURN_COORDINATOR_BALL_POS)*-1;
}

void SDAGBGauge::Load(CIniFile *ini)
{
	std::string idx,str;
	switch(m_AgbNum) {
		case 1:
			idx="AGB1";
		break;
		case 2:
			idx="AGB2";
		break;
		case 3:
			idx="AGB3";
	    break;
	}

	str=idx+"IA";
	m_IsArret=ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),str.c_str(),false);
	str=idx+"IP";
	m_IsPowered=ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),str.c_str(),false);
	str=idx+"IAF";
	m_IsArretForced=ini->ReadBool(SIM_FLT_MY_SECTION.c_str(),str.c_str(),false);

	str=idx+"SPI";
	m_StartPitch=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_StartPitch);
	str=idx+"SPL";
	m_StartPlane=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_StartPlane);

	str=idx+"PID";
	m_PitchDelta=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_PitchDelta);
	str=idx+"PLD";
	m_PlaneDelta=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_PlaneDelta);

	str=idx+"PI";
	m_Pitch=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_Pitch);
	str=idx+"PL";
	m_Plane=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_Plane);
	str=idx+"BL";
	m_Ball=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_Ball);
	str=idx+"FL";
	m_Flag=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_Flag);

	str=idx+"ASPI";
	ArretStepPitch=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),0);
	str=idx+"ASPL";
	ArretStepPlane=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),0);
	str=idx+"ALPI";
	ArretLimPitch=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),0);
	str=idx+"ALPL";
	ArretLimPlane=ini->ReadDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),0);
}

void SDAGBGauge::Save(CIniFile *ini)
{
	std::string idx,str;
	switch(m_AgbNum) {
		case 1:
			idx="AGB1";
			break;
		case 2:
			idx="AGB2";
			break;
		case 3:
			idx="AGB3";
			break;
	}

	str=idx+"IA";
	ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_IsArret);
	str=idx+"IP";
	ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_IsPowered);
	str=idx+"IAF";
	ini->WriteBool(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_IsArretForced);

	str=idx+"SPI";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_StartPitch);
	str=idx+"SPL";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_StartPlane);

	str=idx+"PID";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_PitchDelta);
	str=idx+"PLD";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_PlaneDelta);

	str=idx+"PI";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_Pitch);
	str=idx+"PL";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_Plane);
	str=idx+"BL";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_Ball);
	str=idx+"FL";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),m_Flag);

	str=idx+"ASPI";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),ArretStepPitch);
	str=idx+"ASPL";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),ArretStepPlane);
	str=idx+"ALPI";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),ArretLimPitch);
	str=idx+"ALPL";
	ini->WriteDouble(SIM_FLT_MY_SECTION.c_str(),str.c_str(),ArretLimPlane);
}

//////////////////////////////////////////////////////////////////////////
SDAGB1Gauge::SDAGB1Gauge()
{
	xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_502"));
	xml2=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_406"));
	VCAnimPlane=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb0_plane"));
	VCAnimFlag=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb0_flag"));
	VCAnimBall=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb0_ball"));
	VCAnimPitch=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb0_pitch"));
	VCAnimPitchPos=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb0_pitch_pos"));
	m_AgbNum=1;
}

void SDAGB1Gauge::Update()
{
	if(PWR_GET(PWR_BUS36)&&AZS_GET(AZS_CAPT_ADI_MAIN_BUS))
		m_IsPowered=true;
	else
		m_IsPowered=false;

	if(BTN_GET(BTN_AGB_MAIN_ARRET0))
		m_IsArretForced=true;
	else
		m_IsArretForced=false;

	SDAGBGauge::Update();

	NDL_SET(NDL_AGB0_MAIN_PLANE	,m_Plane	+ g_pSDAPI->GetN(PLANE_BANK_DEGREES)*-1);
	NDL_SET(NDL_AGB0_MAIN_FLAG	,m_Flag);
	NDL_SET(NDL_AGB0_MAIN_BALL	,m_Ball);
	POS_SET(POS_AGB0_MAIN_PITCH	,m_Pitch	+ g_pSDAPI->GetN(PLANE_PITCH_DEGREES));

	int krmchg=KRM_CHG(KRM_AGB_MAIN0);
	if(krmchg<0) {
		m_Pitch-=0.5;
	} else if(krmchg>0) {
		m_Pitch+=0.5;
	}

	xml1->set_number(NDL_GET(NDL_AGB0_MAIN_PLANE));
	xml2->set_number(POS_GET(POS_AGB0_MAIN_PITCH));
	VCAnimPlane->set_degree(dgrd(NDL_GET(NDL_AGB0_MAIN_PLANE)));
	VCAnimFlag->set_degree(dgrd(NDL_GET(NDL_AGB0_MAIN_FLAG)+90));
	VCAnimBall->set_degree(dgrd(NDL_GET(NDL_AGB0_MAIN_BALL)*0.0826));
	VCAnimPitch->set_degree(dgrd(POS_GET(POS_AGB0_MAIN_PITCH)*1.38));
	VCAnimPitchPos->set_number(POS_GET(POS_AGB0_MAIN_PITCH)*-1);
}

//////////////////////////////////////////////////////////////////////////
SDAGB2Gauge::SDAGB2Gauge()
{
	xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_505"));
	xml2=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p0_407"));
	VCAnimPlane=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb1_plane"));
	VCAnimFlag=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb1_flag"));
	VCAnimBall=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb1_ball"));
	VCAnimPitch=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb1_pitch"));
	VCAnimPitchPos=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb1_pitch_pos"));
	m_AgbNum=2;
}

void SDAGB2Gauge::Update()
{
	if((PWR_GET(PWR_BUS36)||PWR_GET(PWR_BUS27))&&AZS_GET(AZS_CAPT_ADI_BACKUP_BUS))
		m_IsPowered=true;
	else
		m_IsPowered=false;

	if(BTN_GET(BTN_AGB_AUXL_ARRET))
		m_IsArretForced=true;
	else
		m_IsArretForced=false;

	SDAGBGauge::Update();

	NDL_SET(NDL_AGB_AUXL_PLANE	,m_Plane	+ g_pSDAPI->GetN(PLANE_BANK_DEGREES)*-1);
	NDL_SET(NDL_AGB_AUXL_FLAG	,m_Flag);
	NDL_SET(NDL_AGB_AUXL_BALL	,m_Ball);
	POS_SET(POS_AGB_AUXL_PITCH	,m_Pitch	+ g_pSDAPI->GetN(PLANE_PITCH_DEGREES));

	int krmchg=KRM_CHG(KRM_AGB_AUXL);
	if(krmchg<0) {
		m_Pitch-=0.5;
	} else if(krmchg>0) {
		m_Pitch+=0.5;
	}

	xml1->set_number(NDL_GET(NDL_AGB_AUXL_PLANE));
	xml2->set_number(POS_GET(POS_AGB_AUXL_PITCH));
	VCAnimPlane->set_degree(dgrd(NDL_GET(NDL_AGB_AUXL_PLANE)));
	VCAnimFlag->set_degree(dgrd(NDL_GET(NDL_AGB_AUXL_FLAG)+90));
	VCAnimBall->set_degree(dgrd(NDL_GET(NDL_AGB_AUXL_BALL)*0.0826));
	VCAnimPitch->set_degree(dgrd(POS_GET(POS_AGB_AUXL_PITCH)*1.38));
	VCAnimPitchPos->set_number(POS_GET(POS_AGB_AUXL_PITCH)*-1);
}

//////////////////////////////////////////////////////////////////////////
SDAGB3Gauge::SDAGB3Gauge()
{
	xml1=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_502"));
	xml2=auto_ptr<CNamedVar>(new CNamedVar("sd_6015_p1_406"));
	VCAnimPlane=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb2_plane"));
	VCAnimFlag=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb2_flag"));
	VCAnimBall=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb2_ball"));
	VCAnimPitch=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb2_pitch"));
	VCAnimPitchPos=auto_ptr<CNamedVar>(new CNamedVar("sd6015_ndl_agb2_pitch_pos"));
	m_AgbNum=3;
}

void SDAGB3Gauge::Update()
{
	if(PWR_GET(PWR_BUS36)&&AZS_GET(AZS_COPLT_ADI_BUS))
		m_IsPowered=true;
	else
		m_IsPowered=false;

	if(BTN_GET(BTN_AGB_MAIN_ARRET1))
		m_IsArretForced=true;
	else
		m_IsArretForced=false;

	SDAGBGauge::Update();

	NDL_SET(NDL_AGB1_MAIN_PLANE	,m_Plane	+ g_pSDAPI->GetN(PLANE_BANK_DEGREES)*-1);
	NDL_SET(NDL_AGB1_MAIN_FLAG	,m_Flag);
	NDL_SET(NDL_AGB1_MAIN_BALL	,m_Ball);
	POS_SET(POS_AGB1_MAIN_PITCH	,m_Pitch	+ g_pSDAPI->GetN(PLANE_PITCH_DEGREES));

	int krmchg=KRM_CHG(KRM_AGB_MAIN1);
	if(krmchg<0) {
		m_Pitch-=0.5;
	} else if(krmchg>0) {
		m_Pitch+=0.5;
	}

	xml1->set_number(NDL_GET(NDL_AGB1_MAIN_PLANE));
	xml2->set_number(POS_GET(POS_AGB1_MAIN_PITCH));
	VCAnimPlane->set_degree(dgrd(NDL_GET(NDL_AGB1_MAIN_PLANE)));
	VCAnimFlag->set_degree(dgrd(NDL_GET(NDL_AGB1_MAIN_FLAG)+90));
	VCAnimBall->set_degree(dgrd(NDL_GET(NDL_AGB1_MAIN_BALL)*0.0826));
	VCAnimPitch->set_degree(dgrd(POS_GET(POS_AGB1_MAIN_PITCH)*1.38));
	VCAnimPitchPos->set_number(POS_GET(POS_AGB1_MAIN_PITCH)*-1);
}
