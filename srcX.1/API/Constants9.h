/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Constants.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

const string REGISTRY_SIM_PATH				= "SetupPath";
const string SIM_FLT_EXTENSION				= "flt";
const string SIM_FLT_GENERATED				= "Program generated temporary flight."+SIM_FLT_EXTENSION;
const string SIM_FLT_PREVIOUS				= "Previous flight."+SIM_FLT_EXTENSION;
const string SIM_FLT_MY_SECTION				= "SD Yak-40 6015";
const string SIM_LANG_MODULE				= "language.dll";

const string MY_GAUGE_PATH_PLAIN			= "..\\..\\..\\..\\";
const string MY_PATH						= "SuprunovDesign\\Yak-40\\";
const string MY_SOUND						= "Sound";
const string MY_SOUND_PATH					= MY_PATH+MY_SOUND;
const string MY_CONFIG_PATH					= MY_PATH+"Config";
const string MY_CONFIG_FILE					= "config.ini";
const string MY_SOUND_CONFIG_FILE			= "sound.ini";

