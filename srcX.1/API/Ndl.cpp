/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Ndl.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Ndl.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_pa_501	""                                
#define sd_6015_pa_502	""                                
#define sd_6015_pa_503	""                                
#define sd_6015_pa_504	""                                
#define sd_6015_pa_505	""                                
#define sd_6015_pa_506	""                                
#define sd_6015_pa_507	""                                
#define sd_6015_pa_508	"Fuel PSI: %(L:sd_6015_pa_508,number)%, Oil PSI: %(L:sd_6015_pa_509,number)%, Oil temp: @(round(%(L:sd_6015_pa_510,number)%,0))@ C, @(round(cels2fahr(%(L:sd_6015_pa_510,number)%),0))@ F"
#define sd_6015_pa_509	""                                
#define sd_6015_pa_510	""                                
#define sd_6015_pa_511	"Fuel PSI: %(L:sd_6015_pa_511,number)%, Oil PSI: %(L:sd_6015_pa_512,number)%, Oil temp: @(round(%(L:sd_6015_pa_513,number)%,0))@ C, @(round(cels2fahr(%(L:sd_6015_pa_513,number)%),0))@ F"
#define sd_6015_pa_512	""                                
#define sd_6015_pa_513	""                                
#define sd_6015_pa_514	"Fuel PSI: %(L:sd_6015_pa_514,number)%, Oil PSI: %(L:sd_6015_pa_515,number)%, Oil temp: @(round(%(L:sd_6015_pa_516,number)%,0))@ C, @(round(cels2fahr(%(L:sd_6015_pa_516,number)%),0))@ F"
#define sd_6015_pa_515	""                                
#define sd_6015_pa_516	""                                
#define sd_6015_pa_517	"N1: %(L:sd_6015_pa_517,integer)%, N2: %(L:sd_6015_pa_518,integer)%"
#define sd_6015_pa_518	""                                
#define sd_6015_pa_519	"N1: %(L:sd_6015_pa_519,integer)%, N2: %(L:sd_6015_pa_520,integer)%"
#define sd_6015_pa_520	""                                
#define sd_6015_pa_521	"N1: %(L:sd_6015_pa_521,integer)%, N2: %(L:sd_6015_pa_522,integer)%"
#define sd_6015_pa_522	""                                
#define sd_6015_pa_523	"Stabilizer pos: %(L:sd_6015_pa_523,number)% degrees"
#define sd_6015_pa_524	"Celsius: @(round(%(L:sd_6015_pa_524,number)%,0))@ Fahrenheit: @(round(cels2fahr(%(L:sd_6015_pa_524,number)%),0))@"
#define sd_6015_pa_525	"Celsius: @(round(%(L:sd_6015_pa_525,number)%,0))@ Fahrenheit: @(round(cels2fahr(%(L:sd_6015_pa_525,number)%),0))@"
#define sd_6015_pa_526	"Celsius: @(round(%(L:sd_6015_pa_526,number)%,0))@ Fahrenheit: @(round(cels2fahr(%(L:sd_6015_pa_526,number)%),0))@"
#define sd_6015_pa_527	"Flaps pos: @(round(%(L:sd_6015_pa_527,number)%,0))@ degrees"
#define sd_6015_pa_528	"TAS: @(round(%(A:XML_AIRSPEED_TRUE,knots)%,0))@ knots, IAS: @(round(%(A:XML_AIRSPEED_INDICATED,knots)%,0))@ knots"
#define sd_6015_pa_529	""                                
#define sd_6015_pa_530	""                                
#define sd_6015_pa_531	""                                
#define sd_6015_pa_532	""                                
#define sd_6015_pa_533	""                                
#define sd_6015_pa_534	""                                
#define sd_6015_pa_535	""                                
#define sd_6015_pa_536	""                                
#define sd_6015_pa_537	"TAS: @(round(%(A:XML_AIRSPEED_TRUE,knots)%,0))@ knots, IAS: @(round(%(A:XML_AIRSPEED_INDICATED,knots)%,0))@ knots"
#define sd_6015_pa_538	""                                
#define sd_6015_pa_539	""                                


#define sd_6015_p0_501	""                                
#define sd_6015_p0_502	"Bank: @(abs(%(L:sd_6015_p0_502,number)%)>180?abs(abs(%(L:sd_6015_p0_502,number)%)-360):abs(%(L:sd_6015_p0_502,number)%))@, Pitch: @(neg(%(L:sd_6015_p0_406,number)%))@"
#define sd_6015_p0_503	""                                
#define sd_6015_p0_504	""                                
#define sd_6015_p0_505	"Bank: @(abs(%(L:sd_6015_p0_505,number)%)>180?abs(abs(%(L:sd_6015_p0_505,number)%)-360):abs(%(L:sd_6015_p0_505,number)%))@, Pitch: @(neg(%(L:sd_6015_p0_407,number)%))@"
#define sd_6015_p0_506	""                                
#define sd_6015_p0_507	"Temperature: %(L:sd_6015_p0_507,number)%"
#define sd_6015_p0_508	"Celsius: @(round(%(L:sd_6015_p0_508,number)%,0))@ Fahrenheit: @(round(cels2fahr(%(L:sd_6015_p0_508,fahrenheit)%),0))@"
#define sd_6015_p0_509	"Turn: %(L:sd_6015_p0_509,number), Ball: %(L:sd_6015_p0_510,number)%, VS: %(L:sd_6015_pa_530,number)%"
#define sd_6015_p0_510	""                                
#define sd_6015_p0_511	""                                
#define sd_6015_p0_512	""                                
#define sd_6015_p0_513	"Current: %(L:sd_6015_p0_513,number)%, Minimum: %(L:sd_6015_p0_511,number)%, Maximum: %(L:sd_6015_p0_512,number)%"
#define sd_6015_p0_514	"Left: %(L:sd_6015_p0_514,number)%, Right: %(L:sd_6015_p0_515,number)%"
#define sd_6015_p0_515	""                                
#define sd_6015_p0_516	"Left: %(L:sd_6015_p0_516,number)%, Right: %(L:sd_6015_p0_517,number)%"
#define sd_6015_p0_517	""                                
#define sd_6015_p0_518	"Main: %(L:sd_6015_p0_518,number)%, Auxl: %(L:sd_6015_p0_519,number)%"
#define sd_6015_p0_519	""                                
#define sd_6015_p0_520	""                                
#define sd_6015_p0_521	"Left: %(L:sd_6015_p0_520,number)%, Right: %(L:sd_6015_p0_521,number)%"
#define sd_6015_p0_522	"Course: @(norm(abs(%(L:sd_6015_p0_522,integer)%-360)))@, Heading: @(norm(%(L:sd_6015_pa_531,integer)%))@"
#define sd_6015_p0_523	""                                
#define sd_6015_p0_524	""                                
#define sd_6015_p0_525	"Left tank: %(C:FUEL_QUANTITY_LEFT,integer)% pounds, Right tank: %(C:FUEL_QUANTITY_RIGHT,integer)% pounds"
#define sd_6015_p0_526	""                                
#define sd_6015_p0_527	""                                
#define sd_6015_p0_528	"%(L:sd_6015_p0_528,number)%"                                
#define sd_6015_p0_529	"Altitude: %(L:sd_6015_p0_529,number)%, Pressure: %(L:sd_6015_p0_418,integer)% Pa, %(L:sd_6015_p0_419,number)% Hg, %(L:sd_6015_p0_420,integer)% mmHg"
#define sd_6015_p0_530	""                                
#define sd_6015_p0_531	""                                
#define sd_6015_p0_532	""                                
#define sd_6015_p0_533	""                                
#define sd_6015_p0_534	""                                


#define sd_6015_p1_501	""                                
#define sd_6015_p1_502	"Bank: @(abs(%(L:sd_6015_p1_502,number)%)>180?abs(abs(%(L:sd_6015_p1_502,number)%)-360):abs(%(L:sd_6015_p1_502,number)%))@, Pitch: @(neg(%(L:sd_6015_p1_406,number)%))@"
#define sd_6015_p1_503	""                                
#define sd_6015_p1_504	"Course: @(norm(abs(%(L:sd_6015_p1_504,integer)%-360)))@, Heading: @(norm(%(L:sd_6015_pa_531,integer)%))@"
#define sd_6015_p1_505	""                                
#define sd_6015_p1_506	""                                
#define sd_6015_p1_507	"ADF1: %(L:sd_6015_p1_507,number)%, ADF2: %(L:sd_6015_p1_508,number)%"
#define sd_6015_p1_508	""                                
#define sd_6015_p1_509	""                                
#define sd_6015_p1_510	""                                
#define sd_6015_p1_511	""                                
#define sd_6015_p1_512	""                                
#define sd_6015_p1_513	""                                
#define sd_6015_p1_514	"Voltage: %(L:sd_6015_p1_514,number)%" 
#define sd_6015_p1_515	"Voltage: %(L:sd_6015_p1_515,number)%"  
#define sd_6015_p1_516	"Voltage: %(L:sd_6015_p1_516,number)%" 
#define sd_6015_p1_517	"Ampers: %(L:sd_6015_p1_517,number)%"
#define sd_6015_p1_518	"Ampers: %(L:sd_6015_p1_518,number)%" 
#define sd_6015_p1_519	"Ampers: %(L:sd_6015_p1_519,number)%" 
#define sd_6015_p1_520	"Ampers: %(L:sd_6015_p1_520,number)%" 
#define sd_6015_p1_521	""                                
#define sd_6015_p1_522	""                                
#define sd_6015_p1_523	"Altitude: %(L:sd_6015_p1_523,number)%, Pressure: %(L:sd_6015_p1_409,integer)% Pa, %(L:sd_6015_p1_410,number)% Hg, %(L:sd_6015_p1_411,integer)% mmHg"                                
#define sd_6015_p1_524	""                                
#define sd_6015_p1_525	""                                
#define sd_6015_p1_526	""                                
#define sd_6015_p1_527	""                                
#define sd_6015_p1_528	""                                
															
															
#define sd_6015_p5_501	""                                
#define sd_6015_p5_502	""                                
 															
 															
#define sd_6015_p7_501	""                                
#define sd_6015_p7_502	""                                

#else

#define sd_6015_pa_501	""
#define sd_6015_pa_502	""
#define sd_6015_pa_503	""
#define sd_6015_pa_504	""
#define sd_6015_pa_505	""
#define sd_6015_pa_506	""
#define sd_6015_pa_507	""
#define sd_6015_pa_508	""
#define sd_6015_pa_509	""
#define sd_6015_pa_510	""
#define sd_6015_pa_511	""
#define sd_6015_pa_512	""
#define sd_6015_pa_513	""
#define sd_6015_pa_514	""
#define sd_6015_pa_515	""
#define sd_6015_pa_516	""
#define sd_6015_pa_517	""
#define sd_6015_pa_518	""
#define sd_6015_pa_519	""
#define sd_6015_pa_520	""
#define sd_6015_pa_521	""
#define sd_6015_pa_522	""
#define sd_6015_pa_523	""
#define sd_6015_pa_524	""
#define sd_6015_pa_525	""
#define sd_6015_pa_526	""
#define sd_6015_pa_527	""
#define sd_6015_pa_528	""
#define sd_6015_pa_529	""
#define sd_6015_pa_530	""
#define sd_6015_pa_531	""
#define sd_6015_pa_532	""
#define sd_6015_pa_533	""
#define sd_6015_pa_534	""
#define sd_6015_pa_535	""
#define sd_6015_pa_536	""
#define sd_6015_pa_537	""
#define sd_6015_pa_538	""                                
#define sd_6015_pa_539	""                                
						
						
#define sd_6015_p0_501	""
#define sd_6015_p0_502	""
#define sd_6015_p0_503	""
#define sd_6015_p0_504	""
#define sd_6015_p0_505	""
#define sd_6015_p0_506	""
#define sd_6015_p0_507	""
#define sd_6015_p0_508	""
#define sd_6015_p0_509	""
#define sd_6015_p0_510	""
#define sd_6015_p0_511	""
#define sd_6015_p0_512	""
#define sd_6015_p0_513	""
#define sd_6015_p0_514	""
#define sd_6015_p0_515	""
#define sd_6015_p0_516	""
#define sd_6015_p0_517	""
#define sd_6015_p0_518	""
#define sd_6015_p0_519	""
#define sd_6015_p0_520	""
#define sd_6015_p0_521	""
#define sd_6015_p0_522	""
#define sd_6015_p0_523	""
#define sd_6015_p0_524	""
#define sd_6015_p0_525	""
#define sd_6015_p0_526	""
#define sd_6015_p0_527	""
#define sd_6015_p0_528	""
#define sd_6015_p0_529	""
#define sd_6015_p0_530	""                                
#define sd_6015_p0_531	""                                
#define sd_6015_p0_532	""                                
#define sd_6015_p0_533	""                                
#define sd_6015_p0_534	""                                
						
						
#define sd_6015_p1_501	""
#define sd_6015_p1_502	""
#define sd_6015_p1_503	""
#define sd_6015_p1_504	""
#define sd_6015_p1_505	""
#define sd_6015_p1_506	""
#define sd_6015_p1_507	""
#define sd_6015_p1_508	""
#define sd_6015_p1_509	""
#define sd_6015_p1_510	""
#define sd_6015_p1_511	""
#define sd_6015_p1_512	""
#define sd_6015_p1_513	""
#define sd_6015_p1_514	""
#define sd_6015_p1_515	""
#define sd_6015_p1_516	""
#define sd_6015_p1_517	""
#define sd_6015_p1_518	""
#define sd_6015_p1_519	""
#define sd_6015_p1_520	""
#define sd_6015_p1_521	""
#define sd_6015_p1_522	""
#define sd_6015_p1_523	""
#define sd_6015_p1_524	""                                
#define sd_6015_p1_525	""                                
#define sd_6015_p1_526	""                                
#define sd_6015_p1_527	""                                
#define sd_6015_p1_528	""                                
						
						
#define sd_6015_p5_501	""
#define sd_6015_p5_502	""
						
						
#define sd_6015_p7_501	""
#define sd_6015_p7_502	""

#endif

ApiName NdlNames[NDL_MAX]={
	// �����
	{"sd_6015_pa_501",sd_6015_pa_501,"",    0,    0,false,false},  // NDL_MAIN_SEC                 
	{"sd_6015_pa_502",sd_6015_pa_502,"",    0,    0,false,false},  // NDL_MAIN_MIN                
	{"sd_6015_pa_503",sd_6015_pa_503,"",    0,    0,false,false},  // NDL_MAIN_HRS                
	{"sd_6015_pa_504",sd_6015_pa_504,"",    0,    0,false,false},  // NDL_MINI_SEC                
	{"sd_6015_pa_505",sd_6015_pa_505,"",    0,    0,false,false},  // NDL_MINI_SEC_HIDE           
	{"sd_6015_pa_506",sd_6015_pa_506,"",    0,    0,false,false},  // NDL_MINI_MIN                
	{"sd_6015_pa_507",sd_6015_pa_507,"",    0,    0,false,false},  // NDL_MINI_HRS                
	{"sd_6015_pa_508",sd_6015_pa_508,"",  -10,  100,false,false},  // NDL_ENG1_FUEL_PSI           
	{"sd_6015_pa_509",sd_6015_pa_509,"",  -10,    8,false,false},  // NDL_ENG1_OIL_PSI            
	{"sd_6015_pa_510",sd_6015_pa_510,"", -100,  150,false,false},  // NDL_ENG1_OIL_TEMP           
	{"sd_6015_pa_511",sd_6015_pa_511,"",  -10,  100,false,false},  // NDL_ENG2_FUEL_PSI           
	{"sd_6015_pa_512",sd_6015_pa_512,"",  -10,    8,false,false},  // NDL_ENG2_OIL_PSI            
	{"sd_6015_pa_513",sd_6015_pa_513,"", -100,  150,false,false},  // NDL_ENG2_OIL_TEMP           
	{"sd_6015_pa_514",sd_6015_pa_514,"",  -10,  100,false,false},  // NDL_ENG3_FUEL_PSI           
	{"sd_6015_pa_515",sd_6015_pa_515,"",  -10,    8,false,false},  // NDL_ENG3_OIL_PSI            
	{"sd_6015_pa_516",sd_6015_pa_516,"", -100,  150,false,false},  // NDL_ENG3_OIL_TEMP           
	{"sd_6015_pa_517",sd_6015_pa_517,"",    0,  110,false,false},  // NDL_ENG1_N1                 
	{"sd_6015_pa_518",sd_6015_pa_518,"",    0,  110,false,false},  // NDL_ENG1_N2                 
	{"sd_6015_pa_519",sd_6015_pa_519,"",    0,  110,false,false},  // NDL_ENG2_N1                 
	{"sd_6015_pa_520",sd_6015_pa_520,"",    0,  110,false,false},  // NDL_ENG2_N2                 
	{"sd_6015_pa_521",sd_6015_pa_521,"",    0,  110,false,false},  // NDL_ENG3_N1                 
	{"sd_6015_pa_522",sd_6015_pa_522,"",    0,  110,false,false},  // NDL_ENG3_N2                 
	{"sd_6015_pa_523",sd_6015_pa_523,"",   -9,    9,false,false},  // NDL_HTAIL_DEG               
	{"sd_6015_pa_524",sd_6015_pa_524,"",    0,  900,false,false},  // NDL_ENG1_TEMP               
	{"sd_6015_pa_525",sd_6015_pa_525,"",    0,  900,false,false},  // NDL_ENG2_TEMP               
	{"sd_6015_pa_526",sd_6015_pa_526,"",    0,  900,false,false},  // NDL_ENG3_TEMP               
	{"sd_6015_pa_527",sd_6015_pa_527,"",    0,   35,false,false},  // NDL_FLAPS                   
	{"sd_6015_pa_528",sd_6015_pa_528,"",    0,  900,false,false},  // NDL_KUS_TAS                 
	{"sd_6015_pa_529",sd_6015_pa_529,"",    0,  700,false,false},  // NDL_KUS_IAS                 
	{"sd_6015_pa_530",sd_6015_pa_530,"",  -30,   30,false,false},  // NDL_VS                      
	{"sd_6015_pa_531",sd_6015_pa_531,"",    0,    0,false,false},  // NDL_GMK_PRICOURSE           
	{"sd_6015_pa_532",sd_6015_pa_532,"",    0,    0,false,false},  // NDL_GMK_SECCOURSE           
	{"sd_6015_pa_533",sd_6015_pa_533,"",    0,    0,false,false},  // NDL_GMK_SCALE               
	{"sd_6015_pa_534",sd_6015_pa_534,"",    0,    0,false,false},  // NDL_ENG1_THROTTLE_POS       
	{"sd_6015_pa_535",sd_6015_pa_535,"",    0,    0,false,false},  // NDL_ENG2_THROTTLE_POS       
	{"sd_6015_pa_536",sd_6015_pa_536,"",    0,    0,false,false},  // NDL_ENG3_THROTTLE_POS       
	{"sd_6015_pa_537",sd_6015_pa_537,"",    0,  700,false,false},  // NDL_KUS_TAS_EN              
	{"sd_6015_pa_538",sd_6015_pa_538,"",    0,  370,false,false},  // NDL_KUS_IAS_EN              
	{"sd_6015_pa_539",sd_6015_pa_539,"",  -30,   30,false,false},  // NDL_VS_EN                   
					  				
	// P0			  				
	{"sd_6015_p0_501",sd_6015_p0_501,"",    0,    0,false,false},  // NDL_AGB_MAIN_FLAG        
	{"sd_6015_p0_502",sd_6015_p0_502,"",    0,    0,false,false},  // NDL_AGB_MAIN_PLANE       
	{"sd_6015_p0_503",sd_6015_p0_503,"",    0,    0,false,false},  // NDL_AGB_MAIN_BALL        
	{"sd_6015_p0_504",sd_6015_p0_504,"",    0,    0,false,false},  // NDL_AGB_AUXL_FLAG        
	{"sd_6015_p0_505",sd_6015_p0_505,"",    0,    0,false,false},  // NDL_AGB_AUXL_PLANE       
	{"sd_6015_p0_506",sd_6015_p0_506,"",    0,    0,false,false},  // NDL_AGB_AUXL_BALL        
	{"sd_6015_p0_507",sd_6015_p0_507,"",    0,  900,false,false},  // NDL_APU_TEMP             
	{"sd_6015_p0_508",sd_6015_p0_508,"",    0,    0,false,false},  // NDL_AIR_TEMP             
	{"sd_6015_p0_509",sd_6015_p0_509,"",   -1,    1,false,false},  // NDL_DA30_TURN            
	{"sd_6015_p0_510",sd_6015_p0_510,"",    0,    0,false,false},  // NDL_DA30_BALL            
	{"sd_6015_p0_511",sd_6015_p0_511,"",   -3,    4,false,false},  // NDL_GFORCE_MIN           
	{"sd_6015_p0_512",sd_6015_p0_512,"",   -3,    4,false,false},  // NDL_GFORCE_MAX           
	{"sd_6015_p0_513",sd_6015_p0_513,"",   -3,    4,false,false},  // NDL_GFORCE_CUR           
	{"sd_6015_p0_514",sd_6015_p0_514,"",  -10,  150,false,false},  // NDL_MAIN_BRAKE_HYD_LEFT  
	{"sd_6015_p0_515",sd_6015_p0_515,"",  -10,  150,false,false},  // NDL_MAIN_BRAKE_HYD_RIGHT 
	{"sd_6015_p0_516",sd_6015_p0_516,"",  -10,  150,false,false},  // NDL_EMERG_BRAKE_HYD_LEFT 
	{"sd_6015_p0_517",sd_6015_p0_517,"",  -10,  150,false,false},  // NDL_EMERG_BRAKE_HYD_RIGHT
	{"sd_6015_p0_518",sd_6015_p0_518,"",  -10,  150,false,false},  // NDL_MAIN_HYD             
	{"sd_6015_p0_519",sd_6015_p0_519,"",  -10,  150,false,false},  // NDL_EMERG_HYD            
	{"sd_6015_p0_520",sd_6015_p0_520,"",    0,    0,false,false},  // NDL_IKU_YELLOW           
	{"sd_6015_p0_521",sd_6015_p0_521,"",    0,    0,false,false},  // NDL_IKU_WHITE            
	{"sd_6015_p0_522",sd_6015_p0_522,"",    0,    0,false,false},  // NDL_KPPMS_SCALE          
	{"sd_6015_p0_523",sd_6015_p0_523,"", -120,  120,false,false},  // NDL_KPPMS_ILS_GLIDE      
	{"sd_6015_p0_524",sd_6015_p0_524,"", -120,  120,false,false},  // NDL_KPPMS_ILS_COURSE     
	{"sd_6015_p0_525",sd_6015_p0_525,"",    0, 4000,false,false},  // NDL_PPTIZ                
	{"sd_6015_p0_526",sd_6015_p0_526,"",    0,  600,false,false},  // NDL_RV3M_DH              
	{"sd_6015_p0_527",sd_6015_p0_527,"",  -10,  600,false,false},  // NDL_RV3M_ALT             
	{"sd_6015_p0_528",sd_6015_p0_528,"",    0,    0,false,false},  // NDL_STARTER              
	{"sd_6015_p0_529",sd_6015_p0_529,"",    0,    0,false,false},  // NDL_UVID                 
	{"sd_6015_p0_530",sd_6015_p0_530,"",    0,    0,false,false},  // NDL_UVID_EN              
	{"sd_6015_p0_531",sd_6015_p0_531,"",    0, 2500,false,false},  // NDL_RV3M_DH_EN           
	{"sd_6015_p0_532",sd_6015_p0_532,"",  -10, 2500,false,false},  // NDL_RV3M_ALT_EN          
	{"sd_6015_p0_533",sd_6015_p0_533,"",    0,    0,false,false},  // NDL_UVID_HIDE            
	{"sd_6015_p0_534",sd_6015_p0_534,"",    0,    0,false,false},  // NDL_UVID_EN_HIDE         
					  						      
	// P1			  						      
	{"sd_6015_p1_501",sd_6015_p1_501,"",    0,    0,false,false},  // NDL_AGB_MAIN_FLAG   
	{"sd_6015_p1_502",sd_6015_p1_502,"",    0,    0,false,false},  // NDL_AGB_MAIN_PLANE  
	{"sd_6015_p1_503",sd_6015_p1_503,"",    0,    0,false,false},  // NDL_AGB_MAIN_BALL   
	{"sd_6015_p1_504",sd_6015_p1_504,"",    0,    0,false,false},  // NDL_KPPMS_SCALE     
	{"sd_6015_p1_505",sd_6015_p1_505,"", -120,  120,false,false},  // NDL_KPPMS_ILS_GLIDE 
	{"sd_6015_p1_506",sd_6015_p1_506,"", -120,  120,false,false},  // NDL_KPPMS_ILS_COURSE
	{"sd_6015_p1_507",sd_6015_p1_507,"",    0,    0,false,false},  // NDL_ARK1            
	{"sd_6015_p1_508",sd_6015_p1_508,"",    0,    0,false,false},  // NDL_ARK2            
	{"sd_6015_p1_509",sd_6015_p1_509,"",    0,   10,false,false},  // NDL_URVK            
	{"sd_6015_p1_510",sd_6015_p1_510,"",    0,    5,false,false},  // NDL_UVPD_ALT        
	{"sd_6015_p1_511",sd_6015_p1_511,"",    0,    8,false,false},  // NDL_UVPD_DIFPRESS   
	{"sd_6015_p1_512",sd_6015_p1_512,"",   -6,    6,false,false},  // NDL_TEMP_SALON      
	{"sd_6015_p1_513",sd_6015_p1_513,"",   -7,   15,false,false},  // NDL_DUCT            
	{"sd_6015_p1_514",sd_6015_p1_514,"",    0,  150,false,false},  // NDL_VOLTS115        
	{"sd_6015_p1_515",sd_6015_p1_515,"",    0,   40,false,false},  // NDL_VOLTS36         
	{"sd_6015_p1_516",sd_6015_p1_516,"",    0,   30,false,false},  // NDL_VOLTS27         
	{"sd_6015_p1_517",sd_6015_p1_517,"",  -10,   50,false,false},  // NDL_AMPS27          
	{"sd_6015_p1_518",sd_6015_p1_518,"",  -10,   50,false,false},  // NDL_AMPSGEN1        
	{"sd_6015_p1_519",sd_6015_p1_519,"",  -10,   50,false,false},  // NDL_AMPSGEN2        
	{"sd_6015_p1_520",sd_6015_p1_520,"",  -10,   50,false,false},  // NDL_AMPSGEN3        
	{"sd_6015_p1_521",sd_6015_p1_521,"",    0,    0,false,false},  // NDL_ALTIM_ENG       
	{"sd_6015_p1_522",sd_6015_p1_522,"",    0,    0,false,false},  // NDL_ALTIM_RUS       
	{"sd_6015_p1_523",sd_6015_p1_523,"",    0,    0,false,false},  // NDL_UVID_1000       
	{"sd_6015_p1_524",sd_6015_p1_524,"",    0,    0,false,false},  // NDL_UVID_1000_EN    
	{"sd_6015_p1_525",sd_6015_p1_525,"",    0,    0,false,false},  // NDL_VS_SRD
	{"sd_6015_p1_526",sd_6015_p1_526,"",    0,    0,false,false},  // NDL_UVID_100       
	{"sd_6015_p1_527",sd_6015_p1_527,"",    0,    0,false,false},  // NDL_UVID_100_EN    
	{"sd_6015_p1_528",sd_6015_p1_528,"",    0,    0,false,false},  // NDL_UVID1    
					  						      
	// P5			  						      
	{"sd_6015_p5_501",sd_6015_p5_501,"",    0,    0,false,false},  // NDL_ARK1SIGNAL
	{"sd_6015_p5_502",sd_6015_p5_502,"",    0,    0,false,false},  // NDL_ARK2SIGNAL
					  						      
	// P7			  						      
	{"sd_6015_p7_501",sd_6015_p7_501,"",    0,  0.6,false,false},  // NDL_2077DIFFPRESS 	
	{"sd_6015_p7_502",sd_6015_p7_502,"",  430,  810,false,false},  // NDL_2077PRESSBEGIN	
											      
};											      
