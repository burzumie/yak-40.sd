/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/APIEntry.cpp $

  Last modification:
    $Date: 19.02.06 10:23 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "APIEntry.h"

SDAPIEntrys		g_SDAPIEntrys;
SDAPIEntrys*	g_pSDAPIEntrys=NULL;
MODULE_VAR		MVSDAPIEntrys;
MODULE_VAR		MVSDAPI;

