/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Api.cpp $

  Last modification:
    $Date: 19.02.06 10:23 $
    $Revision: 8 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Api.h"
#include "ApiName.h"
#include "ServerDef.h"
#include "../Lib/MathParser.h"
#include "../Lib/PanelsTools.h"

std::list<SDAPITooltipBase*> SDAPITooltipBase::m_ChildList;

extern ApiName AzsNames[];
extern ApiName BtnNames[];
extern ApiName GltNames[];
extern ApiName HndNames[];
extern ApiName KrmNames[];
extern ApiName LmpNames[];
extern ApiName NdlNames[];
extern ApiName PosNames[];
extern ApiName TbgNames[];
extern ApiName TblNames[];
extern ApiName SndNames[];
extern ApiName KeyNames[];

SDAPI *g_pSDAPI=NULL;

SDAPITooltip::SDAPITooltip(char *name)
{
	SECUREBEGIN
	m_SrcToolTip.erase(); //clear();
	m_ReadyToolTip[0]='\0';
	int i;
	for(i=0;i<MAX_VPARAMS;i++)
		m_VParamRead[i]=false;

	for(i=0;i<MAX_LPARAMS;i++) {
		m_LParamRead[i]=false;
		m_LParams[i]=NULL;
	}

	m_NeedParse=false;
	m_NeedMathParse=false;
	SECUREEND
}

SDAPITooltip::~SDAPITooltip()
{
	m_SrcToolTip.erase(); //clear();
	for(int i=0;i<MAX_LPARAMS;i++) {
		if(m_LParams[i]) {
			delete m_LParams[i];
			m_LParams[i]=NULL;
		}
	}
}

void SDAPITooltip::SetTT(wstring val, int type, int num)
{
	SECUREBEGIN

	if(!g_pSDAPI->m_Config.m_CfgCommon.m_bCTokensEnable)
		return;

	m_Type=type;
	m_NumInApi=num;
	m_SrcToolTip=val;

	std::basic_string <wchar_t>::size_type index;
	static const std::basic_string <wchar_t>::size_type npos = -1;

	index = m_SrcToolTip.find(L"%(");
	if(index==npos) {
		m_NeedParse=false;
		ConvertWideStringToAnsiCch((char *)m_ReadyToolTip,m_SrcToolTip.c_str(),BUFSIZ); 
	} else {
		//ConvertWideStringToAnsiCch((char *)m_ForProcessTip,m_SrcToolTip.c_str(),BUFSIZ); 
		//m_ProcessedTip.assign(m_ForProcessTip);
		m_NeedParse=true;
		Parse(GetFromApi());
	}
	index = m_SrcToolTip.find(L"@(");
	if(index==npos) {
		m_NeedMathParse=false;
	} else {
		m_NeedMathParse=true;
		MathParse();
	}
	SetToApi();

	SECUREEND
}

void SDAPITooltip::Update()
{
	if(m_NeedParse) {
		Parse(GetFromApi());
	}
	if(m_NeedMathParse) {
		MathParse();
	}
	SetToApi();
}

void SDAPITooltip::GetVParam(WCHAR *in, WCHAR *dest, double simval, int param)
{
	if(!m_VParamRead[param]) {
		int ValIndex=0;
		m_VParams[param].clear();
		CTokenizerW tok(in,L"|");
		std::wstring cs;
		while(tok.Next(cs))
			m_VParams[param].insert(std::pair<int,std::wstring>((int)ValIndex++,cs));
		m_VParamRead[param]=true;
	}

	std::map<int,std::wstring>::iterator p;
	p=m_VParams[param].find((int)simval);
	if(p!=m_VParams[param].end()) {
		wcscpy(dest,p->second.c_str());
	} 	
}

void SDAPITooltip::PrintVal(WCHAR *tmp,const char *buf,double needval)
{
	if(!_stricoll(buf,"number")) {
		swprintf(tmp,L"%.2f",needval);
	} else if(!_stricoll(buf,"integer")||!_stricoll(buf,"bool")) {
		swprintf(tmp,L"%d",(int)needval);
	} else {
		swprintf(tmp,L"%f",needval);
	}
}

void SDAPITooltip::GetLParam(WCHAR *in, WCHAR *dest, int param)
{
	char buf[BUFSIZ];
	ConvertWideStringToAnsiCch(buf,in,BUFSIZ);
	CTokenizerA tok(buf,",");
	std::string cs;
	double needval;
	tok.Next(cs);

	if(!m_LParamRead[param]) {
		m_LParams[param]=new CNamedVar(cs.c_str(),false);
		m_LParamRead[param]=true;
	}
	if(m_LParams[param]) {
		needval=m_LParams[param]->get_number();
		PrintVal(dest,tok.Tail().c_str(),needval);
	}
}

void SDAPITooltip::GetCParam(WCHAR *in, WCHAR *dest)
{
	char buf[BUFSIZ];
	ConvertWideStringToAnsiCch(buf,in,BUFSIZ);
	CTokenizerA tok(buf,",");
	std::string cs;
	double needval;
	tok.Next(cs);
	SDConfigCommon::CTokensList::iterator t=g_pSDAPI->m_Config.m_CfgCommon.m_lCTokens.find(cs);
	if(t!=g_pSDAPI->m_Config.m_CfgCommon.m_lCTokens.end()) {
		needval=g_pSDAPI->GetN((GAUGE_TOKEN)t->second);
		PrintVal(dest,tok.Tail().c_str(),needval);
	}
}

void SDAPITooltip::GetAParam(WCHAR *in, WCHAR *dest)
{
	char buf[BUFSIZ];
	ConvertWideStringToAnsiCch(buf,in,BUFSIZ);
	CTokenizerA tok(buf,",");
	std::string cs;
	double needval;
	tok.Next(cs);
	SDConfigCommon::CTokensList::iterator t=g_pSDAPI->m_Config.m_CfgCommon.m_lXMLTokens.find(cs);
	if(t!=g_pSDAPI->m_Config.m_CfgCommon.m_lXMLTokens.end()) {
		needval=g_pSDAPI->GetXML((XML_TOKEN)t->second,(char *)tok.Tail().c_str());
		PrintVal(dest,tok.Tail().c_str(),needval);
	}
}

size_t SDAPITooltip::GetVal(WCHAR *val, WCHAR *dest, size_t start, double simval)
{
	size_t inLength=wcslen(val);
	bool done=false;
	WCHAR c1,c2,outStr[BUFSIZ]={0};

	c1=val[0];
	c2=val[1];

	if((c1=='V'||c1=='v')&&c2==':') {
		GetVParam(&val[2],outStr,simval,m_TotalVParams++);				
	} else if((c1=='L'||c1=='l')&&c2==':') {
		GetLParam(&val[2],outStr,m_TotalLParams++);				
	} else if((c1=='C'||c1=='c')&&c2==':') {
		GetCParam(&val[2],outStr);				
	} else if((c1=='A'||c1=='a')&&c2==':') {
		GetAParam(&val[2],outStr);				
	} else {
		for(size_t i=0;i<inLength;i++)
			dest[start++]=val[i];
		return inLength;
	}

	size_t outStrLength=wcslen(outStr);

	for(size_t i=0;i<outStrLength;i++)
		dest[start++]=outStr[i];

	return outStrLength;
}

void SDAPITooltip::Parse(double val)
{
	WCHAR outStr[BUFSIZ]={0},Value[BUFSIZ]={0};
	bool done=false, valDone=false;
	WCHAR c,c2;

	size_t inIndex=0, inLength=m_SrcToolTip.length();
	size_t outIndex=0, valIndex=0;

	m_TotalVParams=0;
	m_TotalLParams=0;

	while(!done) {
		if(inIndex>=inLength) {
			done=true;
		} else {
			c=m_SrcToolTip[inIndex];
			c2=m_SrcToolTip[inIndex+1];

			if(c=='%'&&c2=='(') {

				valIndex=0;
				valDone=false;
				inIndex+=2;

				while(!valDone)	{
					if(inIndex>=inLength) {
						done=true;
						valDone=true;
					} else {
						if(m_SrcToolTip[inIndex]==')') {
							inIndex+=2;
							valDone=true;
							Value[valIndex]='\0';
						} else {
							Value[valIndex++]=m_SrcToolTip[inIndex++];
						}
					}
				}

				outIndex+=GetVal(Value, outStr, outIndex, val);

			} else {
				outStr[outIndex++]=c;
				inIndex++;
			}

		}

	}

	outStr[outIndex]='\0';

	ConvertWideStringToAnsiCch(m_ReadyToolTip,outStr,BUFSIZ);
}

size_t SDAPITooltip::GetMathResult(CHAR *val, CHAR *dest, size_t start)
{
	CMathParser mp;
	double result;
	char *ErrMsg = mp.Parse( val, &result );

	char tmp[BUFSIZ];

	if ( ErrMsg == NULL ) {
		sprintf(tmp,"%.2f",result);
		if(tmp[strlen(tmp)-1]=='0'&&tmp[strlen(tmp)-2]=='0')
			tmp[strlen(tmp)-3]='\0';
	} else {
		sprintf(tmp,"%s",val);
	}

	size_t outStrLength=strlen(tmp);

	for(size_t i=0;i<outStrLength;i++)
		dest[start++]=tmp[i];

	return outStrLength;
}

void SDAPITooltip::MathParse()
{
	CHAR outStr[BUFSIZ]={0},Value[BUFSIZ]={0};
	bool done=false, valDone=false;
	CHAR c,c2;
	CHAR inStr[BUFSIZ];

	strcpy(inStr,m_ReadyToolTip);

	size_t inIndex=0, inLength=strlen(inStr);
	size_t outIndex=0, valIndex=0;

	while(!done) {
		if(inIndex>=inLength) {
			done=true;
		} else {
			c=inStr[inIndex];
			c2=inStr[inIndex+1];

			if(c=='@'&&c2=='(') {

				valIndex=0;
				valDone=false;
				inIndex+=2;

				while(!valDone)	{
					if(inIndex>=inLength) {
						done=true;
						valDone=true;
					} else {
						if(inStr[inIndex]=='@') {
							valIndex--;
							inIndex+=1;
							valDone=true;
							Value[valIndex]='\0';
						} else {
							Value[valIndex++]=inStr[inIndex++];
						}
					}
				}

				outIndex+=GetMathResult(Value, outStr, outIndex);

			} else {
				outStr[outIndex++]=c;
				inIndex++;
			}

		}

	}

	outStr[outIndex]='\0';

	strcpy(m_ReadyToolTip,outStr);
}


double SDAPITooltip::GetFromApi() 
{
	switch(m_Type) {
		case 1:
			return AZS_GET(m_NumInApi);
			break;
		case 2:
			return BTN_GET(m_NumInApi);
			break;
		case 3:
			return GLT_GET(m_NumInApi);
			break;
		case 4:
			return KRM_GET(m_NumInApi);
			break;
		case 5:
			return HND_GET(m_NumInApi);
			break;
		case 6:
			return LMP_GET(m_NumInApi);
			break;
		case 8:
			return NDL_GET(m_NumInApi);
			break;
		case 9:
			return POS_GET(m_NumInApi);
			break;
		case 10:
			return TBG_GET(m_NumInApi);
			break;
		case 12:
			return TBL_GET(m_NumInApi);
			break;
	}
	return 0;
}

void SDAPITooltip::SetToApi() 
{
	switch(m_Type) {
		case 1:
			AZS_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
		case 2:
			BTN_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
		case 3:
			GLT_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
		case 4:
			KRM_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
		case 5:
			HND_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
		case 6:
			LMP_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
		case 8:
			NDL_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
		case 9:
			POS_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
		case 10:
			TBG_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
		case 12:
			TBL_TTSET(m_NumInApi,m_ReadyToolTip);
			break;
	}
}

SDAPI::SDAPI(bool logic)
{
	m_UpdateTT=logic;
	//if(m_UpdateTT)
		//m_TimerTT=auto_ptr<CTimerTT>(new CTimerTT(ImportTable.pFS6));
}

SDAPI::~SDAPI()
{
}

void SDAPI::UpdateAPI() 
{
}

void SDAPI::Update()
{
	UpdateVars();

	if(m_UpdateTT&&m_Config.m_CfgCommon.m_bCTokensEnable) 
		SDAPITooltipBase::UpdateAll();

}

void SDAPI::Prepare()
{
	SECUREBEGIN_K
	TCHAR sGaugePath[MAX_PATH]={0};
	TCHAR sSimPath[MAX_PATH]={0};
	char temp[MAX_PATH];

	HKEY hKey;
	DWORD dwBufLen;

	RegOpenKeyExA(HKEY_LOCAL_MACHINE,FSX_REG_BASE,0,REG_SZ,&hKey);
	LONG ret=RegQueryValueExA(hKey,REGISTRY_SIM_PATH.c_str(),NULL,NULL,(LPBYTE)sSimPath,&dwBufLen);
	RegCloseKey(hKey);

	GetModuleFileNameA((HMODULE)g_hInstance,sGaugePath,MAX_PATH);
	ExtractPath(sGaugePath);
	if(ret!=ERROR_SUCCESS) {
		strcpy(sSimPath, sGaugePath);
		strcat(sSimPath, MY_GAUGE_PATH_PLAIN.c_str());
	} else {
		strcat(sSimPath, "\\");
	}

	sprintf(temp,"%s%s",sSimPath,MY_CONFIG_PATH.c_str());
	strcpy(m_SimPath,sSimPath);
	strcpy(m_GaugePath,sGaugePath);
	strcpy(SDConfigBase::m_ConfigPath,temp);
	strcat(SDConfigBase::m_ConfigPath,"\\");
	sprintf(temp,"%s%s",sSimPath,MY_SOUND_PATH.c_str());
	strcpy(m_SoundPath,temp);
	strcat(m_SoundPath,"\\");
	CFG_STR_SET(CFG_STR_SOUND_PATH,m_SoundPath);
	sprintf(SDConfigBase::m_ConfigFile,"%s%s",SDConfigBase::m_ConfigPath,MY_CONFIG_FILE.c_str());
	m_Config.Load();
	InitVars();
	PrepareTT();
	CFG_SET(CFG_SOUND_ENABLE,m_Config.m_CfgSound.m_bEnable);
	CFG_STR_SET(CFG_STR_SOUND_PACK,m_Config.m_CfgSound.m_sSoundPack.c_str());
	//if(m_UpdateTT&&m_Config.m_CfgCommon.m_bCTokensEnable) {
		//m_TimerTT->StartTimer(CHAIN_PRE_FRAME);
	//}
	SECUREEND_K
}

#define CREATE_CONTROL_WITH_TT_D(A,B,C,N,CL,AE,TY)   /**/ \
	for(i=0;i<A;i++) { \
		C[i]=auto_ptr<CL>(new CL(B[i].name)); \
		g_SDAPIEntrys.AE[i].SetLimit(B[i].min,B[i].max,B[i].loop); \
		if(strlen(B[i].tt)>0) { \
			ConvertAnsiStringToWideCch(tmp1,B[i].name,255); \
			ConvertAnsiStringToWideCch(tmp2,B[i].tt,255); \
			ini.WriteStringW(L##N,tmp1,tmp2); \
		} \
		ConvertAnsiStringToWideCch(tmp,B[i].name,255); \
		C[i]->SetTT(ini.ReadStringW(L##N,tmp,L""),TY,i); \
	}

#define CREATE_CONTROL_WITH_TT(A,B,C,N,CL,AE,TY)   /**/ \
	for(i=0;i<A;i++) { \
		C[i]=auto_ptr<CL>(new CL(B[i].name)); \
		g_SDAPIEntrys.AE[i].SetLimit(B[i].min,B[i].max,B[i].loop); \
		/*if(m_Config.m_CfgTooltips.m_bEnable) { */\
			ConvertAnsiStringToWideCch(tmp,B[i].name,255); \
			C[i]->SetTT(ini.ReadStringW(L##N,tmp,L""),TY,i); \
		/*} */\
	}

void SDAPI::PrepareTT()
{
	SECUREBEGIN_D

	CIniFile ini(m_Config.m_CfgTooltips.m_sFile);
	int i;

#ifdef _DEBUG
	WCHAR tmp[512],tmp1[512],tmp2[512]; 
	CREATE_CONTROL_WITH_TT_D(AZS_MAX,AzsNames,m_Azs,"Tooltips",SDAPITooltip,SDAZS,1)
	CREATE_CONTROL_WITH_TT_D(BTN_MAX,BtnNames,m_Btn,"Tooltips",SDAPITooltip,SDBTN,2)
	CREATE_CONTROL_WITH_TT_D(GLT_MAX,GltNames,m_Glt,"Tooltips",SDAPITooltip,SDGLT,3)
	CREATE_CONTROL_WITH_TT_D(KRM_MAX,KrmNames,m_Krm,"Tooltips",SDAPITooltip,SDKRM,4)
	CREATE_CONTROL_WITH_TT_D(HND_MAX,HndNames,m_Hnd,"Tooltips",SDAPITooltip,SDHND,5)
	CREATE_CONTROL_WITH_TT_D(LMP_MAX,LmpNames,m_Lmp,"Tooltips",SDAPITooltip,SDLMP,6)
	CREATE_CONTROL_WITH_TT_D(NDL_MAX,NdlNames,m_Ndl,"Tooltips",SDAPITooltip,SDNDL,8)
	CREATE_CONTROL_WITH_TT_D(POS_MAX,PosNames,m_Pos,"Tooltips",SDAPITooltip,SDPOS,9)
	CREATE_CONTROL_WITH_TT_D(TBG_MAX,TbgNames,m_Tbg,"Tooltips",SDAPITooltip,SDTBG,10)
	CREATE_CONTROL_WITH_TT_D(TBL_MAX,TblNames,m_Tbl,"Tooltips",SDAPITooltip,SDTBL,12)
#else
	WCHAR tmp[512]; 
	CREATE_CONTROL_WITH_TT  (AZS_MAX,AzsNames,m_Azs,"Tooltips",SDAPITooltip,SDAZS,1)
	CREATE_CONTROL_WITH_TT  (BTN_MAX,BtnNames,m_Btn,"Tooltips",SDAPITooltip,SDBTN,2)
	CREATE_CONTROL_WITH_TT  (GLT_MAX,GltNames,m_Glt,"Tooltips",SDAPITooltip,SDGLT,3)
	CREATE_CONTROL_WITH_TT  (KRM_MAX,KrmNames,m_Krm,"Tooltips",SDAPITooltip,SDKRM,4)
	CREATE_CONTROL_WITH_TT  (HND_MAX,HndNames,m_Hnd,"Tooltips",SDAPITooltip,SDHND,5)
	CREATE_CONTROL_WITH_TT  (LMP_MAX,LmpNames,m_Lmp,"Tooltips",SDAPITooltip,SDLMP,6)
	CREATE_CONTROL_WITH_TT  (NDL_MAX,NdlNames,m_Ndl,"Tooltips",SDAPITooltip,SDNDL,8)
	CREATE_CONTROL_WITH_TT  (POS_MAX,PosNames,m_Pos,"Tooltips",SDAPITooltip,SDPOS,9)
	CREATE_CONTROL_WITH_TT  (TBG_MAX,TbgNames,m_Tbg,"Tooltips",SDAPITooltip,SDTBG,10)
	CREATE_CONTROL_WITH_TT  (TBL_MAX,TblNames,m_Tbl,"Tooltips",SDAPITooltip,SDTBL,12)
#endif

	SECUREEND_D
}

