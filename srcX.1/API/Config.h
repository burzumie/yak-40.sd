/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Config.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

#include "../Lib/IniFile.h"
#include "../Lib/Tools.h"
#include "CTokens.h"
#include "XMLTokens.h"
#include "KeyID.h"

#pragma warning(disable: 4996)

const string CONFIG_NOT_FOUND          		= "Not found!!!";

const string CONFIG_SECTION_COMMON  		= "Common";
const string CONFIG_SECTION_TOOLTIPS  		= "Tooltips";
const string CONFIG_SECTION_NAV     		= "Nav";
const string CONFIG_SECTION_RADIO   		= "Radio";
const string CONFIG_SECTION_SOUND			= "Sound";
const string CONFIG_SECTION_HTAIL			= "Htail";
const string CONFIG_SECTION_AUTOPILOT		= "Autopilot";
const string CONFIG_SECTION_FLAPS			= "Flaps";
const string CONFIG_SECTION_GEAR 			= "Gear";
const string CONFIG_SECTION_ACHS 			= "Achs";
const string CONFIG_SECTION_AIRTEMP			= "Airtemp";
const string CONFIG_SECTION_GFORCE 			= "Gforce";
const string CONFIG_SECTION_PPTIZ  			= "Fuelind";
const string CONFIG_SECTION_RV3M   			= "Radioaltimer";
const string CONFIG_SECTION_KUS    			= "KUS";
const string CONFIG_SECTION_VS     			= "VS";
const string CONFIG_SECTION_ENGINES   		= "Engines";
const string CONFIG_SECTION_DA30      		= "DA30";
const string CONFIG_SECTION_UVID      		= "UVID";
const string CONFIG_SECTION_KPPMS     		= "KPPMS";
const string CONFIG_SECTION_IKU       		= "IKU";
const string CONFIG_SECTION_AGB       		= "AGB";
const string CONFIG_SECTION_FRONTLEG  		= "FrontLeg";

const string CONFIG_PARAM_ENABLE			= "Enable";
const string CONFIG_PARAM_MASTER_VOL		= "MasterVolume";
const string CONFIG_PARAM_SOUND_PACK		= "SoundPack";
const string CONFIG_PARAM_FILE				= "File";
const string CONFIG_PARAM_REFRESH			= "Refresh";
const string CONFIG_PARAM_OBSZEROSTART 		= "OBSZeroStart";
const string CONFIG_PARAM_OBS1DEFAULT 		= "OBS1Default";
const string CONFIG_PARAM_OBS2DEFAULT 		= "OBS2Default";
const string CONFIG_PARAM_NAV1DEFAULT 		= "NAV1Default";
const string CONFIG_PARAM_NAV2DEFAULT 		= "NAV2Default";
const string CONFIG_PARAM_XPDRDEFAULT 		= "XPDRDefault";
const string CONFIG_PARAM_ADF1LDEFAULT 		= "ADF1LeftDefault";
const string CONFIG_PARAM_ADF1RDEFAULT 		= "ADF1RightDefault";
const string CONFIG_PARAM_ADF2LDEFAULT 		= "ADF2LeftDefault";
const string CONFIG_PARAM_ADF2RDEFAULT 		= "ADF2RightDefault";
const string CONFIG_PARAM_COM1DEFAULT 		= "COM1Default";
const string CONFIG_PARAM_COM2DEFAULT 		= "COM2Default";
const string CONFIG_PARAM_MIN         		= "Min";
const string CONFIG_PARAM_MAX         		= "Max";
const string CONFIG_PARAM_STEP        		= "Step";
const string CONFIG_PARAM_CORRECTALT  		= "CorrectAlt";
const string CONFIG_PARAM_CHECKALT  		= "CheckAlt";
const string CONFIG_PARAM_SECTOLIGHT		= "SecondsToLight";
const string CONFIG_PARAM_DELAY     		= "Delay";
const string CONFIG_PARAM_CUSTOMPRESS 		= "CustomPress";
const string CONFIG_PARAM_DEFAULTHGLEFT		= "DefaultHGLeft";
const string CONFIG_PARAM_DEFAULTHGRIGHT	= "DefaultHGRight";
const string CONFIG_PARAM_BANKLIMIT			= "BankLimit";
const string CONFIG_PARAM_AILERONLIMITPCT	= "AileronLimitPCT";
const string CONFIG_PARAM_BANKCOEFFP		= "BankCoeffP";
const string CONFIG_PARAM_BANKCOEFFI		= "BankCoeffI";
const string CONFIG_PARAM_BANKCOEFFD		= "BankCoeffD";
const string CONFIG_PARAM_ELEVATORLIMITPCT	= "ElevatorLimitPCT";
const string CONFIG_PARAM_PITCHCOEFFP		= "PitchCoeffP";
const string CONFIG_PARAM_PITCHCOEFFI		= "PitchCoeffI";
const string CONFIG_PARAM_PITCHCOEFFD		= "PitchCoeffD";
const string CONFIG_PARAM_ALTHOLDCOEFFP		= "AltholdCoeffP";
const string CONFIG_PARAM_ALTHOLDCOEFFI		= "AltholdCoeffI";
const string CONFIG_PARAM_ALTHOLDCOEFFD		= "AltholdCoeffD";
const string CONFIG_PARAM_REALCONTROL  		= "RealControl";
const string CONFIG_PARAM_HYDLIMIT     		= "HydLimit";
const string CONFIG_PARAM_LANGUAGE     		= "Language";
const string CONFIG_PARAM_SPEEDINKN    		= "SpeedInKnots";

class SDConfigBase
{
private:
	static std::list<SDConfigBase*> m_ChildList;

public:
	static char						m_ConfigPath[MAX_PATH];
	static char						m_ConfigFile[MAX_PATH];
	static CIniFile					*m_Ini;
	bool							m_bEnable;

	SDConfigBase() {
		m_bEnable=true;
		m_ChildList.push_back(this);
	}
	~SDConfigBase() {
		m_ChildList.remove(this);
	}

	virtual void Init()=0;
	virtual void Load()=0;

	static void InitAll() {
		for(std::list<SDConfigBase*>::iterator i=m_ChildList.begin();i!=m_ChildList.end();i++) 
			(*i)->Init();
	};

	static void LoadAll() {
		for(std::list<SDConfigBase*>::iterator i=m_ChildList.begin();i!=m_ChildList.end();i++) 
			(*i)->Load();
	};

};

class SDConfigCommon : public SDConfigBase
{
public:
	bool		m_bCTokensEnable;
	bool		m_bXMLTokensEnable;
	bool		m_bKeyIDEnable;
	int 		m_iLanguage;

	typedef map<string, int> CTokensList;
	typedef map<string, int> XMLTokensList;
	typedef map<string, int> KeyIDList;

	CTokensList   m_lCTokens;
	XMLTokensList m_lXMLTokens;
	KeyIDList     m_lKeyID;

	bool LoadCTokens()
	{
		for(int i=0;i<CTOKENS_COUNT;i++) {
			m_lCTokens.insert(std::pair<std::string, int>(g_sCTokens[i],i+1));
		}
		return true;
	}

	bool LoadXMLTokens()
	{
		for(int i=0;i<XMLTOKENS_COUNT;i++) {
			m_lXMLTokens.insert(std::pair<std::string, int>(g_sXMLTokens[i],i+1));
		}
		return true;
	}

	bool LoadKeyID()
	{
		for(int i=0;i<KEYID_COUNT;i++) {
			char buf[256];
			int num=0;
			int ret=sscanf(g_sKeyID[i],"%s",&buf);
			ret=sscanf(g_sKeyID[i],"%d",&num);
			m_lKeyID.insert(std::pair<std::string, int>(buf,num+0x00010000));
		}
		return true;
	}

public:
	virtual void Init() {
		m_bCTokensEnable=false;
		m_bXMLTokensEnable=false;
		m_bKeyIDEnable=false;
		m_iLanguage=0;
	}

	virtual void Load() {
		m_lCTokens.clear();
		m_lXMLTokens.clear();
		m_lKeyID.clear();

		m_bCTokensEnable=LoadCTokens();
		m_bXMLTokensEnable=LoadXMLTokens();
		m_bKeyIDEnable=LoadKeyID();
		m_iLanguage	= m_Ini->ReadInt 	(CONFIG_SECTION_COMMON.c_str(), CONFIG_PARAM_LANGUAGE.c_str()      , m_iLanguage);
	}

	virtual ~SDConfigCommon() {
		m_lCTokens.clear();
		m_lXMLTokens.clear();
		m_lKeyID.clear();
	}
};

class SDConfigToolstips : public SDConfigBase
{
public:
	char		m_sFile[MAX_PATH]; 

public:
	virtual void Init() {
		m_sFile[0]='\0';
		m_bEnable=false;
	}

	virtual void Load() {
		strcpy(m_sFile	,m_ConfigPath);
		strcat(m_sFile	,m_Ini->ReadString(CONFIG_SECTION_TOOLTIPS.c_str(), CONFIG_PARAM_FILE.c_str()      , m_sFile));
		m_bEnable	= m_Ini->ReadBool	(CONFIG_SECTION_TOOLTIPS.c_str(), CONFIG_PARAM_ENABLE.c_str()      , m_bEnable);

		if(m_sFile[0]=='\0'||!PathFileExists(m_sFile)) {
			m_bEnable=false;
		}
	}
};

class SDConfigNAV : public SDConfigBase
{
public:
	bool		m_bOBSZeroStart;
	char		m_sOBS1Default[BUFSIZ];
	char		m_sOBS2Default[BUFSIZ];
	char		m_sNAV1Default[BUFSIZ];
	char		m_sNAV2Default[BUFSIZ];
	char		m_sXPDRDefault[BUFSIZ];
	char		m_sADF1LDefault[BUFSIZ];
	char		m_sADF1RDefault[BUFSIZ];
	char		m_sADF2LDefault[BUFSIZ];
	char		m_sADF2RDefault[BUFSIZ];

public:
	virtual void Init() {
		m_bOBSZeroStart=true;
		strcpy(m_sOBS1Default,"340");
		strcpy(m_sOBS2Default,"000");
		strcpy(m_sNAV1Default,"110.30" );
		strcpy(m_sNAV2Default,"116.80");
		strcpy(m_sXPDRDefault,"1200");
		strcpy(m_sADF1LDefault,"0224");
		strcpy(m_sADF1RDefault,"0353");
		strcpy(m_sADF2LDefault,"0281");
		strcpy(m_sADF2RDefault,"1224");
	}

	virtual void Load() {
		m_bOBSZeroStart         = m_Ini->ReadBool  (CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_OBSZEROSTART.c_str() , m_bOBSZeroStart);
		strcat(m_sOBS1Default  , m_Ini->ReadString(CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_OBS1DEFAULT.c_str()  , m_sOBS1Default));
		if(!_stricoll(m_sOBS1Default,"000")&&!m_bOBSZeroStart) {
			m_sOBS1Default[0]='3';
			m_sOBS1Default[1]='6';
			m_sOBS1Default[2]='0';
		} else if(!_stricoll(m_sOBS1Default,"360")&&!m_bOBSZeroStart) {
			m_sOBS1Default[0]='0';
			m_sOBS1Default[1]='0';
			m_sOBS1Default[2]='0';
		}

		strcat(m_sOBS2Default  ,m_Ini->ReadString(CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_OBS2DEFAULT.c_str()  , m_sOBS2Default));
		if(!_stricoll(m_sOBS2Default,"000")&&!m_bOBSZeroStart) {
			m_sOBS2Default[0]='3';
			m_sOBS2Default[1]='6';
			m_sOBS2Default[2]='0';
		} else if(!_stricoll(m_sOBS2Default,"360")&&!m_bOBSZeroStart) {
			m_sOBS2Default[0]='0';
			m_sOBS2Default[1]='0';
			m_sOBS2Default[2]='0';
		}

		strcat(m_sNAV1Default  , m_Ini->ReadString(CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_NAV1DEFAULT.c_str()  , m_sNAV1Default		));
		strcat(m_sNAV2Default  , m_Ini->ReadString(CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_NAV2DEFAULT.c_str()  , m_sNAV2Default		));
		strcat(m_sXPDRDefault  , m_Ini->ReadString(CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_XPDRDEFAULT.c_str()  , m_sXPDRDefault		));
		strcat(m_sADF1LDefault , m_Ini->ReadString(CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_ADF1LDEFAULT.c_str() , m_sADF1LDefault		));
		strcat(m_sADF1RDefault , m_Ini->ReadString(CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_ADF1RDEFAULT.c_str() , m_sADF1RDefault		));
		strcat(m_sADF2LDefault , m_Ini->ReadString(CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_ADF2LDEFAULT.c_str() , m_sADF2LDefault		));
		strcat(m_sADF2RDefault , m_Ini->ReadString(CONFIG_SECTION_NAV.c_str(), CONFIG_PARAM_ADF2RDEFAULT.c_str() , m_sADF2RDefault		));
	}
};

class SDConfigRadio : public SDConfigBase
{
public:
	char		m_sCOM1Default[BUFSIZ];
	char		m_sCOM2Default[BUFSIZ];

public:
	virtual void Init() {
		strcpy(m_sCOM1Default,"119.125");
		strcpy(m_sCOM2Default,"121.050");
	}

	virtual void Load() {
		strcat(m_sCOM1Default  , m_Ini->ReadString(CONFIG_SECTION_RADIO.c_str(), CONFIG_PARAM_COM1DEFAULT.c_str()  , m_sCOM1Default));
		strcat(m_sCOM2Default  , m_Ini->ReadString(CONFIG_SECTION_RADIO.c_str(), CONFIG_PARAM_COM2DEFAULT.c_str()  , m_sCOM2Default));
	}
};

class SDConfigHtail : public SDConfigBase
{
public:
	double		m_dMin;
	double		m_dMax;
	double		m_dStep;
	double		m_dHydLimit;

public:
	virtual void Init() {
		m_dMin=-9.0;
		m_dMax=0.0;
		m_dStep=0.02;
		m_dHydLimit=10;
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool	(CONFIG_SECTION_HTAIL.c_str(),		CONFIG_PARAM_ENABLE.c_str()		, m_bEnable	);
		m_dMin			= m_Ini->ReadDouble	(CONFIG_SECTION_HTAIL.c_str(),		CONFIG_PARAM_MIN.c_str()		, m_dMin		);
		m_dMax			= m_Ini->ReadDouble	(CONFIG_SECTION_HTAIL.c_str(),		CONFIG_PARAM_MAX.c_str()		, m_dMax		);
		m_dStep			= m_Ini->ReadDouble	(CONFIG_SECTION_HTAIL.c_str(),		CONFIG_PARAM_STEP.c_str()		, m_dStep		);
		m_dHydLimit		= m_Ini->ReadDouble	(CONFIG_SECTION_HTAIL.c_str(),		CONFIG_PARAM_HYDLIMIT.c_str()	, m_dHydLimit);
	}
};

class SDConfigAutopilot : public SDConfigBase
{
public:
	double		m_dBankLimit;
	double		m_dAileronLimitPCT;
	double		m_dBankCoeffP;
	double		m_dBankCoeffI;
	double		m_dBankCoeffD;
	double		m_dElevatorLimitPCT;
	double		m_dPitchCoeffP;
	double		m_dPitchCoeffI;
	double		m_dPitchCoeffD;
	double		m_dAltHoldCoeffP;
	double		m_dAltHoldCoeffI;
	double		m_dAltHoldCoeffD;
	int			m_dSecToLight;

public:
	virtual void Init() {
		m_dBankLimit=30.0;
		m_dAileronLimitPCT=45.0;
		m_dBankCoeffP=2000.0;
		m_dBankCoeffI=1.0;
		m_dBankCoeffD=70.0;
		m_dElevatorLimitPCT=100.0;
		m_dPitchCoeffP=40.0;
		m_dPitchCoeffI=10.0;
		m_dPitchCoeffD=150.0;
		m_dAltHoldCoeffP=50.0;
		m_dAltHoldCoeffI=200.0;
		m_dAltHoldCoeffD=50.0;
		m_dSecToLight=40;
	}

	virtual void Load() {
		m_bEnable			= m_Ini->ReadBool	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_ENABLE.c_str()			, m_bEnable			);
		m_dBankLimit		= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_BANKLIMIT.c_str()		, m_dBankLimit		);
		m_dAileronLimitPCT	= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_AILERONLIMITPCT.c_str(), m_dAileronLimitPCT);
		m_dBankCoeffP		= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_BANKCOEFFP.c_str()		, m_dBankCoeffP		);
		m_dBankCoeffI		= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_BANKCOEFFI.c_str()		, m_dBankCoeffI		);
		m_dBankCoeffD		= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_BANKCOEFFD.c_str()		, m_dBankCoeffD		);
		m_dElevatorLimitPCT = m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_ELEVATORLIMITPCT.c_str(),m_dElevatorLimitPCT );
		m_dPitchCoeffP		= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_PITCHCOEFFP.c_str()	, m_dPitchCoeffP	);
		m_dPitchCoeffI		= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_PITCHCOEFFI.c_str()	, m_dPitchCoeffI	);
		m_dPitchCoeffD		= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_PITCHCOEFFD.c_str()	, m_dPitchCoeffD	);
		m_dAltHoldCoeffP	= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_ALTHOLDCOEFFP.c_str()	, m_dAltHoldCoeffP	);
		m_dAltHoldCoeffI	= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_ALTHOLDCOEFFI.c_str()	, m_dAltHoldCoeffI	);
		m_dAltHoldCoeffD	= m_Ini->ReadDouble	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_ALTHOLDCOEFFD.c_str()	, m_dAltHoldCoeffD	);
		m_dSecToLight		= m_Ini->ReadInt   	(CONFIG_SECTION_AUTOPILOT.c_str(),	CONFIG_PARAM_SECTOLIGHT.c_str()		, m_dSecToLight		);
	}
};

class SDConfigFrontLeg : public SDConfigBase
{
public:
	double		m_dMin;
	double		m_dMax;
	double		m_dHydLimit;

public:
	virtual void Init() {
		m_dMin=5.0;
		m_dMax=55.0;
		m_dHydLimit=30.0;
	}

	virtual void Load() {
		m_bEnable	= m_Ini->ReadBool	(CONFIG_SECTION_FRONTLEG.c_str(),	CONFIG_PARAM_ENABLE.c_str(),	m_bEnable	);
		m_dMin		= m_Ini->ReadDouble	(CONFIG_SECTION_FRONTLEG.c_str(),	CONFIG_PARAM_MIN.c_str()   ,	m_dMin		);
		m_dMax		= m_Ini->ReadDouble	(CONFIG_SECTION_FRONTLEG.c_str(),	CONFIG_PARAM_MAX.c_str()   ,	m_dMax		);
		m_dHydLimit	= m_Ini->ReadDouble	(CONFIG_SECTION_FRONTLEG.c_str(),	CONFIG_PARAM_HYDLIMIT.c_str(),	m_dHydLimit	);
	}
};

class SDConfigFlaps : public SDConfigBase
{
public:
	bool		m_bRealControl;
	double		m_dMin;
	double		m_dMax;
	double		m_dStep;
	double		m_dHydLimit;

public:
	virtual void Init() {
		m_bRealControl=true;
		m_dMin=0;
		m_dMax=35.0;
		m_dStep=0.1;
		m_dHydLimit=30.0;
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool	(CONFIG_SECTION_FLAPS.c_str(),		CONFIG_PARAM_ENABLE.c_str(),		m_bEnable		);
		m_bRealControl  = m_Ini->ReadBool	(CONFIG_SECTION_FLAPS.c_str(),		CONFIG_PARAM_REALCONTROL.c_str(),	m_bRealControl  );
		m_dMin			= m_Ini->ReadDouble	(CONFIG_SECTION_FLAPS.c_str(),		CONFIG_PARAM_MIN.c_str()   ,		m_dMin			);
		m_dMax			= m_Ini->ReadDouble	(CONFIG_SECTION_FLAPS.c_str(),		CONFIG_PARAM_MAX.c_str()   ,		m_dMax			);
		m_dStep			= m_Ini->ReadDouble	(CONFIG_SECTION_FLAPS.c_str(),		CONFIG_PARAM_STEP.c_str()  ,		m_dStep			);
		m_dHydLimit		= m_Ini->ReadDouble	(CONFIG_SECTION_FLAPS.c_str(),		CONFIG_PARAM_HYDLIMIT.c_str(),		m_dHydLimit		);
	}
};

class SDConfigGear : public SDConfigBase
{
public:
	bool		m_bRealControl;

public:
	virtual void Init() {
		m_bRealControl=false;
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool		(CONFIG_SECTION_GEAR.c_str(),		CONFIG_PARAM_ENABLE.c_str(),		m_bEnable		);
		m_bRealControl	= m_Ini->ReadBool		(CONFIG_SECTION_GEAR.c_str(),		CONFIG_PARAM_REALCONTROL.c_str(),	m_bRealControl	);
	}
};

class SDConfigAchs : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool		(CONFIG_SECTION_ACHS.c_str(),		CONFIG_PARAM_ENABLE.c_str(), m_bEnable);
	}
};

class SDConfigAirtemp : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool		(CONFIG_SECTION_AIRTEMP.c_str(),		CONFIG_PARAM_ENABLE.c_str(), m_bEnable);
	}
};

class SDConfigGforce : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool		(CONFIG_SECTION_GFORCE.c_str(),		CONFIG_PARAM_ENABLE.c_str(), m_bEnable);
	}
};

class SDConfigPPTIZ : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool		(CONFIG_SECTION_PPTIZ.c_str(),		CONFIG_PARAM_ENABLE.c_str(), m_bEnable);
	}
};

class SDConfigRV3M : public SDConfigBase
{
public:
	double		m_dCorrectAlt;
	double		m_dCheckAlt;
	double		m_dSecToLight;
	double		m_dMin;
	double		m_dMax;

public:
	virtual void Init() {
		m_dCorrectAlt=3.0;
		m_dCheckAlt=0.5;
		m_dSecToLight=3.0;
		m_dMin=-10.0;
		m_dMax=600.0;
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool	(CONFIG_SECTION_RV3M.c_str(),		CONFIG_PARAM_ENABLE.c_str()		, m_bEnable		);
		m_dCorrectAlt	= m_Ini->ReadDouble	(CONFIG_SECTION_RV3M.c_str(),		CONFIG_PARAM_CORRECTALT.c_str()	, m_dCorrectAlt	);
		m_dCheckAlt		= m_Ini->ReadDouble	(CONFIG_SECTION_RV3M.c_str(),		CONFIG_PARAM_CHECKALT.c_str()	, m_dCheckAlt		);
		m_dSecToLight	= m_Ini->ReadDouble	(CONFIG_SECTION_RV3M.c_str(),		CONFIG_PARAM_SECTOLIGHT.c_str()	, m_dSecToLight	);
		m_dMin       	= m_Ini->ReadDouble	(CONFIG_SECTION_RV3M.c_str(),		CONFIG_PARAM_MIN.c_str()		, m_dMin       	);
		m_dMax       	= m_Ini->ReadDouble	(CONFIG_SECTION_RV3M.c_str(),		CONFIG_PARAM_MAX.c_str()		, m_dMax       	);
	}
};

class SDConfigKUS : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool		(CONFIG_SECTION_KUS.c_str(),		CONFIG_PARAM_ENABLE.c_str(), m_bEnable);
	}
};

class SDConfigVS : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool		(CONFIG_SECTION_VS.c_str(),		CONFIG_PARAM_ENABLE.c_str(), m_bEnable);
	}
};

class SDConfigEngines : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool		(CONFIG_SECTION_ENGINES.c_str(),		CONFIG_PARAM_ENABLE.c_str(), m_bEnable);
	}
};

class SDConfigDA30 : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable		= m_Ini->ReadBool		(CONFIG_SECTION_DA30.c_str(),		CONFIG_PARAM_ENABLE.c_str(), m_bEnable);
	}
};

class SDConfigUVID : public SDConfigBase
{
public:
	bool		m_bCustomPress;
	double		m_iDefaultHGLeft;
	double		m_iDefaultHGRight;

public:
	virtual void Init() {
		bool			g_bCustomPress=true;
		double			g_iDefaultHGLeft=29.92;
		double			g_iDefaultHGRight=29.92;
	}

	virtual void Load() {
		m_bEnable			= m_Ini->ReadBool	(CONFIG_SECTION_UVID.c_str(),		CONFIG_PARAM_ENABLE.c_str()		,	 m_bEnable		);
		m_bCustomPress		= m_Ini->ReadBool	(CONFIG_SECTION_UVID.c_str(),		CONFIG_PARAM_CUSTOMPRESS.c_str(),	 m_bCustomPress	);
		m_iDefaultHGLeft	= m_Ini->ReadDouble	(CONFIG_SECTION_UVID.c_str(),		CONFIG_PARAM_DEFAULTHGLEFT.c_str() , m_iDefaultHGLeft);
		m_iDefaultHGRight	= m_Ini->ReadDouble	(CONFIG_SECTION_UVID.c_str(),		CONFIG_PARAM_DEFAULTHGRIGHT.c_str(), m_iDefaultHGRight);
	}
};

class SDConfigKPPMS : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable			= m_Ini->ReadBool	(CONFIG_SECTION_KPPMS.c_str(),		CONFIG_PARAM_ENABLE.c_str()		, m_bEnable);
	}
};

class SDConfigIKU : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable			= m_Ini->ReadBool	(CONFIG_SECTION_IKU.c_str(),		CONFIG_PARAM_ENABLE.c_str()		, m_bEnable);
	}
};

class SDConfigAGB : public SDConfigBase
{
public:

public:
	virtual void Init() {
	}

	virtual void Load() {
		m_bEnable			= m_Ini->ReadBool	(CONFIG_SECTION_AGB.c_str(),		CONFIG_PARAM_ENABLE.c_str()		, m_bEnable);
	}
};

class SDConfigSound : public SDConfigBase
{
public:
	std::string		m_sSoundPack;

public:
	virtual void Init() {
		m_sSoundPack="";
	}

	virtual void Load() {
		m_bEnable			= m_Ini->ReadBool	(CONFIG_SECTION_SOUND.c_str(), CONFIG_PARAM_ENABLE.c_str()		, m_bEnable);
		m_sSoundPack		= m_Ini->ReadString (CONFIG_SECTION_SOUND.c_str(), CONFIG_PARAM_SOUND_PACK.c_str()  , "");
	}
};

class SDConfig 
{
private:
	bool m_bLoaded;

public:
	SDConfigCommon		m_CfgCommon;
	SDConfigToolstips	m_CfgTooltips;
	SDConfigNAV			m_CfgNav;
	SDConfigRadio		m_CfgRadio;
	SDConfigHtail		m_CfgHtail;
	SDConfigAutopilot	m_CfgAutopilot;
	SDConfigFrontLeg	m_CfgFrontLeg;
	SDConfigFlaps		m_CfgFlaps;
	SDConfigGear		m_CfgGear;
	SDConfigAchs		m_CfgAchs;
	SDConfigAirtemp		m_CfgAirtemp;
	SDConfigGforce		m_CfgGforce;
	SDConfigPPTIZ		m_CfgPPTIZ;
	SDConfigRV3M		m_CfgRV3M;
	SDConfigKUS			m_CfgKUS; 
	SDConfigVS			m_CfgVS;	
	SDConfigEngines		m_CfgEngines;
	SDConfigDA30		m_CfgDA30;
	SDConfigUVID		m_CfgUVID;
	SDConfigKPPMS		m_CfgKPPMS;
	SDConfigIKU			m_CfgIKU;
	SDConfigAGB			m_CfgAGB;
	SDConfigSound		m_CfgSound;

	SDConfig() {
		m_bLoaded=false;
	};
	
	~SDConfig() {
		if(SDConfigBase::m_Ini) {
			delete SDConfigBase::m_Ini;
			SDConfigBase::m_Ini=NULL;
		}
	};

	void Load() {
		if(!SDConfigBase::m_Ini)
			SDConfigBase::m_Ini=new CIniFile(SDConfigBase::m_ConfigFile);

		SDConfigBase::InitAll();
		SDConfigBase::LoadAll();
	}

};