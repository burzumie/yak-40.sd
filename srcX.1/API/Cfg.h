/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Cfg.h $

  Last modification:
    $Date: 19.02.06 5:27 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

enum CFG {
	CFG_OBS_ZERO_START,
	CFG_SOUND_ENABLE,
	CFG_SOUND_SPEED_IN_KNOTS,
	CFG_SOUND_ALT_IN_FEET,
	CFG_MAX
};

enum CFG_STR {
	CFG_STR_SOUND_PACK,
	CFG_STR_SOUND_PATH,
	CFG_STR_MAX
};
