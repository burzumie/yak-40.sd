/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Krm.cpp $

  Last modification:
    $Date: 19.02.06 7:03 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Krm.h"
#include "ApiName.h"

#ifdef _DEBUG

#define sd_6015_p0_201	""                                
#define sd_6015_p0_202	""                                
#define sd_6015_p0_203	""                                
#define sd_6015_p0_204	""                                
#define sd_6015_p0_205	""                                
															
															
#define sd_6015_p1_201	""                                
#define sd_6015_p1_202	""                                
#define sd_6015_p1_203	""                                

#else

#define sd_6015_p0_201	""                                
#define sd_6015_p0_202	""                                
#define sd_6015_p0_203	""                                
#define sd_6015_p0_204	""                                
#define sd_6015_p0_205	""                                


#define sd_6015_p1_201	""                                
#define sd_6015_p1_202	""                                
#define sd_6015_p1_203	""                                

#endif

ApiName KrmNames[KRM_MAX]={                
	// P0
	{"sd_6015_p0_201",sd_6015_p0_201,"",  0, 19,true ,false},  // KRM_AGB_MAIN0
	{"sd_6015_p0_202",sd_6015_p0_202,"",  0, 19,true ,false},  // KRM_AGB_AUXL
	{"sd_6015_p0_203",sd_6015_p0_203,"",  0, 19,true ,false},  // KRM_RV3M     
	{"sd_6015_p0_204",sd_6015_p0_204,"",  0, 19,true ,false},  // KRM_KPPMS0   
	{"sd_6015_p0_205",sd_6015_p0_205,"",  0, 19,true ,false},  // KRM_UVID0    
										  
	// P1			   
	{"sd_6015_p1_201",sd_6015_p1_201,"",  0, 19,true ,false},  // KRM_AGB_MAIN1
	{"sd_6015_p1_202",sd_6015_p1_202,"",  0, 19,true ,false},  // KRM_UVID1     
	{"sd_6015_p1_203",sd_6015_p1_203,"",  0, 19,true ,false},  // KRM_KPPMS1    
														  
};														   

