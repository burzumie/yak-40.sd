/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Api/Tbl.h $

  Last modification:
    $Date: 18.02.06 16:24 $
    $Revision: 1 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#pragma once

enum TBL {                       
	// �����
	TBL_ASPD_LOW                ,   // "sd_6015_pa_701"
	TBL_EXIT_OPEN               ,	// "sd_6015_pa_702"
	TBL_WARN_GEAR               ,	// "sd_6015_pa_703"
	TBL_BANK_L_HIGH             ,	// "sd_6015_pa_704"
	TBL_ADI_FAIL                ,	// "sd_6015_pa_705"
	TBL_BANK_R_HIGH             ,	// "sd_6015_pa_706"
	TBL_MARKER                  ,	// "sd_6015_pa_707"
	TBL_VOR1_TO		            ,	// "sd_6015_pa_708"
	TBL_VOR1_FROM				,	// "sd_6015_pa_709"
	TBL_CAB_ALT_HIGH            ,	// "sd_6015_pa_710"
	TBL_CAB_AIR_PRESS_HIGH      ,	// "sd_6015_pa_711"
	TBL_INV_36V_FAIL            ,	// "sd_6015_pa_712"
	TBL_INV_115V_FAIL           ,	// "sd_6015_pa_713"
	TBL_OIL_PRESS_LOW           ,	// "sd_6015_pa_714"
	TBL_AIR_STARTER_ON          ,	// "sd_6015_pa_715"
	TBL_FUEL_LOW_L              ,	// "sd_6015_pa_716"
	TBL_FUEL_LOW_R              ,	// "sd_6015_pa_717"
	TBL_ENG1_VIB_HIGH           ,	// "sd_6015_pa_718"
	TBL_ENG2_VIB_HIGH           ,	// "sd_6015_pa_719"
	TBL_ENG3_VIB_HIGH           ,	// "sd_6015_pa_720"
	TBL_STALL                   ,	// "sd_6015_pa_721"
	TBL_OUTER_MARKER            ,	// "sd_6015_pa_722"
	TBL_MIDDLE_MARKER           ,	// "sd_6015_pa_723"
	TBL_INNER_MARKER            ,	// "sd_6015_pa_724"
	TBL_APU_STARTER             ,	// "sd_6015_pa_725"
	TBL_APU_OIL_PRESS_NORM      ,	// "sd_6015_pa_726"
	TBL_APU_FUEL_VALVE_OPEN     ,	// "sd_6015_pa_727"
	TBL_APU_ON_FIRE             ,	// "sd_6015_pa_728"
	TBL_APU_RPM_NORM            ,	// "sd_6015_pa_729"
	TBL_APU_RPM_HIGH            ,   // "sd_6015_pa_730"
	TBL_HYD_PUMP_ENG1_FAIL      ,	// "sd_6015_pa_731"
	TBL_HYD_PUMP_ENG2_FAIL      ,	// "sd_6015_pa_732"
	TBL_RAD_ALTIMETER_FAIL      ,	// "sd_6015_pa_733"
	TBL_FIRE                    ,	// "sd_6015_pa_734"
	TBL_CABIN_CREW              ,	// "sd_6015_pa_735"
	TBL_ENG_OVERHEAT            ,	// "sd_6015_pa_736"
	TBL_ICE_ENG_L               ,	// "sd_6015_pa_737"
	TBL_ICE_ENG_R               ,	// "sd_6015_pa_738"
	TBL_GEN1_FAIL               ,	// "sd_6015_pa_739"
	TBL_GEN2_FAIL               ,	// "sd_6015_pa_740"
	TBL_GEN3_FAIL               ,	// "sd_6015_pa_741"
	TBL_GMK01                   ,	// "sd_6015_pa_742"
	TBL_GMK02                   ,	// "sd_6015_pa_743"
	TBL_GMK03                   ,	// "sd_6015_pa_744"
	TBL_GMK04                   ,	// "sd_6015_pa_745"
	TBL_GMK05                   ,	// "sd_6015_pa_746"
	TBL_GMK06                   ,	// "sd_6015_pa_747"
	TBL_BANK3_L_HIGH            ,	// "sd_6015_pa_748"
	TBL_BANK3_R_HIGH            ,	// "sd_6015_pa_749"
	TBL_VOR2_TO		            ,	// "sd_6015_pa_750"
	TBL_VOR2_FROM				,	// "sd_6015_pa_751"

	//
	TBL_MAX                      	
};                               	
