#include "stdafx.h"
#include "ConfigMain.h"
#include "ConfigDialog.h"
#include "../Common/CommonAircraft.h"

#ifdef _DEBUG
_CrtMemState memoryState; 
#endif

HINSTANCE g_hInstance=NULL;
SDSimVars *g_pSimVars=NULL;

CConfigDialog *pConfigDialog=NULL;
bool m_AlreadyInited=false;

void FSAPI fnInitPanel(void);

CSimConnect		*g_SimData				= NULL;
HANDLE			hFileMapping			= NULL;

void OpenPipe();
void ClosePipe();

void UpdatePanel()
{
	if(g_pSimVars)
		g_pSimVars->UpdateVars();

	if(pConfigDialog) {
		if(!pConfigDialog->m_hWnd) {
			pConfigDialog->Create(IDD_MAINFORM);
		}
		if(POS_GET(POS_GROUND_STARTUP)) {	
			//g_pSD6015_SIM->UpdatePayload=true;
			if(!pConfigDialog->IsWindowVisible()) {
				//g_pSD6015_SIM->ProcessFuel=true;
				pConfigDialog->ShowWindow(SW_SHOW);
			}
			pConfigDialog->Update();
		} else {
			//g_pSD6015_SIM->UpdatePayload=false;
			if(pConfigDialog->IsWindowVisible()) {
				//g_pSD6015_SIM->ProcessFuel=false;
				pConfigDialog->ShowWindow(SW_HIDE);
			}
		}
	}
}

void FSAPI fnInitPanel(void)
{
#ifdef _DEBUG
	_CrtMemCheckpoint(&memoryState);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
#endif

}

void FSAPI fnDeinitPanel(void)
{
	if(pConfigDialog) {
		delete pConfigDialog;
		pConfigDialog=NULL;
	}

	if(g_pSimVars) {
		delete g_pSimVars;
		g_pSimVars=NULL;
	}

	if(g_SimModules) {
		delete g_SimModules;
		g_SimModules=NULL;
	}

	ClosePipe();

#ifdef _DEBUG
	CHECK_MEM_LEAK("f:\\MemLeaks\\Yak40\\CF.log");
#endif
}

#ifdef GAUGE_NAME
#undef GAUGE_NAME
#endif

#ifdef GAUGEHDR_VAR_NAME
#undef GAUGEHDR_VAR_NAME
#endif

#ifdef GAUGE_W
#undef GAUGE_W
#endif

#define GAUGE_NAME         "main"
#define GAUGEHDR_VAR_NAME  cfg_hdr
#define GAUGE_W            MISC_DUMMY_SX

extern PELEMENT_HEADER cfg_list;
GAUGE_CALLBACK cfg_cb;
GAUGE_HEADER_FS700(GAUGE_W, GAUGE_NAME, &cfg_list, 0, cfg_cb, 0, 0, 0);
MAKE_STATIC(cfg_bg,MISC_DUMMY,NULL,NULL,IMAGE_USE_TRANSPARENCY,0,0,0)
PELEMENT_HEADER	cfg_list = &cfg_bg.header;

void FSAPI cfg_cb( PGAUGEHDR pgauge, SINT32 service_id, UINT32 extra_data ) 
{
	switch(service_id) {
		case PANEL_SERVICE_CONNECT_TO_WINDOW:
			OpenPipe();
			if(!g_SimModules) {
				g_SimModules=new CSimModulesContainer();
			}
			if(!g_pSimVars) {
				g_pSimVars=new SDSimVars();
				g_pSimVars->InitVars();
			}
			initialize_var_by_name(&MVSDAPIEntrys,SDAPISIGN); 
			g_pSDAPIEntrys=(SDAPIEntrys *)MVSDAPIEntrys.var_ptr; 
			if(!pConfigDialog) {
				if((pConfigDialog=new CConfigDialog())) {
					if(pConfigDialog->Create(IDD_MAINFORM)) {
						pConfigDialog->Update();
					} else {
						delete pConfigDialog;
						pConfigDialog=NULL;
					}
				}
			}
		break;
		case PANEL_SERVICE_PRE_UPDATE:
			UpdatePanel(); 
		break;
	}
}

GAUGESIMPORT	ImportTable =						
{													
	{ 0x0000000F, (PPANELS)NULL },						
	{ 0x00000000, NULL }								
};													

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&cfg_hdr),
		0
	}
};

void OpenPipe()
{
	hFileMapping=OpenFileMapping(FILE_MAP_READ|FILE_MAP_WRITE,FALSE,SIM_SHARE_NAME);

	if(hFileMapping==NULL) {
		return;
	}

	g_SimData=(CSimConnect *)MapViewOfFile(hFileMapping,FILE_MAP_READ|FILE_MAP_WRITE,0,0,sizeof(CSimConnect));

	if(g_SimData==NULL) {
		return;
	}
}

void ClosePipe()
{
	if(g_SimData) {	
		UnmapViewOfFile(g_SimData);
		g_SimData=NULL;
	}
	if(hFileMapping) {
		CloseHandle(hFileMapping);
		hFileMapping=NULL;
	}
}

