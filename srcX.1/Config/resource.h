//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Config.rc
//
#define MISC_DUMMY_SX                   1
#define MISC_DUMMY_SY                   1
#define IDR_MAINFRAME                   5
#define IDD_DIALOG1                     1000
#define IDD_MAINFORM                    1000
#define IDC_MESSAGEHEADER               1000
#define IDD_MESSAGEFORM                 1001
#define IDC_BTNPAXFULL                  1002
#define IDC_BTNPAX75PCT                 1003
#define IDC_BTNPAX50PCT                 1004
#define IDC_BTNPAX25PCT                 1005
#define IDB_SALON27                     1006
#define IDC_BTNPAXEMPTY                 1006
#define IDC_ENGINEER                    1007
#define IDC_STEWARDESS                  1008
#define IDC_EDITEQUIP                   1009
#define IDC_SLIDEREQUIP                 1010
#define IDC_EDITACRGO                   1011
#define IDC_HAND_MOVE                   1011
#define IDC_EDITCARGO                   1011
#define IDC_SLIDERCARGO                 1012
#define IDC_HAND_POINT                  1012
#define IDC_EQUIP                       1013
#define IDC_CARGO                       1014
#define IDB_BITMAP1                     1014
#define IDB_TITLE                       1014
#define IDC_LEFTTANK                    1015
#define IDC_EDITLEFTTANK                1016
#define IDC_SLIDERLEFTTANK              1017
#define IDC_RIGHTTANK                   1018
#define IDC_EDITRIGHTTANK               1019
#define IDC_SLIDERRIGHTTANK             1020
#define IDC_TOTALW                      1021
#define IDC_EDITTOTALW                  1022
#define IDC_MAXW                        1023
#define IDC_EDITMAXW                    1024
#define IDC_CURCG                       1025
#define IDC_EDITCURCG                   1026
#define IDC_MINCG                       1027
#define IDC_EDITMINCG                   1028
#define IDC_MAXCG                       1029
#define IDC_EDITMAXCG                   1030
#define IDC_STABPOS                     1031
#define IDC_EDITSTABPOS                 1032
#define IDC_SALON27                     1033
#define IDC_CHKPILOT                    1065
#define IDC_CHKCOPILOT                  1066
#define IDC_CHKENGINEER                 1067
#define IDC_CHKROW1SEAT1                1068
#define IDC_CHKROW1SEAT2                1069
#define IDC_CHKROW1SEAT3                1070
#define IDC_CHKROW2SEAT3                1071
#define IDC_CHKROW2SEAT2                1072
#define IDC_CHKROW2SEAT1                1073
#define IDC_CHKROW4SEAT1                1074
#define IDC_CHKROW4SEAT2                1075
#define IDC_CHKROW4SEAT3                1076
#define IDC_CHKROW3SEAT1                1077
#define IDC_CHKROW3SEAT2                1078
#define IDC_CHKROW3SEAT3                1079
#define IDC_CHKROW8SEAT1                1080
#define IDC_CHKROW8SEAT2                1081
#define IDC_CHKROW8SEAT3                1082
#define IDC_CHKROW7SEAT1                1083
#define IDC_CHKROW7SEAT2                1084
#define IDC_CHKROW7SEAT3                1085
#define IDC_CHKROW6SEAT1                1086
#define IDC_CHKROW6SEAT2                1087
#define IDC_CHKROW6SEAT3                1088
#define IDC_CHKROW5SEAT1                1089
#define IDC_CHKROW5SEAT2                1090
#define IDC_CHKROW5SEAT3                1091
#define IDC_CHKROW9SEAT1                1092
#define IDC_CHKROW9SEAT2                1093
#define IDC_CHKROW9SEAT3                1094
#define IDC_CHKRAP                      1096
#define IDC_CHKAIR                      1097
#define IDC_BTNCLOSE                    1098
#define IDC_TITLE                       1099
#define MISC_DUMMY                      5931

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1015
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1100
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
