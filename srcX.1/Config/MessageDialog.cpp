// MessageDialog.cpp : implementation file
//

#include "stdafx.h"
#include "Config.h"
#include "MessageDialog.h"
#include ".\messagedialog.h"

// CMessageDialog dialog

#define WS_EX_LAYERED     0x80000
#define LWA_ALPHA         0x02
//BOOL (WINAPI* SetLayeredWindowAttributes)(HWND,int,int,DWORD);

IMPLEMENT_DYNAMIC(CMessageDialog, CDialog)
CMessageDialog::CMessageDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMessageDialog::IDD, pParent)
{
}

CMessageDialog::~CMessageDialog()
{
}

void CMessageDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MESSAGEHEADER, cstHeader);
}


BEGIN_MESSAGE_MAP(CMessageDialog, CDialog)
	ON_WM_PAINT()
//	ON_STN_CLICKED(IDC_MESSAGEHEADER, OnStnClickedMessageheader)
ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CMessageDialog message handlers

void CMessageDialog::Update()
{

}


BOOL CMessageDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

/*
	HINSTANCE hLib = LoadLibrary("user32.dll");
	SetLayeredWindowAttributes=(int(__stdcall*)(HWND,int,int,DWORD))GetProcAddress(hLib,"SetLayeredWindowAttributes");
	SetWindowLong(this->m_hWnd,GWL_EXSTYLE,GetWindowLong(this->m_hWnd,GWL_EXSTYLE) | WS_EX_LAYERED);
	SetLayeredWindowAttributes(m_hWnd,0,128,LWA_ALPHA);
*/

	return TRUE;
}

void CMessageDialog::OnPaint()
{
	CPaintDC dc(this);

	RECT rect;
	CBrush brush1; 

	GetClientRect(&rect);
	brush1.CreateSolidBrush(RGB(0,0,200));

	dc.FillRect(&rect,&brush1);

	CDialog::OnPaint();
}

//void CMessageDialog::OnStnClickedMessageheader()
//{
//	// TODO: Add your control notification handler code here
//}

HBRUSH CMessageDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetDlgCtrlID() == IDC_MESSAGEHEADER)
	{
		// Set the text color to red
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(0, 0, 255));

		// Set the background mode for text to transparent 
		// so background will show thru.
		pDC->SetBkMode(OPAQUE);

		// Return handle to our CBrush object
		//hbr = m_brush;
	}

	//CDialog::OnCtlColor(pDC,pWnd,nCtlColor);

	return hbr;
}
