/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#pragma once

#include "Main.h"
#include "External.h"

enum INT_SOUNDS {
	INT_SOUND_APU 				,
	INT_SOUND_AZS   			,
	INT_SOUND_BATTERY			,
	INT_SOUND_BUTTON			,
	INT_SOUND_GALETA			,
	INT_SOUND_GYRO				,
	INT_SOUND_NOSELEG			,
	INT_SOUND_RV_VPR			,
	INT_SOUND_SIRENA			,
	INT_SOUND_STAIRSCLOSE		,
	INT_SOUND_STAIRSOPEN		,
	INT_SOUND_TUMBLER			,
	INT_SOUND_ZVONOK			,
	INT_SOUND_INNER 			,
	INT_SOUND_MIDDLE			,
	INT_SOUND_OUTER 			,
	INT_SOUND_PT500				,
	INT_SOUND_FV_AI25_OPEN1		,	
	INT_SOUND_FV_AI25_CLOSE1	,	
	INT_SOUND_FV_AI25_OPEN2		,	
	INT_SOUND_FV_AI25_CLOSE2	,	
	INT_SOUND_FV_AI25_OPEN3		,	
	INT_SOUND_FV_AI25_CLOSE3	,	
	INT_SOUND_SIRENA_FIRE		,
	INT_SOUND_MAX
};

class CSounder : public SDSimVars
{
public:
	CSounder();
	~CSounder();

	void Init();
	void Update();

private:
	HRESULT hr;
	std::auto_ptr<CExternalSounder> m_ExternalSounder;
	std::auto_ptr<sndkit::CSoundManager> m_SndMgr;
	sndkit::CSound *m_Sounds[INT_SOUND_MAX];

	void CheckMuted();

	void PrecessAZS();
	void ProcessGLT();
	void ProcessPT500();
	void ProcessPO1500();
	void ProcessMarkers();
	void ProcessAPU();
	void ProcessPKAI25();
	void ProcessRing();
	void ProcessStairs();
	void ProcessFire();
};

extern CSounder *g_pSounder;
