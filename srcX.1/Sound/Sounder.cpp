/*=========================================================================

SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

$Archive: /6015v2.root/6015v2/Src/Logic/Main.h $

Last modification:
$Date: 18.02.06 18:04 $
$Revision: 3 $
$Author: Except $

Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

All rights reserved.

=========================================================================*/

#include "Sounder.h"
#include "../../Res/sn/snd_resX.h"
#include "../Common/CommonAircraft.h"

CSounder *g_pSounder=NULL;

static int resources[INT_SOUND_MAX]={
	SND_SD6015_APU_AI9				,
	SND_SD6015_AZS					,
	SND_SD6015_BATTERYON			,		
	SND_SD6015_BUTTON				,
	SND_SD6015_GALETA				,
	SND_SD6015_GYRO					,
	SND_SD6015_NOSELEG				,	
	SND_SD6015_RV_VPR				,
	SND_SD6015_SIRENA				,
	SND_SD6015_STAIRSCLOSE			,	
	SND_SD6015_STAIRSOPEN			,			
	SND_SD6015_TUMBLER				,		
	SND_SD6015_ZVONOK3SEC			,
	SND_SD6015_INNER				,
	SND_SD6015_MIDDLE				,
	SND_SD6015_OUTER 				,
	SND_SD6015_PT500 				,
	SND_SD6015_FUELVALVE_AI25_OPEN  ,
	SND_SD6015_FUELVALVE_AI25_CLOSE	,
	SND_SD6015_FUELVALVE_AI25_OPEN  ,
	SND_SD6015_FUELVALVE_AI25_CLOSE	,
	SND_SD6015_FUELVALVE_AI25_OPEN  ,
	SND_SD6015_FUELVALVE_AI25_CLOSE	,
	SND_SD6015_SIRENA				,
};

CSounder::CSounder()
{
	m_SndMgr=std::auto_ptr<sndkit::CSoundManager>(new sndkit::CSoundManager());
	if( FAILED( hr = m_SndMgr->Initialize(g_hInstance, DSSCL_NORMAL, ::FindWindow("FS98MAIN", NULL))) ) {
		DXTRACE_ERR( TEXT("Initialize"), hr );
	};
	for(int i=0;i<INT_SOUND_MAX;i++) {
		m_Sounds[i]=NULL;
	}
	m_ExternalSounder=std::auto_ptr<CExternalSounder>(new CExternalSounder(m_SndMgr.get()));
	m_ExternalSounder->LoadAll();
}

CSounder::~CSounder()
{
}

void CSounder::Init()
{
	char buf[32];
	WCHAR wbuf[64];
	for(int i=0;i<INT_SOUND_MAX;i++) {
		if(!m_Sounds[i]) {
			sprintf(buf,"#%d\0",resources[i]);
			ConvertAnsiStringToWideCch(wbuf,buf,64);
			if( FAILED( hr = m_SndMgr->Create(&m_Sounds[i], wbuf, DSBCAPS_CTRLVOLUME, GUID_NULL, 1))) {
				DXTRACE_ERR( TEXT("CreateSound"), hr );
			};
		}
	}
	InitVars();
}

void CSounder::CheckMuted()
{
	int snd_master=int(POS_GET(POS_SOUND_MASTER));
	VIEW_MODE viewmode=(VIEW_MODE)int(POS_GET(POS_ACTIVE_VIEW_MODE));

	if(snd_master) {
		if(viewmode==VIEW_MODE_COCKPIT||viewmode==VIEW_MODE_VIRTUAL_COCKPIT) {
			for(int i=0;i<INT_SOUND_MAX;i++) {
				m_Sounds[i]->RestoreVolume();
			}
		} else {
			for(int i=0;i<INT_SOUND_MAX;i++) {
				m_Sounds[i]->Mute();
			}
		}
	} else {
		for(int i=0;i<INT_SOUND_MAX;i++) {
			m_Sounds[i]->Mute();
		}
	}
}

void CSounder::Update()
{
	if(POS_GET(POS_MAIN_IS_LOADING))
		return;

	UpdateVars();

	CheckMuted();

	m_ExternalSounder->Update();

	PrecessAZS();
	ProcessGLT();
	ProcessPT500();
	ProcessPO1500();
	ProcessMarkers();
	ProcessAPU();
	ProcessPKAI25();
	ProcessRing();
	ProcessStairs();
	ProcessFire();

}

void CSounder::PrecessAZS()
{
	int azsprev=0;
	for(int i=0;i<AZS_MAX;i++) {
		if(AZS_CHGNR(i)) {
			int azsval=AZS_GET(i);
			switch(i) {
				case AZS_KMP1VOR		:
				case AZS_KMP1PWR		:
				case AZS_KMP2VOR		:
				case AZS_KMP2PWR		:
				case AZS_2077PWR		:
				case AZS_XPDRMAYDAY		:
				case AZS_SO72PWR		:
				case AZS_SO72EMERG		:
				case AZS_ARK1DIR		:
				case AZS_ARK2DIR		:
				case AZS_ARK1CW			:
				case AZS_ARK2CW			:
				case AZS_KMP_RCVR		:
				case AZS_KMP_DAYNIGHT	:
				case AZS_KMP_RSBN_DME	:
				case AZS_GMK_HEMISPHERE	:
				case AZS_GMK_MODE		:
				case AZS_GMK_TEST		:
				case AZS_GMK_COURSE		:
				case AZS_GMK_PRI_AUX	:
					m_Sounds[INT_SOUND_GALETA]->Play();
					break;
				case AZS_POWERSOURCE	:
					if(azsval==POWER_SOURCE_BAT||(azsval==POWER_SOURCE_RAP&&PWR_GET(PWR_BUS27EXT)))
						m_Sounds[INT_SOUND_BATTERY]->Play();
					else
						m_Sounds[INT_SOUND_AZS]->Play();
					break;
				case AZS_HYD_PUMP		:
				case AZS_EMERG_ENG1_CUT	:
				case AZS_EMERG_ENG2_CUT	:
				case AZS_EMERG_ENG3_CUT	:
				case AZS_EMERG_GEAR		:
				case AZS_XFEED_TANKS	:
				case AZS_XFEED_PUMPS	:
				case AZS_PKAI251		:
				case AZS_PKAI252		:
				case AZS_PKAI253		:
				case AZS_TOPL_PRAV_AVAR	:
					azsprev=AZS_GETPREV(i);
					if(azsprev==0&&(azsval==1||azsval==2)) {
						m_Sounds[INT_SOUND_TUMBLER]->Play();
					} else if(azsprev==1&&(azsval==0||azsval==3)) {
						m_Sounds[INT_SOUND_TUMBLER]->Play();
					} else if(azsprev==2&&(azsval==0||azsval==3)) {
						m_Sounds[INT_SOUND_TUMBLER]->Play();
					} else if(azsprev==3&&(azsval==1||azsval==2)) {
						m_Sounds[INT_SOUND_TUMBLER]->Play();
					} else {
						m_Sounds[INT_SOUND_AZS]->Play();
					}
					break;
				default:
					m_Sounds[INT_SOUND_AZS]->Play();
					break;
			}
		}
	}
}

void CSounder::ProcessGLT()
{
	for(int i=0;i<GLT_MAX;i++) {
		if(GLT_CHGNR(i)) {
			m_Sounds[INT_SOUND_GALETA]->Play();
		}
	}
}

void CSounder::ProcessPT500()
{
	static bool alreadystopped=false;
	if(PWR_GET(PWR_BUS36)) {
		alreadystopped=false;
		m_Sounds[INT_SOUND_GYRO]->Play();
	} else {
		if(!alreadystopped) {
			m_Sounds[INT_SOUND_GYRO]->Stop();
			alreadystopped=true;
		}
	}
}

void CSounder::ProcessPO1500()
{
	static bool alreadystopped=false;
	if(PWR_GET(PWR_BUS115)) {
		alreadystopped=false;
		m_Sounds[INT_SOUND_PT500]->Play();
	} else {
		if(!alreadystopped) {
			m_Sounds[INT_SOUND_PT500]->Stop();
			alreadystopped=true;
		}
	}
}

void CSounder::ProcessMarkers()
{
	if(!AZS_GET(AZS_KMP_RCVR)&&PWR_GET(PWR_BUS27)) {
		int marker=int(GetN(MARKER_BEACON_STATE));
		// Inner
		if(marker==3) {
			m_Sounds[INT_SOUND_INNER]->PlayIfNotPlay(DSBPLAY_LOOPING);
		} else {
			m_Sounds[INT_SOUND_INNER]->StopIfPlay();
		}
		// Middle
		if(marker==2) {
			m_Sounds[INT_SOUND_MIDDLE]->PlayIfNotPlay(DSBPLAY_LOOPING);
		} else {
			m_Sounds[INT_SOUND_MIDDLE]->StopIfPlay();
		}
		// Outer
		if(marker==1) {
			m_Sounds[INT_SOUND_OUTER]->PlayIfNotPlay(DSBPLAY_LOOPING);
		} else {
			m_Sounds[INT_SOUND_OUTER]->StopIfPlay();
		}
	}
}

void CSounder::ProcessAPU()
{
	switch(SND_GET(SND_APU_START)) {
		case 1:
			m_Sounds[INT_SOUND_APU]->PlayIfNotPlay();
			SND_SET(SND_APU_START,2);
		break;
		case 3:
			SND_SET(SND_APU_START,0);
			m_Sounds[INT_SOUND_APU]->StopIfPlay();
		break;
	}
}

void CSounder::ProcessPKAI25()
{
	if(SND_GET(SND_FUEL_AI25_1)==1) {
		SND_SET(SND_FUEL_AI25_1,0);
		m_Sounds[INT_SOUND_FV_AI25_OPEN1]->Play();
	} else if(SND_GET(SND_FUEL_AI25_1)==2) {
		SND_SET(SND_FUEL_AI25_1,0);
		m_Sounds[INT_SOUND_FV_AI25_CLOSE1]->Play();
	}
	if(SND_GET(SND_FUEL_AI25_2)==1) {
		SND_SET(SND_FUEL_AI25_2,0);
		m_Sounds[INT_SOUND_FV_AI25_OPEN2]->Play();
	} else if(SND_GET(SND_FUEL_AI25_2)==2) {
		SND_SET(SND_FUEL_AI25_2,0);
		m_Sounds[INT_SOUND_FV_AI25_CLOSE2]->Play();
	}
	if(SND_GET(SND_FUEL_AI25_3)==1) {
		SND_SET(SND_FUEL_AI25_3,0);
		m_Sounds[INT_SOUND_FV_AI25_OPEN3]->Play();
	} else if(SND_GET(SND_FUEL_AI25_3)==2) {
		SND_SET(SND_FUEL_AI25_3,0);
		m_Sounds[INT_SOUND_FV_AI25_CLOSE3]->Play();
	}
}

void CSounder::ProcessRing()
{
	if(!BTN_GET(BTN_LIGHTS_TEST0)&&!BTN_GET(BTN_LIGHTS_TEST1)&&!GetN(AIRCRAFT_ON_GROUND)) {
		if((TBLE_CHGNR(TBL_BANK_L_HIGH)&&TBLE_GET(TBL_BANK_L_HIGH))||(TBLE_CHGNR(TBL_BANK_L_HIGH)&&TBLE_GET(TBL_BANK_L_HIGH)))
			m_Sounds[INT_SOUND_ZVONOK]->Play();
		if((TBLE_CHGNR(TBL_BANK_R_HIGH)&&TBLE_GET(TBL_BANK_R_HIGH))||(TBLE_CHGNR(TBL_BANK_R_HIGH)&&TBLE_GET(TBL_BANK_R_HIGH)))
			m_Sounds[INT_SOUND_ZVONOK]->Play();
		if(POS_CHGNR(POS_RV3M_LAMP)&&POS_GET(POS_RV3M_LAMP))
			m_Sounds[INT_SOUND_RV_VPR]->PlayIfNotPlay();
	}
}

void CSounder::ProcessStairs()
{
	if(SND_GET(SND_STAIRS_DOWN)) {
		SND_SET(SND_STAIRS_DOWN,0);
		m_Sounds[INT_SOUND_STAIRSOPEN]->Play();
	}

	if(SND_GET(SND_STAIRS_UP)) {
		SND_SET(SND_STAIRS_UP,0);
		m_Sounds[INT_SOUND_STAIRSCLOSE]->Play();
	}
}

void CSounder::ProcessFire()
{
	if(AZS_CHG(AZS_FIRE_SOUND_ALARM))
		SND_SET(SND_FIRE,0);

	if(SND_GET(SND_FIRE)==1&&AZS_GET(AZS_FIRE_SOUND_ALARM)) {
		m_Sounds[INT_SOUND_SIRENA_FIRE]->PlayIfNotPlay(DSBPLAY_LOOPING);
	} else {
		m_Sounds[INT_SOUND_SIRENA_FIRE]->StopIfPlay();
	}
}
