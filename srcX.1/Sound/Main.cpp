/*=========================================================================

  SD Yak-40 - Yakovlev 40 aircraft for Microsoft Flight Simulator X

  $Archive: /6015v2.root/6015v2/Src/Logic/Main.cpp $

  Last modification:
    $Date: 19.02.06 7:21 $
    $Revision: 9 $
    $Author: Except $

  Copyright (c) 1998-2006 by Nick Sharmanzhinov [except]

  All rights reserved.

=========================================================================*/

#include "Main.h"
#include "../../Res/sn/snd_res.h"
#include "Sounder.h"

#ifdef _DEBUG
_CrtMemState memoryState; 
#endif

HINSTANCE	g_hInstance=NULL;
HWND		g_hWND=NULL;

BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
#ifdef _DEBUG
	_CrtMemCheckpoint(&memoryState);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
#endif
	g_hInstance=(HINSTANCE)hModule;
	switch(ul_reason_for_call)	{
		case DLL_PROCESS_ATTACH: 
#ifdef _DEBUG
			AllocConsole();
#endif
			DisableThreadLibraryCalls((HMODULE)hModule);
			g_hWND=GetActiveWindow();
		break;
		case DLL_THREAD_ATTACH: 
		break;
		case DLL_THREAD_DETACH:
		break;
		case DLL_PROCESS_DETACH:
#ifdef _DEBUG
			//FreeConsole();
#endif
		break;
	}
	return TRUE;
}

void UpdatePanel()
{
	if(g_pSounder)
		g_pSounder->Update();

}

void FSAPI fnInitPanel(void)
{
	
}

void FSAPI fnDeinitPanel(void)
{
	if(g_pSounder) {
		delete g_pSounder;
		g_pSounder=NULL;
	}

#ifdef _DEBUG
	CHECK_MEM_LEAK("f:\\MemLeaks\\Yak40\\SN.log");
#endif
}

#ifdef GAUGE_NAME
#undef GAUGE_NAME
#endif

#ifdef GAUGEHDR_VAR_NAME
#undef GAUGEHDR_VAR_NAME
#endif

#ifdef GAUGE_W
#undef GAUGE_W
#endif

#define GAUGE_NAME         "main"
#define GAUGEHDR_VAR_NAME  snd_hdr
#define GAUGE_W            MISC_DUMMY_SX

extern PELEMENT_HEADER snd_list;
GAUGE_CALLBACK snd_cb;
GAUGE_HEADER_FS700(GAUGE_W, GAUGE_NAME, &snd_list, 0, snd_cb, 0, 0, 0);
MAKE_STATIC(snd_bg,MISC_DUMMY,NULL,NULL,IMAGE_USE_TRANSPARENCY,0,0,0)
PELEMENT_HEADER	snd_list = &snd_bg.header;

void FSAPI snd_cb( PGAUGEHDR pgauge, SINT32 service_id, UINT32 extra_data ) 
{
	switch(service_id) {
		case PANEL_SERVICE_CONNECT_TO_WINDOW:
			initialize_var_by_name(&MVSDAPIEntrys,SDAPISIGN); 
			g_pSDAPIEntrys=(SDAPIEntrys *)MVSDAPIEntrys.var_ptr; 

			if(CFG_GET(CFG_SOUND_ENABLE)) {
				if(!g_pSounder)
					g_pSounder=new CSounder();
				if(g_pSounder)
					g_pSounder->Init();
			}
		break;
		case PANEL_SERVICE_PRE_UPDATE:
			UpdatePanel(); 
		break;
	}
}

GAUGESIMPORT	ImportTable =						
{													
	{ 0x0000000F, (PPANELS)NULL },						
	{ 0x00000000, NULL }								
};													

GAUGESLINKAGE Linkage={
	0x00000013,
	fnInitPanel,
	fnDeinitPanel,
	0,0,
	FS9LINK_VERSION,
	{
		(&snd_hdr),
		0
	}
};

